Introduction 19

Christian denies the unbreakable connection between the case laws
and the Ten Commandments, he must then seek to apply the “general
moral principles” of the Decalogue to his own society in order to provide
legitimacy to the “common legal order.” Yet he is then forced by his
theory of natural law to defend the Decalogue’s highly general princi-
ples in terms of their common status among all ‘right thinking” people.

There is a major problem here: there have been so many wrong-think-
ing tyrants and societies in history. Christians have suffered under many
of them, usually in silence, for they have been taught that there are
no specific legal standards of righteousness on which to base a legiti-
mate appeal to God {for example, by corporately praying the im-
precatory psalms, such as Psalm 83). Nevertheless, Christians again
and again have proclaimed their nearly unqualified allegiance to this
or that humanist alternative to biblical social order. They base their
allegiance on the supposed “natural conformity” to the Decalogue of
their societies’ legal order. Natural law theory then becomes an all-
purpose smoke screen for the Christians’ passive (or even active)
acceptance of specific social evils.

The Problem of Social Reform

‘The acceptance of natural law philosophy inevitably leads to two
possible and recurring evils. First, it paralyzes the Christians’ legiti-
mate efforts to reform society, for it denies that there are specific bib-
lical blueprints for social reform. This is the curse of the pietistic
escape religion on Christianity. Second, it enables humanist reformers
to enlist Christians in this or that reform effort that is wrapped in the
language of the Ten Commandments but which is in fact inspired by
covenant-breakers and designed to further their aims. This is the
curse of the power religion on Christianity.

In American history, no better example exists of both of these
processes than the Unitarians’ successful enlisting of evangelical
Christians in the State-centralizing abolitionist movement.*? The
fact is, the Quakers had pioneered the theory of abolitionism in the
1755-75 period, decades before the Unitarian Church even existed.
The unwillingness of trinitarian American Christians to obey the
New Testament teachings with regard to the illegitimacy of lifetime
chattel slavery allowed the Unitarians to capture the Quakers’ issue

39. Otto Scott, The Secret Six: John Brown and the Abolitionist Movement (New York:
Times Books, 1979), reprinted by the Foundation for American Education; Bertram
Wyatt-Brown, Lewis Tappan and the Evangelical War Against Slavery (Cleveland, Ohio:
Case Western Reserve University Press, 1969),
