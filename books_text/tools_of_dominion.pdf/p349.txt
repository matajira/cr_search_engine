The Costs of Private Conflict 341

The judicially significant point is that the person who wins the
conflict physically becomes the loser economically, The one whe is
still walking around after the fight must finance the physical
recovery of the one who is in bed. The focus of judicial concern is on
the victim who suffers the greatest physical injury. Biblical law and
Jewish law impose economic penalties on the injury-inflicting victors
of such private conflicts. As Maimonides put it, “The Sages have
penalized strong-armed fools by ruling that the injured person
should be held trustworthy. . . .”*

Games of Bloodshed

The murderous “games” of ancient Rome, where gladiators slew
cach other in front of cheering crowds, violated biblical law. The same
is true of “sports” like boxing, where the inflicting of injuries is basic
to victory. The lure of bloody games is decidedly pagan. Augustine,
in his Confessions, speaks of a former student of his, Alypius. The young
man had been deeply fond of the Circensian games of Carthage.
Augustine had persuaded him of their evil, and the young man stopped
attending. Later on, however, in Rome, Alypius met some fellow
students who dragged him in a friendly way to the Roman amphi-
theater on the day of the bloody games. He swore to himself that he
would not even look, but he did, briefly, and was trapped. “As he saw
that blood, he drank in savageness at the same time. He did not turn
away, but fixed his sight on it, and drank in madness without know-
ing it. He took delight in that evil struggle, and he became drunk on
blood and pleasure. He was no longer the man who entered there,
but only one of the crowd that he had joined, and a true comrade of
those who brought him there. What more shall I say? He looked, he
shouted, he took fire, he bore away with himself a madness that should
arouse him to return, not only with those who had drawn him there,
but even before them, and dragging others along as well.”> Only
later was his faith in Christ able to break his addiction to the games.

 

miliation, ‘hese five effects are all payable from the injurer's best property, as is the
law for all who do wrongful damage.” Moses Maimonides, The Book of forts, vol. 11
of The Code of Maimonides, 14 vols, (New Haven, Connecticut: Yale University Press,
1994), “Laws Concerning Wounding and Damaging,” Chapter One, Section One,
p. 160. Maimonides made one strange exception: if a person deliberately frightens
someone, but does not touch him, he bears ne legal liability, only moral liability.
Even if he shouts in 2 person’s ear and deafens him, there is no legal liability. Only if he
touches the person is there legal liability: sbid., Chapter Two, Section Seven, pp. 165-66.

4. Torts, Chapter Five, Section Four, p. 177.

5. The Confessions of St. Augustine, trans. by John K. Ryan (Garden Ci
York; Tmage Books, 1960), Book 6, ch. 8.

 

 

 

 
