What Is Covenant Law? 81

Satan was a liar, but not so great a liar as to deny the idea of pre-
dictable sanctions in history. He simply denied God’s negative sanc-
tion and promised Eve a positive one, Would that modern Christian
theologians were as honest as Satan! Instead, they deny the very ex-
istence of predictable covenantal sanctions in New Testament times.
They write such things as: “And meanwhile it [the common grace or-
der] must run its course within the uncertainties of the mutually con-
ditioning principles of common grace and common curse, prosperity
and adversity being experienced in a manner largely unpredictable
because of the inscrutable sovercignty of the divine will that dis-
penses them in mysterious ways.”?? This muddled prose matches an
equally muddled concept of ethics and history. In English, this state-
ment means simply that there is no ethical cause-and-effect relationship in
post-crucifixion history.

Biblical case laws are still morally and judicially binding today.
This is the thesis of Tools of Dominion. Kline’s theology explicitly
denies this. Second, Kline’s argument also means the denial of God’s
sanctions ~blessing and cursing—in New Testament history. It is
the denial of any long-term cause-and-effect relationship between
covenantal faithfulness and external blessings—positive feedback
between covenant-keeping and vistéle blessings. It is also the denial
of any long-term cause-and-effect relationship between covenantal
unfaithfulness and external cursings. Thus, when I refer to “anti-
nomianism,” I have in mind the hostile attitude regarding ethical
cause and effect in society—soctal antinomianism™—but also a
deeper and more fundamental hostility: a denial, implicit or explicit,
of the reliability of the covenantal promises (sanctions) of God in
history.

5. Succession/Continutty/Inkeritance

If you die, you do not inherit. If you die without children, some-
one else inherits. Who would inherit in history if Eve listened to the
serpent and did what he recommended?

27. Meredith G. Kline, “Comments on the Old-New Exror,” Westminster Theologi-
cal Journal, XLI (Fall 1978), p. 184.

28, Gary North, The Sinai Strategy: Economics and the Ton Commandments (Tylex,
‘Texas: Institute for Christian Economics, 1986), Appendix C: “Social Antinomian-
ism.”
