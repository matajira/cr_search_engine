What Is Covenant Law? 79

God the choice. God sent the worst one, nationally speaking: a
plague that killed 70,000 people (II Sam. 24), (Anyone who teaches
that God does not send sickness to His people has a real problem in
explaining this passage.) In short, covenantal representation ix important.

‘There are theologians today who say that God’s law applies only
to individuals, that nations are not under God’s law. They deny the
very possibility of a national covenant in New Testament times.
Such a covenant was only for ancient Israel. National leaders are not
representatives of their subordinates before God, theologians insist,
and national leaders are surely not God’s representatives before their
subordinates. God’s law has nothing to do with politics, they insist.
There is no hierarchy of appeal based on God’s law. There is no na-
tional covenant: this is a basic philosophy of all modern secular politi-
cal theory, and few Christian scholars disagree. And those few who
are willing to affirm the legitimacy of a national covenant gag on the
idea of a future international covenant. International covenants are
unthinkable for them. Not so for Isaiah (19:18-25).2!

3. Ethics/Law/Dominion

“Forget about the law against eating this fruit,” Satan told Eve.
“Go ahead and eat.”

“Do what thou wilt shall be the whole of the law,” announced the
self-proclaimed early twentieth-century magician, Aleister Crowley,
who also called himself the Beast and 666.” The ethical positions are
the same. The results are also the same.

“We're under grace, not law.” This is the fundamentalist Chris-
tians’ version of the same ethical position. So is, “No creed but
Christ, no law but love!” They do not mean what Paul meant: that
Christians are no longer under the threat of the negative efernal sanc-
tions of the law, They mean rather that God’s law no longer applies
in any of the five aspects of the covenant, eternally or historically.

Christian social thinkers, especially neo-evangelicals in the
Wheaton College-InterVarsity Press-Chrstianity Today orbit, prefer
to muddy the ethical waters by using fancier language than the fun-
damentalists use. Examples:

21. Gary North, Healer of the Nations: Biblical Blueprints for International Relations (Ft.
Worth, Texas: Dominion Press, 1987).

22. Aleister Crowley, Magick in Theory and Practice (New York: Castle, n.d.), p.
193. A short biography of Crowley is Daniel P. Mannix, The Beast (New York:
Ballentine, 1959).
