Pollution, Ownership, and Responsibility 565

These groups of beneficiaries can become allies of the polluter in
any political dispute concerning the continuation of the polluting
practices. Edwin Dolan’s comment is applicable: “If he has to clean
up he may pass part of the cost along to his customers in the form of
a price increase, so his customers may testify on his behalf before the
city council. If less of the product can be sold at the higher price, he
may have to lay off some of his workers, and thus his employees may
join the propollution faction. The addition of these allies does not
alter the normative analysis of the situation, for if the act of pollution
itself is a crime then these allies are nothing but partners in crime.
The customers of the firm are in a position analytically identical to
the recipient of stolen goods. The producer kept his price low only by
forcing the residents adjacent to his establishment involuntarily to
subsidize the cost of production, by permitting their lungs and noses
to be used as industrial waste disposal units, substituting for the me-
chanical units which should havé been installed at the plant. The
customers no more deserve to benefit from this tactic than the owner
himself.”

Notice that Dolan explicitly uses normative economic analysis. He
does not ignore ethics. To ignore ethics as a matter of methodological
objectivity, as most humanistic free market economists claim that
they must,” is to subsidize immorality. They are importing immor-
ality into their “neutral” economic analysis, all in the name of scientific
objectivity. There are always unrecognized and uncompensated victims
of “neutral” economic analysis, at least in those cases when policy-
makers take seriously an economist’s suggestion (sometimes called “a
conclusion of scientific economics”).

Allocating Pollution

Dolan’s analogies are both clever and graphic: consumers as “re-
cipients of stolen goods,” and nearby residents as “unpaid organic
pollution-absorption devices.” We need to pursue the analogy of the
consumer as a receiver of stolen goods. If a buyer of a domestically
produced consumer good is enabled to make the purchase at a lower

29. Edwin G. Dolan, TANSTAAFL: The Economic Strategy for Environmental Crisis
(New York: Holt, Rinehart & Winston, 1971), pp. 42-43. TANSTAALL, the book’s
cover tells us, stands for: “there ain’t no such thing as a free lunch.”

30. In fact, they sneak in their ethical views through the back door of applied eco-
nomics—evaluating economic policies~ and also through aggregation. See Appen-
dix D: “The Epistemological Problem of Social Cost.”

 
