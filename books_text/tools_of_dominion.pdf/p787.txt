Finders Should Not Be Keepers 779

the animal or other lost object. It is also difficult to imagine what
civil penalties might be attached te this law. We therefore should
conclude that the enforcement of this law is based on self-government
under God's law. The person who returns a lost object to its owner is
demonstrating that he acted out of concern for the law, not out of
concern for the civil magistrate. He is a person who cxercises self-
government under law. Again, it becomes more difficult to entertain
suspicions about his overall ulterior motives.

Maimonides’ Rule and Social Conflict

Clearly, Moses Maimonides’ rule would drastically increase
hostilities between Jews and gentiles: “The lost property of a heathen
may be kept, for Scripture says, Lost thing of thy brother’s (Deut. 22:3).
Furthermore, if one returns it, he commits a transgression, for he is
supporting the wicked of the world.”3 In other words, returning lost
property to a gentile is primarily a form of economic subsidy, not
primarily an honoring of the principle of owner’s rights. Et is reveal-
ing that he cited Deuteronomy 22:3, which refers to the lost property
of one’s brother, and made no mention of Exodus 23:4-5, which ex-
plicitly deals with the lost property of enemies.

He did add this qualification: “But if one returns it in order to
sanctify God’s name, thereby causing persons te praise the Israelites
and realize that they are honest, he is deemed praiseworthy. In cases
involving a profanation of God’s name, it is forbidden to keep a
heathen’s lost property, and it must be returned.”* In other words, in
order to maintain the appearance of honesty, the property should be
returned. The problem was, of course, that eventually these rules
would become known to the gentile community, and they would
learn the truth about those Jews who follow Maimonides’ precepts:
they are governed by a very different concept of honesty from what
the Bible itself establishes. At that point, the rule of expediency
would be recognized for what it is, and would thereforc backfire,
bringing reproach on the Jewish community. This is not the way to in-
crease social peace between hostile religious groups in a community.

If the town is equally inhabited by Jews and gentiles, he said, the
Jew has to advertise that he has found lost property.* But if the town

3. Moses Maimonides, The ouk of Torts, vol. Ul uf The Code of Maimonides, 14 vols,
(New Haven, Gunnecticut: Yale University Press, [1180] 1954), “Laws Concerning
Robbery and Lost Property,” Chapter Eleven, Section Three, p. 128.

4, Idem,

8. Ibid., Section Six, p. 129.

 
