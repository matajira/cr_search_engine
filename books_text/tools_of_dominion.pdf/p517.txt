Proportional Restitution 509

ments sometimes pass usury laws that establish a price ceiling on in-
terest rates. These laws are almost always applied only on monetary
transactions. As with any price control, a usury law will reduce the
number of transactions at the coercively fixed price. It will reduce
the supply of loanable funds, since lenders do not wish to loan
money at an artificially undervalued rate of return. * Usury laws are
the destroyers of civilization, for they impede the free flow of capital.
Indeed, if they could be fully enforced, usury laws that prohibit all
interest payments would make impossible the creation of capital
goods, for capital goods are the product of 1) human labor (including
intellectual insight and entrepreneurship) combined with 2) raw ma-
terials 3) over time.‘ All three must be paid for: labor (wages), raw
materials (rent), and time (interest). Usury laws deny the legitimate
return of the third component of a capital good.'®

This process of capitalization means that the higher the prevail-
ing interest rate, the smaller the cash payment that a buyer will offer
for a piece of land today: the buyer applies a higher discount to its ex-

14. A low official rate of interest makes it appear as though people are discounting
future income at a lower rate than is actually the case. Thus, a legislated (or fiat-
money-induced) lower rate of interest will make it appear as though buyers are
willing to offer higher prices for land bought by means of long-term debt contracts
(mortgages). But this is an illusion created by the government's usury law. In the
case of property sold by a seller who is willing to finance the sale by accepting a long-
term debt contract from the buyer, he will have ta accept a lower price if the market's
true rate of interest exceeds the official interest rate ceiling; otherwise the buyer will
not buy. A usury law, like any price control, is analogous to placing a limit on a
thermameter’s scale. A cap on a thermometer does not reduce the fever of the sick
person; it simply keeps people from assessing the true conditions. A usury law
creates an illusion of a lower rate of discount than market transactors voluntarily
agree upon.

15. Rothbard, Man, Economy, and State, ch. 6.

16. There is no surer way to identify a crackpot theory of economics than to ex-
amine what the economist’s theory of interest is. If he denies the legitimacy of inter-
est in morally legitimate profit-seeking transactions, he is not an economist; he is a
monetary crank. If he denies interest as a theoretically inescapable tool of economic
analysis, he is a true crackpot, as nully as a man who promotes the idea of the possi-
bility of a perpetual motion machine. But he is far more dangerous: legislators do
not listen to “scientists” who would propose making illegal all machines except per-
petua] motion machines. Legislators have on occasion passed usury laws that are
based on the idea that interest is illegitimate, The most precise discussion of interest
is still Eugen von Bohm-Bawerk’s classic study, History and Critique of Interest Theories
(1884), which is volume 1 of Capital and Interest, 3 vols. (South Holland, Iltinois:
Libertarian Press, 1959). This publishing firm is now located in Spring Mills, Penn-
sylvania.
