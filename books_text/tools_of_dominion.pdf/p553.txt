Pollution, Ownership, and Responsibility 545

economic theory. The economists almost never discuss this embar-
rassing fact, although the more sophisticated members of the eco-
nomics profession have been aware of it since at least 1938, but it is
nonetheless a fact.

Thus, both collectivism and free enterprise face a growing prob-
Jem, the problem of minimizing the negative effects of pollution
without simultaneously destroying the benefits of economic growth.
Ncither variety of secular economic theory has a scientific answer to
this problem. This is why Christian economics is needed. This is
why we must begin our economic analysis with Exodus 22:5-6.

Capturing Economies for Christ

The theoretical and practical problems associated with the pollu-
tion question are numerous. The problems are ethical, technical,
theoretical, and ultimately philosophical. Economists do not like to
admit that all problems in applied economic theory have inherent
and inescapable ethical and philosophical aspects, so they tend to
ignore or even suppress these aspects of applied economics when dis-
cussing them in their scholarly journals. This is why modern eco-
nomics to a great extent is fraudulent—a mental contrivance to con-
ceal fundamental ethical issues, a scries of rarified mental exercises
devised for agnostics by agnostics. But the agnostics maintain mon-
opoly control over the professional journals because they control the
funds, the academic institutions, and the certification of younger
scholars. This epistemological agnosticism must change if economics
as a discipline is to be saved, but only self-consciously Christian
scholars can redeem it.

How should Christians go about redeeming any academic dis-
cipline? By beginning with the whole Bible as academically and
professionally authoritative. Christians must begin to tackle those
intellectual problems for which the humanists have no consistent an-
swers, In the case of economics, Christians must follow the lead set
by Cornelius Van Til in philosophy. Van Til did not ask: “Is Chris-
tian philosophy valid?” He started with the premise that there is no
valid philosophy except Christian philosophy. That is what I have
asserted with regard to Christian economics. The humanists have
run out of internally consistent answers. In fact, they never had ac-
curate answers that were not implicitly based on biblical presupposi-
tions, and the farther away the economists get from the Bible, the
fewer accurate answers they can provide.
