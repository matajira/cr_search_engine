390 TOOLS OF DOMINION

nent judgment. Ged pays back what is owed to the sinner, It is repay-
ment in kind, an original meaning of “retaliate.” Capital crimes re-
quire the public execution of the guilty person. In the case of crimes
less repugnant to God than capital crimes, economic restitution is
often paid by the criminal to the victim. But restitution is ultimately
owed to God.” The victim, as God’s image bearer, deserves his resti-
tution, just as God deserves His. When repayment in kind is not
made, a sense of injustice prevails. The victim, or the family mem-
bers who survive the victim, understand that a convicted criminal
who is not forced to make restitution has evaded justice. Such an
escape is seen as being unfair,

Fair Warning

God reminds His people that His ultimate justice cannot be
evaded. This testimony of a final judgment is provided by the sanc-
tions imposed by the authorities. Historical sanctions are designed
by God to fit the crime in order to persuade men that the universe is ul-
timately fair, for both time and eternity are governed by the decree of God. God's
people should not despair because some men escape the earnest
(down payment) of the final justice that is coming. The 73rd Psalm is
a reminder of the seeming injustice of life, and how the wicked are
finally rewarded according to their deeds. “For I was envious at the
foolish, when I saw the prosperity of the wicked” (Ps. 73:3), David
was beaten down by events (v. 2), yet he saw all the good things that
come to the wicked in life (vv. 4-5, 12). He flayed himself with such
thoughts, “Until I went into the sanctuary of God; then understood I
their end. Surely thou didst set them in slippery places: thou castedst
them down into destruction. How are they brought into desolation,
as in a moment! They are utterly consumed with terrors” (vv. 17-19).
David finally admits: “So foolish was I, and ignorant: I was as a
beast before thee” (v. 22).

The relationship between covenantal faithfulness and external
prosperity is clearly taught in the Bible (Deut. 28:1-14). So is the rela-
tionship between covenant-breaking and calamity (Deut. 28:15-68).
This system of sanctions applies to the whole world, not just in Old
Testament Israel. Deny this, and you have also denied the possibility
of an explicitly and exclusively Christian social theory. Christians
who deny the continuing relevance of Deutcronomy 28's sanctions in

30. R. J. Rushdoony, The institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), pp. 525-30.
