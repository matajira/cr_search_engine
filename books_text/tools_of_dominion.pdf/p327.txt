Victim’s Rights vs. the Messianic State 319

And the man that Jieth with his father’s wife hath uncovered his father’s
nakedness: both of them shall surely be put to death; their blood shall be
upon them (Lev. 20:11).

And if a man lie with his daughter in law, both of them shall surely be
put to death: they have wrought confusion; their blood shall be upon them
(Lev. 90:12).

If a man also lie with mankind, as he lieth with a woman, both of
them have committed an abomination: they shall surely be put to
death; their blood shall be upon them (Lev. 20:13).

And if a man lie with a beast, he shall surely be put to death: and ye
shall slay the beast (Lev. 20:15).

And if a woman approach unto any beast, and lie down thereto,
thou shalt kill the woman, and the beast: they shall surely be put to
death; their blood shall be upon them (Ley. 20:16).

A man also or woman that hath a familiar spirit, or that is a wizard,
shall surely be put to death: they shall stone them with stones: their
blood shall be upon them (Lev. 20:27).

And be that blasphemeth the name of the Loxp, he shall surely be
put to death, and alll the congregation shall certainly stone him: as well
the stranger, as he that is born in the land, when he blasphemeth the
name of the Lorp, shall be put to death (Lev. 24:16).

And he that killeth any man shall surely be put to death (Lev,
24:17).

I the Lorn have said, I will surely do it unto all this evil congregation,
that are gathered together against me: in this wilderness they shall be con-
sumed, and there they shall die (Num. 14:35).

For the Lorp had said of them, They shall surely die in the wilderness.
And there was not left a man of them, save Caleb the son of Jephunneh,
and Joshua the son of Nun (Num. 26:65).

And if he smite him with an instrument of iron, so that he die, he is
a murderer: the murderer shall surely be put to death. And if he smite
him with throwing a stone, wherewith he may die, and he die, he is a
murderer: the murderer shall surely be put to death. Or if he smite him
with an hand weapon of wood, wherewith he may die, and he die, he is
a murderer: the murderer shall surely be put to death (Num. 35:16-18).
