The Hoax of Higher Criticism 1065

fear of losing the basis for certainty of faith, and fear of posing ques-
tions in the area of authority.”5 He, too, identifies English Deism as
the source of this intellectual development. “The eighteenth-century
Deists treated the Bible with freedom when it did not, in their lights,
accord with reason. For example, they argued that Isaiah was com-
posite, the Gospels contradictory, and the apostles often unreliable.”6

The Deist’s attack on the divine authority of the Bible was not
simply a product of the scholar’s dusty study. It was closely associ-
ated with warring social and intellectual movements of the day.
James Barr's observations are very important in understanding the
roots of higher criticism and also in understanding the revival of bib-
lical literalism as a social force in the United States, especially after
1960. The link between social action and biblical hermeneutics has
been missed by most historians. Barr, following Reventlow’s lead,
does not make this mistake:

Church and state formed a single continuum, and political and theological
questions were seen as interdependent. Questions about power and legiti-
macy rested in a high degree upon exegetical and interpretative ideas. In
this the Old Testament—Reventlow’s own specialism —was of primary im-
portance. Even if the New Testament was the document of the earliest
Christianity, the way in which the other collection of books form a yet older
age, the Old Testament, was relatcd to it. For it was the Old Testament, as
it seemed, that offered guidance about king and state, about a common-
wealth organized under divine statutes, about law and property, about war,
about ritual and ceremony, about priesthood, continuity and succession.
All of this was a disputed area from the Reformation onwards: because
these were controversial matters in church and state, they generated deep
differences in biblical interpretation. It was precisely because the Bible was
assumed on ail hands to be authoritative that it stimulated new notions
about its own nature. It was because men sought answers to problems of life
and society, as well as of thought and belief, that the Bible stimulated ‘criti-
cal modes of understanding itself.”

 

The heart of English Deism’s attack on Christian orthodoxy was
its faith in Newtonian natural law and hostility to Old Testament
law and Old Testament prophecy. “If one could write off the Old
Testament as testimony to a pre-Christian religion and vindicate the
New Testament in another way (e.g. through its accord with the law

5. Krentz, op. eit., p. 15.
6. Lbid., p. 16.
7. James Bare, Foreword, in Reventlow, Authority of the Bible, p. xiii.
