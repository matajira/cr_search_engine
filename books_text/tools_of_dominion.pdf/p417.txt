Criminal Law and Restoration 409

puts too much power in the hands of guards, whe can then indulge
their tastes in brutality. It puts too much power in the hands of
parole boards, who can shorten a man’s sentence irrespective of the
crime, thereby making the punishment fit the board’s assessment of
the criminal, not the judge’s assessment of the effects of the crime—or
more to the point, making the punishment fit the latest humanistic
theory of criminal behavior and social responsibility, not the crime.°?

Left-wing humanists have begun to see the threat to justice posed
by the indeterminate sentence.® Mitford has described the indeter-
minate sentence as “a potent psychological instrument for inmate
manipulation and control, the ‘uncertainty’ ever nagging at the
prisoner's mind a far more effective weapon than the cruder ones
then [in the 1870's] in vogue: the club, the starvation regime, the
iron shackle.”® Because of doubts regarding the prison as a means of
correcting evil behavior, we have seen an increasing resistance by
juries and judges to send first offenders or minor offenders to prison.
But because restitution has not yet become a common means of pun-
ishing criminals, these “minor” criminals receive no punishment,
other than having to report occasionally to an overburdened proba-
tion or parole officer.®

These same humanists look at the “eye for eye” principle, and
react in horror. They do not react with equal consternation when

93. C. 8. Lewis, “The Humanitarian Theory of Punishment,” in Lewis, God in the
Dock: Essays on Theology and Ethics, edited by Walter Hooper (Grand Rapids, Michi-
gan: Eerdmans, 1972), pp. 287-300.

94. Jessica Mitford, Kind and Usual Punishment: The Prison Business (New York:
Knopf, 1973), ch. 6. ‘Those who have opposed capital punishment have denounced it
as cruel and unusual. Mitford’s attack implies that imprisonment is, too. What,
then, is legitimate punishment? The Bible gives us guidelines; few humanists do.

95. THid., p. 82.

96. Charles Manson, who led the “family” (gang) of murderers who killed actress
Sharon Tate and several others in 1969, was on parole from prison at the time.
Others in his “family” were also on probation. As the prosecuting attorney later
wrote: “Manson associated with ex-cons, known narcotics users, and minor girls.
He failed to report his whereabouts, made few attempts to obtain cmployment, re-
peatedly lied regarding his activities. During the first six months of 1969 alone, he
had been charged, among other things, with grand theft auto, narcotics possession,
rape, contributing to the delinquency of a minor. There was more than ample rea-
son for parole revocation.” Vincent Bugliosi, Helter Skelier- The True Story of the Manson
Murders (New York: Norton, 1974), p. 420. Manson’s parole officer stated in court
that he could not remember whether Manson had been on probation or parole; the
man was responsible for overseeing 150 persons (p. 419). Manson had actually begged
to be allowed (o remain in jail when they released him in 1967; at that time, he was
32 years old, and had spent 17 years in penal and reform institutions (p. 146).

 
