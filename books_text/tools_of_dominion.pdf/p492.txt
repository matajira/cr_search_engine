484 TOOLS OF DOMINION

Personal liability insurance is a development of the West that
allows criminally negligent people a greater opportunity to escape
the death penalty by means of high payments to the heirs of their vic-
tims. Purchasing such insurance is not to become mandatory, except
in cases related to the use of State-financed capital (e.g., highways).
Nevertheless, the risk is so high— execution —and the cost of prem-
iums so low in comparison to the risk, that personal liability cover-
age is available to most people, Only the very poor, who would not
normally own “oxen” (expensive capital equipment), or people con-
victed repeatedly of criminal negligence or actions that would lead to
convictions for criminal negligence (e.g., drunk driving), or people
who manage or own businesses that create high risks for innocent
bystanders, would normally be excluded from the purchase of such
insurance coverage. They would have to learn to handle their “oxen”
with care.
