Selling the Birthright; ‘The Ratification of the U.S. Constitution 1205

to discuss publicly. The reader must search his footnotes for the ap-
propriate bibliographical leads, and very few readers do this. He
only discusses freemasonry in relation to the French Revolution,
which he knows was pagan to the core, and in relation to New Eng-
land in the nineteenth century. He insists that “This decline came
later. At the time of the Revolution and much later, New England
and the rest of the country shared a common faith and experience.”5!
Absolutely crucial to his interpretation of Constitutional history is
what he never mentions: the legally secular (“neutral”) character of
Article VI, Section 3. He pretends chat it does not says what it says,
and it does not mean what it has always meant: a legal barrier to
Christian theocracy. Instead, he rewrites history:

Forces for secularization were present in Washington’s day and later,
French sympathizers and Jacobins, deists, Illuminati, Freemasons, and
soon the Unitarians. But the legal steps towards secularization were only
taken in the 1950’s and 1960's by the U.S. Supreme Court, For the sake of
argument —G.N.], we may concede to the liberal, and to some ortho-
dox Christian scholars,*? that Deism had made extensive inroads into
America by 1776, and 1787, and that the men of the Constitutional Conven-
tion, and Washington, were influenced by it. The fact still remains that
they did not attempt to create a sccular state, The states were Christian
states, and the federal union, while barred from intervention in this area,
was not itself secular. The citizens were citizens of their respective states
and of the United States simultaneously. They could not be under two sets
of religious law.

 

This is mytho-history designed to calm the fears of Bible-believing
Gbristians as they look back to the origin of the Constitution. Of
course the Framers created a secular state. The secular character of
the Federal union was established by the oath of office. Politically,
the Framers could not in one fell swoop create a secular state in a
Christian country; judicially and covenantally, they surely did.
Hamilton made it clear in Federalist 27 that the oath of allegiance to
the Constitution superseded all state oaths. That was why he insisted
on it. Yet Rushdoony substitutes the language of church worship

51. Rushdoony, This Independent Republic: Studies in the Nature and Meaning of Ameri-
can History (Fairfax, Virginia: Thoburn Press, [1964] 1978), p. 60.

52, He seems to have in mind here C. Gregg Singer’s A Theological Interpretation of
American History (Nutley, New Jersey: Craig Press, 1964), ch. 2: "Deism in Colonial
Life.”

53. Rushdoony, Nature of Ameriian History, p. 48.

 
