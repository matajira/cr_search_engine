936 TOOLS OF DOMINION

A Preference for Irresponsibility

In the Northern Kingdom from the days of Jeroboam’s revolt,
there were only two publicly acceptable operating religious systems:
the worship of Jehovah by means of Baalist icons and practices (the
golden calves: I Ki. 12:28) and the worship of Baal by means of
Baalist icons and practices (I Ki. 18). Elijah challenged the represen-
tatives of the people of Israel to choose between Baal and Jehovah,
but they answered not a word (I Ki, 18:21), Even when they at last
declared themselves in favor of God (I Ki. 18:39), it was only as a
result of God’s display of greater supernatural but highly visible
power, and their commitment did not last longer than Elijah’s ability
to repeat such displays on a regular, invariable basis. In their
deepest apostasy, they became disciples of the power religion, They
had returned to Egypt spiritually.

The Northern Kingdom was worse in this regard than the Southern
Kingdom was. Judah did have the temple, It had a ritually acceptable
religion. It never adopted pure Baalism. God therefore delivered
Israel into captivity to the Assyrians more than a century before He
delivered Judah (and Assyria) to the Babylonians. Even so, He had
graciously waited several centuries to deliver up Israel to her ene-
mies. The Northern Kingdom's religious practices had been corrupt
from the beginning, but there are degrees of corruption. For a time,
God graciously delays bringing His negative sanctions in history. It
is not that He honors corruption, instead, He honors the absence of
fully developed corruption. But corruption, like “incorruption,” does
not remain idle. Corruption either grows or contracts. Both cor-
ruption and righteousness are kingdom principles. It depends upon
which kingdom we are discussing: God’s or Satan’s. Each kingdom
seeks extension geographically, temporally, institutionally, and psy-
chologically. Each serves as leaven."! Each recognizes that, in princi-
ple, there can be no neutrality. Each therefore recognizes that as
time goes on, there will be less and less cooperation possible between
covenant-keepers and covenant-breakers.

Progressive Ethical Self-Consciousness
Covenant-breakers generally recognize the nature of this ethical
and institutional conflict much earlier than covenant-keepers do.

11. Gary North, Unconditional Surrender: God's Program for Victory (3rd cd.; Tyler,
Texas: Institute for Christian Economics, 1987), pp. 315-19, 325-26.
