The Human Commodity 377

influence of self-interest. . . . The freedmen were the beneficiaries
of emancipation, not of slavery.”#

They forget that emancipation from demonism is the first ste
ward long-term economic success. ‘Lhe slaves went through Lwo stages
of social emancipation: first, when the original Africans were (rans-
ported by force to the insuflicienUy Christian South, and second,
when their heirs were emancipated from their insufficiently ethical
masters, Although the original acts of kidnapping were immoral,
their long-term results were to the benefit of those victimized
Africans who survived the Atlantic passage and the early years of
their enslavement.** The critics also forget that what men regard as
economic self-interest varies widely across the globe, culture to cul-
ture. Men respond to incentives and opportunities (problems) in
different ways. To imagine that the freedmen of 1865-80 responded
to their economic environment in approximately the same way that
their savage, demon-worshipping, shaman-manipulated forebears
would have responded is not only naive, it is positively denigrating
to the economic and spiritual wisdom of the freedmen.* More to the
point, it is all too favorable to their ancestors, not to mention the
pagan gods that they worshipped.

What we must recognize is that bondage to sin produces bond-
age in other areas of life, both personal and cultural. Neither judicial
emancipation nor slavery is in itself a solution to the bondage of sin.
Slavery in tribal Africa would not have solved the black African’s
spiritual poverty, but slavery in a spiritually compromised Christian
culture eventually led to his hoped-for emancipation. Hard work as
slaves within the cultural framework of a generally free and gener-
ally Christian society was a better training ground for a slave’s even-
tual emancipation than hard work as a slave within some shaman-

to-

 

governed tribe,
Freedom begins with internal regeneration, and then steadily
works its effects outward, If spiritual freedom is not allowed by civil

48. Ransom and Sutch, One Kind of Freedom., p. 22.

49. It would be preposterous to deny the benefits to Israel of Solomon's wisdom
just because he was the product of a marital union originally based on adultery and
murder. The undeniable evil of the latter dues not negate the equally undeniable
benefits of the former,

50. T have no doubt that the proportional representation of saints in heaven is
much higher for nineteenth-century American slaves than it is for twentieth-century
economists. The bulk of the economists will be spending eternity with the shamans
who stayed behind in Africa.
