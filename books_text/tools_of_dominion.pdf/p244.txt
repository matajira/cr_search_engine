236 TOOLS OF DOMINION

What is truly remarkable in retrospect is that actual practice
throughout the South was very different from the civil law’s lack of
concern regarding slave marriages, The strong influence of the black
churches before and after emancipation helped keep slave marriages
intact.®® White churches also sometimes pressured black slaves to re-
main married. In a survey of blacks who were seeking to be legally
married after emancipation in Mississippi in 1864-65, records indi-
cate that perhaps one out of six reported that a prior marriage had
been separated by forced sale, or 17 percent (therefore leaving 83
percent intact).® Obviously, this threat of separation was an impor-
tant one in the owners’ available sanctions against slaves, but it was
one that had to be used sparingly if the black family structure was to
remain intact. If breakups through forced sale had been common,
then slave families would not have remained stable,

Planters also recognized that the maintenance of slave discipline
required the formation of slave families. Gutman is correct: “Only
those slaves who lived in affective familial groupings (and especially
the greatly prized slave husband and father) could respond to in-
direct and direct incentives that exploited their familial bonds. Mon-
etary rewards based on family labor (such as the slave garden plot)
and incentive payments for ‘extra’ work balanced the threat of the
sale of relatives and especially grown children. A busband and father
might work harder to get extra rations for his children, to earn cash
to purchase a luxury item for his wife, or to prevent his children
from being sold.” Owners generally kept families together at least
until the children reached adulthood.*

This story of the strong slave family structure was not recognized
by most scholars of antebellum period until the pioneering studies by
Herbert Gutman were published in the 1970’s. The influential sociol-
ogist E, Franklin Frazier had painted a very different picture in the
previous academic generation. Based on his accurate understanding
of the South’s civil laws regarding slave marriages, he drew conclu-
sions that would have followed, had church law and actual custom
not been radically different from the civil law. By removing the male
as head of the slave household, he concluded, females tended to be-

60. Z6id., pp. 70-75.
61, Ibid., pp. 286-87.
62. Ibid., pp. 146-47.
63, Ibid., p. 79.
64. Ibid., p. 149.
