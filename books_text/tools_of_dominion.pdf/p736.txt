728 TOOLS OF DOMINION

3. Price Inflation Premium

The inflation premium becomes an increasingly important factor
in the market rate of interest in a society which permits or encour-
ages monetary debasement, including fractional reserve banking.
Loans will contain an inflation premium component — interest rates
higher than the mere originary rate, or “present goods vs. future
goods” component. The lender of money will lose if money of less
purchasing power is returned to him. Inflation raises long-term in-
terest rates.3%

One way around price inflation is to make loans in kind. The
lender loans gold coins, for example, and demands repayment of
both principal and interest in gold coins. Or perhaps the loan is
made in a comparatively stable foreign currency. The loan’s price in-
flation premium then disappears.

Summary

The reason why interest rates never fall to zero is that a lender
does not need to transfer an asset to anyone else merely to have that
same asset returned to him in the future. He can hold onto the asset
and achieve the same economic return in the future. Meanwhile, he
has the asset ready for immediate use, should a profitable opportun-
ity arise. Therefore, should someone voluntarily lend any asset at a
zero rate of interest, it is because the person is making a charitable
loan, or else he is buying safer storage for the asset. In the latter case,
he is then paying an implicit fee for storage: the interest that he is for-
feiting that the borrower will receive by re-lending the asset, or the
immediate access to the asset that he is forfeiting. A negative interest
rate, should it ever appear on a voluntary market, is clearly evidence
of a storage fee.

People do not voluntarily give up something for nothing unless
they are confused about the details of the transaction.** Thus, all
talk about a zero rate of interest in a time-bound, risk-bound, free
market world is nonsense,*> In an attempt to achieve such a world,
the civil government would have to prohibit all profit-seeking lend-

33. Monetary inflation can temporarily lower short-term interest rates: Mises,
Human Action, ch. 20,

34, In the case of making a zero-interest charitable loan, the lender is honoring
God, He is thereby building up treasures in heaven (Matt. 6:20), to be received in
the future (I Gor. 3:12-14).

35. Rothbard, Man, Economy, and State, p, 326.
