350 TOOLS GF DOMINION

gentlemen and commoners, The duel was considered a cut above the
street brawl, even though the duel’s use of handguns could easily
result in the death of onc or both rivals. It also offered the possibility
of placing restrictions around the conflicting individuals, so that the
potentially lethal conflict did not result in a lengthy feud between ex-
tended families. Wyatt-Brown writes:

 

Feuds were generally much deplored, particularly among the gentry, be-
cause quite obviously they disrupted community life grievously, and incited
conflicts of loyalty among related family members and their friends. Duels,
in contrast, provided structure and ritual, Referees assured the fairness of
the fight and witnesses reported back to the public on the impartiality of the
proceedings. Moreover, the rites of challenge and response afforded time
and means for adjustment of differences through third parties... . In
addition, the duel set the boundaries of the upper circle of honor. They ex-
cluded the allegedly unworthy and therefore made ordinary brawling ap-
pear ungentlemanly, vulgar, and immoral. In a hierarchical society, all
these factors were socially significant. They made violence a part of the so-
cial order even in the upper ranks, but at least duels helped to restrict the
bloodletting, which otherwise would have been much more chaotic and
endlessly vindictive.

It would be a mistake, however, to argue that duels were as much deplored
as Southern hand-wringing would lead an observer to believe. Hardly more
than a handful genuinely considered duels socially beneficial, although
some apologists claimed that the prospect of dueling forced gentlemen to be
careful of their language and cautious in their actions. The criticism of out-
siders, the clear opposition of the church, the recognition that valuable
members of the community sometimes fell for reasons that retrospectively
seemed petty — these attitudes placed duelists on the defensive. As a result,
most of them explained their general opposition to the code duello in almost
ritual words, but in the next breath gave reason for its continuation.*

He offers evidence that the duel became a familiar aspect of the
coming to maturity of young gentlemen in the South. Teenage duels
were not uncommon in New Orleans and South Carolina.* At the
center of the duel was the pride of man:

Tn 1855 Alfred Huger of Gharleston, for instance, rejoiced in the news that
the son of a friend had killed his rival in a contest. The boy had showed

34, Ibid., pp. 352-53.
35, Ibid., p. 167. The rituals associated with dueling were well known only in
Charleston and New Orleans: ibid., p. 355.
