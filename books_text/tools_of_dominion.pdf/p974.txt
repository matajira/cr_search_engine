966 TOOLS OF DOMINION

What must be stressed is the element of continuous develop-
ment. “The kingdom of heaven is like to a grain of mustard seed,
which a man took and sowed in his field: Which indeed is the least of
all seeds: but when it is grown, it is the greatest among herbs, and
becometh a tree, so that the birds of the air come and lodge in the
branches thereof” (Matt. 13:31-32). As this kingdom comes into
maturity, there is no physical separation between saved and lost.
‘That total separation will come only at the end of time. There can be
major changes, even as the seasons speed up or retard growth, but
we should not expect a radical separation.

While I do not have the space to demonstrate the point, this means
that the separation spoken of by premillennialists— the Rapture—is
not in accord with the parables of the kingdom. The Rapture comes
at the end of time. The “wheat” cannot be removed from the field un-
til that final day, when we are caught up to meet Christ in the clouds
(I Thess. 4:17). There is indeed a Rapture, but it comes at the end of
time—when the reapers (angels) harvest the wheat and the tares.
There is a Rapture, but it is a postmillennial Rapture.

Why a postmillennial Rapture, the amillennialist may say? Why
not simply point out that the Rapture comes at the end of time and
let matters drop? The answer is important: We must deal with the
question of the development of the wheat and tares. We must see that
this process of time leads to Christian victory on earth and in time.

Knowledge and Dominion

Isaiah 32 is a neglected portion of Scripture in our day. It informs
us of a remarkable day that is coming, It is a day of “epistemological
self-consciousness,” to use Cornelius Van Til’s phrase. It isa day
when men will know God's standards and apply them accurately to
the historical situation. It is not a day beyond the final judgment, for
it speaks of churis as well as liberal people. Yet it cannot be a day in-
augurated by a radical separation between saved and lost (the Rap-
ture), for such a separation comes only at the end of time. This day
will come before Christ returns physically to earth in judgment. We
read in the first eight verses:

Behold, a king shall reign in righteousness, and princes shall rule in
judgment. And a man shall be as an hiding place from the wind, and a
covert from the tempest; as rivers of water in a dry place, as the shadow ofa
great rock in a weary land. And the eyes of them that see shall not be dim,
and the ears of them that hear shall hearken. The heart also of the rash shall
