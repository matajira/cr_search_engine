A Biblical Theology of Slavery 197

week.56 Tt was illegal to dismiss a slave-apprentice if he was over age
49 or if he was mentally or physically infirm. If discharged from ser-
vice prematurely for any of these reasons, his master then had to
provide him with lifetime subsistence.°’ One class of apprentices,
non-praedial apprentices, were under a less rigorous provision: full
freedom in 1838.28 As it turned out, all apprentices were released on
August 1, 1838, and Jamaicans date the nation’s liberation on this
date. (I was in Jamaica on the 150th anniversary of liberation day. I
had expected considerable public festivities. As it turned out, the
government played down the event, substituting instead a national
holiday celebrating liberation from British political rule in 1962.)

Government Debt and Servitude

The relationship between long-term government debt and servi-
tude must be considered at this point. Unlike a voluntary private
debt, a government debt does use the possibility of coercion and ser-
vitude—jail sentences for tax protesters—to collateralize its debt
obligations. This places a government debt in a biblical judicial cate-
gory different from a private debt. The government’s debt is much
closer to the emergency charitable debt of Deuteronomy 15: a mor-
ally (though not legally) mandatory extension of credit to a destitute
neighbor who collateralizes his obligation by an agreement to go into
debt servitude in case he defaults.

There are no legal or physical assets that collateralize any gov-
ernment debt. There is no way for creditors to repossess this collateral
if the government should default. Taxpayers are then threatened by
rising taxes. The State escapes the traditional political restraints of
tax protests by means of perpetual debt. If the voters rebel against
rising taxes, there is then a great temptation for a national govern-
ment to sell its debt obligations to its national central bank, which
uses these notes to collateralize an expansion of fiat money.?5° This
leads to price inflation and the creation of the boom-bust business

236. Section V, The Abolition Act of 1833 (reprint: Virgin Islands Public Library,
1984), [p. 16, Paragraph 3]. Other provisions included the slave's right of purchase
out of bondage (Sect. VHT), a ban on the sale of slaves from off the island (Sect. IX),
and a ban on separating former slave families (Sect. X).

237. Ibid., Sect. VII.

238, Ibid.; Sect. VI.

239, Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Worth, Texas; Dominion Press, 1986), ch. 8.
