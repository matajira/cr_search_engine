Meyer, John, 143n, 334n
Michal, 257
Middle Ages, 463-64
Middle men, 696
Milgrom, Jacob, 625
Millennium, 55-56, 58
Milliman, Jerome, 574
Minneapolis, 526n
Minnesota, 404
Mirror, 1
Miscarriages, 857-58
Mises, Ludwig von
constants, 1085n
equilibrium, 1124
imaginary constructs, 1126
omniseience, 1125
time-preference, 723-27
utilitarian, 1123
Mishan, E, J., 588, 8700
Mishnah, 840, 1008-9, 1011
Mishneh Torah, 1041-42
Missionaries, 268n
Missions, 144
Mites, 688, 802-3
Mitford, Jessica, 119n, 409
Moab, 148, 229
Mobility, 898-99
Model penal code, 1167
Modernism, 1036
Mogen David, 1002
Molech, 1019
Money, 719, 752-53, 775
Monogamy, 268
Monopoly
civil government, 790
courts, 787, 801-2
criminals, 335
medical, 8670
misused, 786-89
public utility, 696-97
Montes pietatis, 745
Moody, D. L., 167
Mooney, 8. G.
criticisms of North, 716-17
debt as slavery, 8187,
decapitalization, 1186-88

Index

Leviticus 25, 1183
rent, 1179, 1181-83
scholasticism of, 720n
zero-interest loans, 709n, 714n
Morey, Robert, 1700
Morgan, Edmund, 254n
Morgenstern, Julian, 1034n, 1082
Mormons, 865-67, 901
Mortgages, 718
Moses
absence of, 880
age, 977
God's instructions to, 879
God's words, 93
insufferable, 64
ministry of, 89
representative, 93
Mountain’s hole, 1162-63
Moving van, 613n
Moynihan, Daniel, 657n
Mi. Zion, 673
Muffler, 577
Muggeridge, Malcolm, 191, 202, 900
Multiple indebtedness, 738-40
Murder

automatic death penalty } (
brawl, 345

Britain, 1169
capital crime, 281
duel, 346
pleonasm, 305-6
rabbinic Judaism, 1000, 1048-49
sentence, 1970's, 465
ULS., 1170
Uriah’s, 522
Murray, Gilbert, 410
Murray, John, 50n, 187, 282n, 313,
314n, 98in, 1000
Mush (Higher criticism), 1076
Muster (see Numbering)
Mutilation
Adoni-Bezek, 416
auction, 426-30
bondservant, 489-40
figurative, 418
freedom &, 451-52
loss of productivity, 394
