Bribery and Judgment 80!

this is called market competition, not bribery.

This illustrates a very important economic principle: different sys-
tems of financing govern different sovereign spheres of society. The principle of
“highest money bid wins” governs the competitive free market. If this
principle were not honored, then the auctions (competitive open mar-
kets) of the world could not function, as we shall examine in detail
below. Men always have expectations of how resources are to be dis-
tributed in any social order. If the principle of private ownership is
maintained by the civil authorities, then people know that they have
the right to exclude others from access to their property. The civil govern-
ment is expected to uphold ownership boundaries.°° Only by offering
higher and higher bids can other people hope to gain access to my
assets and the key legal right (immunity) associated with ownership,
namely, the right to exclude. The principle of Aighest bid wins is inher-
ent in any society that upholds the private property system. The
rules of economic order are known in advance, and people can make
economic plans for the future in terms of these judicial assumptions.

The difference between the operation of the free market and the
operation of the court system is that God has granted a legal monop-
oly of enforcement to church government and civil government.
Courts must serve as the final voice of civil authority.*° They are to
be neither open nor competitive, This means that they are not to

38, An exception: a salesman who pays a bribe to a purchasing agent who is in a
position to place a large order using his firm’s money. They in effect “split the
commission.” This is a violation of company policy on the part of the purchasing
agent, who is misusing company funds in order to get a personal reward. It is a
criminal offense: theft. This must be recognized for what it is: a violation of com-
pany policy. It is not inherently a “capitalist act.” It is a thief’s act.

39. This is point three of the biblical covenant model: Gary Narth, The Sinai Strat-
egy: Economics und the Ten Commandments (Tyler, Texas: Institute for Christian Eco-
nomics, 1986), ch, 8

40. If a national (or international) supreme court possesses, as a side-effect of
rendering judgments in court cases, the constitutional authority to declare an act of
the legislature illegal or unconstitutional —a power possessed by the U.S. Supreme
Court (though not by many other nations’ courts) —~ this power should be tempered
by the right of the legislature and the executive to combine (if they are divided) in a
decision to overturn the supreme court's decision, if the vote of the legislature is
large enough (say, three-quarters of bath houses of the legislature). Without this
right of appeal beyond the supreme judicial court, a single agency of civil govern-
ment gains the exclusive voice of authority, a power trustworthy only in the hands of
God. On the accelerating power of the U.S. judiciary, see Carrol D. Kilgore, judicial
Tyranny (Nashville, ‘Tennessee: Nelson, 1977).

41, The legitimacy of a system of exclusively private, competitive, profit-seeking,
free market civil courts is promoted by Bruno Leoni, Preedom and the Law (Princeton,
New Jersey: Van Nostrand, 1961); cf. Murray N. Rothbard, “On Freedom and the
