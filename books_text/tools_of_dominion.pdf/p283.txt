Wives and Concubines 275

new economic and moral responsibilities on the shoulders of single
adult women, With greater authority inevitably comes greater re-
sponsibility. They can set the terms of their own marriages. What we
have seen is that they have proven to be tragically incompetent bar-
gainers. No one represents them any more. With the rise of no-fault
divorce, not even the civil government protects their interests any
longer. In the United States, one year after a divorce, the woman’s
standard of living has fallen by over 70 percent, while her former
husband’s has risen by over 40 percent.*?

To ignore these economic realities in the name of formal biblical
Jaw would be foolish. The dowry is not legally required in order to
avoid concubinage, since concubinage is no longer a biblical officc,
but this does not solve the economic problem of the economic vul-
nerability of wives, especially in an increasingly humanistic civiliza~
tion in which divorce is regarded as some sort of opportunity to
escape responsibility — an economic subsidy to lawless, irresponsible
maies if there ever was one, When husbands walk out of a marriage,
leaving the care of children to the wives, as well as the wives’ support
of themselves, the division of labor is restricted. Wives must become
self-supporting, even when husbands pay child support, and in mil-
lions of cases, they refuse, With this contraction of the division of
labor, wives’ personal productivity necessarily falls, and thercfore
their net income falls. The husbands find younger wives to marry,
but divorced wives over age 35 with children seldom find husbands.
The majority of divorced husbands win; the majority of divorced
wives lose. Thus, wives without dowries are still unprotected eco-
nomically, just as they were in the Old Testament. The difference is,
concubines had biblical laws to protect them in the Old Testament.
So did their aged parents. Today, these economic problems must be
dealt with early by voluntary contract rather than by civil law. They
seldom are, except in second marriages” or in cohabitation.™ In the

32. Lenore J. Weitzman, The Divorce Revolution: The Unexpected Social and Economic
Consequences for Women and Children in America (New York: The Free Press, 1985), p.
xii; cited by George Grant, The Dispossessed: Homelessness in America (Ft. Worth,
Texas: Dominion Press, 1986), p. 79. I referred lo these statistics in the previous
chapter. Remember, this is a commentary; many readers will read only one chapter
or section at a time,

33. Georgia Dullea, “Prenuptial Agreements the 2nd Time Around,” New! York
Times (Jone 7, 1982).

34, So widespread in the United States is cohabitation without marriage that law-
yers now draw up cohabitation agreements that deal with such issues as individual
and joint bank accounts, real estate contracts, equal obligations (expenses), and em-
