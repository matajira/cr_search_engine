10 TOOLS OF DOMINION

mands of the Law are presumed to be no longer binding except
where the New Testament repeats or ratifies them.”!© We would
agree with Bowman when he concludes that “dispensationalism,
technically speaking, is antinomian, though more in theory than
practice; .. .”27

This is precisely the Reconstructionists’ point: most of our op-
ponents are antinomian in theory, though not necessarily in practice
{i.e., in the specific details of personal ethics).’* It is not the details of
the personal ethics of our critics that concern Reconstructionists
theologically; rather, it is our opponents’ governing principle of in-
terpretation regarding Old Testament law in New Testament times.
Our primary theological distinctives as a movement are judicial and
cultural. We do not ignore the question of personal ethics, but per-
sonal ethical issues must inevitably be dealt with intellectually on the basis of
sume general principle of biblical interpretation. Our principle of biblical
interpretation is explicit (theonomy); that of our opponents is gen-
erally implicit (antinomianism). Our hermeneutical explicitness is
now forcing our critics to respond explicitly, and this pressure
bothers them. They resent it. They must give up either their anti-
nomianism or their claims to cultural relevance as Christians. They
do not want to give up either position, but they no longer have any
intellectual choice. They do not like to admit this, however. It dis-
turbs them. But if they had an answer, someone in the evangelical
world would provide at least an outline of a comprehensive Chris-
tian social theory based neither on biblical case laws nor natural law
theory. We are still waiting. It has been 1,900 years.

Their silence in this time of cscalating international criscs, in
every area of life, in the decades immediately preceding the third
millennium after Christ, is an important reason for the growing in-
fluence of Christian Reconstructionism. Their silence is costing
them heavily, but so will any attempt to respond to us without offer-
ing a biblically plausible alternative worldview. You cannot beat
something with nothing.

16. fbid., p. 25.

17. lbid., p. 26

18. Given the sexual scandals of television evangelists Jim Bakker in 1987 and
Jimmy Swaggart in 1988, we Reconstructionists are sorely tempted to conclude that
dispensationalisin umds toward antinomianism in practice, too.
