A Biblical Theology of Slavery 155

what he is.”®® He is also correct in his observation: “The majority of
people on the earth are presently enslaved to Babelic statist powers,
owned in body and usually in soul as well by political masters.
Moreover, even in the ostensibly free West, increasingly large num-
bers of persons forsake the dominion mandate and place themselves
on the dole, crying out to the messianic state to become their sover-
eign provider. Scripture speaks to these matters, in the language of
slavery,”®9

If men refuse to place themselves under God and God’s required
law-order for society, then they will inevitably place themselves in
bondage to someone other than God, under laws that are different
from God’s. But only God is omniscient and omnipotent; only He
can control men from the inside-out as well as from the outside-in.°%
To deny God’s control over both body and soul is to surrender con-
tro] to some other aspect of the fallen creation —a tyrant that will at-
tempt to control both body and soul. Man cannot achieve freedom
by rebelling against God and His law.

We must begin our journey on the road from serfdom by placing
ourselves under covenantal bondage to the God of liberation.®1 We
must seek to become passive toward God and active over His crea-
tion (Gen. 1:26-28). The only alternative to this unqualified ethical
subordination to God is to become passive toward something or
someone else—other men (tyranny), demonic spirits (occultism,
mysticism), some aspect of nature (environmental determinism), the
“cunning of history” (Hegel), “inevitable social forces” (Marx), the
“unconscious” (Freud), alcohol or drugs, or even outright madness.°?
Second, becoming passive to something other than God, mankind
then becomes either an active destroyer of both his environment and
his own freedom (power religion) or else an essentially passive
bystander who is subordinate to impersonal forces of nature or to
some pantheist god (escape religion).

Slavery and Empire

Slavery in the ancient Near East was common, and the law codes
of various Near Eastern societies provided rules that regulated the

88. Zhid., p. 9.

89, Ibid., pp. 4-5.

90. Hbid., p. 20.

91. North, Liberating Planet Earth, op. cit.

92. Paul F. Stern, Zn Praise of Madness (New York: Dell, 1973).
