176 TOOLS OF DOMINION

This was one of the most astounding cases of self-professed collective
amnesia in human history.

The best estimate of the total number of slaves transported from
Africa to the New World is 10 million people. /5° Fewer than 400,000
of these slaves were brought into British North America.5! Com-
pare this with over 1.6 million imported by the British Caribbean
islands. '*? The slave system of the American South developed only
in the late seventeenth or early eighteenth century. While the docu-
mentary evidence is somewhat elusive and incomplete,'* it appears
that prior to the 1640’s, blacks and whites entered the region as tem-
porary indentured servants. In 1640, however, the General Court of
Virginia pronounced sentence on three escaped servants: a Dutch-
man, a Scot, and a black. The first two were ordered to serve their
masters for an additional year; the black was returned to his master
for life. Winthrop Jordan comments: “No white servant in any Eng-
lish colony, so far as is known, ever received a life sentence.”!* After
1640, surviving Virginia county court records began to mention
black slavery for life.155 Also, after 1640, records of prices paid for
servants indicate a higher price for blacks, indicating a longer term
of service. These higher prices may also indicate that blacks could be
forced to do field work that whites refused to accept.5 Beginning
also in 1640 was a prohibition against blacks’ bearing arms.'57 This
was imitated in the North by Puritan Massachusctts in 1656, who ex-
cluded blacks from serving in the militia, and by Puritan Connecti-
cut in 1660. 158

Very few blacks were imported into the American colonies in the
seventeenth century. In 1649, three decades after the arrival in
Virginia of the first Africans, there were only 300 black laborers in

150. Philip D. Gurtin, The Adlantic Slave Trade: A Census (Madison: University of
Wisconsin Press, 1969), pp. 268-69.

151, Ibid., p. 268.

152. Idem.

153. Historians who believe that the evidence is too incomplete to make any sure
judgments regarding the terms of white and black indentures inchide Winthrop D.
Jordan, “Modern Tensions and the Origins of American Slavery,” fournal of Southern
‘History, XXVIII (1962), p. 22; cf. Jordan, White Over Black, p. 75. See also Paul C.
Palmer, “Servant Into Slave: The Evolution of the Legal Status of the Negro
Laborer in Colonial Virginia,” South Adantic Quarterly, LXV (1966), p. 369.

154, Jordan, White Over Black, p. 75.

155. Idem.
156. Zbid., pp. 76-77.
157. Ibid., p. 78.

158, Ibid., p. 71.
