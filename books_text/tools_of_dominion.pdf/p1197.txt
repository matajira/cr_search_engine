Lots of Free ‘Lime: The Existentialist Utopia of S.C. Mooney 1189

them were looking to become part of the “wave of the future.” Only a
few of them survived the test of time, to become remembered as the
founders of yet another failed social religion.

Anyone can hang out a sign which announces that he is a Chris-
tian Reconstructionist. There is no licensing required. Not very
many people choose to do this, since to join the tiny band of theono-
mists today is to become a modern-day John the Baptist, typing in
the wilderness. But what should make a reader more than a litle
suspicious of anyone who claims to be a theonomist is the promoter’s
narrow range of concern. Specialization is legitimate, but anyone
who claims that he is offering a revolutionary blueprint for this or
that aspect of society had better also offer at least a first draft of the
overall integrated plan. The old rule of ecology is true: yeu cannot
change just one thing, You cannot reconstruct just one aspect of society,
or just one aspect of an economy, For example, if you suggest a zon-
ing code that makes sewers illegal, you had better strongly recom-
mend the installation of septic tanks; otherwise, you can expect con-
siderable overflow problems. I perceive that Mr. Mooney is drown-

 

ing in overflow.

Again, I do not expect any society to adopt Mr. Muoney’s bap-
tized Aristotelianism. If it does, it will not remain productive very
long. What does concern me is that a lot of well-meaning Christians
will take such nonsense seriously, assume that it is “truly biblical”
economics, and then try to “spread the gospel” of crackpottery in the
name of Jesus. This would be an embarrassment to the kingdom of
God generally and Christian Reconstruction speciftcally. We Chris-
tians are already regarded as otherworldly dreamers. Let us not pro-
vide additional ammunition to our enemies.

But if you're not convinced by the logic of my presentation, I’d
like to borrow a few ounces of gold from you, interest-free, for ten
years. Drop me a letter.
