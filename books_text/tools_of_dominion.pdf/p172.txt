164 TOOLS OF DOMINION

What about the peacetime enslavement of a nation’s citizens?
There was household servitude, but on the whole it was the result of
poverty or hard circumstances that befell individual families. Debt
bondage would have been the most common reason for becoming an
indentured servant. Another reason was the need for a criminal to
raise the funds necessary to repay his victims.

If private, household servitude was relatively uncommon in the
agriculture-based ancient Near East, why did God begin His case
laws with laws governing household servitude? If the servitude laws
were intended to govern only a minor area of Israel’s social and eco-
nomic life, why did God immediately focus the attention of His re-
cently redeemed people on servitude? It was because God’s concern
is always theological, not simply social or economic. Their recent ex-
periences in Egypt had been designed by God to teach them con-
cerning the God-imposed relationship between covenant-keeping
and liberty, and between covenant-breaking and permanent slavery.
God’s instructional purpose, as always, was covenanital.

Embarrassed by God

One thing every Christian reader should accept without question
is this: nothing in the Bible should be an embarrassment to any Christian, We
may not know for certain precisely how some biblical truth or his-
toric event should be properly applied in our day, but every historic
record, law, announcement, prophecy, judgment, and warning in
the Bible is the very word of God, and is not to be flinched at by any-
one who calls himself by Christ’s name. We must never doubt that
whatever God did in the Old Testament era, the Second Person of
the Trinity also did. God’s counsel and judgments are not divided.
We must be careful not to regard Jesus Christ as a sort of “unindicted
co-conspirator” when we read the Old Testament. “Whosoever
therefore shall be ashamed of me and of my words in this adulterous
and sinful generation; of him also shall the Son of man be ashamed,
when he cometh in the glory of his Father with the holy angels”
(Mark 8:38).

What we need to understand early in any serious discussion of
the Old Testament is this: most Christians today are embarrassed by God.
They are embarrassed by the very word of God. They are embar-
rassed by the ways that God dealt with people in the Old Testament.
It makes them uncomfortable when they read the holy word of God
when it says: “The righteous shall rejoice when he seeth the ven-
