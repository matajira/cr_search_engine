1028 TOOLS OF DOMINION

Would he say that teaching that Jesus Christ and His followers
will be boiled in hot semen and hot excrement for eternity consti-
tutes a reverent attitude? Are Christians supposed to believe that
this is a reverent “attitude toward Christ and the Church”?

He goes on: “For many centuries the Talmud was regarded as
mysterious and a source of blasphemous statements against Christi-
anity. This suspicion was not only grossly untrue but it was magnified
and distorted by ignorance of the Talmud, The inability of Christian
scholars to read the 7a/mud made matters worse.” An uncensored (as
far as we gentiles know) version of the Talmud is now in English, Those
few of us who bother to consult it still do not find that these ancient
suspicions have been calmed. They have in fact been confirmed.

1 do not think that Michael Rodkinson was being any morc hon-
est that Rabbi Trattner when he wrote these words in the Preface to
his expurgated version of the Talmud: “The Talmud is free from the
narrowness and bigotry with which it is usually charged, and if
phrases used out of their context, and in a sense the very reverse
from that which their author intended, are quoted against it, we may
be sure that those phrascs never existed in the original Talmud, but
are the later additions of its enemies and such as never studied it.7104
Then came the Soncino edition.

It is my belief that mandatory training in the oral law served
covenant-breaking Judaism for at least two millennia as a means of
initiating its religious leaders into what was basically a secret society.
By requiring its brightest adolescent males to go through long hours
of memorization and discussion of such material, year after year, if
they wanted to become rabbis, Judaism for almost two millennia
sidetracked its best and brightest young men into some very peculiar
ethical avenues — peculiar at least to the outlook of Christians.

It is also my contention that (he unprecedented economic, intel-
lectual, and cultural strides made by Jews in the West could begin,
and did begin, only when their young men at last were allowed to
become rabbis and leaders within the cormmunily without being re-
quired to go though this initiatory process. But a price has been ex-
tracted by Western society for this advancement. The price has been
the steady secularization of the vast majority of Jews, just as Orthodox
rabbis have warned their upwardly mobile brethren from the early
decades of the nincteenth century until today. Most Western Jews

 

   

  

100. Idem.
101, Michael L. Rodkinson, Editor's Preface, New Hatiion of the Babylonian Talmud
(Boston: New Talmud Pub. Co., 1903), T, p. xi.
