1212 TOOLS OF DOMINION

only procedural, or that family government is only procedural. In
short, he is saying what Van Til denied: that form can be segregated
from content, ethically speaking. Rushdoony wrote in the Jnsitiutes
that “The basic premise of the modern doctrine of toleration is that
all religious and moral positions are equally true and equally false.”’*
This is exactly the worldview which the Framers wrote into the
Constitution when they abolished state religious tests for holding
Federal office.

I cannot avoid the obvious conclusion: if a defense of the U.S.
Constitution as being somehow inherently Christian, or in some way
fundamentally conformable to Christianity, is the position of the
Christian Reconstruction movement, this means the suicide of
Christian Reconstructionism. Rushdoony said it best: “The modern
concept of total toleration is not a valid legal principle but an ad-
vocacy of anarchism. Shall all religions be tolerated? But, as we have
seen, every religion is a concept of law-order. Total toleration means
total permissiveness for every kind of practice: idolatry, adultery,
cannibalism, human sacrifice, perversion, and all things else. Such
total toleration is neither possible nor desirable. . . . And for a law-
order to forsake its self-protection is both wicked and suicidal.””

The Question of Sovereignty

His rewriting of U.S. history has gone on from the beginning, In
the Jnstitutes, he says that “The presidential oath of office, and every
other oath of office in the United States, was in earlier years recog-
nized precisely as coming under the third commandment, and, in
fact, invoking it. By taking the oath, a man promised to abide by his
word and his obligations even as God is faithful to His word. If he
failed, by his oath of office, the public official invoked divine judge-
ment and the curse of the law upon himself.”® This is Presidential
mytho-history.

Rushdoony’s view of U.S, political history is heavily influenced
by a bizarre idea that he picked up in a speech by President John
Quincey Adams,*! who shared his President father’s Unitarian the-
ology. So far as I know, no one else has maintained the following in-
terpretation: the U.S. Constitution rests on no concept of God because

78. Rushdoony, Institutes, p. 295.

79. [bid., p. 89.

80. Tbid., p. Ii.

81. Cited in Rushdoony, This Independent Republic, p. 38.
