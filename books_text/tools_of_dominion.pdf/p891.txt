God's Limits on Sacrifice 883

The link between the two realms, natural and supernatural, is
the covenant. Christ told the disciples to pray: “Thy kingdom come.
Thy will be done in earth, as it is in heaven” (Matt. 6:10}. The will of
God, as revealed in His covenant law, is the standard of what should
take place both above and below. Christ also told Peter, after Peter's
confession of Jesus as the Son of God: “And I will give unto thee the
keys of the kingdom of heaven: and whatsoever thou shalt bind on
earth shall be bound in heaven: and whatsoever thou shalt loose on
earth shall be loosed in heaven” (Matt. 16:19). It is the law of God
which binds and looses; the keys of the kingdom are biblical law. 9
Men in their capacity as ordained officers, as God’s representative
covenantal agents, declare His law and enforce it. The will of God,
not the will of man, is absolute. This is why the Creator-creature dis-
tinction must be at the foundation of all Christian philosophy, for
without it, the chain-of-being theology of autonomous man under-
mines the revelation of God to man and the law of God for man. As
Van Til says:

The Christian position maintains that man, as a creature of God, naturally
would have to inquire of God what is right and wrong. Originally God
spoke to man directly and man could speak to God directly. Since the en-
trance of sin man has to speak to God mediately, He has now to learn from
Scriptures what is the acceptable will of God for him. In opposition to this
the non-Christian position holds that man does not need Scripture as a final
authority. And this is maintained because the non-Christian does not believe
that man ever needed to be absolutely obedient to God. Non-Christian eth-
ics maintains that it is of the nature of the ethical life that man must, in the
last analysis, decide for himself what is right and what is wrong.”

Broken Tablets, Broken Covenant

Moses’ dramatic response to the Hebrews’ public demonstration
of magical power religion—his response of symbolic ritual -- was to
break the stone tablets that had been delivered to him by God. These
inscribed tablets were not the product of man’s hand. God, not
Moses, had written His ten laws on the tablets (Ex. 31:18). These
laws set forth the basis of God’s cooperation with man, a set of ethical
principles rather than prescribed rituals. The ethical bond was based on a

19. R. J. Rushdoony, Institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), p. 619.

20. Cornelius Van Til, Christian Theistic Ethics, vol. Il of In Defense of Biblical
Christianity (Phillipsburg, New Jersey: Presbyrerian & Reformed, 1980), p. 33.
