464 TOOLS OF DOMINION

attempted to explain this practice rationally.!? Yet the specialized
historians have generally remained silent, and few professional his-
torians have ever heard of such goings-on, nor are they aware that in
ancient Athens, the courts tried inanimate objects, such as statues
that had fallen and killed someone. If convicted, the object was
banished from the city.!3 Why the silence? Why don’t these stories
get into the textbooks? As Humphrey asks: “Why were we never
told? Why were we taught so many dreary facts of history at school,
and not taught these?” +

He answers his own question: modern historians can make little
sense out of these facts. There seems to be no logical explanation for
the way our ancestors treated guilty animals. What is a guilty ani-
mal, anyway—a legally convicted guilty animal? How can such
events be explained? Finkelstein cites the theory of legal scholar
Hans Kelsen that such a practice points to the “animism” of early
medieval Europe, since to try an animal in court obviously points to
a thcory of the animal's possession of a soul." Kelsen says that this
reflects early Europe’s older primitivism. Finkelstein then attacks
Kelsen’s naive approach to an understanding of this practice. In con-
trast to primitive societies, it is only in the West that such legal sanc-
tions against offending animals have been enforced. “Only in Western
society, or in societies based on the hierarchic classification of the
phenomena of the universe that is biblical in its origins, do we see
the curious practice of trying and executing animals as if they were
human criminals.”'® Then he makes a profound observation: “What
Kelsen has misunderstood here—and in this he is typical of most
Western commentators—is the sense, widespread in primitive soci-
ctics (as, indeed in civilized societies of non-Western derivation),
that the extra-human universe is autonomous and that this autonomy
or integrity is a quality inherent in cvery species of thing.”"? Because
Western society long denied such autonomy to the creation, it has in
the past adhered to the biblical requirement of destroying killer ani-
mals; in Europe, they were even given a formal trial.

 

12. Nicholas Humphrey, Foreword, ibid., p. xviii,

13. W. W. Hyde, “The prosecution of animals and lifeless things in the middle
ages and modern times,” University of Pennsylsania Law Review (1916). Finkelstein is
somewhat suspicious of these accounts,

14. Humphrey, Foreword, p. xv.

15, Finkelstein, Ox That Gored, p. 48. He cites Kelsen, General Theory of Law and
State (1961), pp. 3-4.

16. Finkelstein, op. cit., p. 48.

17, Thid., p. 51.

 
