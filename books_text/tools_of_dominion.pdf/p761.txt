The Prohibition Against Usury 753

out history paid to gain access to its use for a period? How are so
many people fooled into paying for the use of a sterile asset? Besides,
interest is a phenomenon of every Joan, not just loans of money.
Modern economics teaches this; so does the Bible.

It is obvious that the phenomenon of interest is not confined to
money. Aristotle was incorrect. The phenomenon of interest applies to
every scarce economic resource. We always discount future value. What-
ever we own in the present is worth more to us than the promise of
owning that same item in the future. Promises to repay can be
broken (the risk factor), but more to the point, the present commands a
price premium over the future.®®

We live in the present, We make all of our decisions in the pres-
ent. We enjoy the use of our assets in the present. While wise people
plan for the future by purchasing streams of future income by buy-
ing assets that they expect to produce net income over time, they
purchase these hoped-for streams of income at a discount. The rate of
discount that we apply to any stream of expected future income is
called the rate of interest, Mises called it time-preference,

Thus, the rate of interest is not exclusively a monetary phenome-
non. Inierest is a universal discount that we apply to every economic service that
awe expect to receive in the future, We buy a hoped-for stream of rents; we
can buy them for cash; but we expect a discount for cash. This pur-
chase at a discount for cash is called capitalization. It is the heart of
capitalism. It is the heart of every socicty more advanced than the
utterly primitive.

The person who lends money at zero interest is clearly forfeiting
a potential stream of income. He will seldom do this voluntarily, ex-
cept for charitable reasons. The ownership of the asset offers him an

 

 

expected stream of income: psychological, physical, or monetary. If
it did not offer such a stream of income, it would be a free good. It
would not be demanded. It would therefore not command a price.
The owner expects to receive a stream of income. He chooses the
degree of risk that he is willing to accept, and he then refuses to lend
the asset for less than the interest rate appropriate to this degree
of risk,

The borrower compensates the owner for the use of the asset, or
its exchange value, for a specified period of time. He borrows it only
because he values its stream of services more highly than he values

80, Mises, Human Action, ch. 19.
