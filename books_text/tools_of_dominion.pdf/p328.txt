320 TOOLS OF DOMINION

But if he thrust him of hatred, or hurl at him by laying of wait, that
he die; Or in enmity smite him with his hand, that he die: he that
smote him shall surely be put to death; for he is a murderer: the
revenger of blood shall slay the murderer, when he meeteth him (Num,
35:20-21).

Moreover ye shall take no satisfaction for the life of a murderer,
which is guilty of death: but he shall be surely put to death (Num.
35:31).

No instances of the pleonasm appear in the Book of Deuteron-
omy. I do not think that this has any biblical-theological significance.
The biblical hermeneutical principle of the continuity of a God-
revealed law is that unless a law or its sanction is repealed by a subse-
quent biblical revelation, it is still judicially binding. The pleonasms
did not have to be repeated in Deuteronomy in order for them to be
binding in the land. God’s laws in Exodus, Leviticus, and Numbers
were not exclusively “wilderness laws,” with the laws of Deuteron-
omy alone to serve as the law of Israel in the land. In any case, the
severity of God’s sanctions tends to increase over time as men’s ma-
turity increases. This is a basic principle of biblical jurisprudence:
men’s knowledge of God increases over time, and so does their personal and cor-
porate responsibility, “The lord of that servant will come in a day when
he looketh not for him, and at an hour when he is not aware, and
will cut him in sunder, and will appoint him his portion with the un-
believers. And that servant, which knew his lord’s will, and prepared
not himself, neither did according to his will, shall be beaten with
many stripes. But he that knew not, and did commit things worthy
of stripes, shall be beaten with few stripes. For unto whomsoever
much is given, of him shall be much required: and to whom men
have committed much, of him they will ask the more” (Luke
12:46-48). Because they were required by God to exercise greater re-
sponsibility in the Promised Land, as testified to by the ending of the
miraculous agricultural subsidy of the manna (Josh. 5:12), the law’s
civil sanctions did not decrease in rigor; if anything, they increased.
The pleonasm was still judicially binding in Canaan. The equivalent
phrase in Deuteronomy is, “so shalt thou put [purge] evil away from
among you” (Deut. 17:7; 19:19; 21:21; 22:21; 24; 24:7).
