Common Grace, Eschatology, and Biblical Law 957

The most frequently cited passage used by those who defend the
idea of God’s favor to the unregencrate is Matthew 5:44-45;

But I say unto you, Love your enemies, bless them that curse you, do
good to them that hate you, and pray for them which despitefully use you,
and persecute you; That ye may be the children of your Father which is in
heaven: for he maketh his sun to rise on the evil and on the good, and
sendeth rain on the just and on the unjust,

It is understandable how such verses, in the absence of other
verses that more fully explain the nature and intent of God’s gifts,
could lead men to equate God’s favor and gifts. Certainly it is true
that God protects, heals, rewards, and cares for the unregenerate.
But none of these verses indicates an attitude of favor toward the un-
regenerate beneficiaries of His gifis. Only in the use of the word
“favor” in its slang form of “do me a favor” can we argue that a gift
from God is the same as His favor. Favor, in the slang usage, simply
means gift— an unmerited gift from the donor. But if favor is under-
stood as an attitude favorable to the unregenerate, or an emotional
commitment by God to the unregenerate for their sakes, then it must
be said, God shows no favor to the unrighteous.

Coals of Fire

One verse in the Bible, above all others, informs us of the under-
lying attitude of God toward those who rebel against Him despite
His gifts. This passage is the concomitant to the oft-quoted Luke
6:35-36 and Matthew 5:44-45. It is Proverbs 25:21-22, which Paul
cites in Romans 12:20:

If thine enemy be hungry, give him bread to eat; and if he be
thirsty, give him water to drink: For thou shalt heap coals of fire
upon his head, and the Lord shall reward thee.

Why are we to be kind to our enemies? First, because God in-
structs us to be kind, He is kind to them, and we are to imitate Him.
Second, by showing mercy, we heap coals of fire on their rcbellious
heads. From him to whom much is given, much shall be required
(Luke 12:47-48). Our enemy will receive greater punishment for all
eternity because we have been merciful to him. Third, we are prom-
ised a reward from God, which is always a solid reason for being
obedient to His commands. The language could not be any plainer.
Any discussion of common grace which omits Proverbs 25;21-22
from consideration is not a serious discussion of the topic.
