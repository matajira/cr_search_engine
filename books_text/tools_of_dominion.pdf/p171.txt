A Biblical Theology of Slavery 163

at least one specialist has argued that it was the Northern Kingdom’s
protest against Solomon's forced labor.'2° In any case, God put a
check to the expansion of empire in Israel. The empire of Egypt in-
vaded Judah, and this event brought Rehoboam to his theological
senses (II Chron. 12:1-12), After the breakup of the united monarchy,
there were no further examples of forced labor, except in the case of a
national military emergency: fortifications (I Ki. 15:22).

Greece’s descent into slavery to Rome, and Rome’s subsequent
descent into slavery to the State were both fitting. Free men of both
societies had been unwilling to submit to the economic authority of
other free men (i.e., the competitive free market}. They eventually
were compelled to submit to the State. Slaves served as high-level
managers in both Greece and Rome. Why did these positions of
great economic responsibility become the inheritance of slaves? A.
H. M. Jones argues that the reason was the unwillingness of free
men to work as employees of others. They refused to take orders
from anyone. Freed slaves in Rome became the secretaries of the
tich and powerful. The emperor’s secretaries and accountants in
Cicero’s day became Rome’s Secretaries of State and Ministers of
Finance; ‘no Roman of standing would have demeaned himself by
becoming the emperor's personal servant.” Only in the first century
A.D. did these offices become acceptable to the upper classes, though
never to senators.1?! Men’s arrogance led to their own enslavement.

Limited Slave Trade

Tt was possible in all ancient societies to purchase foreign slaves, but
the evidence indicates that this form of commerce was limited. A sepa-
rate class of slave traders did not develop in the ancient Near East, as
far as the presently known records reveal.!?? This indicates that there
was insufficient demand for imported foreign slaves. As Adam Smith
said in Wealth of Nations, specialization is limited by the extent of the
market. !23 The international market for slaves was not extensive. !*+

120. J. Alberto Soggin, “Compulsory Labor under David and Solomon,” in
Tomoo Ishida (ed.), Studies in the Period of David and Solomon and other essays (Winona
Lake, Indiana: Eisenbrauns, 1982), p. 267.

121. Jones, “Slavery in the Ancient World,” Econ. Hist. Reo, p. 186

122. Mendelsohn, Slavery In the Ancient Near East, p. 4.

123. Adam Smith, An Inquiry into the Nature and Causes of the Wealth of Nations (New
York: Modern Library Edition, [1776] 1937), ch. 3.

124. Tt might be argued that all markets were lintited in the ancient world. This
in fact was the argument of Karl Polanyi, The Livelihood of Man, edited by Harry W.
Peterson (New York; Academic Press, 1981), pp. 78-79, 146. This peculiar thesis is
cfiectively refuted by Morris Silver, Economic Structuses of the Ancient Near East
(Totowa, New Jersey: Barnes & Noble, 1985), chaps. 5, 6.
