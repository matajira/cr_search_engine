380 TOOLS OF DOMINION

who is not a producer of the commodity of labor—is in sorry shape
unless he has a great deal of income-producing capital.

The Bible’s slave laws confirm this obvious economic truth. So
valuable is “man, the commodity,” that specific rules which limit the
exploitation of this commodity by other men have been established
by God. The key limitation is the seven-year maximum period of in-
dentured servitude (Deut. 15:12). This limitation keeps down the
price of the human commodity by restricting the period of time in which
his services can be lawfully capitalized hy an owner, Ewen in the case of life-
time slavery, Old Testament iaw restricted slave-owners in their
dealings with slaves. It is not true, as M. I. Finley asserts, that “The
failure of any individual slaveowner to exercise all his rights over his
slave-property was always a unilateral act on his part, never bind-
ing, always revocable.”™ In Greece and Rome, perhaps; not in an-
cient Israel. God, then as now, always warned those under the terms
of His covenant that those in authority over men are also under the
authority of other men, and that all men are under God and His law.

The Bible uses the economic self-interest of the owner to supple-
ment the self-government and therefore the self-restraint that own-
ers are expected to demonstrate to those under their authority. The
bondservant is a valuable commodity. God tells bondservant-owners,
“Handle with care, for these people are made in My image!” If they
refuse to listen to God, then perhaps they will listen to the market. If
they refuse to listen either to God or to the market, then the civil
government must step in and enforce the law of God regarding in-
dentured servitude. If the civil government refuses to obey God in
this way, then God imposes other forms of negative sanctions: war,
pestilence, or famine. There is no better example of this inescapable
covenantal process in New Covenant history than the history of slay-
ery in the American South.

Modern democratic theory has denied the legitimacy of biblical
indentured servitude, but it has substituted a new form of slavery,
which is in fact a very ancient form of slavery: slavery to the State. The
State is a slave-owner which wants no private competition. It wants
people placed in permanent bondage to the State. It establishes what
sociologist Max Weber described as the bureaucratic cage.* It calis
this system democratic freedom.

56. Finley, Ancient Slovery, p, 74.

57. Gary North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Cage,” in North (ed.), Foundations of Christian Scholarship: Essays in the Van ‘Lil Perspec~
tive (Vallecito, California: Ross House Books, 1976).
