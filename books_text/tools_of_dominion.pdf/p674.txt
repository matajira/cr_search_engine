666 TOOLS OF DOMINION

What is the modern dowry? A monetary payment equivalent of a
college education or other formal training, plus the cost of a wed-
ding. This would probably involve the equivalent of many years of
net income, after minimal support for himself. If the girl had re-
ceived no advanced education, he would pay for it. If she had been
sent to private high school and college by her father, the father would
be reimbursed for the expenses, plus interest from the time of the
seduction until final payment. The seducer would pay for her dowry.

The next potential bridegroom could not be asked to pay some-
thing. She has become a liability. In a God-honoring society, her
Jack of virginity would be an initial liability, depending on the cir-
cumstances of her rebellion. A righteous young man would fear a
flaw in her moral character. But if she brings skills and money into
the marriage, plus several years of righteous behavior, he may be
willing to consider her.

In our day, all this sounds very old fashioned, even archaic.
These days, so does chastity. This marks the moral decline of the
West, not its moral maturity. With Christ’s payment behind us, all
sins can be covered in each person's experience, but this does not
obliterate the need for visible evidence of progressive sanctification.
The words of moral warning of the father to his son in the first nine
chapters of Proverbs are still valid.

Conclusion.

This case law indirectly brings up the threat of slavery. This is
the integrating theme of the case laws of Exodus 21-23. The penalties
of public sinning are always of such magnitude that flagrant public
sinning could and probably would involve a return to slavery for
most publicly condemned sinners. This, of course, is the whole
message of the Book of Exodus: God delivers His covenant people
from slavery, but He threatens them with a return to slavery if they
should continue to break His covenant. Ultimately, He threatens
them with public execution.

The bride price paid to the father by the seducer is a classic ex-
ample of this return to slavery. The short-run perspective of the
seducer is essentially the time perspective of Satan and his fullowers:
a few moments of ecstasy in defying God, and eternity in bondage to
repay Him. These forbidden moments of ecstasy began in the gar-
den and will end at the final judgment.
