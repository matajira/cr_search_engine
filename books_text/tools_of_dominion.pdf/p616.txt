19

SAFEKEEPING, LIABILITY,
AND CRIME PREVENTION

If a man shall deliver unto his neighbour money or stuff to keep, and it be
stolen out of the man’s house; if the thief be found, let him pay double. If
the thief be not found, then the master of the house shall be brought unto the
Judges, to see whether he have put his hand unto his neighbour's goods. For
all manner of trespass, whether tt be for ox, for ass, for sheep, for raiment,
or for any manner of lost thing, which another challengeth to be his, the
cause of both parties shall come before the judges; and whom the judges
shall condemn, he shall pay double unto his neighbour (Ex. 22:7-9).

Part of ancient Israel’s concept of neighborly hospitality! involved
taking care of the neighbor’s property from time to time. Exodus
22:7-9 deals with inanimate property as well as animals. Exodus
22:10-13 deals exclusively with animals entrusted to another's care.”
The existence of case laws governing safekeeping testifies to the fact
that it was considered socially acceptable for an Israelite to ask his
neighbor to safeguard his goods temporarily. This should also be
true for modern Christians, Neighborly safekeeping is clearly a bene-
fit to a man who is taking his family on a journey. He needs someone
to watch over his possessions.

The neighbor is expected by both God and man voluntarily to
accept this caretaking responsibility. Why? Because God accepted
this same responsibility in ancient Israel. God promised to serve the
Israelites as the safekeeper of their goods when they journeyed to
Jerusalem to celebrate the feasts. “For I will cast out the nations be-
fore thee, and enlarge thy borders: neither shall any man desire thy
land, when thou shalt go up to appear before the Lorn thy God

1, James B. Jordan, “God's Hospitality and Holistic Evangelism,” (1981), in
Jordan, The Sociology of the Church; Essays in Reconstruction (Tyler, Texas: Geneva
Ministries, 1986), pp. 207-20.

2. See Chapter 20: “Caretaking and Negligence.”

608
