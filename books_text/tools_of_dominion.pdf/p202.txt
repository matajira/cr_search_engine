194 TOOLS OF DOMINION

except through the death, either physical or covenantal, of one of the
partners.

This brings us back to the topic of slavery. The covenant is the
key concept that distinguishes biblical slavery from indentured servi-
tude. Heathen slavery in the Old Testament was a permanent (ten
generations, minimum) relationship that was part of a covenant in-
stitution: church (tabernacle-temple), State, or family. As I men-
tioned cartier, the Gibeonites were servants of the congregation, mean-
ing they were employed by the tabernacle (Josh. 9:27), All other
forms of non-criminal personal subordination in Israel were limited
by the terms of a contract, either stated or implied, and were re-
stricted temporally, As we have seen, the one exception to this law of
covenants —intergenerational heathen slavery—was abolished by
Jesus Christ, through His fulfillment of the jubilee law. All economic
relationships in New Testament times are therefore exclusively con-
tractual; none is lawfully based on the invoking of a covenant, which
is why contracts are less binding than covenants.*#! In economic
affairs, there is a temporal limit on debt “bondage,” precisely because
debt is not legally a covenantal bond. That temporal limit is seven years
(Deut. 15}.252

Debt and Slavery: Limits on Capitalization

The contractual nature of New Testament economic relations
means that it is illegal in the eyes of God for a person or an institu-
tion to capitalize another person’s net future productivity beyond
seven years, Individuals cannot legally promise to fulfill the terms of
any contract beyond seven years. God’s law points to a biblical prin-
ciple: that no man can know the economic future accurately enough
to allow him to make such legally enforceable promises. Thus, a
man cannot legally capitalize his future beyond seven years. This is
another way of saying that he cannot legally offer to sell for a lump
sum his net future productivity beyond the seventh year. If he enters
into such an agreement, no agency of civil or ecclesiastical govern-
ment should enforce the terms of the contract.

We understand this with respect to chattel slavery. The seller of
lifetime labor services is not supposed to make such an offer, except

231, North, Sinai Strategy, pp. 65-70.

232, The one exception is debt bondage that is imposed by the State against a
criminal who must sell himself into long-term slavery in order to make restitution to
his victims, This is a temporal manifestation of God’s wrath against the criminal's
breaking of the civil covenant, It is an involuntary relationship of subordination,
once the criminal is convicted,
