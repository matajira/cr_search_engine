Knowledge, Responsibility, and the Presumption of Guilt 501

Ox for Ox

“Or if it be known that the ox hath used to push [gore] in time
past, and his owner hath not kept him in; he shall surely pay ox for
ox” (Ex, 21:36), What is the meaning of “ox for ox”? In the previous
case of a beast whose dangerous behavior had not been a matter of
public knowledge, the owner of the dead beast does not receive a re-
placement ox. He only receives haif of the proceeds of the sale of the
live ox and the dead ox’s carcass. But this case is different. The gor-
ing beast is known to have gored in the past. The owner of the dead
ox is to be fully reimbursed, “ox for ox.”

Does this mean that the owner of the dead beast is simply to be
given the surviving ox? This would be a very unlikely interpretation,
First, the surviving ox is now a known renegade. It is a menace, as
the owner of the dead ox knows only too well. The owner of the sur-
vivor therefore would be transferring ownership of a high-risk beast
to the owner of the victim. But a high-risk property always com-
mands a lower sale price than a low-risk property, for obvious rea-
sons. The buyer has to be compensated for the added expense he is
accepting by purchasing the high-risk property.

Second, the market value of the dead beast may be far higher
than the transgressing survivor, irrespective of the risk factor. Per-
haps the dead beast was a prize-winning beast. The victim now can
sue for damages. He is to be reimbursed, “ox for ox.” In other words,
he is to be reimbursed like for like, value for value. On the one hand,
as the owner of a champion bull, he has a financial incentive to keep
his high-value beast away from any potentially dangerous beast that
has not been identified as dangerous. On the other hand, it is the re-
sponsibility of the owner of a known renegade beast to keep it away
from other bulls, especially champion bulls. The economic burden
now shifts to the owner of the killer beast.

What is the difference between the two cases? In both cases, one
man loses his beast, and another man’s beast survives. The differ-
ence has to do with differences in knowledge: by the court, by the dead
beast’s owner, and by the surviving beast’s owner, Greater knowl-
edge establishes greater responsibility (Luke 12:41-48).

This principle of comparative knowledge leads to the conclusion
that certain animals that are by nature dangerous, and only
marginally and sporadically responsive to human training, are auto-
matically considered notorious. Maimonides defined such animals
as those that kill by goring, biting, clawing, or similar action. Fol-
