Selling the Birthright: The Ratification of the U.S. Constitution 1215

Rushdoony is still using this bit of mytho-history regarding the idea
of sovereignty in the early American period in order to justify his
defense of the Constitution. “The Constitution is unique in world
history in that there is no mention of sovereignty, because sover-
eignty was recognized as being an attribute of God.”®* Indeed, it
truly was seen ag an attribute of God, and the Framers identified this
god: the People.

The transformation of Rushdoony’s biblical judicial theology of
the early 1970's into a theological defense of judicial neutrality in the
late 1980’s was accurately predicted . . . by Rushdoony: “If a doc-
trine of authority embodies contradictions within itself, then it is
eventually bound to fall apart as the diverse strains war against one
another, This has been a continuing part of the various crises of
Western civilization. Because the Biblical doctrine of authority has
been compromised by Greco-Roman humanism, the tensions of au-
thority have been sharp and bitter.”8? No sharper and no more bitter
than in the remarkable case of Rushdoony v, Rushdoony.

 

Conclusion

The ratification of the U.S. Constitution in 1788 created a new
nation based on a new covenant. The nation had broken with its
Christian judicial roots by covenanting with a new god, the sover-
eign People. There would be no other God tolerated in the political
order, There would be no appeal beyond this sovereign god. That
collective god, speaking through the Federal government, began its

 

who engineered the acceptance of a privately owned central bank, the Bank of the
United States, the forerunner of the modern Federal Reserve System. But he had
apparently repented. The story of Hamilton’s plans is Rushdoony’s version of the fa-
miliar story of the death-bed conversions of atheists.

In response, let me cite Rushdoony’s June, 1968 Chalcedon Report: “Why am I
quoting this story? Because it illustrates so well the desire of many people for a happy
ending, for fairy tales. A few years ago when 1 spoke in onc city, a woman told me
(the entire group knew the story from her) that Charles Darwin had renounced evo-
lution in his old age and died a Christian. Also, she claimed this could be found in a
book she had seen, of Darwin's letters, and the book had since disappeared from the
public fibrary. I stated that [ owned the book, and it contained no such statement.
The result: no one in that group wanted to hear me again!” He has said it best.
Christians want to believe that enemies of the faith who are famous eventually con-
vert to Christianity.

86. This was his reply to Otto Scott’s comment about the U.S, being the first
nation to establish itself without reference to God. 0 & A, Leviticus serman, Jan.
30, 1987.

87. Rushdoony, fnstitutes, p. 213.
