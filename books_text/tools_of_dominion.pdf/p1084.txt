1076 TOOLS OF DOMINION

inherently unknowable god. An unknowable god is the only gad who
is acceptable to modern autonomous man, for an unknowable god
presumably will not bring final judgment to inherently uninformed
and uninformable finite mankind. We must never forget: the primary
goal of self-proclaimed autonomous man is to escape God's final judgment. So,
in order to escape this judgment, the higher critics spin a web of
pompous verbiage that they hope and pray — well, at least they hope—
will protect them from the eternal consequences of their God-defying
rebellion.

Wha Is the Hoaxer?

Our authors ask three rhetorical questions, and then give their
hapless readers a bowl of lukewarm mental mush in reply. First, the
questions: “Yet there is always the final lurking question: Is the Bible
true? What is truth and what is just symbolic? Cannot I have any-
thing that is absolutely certain?” Then the mush: “The answer must
be that the symbol ¢s the truth. We have no other truth. We know it is
not literal truth, but we know that the biblical portrayal is the rela-
tionship between the unknown infinite and ourselves here and now.
No precise dividing line can be drawn between the ultimately real
and the poetic symbol, because God has not made us infinite.” In
short, they argue that because I am not infinite, and therefore not
God, I need not fear an infinite God, for my very finitude keeps me
from knowing God. To which Paul answered many centuries ago:

For the wrath of God is revealed from heaven against all ungodlincss
and unrighteousness of men, who hold [back] the truth in unrighteousness;
because that which may be known of God is manifest in them; for God hath
shewed it unto them. For the invisible things of him from the creation of the
world are clearly seen, being understood by the things that are made, even his
eternal power and Godhead; so that they are without excuse (Rom. 1:18-20).

The Bible of the higher critics cannot possibly be what it says
clearly that it is: the revealed Word of the Creator and Judge of the
universe. Now, if the Bible really isn’t what it says it is, then it must
be a hoax. Once the implicit though politely unstated accusation of
hoaxing is made, the question then arises: Who is the true hoaxer,
God or the higher critic? There should be no doubt in our minds: the
literary critic is the myth-maker. Literary higher criticism of the

35. Wright and Fuller, Acts of God, p. 37.
