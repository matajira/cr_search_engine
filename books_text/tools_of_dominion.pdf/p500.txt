492 TOOLS OF DOMINION

The same principles of economic analysis that apply to speeding
and drunk driving can be applied to other areas of life in which the
State is the primary protector of life and limb. Fines to the civil gov-
ernment should be imposed on convicted violators only in cases where
the civil government is acting as a trustee for future unknown victims.

Political Hypocrisy

The problem today is that society refuses to accept the morally
and legally binding nature of Old Testament legal principles of
criminal negligence. First, legislators do not consistently make “pit
owners” legally liable for damages, as the Bible requires. The most
flagrant example is the failure of state and local governments to im-
pose stiff fines on all drunk drivers, and capital punishment on
drunk drivers whose unsafe driving leads to someone else’s death.
Furthermore, politicians do not impose fines on themselves or city
employees for failing to repair public streets with potholes which
cause damages to people’s cars or which cause accidents.

Second, politicians pass safety laws (or allow the bureaucracy to
define and then enforce earlier laws) whose costs to the general pub-
lic are not immediately perceptible. They may require automobile
companies to install seat belts that buyers do not want to pay for,
and which occupants subsequently refuse to use, but politicians are
not about to pass a law that would impose fines on families for refus-
ing to install smoke detectors in their own homes. The first piece of
legislation would not gain the reprisal of voters; the second probably
would, In short, they pass pieces of legislation with minimal political
and statistical impact (for good or evil) in terms of the utopian prin-
ciple of “better to spend millions of dollars than to suffer one dead
victim,” but fail to honor it in statistically relevant cases because of
the equally relevant (to them) political backlash they would receive
from voters. The proclamation of the “better millions of dollars
than. . . .” principle has been, is, and will continue to be the prod-
uct of economic ignorance and political hypocrisy.

This is not to say that it is always wrong to require owners to pay
more in order to save lives, but the Bible provides us with the proper
guidelines, not some hypothetically universal utopian principle that
would necessitate the creation of a messianic State. The general
principle is simple: those who own a known dangerous object are legally re-
sponsible for making it safer for those who are either immature or otherwise un-
warned about the very real danger.
