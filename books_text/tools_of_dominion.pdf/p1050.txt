1042 TOOLS OF DOMINION

He is not alone in his assessment of Maimonides’ Code, Maimonides
specialist Isadore Twersky says that “The Mishnek Torah, which was
to change the entire landscape of Rabbinic literature, also pushed
back the frontiers of Maimonides’ sphere of influence and made his
fame global as well as imperishable. It transformed him, in the
course of a few decades, from the ‘light of the East’ to ‘the light of the
{entire] exile.’ He almost literally became a major Jewish luminary.
. . . In one broad generalization, we may say that the Miskneh Torah
became a prism through which reflection and analysis of virtually all
subsequent Talmud study had to pass. There is hardly a book in the
bread field of Rabbinic literature that does not relate in some way to
the Mishneh Torah.” Furthermore, “The Mishneh Torah is reputedly
second only to the Bible in the number of commentaries and studies
it has elicited.”"*

An incomplete list of 220 major commentaries on the Mishneh
Torah was made in 1893.4 Michael Guttman has written: “The
Mishneh ‘Torah became the center of the whole halachic literature. It
acquired the place of a new code of general esteem and acknowledg-
ment, like the Mishna a thousand years before, and the greatest
halakhic scholars entered into competition with each other in com-
posing commentaries to Maimonides and settling the difficulties,
which the lack of indicating sources left to them.” His fame
throughout Europe spread even faster than copies of the Code, '*

Why should the Code have had such an impact? For one thing,
because copies of any book as massive as the Talmud were scarce in
the era before modern printing. Maimonides’ [4 relatively compact
volumes were minuscule when compared to the gigantic Talmud.
Furthermore, the Code is structured by judicial topics; the Talmud’s
structure is highly complex and intimidating.

142. Isadore Twersky, Iniroduction to the Code of Maimonides (Mishneh Torah) (Now
Haven, Connecticut: Yale University Press, 1980), pp. 19-20; ef. 516-18.

143. Ibid., p. 526. Nevertheless, for generations Talmudists refused to mention
the Mishneh Torah by name: p. 527. This may have been because it enabled laymen
to check the decisions of the judges: Johnson, Hisiory, p. 191.

144, Alexander Marx, Studies in Jewish History and Booklore (New York, 1969), pp.
38-41; cited by Johnson, History, pp. 191.

145, Michael Guttman, “The Decisions of Maimonides in THis Commentary on
the Mishna,” Hebrew Union College Annual, 1 (1925), p. 229.

146. Alexander Marx, “Ihe Gorrespondence Between the Rabbis of Southern
France and Maimonides About Astrology,’ ibid., ITT (1926), pp. 325-26.
