Kidnapping 335

A fundamental principle of economics is this: the division of
labor is limited by the extent of the market. This was articulated by
Adam Smith in Chapter 3 of Wealth of Nations (1776). Another basic
principle is this one: the greater the division of labor, the greater the
output per unit of resource input in short, the greater the efficiency
of the market, When the market increases in size, it makes possible
an increase in cost-effective production. Advertising and mass-
production techniques lower the cost of production and therefore in-
crease the total quantity of goods and services demanded. This is
well understood by all economists.

Nevertheless, there are some people who still believe that laws
against so-called “victimless crimes” —sins that they do not regard as
major transgressions, I suspect ~ actually increase the profitability of
crime. On the contrary, such laws increase the risk of the prohibited
activities, both to sellers and consumers. Prices rise; the market
shrinks; per unit costs rise; efficiency drops. What such laws do is
create monopoly returns for a few criminals. But the critics of such
laws conveniently forget that monopoly returns are always the product of
reduced output, This, in fact, is the conventional definition of a mon-
opoly. Thus, civil laws do reduce the extent of the specified criminal
behavior.*3 They confine such behavior to certain criminal sub-
classes within the society. Biblically speaking, such laws place bound-
aries around such behavior.

There is no doubt that nineteenth-century laws against the slave
trade drastically reduced the profitability of the international slave
trade. These laws increased the risks for slavers, reduced their
profits, and narrowed their markets. The result was a drop in output
(slavery) per unit of resource input.

Househald Evangelism

Apart from the one exception provided by the jubilee law, the
Old Testament recognized the legitimacy of involuntary slavery of
foreigners only when the slaves were female captives taken after a
battle (Deut, 20:10-11, 14). To fight a war for the purpose of taking
slaves would have been illegitimate, for this was (and is) the foreign
policy of empires. It is true that the jubilee law did allow both the
importation of pagan slaves and the purchase of children from resi-

33. Gf. James M. Buchanan, “A Defense of Organized Crime” in Ralph Andreano
and John J. Siegfried (eds.), The Economics of Crime (New York: Wiley, 1980), pp.
395-409.
