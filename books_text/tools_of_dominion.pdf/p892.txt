884 TOOLS OF ROMINION

personal covenant betwee God and His people, a aw-covenant.
Moses destroyed the tablets as a ritual response to the people be-
cause they had broken the ethical covenant (Ex. 19) by their rebel-
lious ritual response to God. They had chosen to worship a god of
their own hands; Moses demonstrated ritually what this really
meant: their breaking of the covenant of God the Cosmic Potter,
who makes man as a potter forms the clay, They were not willing to
acknowledge, as Isaiah later acknowledged: “But now, O Lorn, thou
art our father; we are the clay, and thou our potter; and we all are
the work of thy hand” (Isa. 64:8). God then smashes the rebellious
clay in. judgment: “Behold, ye are of nothing, and your work of
nought: an abomination is he that chooseth you. I have raised up
one from the north, and he shall come: from the rising of the sun
shall he call upon my name: and he shall come upon princes as upon
morter, and as the potier treadeth the clay” (Isa. 41:24-25).

To dramatize the inevitable judgment of God, Moses then con-
ducted another ritual—from a strictly economic standpoint, proba-
bly the most graphic ritual ever recorded in the Bible. He burncd the
calf in the fire, smashed ils remains to powder, put the powder in
water, and then commanded the people to drink the water (32:20).2!
Biological processes then took over to produce the final, graphic, and
memorable ritual disposal of the religious symbol that had consumed
so much of their capital. They saw their capital go down the prover-
bial drain.

The people had demanded the right to sacrifice part of their
wealth to the god of their own hands. The calf had been made quickly
by amateur craftsmen working under Aaron, and had been pul into
immediate service. They sacrificed joylully, participating in sexual
debauchery (Ex. 32:25) as a religious affirmation of their faithfulness
to the gods of the chaos festival, the gods of cosmic renewal through
ritual lawlessness.”? ‘These were the gods that were familiar to them,
polytheistic gods like those of Egypt, from which they had been de-
livered, and also like those of Canaan, which they believed was
about to be delivered into their hands. Here were gods that demanded sac-

21. This was equivalent to the ordeal of jealousy which was required in the Old
Testament when a husband brought a charge of adultery against his wife (Num.
5.1131).

22. Roger Caillois, Man and the Sacred (Glencoe, Illinois: The Free Press, 1959), p.
164. Cf. Thorkild Jacobsen, “Mesopotamia: The Function of the State,” in IL. and
H. A, Frankfort, et al., The Intellectual Aduenture of Ancient Man: An Essay on Speculative
Thought in the Ancient Near East (University of Chicago Press, [1946] 1977), pp. 198-201.
