762 TOOLS OF DOMINION

intuitive continuum belongs to that special category of concepts
about which we can discourse with each other without being able to
define them.”!” This statement does not go far enough: adi logical
concepts possess this same quality of not being able to be defined
precisely. The human mind is not omniscient; absolutely precise de-
finitions are always elusive to man’s mind. The mathematician-
philosopher Alfred North Whitehead said, “As soon as you leave the
beaten track of vague clarity, and trust to exactness, you will meet
difficulties.” ® You will meet more than difficulties: you will meet fail-
ure. As Georgescu-Roegen notes, “any vocabulary is a finite set of
symbols.”!9 The structure of vocabulary “does not have the power of
the continuum.””? In short, there is an inescapable element of uncer-
tainty in exercising judgment. ‘A measure for all uncertainty situa-
tions, even though a number, has absolutely no scientific value, for it
can be obtained only by an intentionally mutilated representation of
reality. We hear people almost every day speaking of ‘calculated risk,’
‘but no one yet can tell us how he calculated it so that we could check
his calculations.”?!

Men are not omniscient. They cannot know another man’s heart
(Jer. 17:9), Only God knows men’s hearts (Jer. 17:10). “But the Lorp
said unto Samuel, Look not on his countenance, or on the height of
his stature; because I have refused him: for the Lorp seeth not as
man seeth; for man looketh on the outward appearance, but the
Lorn looketh on the heart” (I Sam. 16:7). But we do not need to
render perfect justice in order to render adequate justice. We render
preliminary justice, and leave the rest to God. This is why capital
punishment is required by God: it turns over the person immedi-
ately to the highest court of all, the throne of God. God does not wait
for a judicially convicted person’s “biological time clock” to deliver
him into His presence for God’s preliminary judgment.?2

17. Ihid., p. 66.

18. Whitehead, Science and Philosophy (New York: Littlefield, 1948), p. 136; cited in
ibid, p. 90.

19. Ibid., p. 73.

20. Idem.

21. Thid., p. 83.

22. This judgment by God is preliminary because God confines a soul either to
heaven or hell, both of which are temporary places of residence. Final judgment
comes at the resurrection, when body and soul are reunited perfectly, and people are
sent cither into the eternal lake of fire (Rev. 20:14-15) or into the final manifestation
of the new heaven and new earth (Rev. 21).
