810 TOOLS OF DOMINION

prayer and sacrifice to God are different from bribes.”*7 What is the
difference? It is the difference between worship and judgment. We
do not lawfully ask God to pervert judgment when we pray or bring
sacrifices to him, We honor Him as Creator and Redeemer, not as
Judge. Civil judges are not to receive gifts because they are neither to
be worshipped nor asked to pervert judgment; they serve as represen-
tatives of God’s justice, not God’s character as Creator and Redeemer.

The evil of taking gifts is the evil of threatening unrighteous
judgment through respect of persons. Taking a bribe is synonymous
with perverting judgment; it is prohibited in the affairs of civil or ec-
clesiastical judgment. It is not wrong for pastors to receive gifts to
the church in the name of God, but to the extent that these gifts are
received in order to pervert justice, they are regarded by God as
bribes. Thus, church rulers have a more difficult task in identifying
bribery than civil judges do. The civil magistrate does not represent
God in His capacity as Creator and Redeemer, but only in His ca-
pacity as Judge. He is unlikely ever to be given a gift, except in his
capacity as judge. This is not true of the church officer, who receives
gifts in the name of the church.

The Bible does not teach that bribe-offering is always wrong. If
given by a righteous person who sccks righteous judgment from an
unrighteous judge, it is valid. If given by someone 10 pervert God’s
law, it is evil. The quest for a neutral definition of bribery which
equates both practices is a biblically illegitimate quest.

47, Noonan, Bribes, p. xvi.
