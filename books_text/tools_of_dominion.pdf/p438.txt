430 TOOLS OF DOMINION

pays for it, plus restitution for the victim’s pain, fear, and trouble.
The technological advances brought by Western ~ and initially Chris-
tian®2— civilization make possible the best solution for both parties,
namely, the restoration of the injured man’s sight, but at the expense
of the criminal. The technological progress that would be brought by
a thoroughly Christian civilization would make possible a better set
of options for both victim and criminal. The more faithful society’s
commitment to enforcing God’s law, the more rapid the technologi-
cal progress is going to be.

Limiting One’s Original Demands

The threat of actual physical mutilation for the convicted violent
criminal will always be present in a biblical legal order. The victim
has lost his cye or tooth; the criminal deserves to lose his. But few
criminals would sacrifice an eye if they could make restitution in
some other way. They might sacrifice a tooth, but not an eye. The
victim can legitimately demand the removal of the other man’s eye,
but there is not much doubt that he would prefer a large cash settle-
ment to help him recover his lost productivity and forfeited economic
opportunities. He might even be able to get a new eye through
surgery. The rich man is allowed to “buy his way out,” but only at the
discretion (and direct economic benefit) of the victim. On the other
hand, the victim can demand his “pound of flesh,” but only by for-
feiting the money that he might have been paid.

What if the victim is really vindictive? What if he demands 1,000
ounces of gold for the other person’s tooth? In all likelihood, the
criminal would prefer to forfeit the tooth. Under this kind of judicial sps-
tem, the victim must estimate carefully in advance just what the convicted person
might be willing and able to pay, There must be no “fall-back position”
after the victim submits his pair of demands to the judges: physical
mutilation or a specified financial restitution payment.

Under a biblical system of economic substitution, the victim
would be required by the court to specify the minimum amount of
money he would be willing to accept in exchange for not having
mutilation imposed on the criminal. The victim would not be allowed
to present a false estimate about how much restitution he would
be willing to accept. This would be false witness, or perjury. He
could not come back a second time, after the criminal has refused to

22, See footnote #8, above.
