1050 TOOLS OF DOMINION

For example, Maimonides discussed the case of a thief who stole
an animal or a vessel, and who then immediately slaughtered the an-
imal or deliberately broke the vessel—perhaps to conceal the evi-
dence of the crime? — and later is convicted of the theft. What if, in
the meantime, the market value of the stolen object has doubled?
Does the thief pay double restitution based on the value of the item
at the time of the theft or based on its market value at the time of the
trial? If he has profited fromthe transaction, Maimonides said, he must
pay restitution based on the stolen ‘object’s value at the time of the
trial, But what if the thief accidentally lost the animal or accidentally
broke the vessel? Maimonides stated, though without presenting any
justifying argument, that the negligent thief owes restitution only on
the value of the object at the time of the theft. '”

Undermining Justice

Such a legal principle would undermine biblical civil justice.
First, how is the court to determine whether the loss was accidental?
The thief obviously has a financial incentive to lie, since the burden
of his repayment will be lighter. Second, what of the victim’s added
economic loss? Who protects the victim’s interests? Why should his
loss as a result of the time delay between the theft and the trial not be
fully compensated by the thief, irrespective of the latter’s quality of
stewardship over the stolen goods? What Maimonides should have
concluded was that the thief must provide multiple restitution to a
victim based on the replacement cost at the time of his conviction for the
crime. If the animal were still alive, he would be required to return
that animal, and the animal would obviously be worth today’s mar-
ket value. Thus, the replacement value for a slaughtered animal is
also to be worth today’s market value, and so is the equivalent pro-
portional restitution payment. This is obvious, this is fair, and
Maimonides ignored it. He departed from both the letter of biblical
law and its spirit.

He concluded all this by stating that two-fold restitution is not
required from any thief who is convicted of stealing bonds, land, or
slaves, “because Scripture has imposed the liability for double pay-
ment only on movable things that have an intrinsic value, for it says,
On an ox or an ass or a sheep or a garment (Ex. 22:8).” But aren’t slaves mov-

169. “. . . if, however, the animal dies or the vessel is lost, he need pay only dou-
ble its value at the time of the theft.” Ibid., “Laws Concerning Theft,” Chapter Onc,
Section Fourteen, p. 63.
