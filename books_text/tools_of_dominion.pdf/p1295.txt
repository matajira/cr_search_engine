Index 1287

Weitzman, Lenore, 214n
Wells, Ronald, 53
Wenhem, Gordon, 656n
Westermann, William, 136n, 137n,
158, 160, 186, 187
Westminster Confession, 974, 986
Wheat and tares, 964-68, 974,
982-83, 996
Whip, 168, 170, 173, 204, 241, 299
Whipping, 289-99, 316, 378, 443-44,
527
Whitehead, Alfred, 762
Whitney, Eli, 191
Whoredom, 646-47, 649, 651, 659,
661
Widow
church welfare, 270,
defenseless, 685
judge &, 795-96
mites, 802-3
oppression of, 669
remarriage, 647
usury, 705
vaws, 219
Wigner, Eugene, 994
Wilberforce, William, 186n
Wilde, Oscar, 1086
Wilderness, 1, 667
Williams, Eric, 188n
Williams, Roger, 200n
Wilson, Edmund, 1063-64, 1069-70
Wilson, James G., 95n, 1170-71, 1197
Wilson, John L., 357
Wilson, Woodrow, 353n
Wine, 189n
Winthrop, John, 682-83
Wisdom, 678, 832
Witchcraft, 791
Witness
accomplice, 638
carcass, 637
conscience, 849
false, 650
Maimonides on, 499
negligence, 637-38
no legal claim, 296
prosecution, 280

States’ agent, 296
Witnesses
double, 468
dual, 286
execution, 44
prosecution, 291
standards, 292
Wittfogel, Karl, 922
Wives
auction for, 196
freedom, 124
negative sanctions, 855n
Women
baptized, 843
freedom, 124
representation, 843
slaves, 122
Talmud’s view, 1021-22
Wood, 926
Work ethic, 232, 238, 374-75, 376,
378, 417-18
World War I, 192
Worms, 823
Wrath to grace, 94
Wright, G. Ernest, 1072-74
Wyatt-Brown, Bertrum
abolitionism, 37in
boredom, 349n
Christians vs. duels, 357
power, 352
South’s attitudes, 350-51
ungrammatical fighting, 345
Wyoming, 8440

Yates, Frances, 898
Yizhak, Akedat, 511-12
Yoke, 370

Zelophehad, 271-72
Zion, 673

Zionism, 1001-1002
Zoning, 586

Zorn, Raymond, 36
Zoroaster, 61
