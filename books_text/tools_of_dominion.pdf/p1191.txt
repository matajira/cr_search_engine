Lots of Free Time: The Existentialist Utopia of S.C. Mooney 1183

And if a sojourner or stranger wax rich by thee, and thy brother that
dwelleth by him wax poor, and sell himself unto the stranger or sojourner
by thee, or to the stock of the stranger's family: After that he is sold he may
be redeemed again; one of his brethren may redecm him: Either his uncle,
or bis uncle’s son, may redeem him, or any that is nigh of kin unto him of
his family may redeem him; or if he be able, he may redeem himself. And
he shall reckon with him that bought him from the year that he was sold to
him unto the year of jubilee: and the price of his sale shall be according un-
to the number of years, according to the time of an hired servant shall it be
with him. If there be yet many years behind, according unto them he shalt
give again the price of his redemption out of the money that he was bought
for (Lev. 25:47-51).

Mooney’s Strategy of Avoidance

It is worth peinting out that Mr. Mooney’s book includes com-
ments on Leviticus 25, but only on verses 2-7, 15-16, 35-37, and
39-45, He scrupulously avoids mentioning verses 25-28 and 47-51—
verses that absolutely refute his conclusion regarding the supposedly
biblically illegitimate nature of rental income. He freely admits that
the economists are correct, that rental income is the same as interest
income—a payment for the use of an asset over time, said B6hm-
Bawerk, whom he quotes favorably on the question of the equiva-
lence of rental income and interest income—and then he tries to
justify his universal condemnation of interest income by laying down
an equally universal condemnation of rental income. The problem
is, the Bible clearly honors the legitimacy of rental income: a stream
of income, either labor income or land income, which one receives
when he purchases an income-producing asset for cash (i-e., capital-
ization). Mr. Mooney’s answer to this dilemma is simple and direct:
he refuses to cite that portion of the Bible that categorically destroys
his argument.

So, he says, it is immoral to collect income from any form of
property. While Mr. Mooney is sufficiently astute tactically not to
spell out the implications of this statement—in this regard, he fol-
lows the lead of his predecessor, Mr. Keynes — what he really means is
that it is illegal biblically to seek a positive rate of return by loaning
someone money to buy a house, and it is also illegal biblically to rent

5, Ibid., p. 172.
