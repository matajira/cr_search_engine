Conclusion 943

biblical civil blueprints.?! That idea is the doctrine of an inescapably
predestined eternity of personal negative sanctions that will be im-
posed on everyone God hates. These two hated ideas are linked judi-
cially: sanctions. Men do not like to be reminded by Paul that “the
scripture saith unto Pharaoh, Even for this same purpose have I raised
thee up, that I might shew my power in thee, and that my name
might be declared throughout all the earth. Therefore hath he mercy
on whom he will have mercy, and whom he will he hardeneth”
(Rom. 9:17-18). If God did this with Pharaoh, He can do it to any-
one. This means sanctions.

The comparatively gentle negative civil sanctions of the Old Tes-
tament~— whipping, restitution, slavery, banishment, and public
execution—are light taps on the wrist when compared to an eternity
of screaming agony in the lake of fire. Civil sanctions are limited by
time; eternity is forever. Men easily understand this distinction.
Thus, in order to banish from their consciousness the thought of
eternal torture at the hand of an outraged, implacable, non-
rehabilitating God, they feel compelled to banish also the idea that
God has established civil covenants in history that authorize and re-
quire His lawful civil representatives to apply the Old Testament’s
minimal negative sanctions. Instead, they have implicitly adopted
two other doctrines, the doctrine of autonomous man and the con-
comitant doctrine, the autonomous State.

The State becomes the sole agency authorized by autonomous
man to impose compulsory sanctions. (The only alternative to this
view is the doctrine of zero civil government, meaning zero compul-
sory sanctions, a consistent but seldom articulated viewpoint.) In or-
der to assert his autonomy from God, the covenant-breaker places
himself under the authority of a self-proclaimed autonomous State.
He prefers to believe that the State’s sanctions are final. The State’s

21. That no such blueprints exist in the field of economics was the assertion of all
three of the other authors in the book, Wealth and Poverty: Four Christian Views, edited
by Robert G. Clouse (Downers Grove, Illinois: InterVarsity Press, 1984). The
fourth view —the explicitly, self-consciously, blueprint-insistent Christian one —was
mine. I, of course, challenged all three of the others, calling attention to their self-
conscious rejection of any explicitly biblical standards in economic analysis, Not
surprisingly, in less than a year, with the book selling well and our royaities ade-
quate, the neo-cvangelical liberals who run InterVarsity pulled the book off the mar-
ket and sold my company the remaining 6,000 copies at 25 cents per copy, just to
wash their hands of the whole project. That was when I knew who had won the
debate. Liberals would never be so crass as to burn conservative baoks; they simply
refuse to publish them or, once the mistake has been made, dump them.
