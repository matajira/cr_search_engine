994 TOOLS OF DOMINION

that this fact of mental symmetry can be used to cause desired effects
in the external realm of nature. Why this mental symmetry should
exist, and why it should bear any relation to the external realm of
nature, is unexplainable by the knowledge of natural man, a fact ad-
mitted by Nobel prize-winning physicist, Eugene Wigner.”?

Ghristians, because they have a proper doctrine of creation, can
explain both. So the unbeliever uses borrowed intellectual capital at
every step. Christians can use some of his work (by checking his
findings against the revelation in the Bible), and the unbeliever can
use the work of Christians. The earth will be subdued. The closer
the unbeliever’s presuppositions are to those revealed in the Bible
(such as the conservative economist’s assumption of the fact of
economic scarcity, corresponding to Gen. 3:17-19), the more likely
that the discoveries made in terms of that assumption will be useful.
By useful, I mean useful in the common task of all men, subduing
the earth. Thus, there can be cooperation between Christians and
non-Christians.

Conclusion

Unbelievers appear to be culturally dominant today. Believers
have retreated into antinomian pietism and pessimism, for they have
abandoncd faith in the two features of Christian social philosophy
that make progress possible: 1) the dynamic of eschatological optimism,
and 2) the tool of the dominion covenant, biblical law. We should con-
clude, then, that either the dissolution of culture is at hand (for the
common grace of the unregenerate cannot long be sustained without
leadership in the realm of culture from the regenerate), or else the
regeneraie must regain sight of their lost truths: postmillennialism
and biblical law. For common grace to continue, and for external
coopcration between believers and unbelievers to be fruitful or even
possible, Christians must call the external culture’s guidelines back
to God's law. They must regain the leadership they forfeited to the
speculations of self-proclaimed “reasonable” apostates. If this is not
done, then we will slide back once more, until the unbelievers
resemble the Ik and the Christians can begin the process of cultural

 

29. Fugene Wigner, “The Unreasonable Effectiveness of Mathematics in the Na-
tural Sciences,” Communications on Pure and Applied Mathematics XIE (1960), pp. 1-14

See also Vern Poythress, “A Biblical View of Mathematics,” in Gary North (ed.),
Foundations of Christian Scholarship, op. cit., ch. 9. Sce alyo his essay iv The Journal of
Christian Reconstruction, 1 (Sumer 1974).
