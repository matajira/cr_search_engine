Proportional Restitution 517

that was inherently filled with uncertainty for the buyer. The latter
might have been convicted of being a “fence”—a professional re-
ceiver of stolen goods. He has therefore been defrauded by the thief.
He deserves restitution.

What if the original owner says that he does not want to buy the
beast from the defrauded buyer? The buyer has now in effect pur-
chased the beast from its rightful owner. He now owns the “bundle of
rights” associated with true ownership. But the thief has nevertheless
exposed him to the discomfort of being involved in an illegal transac-
tion. Shouldn't the thief still owe the seller a 100 percent restitution
payment? My assessment of the principle of victim’s rights leads me
to conclude that biblical law does in principle allow the defrauded
buyer to come to the judges and have them compel the thief to pay
him 100 percent of the price he had paid the thief. This has nothing
to do with whether he bas sold the beast to the original owner or
whether the owner has allowed him to retain legal possession of it.

Transferring Lawful Title

Why must we regard the sale of the animal as fraudulent? Why
can the authorities legitimately demand that the purchaser return
the animal to the original owner? Because the thief implicitly and
possibly explicitly pretended to be transferring an asset that he did
not possess: lawyul title. The thief did not possess lawful title to the
property. This illuminates a fundamental principle of biblical owner-
ship: whatever someone does not legally own, he cannot legally sell. Ownership
is not simply possession of a thing; it is possession of certain fegal in-
munities associated with the thing. It involves above all the right to ex-
clude. Writes economist-legal theorist Richard Posner: “A property
right, in both law and econornics, is a right to exclude everyone else
from the use of some scarce resource.” This right to exclude was
never owned by the thief; therefore, he cannot transfer this bundie of
legal immunities to the purchaser. The purchaser can legally demand
compensation from the thief, but he does not lawfully own the stolen
item. The civil authoritics can legitimately compel the buyer to trans-
fer the property back to the thief, who then returns it to the original
owner, or else compel him to return it directly to the original owner.

The explicit language of the kidnapping statute provides us with
the legal foundation of this conclusion regarding the transfer of own-

29. Richard A. Posner, The Economics of Justice (Cambridge, Massachusetts:
Harvard University Press, 1983), p. 70.
