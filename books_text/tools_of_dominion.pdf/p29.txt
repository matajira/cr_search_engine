Introduction 21

Testament law and pro-natural law philosophy.** (It is unfortunate
that both Cornelius Van Til and Francis Schaeffer were inconsistent
in this regard: they ignored or denied biblical law, yet also officially
denied natural law philosophy. This has produced great confusion
among their respective followers. )#

For two centuries, humanists in the United States have been en-
listing Christian evangelicals into a seemingly endless stream of
“save the world” programs. The humanists cry out, “Baptize us!
Baptize us! . . . and please take up a compulsory collection for us.”
For two centuries, well-meaning Christians have been digging deep
into their wallets in order to supply the tax collectors with funds to fi-
nance a series of supposedly religiously neutral social reform pro-
grams that have been created by the messianic State and staffed by
humanist bureaucrats. Taxpayer-funded, evolution-teaching gov-
ernment schools have been the most persistent, effective, and repre-
sentative example of this continuing delusion. Without the spurious
supporting doctrine of morally and intellectually neutral natural
law, it would not be possible for the humanists to wrap these anti-
Christian programs in the ragged swaddling clothes of common
morality.

45. Norman Geisler, “A Premillennial View of Law and Government,” in J. L.
Packer (ed.}, The Rest in. Theology (Carol Stream, Ilinois: Christianity Today/Word,
1986). Writes the Fundamentalist Journal (Sept. 1988): “Geisler credits [Thomas]
Aquinas with ‘having the most influence on my life,’ and says that if his house were
burning he would grab his wife, his Bible, and the Summa Theologiae by Aquinas” (p.
20). It is hardly surprising that he should be a professor of philosophy at Baptist fun-
damentalist Liberty University. The anabaptists, who possess no separate philoso-
phical tradition of their own, have always relied on the philosophy of medieval
Roman Catholic scholasticism to defend their cause.

46, See North, Political Polythessm, chapter 2; “Halfway Covenant Ethics," and
chapter 3: “Halfway Covenant Social Criticism.” Van Til’ self-conscious rejection of
both dispensationalism and natural law theory left him without any concept of social
law or social justice, for he also rejected the continuing authority of the Old Testa-
ment case laws—by silence in his published writings and explicitly in private com-
munications. Thus, his system was always incomplete, hanging timelessly in the air
like a ripe fruit that has just begun its fail to the ground. That the fruit was grabbed
by R. J. Rushdoony in the carly 1960's did not please Van Til, but there was not
much that he could politely do about it. He had to remain silent, for his system is in-
herently ethically silent; it rejects both forms of law, natural and biblical, which is
why he explicitly denied ethical cause and effect in history, and why he implicitly
adopted the humanists’ version of ethical cause and effect: the good guys lose in his-
tory, and the bad guys win.
