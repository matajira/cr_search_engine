648 TOOLS OF DOMINION

ingness of the prospective bridegroom to work hard to earn what for
him would be a large sum, but which would nevertheless be a pit-
tance for the father, This was the problem David faced (I Sam.
18:23). The bride price was, first, a ritual sign of subordination; sec~
ond, it was a screening device for the girl’s parents; and third, it was
a means of compensating the girl’s family for the expense of the
dowry. The first two aspects were more important than the third.
Thus, a fixed bride price was not set by biblical law. The existence of
its requirement was far more important than the actual money in-
volved, with only two judicial exceptions: the case of seduction (Ex.
22:16-17) and the case of accused harlotry (Deut. 22:13-19).

Seduction

Let us consider the case of seduction. There is no doubt that the
father, under the jurisdiction of the judges, was allowed to establish a
bride price requirement for the seducer, and even prohibit the mar-
riage after having collected it. Obviously, only the State could have
lawfully enforced such a penalty.

When the State enters the picture to enforce a private decision,
there must be upper limits on the punishment if liberty under pre-
dictable law is to be preserved. At the same time, the penalty must
be high enough to deter the immoral behavior. Thus, the maximum
bride price that could be imposed by the father with the consent of
the judges could and would be different from normally negotiated
bride prices. We know what that upper limit was: 50 shekels of silver.
T call this compulsory maximum the formal bride price, in contrast to
the normal or negotiated bride price, in which the State was not in-
volved. It is specified in Deuteronomy 22:28-29:

Ifa man find a damsel that is a virgin, which is not betrothed, and lay
hold on her, and lie with her, and they be found; then the man that lay with
her shall give unto the damsel's father fifty shekels of silver, and she shall be
his wife; because he hath humbled her, he may not put her away all his
days.

The formal bride price of 50 shekels of silver specitied here was
far higher than the common dowry in Israel. This was a great deal of
money. It was not required of every suitor. The Old Testament did
not establish a fixed price so high that only a few women could have
become wives, with most of them being forced by a government-
imposed price floor to settle for status as concubines (wives without
