27
SABBATICAL LIBERTY

Also thou shalt not oppress a stranger: for ye know the heart of a
stranger, seeing ye were strangers in the land of Egypt. And six years thou
Shalt sow thy land, and shalt gather in the fruits thereof: But the seventh
jear thou shalt let it rest and lie still; that the poor of thy people may eat:
and what they leave the beasts of the field shall eat. In like manner thou
shalt deal with thy vineyard, and with thy oliveyard. Six days thou shalt
do thy work, and on the seventh day thou shalt rest: that thine ox and
thine ass may rest, and the son of thy handmaid, and the stranger, may be
refreshed (Ex. 23:9-12),

It is not immediately clear how these verses of the Bible should
be linked together, because they appear to be separate units. The re-
ference to the stranger in verse 9 seems to be linked to the reference
to the stranger in verse 12. In between are laws relating to the sab-
bath: the sabbath year rest of the land, and the sabbath day rest of
domestic animals, servants, and strangers, The question needs to be
raised: Why should the section on sabbath rest begin and end with
references to the stranger?

To answer this, we need to discover the theocentric principle of
the passage: God as Deliverer or Liberator, James Jordan has argued
that the theme of the Book of Exodus is God’s deliverance of His peo-
ple from bondage to sabbath rest. “The instructions for the design of
the Tabernacle culminate in sabbath rules (31:12-17), and the pro-
cedure for building the Tabernacle commences with sabbath rules
(35:1-3). The book closes with the definitive establishment of Old
Covenant worship on the very first day of the new year. Thus, the
book moves from the rigors of bondage to the sinful world order, to
the glorious privilege of rest in the very throne room of God.”! I

i. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984), p. 75

Bul
