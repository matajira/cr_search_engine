The Ability to Teach 925

They experienced this splendor as representative agents of the nation.
Nevertheless, everyone who read the account in Exodus knew what
was inside. The people were called upon to visualize this splendor
whenever they heard this section of the Word of God. They learned
of a God who enjoys splendor for His own sake.

Man is made in God’s image. Why, then, shouldn’t a person en-
joy the beauties of art for his own sake? Christian art and architec-
ture do not have to serve the needs of State in order to be legitimate.
Art must please God, but in a free society, God’s delegated aesthetic
agents are the patron and the artist, not anyone else. The very fact
that the interior of the tabernacle had to be visualized by most
Hebrews must have called forth the creative imaging process in the
minds of artists.

They needed teachers, The students gained confidence in their
ability to build. This gave them confidence concerning the future.
They would not be dependent on the architectural capital of the
Canaanites after the conquest. They would not be forced to live in
the shadow of a rebellious culture’s greatness. Apprentices now were
present in the Hebrew commonwealth who had been given direction
by master teachers who had been filled by God with the spirit of
competence. The nation would not be forever dependent on the continuing
miracles of architectural revelation and Spirit-filled craftsmen.

Men need self-confidence if they are to perform difficult tasks. If
the two master craftsmen had been unable to irapart their skills to
others, then the society would have been aesthetically dependent on
the one-time creation of two God-endowed men whose skills might
not appear again. The Hebrews would then have lived in the fear of
becoming aesthetic slaves to their experience in the wilderness, unable
to take a progressive culture across the face of the globe in confidence.

Once the tabernacle was built, men who were recently trained in
creative architecture could pass these skills dawn to their successors.
This would not be easy in a wilderness. The locus of artistic creativ-
ity would have to be personal and local. Essentially, the source of de-
mand must have been familial or tribal. The small scale of artistic
creativity must have decentralized craftsmanship. This is one reason
why we find no examples of magistcrial artistry in the archeological
digs of Israel.

Another reason was covenantal: they kept rebelling against God,
and God kept delivering them into the hands of their enemies. There
was a constant dispersion of Hebrew wealth out of the land. The dis-
