166 TOOLS OF DOMINION

means “wheel”; !25 in the context of Joshua’s invasion, it meant some-
thing very specific: “And the Lorp said unto Joshua, This day have I
rolled away the reproach of Egypt from off you. Wherefore the name
of the place is called Gilgal unto this day” (Josh. 5:9). By hacking
Agag to pieces, Samuel rolled from Israel the reproach to God of
Saul’s “merciful” disobedience regarding Agag and Amalek’s animals,

While there is always a need for biblical disputations and clarifi-
cations regarding the proper New Testament application of any Old
Testament text, there is no room for debate concerning the innate
morality of the total annihilation of the Canaanites by the people of Israel,
just as there is no debate regarding the morality of God's destruction
of humanity by means of a great flood. This was morally required of
them by God. To have done anything less would have been rebellion
against God. Saul lost his kingship because he did less.

Why should this bother modern Christians? Because they do not
want to face the inescapable biblical reality of a God who has already sent to hell
the vast majority of all people who have ever lived. The doctrine of eternal
damnation in the lake of fire (Rev. 20:14-15) embarrasses them, and
they are also embarrassed by the far milder doctrine of God’s historic
judgments against nations. Such historic destruction is simply a
minimal down payment by God to those who are eventually going to
be ushered into the fires of eternal damnation.

Ts there no mercy in hell? Not a speck. Was there no mercy for
Canaanites? Only the mercy of perpetual slavery, and even this
highly limited mercy was shown only to the Gibeonites, and only be-
cause they successfully tricked Joshua (Josh. 9). They should have
been destroyed, but Joshua did not enquire of God before he made a
covenant with them (Josh. 9:14-15).

Slavery and Hell

The doctrine of perpetual slavery is nothing special when com-
pared to the doctrine of eternal damnation. In fact, perpetual slavery
is an institutional testimony to the reality of eternal damnation. It
should direct the slave’s attention to the fate of his eternal soul. (It
should also direct the master’s attention to the same issue.) Slavery
was designed by God to be a means of evangelism in the Old Testament, The
question can therefore legitimately be raised: Is it a means of evan-
gelism in New Testament times? For instance, why did Paul send

125. James Strong, “Hebrew and Chaldee Dictionary,” in The Exhaustive Concor-
dance of the Bible (New York: Abington, [1890} 1961), p. 27, Nos. 1534-37.
