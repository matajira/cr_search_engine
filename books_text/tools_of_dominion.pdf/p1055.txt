Maimonides’ Code: Is It Biblical? 1047

They chose instead to allow Jews to be governed by their own courts
in most matters that involved disputes between Jews. Of course,
when it came to Christian rulers (and presumably also private citi-
zens) who defaulted on loans, the Jews may also have occasionally
appreciated the walls that protected them from excessive contact
with gentiles. 1 (It is also interesting that in the twelfth century, the
walled-in Jewish community of Constantinople also had its own wall
that separated the 2,000 Talmudic Jews from the 500 anti-Talmudie,
“Torah-only” Karaitcs.}'©

Forced social division is inevitably the curse of a double legal
standard in a single society. Neither group trusts the other; both
groups seek to exploit the other, or at least tolerate those within their
midst who do. This is why the Bible says, “Onc law shall be to him
that is homeborn, and unto the stranger that sojourneth among you”
(Ex. 12:49). This case law appears in the section on the laws regard-
ing strangers and the Passover, it was given to Israel immediately
afier the exodus itself. This indicates how emphatically God de-
mands that men observe it: even their oppressors, the Egyptians, are
entitled to equal treatment before the law.

“For the Sake of the Peace”

The rabbis were not fools, of course. They modified this judicial
double standard for practical purposes, namely, “for the sake of the
peace.” Horowitz explains: “Halakot [law] and customs which dis-
criminated against Gentiles and which might, therefore, appear un-
just in the cyes of the world, were not to be enforced or practiced
though perhaps ‘legally’ valid, because it might reflect unfavorably
on the Jewish people, its morals and its religion. ‘For the Sake of
Peace’ was in effect an equitable principle which modified the strict
law, with regard to treatment of Gentiles.”!%! This was a belated rec-
ognition of the need for a unified legal standard in civil justice and
economic dealings. He offers several examples, including this one:
“The Talmud seemed definitely to countenance the over-reaching of

159, In 1306, Philip IV of France evicted the Jews, repudiated his debts to them,
and confiscated their property. England drove them out in 1290, after having tax
them heavily and soaked up their capital with forced loans that were then repudi-
ated. In 1370, they were driven from the low countries. Herbert Heaton, Econumic
History of Eurape (New York: Harper & Row, 1948), p. 184.

160. This was recorded by Benjamin of Tudela in his Bouk of Travels (1168); cited
in Johnson, [History of the Joos, p. 169.

161. Horowitz, Spirit of fewish Law, p. 100.

 
