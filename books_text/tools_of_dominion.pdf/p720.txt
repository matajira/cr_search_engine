712 TOOLS OF DOMINION

shall not reign over thee” (Deut. 15:5-6). Notice also that the means
of exercising this rule is through extending them credit.

This is a very significant covenantal cause-and-effect relation-
ship. If a nation is characterized by the willingness of its citizens to
loan money, interest-free, to their poverty-stricken neighbors, in-
cluding resident aliens, the nation will eventually extend its control
over others by placing them under the obligation of debt. “The rich
ruleth over the poor, and the borrower is servant to the lender” (Prov.
22:7). This is why it was legal to take interest from the foreigner who
was living outside the land. It was a means of subduing him, his
family, and his God-defying civilization. It was (and is) a means
of dominion.

Moral Compulsion

Because these charitable loans were supposed to be cancelled in
the seventh year, the national sabbatical year, there was an obvious
temptation to refuse to make such loans as the sabbatical year ap-
proached. God recognized this temptation, and He warned against it.

If there be among you a poor man of one of thy brethren within any of
thy gates in thy land which the Lorp thy God giveth thee, thou shalt not
harden thine heart, nor shut thine hand from thy poor brother: But thou
shalt open thine hand wide unto him, and shalt surely lend him sufficient
for his need, in that which he wanteth. Beware that there be not a thought
in thy wicked heart, saying, The seventh year, the year of release, is at
hand; and thine eye be evil against thy poor brother, and thou givest him
nought; and he cry unto the Logp against thee, and it be sin unto thee.
Thou shalt surely give him, and thine heart shall net be grieved when thou
givest unto him: because that for this thing the Lorp thy God shall bless thee
in all thy works, and in all that thou puttest thine hand unto (Deut. 15:7-10).

This indicates that God placed a moral obligation on the heart of
the more successful man. He was supposed to lend to his neighbor.
But this was not statute law enforceable in a civil court. God would
be the avenger, not the State.

The context of the obligatory loan of Deuteronomy 15, like the
zero-interest loan of Exodus 22:25-27, is poverty. There will be poor
people in the promised land, Moses warned. Because of this, these
special loans are morally mandatory. There must be a year or
release, “Save when there shall be no poor among you; for the Lorp
shall greatly bless thee in the land which the Lorp thy God giveth
