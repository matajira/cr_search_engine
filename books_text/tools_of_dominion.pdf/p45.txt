The Restoration of Biblical Casuisiry 37

scholar, I. Grunfeld, when he writes that “true religion and true
civilisation are identical. It is the view of the Torah as the civilisation
of the state of God—where Torah is coextensive with life in all its
manifestations, personal, cconomic, political, national.””

Nothing less than this comprehensive replacement of humanism and
occultism with Christianity will suffice to please God. We are called
to work for the progressive rcplacement of humanist civilization by
Christian civilization, a replacement that was definitively achieved
with the death, resurrection, and ascension of Jesus Christ, and
manifested by the coming of the Holy Spirit at Pentecost. We are to
replace Satan’s humanistic kingdoms. “Kingdom” is an inescapable
concept. It is never a question of kingdom vs. no kingdom; it is
always a question of whose kingdom. Rushdoony is correct in his
evaluation of mankind's inevitable quest for utopia, the final order,
which only God can inaugurate and bring to pass: “Che church ac-
cordingly has never been alone in history but has rather faced a
toultiplicity of either anti-Christian or pseudo-Christian churches
fiercely resentful of any challenge to their claim to represent the way,
truth and life of that final order. The modern state, no less than the
ancient empire, claims to be the vehicle and corporate body of that
true estate of man. As the incarnation of that final order, it views
family, church, school and every aspect of society as members and
phases of its corporate life and subject to its general government. It

 

agree with Geerhardus Vos’ statement: “While thus recognizing that the kingdom of
God has an importance in our Lord's teaching second to that of no other subject, we
should not go to the extreme into which sume writers have fallen, of finding in it the
only theme on which Jesus actually taught, which would imply that all other topics
dealt with in his discourses were to his mind but so many corollaries or subdivisions
of this one great truth. . . . Salvation with all it contains flows from the nature and
subserves the glory of God. . . .” Geerhardus Vos, The Teaching of Jesus Concerning the
Kingdom and the Church (Grand Rapids, Michigan: Eerdmans, 1958), p. ll. Tam say-
ing only that the kingdom of God is inherently all-encompassing culturally. In fact, I
am convinced that the best biblical definition of “kingdom” is civilization. The king-
dom of God is the civilization of God—internal, external, heavenly, earthly, histori-
cal, and cternal,

23, Grunfeld, “Samson Raphael Hirsch—the Man and His Mission,” Judaism
Eternal, I, p. xiv. Obviously, I do not agree with Granfeld’s next sentence: “This con-
cept is applicable, of course, only when there is a Jewish State, or at least an autono-
mous Jewish Society, which can be entirely ruled by the Torah.” This staternent pro-
vides evidence of the accuracy of Vos’ analysis of Jewish teaching concerning the
Kingdom of heaven: “The emphasis was placed largely on what the expected state
would bring for Israel in a national and temporal sense. Hence it was preferably
thought of as the kingdom of Israel over the other nations.” Vas, Kingdom and the
Church, p. 19.
