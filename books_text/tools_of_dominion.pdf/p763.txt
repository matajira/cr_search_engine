The Prohibition Against Usury 755

the collateral. The borrower feels better about owning the collateral
and paying the money. Neither is a servant; neither is a master.®?

Conclusion

The confusion throughout the Middle Ages and early modern
period concerning the evil or illegitimacy of interest came as a result
of not paying attention to the biblical texts, and then mixing in the
fallacious economic opinions of Aristotle. The Bible is clear: there is
to be no interest return from money loaned to the poverty-stricken
neighbor. This applies to money loans or loans of goods. But the
definition of poverty must be the willingness of the borrower to serve
as a bondservant of the lender should he be unable to repay the loan.
The larger the loan, the longer the term of service that will be re-
quired to repay it. Ordinarily, though, charity loans would be small,
and the time to repay would probably not be seven years, unless it
was for something like the payment of physicians’ bills or lawyers’
fees.

There is no prohibition on interest returns from loans to distant
pagans or from business loans. The term translated as “usury” in the
King James Bible is narrow and precise in its application: interest de-
rived from morally mandatory charity loans, either from poverty-stricken righteous
brothers in the faith or from resident aliens who live alongside believers in nations
that are formally covenanted under the God of the Bible. The word does not
mean “exorbitant” interest. That usage was the product of the early
modern period, and is not the product of biblical analysis, Any interest
taken from a loan to the poor brother in the faith is usurious; no maxi-
mum rate of interest from other loans is ever mentioned in the Bible.

Interest is inescapable. It is not a uniquely monetary phenome-
non. It is the discount we apply to future goods as against present
goods. This process goes on continually, whether or not there is a
money market, whether or not published loan rates are available. We
are mortal, We die. We live in an uncertain world. We cannot know
the future. Thus, we discount the value of future goods, and we also
confront the phenomenon of risk whenever we defer present consump-
tion. If nothing else, we may not live long enough to enjoy the future.

Fractional reserve banking is prohibited in the Bible, for two
reasons: 1} it violates the prohibition against false weights and mea-

82. Warning: do not take a loan if it is not 100 percent collateralized by an asset
you are willing to fose.
