A Biblical Theology of Slavery 171

the lake of fire. Yet the vast majority of today’s Christians become
embarrassed and begin to squirm uncomfortably when they are
challenged by pagans (or Christian college professors in humanist-
accredited liberal arts colleges) to explain and defend Old Testament
stories of God’s earthly cursings. Squirming should be the response
of the covenant-breakers who hear of God’s impending wrath, not
the response of Christians who are seeking to obey the Bible.

The doctrine of the lake of fire should not embarrass any Bible-
believing Christian; neither should anything else the Bible says that
God did, does, or will do. God’s warning to us is clear: “Wherefore,
the Lorn God of Israel saith, I said indeed that thy house, and the
house of thy father, should walk before me forever: but now the
Lorp saith, Be it far from me; for them that honour me I will
honour, and they that despise me shall be lightly esteemed” (I Sam.
2:30). Let us avoid being “lightly esteemed” by God; it is better to be
despised by men—even by professors in fully accredited Christian
colleges. '35

If you have already accepted the doctrine of eternal punishment,
then you have accepted the doctrine which above all other Christian
doctrines outrages non-Christians. They do not want to think about
it, which is understandable. The doctrine of eternal damnation is the
ultimate offense of Christianity in the opinion of those who reject
Christianity. This is why it is the first doctrine to be abandoned by
covenant-breakers and heretics.!*4 So, there is little reason for
Christians to get embarrassed about other aspects of the Bible that
the pagan world will taunt us with, since it is the doctrine of hell that
really offends covenant-breakers, and we cannot give up this doc-
trine without accepting some version of an ancient heresy. In any
case, ridicule by pagans is a temporary phenomenon; they will not
be taunting us in eternity. They will be too busy being permanently
offended by God in the lake of fire.

But if a Christian for some reason ts embarrassed by the doctrine
of hell, then he is not ready to get involved in the fight against hu-
manism, or any other Bible-mandated fight. He must first know
what he has been delivered from before he can bring the message of sal-

133. On the leftward drift into humanism by professors in neo-evangelical Chris-
tian colleges, and the theological liberalism and even apostasy that they produce in a
significant percentage of their students, sce James Davison Hunter, Evangelicalism:
The Coming Generation (University of Chicago Press, 1987), pp. 165-80.

134, CLD. P. Walker, The Dectine of Hell: Seventeenth-Century Discussions af Eternal
‘Torment (University of Chicago Press, 1964), Part Ii.
