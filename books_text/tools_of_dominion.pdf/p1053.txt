Maimonides’ Code. Is ft Biblical? 1045

course of centuries, that offer the most striking manifestation of the
true, the humane spirit of Jewish law.” '5! But is this really true? Was
the “humaneness” of the Jewish legal order truly increased when the
rabbis departed from the letter of Old Testament law? I argue that
the self-conscious departure on the part of both Christians and Jews
from the revealed law of God has decreased the West’s humaneness.

The question I am raising in this essay is this: Does the Code rep-
resent the spirit of the Old Testament? As we shall sce, it clearly does
not represent the letter of the Old Testament. But were Maimonides
and the Talmudic scholars whose conclusions he summarized and
classified able to retain and make practical the spirit of the Mosaic
law? My answer is simple: o. But I must prove my case. To provide
evidence of my assertion regarding Jewish law, I have decided to pro-
vide a kind of lawyer’s brief against Moses Maimonides — specifically,
against his views of restitution to gentile victims by Jewish criminals.

The Double Standard

Maimonides insisted that biblical law’s gencral requirement that
the thief make two-fold restitution to his victim (Ex. 22:7) applies
only in the case of Jews who steal from Jews. It does not apply if a
Jew steals from a heathen (gentile). Incredibly, it also does not apply
in the case of sacrilege: stealing an animal from a Jewish household
if the animal has been set aside for sacrifice to God; the thief is ex-
empted from making two-fold, four-fold, or five-fold restitution,
“For Scripture says, And it be stolen from the house of the man (Ex. 22:6),
but not from the house of the sanctuary.”*? This means that it is less
of a crime to steal from God than to steal from man—a strange sys-
tem of ethics on which to build an explicitly theocentric civilization.

A convicted Jew need not pay double restitution to a gentile,
cither: “If one steals from a heathen, or if one steals sacred property,
he need pay only its capital value, for Scripture says, Shall pay double
to his neighbor (Ex, 22:8)— to his neighbor, but not to the sanctuary; fo
his neighbor, but not to a heathen.”15%

151, Horowitz, Spirit of Jewish Law, pp. 1-2. This reflects a view quite similar te
that expressed by Lauterbach in his criticism of Sadduceeism because of its having
become “blind slaves of the law without regard for its spirit. It divéreed the law from
life, in that it made the two absolutely independent of each other” Jewish Essays, p, 38.

152. Moses Maimonides, The Book of Torts, Book I of The Code of Maimonides, 14
vols. (New Haven, Connecticut: Yale University Press, 1954), “Laws Concerning
Thett,” Chapter Two, Section One, p. 64.

153, Idem,
