46 TOOLS OF DOMINION

Economic Restitution

A considerable percentage of this book is devoted to a defense of
the biblical concept of penal restitution. Convicted criminals are sup-
posed to make restitution payments to their victims. This “revolutionary”
idea is at last being taken seriously by a few judges in the United
States.*? But behind the ability of today’s civil courts to impose the
sanction of restitution lies a greater threat to the criminal: imprison-
meni. This is the “dirty little secret” of those atheists, pictists, and
antinomians who ridicule the biblical system of slavery: they have
accepted the horror of unproductive imprisonment in place of the
biblical institution of penal labor servitude, out of which an in-
dustrious slave could purchase his freedom. If the criminal in an-
cient Israel was financially unable to pay his victim, his sale to a
slave-buyer was what provided the victim with his lawful restitution
payment, The prison system has always been the Bible-hater’s
preferred substitute for the Old Testament’s system of law-restricted
labor servitude. In short, in order to enforce the Bible’s principle of
economic restitution to victims by criminals, there always has to be a
more fearful support sanction in reserve: death, imprisonment,
whipping, banishment, or indentured servitude. But only one of
these reserve sanctions raises money for the victims: indentured ser-
vitude, The critics of biblical law just never seem to remember to
mention. this fact.

The Fear of God’s Law

This hatred of God’s law has affected millions of Christians who
sing the old hymn, “O How Love I Thy Law.” Even when they do
not actively hate it (and most do), they are simply afraid of God’s
law. They have not studied it, and they have been beaten into intcl-
lectual submission by humanists, Christian antinomians, and those
who fear personal and cultural responsibility.

A discouraging example of this is Dr. James Dobson, whose books,
films, and daily radio broadcasts on Christian family issues have
inspired millions of Americans, and who by 1988 had become the
Protestant evangelical leader in the United States with the largest

43. For example, Lois G. Forer, Criminals and Victims: A Trial Judge Reflects on
Grime and Punishment (New York: Norton, 1980).
