768 TOOLS OF DOMINION

With Justice for All

The court is to be a placc of justice for all men, without respect to
their economic position, Bearing false witness is described in Exodus
23 as being an aspect of oppression. The innocent are to be protected
(v. 7), bribes are to be rejected by judges (v. 8), and the stranger is
not to be oppressed (v. 9). When men can have reasonable faith in
the content of the law and the reliability of the judges, they can
cooperate with each other less expensively. The division of labor in-
creases, along with voluntary exchange. Productivity increases
throughout the society. The “miracle of the market,” with its benefits
to all individuals who serve their neighbors by responding efficiently
to consumer demand, becomes so familiar to the beneficiaries that
they may forget the source of their blessings: God and His law-order.

A society that is filled with envy-driven false witnesses who
“uphold the cause of the poor” by means of courtroom lies, university
indoctrination, guilt-manipulation from the pulpit, and orchestra-
tion of the public by the mass media, is a doomed society if it con-
tinues in its rebellion. ‘I'he self-righteousness of the envious will not
alter the reality of the economic effects of envy. All the rhetoric about
“healing unjust social structures” and “providing justice for the op-
pressed” will not delay the judgment of God if the content of the
promised utopian reformation is founded on the politics of envy.*?

By perverting judgment, men tear down the foundation of their
liberties and the foundation of their wealth, especially their freedom
to profit trom their own ingenuity, labor, and thrift. They find that
others are increasingly hesitant to display visible signs of their pros-
perity. Economic prosperity cannot survive when productive mem-
bers of a society withdraw from entrepreneurial activities—the un-
certainty-bearing, future-oriented, consumer-satisfying quest for
profit—and instead become content to consume their wealth (and
hide it) rather than face the slander of false witnesses who rise up
against them in the name of the poor.%!

Justice and Productivity
As capital, including human capital, is steadily withdrawn from

30. Gonzalo Fernandez de la Mora, Egalitarian Envy: The Political Foundations of
Justice (New York: Paragon House, [1984] 1987).

31. Helmut Schoeck, Enoy: A Theory of Social Behavior (New York: Harcourt,
Brace, [1966] 1970), pp. 46-47, 88, 290-91.
