Vietim’s Rights vs. the Messianic State 297

prosecution of a covenant lawsuit. (This is the judicial basis of what
in English common law is known as “citizen’s arrest,” although it is
seldom invoked today.) This is why the State can lawfully compel
honest testimony from a witness: the witness is under the authority
of the State. It is in fact unlawful to withhold evidence of a crime
when subpoenaed. While the State may offer a reward for the cap-
ture and conviction of a criminal (a positive sanction: blessing), this
is at the discretion of the State. The witness who seeks an announced
reward has a claim on the State, not on the criminal.

The most important example in history of a reward-seeking wit-
ness is Judas Iscariot, who collected 30 pieces of silver from the
Jewish court to witness against Jesus Christ. He later returned the
money, not because it is inherently wrong to accept money as an
honest witness, but because he knew he had been a false witness in a
rigged, dishonest trial. The Jewish leaders self-righteously replied,
“What is that to us?” (Matt. 27:4b). They felt no sense of guilt, so
why should he? They also recognized the tainted nature of the
money, which was the price of blood, and as true Pharisees, they
refused to accept his repayment (Matt. 27:6). Committing murder
by rigging a court was irrelevant in their view, a means to a legiti-
mate end; getting paid for false witness-bearing, however, was seen
by them asa sin. This is the essence of Pharisaism, the classic histor-
ical example of Pharisaism in action. They were happy to serve as
the most corrupt court in man’s history, but they judiciously refused
to accept money for their efforts. (What is not recognized by most
Christian commentators is that the testimony of a witness in a
Hebrew court was invalidated, at least by the law of the Pharisees, if
he had received payment for testifying.)?

What is my conclusion? Only that witnesses have no legal claim
on the criminal, The authorized agents of God in the prosecution of
a covenant lawsuit are officers of one of the three courts— church,
State, and family—and the victim of the crime.

 

The Right of Refusal

If the authorized biblical penalty is economic restitution, then
the victim whose covenant lawsuit is successfully prosecuted by the
civil government has the right to refuse payment, or the right to take
less than what biblical law authorizes. Like the creditor who has the

23, Bekhoroth 4:6, in The Mishnah, edited by Herbert Danby (New York: Oxford
University Press, [1933] 1987), p. 534.
