Maimonides’ Code. Is Tt Biblical? 1017

Or: “You have heard it said that a homosexual who seduces a boy under
the age of nine need have no guilt, while others have argued that age three
is the minimum, but I say unto you that anyone who doves this should be
executed, as required by biblical law.”

Did you read the footnotes? This is only the beginning, but it should
be sufficient. You now recognize that the Talmud is not a conven-
tional commentary on the Old Testament, although with certain key
New Testament concepts missing. On the contrary, the Talmud’s
contents are only peripherally related to the Old Testament. The
Talmud is a giant exercise in finding ways to escape the Old Testa-
ment texts. The Pharisees were in rebellion against God's law, all in
the name of God’s law. This was Jesus’ assertion from the beginning:

Woe unto you, scribes and Pharisees, hypocrites! for ye compass sea
and land to make one proselyte, and when he is made, ye make him twofold
more the child of hell than yourselves. Woe unto you, ye blind guides, which
say, Whosoever shall swear by the temple, it is nothing; but whosoever shall
swear by the gold of the temple, he is a debtor! Ye fools and blind: for whether
is greater, the gold, or the temple that sanctificth the gold? And, Whosocver
shall swear by the altar, it is nothing; but whosoever sweareth by the gift
that is upon it, he is guilty. Ye fools and blind: for whether is greater, the
gift, or the altar that sanctifieth the gift? Whoso therefore shall swear by the
altar, sweareth by it, and by all things thereon. And whoso shall swear by
the temple, sweareth by it, and by him that dwelleth therein. And he that
shall swear by heaven, sweareth by the throne of God, and by him that sit-
teth thereon. Woe unto you, scribes and Pharisecs, hypocrites! for ye pay
tithe of mint and anise and cummin, and have omitted the weightier mat-
ters of the law, judgment, mercy, and faith: these ought ye to have done,
and not to leave the other undone. Ye blind guides, which strain at a gnat,
and swallow a camel. Woe unto you, scribes and Pharisees, hypocrites! for
ye make clean the outside of the cup and of the platter, but within they are
full of extortion and excess. Thou blind Pharisee, cleanse first that which is
within the cup and platter, that the outside of them may be clean also. Woe

67. “Rab said: Pederasly with a child below nine years of age is not deemed as
pederasty with a child above that, Samuel said: Pederasty with a child below three
years is not treated as with a child above that.” Babylonian Talmud, Sanhedrin 54b.
The modern commentator’s note explains: “Rab makes nine years the minimum;
but if one committed sodomy with a child of lesser age, no guilt is incurred. Samuel
makes three the minimam.” Rab is the nickname of Rabbi Abba Arika (175?-247
a.v.), the founder of the Jewish academy in the Persian city of Sura [Sora], one of
the three great Jewish academies in Persia. Samuel was Mar-Samuel (180-257 4.0.),
Rab’s contemporary and fellow teacher at Sura, a master of Jewish civil law. See
Heinrich Gractz, History of the Jews, TL, pp. 512-22.

 
