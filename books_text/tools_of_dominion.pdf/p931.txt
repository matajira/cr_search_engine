The Ability to Teach 923

cities, temples, and royal monuments of Pharaonie Egypt and an-
cient Mesopotamia. No one who has ever observed the city gates
and walls of a Chinese capital, such as Peking, or who has walked
through the immense palace gates and squares of the Forbidden City
to enter the equally immense court buildings, ancestral temples, and
private residences can fail to be awed by their monumental design.
Pyramids and dome-shaped tombs manifest most consistently the
monumental style of hydraulic building. They achieve their aesthetic
effect with a minimum of ideas and a maximum of material. The
pyramid is little more than a huge pile of symmetrically arranged
stones.”7

In contrast to the pyramid was the tabernacle. It was ornate and
magnificent inside (for the priests to view), but it was not monumen-
tal. It was transportable. Its builders were wilderness wanderers.
There was no possibility of pyramid-building for the Hebrews in the
wilderness. The tabernacle’s grandeur was visual, but this grandeur
was based upon the creation of a sense of subordination in those few
who entered it. God laught the Hebrews a scnsc of awe, but this
sensc of awe was based on God’s aciual presence in the tabernacle,
not on its shape. The closer they came to the center, the more awe-
some it became, and only priests were able to get close to the holy of
holies, and only the high priest could enter it. Take away God’s pres-
ence, and the tabernacle became a large, ornament-filled tent; it lost
its awesome quality.

‘The great Cheops pyramid of Egypt is empty and awesome, but
hardly beautiful. Its awesomeness is based on its height and immen-
sity, not its communicated sense of God's presence. The tabernacle
required constant care, meaning constant devotion; the pyramids
stand unattended, monuments to the static civilization that they rep-
resented. They have always served as giant graveyard monuments.

The massive, monumental architecture of Egypt had glorified
the State and the static social pyramid. It had inspired the wrong
kind of awe. It had been designed by tyrants and built by slaves. The
rulers of Egypt paid for such architecture but had not participated in
its construction.

The “empire” architecture of almost every national capital—
Washington, D.C., the Kremlin, Nazi Berlin—is easily recog:

7, fhid., pp. 43-44,
8. Gary North, Moses and Pharaoh: Dominion Religion us. Puwer Religion (Tylec,
Texas: Institure for Christian Economics, 1985), ch. 2: “Imperial Bureaucracy.”
