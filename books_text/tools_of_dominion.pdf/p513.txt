7
PROPORTIONAL RESTITUTION

Ifa man shall steal an ox, or a sheep, and kill it, or sell it; he shall
restore five oxen for an ox, and four sheep for a sheep. . . . If the theft be
certainly found in his hand alive, whether it be ox, or ass, or sheep; he
shall restore double (Ex. 22:1, 4).

In any attempted explanation of a Bible passage, we must have
as our principle of interpretation the Bible’s revelation of the theo-
centric nature of all existence. God created and now sustains all life.
Thus, a sin against a person is first and foremost a sin against God.
Restitution must always be made to God. God demands the death of
the sinner as the only sufficient lawful restitution payment. But God
allows a substitute payment, symbolized in the Old Testament econ-
omy by the sacrifice of animals. These symbols pointed forward in
time to the death of Jesus Christ, which alone serves as the founda-
tion of all of life (Heb. 8). Jesus Christ made a temporary restitution
payment to God in the name of mankind in general (temporal life
goes on) and a permanent one for His people (eternal life will
come).! Adam deserved death on the day he rebelled; God gave him
extended life on earth because of the atonement of Christ. The same
is true for Adams biological heirs. We live because of Christ’s atone-
ment, and only because of it.

Crimes can also be against men. This means that restitution
must be made to the victim, and not just to God. There is no forgive-
ness apart from restitution: Christ’s primarily, and the criminal’s
secondarily. As images of God, victims are entitled to restitution pay-

1. Gary North, Deminion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987), chaps. 3, 6. The Bible passage that
indicates these two aspects of salvation is 1 Timothy 4:10: “For therefore we both
labour and suffer reproach, because we trust in the living God, who is the Saviour of
all men, specially of those that believe.”

505
