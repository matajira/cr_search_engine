Seduction and Servitude 653

provided the bride price. Her father had not delivered the required
50 shekels to her as her dowry; he had delivered only the bride price
unjustly collected from the bridegroom. If the bridegroom could
prove that he had been defrauded by the girl, or by her and her
father, he could get back his bride price that had becn unjustly ex-
tracted from him.

He had paid something for the girl, but probably not 50 shekels
of silver. Why would the court not have returned whatever bride
price he had paid? What has precisely 50 shekels got to do with it?
The bridegroom was saying in effect that 50 shekels should have
been given to him by her father as a ritual sign of her family’s de-
pendence on his merciful willingness to marry a non-virgin. There
was mutual subordination involved, so her father should have provided
this bride price to the bridegroom, and then the bridegroom would
have ritually returned it to her father. Just as the bride price was a
ritual sign of his subordination to the father-in-law, so was the
father-in-law’s provision of a bride price to the bridegroom a ritual
sign of his dependence on the bridegroom. It was a sign that her
father was in no position to bargain under such circumstances be-
cause of his daughter's defiled status. But her father had been unwill-
ing to pay him the 50 shekels that would have served as his bride
price payment, so that he could in turn pay the 50 to the father, who
would then endow the daughter. The symbolism of the bridegroom’s
dependence was basic to the bride price-dowry transaction. Even
without the formal double transaction, the father’s payment of the
daughter’s dowry was implicitly a form of his dependence on the
bridegroom, But I believe that the double formal transaction would
have been carried out, as a public manifestation of the daughter's
lack of virginity. Such a formal public transaction would have
secured her from future prosecution by her husband.

The bridegroom was saying that he had never been informed of
the girl’s status, Her father had treated him unjustly, defrauding the
bridegroom of whatever bride price he had been asked to pay. Thus,
from a strictly economic standpoint, her father owed him at least the
return of the original bride price that he had paid. Her father may
also have owed the 50 shekels that should have been given to him by
the seducer. The text does not say this, and I may be reading too
much into it. It may be that the death of the daughter was regarded
by the court as a sufficient penalty on her family. The death of the
daughter may have replaced the implicit 50-shekel payment owed by
