228 TOOLS OF DOMINION

tance, helping her to mature in the spiritual independence from sin
that He has purchased. The church experiences progressive liberation
from sin and bondage in history — a progressive liberation based on the
Bridegroom’s definitive redemption payment at Calvary. The Lord’s
Supper covenantally represents this communion with the Bride-
groom. The church now awaits His return at the final consummation.

We know that we are in principle set free from sin, but in history,
our sanctification is not yet complete. Our first husband, Adam,
died in slavery to sin, and we had been left behind, enslaved to
Adam’s ethical master, Satan. Christ, like the brother whe honors
the terms of the levirate marriage (Deut. 25:5-10), then married us,
thereby delivering us legally out of bondage to sin, but the consum-
mation has not taken place. We wait for the return of our Bride-
groom, who has redeemed us from the household of servitude. He
did not marry us as a servant marries. We will not remain in ethical
bondage, He completed His work on Calvary. The resurrection tes-
tifies to His condition as a free man. We are resurrected in Him in
principle — definitively set free judicially and ethically from sin as His
lawful bride (Gal. 4:7).2° But in history, we still labor under the
bondage of sin (Heb. 2:8-18). Our sanctification in history is not yet
complete. We have not yet been presented as a chaste virgin belore
Christ (IT Cor. 11:2). One reason why there is no marriage after the
resurrection (Matt. 22:30) is that the church has but one husband,
Christ. There will be no divided family loyalties.

The marriage ceremony between Christ and His church did not
take place before Calvary. He was still laboring to complete His term
of service. He would not marry prematurely. It was the error of the
Jewish multitudes that they cxpected Hiberation—both marriage and
the consummation — in history, when they hailed Him as their earthly
king and placed palm branches before Him as He entered Jerusalem
in the final week of His pre-resurrection ministry (John 12:12-15).

The Fulfillment of the Jubilee Year

God's laws regarding Israel’s land tenure system required that
every fiftieth year, each plot of ground in Israel be returned to the
heirs of the original family member who had it allocated to him after
the conquest of Canaan (Lev. 25:8-34). This land tenure system kept

36. Gary North, Uncuditional Surender: God's Program for Victory (3rd ed.; "Tyler,
Texas: Institute for Christian Economics, 1988), pp. 66-71
