366 TOOLS OF DOMINION

vitude, these labor services are legally capitalized at the time of purchase,
and the buyer takes delivery of the person who is to supply them.
Slaves and indentured servants command a sale price. Why? Be-
cause their expected labor services are valuable. These services can
be capitalized. The purchaser calculates the present market value of
this expected stream of income in exactly the same way that he capi-
talizes the expected future income stream of any commodity, The
same rate of interest establishes the discount of the future services of
man, land, and machine, and to the same degree. The buyer esti-
mates the proper purchase price of all forms of capital by means of
the same statistical techniques.'® To this extent, the transaction ap-
pears to be impersonal, “treating men like machines.” But if we look
closer, we find that all such transactions are ultimately personal. The
wise (profit-seeking) slave-buyer calculates the expected future ser-
viccs of the slave in terms of how well he will treat the slave. He does
the same when he estimates the value of a piece of farmland. He
even makes such calculations regarding machinery. We speak of
“babying” a tool when we really mean treating it with care by
lubricating it, servicing it, and recognizing its limits in service. The
rate of interest is itself an impersonal number that is the product of
all the highly personal time-preferences (discounts for future goods
and services) of the many economic deci

 

ion-makers in the society.
Ultimately, there can be no impersonalism in a universe created and
providentially sustained by God.!”

The very fact that bondservants command a price, and owners
make rational economic decisions about how much to pay for bond-
servants, testifies to the reality of the commodity aspect of human
labor. The existence of a market for bondservants indicates that
men’s labor services can be treated as commodities. In short, ex-
pected future labor services can be capitalized— converted into capital

 

bonuses in advance when they sign their professional contracts, as well as receiving
a guarantced wage for a specified period of time. They can legally quit the team and
forfeit the agreed-upon wage income, bul they arc legally prohibited from offering
their services to a rival team within the same sports league. The bonus capitalizes a
portion of their future productivity.

16. If the tax laws recognized indentured servitude, hondservants would proba-
bly be depreciated the way a machine or any other depreciating asset would be. The
United States tax code allows animals and fruit-bearing trees to be depreciated in
this fashion.

17. Gary North, The Dominion Covenant: Genesis Qnd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), ch. 1.
