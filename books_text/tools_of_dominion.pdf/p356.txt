348 TOOLS OF DOMINION

knowledge. He felt he could not retract the statement without calling
the man who repeated it in public a liar, which was the only way he
could avoid the duel. Hamilton believed that to avoid the challenge
from Burr would ruin his influence within the Federalist Party.”

Hamilton had come close to other duels in his life. His brother-
in-law Robert Church had been involved in several duels, including
one with Burr.” His eldest son Philip had died in a duel with a Burr
supporter in 1801. All this in one prominent family. Thus, the duel in
late-eighteenth-century America was not uncommon within the up-
per classes, North and South. Hamilton and Burr fought their duel
across the Hudson River from New York City in Weehawken, New
Jersey. This was a common practice of duelers: fighting the duel in a
neighboring state.** Hamilton’s son Philip had been killed on this
very spot; it was a popular place for New York City residents to con-
duct their duels,7 New York’s laws against dueling were strict,
though not so strict as Exodus 21:18. Conviction for the mere issuing
ofa challenge could result in a prohibition on his holding State office
for 20 years.” Burr shot Hamilton on July 11, 1804, and the latter
died the next day. Hamilton had expressed his intention of not
shooting Burr, a practice which remained common but risky in sub-
sequent duels in the South.?” The legend that Hamilton fired into
the air while Burr fired into Hamilton is a myth; Burr fired first, and
Hamilton’s pistol went off as he fell.”

Hamilton knew full well that the duel was illegal and immoral,
which he stated in his diary. He insisted that his “religious and moral
principles strongly opposed the practice of duelling.” It would give
him pain, he wrote, “to shed the blood of a fellow in a private com-
bate forbidden by the laws.”** Nevertheless, the pressure he felt re-
garding his honor left him no choice: “To those, who with abhorring
the practice of duelling, may think that I ought on no account to
have added on the number of bad examples, I answer that it is my
relative situation, as well in public as private appeals, enforcing all

22. Ibid., p. 627.

23, Idem.

24. Bruce, Violence and Culture, p, 27.

25. John G. Miller, Alexander Hamilton and the Growth of the New Nation (New York:
Harper Torchbooks, 1959), p. 573.

26, Idem.

27. Bruce, Violence and Culture, p. 36.

28. Hendrickson, Hamilton 17, pp. 638-39.

29. Ibid., p. 634.
