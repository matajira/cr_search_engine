794 TOOLS OF DOMINION

brought before an unrighteous judge or an unrighteous court, how
does he gain righteous judgment? What if he is a stranger in some
society that expects bribes from those secking justice? An analogous
example is the salesman who seeks to sell military equipment or
other goods to nations governed by corrupt State officials. What if
that nation’s customs regarding State purchases recognize the legiti-
macy, or at least the necessity, of kickbacks and payoffs to public offi-
cials? In other words, what if some nation’s traditions would rather
have foreign capitalists pay part of the salaries of public ollicials,
even though this means using tax money to buy possibly substan-
dard foreign products? Obviously, to make such payments is to sub-
sidize evil — corrupt officials—to some degree. On the other hand, to
allow corrupt officials to continue to make personally profitable but
socially bad decisions is also to subsidize evil to some degree.
Wouldn't it be better to have a bribe-seeking public official profit
from a good decision rather than from a bad decision? The question
then arises: Are righteous people allowed to pay bribes, even though
officials are forbidden by the Bible to receive them?

Contrary to most people’s expectations, the Bible says yes. The
Bible recognizes that in order to gain legitimate goals in life, right-
eous people are allowed to pay bribes to corrupt officials. In the same
way that a bribe to a righteous judge is designed to twist righteous
justice, a bribe to an unrighteous judge is designed to straighten out
unrighteous judgments.

Solomon's Recommended Strategy
Solomon the wise understood this biblical principle of productive
bribery:

A gift is as a precious stone in the eyes of him that hath it: whithersoever
it turneth, it prospereth (Prov. 17:8).

A gift in secret pacifieth anger: and a reward in the bosom strong wrath
(Prov. 21:14).

Notice the phrase, “a reward in the bosom.” It produces a mental
image of a secret gift, one tucked away in one’s cloak, Nevertheless,
someone might argue that Solomon did not have civil government in
mind when he wrote these two proverbs. Perhaps Solomon had in
mind only personal friendship rather than civil justice. But to argue
in this fashion makes it very difficult to interpret Solomon’s use of the
