142 TOOLS OF DOMINION

returned to thcir share of the family’s traditional lands, thereby
reducing the possibility of large-scale slave gang labor. It would also
have increased the likelihood of manumission: freedom for slaves
whose economic productivity, without large land holdings, would
have dropped sharply. In other words, by reducing Israel’s per capita
capital (land), the jubilee land tenure law would have reduced labor
productivity for all those who remained in Israel. This was the whole
idea: to encourage covenanial dominion outside the land by encouraging
Hebrew emigration.

This economic link between the size of land holdings and the eco-
nomic feasibility of large-scale gang slavery is the simplest explana-
tion for God’s inclusion of the heathen slave laws within the section
of Leviticus that deals with the jubilee land tenure laws. One obvi-
ous reason why the Bible offers no example of the nation’s honoring
of the jubilee land distribution laws is that politically influential own-
ers of large slave gangs no doubt recognized that the economic value
of their slave holdings would be reduced drastically if they had to
return their land to the original families. Thus, any significant in-
crease of inter-generational slavery by heathens would have testified
to a refusal by the judges to enforce the original jubilee land distribu-
tion agreement that had been agreed to by all the tribes prior to the
conquest of Canaan. A growing population of permanent foreign
slaves would therefore have been a visible warning to Israel that they
were disobeying God's law. This was the same visible warning that
God had given to Egypt (Ex. 1:12, 20).

Slavery very clearly was not supposed to become a major institution
in Isracl. Land and labor are complementary factors of production.
The larger the population grew—a promised blessing of God—the
more valuable land would become; the more valuable the land be-
came, the less would be the return from net economic rents pro-
duced by slaves. Free laborers and tenants would be willing to work
for low wages for as long as they remained in the land of Israel; slav-
ery would offer no important economic advantages to rent-scekers.
The primary economic goal in such a land-starved economy would
have been to add to one’s land holdings, not to one’s supply of slaves.

Without cheap land, or increasingly productive land, permanent
agricultural slavery is unlikely to be maintained long term.°* Under
circumstances of increasing land scarcity, the reasons for holding

56, Evsey D. Doras, “The Causes of Slavery or Serfdom: A Hypothesis,” Journal
of Feonomic History, XXX (1970), pp. 18-32.
