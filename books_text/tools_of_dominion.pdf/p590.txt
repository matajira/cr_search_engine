582 TOOLS OF DOMINION

Permitting Voluntary Exchange

Suppose, for example, that there is a very desirable piece of land
overlooking a lake which is in the path of a proposed runway for jet
planes. The land sells at a discount because of the expected noise.
Potential buyers are warned in writing of the proposed airport. ‘The
buyer takes a risk. He buys his land less expensively, assuming that
he will get used to the noise (which most people do). Perhaps the
runway will never be built. Then he may find himself the owner of a
far more valuable piece of property. Or perhaps the airport will be
built, and the land appreciates anyway. (Empirical research indi-
cates that almost without exception, land adjacent to proposed air-
ports rises in value after the construction of the airport.)5*

Question: Should the civil government forbid such a transaction
if the seller has warned the buyer in writing concerning the risk?
Why should the civil government be given such power? Perhaps a
potential buyer cannot afford to buy a piece of land near a lake in
any area not subject to a negative factor like noise. Wouldn't an out-
right prohibition on land sales be harmful to potential buyers and
potential sellers? Wouldn't such legislation be discriminatory against
poorer members of the community? Why should men be forbidden
to trade off money against noise? On the other hand, should the air-
port be shut down by law because people who bought the land at a
discount later decide that they want a noise-free environment, and
then decide that a lawsuit is the way to get it? Is this not another case
of theft, a coercive redistribution of wealth from the airport and air-
lines to the buyers of discounted land?

This example should not be construed to validate the case of a
person who buys land at a high price and then is informed that the
city council has voted to build the runway. Here is a case of a viola-
tion of his property rights. The Bible says that he must be compen-
sated for any resulting loss. The beneficiaries of the council’s action,
the airlines that use the airport facility, should pay the victims either
directly or indirectly, through taxes collected by the city and passed
on to the victims. What should the compensation be? A payment
equivalent to any drop in the market value of the property caused by
the airport, plus moving costs, if owners decide to leave.

Something else should be considered. One reason why Western
industrial nations have become so concerned with pollution is that

54. Cheung, Myth of Social Cost, p, 20,
