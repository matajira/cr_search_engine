374 TOOLS OF DOMINION

Academic Hostility to the Protestant Ethic

African blacks were savages who were being delivered by South-
ern slavery from earthly bondage to demons. They were being given
the opportunity to improve their religious commitment, improve
their skills, and ultimately achieve spiritual freedom. Scholars do not
recognize that covenanially faithful people who achieve spiritual freedom by
the grace of God in history cannot forever be enslaved, They lose their status
as slaves to sin, This new judicial and ethical status eventually is
manifested in history. This is a major theme of the Book of Exodus.
Spiritual freedom under Jesus Christ eventually produces political and economic
Liberty, though seldom in a single generation. Conversely, spiritual bond-
age under Satan eventually produces political and economic bond-
age, though seldom in a single generation. History is not covenantally
neutral. There is ethical cause and effect in mankind’s institutional
history, a covenantal fact denied vehemently by humanists and
pietists alike. It is this denial which is the foundation of the operating
alliance between humanists and pietists,#! the defenders of the power
religion and the defenders of the escape religion.

There are five steps in the securing of this institutional liberty.
They match the five points in the biblical covenant model. The first
step is spiritual: faith in Jesus Christ as the sovereign Lord and
Savior, the redeemer of men and institutions in history. The second
step is the recognition of God’s hierarchical covenants: the require-
ment of faithful labor under guidance from those who possess author-
ity. The third step is covenantal faithfulness to the ethical terms of
God’s covenant. The fourth step is self-government (self-judgment)
with the hope of God’s blessings, both in heaven and in history. The
fifth step is confidence concerning the long-run earthly effects of
one’s efforts. This confidence leads to a more efficient management
of time and capital. In short, for any people to become liberated,
they must change their perception of God, man, law, judgment, and
time, They must then discipline their lives in terms of this cove-
nantal worldview, In short, the way to liberty is by means of the
Protestant ethic.

Technically oriented economic historians often not only ignore
the capacity for self-transformation that the Protestant work ethic

41. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Instivute
for Christian Economics, 1989), Part 2.

42. Gary North, Moses and Pharaoh: Dominion Religion os. Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985), pp. 3-5.
