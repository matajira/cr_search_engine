526 TOOLS OF DOMINION

B. Owners

‘The penalty paid to the victim by the criminal compensates him for
his trouble, while it simultaneously serves as a deterrent to future criminal
behavior, Biblical restitution achieves both goals—compensation of
the victim and deterrence of criminal behavior —by means of a sin-
gle judicial penalty: restitution. In contrast, modern humanistic jur-
isprudence has until quite recently ignored the needs of the victim by
ignoring restitution. Edward Levi, who served as President Gerald

 

evidence of an earlier law. “. . . the older rule makes a rather primitive distinction
between theft of an ox and theft of a shcep: for one ox you have to give five, but for
one sheep only four. No such distinction occurs in the later rule, Whatever kind of
animal you steal, you have tu restore two for one.” Daube, Studies in Biblical Law
(Cambridge: At the University Press, 1947), pp. 94-95. He uses a similar line of ar-
gumentation to distinguish Exodus 21:28-31 from 21:35-36: ibid., pp. 86-87.

40. This two-fold purpose of criminal law was ignored by modern American jur-
isprudence until the 1960's, when the subject of restitution to victims at last became a
topic of discussion among legislators and law enforcement authorities, Cf. Burt
Galaway and Joe Hudson (eds.), Offender Restitution in Theory and Action (Lexington,
Massachusetts: Lexington Books, 1977); Randy E. Barnett and John Hegel III
(eds.), Assessing the Criminal: Restitution, Retribution, and the Legal Process (Cambridge,
Massachusetts: Ballinger, 1977). See the special feature, “Crime and the Victim,” in
Thal (May/June 1972), the national legal news magazine of the American Trial
Lawyers Association.

The Department of Corrections of Minneapalis, Minnesota began a “restitution
and release” experimental program in 1974, the Minnesota Restitution Center, in
which criminals involved in crimes against other people’s property compensate the
victims, Only 28 men were admitted to the experiment during its first year. Violent
criminals were not accepted: Los Angeles Times (April 24, 1974). By 1978, 24 of the 50
states in the United States had adopted some form of compensation to victims of vio-
lent crimes.

This policy had begun in the mid-1960's in Great Britain. In the United States,
the first state to introduce such a program was California, in 1965. Such costs as
legal fees, money lost as a result of the injured person’s absence from work, and
medical expenses are covered in some states. In cases of death or permanent disubil-
ity, maximum payments were anywhere from $10,000 to $50,000. Average pay-
ments in 1978 were $3,000 to $4,000 (with the price of gold in the $175-240 per
ounce range). Nonviolent crimes were not covered, nor were property losses in vio-
lent crimes. Only a small percentage of citizens are aware of these laws; only a small
percentage (1% to 39%) of victims received such payments. Also, the states compen-
sated victims from state treasuries; the criminals did not make the payments: U.S.
News and World Report (July 24, 1978). Predictably, state officials want the federal
government to fund most of these payments. A bill to pay 90% of such costs through
the Law Enforcement Assistance Administration (L.E.A.A.), the Victims of Crime
Act, passcd the U,S. Senate in 1973, but did not pass the House and was not signed
into law. Hearings werc held in 1972 and 1973. It was a sufficiently important topic
to be included in Major Issues System of the Congressional Research Service of the
Library of Congress beginning in 1974: Crime: Compensation for Victims and Survivors,
Issue Brief 74014.
