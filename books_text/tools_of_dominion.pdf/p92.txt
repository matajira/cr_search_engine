84 TOOLS OF DOMINION

plicit and even explicit denial of these doctrines (and the relevance of
Deuteronomy 28) has been a basic tactic of the vast majority of
Christian theologians for over a millennium. Thus, they have at-
tempted to define away the case laws and historical sanctions. What
I am saying is that it is theologically invalid to attempt to define
away the continuing authority of Deuteronomy 28. I therefore see
the inescapable theological necessity of restoring the biblical defini-
tion of biblical law and therefore anti-law.

T fully realize that my definition of antinemian is not the accepted
usage. This common usage exists primarily because theological anti-
nomians who have rejected one or more of the covenant model's five
points have previously defined the word so that it conforms to their
pessimistic historical outlook: the long-term cultural impotence of
God's redeemed people in history. They argue that antinomianism is
merely the denial of one’s personal responsibility to obey God’s moral
law (undcfined).** This deliberately restricted definition implicitly
surrenders history to the devil. What I am saying is this: anyone who
denies that there are cause-and-effect relationships in history between
the application of biblical case laws and the success or failure of so-
cial institutions has also inevitably and i principle adopted the idea
that the devil controls and will continue to control this world. Why?
Because the devil’s representatives are said to be able to maintain con-
trol over the social institutions of this world throughout history
(point two of the covenant: representation). It does no good for a
person to answer that he is not an antinomian just because he
respects God's law in his personal life, family life, and church life.
He is still saying that God's law is historically impotent in social
affairs, that covenant-keeping or covenant-breaking offers rewards
and curses only to individuals and only after the final judgment.

Yes, J am offering a more comprehensive definition of “anti-
nomian.” My major goal in life is to lay additional foundations for a
major theological paradigm shift that has already begun. I am self-
conscious about this task. Readers deserve to know this, One inesca-
pable aspect of a new movement or new way of viewing the world is
the creation of new terms (e.g., “theonomy”), and the redefining of

32. The major exceptions were the Puritans: Journal of Christian Reconstruction, V
(Winter 1978-79): “Symposium on Puritanism and Law.”

33, “It refers to the doctrine that the moral law is not binding upon Christians as
a way of life.” Alexander M, Renwick, “Antinomianism,” in Baker's Dictionary of
Theology, edited by Everett F. Harrison, Geofirey W. Bromiley, and Carl F. H.
Henry (Grand Rapids, Michigan: Buker, 1960), p. 48.
