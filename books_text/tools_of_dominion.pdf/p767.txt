Impartial Justice and Legal Predictability 759

would-be court theologians dutifully reinterpret the biblical texts to
fit the ruters’ socialist goals: “The God of the Bible is on the side of
the poor just because he is not biased, for he is a God of impartial jus-
tice.”* The fact is, however, it is the idea that rulers are under God and
under the obligation to enforce God's revealed law that most antagonizes
rulers, not to mention their court theologians. Whether they repre-
sent the poor, the rich, or the “middling sort,” rulers refuse to repre-
sent God’s court of justice. To do so would point to God as final
Judge, and this doctrine is too repulsive for autonomous man.

Judicial Stability

God’s justice is the goal for the entire commonwealth, and all
members of society are personally responsible before God to meet all
of the demands of His law. The 23rd chapter of Exodus provides us
with some specific details of what constitutes biblical justice. False
reports are prohibited (23:1, 7), Evil acts by men in crowds are banned
(23:2). Favoritism of the rich or poor is banned (23:3, 6). Animals
that belong to a hated neighbor must be assisted and returned to him
{23:4-5), The acceptance of bribes by leaders is banned (23:8). Op-
pression of strangers is prohibited (23:9). God’s law is to rule over the
affairs of men, irrespective of anyone’s personal emotions concerning
the “worthiness” of a man or his cause. All men are worthy to receive
God’s justice, just as all men are worthy of the wrath to come.

God’s justice is constant.5 It is constant because it is theocentric.® It
reflects the unchanging character of God. God’s justice on judgment
day will be reliable. Therefore, human judges are required by God
to strive to become analogously reliable. They are to render deci-
sions in terms of the fixed principles of biblical law.

This does not mean that the application of the law’s principles is
essentially a near-mechanical operation. While the principles of bib-
lical justice do not change, the applications of God’s general principles
in specific instances can change over time, for history has meaning.”
Chrisi’s replacement of the Mosaic ritual ordinances with new ones,
baptism and communion, is indicative of the nature of the relation-

4, Ronald J. Sider, Rich Christians in an Age of Hunger: A Biblical Study (Downers
Grove, Ulinois: Inter-Varsity Press, 1977), p. 84.

5. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984), pp. 11-17.

6. ibid., pp. 1-3.

7. Wid, pp. 12-17,
