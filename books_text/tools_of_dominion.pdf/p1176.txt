1168 TOOLS OF DOMINION

1974, the number of recorded crimes rose by over 30%. In England
and Wales, crime also rose by 30 percent, 1974-78. Substantial in-
creases also took place in France, Sweden, the Netherlands, West
Germany, Denmark, Austria, and Italy,? This indicates the direc-
tion of the growth in the 1970’s. Of great concern is the fact that ac-
tual crimes seem to have exceeded reported crimes by many times.
In the U.S., actual crimes were as high as three times those re-
ported; in England and Wales, it was closer to ten times higher.*

There is no doubt that there is a still major crime problem today
in the U.S. Reports one article on the economics of prisons: “Every
week, like clockwork, the total number of prison inmates in the U.S.
grows by 1,000 people. That's two big prisons worth of lawbreakers,
most of whom cost between $14,000 and $30,000 a year to feed, house
and guard. With 605,000 men and women behind bars in state and
federal prisons, the U.S. already has the highest incarceration rate
in the Western world; about four times that of the U.K or France on
a per capita basis. And that’s not even counting the 300,000 or so in
county jails across America. . . . With 37 states under court orders
to reduce overcrowding, the U.S. has embarked on a prison-building
program unparalleled in history.”* The primary response of the au-
tharities to crime has been prison-building. The rate of incarceration
has grown every year from 1972: from slightly under 100 per 100,000
population to over 220.° “Just as rehabilitation was the byword of the
1960s, in the late 1980s a crime-weary citizenry wants to lock the bad
guys up and throw away the keys.”¢

The Explosion in Crime, 1960-80

In the United States, from 1960 to 1980, reported violent crimes
skyrocketed in the United States and Western Europe, although not
in Japan.’ The major increase in the United States took place in the

2, David J. Pyle, The Econoraics of Crime and Law Enforcement (New York: St. Martin’s,
1983), pp. 1-2.

3. Hbid., p. 2; citing H. J. Schneider, “Crime and Criminal Policy in some West-
ern European and North American Countries,” Intemational Reniew of Criminal Policy,
(1979), pp. 55-65.

4. Katherine Barrett and Richard Greene; “Prisons: The Punishing Cost,” Finan-
cial World (April 18, 1989), p. 18. (Gold’s price was about $385 per ounce.)

5, dbid., p. 2.

6. ibid., p. 18.

7. “Social Scientists Say U.S. Grime Has Leveled Off,” New York Times (Feb, 2,
1982). On Japan, see “Tokyo, Where Law Means Order,” Wall Street Journal (Nov.
29, 1973).
