628 TOOLS OF DOMINION

What would he have owed to the temple in the case of unconfessed
perjury? If the trespass offering was one animal if he had confessed
after making a false oath or oaths, presumably the penalty was double
this.%* This follows from my thesis that there ts an escalation of penalties.
At each step of the legal proceedings, he can confess and bear a reduced
penalty. For each level of deception, there are increased sanctions,

God is honored by the very act of self-confession, when such con-
fession has a penalty attached to it. Oath or no oath, the two primary
goals of laws governing theft are the protection of property and the com-
pensation of the victim. Earthly civil courts are therefore to safeguard
the property rights of the victims, making sure that the appropriate
penalty is extracted from the criminal and transferred to the victim.
There is no requirement of an additional money penalty payment to
the civil court because of a false oath regarding theft. A trespass or
guilt offering must be paid to the church.??

The false oath before God invokes the threat of the ultimate
penalty: the eternal wrath of God, preceded by the physical death of
the criminal. Unless a person confesses his false oath in this life,
makes appropriate restitution to his victim and brings a transgres-
sion offering, God will collect His own restitution payment, and it is
far greater than 20 percent. Ananias and Sapphira lied to church au-
thorities concerning the percentage of their economic gains that they
had voluntarily donated to the church. When asked individually by
Peter if what they had told the authorities was true, they lied, and
God struck each of them dead on the spot, one by one (Acts 5:1-10).
This served as a very effective warning to the church in general (v.
11). Presumably, they could have confessed their crime at that point,
paying all the money from the sale into the church's treasury, since
God was the intended victim of their lies (Acts 5:4). They chase in-
stead to lie. So, God imposed His more rigorous penalty.

After the Accusation, but Before the Trial

What if the thief stole an animal, especially a sheep or an ox, and
then sold it? If the civil authorities have brought the thief to trial, but
the trial has not been held, would he be given the opportunity to

36, It could be argued that the penalty was death; a high-handed false oath that
was not confessed,

37. The question arises: Which church? To the church that the convicted thief
belongs to, since it suffers the public humiliation. If he belongs to no church, then it
should probably go to the victim’s church, or if he also does not belong to a church,
to a local church selected randomly or in predictable sequence by the civil judges.
