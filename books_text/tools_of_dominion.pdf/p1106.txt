1098 TOOLS OF DOMINION

were really serious about this argument, then he would have to
abandon the idea that it is possible for the economist, as a scientist,
to make any recommendations concerning proper economic policy,
since any State-imposed policy always hurts some participants and
benefits others. If it is impossible to make interpersonal comparisons
of subjective utility, then economists must remain forever silent
about the aggregate (social) economic benefits and costs of any deci-
sion by an individual or the State.*

Robbins was correct in his criticism of Pigou, given the presup-
positions of modern, subjectivist economics. Harrod was equally
correct in his criticism of Robbins, namely, that Ais conclusion would
destroy all applied economic science. Robbins subsequently backed away
from this conclusion concerning the inability of economists to say any-
thing about social welfare or the benefits of social policies in general.”
But he never explained how he could logically back away from this
conclusion, and he lived until 1985. Even more inconsistently, he
also never backed away from his critique of Pigou’s argument in
favor of graduated (“progressive”) income taxation.

The implications of Robbins’ position are radical, and economists
have long been unwilling to face them, including Robbins. Buchanan
once wrote that “it is precisely the problems posed in modern welfare
economics that force the economist to come to grips with the basic
issues of political and legal philosophy.” These issues also force the
more astute economist to come to grips with the fundamental issue
of all philosophy: epistemology. But the ranks of the economics pro-
fession are filled with men and women who have no training in epis-
temology and care nothing about it.** They never answer by means
of modern subjectivism the fundamental philosophical question:
“What can men know, and how can they know it?” They operate in

 

the bachelor’s degree: Keynes’ was in mathematics and Harrod’s was in the human-
ities, See Don Patinkin, Anticipations of the General Theory? And Other Essays on Keynes
(University of Chicago Press, 1982), pp. xv, xvi. John Neville Keynes, Maynard's
father, and Pigou personally paid for young Maynard’s salary when they hired him
to teach econumics at King’s College, Cambridge in 1908. Keynes, Sr. was chairman
of the department for many years.

36. R. F. Harrod, “Scope and Method of Economics,” Economic Journal, XLVUT
(1938), p. 397.

37. Lionet Robbins, “Interpersonal Comparisons of Utility: A Comment, ibid,
pp. 635-37.

38. James Buchanan, “Good Economics—Bad Law,” Virginia Law Review, LX
(2974), p. 488.

39. An exception is the Austrian School.
