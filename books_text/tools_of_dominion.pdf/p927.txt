34
THE ABILITY TO TEACH

And he hath filled him [Bezaleel] with the spirit of God, in wisdom, in
understanding, and in knowledge, and in all manner of workmanship;
. .. And he hath put in his heart that he may teach, both he, and Aholiab
... (Bx, 35-31, 34a).

God is the source of all wisdom and all technical skills. Human
civilization is the result of the procession of God’s Holy Spirit in
time. There is continuity in human culture, generation to genera-
tion, only because there is continuity of the work of God’s Spirit in
time. God uses human instruments in order to achieve the progres-
sive establishment of His kingdom in history. The kingdom of God is
best described as the civilization of God. It is both heavenly and
earthly. Architecture is certainly a visible aspect of God’s earthly
kingdom, and it points to the architecture of heaven. This was
understood far better by medieval Christians than it is today. They
also understood the need of personal apprenticeship as the best
means of training men in building skills. As Christians’ time per-
spective has shortened, su has their sense of architectural aesthetics.
The aesthetic link between earth and heaven is not taken seriously
by most evangelical Christians, as their church buildings reveal.'

It is significant that almost nothing remains of Israelite architec-
ture. Neither the first nor second temple survived the invasions of
Israel’s enemies, nor did the king’s palace. God destroyed all traces
of Israelite monumental architecture because of their repeated rebel-
lion. The Israelites lost continuity architecturally because they did
not maintain continuity ethically.

1. The aesthetic link between earth and hell has been taken very seriously by
satanists, as their record album and audio disk covers and posters reveal so blatantly.

919
