260 TOOLS OF DOMINION

male servant could be sold to any Hebrew inside the land. (If the
Hebrew buyer adopted her, so could a Hebrew girl.)!3 Normally, the
resident alien was not under the limitations of the sabbatical year; he
was only under the terms of the jubilee year. Since the resident alien
could capitalize up to 49 years of service from a Hebrew male bond-
servant (Lev. 25:47-52), he was in a position to offer a higher pur-
chase price. This would have created a major source of profit: buying
sabbatical-year bondservants and selling them to pagans. Therefore,
we have to conclude that if a sabbatical-year bondservant was sold to
a resident alien, the stranger would have had to abide in this unique
instance by the terms of the sabbatical year. J¢ is illegal to sell what you
do not own; a Hebrew who purchased a sabbatical-year Hebrew ser-
vant did not own any claim on his services beyond the sabbatical
year,!#

What this passage establishes, at the very least, is that a Hebrew
girl could not be sold to a stranger." There was a covenantal reason
for this restriction: Aterarchy. A woman was always covenantally sub-
ordinate to a man, except for a widow (Num. 30:9). She was inher-
ently in a position of covenantal subordination. It was therefore il-
legal to sell her into a pagan household ruled under pagan household
deitics. This cultural influence was too dangerous for her, compared
to the risks for a man. A father could not sell a daughter into a for-
eign househoid, for he was her lawful representative before God. His
son could lawfully be sold into servitude to a resident alien.1®

Adoption

The daughter referred to in the text is someone who has been
bought from her father to become a wife, either for the master or for
his son. Thus, she was bought by means of a fermanent transfer of au-
thority, The master, as either a future husband or future father-in-

18. Maimonides denied this: “The Hebrew slave may neither be sold by her mas-
ter or given away to another man, regardless of whether he is a stranger or a kins-
man.” Maimonides, Acquisition, Chapter Four, Section Ten, p. 262. He went so far
as to say, “Neither may one sell or give away to another a Hebrew male slave,” Ldem.,

14. A Hebrew convicted of a crime and sold into bondservice was therefore legal
to sell again wo a resident alicn on the same terms: service for full restitution.

15, Rabbi Moses ben Nachman [Ramban], Commentary on the Torah: Exodus (New
York: Shilo, [1267?] 1973), pp. 352-53.

16, This has nothing to da with Christ’s serving as a substitute. Christ served as a
substitute for His brothers, not for His Father, just as Judah offered to serve as 2
substitute for Benjamin (Gen. 44:18-34).
