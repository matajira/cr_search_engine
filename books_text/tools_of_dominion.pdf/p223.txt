Servitude, Protection, and Marriage 215

minion covenant (Gen. 1:26-28). God honored this crucial dominion
function of the family by placing restrictions on it. A servant is ex-
pected to defer marriage until he is an independent man. Later, as a
husband in a position of authority, he can exercise dominion under
God as the head of his family. The model here is Jacob (Gen. 29:20).

Both marriage and labor are normally to be part of the dominion
covenant between man and God. Since the servant’s dominion over
his assigned portion of the earth is not independent of his master’s
authority, his authority over a wife taken during his term of service is
also under his master’s authority. There is a Auman mediator between.
God and the servant: the master. Therefore, it is the master, not the
servant, who is directly responsible to God for the general care of the
servant's wife. The servant takes orders from the master.'*

The servant’s protection comes from the master. The capital at
his disposal comes from his master. He takes orders directly from his
master or a representative of the master. If he is a foreman himself,
he issues orders only as a representative of his master, since he is act-
ing as an official under the master’s general sovereignty. The master
is responsible before God for any delegation of authority to a ser-
vant, so the mediatorial position of the master is not abrogated sim-
ply because he turns limited authority over to the servant.

This law made it clear to any woman who married a Hebrew in-
dentured servant that the ultimate human authority over her, and there-
fore her legal protector, was not her husband but rather her husband’s
master. She was fulfilling the terms of the dominion covenant as a
wife within a family unit, but the head of her family was her hus-
band’s master. Her husband was therefore only a representative of the
head of her family. The covenant of marriage was in this instance
four-way: 1) God, 2) the master of the house, 3) the indentured ser-
vant, and 4) the servant’s wife. Since the protection of the wife and
children was ultimately the legal responsibility of the master, the ser-
vant’s wife and the children remained with the master when the hus-
band, now released, departed.

The existence of such a law regarding servant families testifies to
the importance of protection for a wife, Economic protection is one

14. A modern application of this biblical principle would be that a wife should re-
main a member of the Bible-believing church she is covenanted to even if her hus-
band leaves the church and joins a more liberal church, let alone an apostate church.
Her spiritual covering is provided by the church, mediated through her husband.
Even though he has removed himself from the church's covering for the family, she is
still entitled to it.
