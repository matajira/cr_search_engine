A Biblical Theology of Slavery 183

tized but were not immediately freed is not hypocritical, given the
assumption that Christians do not recognize that the abolition of the
jubilee land tenure law also abolished permanent slavery.

Christianity and slavery were considered compatible until the
1770's, The geographical and ecclesiastical differences, North vs.
South, did not lead to sharp differences of opinion regarding slavery
until the carly nineteenth century. Racist hostility to blacks in both
the North and the South was common throughout the antebellum
period,/9* These attitudes were inherently anti-biblical, but almost
nobody recognized this prior to the late eighteenth century. This is
why David Brion Davis calls this the problem of Western slavery.!9°
It was, above all, a moral and theological problem.

The War Escatates

Let us not forget that the Bible-denying, Trinity-denying
Unitarian abolitionists of the 1840's and 1850's in the United States
had first decided to abolish the God of the Bible; only afterward did
they turn their attention to the problem of abolishing Southern slav-
ery.20° Some of them had even decided to abolish the Union, calling
the U.S. Constitution “a covenant with death.”?! William Lloyd
Garrison concluded that the Bible must be subjected to the tests of
reasonableness, historical confirmation, the facts of science, and
man’s intuition, “Truth is older than any parchment,” he affirmed.
His radical disciple, Henry Clarke Wright, proclaimed: “The Bible,

198. Leon F. Litwack, North of Slavery: The Negro in the Free States, 1790-1860 (Uni-
versity of Chicago Press, 1961).

199, See the review of Davis’ book by Moses I. Finley, “The Idea of Slavery,” New
York Review of Books, VIII ( Jan. 26, 1967); reprinted in Allen Weinstein and Frank
Otto Gatell (eds.), American Negro Slavery: A Modern Reader (New York: Oxford Uni-
versity Press, 1968), pp. 348-54.

200. Most of the Unitarian abolitionist leaders were or had been ministers. Otto
Scott inserted the word “Rev.” before the names of these men throughout the manu-
script of his book, The Secret Six: John Brown and the Abolitionist Movement (New York:
Times Books, 1979). When the page proofs came back from the printer, “Rev.” had
been removed. He marked the page proofs to indicate that the word should be rein-
serted. When the book was printed, these changes had not been made. The editor
had known just how devastating these references to their ordination would appear,
and he had kept them from appearing. I was told about this by Scott several years
later.

201. Philip S. Paludan, A Covenant With Death: The Constitution, Law, and Equality
in the Civil War Era (Urbana: University of Minois Press, 1975). Cf, Staughton Lynd,
“The Abolitionist Critique of the United States Constitution,” in Martin Duberman
{ed.), The Antislavery Vanguard: New Essays on the Abolitionists (Princeton, New Jersey:
Princeton University Press, 1965), pp. 209-39,

 

 
