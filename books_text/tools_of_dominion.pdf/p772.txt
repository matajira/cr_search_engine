764 TOOLS OF DOMINION

Darwinian world must be relative. Law is part of the overall evolu-
tionary process. Any correspondence between the one (general law)
and the many (specific circumstances) may last for no longer than
an instant. Darwinism produces process philosophy: the assertion of
a world devoid of permanent standards.?* A sea of randomness
engulfs Darwin’s universe, threatening to. overcome islands of per-
manence. Randomness also engulfs the mind of self-professed au-
tonomous man.?”

Legal Predictability

Justice is simultaneously personal and impartial. God does not
respect persons, a doctrine that is repeated again and again in Scrip-
ture, as we have seen.” Cosmit personalism, meaning God’s compre-
hensive judgment of every fact in the universe, requires judicial
impartiality for human law courts. Men are to think God’s thoughts
after Him, within the limits of their creaturehood. Truth is placed
before friendship or hatred, class or status. Biblical law is not class
law, contrary to Marxists. It is not the product of class conflict. It is
accurate to say that the arena of biblical law’s application is the histor-
ical product of ethical conflict between man and God. Conflicts be-
tween men are a result of this ethical conflict between man and God
(James 4:1), but these conflicts are not the origin of biblical law. Bib-

 

impossible) at the level of subatomic physics. The positions between electrons are far
smaller than the smallest light wave, so the light serves as a kind of blanket which
covers up what is going on. If smaller gamma rays could ever be employed in a
“microscope,” these would strike the clectrons and “kick” them, thereby changing
their momentum. In short, the observer interferes with the observed. “A quantitative analy-
sis of this argument shows that beyond any instrumental errors there is, as stated by
the uncertainty principle, a residual uncertainty in these observations.” /éid., p. 559.
As a result, the optimism of scientists regarding Newtonian mechanics as a perfect
description of the physical universe has disappeared.

But this textbook summary for undergraduates avoids the real problem of mod-
ern quantum mechanics. The uncertainty of the universe is now said to be funda-
mental, and not just our uncertainty of measurement. The unobserved “real world”
is said to be statistical rather than physical at the subatomic level. See North, is the
World Running Down?, ch. 2,

26, North, Genesis, pp. 273, 287, 323-24, 333-35, 339, 351, 355, 419-20.

27. Cornelius Van Til, The Defense of the Faith (2nd ed.; Philadelphia: Presbyter-
jan & Reformed, 1963), pp. 124-28. For a detailed defense of this thesis from a
humanistic viewpoint, see William Barrett, Irrational Man: A Study in Existential Philo-
sophy (New York: Doubleday Anchor, [1958] 1962).

28. Deut. 10:17; II Chr. 19:7; Job 34:19; Acts 10:34; Rom. 2:
6:9; Col. 3:25; I Pet. 1:17.

 

; Gal. 2:6; Eph.
