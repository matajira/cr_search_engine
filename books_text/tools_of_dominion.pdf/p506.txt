498 TOOLS OF DOMINION

tested danger, the owner must pay full damages.”? Almost a millen-
nium later, Maimonides agreed: he exempted the Israelite owner
from being required to pay damages, whether or not he was fore-
warned about his beast, if his ox gores an ox belonging to a heathen.
He adds reasons for the Mishnah’s discriminatory law. The “heathen
do not hold one responsible for damage caused by one’s animals, and
their own law is applied to them.” (This is truly preposterous, and he
offers no evidence.) On the other hand, the heathen is fully liable,
whether or not he was forewarned, if his ox gores the ox of an Israelite.
Why? Because “should they not be held liable for damage caused by
their animals, they would not take care of them and thus would in-
flict loss on other people’s property.”® This is a classic example of dif-
ferent laws for different residents, in open violation of Exodus 12:49.

Maimonides argued that if the ox was unowned at the time of the
goring, and is subsequently appropriated by someone else, before
the plaintiff can seize it, the new owner is not liable for previous
damages.‘ This would leave the victim without recourse, and it
would leave the animal immune from judgment, for it would not
serve as payment—ox for ox—for the damages it caused. (Rabbi
Judah had early argued that “A wild ox, or an ox belonging to the
Temple, or an ox belonging to a proselyte who died are exempt from
death, since they have no owner.”)!

Even more incredibly, Maimonides argued that if the existing
owner renounces ownership after the goring takes place, but before
the trial, “he is exempt, for there is no liability unless the ox has an
owner both at the time it causes the damage and at the time the case
is tried in court.”* This would destroy personal legal liability in the
most serious cases. The owner would be allowed to separate himself
retroactively from the social responsibilities of ownership, as if own-
ership of a physical object were all that is involved in ownership, and
not also the legal immunities and legal responsibilities that are in-
escapably bound up with possession of the object. Maimonides did

2, Idem,

3. Moses Maimonides, The Book of Torts, vol. 11 of The Code of Maimonides, 14 vols.
(New Haven, Connecticut: Yale University Press, 1954), “Laws Concerning
Damage by Chattels,” Chapter Eight, Section Five, p. 29.

4, Idem,

5. Baba Kama 4:7, Mishnah, p. 337. The Talmud also specifies that the ox had to
have gared on three previous accasions for the owner to become personally liable:
Shalom Albeck, “TORTS, The Principal Categories of Torts,” in The Principles of
Jewish Law, edited by Menachem Elon (Jerusalem: Keter, 1975?), col. 322.

6. Maimonides, Torts, loc. cil.
