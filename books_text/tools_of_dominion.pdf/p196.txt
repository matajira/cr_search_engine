188 TOOLS OF DOMINION

this took at least 40 years. In other words, # took time, yet it was
essentially one event, covenantally speaking: the coming of Christ’s
New World Order.

The legitimacy of lifetime heathen slavery and inherited slaves
ended with Israel’s final jubilee year in a.p. 70. In principle, that
jubilee event came with Christ’s announcement of the meaning of
His ministry (Luke 4:16-21). But the development of this jubilee
principle of release progressed for one generation until God de-
stroyed Jerusalem, in order to destroy the liturgical and political
foundations of the Jewish religious leaders who had refused to tet
their spiritual slaves go free. The leaders had rejected Christ’s
message of final jubilee release, and so had most of their spiritually
enslaved followers; in response, God destroyed their civilization.

Jesus Christ was in principle an abolitionist, for He was the fulfillment
of the jubilee year principle, the great year of release, His fulfillment
of the jubilee year announced the advent of His New World Order.
Nevertheless, He did noi verbally require an overnight (or seven-year) pro-
gram of manumission or abolition, As is the case with many of the im-
plicit social and economic principles of the Bible, Jesus established
the principle of abolition, and He was then content to wait for His
people to acknowledge it and put it into action in history. He waited
for seventeen centuries. (In the case of the biblical principle of judi-
cially restrained debt, He is still waiting.)

The task of abolition was delayed until the advent of the modern
world — specifically, until the Industrial Revolution made possible
new sources of economic productivity that dwarfed anything that the
old slave and scrf systems could produce. This does not mean, how-
ever, that the Industrial Revolution as such led inevitably to the aboli-
tion of slavery. Historical causation is more complex than this. The
issues and pressures behind abolitionism were far more complex
than mere economic determinism or “mere” anything else.?!® Never-

215. Eric Williams argued that England's Caribbean slave colonies supplied
much of the capital for England's Industrial Revolution, but then slavery became an
economic drag on capitalism, feading to a shift of opinion toward anti-slavery
among the new capitalist leaders of British society, Williams, Capitalism and Slavery
(New York: Putnam, [1944] 1966). ‘his thesis gained wide support among scholars
for at least a generation, but both halves of this thesis have been seriously challenged
in recent years, See especially Seymour Drescher, Kconocide: British Slavery in the Era
of Abolition (Pittsburgh, Pennsylvania: University of Pilisburgh Press, 1977). David
Brion Davis now admits that he was incorrect in 1966 when he argued in The Problem
of Slavery in Western Culture that “no country thought of abolishing the slave trade until
its economic value had considerably declined.” On the other hand, he says, no one
has shown that the abolition of slavery retarded England’s economic gruwth or was
regarded by most English policy-makers as a threat to vital national interests. Slavery
and Human Progress, p. 335, note 121. See also his bibliography in note 119.
