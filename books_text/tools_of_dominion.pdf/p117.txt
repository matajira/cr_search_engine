What Are the Case Laws? 109

regarded by the liberals as ancient history.%?

Nevertheless, this practice of citing liberal scholars, even if con-
fined to footnotes only, is like a nasty habit picked up in one’s youth;
it is very difficult to overcome once you are afflicted, It usually be-
comes a lifelong addiction. You can spot the addict easily: as he
reads the final manuscript version of his book, just before he sends it
to the typesetter, he scans the footnotes, making sure that there are a
sufficient number of German works cited, even if only in translation.
If there are none, the author’s hands begin to shake uncontrollably,
like a heroin addict suffering withdrawal symptoms. He rushes back
to make one more set of revisions, frantically scanning the latest
theological journals in search of a handful of citations—any
citations —just to make his book appear academically respectable.
“One more fix; just one more fix! Then Pll quit forever!” But the
pathetic addict knows he can’t quit. Even when he is ashamed by his
habit, he returns to the sins of his youth. He pretends that the drivel
he reads in scholarly theological journals is significant. In time, he
risks being remade in their image.

The works of modern theologians are overwhelmingly useless,
yet occasionally one of them will randomly offer some vaguely useful
insight, so the addicted scholar keeps plowing through their books,
hoping for a footnote or two. Tools of Dominion displays occasional
evidence of being the product of this bad habit picked up in my
youth. But at least you will find no trace of ethical relativism in this
book’s thesis.

Conclusion

The case laws of Exodus provide us with fundamental legal prin-
ciples that God has established in order to provide His people with a
means of gaining His external, historical blessings, These case laws
are mankind’s God-given tools of dominion. Without them, and

52. A good example of such a fad was the “death of God” movement, which lasted
from 1966 until (maybe) 1969, ‘Lhe “hot” theologians who promoted this short-lived
fad were Gabriel Vahanian and Thomas J. J. Altizer. This is an affliction I call
Altizer’s disease: two years after people get it, they forget all about you. See Altizer,
The Gospel of Christian Atheism (Philadelphia: Westminster Press, 1966). Altizer was
leaching at Emory University, a Methodist school in Atlanta, Georgia; Westminster
Press is mainline Presbyterian, See also Altizer and Hamilton, Kadical Theology and
the Death of God (New York: Bobbs-Merrill, 1966). Vahanian’s book had been pub-
lished half a decade earlier, but did not immediately catch on: The Death of God: The
Culture of Our Post-Christian Era (New York: George Braziller, 1961).
