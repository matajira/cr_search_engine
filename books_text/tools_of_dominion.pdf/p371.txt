The Human Commodity 363

You might imagine that such a moralistic argument against capi-
talism is a variation of Marxism. Such is not the case. Marx’s few
references to workers as commodities appear only in his youthful
and unpublished Economic and Philosophic Manuscripts of 1844, which
were not translated into English until the mid-1960’s, and which had
zero influence on traditional Marxist thought.!° Marx was quite
matter-of-fact in his published writings concerning human labor as a
commodity. In his major theoretical work, Capital (1867), Marx
argued that the “free laborer,” meaning the wage-eamner in a capital-
ist economy, sells his own commodity, labor power, to the capitalist.
He “must constantly look upon his labour-power as his own prop-
erty, his own commodity, and this he can only do by placing it at the
disposal of the buyer temporarily, for a definite period of time."
Original Marxist theory presumes that if the legally free laborer can
legitimately look at his own labor power as a commodity, then so can
the capitalist buyer. Marx argued that the terms of sale involve ex-
ploitation by the capitalist, but he did not argue that the item sold,
human labor, is somehow not a commodity.

Years carlier, Marx had distinguished between slave labor, in
which the worker is a commodity, and free labor under capitalism, in
which he isn’t. He discussed labor power, not the worker as a com-
modity. “Labour power was not always a commodity. Labour was not
always wage labour, that is, free labour. The slave did not sell his
labour power to the slave-owner, any more than the ox sells its ser-
vices to the peasant. The slave, together with his labour power, is
sold once and for all to his owner. He is a commodity which can pass
from the hand of one owner to that of another. He is Aimself a com-
modity, but the labour power is not Ais commodity.” !? Popular Marx-
ism may occasionally use the idea of “proletarian man, the commod-
ity” to gain converts, but traditional Marxism has always focused on
Marx’s exploitation theory, his surplus value theory, and other more
arcane topics. Thus, to criticize capitalism because of its alleged
result ~~ workers as commodities —is a most un-Marxist line of rea-

 

 

10. These statements appear in the essay, “Antithesis of Capital and Labor. Landed
Property and Capital.” Two brief references to workers as commodities from this
essay are the only ones listed in Kari Marx Dictionary, edited by Morris Stockhammer
(New York: Philosophical Library, 1965), p. 268.

11. Karl Marx, Capital (New York: Modern Library, [1867] 1906), ch. 6, pp.
186-87. The Modern Library version is a reprint of the Charles H. Kerr edition.

12. Marx, Wage Labour and Capital (1849), in Karl Marx and Frederick Engels,
Selected Works, 3 vols. (Moscow: Progress Publishers, 1969), I, p. 153.
