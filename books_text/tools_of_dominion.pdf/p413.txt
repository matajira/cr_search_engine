Criminal Law and Restoration 405

the program. Also, the amount of restitution was limited to the
amount of the economic loss by the victims, not two-fold restitution,
as required by the Bible. The original state-level trial program was
dropped in 1976, but the principle has been instituted at the local
level. Judges in every jurisdiction now impose restitution as a penal
sanction.

The Summary Report states that “Most judges and probation offi-
cers favored the use of restitution. Similarly most judges and proba-
tion officers expressed the belief that restitution had a rehabilitative
effect.” Furthermore, “most victims believed that restitution by the
offender to the victim is the proper method of victim compensation.
Victims who were dissatisfied tended to be those who felt that they
had not been involved in the process of ordering or aiding in the
completion of restitution.” And perhaps most revealing of all, “Most
offenders thought that restitution as ordered was fair.”®* Only ten of
the offenders (14.4 percent) would have preferred a fine or a jail sen-
tence, ® It is understandable why we have seen a renewed interest in
restitution as a form of punishment.”

Prisons

The prison as a correctional and rehabilitative institution was the
invention of the early nineteenth-century reform movement in the
United States. Visitors from all over Europe came to see these cor-
rectional “wonders.” The most famous of these visitors was Alexis de
Tocqueville, who came from France in 1831 to see our prisons, and
who then wrote the most insightful study of American institutions in
the nineteenth century, which also became the earliest major work in
the discipline of sociology, Democracy in America (1835, 1840). He and
his colleague Gustave de Beaumont produced a report on their
observations, On the Penitentiary System in the United States (1833).7

68. Zdem,

69. Ibid, p. 26.

70. Joe Hudson and Burt Galloway (eds.), Considering the Victim: Readings in Resti-
tution and Victim Compensation (Springfield, Illinois: Charles C. Thomas, 1975);
O. Hobart Mowrer, “Loss and Recovery of Community,” in George M. Gazda
(ed.), Innovations to Group Psychotherapy (Springfield, Illinois: Charles C. Thomas,
1975). Such interest has never been entirely absent: see Irving E, Cohen, “The Inte-
gration of Restitution in the Probation Services,” journal of Criminal Law, Criminology
and Police Science, XXXIV (1944), pp. 315-26.

71. Toequevitle and Beaumont on Social Reform, edited by Seymour Drescher (Santa
Fe, New Mexico: Gannon, 1968)
