Maimonides’ Code: Is It Biblical? 1031

The revival of Christian casuistry that is presently taking place’
proclaims self-consciously the authoritative character of the Old Tes-
tament’s ethical principles and, as my economic commentary indi-
cates, the contemporary applicability of the letter of Old Testament
law as well.

The evolutionary judicial character of Judaism has led to the
near-destruction of Orthodoxy’s influence in Western Judaism. The
dual social forces of Western capitalism and secularism established
institutional and philosophical foundations that have steadily under-
mined Talmudic religion and culture. The more ethically evolutionary
any particular worldview has been, the more rapidly it has saccumbed
to this powerfull pair of social forces. Judaism was especially vulnerable.

The factor that most threatened Orthodox Judaism was indus-
trial society's growing toleration, In the mid-nineteenth century,
when Jews in Western Europe and the United States began to enter
the new industrial capitalist world, they found that the older dis-
criminatory legal barriers had been progressively weakened by the
new forces of economic competition. An individual’s economic pro-
ductivity in an open (“impersonal”) competitive market is judged
apart from considerations of his religious affiliation. To the extent
that non-market forms of racial or religious discrimination persist,
those who discriminate against economically efficient employees or
suppliers (or— much more rare—buyers) must pay a price for their
actions: reduced income because of reduced efficiency. The free
market penalizes economically all those who discriminate on any
basis except price and quality of output. Price competition has
always been fundamental to the spread of free market capitalism, 1"!
and Jews became masters of competitive pricing.'!? Jews began to

108. I refer here ta Christian Reconstruction or theonomy.

109, On the proper and improper use of the term “impersonal” to describe market
economies, see Gary North, The Dominion Covenant: Genesis 2nd ed,; Tyler, Texas:
Institute for Christian Economics, 1987), pp. 9-11.

110. “The least prejudiced sellers will come to dominate the market in much the
same way as people who arc least afraid of heights come to dominate occupations
that require working at heights: They demand a smaller premium.” Richard A,
Posner, Economic Analysis of Law (Boston: Little, Brown, 1986), p. 616.

lll. Max Weber, General Economic History, trans. Frank H. Knight (New York:
Collier, [1920] 1966), p. 230.

U2. ‘he common phrase, “he Jewed me down,” points to this phenomenon of the
Jew as a price-cutter. [If one were to say, “he Jowed me up,” it would make no sense.
‘The Jew as the price-cutting haggler is universally recognizable, but not the Jew as
the price-gouger. He is resented by people in their capacity as producers and retail

 
