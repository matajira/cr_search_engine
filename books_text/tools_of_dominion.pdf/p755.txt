The Prohibition Against Usury 747

There are also secondary implications. First, it should be noted
that the servants were required to act on their own initiative for a long
period, The master was not present to tell them precisely what to do.
He imposed a profit management sysiem of cantrol, a bottom-up hierarchy. It
was not the management alternative, a non-market, top-down bu-
reaucracy.”* He wisely decentralized his investment portfolio before
he departed. He allowed his subordinates to make their own deci-
sions regarding the proper use of his capital. He subsequently held
them legally responsible for the results.

Marxism as Covenant-Breaking

What about the person who takes no risks, buries his talent, and
returns to the master only what he had been given initially? This
man has produced losses. He is an evil, unprofitable servant. He has
not performed according to minimum standards.

Like so many other incompetent, slothful people in history, the
servant of the parable tries to justify his poor performance by blam-
ing the master. He accuses the master of being a thief, or at Jeast an
unscrupulous exploiter. “Then he which had received one talent
came and said, Lord, I knew thee that thou art an hard man, reap-
ing where thou hast not sown, and gathering where thou hast not
strawed. And I was afraid, and went and hid thy talent in the earth:
lo, there thou hast that is thine” (vv. 24-25).

What was the slothful servant’s accusation of the master? Clearly,
he was accusing him of being a capitalist. The master is rich, yet he
does not go into the fields to labor. He expects a positive return on
his money, even though he goes away on a journey. In short, éhe ser-
vant is an incipient Marxist. He believes, as Marx did, in the labor
theory of value. He also believes in Marx’s exploitation theory of
profits. Anyone who gets money without working for a living is noth-
ing but an exploiter, living on the labor of the poor. The servant calls
him “a hard man.” (Theologically speaking, this is the covenant-
breaker’s accusation against God: God is an unfair exploiter.)

The master accepts the ideological challenge. He reminds the
servant that he is indeed a hard man, meaning sorneone who has the
lawful authority to establish standards of profitable performance, as
well as the authority to hand out rewards and punishments. He ad-
mits freely to the servant that as a successful capitalist, he does not

74. Ludwig von Mises, Bureaucracy (Spring Mills, Pennsylvania: Libertarian
Press, [1944] 1983).
