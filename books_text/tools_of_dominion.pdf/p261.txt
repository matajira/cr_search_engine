Wives and Concubines 253

Cancelling the Daughier’s Obligation

The dowry functioned in Israel as an alternative to inheritance
by daughters. Sons inherited the family land in the Old ‘lestament,
not daughters. Sons had the responsibility of caring for aged par-
ents, not daughters and sons-in-law.3 To whom much is given, much
js expected (Luke 12:47-48). Since the daughter could not inherit,
she was not obligated to share in her parents’ support. But because
she would not share in her parents’ support, she was not supposed to
receive her dowry from her father’s capital, for this would deplete the
portion remaining to her brothers. The system was consistent.

Normally, the bride price was used to repay the family for the ex-
pense of the dowry. Such a system guaranteed that being a daughter
would not be regarded by her family as being an economic liability.
The bride price kept daughters from draining the inheritance that
normally went to sons. A daughter did not normally remain eco-
nomically responsible for her parents; she became responsible for
her husband’s parents. Why? Because legally she was adopted into the
family of her husband. Thus, inheritances in Isracl went to sons,
who later cared for aged parents, and dowries went to daughters,
who extended their original family’s ethical standards over time,
though not the family’s name.

To enable a girl to leave her father’s household as a free woman —
a wife with a dowry—the bridegroom paid the bride price. Most of
the bride price or perhaps all of it would have passed to his wife as
her dowry. By paying her father the equivalent of the girl’s dowry, he
was relieving both her and himself from the legal obligation to support her par-
ents in their old age. The girl's father would officially provide the
dowry. The daughter would therefore be in a position to take a por-
tion of the family’s inheritance now, indicating her future obligation.
Then the bridegroom would replace the dowry with the payment of
the bride price, thereby relieving her and himself of the future re-
sponsibilities associated with supporting her parents. Her brothers
lost nothing, she gained a dowry, and he escaped the future obliga-
tion of supporting her parents.

Whether she brought a dowry into the marriage or not, the
bridegroom had to pay the bride price to her father or to her
brothers. This indicated that in principle, he owed the family of the
bride some form of service if he was going to be permitted to marry

 

 

3. R. J. Rushdoony, The Institules of Biblical Law (Nutley, New Jorsey: Craig
Press, 1973), p. 180.
