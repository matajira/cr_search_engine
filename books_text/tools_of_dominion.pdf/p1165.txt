The Epistemological Problem of Social Cost 1157

thing with nothing.” Men cannot legitimately fight the Bible’s definition
of property rights with an appeal to circumstances, or to the intuitive
ability of men to assess total social costs and total social benefits—
especially a total cost package that ignores the right, meaning /egal
predictability, of compensation to the victims.

In the case of the problem of social costs, Pigou’s analysis of pollu-
tion and restitution was generally in accord with the Bible’s discussion
of the problem of social cost. The railroad has the legal responsibility
to compensate the farmer for any fire it sets, There will undoubtedly
be problems for a jury or arbitrator in assessing exactly what the
losses were. If the fires continue, then the railroad’s officers can be
sucd for criminal misconduct. Like the man whose ox gains a reputa-
tion for goring, but is not penned up by its owner, so are the railroad
officers who do not take care to protect people from an identified
physical hazard. The formerly docile ox that gores someone to death
must be killed (Ex. 21:28). (Ihe engine would at that point be fitted
with a spark-arrester or prohibited from the tracks.) But the ox with
a bad reputation that kills a man must die, and so must its owner,
unless he makes restitution to the heirs of the victim (Ex. 21:29-30).
(The directors of the railroad could be held responsible in a court of
law for criminal actions for not taking care to install safety equipment
after the fire threat had been pointed out to them by the authorities.)
Biblical case laws are to govern the courts, not the speculative con-
clusions of economists that are opposed to the Bible’s explicit state-
ments. Sometimes very bright economists can come up with outrageous
hypotheses. The public adopts these “logical discoveries” at its peril.
Goase’s essay is regarded by academic economists—at least non-
Keynesian and non-mathematical economists— as a landmark essay.
What it is, on the contrary, is clever sophistry: a land mine essay.

Conclusion

In a brilliant yet almost despairing cssay, Arthur Allen Leff has
described the development of modern legal theory; a war between legal
formalism (the “logic of the law”) and legal empiricism or positivism
(“man announces the law”), The fact is, this debate goes back at least
to the Socratic revolution in Greek political thought: the debate over
physis (nature) and nomos (convention).?# Writes Leff: “While ail this

210. On the rival conceptions of law, see Sheldon Wolin, Politics and Vision: Gon-
tinuity and Innovation in Western Political Thought (Boston: Lite, Brown, 1960), pp.
29-34. On piysis, sec Robert A. Nisbet, Social Change and Histary: Aspects of the Western
Theory of Development (New York: Oxford University Press, 1969), pp. 21-29.
