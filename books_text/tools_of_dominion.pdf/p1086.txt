1078 TOOLS OF DOMINION

Barth’s basis there cannot be a God-revealed permanent Christian eth-
ics, nor civil statutes that conform to fixed biblical principles. Statutes
and creeds are supposedly only the inventions of men, not the appro-
priate human responses to God’s fixed and reliable revelation of Him-
self in a God-inspired historical document. Barth thereby proclaimed
the triumph of Kant’s noumenal trans-historical realm of randomness
over Kant’s phenomenal historical realm of scientifically predictable
cause and effect, all in the name of higher ethics and higher critical
insights. This was Barth’s assertion of the triumph of historical and
ethical relativism over the Bible. This was his announcement of the
triumph of covenant-breaking man over God, and above ail, over
the final judgment. Autonomous man seeks to impose his temporal
judgments on God by denying the historic validity of God’s revelation
of Himself. This, of course, was precisely what Adam attempted to
do in the garden by eating the forbidden fruit in defiance of God’s ex-
plicit revelation. The results are equally predictable.

Permanent Standards for Eternal Judgment

A righteous God who judges men eternally does so only on the
basis of a unified ethical system. Only because the ethical standards
never change could the punishment never change. If the texts are
not ethically unified, then there is no threat to man from the God of
the Bible. Thus, the “prime directive” of higher criticism is to affirm
the lack of unity in the Bible. This is the “higher” critic’s operating
presupposition when he begins to study the Bible.

He adopts a five-step process. First, he assumes that the books of
the Bible are textually jumbled. Second, he tries to prove that the
books of the Bible are textually jumbled. Third, he assumes that
through creative myth-making, he himself can produce a mean-
ingful reconstruction of what the ancient authors (“redactors”) really
wanted to convey to all mankind, despite each one’s short-term goals
of political or bureaucratic manipulation. Fourth, he tries to present
a “deeper” message for modern man that transcends the Bible’s unfortu-
nately jumbled texts. Finally, the higher critic offers Ais verston of the
Bible’s true transcendent ethical unity, Somehow, this newly discovered
transcendent cthical unity always winds up sounding like the last
decade’s political manifesto for social democracy, or else it sounds
like Marxism.

A good statement of this operating presupposition of textual dis-
unity is J. L. Houlden’s remark that “There is, strictly speaking, no
