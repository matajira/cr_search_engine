686 TOOLS OF DOMINION

nacles (so were the landless Levites: Deut. 16:11, 14), as well as to the
third-year tithe (Deut. 14:28-29). These injunctions would have
been enforced by the priests.

Positive Injunctions

To protect these groups, biblical law imposes morally mandatory
forms of charitable giving on the part of neighbors. But there is no
civil sanction attached to this moral obligation. Biblical civil law does
not compel people to do good things for others; it imposes sanctions
on those who do evil things to others, Biblical civil law is therefore
a barrier to the creation of a State-funded, State-mandated wel-
fare system.

Interest payments (usury) are prohibited in the case of a morally
obligatory loan to a poor brother in the faith.2# Thus, since usury —
defined very strictly in the Bible as a charitable loan with an interest
payment attached—is prohibited, the oppressed victim can sue 4
lender in a civil court and recover double damages upon the lender's
conviction, meaning twice the judicially prohibited interest payment.
Such a lawsuit is legitimate because there arc civil sanctions against
specified activities. What the State cannot lawfully do is compel lend-
ers to make charitable loans. God is the enforcer in this instance. He
brings posttive sanctions to those who obey His positive injunction.
“Beware that there be not a thought in thy wicked heart, saying, The
seventh year, the year of release, is at hand; and thine eye be evil
against thy poor brother, and thou givest him nought; and he cry unto
the Lorp against thee, and it be sin unto thee. Thou shalt surely give
him, and thine heart shall not be grieved when thou givest unto him:
because that for this thing the Lorp thy God shall bless thee in all thy
works, and in all that thou puttest thine hand unto” (Deut. 15:9-10).
The State is not authorized by God to bring positive sanctions.

It is not lawful to ask for the cloak of a widow as collateral (Deut.
24:17), but it is legal to ask for a cloak as collateral from a poverty-
stricken Hebrew man (Ex. 22:26). The Bible recognizes degrees of
vulnerability and degrees of responsibility. Farmers are told to per-
mit strangers, widows, and the fatherless to glean the fallen fruit and
unharvested corners of their fields (Lev, 19:9-10; Deut. 24:19-21), but
being a positive injunction, it is not a judicially enforceable law in

24. On the other hand, usury is permitted in loans to religious strangers (Deut.
23:20).
