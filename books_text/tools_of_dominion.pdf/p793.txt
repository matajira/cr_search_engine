26
BRIBERY AND JUDGMENT

And thou shalt take no gift: for the gift blindeth the wise, and per-
verteth the words of the righteous (Ex. 23:8).

God is a righteous Judge. His judgment cannot be purchased by
anyone. He honors His law, not gifts from men. “Wherefore now let
the fear of the Lorp be upon you; take heed and do it: for there is no
iniquity with the Lorp our God, nor respect of persons, nor taking
of gifts” (II Chron. 19:7). He sets the standard for rendering judg-
ment; human judges are to follow it. “Thou shalt not wrest judg-
ment; thou shalt not respect persons, neither take a gift: for a gift
doth blind the eyes of the wise, and pervert the words of the right-
eous” (Deut. 16:19).

The context of this law is olficial judgment rendered by a court.
Judges are not to render false judgment in favor of a poor man (Ex.
23:3) or against him (Ex. 23:6), People are not to offer false witness
in a court against a righteous person (Ex. 23:7). They are not to op-
press a stranger (Ex. 23:9). Such corrupt judicial acts constitute op-
pression, which poinis to the most common source of oppression in
society; a misuse of God’s authorized monopoly of justice, the
courts.! Oppression is therefore primarily judicial: either the court renders
false judgment or else it refuses to prosecute a righteous person's
cause. “They afflict the just, they take a bribe, and they turn aside
the poor in the gate” (Amos 5:12). The court indulges in official sins
of commission or omission. It is supposed to uphold God's mission by
rendering righteous judgment as a means of national and interna-
tional evangelism.

Behold, I have taught you statutes and judgments, even as the Lorp my
God commanded me, that ye should do so in the land whither ye go to possess

1, See Chapter 22: “Oppression, Omniscience, and Judgment.”
785
