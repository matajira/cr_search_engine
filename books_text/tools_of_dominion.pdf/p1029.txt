Maimonides’ Code: Is It Biblical? 1021

whoso confesseth and forsaketh them shall have mercy” (Proverbs
28:13). “Wash you, make you clean; put away the evil of your doings
from before mine eyes; cease to do evil” (Isaiah 1:16). There is no sec-
ond strategy. The Talmud suggests a second strategy: “For R. Iai
says, If one secs that his [evil] yezer is gaining sway over him, let him
go away where he is not known; let him put on sordid clothes, don a
sordid wrap and do the sordid deed that his heart desires rather than

 

profane the name of Heaven openly.”

The wages of sins not recorded in the Book of Judges: “That
wicked wretch |Sisera] had sevenfold intercourse [with Jael] at that
time, as it says, At her feet he sunk, he fell, he lay, ete.”®!

A way to get even with one’s enemies: “In R. Judah’s opinion the
snake’s poison is lodged in its fangs; therefore, one who causes it to
bite [by placing its fangs against the victim’s flesh] is decapitated,
whilst the snake itself is excmpt. But in the view of the Sages the
snake emits the poison of its own accord, therefore the snake is stoned,
whilst he who caused it to bite is exempt.”®

Binding, you may bind: “Raba said: If one bound his neighbor
and he died of starvation, he is not liable to execution... . Raba
also said: If he bound him before a lion, he is not liable. . . .”8?

Their view of women: “ENGAGE NOT IN TOO MUCH CON-
VERSATION WITH WOMEN. THEY SAID THIS WITH RE-
GARD TO ONE'S OWN WIFE, HOW MUCH MORE [DOES
THE RULE APPLY] WITH REGARD TO ANOTHER MAN’S
WIFE.”* Maimonides’ comments do not make the passage any
more acceptable: “It is a known thing that for the most part conver-
sation with women has to do with sexual matters.”® This view is
consistent with the Talmud’s general view of women: “The world
cannot do without either males or females. Yet happy is he whose
children are males, and alas for him whose children are females.”®6
At least one section of the Talmud questions the wisdom of instruc-
ting women in the law: “How then do we know that others are not

80. Mo'd Katan 17a

81. Nazir 23b.

82. Sanhedrin 78a.

83. Sanhedrin 77a

84, Aboth, Chap. 1. This is the famous Pierke Abath, or “Sayings of the Fathers.”

85. Cited by Judah Goldin, The Living Talmud (University of Chicago Press,
1957), p. 55.

86. Baba Bathra 16b.
