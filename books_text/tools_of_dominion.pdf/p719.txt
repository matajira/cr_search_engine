The Prohibition Against Usury 71k

foreigner who owned a business locally. It could have been a busi-
ness contact in another country. It was not a poverty-stricken resi-
dent alien, who was treated by biblical civil law as a neighbor.

Whe Is My Neighbor?

Because all debts to a neighbor are to be forgiven, the legal ques-
tion legitimately arises: “Who is my neighbor?” This was the ques-
tion that the lawyer asked Jesus (Luke 10:29). Jesus answered this
question with His parable of the good Samaritan. The Samaritan
finds a beaten man on the highway. The man had been robbed. He
looked as though he was dead. He was in deep trouble through no fault
of his own. He was on the same road that the Samaritan was travel-
ing. The Samaritan takes him to an inn, pays to have him helped,
and goes on his journey. He agrees to cover expenses. He is the
neighbor. He showed mercy to the man. The lawyer admitted this
(Luke 10:37).

So, the context of the parable is not simply geographical proxim-
ity in a neighborhood. It is proximity of life. Samaritans did not live in
Israel. They had very litte to do with the Israelites, But this
Samaritan was walking along the same road as the beaten man, and
he was in a position to help. He saw that the man was a true victim.
The latter was in trouble through no visible fault of his own. He
therefore deserved help—morally, though not by statute law—but
the priest and the Levite had refused to offer him any help. The
Samaritan was being faithful to the law.

This parable was a reproach to the Jews. They knew what Jesus
was saying: they were too concerned with the details of the
ceremonial law to honor the most important law of all, which the
lawyer had cited: “Thou shalt love the Lord thy God with all thy
heart, and with all thy soul, and with all thy strength, and with all
thy mind; and thy neighbour as thyself” (Luke 10:27). What they
also fully understood was that Jesus was predicting that the gentiles
(Samaritans) who did obey this law of the neighbor would eventually
rule over the Jews, for this is what Deuteronomy 15 explicitly says.
He who shows mercy ta his neighbor will participate in his nation’s rule over
other nations. “Only if thou carefully hearken unto the voice of the
Lorp thy God, to observe to do all these commandments which I
command thee this day. For the Lorp thy God blesseth thee, as he
promised thee: and thou shalt lend unto many nations, but thou
shalt not borrow; and thou shalt reign over many nations, but they
