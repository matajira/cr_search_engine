il
MUMBLE, MUMBLE

And we shall argue, moreover, that if the underlying and environ-
mental economic conditions are such that a government budget deficit is
necessary in order to support the general economic health of the country,
and if there are both policy and analytical reasons why such a budget
deficit should be financed by government loans from the central bank
which involve the expansion of the money supply, then an increase in
the money supply should most definitely be countenanced and encour-
aged, 2%0 much depends on underlying conjunctures of economic forces to
conclude, as North apparently wishes to do, that there is a single cause
or a single cure for inflaiton.

There is, perhaps, no more controversial topic in economic
thinking today than the meaning of inflation. As someone has ob-
served, where there are five economists you have six opinions.
The error that can be made at this point, however, is the conclu-
sion that because such diverse opinions arise, it is therefore im-
possible to make our way through the forest of ideas to ascertain
what is, and is not, sound economic reasoning. If this is true, then
the best thing to do is nothing, until we understand more. When
we do not understand an economic process, then we should turn it
over to the free market for resolution, not over to a bunch of bu-
reaucrats with guns.

1, Economics and Man, pp. 178-79.
177
