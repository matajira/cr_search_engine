xxii Baptized Inflation

tremely bad taste, and furthermore, the headline was in very
large print, which is not scholarly, and it should have read, ‘Close
Acquaintances in a Friendly Hug.’ You owe them both an apol-
ogy.” There is a P. S., too: “I notice that no respectable scholarly
press was willing to publish this scurrilous attack, which proves
that you are way out of line .” No, it only indicates that the editors
of the scholarly presses are not innocent bystanders. They guard.
the hall of the motel while the couple is otherwise engaged; they
try to head off photographers.

These mild-mannered drones are defenders of the revolution-
ary destroyers of Western civilization. They think they will all be
safe and sound when this civilization falls. They act as though
they believe that their tweed jackets are bulletproof. They think
the revolutionary conflicts going on today are the equivalent of a
monthly debate in the faculty lounge. They are even naive
enough to imagine that the agreed-upon position statements of
the latest faculty club meeting are relevant to anyone, anywhere,
anytime soon. They think that the great issues of the world will be
settled in the pages of scholarly journals.

They are wrong. The only position statements that can make
it through the faculty committees in today’s Christian liberal arts
colleges are watered-down conclusions that were dropped as un-
workable (or no longer useful in a program of planned deception)
by the liberal intelligentsia ten years earlier. If you doubt me, wait
until you read Jan Hedge’s account of what happened to Dr. Vick-
ers’ “revolutionary new ideas” long before Dr. Vickers went into
print with Economics and Man. The delightful fact is that Dr. Vick-
ers still hasn’t figured out what his younger humanist peers did to
him. They did to him what he did to his elders decades ago. They
left him behind in the dust, or the academic economists’ equiv-
alent of dust, stochastic equations.

Over the Falls With the Mainstream

Herbert Schlossberg, whose Idols for Destruction is a footnoted,
gentlemanly, academically acceptable hand grenade against mod-
ern campus liberalism, has the trust of the neo-evangelical aca-
