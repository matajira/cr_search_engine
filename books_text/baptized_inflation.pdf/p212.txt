Mumble, Mumble 191

“original universal stuff”). This leads to the unfortunate con-
clusion that the world is impersonal, that there are impersonal
forces that bring about “whatsoever comes to pass.” By denying
the all-controlling God of Scripture who determines whatsoever
comes to pass, men transfer predestination to some other force or
object. In the mind of twentieth-century man, chance controls
whatsoever comes to pass. Conveniently, this ultimately denies,
unlike Biblical predestination, the validity of human choice. We
are not responsible, says modern man, for our actions. (“The ran-
dom, impersonal universe which chance gave unto me bath made
me to sin.”) This has had disastrous effects in the study of econom-
ics.*

The denial of the law of supply and demand has reinforced the
popular notion of evolutionary Darwinisrn: that whatever happens
is caused by chance, not deliberate human action. Consequently,
price inflation is seen as the product of chance. Sometimes we
have it, and sometimes we don’t. After all, don’t our politicians
say they are doing their best to control price inflation? Don’t we
hear their self-adulation at bringing present (early 1986) price in-
flation rates down to “tolerable” levels of around five percent? Are
we not promised, especially at election time, that their utmost
efforts will be given to control price inflation?

Evolutionary thought thus plays into the hands of power-
hungry politicians who, committed to the Keynesian fallacies,
would rather impose their ridiculous fiscal and monetary policies
on an ignorant and gullible public. But as Peter Drucker notes, “It
is simply not true, as is often asserted, that economists do not
know how to stop inflation. Every economist since the late 16th
century has known how to do it: Cut government expenses and.
with them the creation of money. What economists lack is not
theoretical knowledge, it is political will or political power. So far
all inflations have been ended by politicians who had the will

8, The best defense, and the most delightful reading, of biblical predestina-
tion is Martin Luther's Bondage of the Will (various translations).
