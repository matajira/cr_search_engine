136 Baptized Inflation

Useless Work

The first notable feature of Keynes’ vivid analogy is the use-
lessness of the work. The Treasury could spend the money into
circulation by paying for public housing projects. That was what
he really preferred: government-built houses. But digging up
filled-in holes was satisfactory.

The point to bear in mind is that when working men dig up
coal, it is because they believe they will meet a consumer’s de-
mand for coal. It is not the activity, but rather the consumer need ful-
filled, which is crucial in a free market arrangement. Digging up
bottles filled with paper money is useless expenditure. Instead,
dig for coal. But if the mine has played out, and it costs too much
to dig it, then stop digging. You can just hand out the counterfeit
money But stop digging. You are wasting time and money in dig-
ging. Yes, you get some coal, but it costs too much.

True, the government could construct houses. But this is no
different from digging up coal which is too expensive to extract in
terms of what it will bring on the free market. Do not build houses
that would not otherwise sell. Do what the coal miners had to do:
stop building. You can just hand out the counterfeit money But
stop building,. You are wasting time and money in building. Yes,
you get some houses, but they cost too much.

Professionally Managed Counterfeiting

The second notable feature of his clever analogy is that the
Treasury must print the notes. Why not private counterfeiters?
He did not say. I think he might have replied: “People want to
trust the government. They don’t want to be arrested for dealing
in counterfeit notes.” So why not rewrite the law and allow private
counterfeiting? “Because the government policy-makers must
guide the overall creation of money, in order not to allow mass in-

 

of effective demand. Keynes, probably to his credit, saw and provided what was
needed — enthusiastic polemics, sardonic passages, bits of esoteric and even
shocking doctrine.” Yeager, “The Keynesian Diversion,” Westen Economic Journal,
XI (June 1973), p. 150.
