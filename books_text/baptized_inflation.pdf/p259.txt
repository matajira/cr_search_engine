240 Baptized Inflation

to a store-owner (taxpayers) giving his customers money to spend
in his store, but which they go and spend somewhere else instead
(ew York banks). It makes economic sense only for the multina-
tional bankers and their hired lackeys: economists, politicians,
and media spokesmen.

Dr. Vickers’ failure to understand both monetary theory and.
Biblical teaching concerning debt causes him to assert that debt is
not such a bad idea, and that national budget deficits are accept-
able when related to the nation’s productivity and ability to pay.
He cites comparative statistics to defend this view. !

Howard Ruff pointed in 1979 to the manner in which national
figures are manipulated to produce a result which is not, in fact,
anywhere near the truth of the situation. Quoting from the Sep-
tember 30, 1976 Treasury Department hi-annual report “State-
ment of Liabilities and Other Financial Commitments of the
United States Government: which tables the liabilities of the
U.S. Government (which should more accurately be called a lia-
bility of the U.S. people, for they must eventually pay the bill),
Ruff observes that the total debt for that year was closer to $6 tril-
lion than the more commonly referred to figure of $650 billion. In
addition, “Arthur Anderson, the largest accounting firm in the
world, recently completed a study of the U.S. Government’s
accounting methods. They urged the government to do just like
corporations do — use an accrual system that matches assets with
liabilities. According to Anderson, if they used standard corporate
accounting figures for fiscal 1974, it would show that the U.S.
Government really ran a budget deficit of $95 billion, not the $3.5
billion they reported. This kind of accounting would highlight for
the public and our legislatures, particularly the free-spenders, just
how horrible the situation really is... . By no stretch of the
imagination can the United States government be called solvent.
The big question is: How long can it continue to get away with it?
Only until such time as the public refuses to give them any more
money, having discovered Uncle Sam is a dead-beat who has

1, Vickers, Economics and Man, p. 829-80.
