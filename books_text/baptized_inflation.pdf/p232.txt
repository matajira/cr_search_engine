212 Baptized Inflation

raise their prices at least as much as the wage increases in order to
maintain profitability.

But wait a minute. How could they all raise their prices? Yes,
yes, Dr. Vickers, we know: they are all incredibly greedy. But
the y were all incredibly greedy yesterday, too, in your view. So
why didn’t they all raise their prices yesterday? Because we consum-
ers would not have bought all of those higher priced goods they brought to
market, We will not buy them today, either . .. unless the government
creates sufficient fiat money to let us do it. And this, my friend, is why
we are back to the Keynesian solution. Print money.

So the government prints the money (or allows the fractional
reserve banking system to create it). How will the workers be bet-
ter off? The producers are still producing the same quantity, but it
is being sold at higher prices. Workers have received higher
wages, but they now pay higher prices on average for goods and.
services. How are they better off? The answer is, of course, they
are no better off at all. Nothing has changed in this situation ex-
cept that the dollar amounts being transacted are at higher levels
than before the general wage increase was granted.

Oh, yes, now that I think of it, something has changed. I for-
got about Dr. Vickers’ enthusiastic recommendation: the gradu-
ated income tax. The higher a worker’s money-denominated in-
come, the larger the percentage of his income is taken “off the top”
by the tax collector. So I was incorrect. Almost all workers are worse
off. They will suffer a reduction of their after-tax real income.

It is the old double whammy: Keynesian-recommended mon-
etary inflation produces Keynesian-created price inflation, which
opens the door to the Keynesian-approved graduated income tax to
achieve its confiscatory mission more efficiently than ever. Ke ynes-
ianism increases efficiency ... of the State to destroy freedom.

Price Controls

But our Great Benefactor may realize that prices will go up in
response to his monetary policies. He knows that producers will
raise their prices, thereby destroying the effects of wage increases.
The obvious solution to the problem is to prevent prices from
