214 Baptized Inflation

mining whether labor is worth that particular price. Furthermore,
trade unions are allowed to impose their demands upon employ-
ers, in order for union members to extract above-market wages
from employers. These above-market wages are possible only
because of State-permitted exclusion of non-union potential employ-
ees, If he hires some of these State-excluded workers, the employer
might well have to face a government tribunal. The employer is
told to deal with union members; he cannot legally replace them
with non-union members. This penalizes non-union workers, and
it forces them into lower-paying jobs that are made available by
non-union employers. Pro-union legislation is therefore an indirect eco-
nomic subsidy to non-unionized business owners. The new wage rates
means a loss in profits for union-dominated employers. It might
well force some out of business altogether. But it all adds up to one
thing: less labor being employed than what an unhampered free
market would support.

Keynesian economists tell us they are for the working man.
They may be for him, but their recommended policies are reduc-
ing the legal employment opportunities of most of them. Most
people (about 75% of them) are not employed by unionized busi-
nesses in the U.S. But they are not allowed to bid against union
members in major industries. So they are forced to seek employ-
ment in non-union sectors of the economy. Non-union employers
can then offer lower wages than they could otherwise have
offered, had there been no compulsory unionization of their com-
petitors’ businesses. Here is a classic example of what Bastiat
called the fallacy of the thing unseen. We can see the above-market
wages of union members — at least of those whose companies have
not yet been bankrupted. But we cannot see that these above-
market wages are being paid for by the loss of freedom to bid for
these high-paying jobs on the part of the average working man.

Market-Created Unemployment

We do not live in a perfect world. Unemployment can never
be eliminated. There are those who would rather be unemployed
and collect social welfare benefits than take on the responsibility
