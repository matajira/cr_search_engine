Economic Law 107

reason to be confident in the effective operation of the “invisible hand’ of
Adam Smith and the efficacy of the assumptions of automatic harmony
on the basis of which the classical and neo-classical schools of economic
thought proceeded. As we have seen, a more perceptive reading of eco
nomic history shows that a system of uninhibited laissez faire has never
existed and cannot properly be expected to exist, The proper definition
of the economic functions of the God-ordained institution of the state
will therefore figure prominently in our analysis. °

Thate to bring it up again, but classical economics did not rest
on the assumption of any “automatic harmony.” No textbook ac-
count of the history of economic thought argues that classical
economists (except the pamphleteer Bastiat) ever used such argu-
ments. What classical economists did argue is that the competitive
market pressures that are produced by the quest for profit serve as
limiting devices. This free market competition places pressures on
businessmen to serve the needs of customers. (If this argument isn’t
the essence of Adam Smith’s Wealth of Nations, what is?) Not only is
there no “automatic equilibration,” the very Jack of “automatic equi-
libration” in the market is what offers the hope of profit and the
threat of loss to businessmen. If it were automatic, there would be
no entrepreneurship, no seeking out of profit opportunities. 6

The classical economists certainly argued that there is a tend-
ency for prices to become equal, and for the rate of return on in-
vestments to become equal, and for wage rates to become equal

for the same goods, services and riske, They argued — you won't believe
this! — that if I can market a product for an ounce of silver per
unit, and I try to sell it for five ounces, while you can also market
it for one ounce, you will have an incentive to take away some of
the sales I would generate. You will probably attempt this by sell-
ing units for something under five ounces a piece. This is the essence
of the classical economists’ argument, It was Keynes’ “revolution” (fol-
lowing Parson Malthus, Chalmers, and a few other lesser-known
“temporary general glut” economists) to deny that the market stead-

5, Ibid., p. 195,
6, Israel Kirzner, Competition and Entrepreneurship (Chicago: University of
Chicago Press, 1978).
