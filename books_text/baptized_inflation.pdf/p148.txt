126 Baptized Inflation

was by 1967 already out of fashion, not taken very seriously by the
economics profession, and basically a thing of the past. Second,
its members had been banned by their peers to the Siberia of the
profession: teaching undergraduates and writing textbooks. Time
plays cruel tricks on once-young intellectual revolutionaries. It
turns them into fuddy-duddies. The methodological expropriators
are expropriated.

Who is Axel Leijonhufvud? He is the author of the major rein-
terpretation of Keynes in this generation, which appeared a year
after his essay.? Furthermore, the essay appeared in the American
Economic Review, the most prestigious of all the professional eco-
nomics journals in the United States, and therefore in the world,
given the influence of U.S. economists. Leijonhufvud was an-
nouncing the demise of the previously reigning branch of Keynes-
ianism, and he was doing so in the confidence that the bulk of the
profession understood th-at he was correct. Those who refused to
recognize it were precisely the aging holdouts whose opinions no
longer counted.

What is ironic about the post-Keynesian revolution which
buried the older Keynesians is that the older men continue to
write as if they were still on the cutting edge of revolution, rather
than under the cutting edge. They are rather like those aging neo-
classical scholars in the late 1930’s and early 1940’s who expected
“this Keynesian nonsense” to go away after the War. The major
non-Keynesian scholars did recognize the challenge of The General
Theory and tried to challenge it. They understood that a methodo-
logical earthquake was underway. It was the classroom teacher on
the fringes of the profession who never quite knew what had hit
him. So it is today. Dr. Vickers, as late as 1982, was still writing as
if he were in the army of the innovators, in hot pursuit of the
neoclassical Philistine:

The outlines of this discontent are clear on only a minimal inspec-
tion. An earlier and comfortable orthodoxy has been shattered, and new

8. On Keynesian Economics and the Economics of Keynes: A Study in Monetary Theory
(New York: Oxford University Press, 1968).
