The Great Unmentionable: Unemployment 213

being raised by sellers. Let’s have wages increased, but all other
prices remain unchanged. Make it illegal to raise prices.

Now what is the situation? Wages and salaries are up, but the
producers cannot raise their prices to recoup costs, so they find al-
ternatives to the now-overpriced labor. They mechanize produc-
tion (if they can buy the equipment before the manufacturers go
out of business), thereby employing less labor, or they demand in-
creased productivity to pay for the increased wages. They start fir-
ing workers who do not improve productivity. Whichever choice
employers make, it involves a fundamental calculation that the
present labor rates are too high,

So what is the result? Unemployment. We are right back where
we started. So much for the Keynesian miracles.

Cause and Effect (Again)

Perhaps now we are in a position to inquire what causes un-
employment. Is it wage reductions? Yes, it could well be, if pro-
ducers do not lower prices to induce sales. But to get sales, they
will lower prices. So unemployment goes away.

Is it wage increases? Again, it could be, if producers are not
able to increase their prices to recoup production costs. So they
fire workers, and unemployed workers drop their wage demands,
and they get hired again, and unemployment goes away.

Tn reality (though perhaps not in the academic departments of
tax-supported universities), wages are paid out of productivity. When
wages levels are greater than the value of labor’s output, labor is
overpriced and will not be purchased.

Today's Unemployment: Compliments of Keynes

Then what is the cause of the persistently high unemployment
which is afflicting the U. S., Great Britain, Western Europe, and.
Australia? It is not just limited to these countries, but these exam-
ples will serve our purpose here. Very simply, the cause of unemploy-
ment is the politicians’ refusal to consider the effects wage rates have within
the overall context of the price mechanism. They legislate minimum
wage laws, thereby denying the employer the prerogative of deter-
