Copyright ° 1986
Ian Hedge

Printed in the United States of America
ISBN 0-980464-08-7
Library of Congress Catalog Card Number 86-081800

Typesetting by Thoburn Press, Tyler, Texas

Published by
Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711
