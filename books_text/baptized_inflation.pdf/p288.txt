Conclusion 269

If ever there was a book which could not conceivably be
described as ‘crisp and lucid,” it is Economics and Man. If ever there
was an incongruous intellectual patchwork, it is Douglas Vickers’
attempt to fuse the Bible and Keynes. Keynes would have been
appalled — indeed, as I am, but for different reasons: he for the
stain of the Bible, and I for the stain of Keynes.

Final Remarks

Economics and Man never gained any influence. Conservative
Christians had no use for baptized Keynesianism, and liberals
had no use for Dr. Vickers’ affirmation of Van Til’s anti-humanist
epistemology. The book is a classic example of a desperate at-
tempt by an intellectually compromised Christian academic to
synthesize opposites: his religious faith and his academic faith. As
always, the academic faith won out. We have seen this story
repeated endlessly in every little Christian college in the land,
with their officially certified Ph.D.-holding scholars, and their
vain quest for academic respectability. Faculty members have sold
their theological birthright for a mess of pottage, just as Dr. Vick-
ers sold his.

Economics and Man is notable only for its failure — intellectually,
stylistically, and above all economically. It sank without a trace,
except for a lecture or two at Dr. North’s old alma mater, West-
minster Theological Seminary. That public appearance did noth-
ing noticeable for the sales of Economics and Man.

Dr. Vickers devoted his academic career to becoming a “front
man” for an academically successful religion. The founder of that
religion was a homosexual who had a very short-run view of life.
(Given the Biblical doctrine of final judgment, he was certainly
entitled to such a view. ) He began his career as a logician and a
brilliant essayist and polemicist. The more he wrote, the less co-
herent he became. We can see in his writings the earthly judgment
of God. There was a progressive deterioration in his ability to
communicate his ideas — not in making converts in an increas-
ingly perverse intellectual and political world, but in setting forth
his ideas in a way that his followers could repeat them, predict the
