210 Baptized Inflation

have been cut somewhat, and he can therefore reduce selling
prices a little, yet still maintain his profit margin. What has he to
lose by trying such a tactic? Absolutely nothing. His sales are
already down. His wages and salaries bill is down. He has noth-
ing to lose, and everything to gain, by reducing his prices. Perhaps
that price reduction will be the temptation needed to induce pros-
pective buyers to reconsider the purchase of a television. So we
must disagree most strongly with Dr. Vickers when he asserts
there is “only one course of action which economic reason” sug-
gests. What we need to ask is whose economic reasoning is suggest-
ing what. The manufacturer's reasoning tells him to lower his
prices if he wants to sell the goods.

This may not always work. If there is simply no demand for
the good at any price, it has ceased to be an economic good, Per-
haps the producer has no possibility of lowering the price far
enough to induce retail customers to buy. As an example, con-
sider Economics and Man. After ten years, the publisher has yet to
sell out the initial run of 2,000 copies, even at the low, low price of
$6.95. What should he do? Sell all copies at once for a few cents
each to a “remainder” book distributor? Give it away as a prem-
ium for buying five (or four, or even two) other books? Or wait for
Baptized Inflation to create enough controversy to get the last 250
copies sold? Entrepreneurs have difficult decisions in this life. But
that is the price they pay for remaining entrepreneurs.

In Dr. Vickers’ hypothetical example, we can see that a lower-
ing of the wage rate is not necessarily a cause of unemployment.
(Economic analysis tells us that falling wage rates are almost
never the cause of unemployment, and never the cause of permanent
unemployment. Falling wage rates are the economically rational
response of employers and employees to falling demand for their
output. Falling wage rates are a rational attempt to maintain em-
ployment. They are a way to keep from going bankrupt or getting
fired. ) There may be some éemporary unemployment if manufac-
turers and producers are slow to realize that sales will decline,
since workers now have less spending money due to the general
wage decline. But not all businessmen are that slow in under-
