252 Baptized Inflation

ward its realization .”?

This is to be achieved, according to Dr. Vickers, by the
“proper regulatory function of the government... . 73 “New
guidelines” are to be laid down circumscribing the actions of indi-
viduals, those nasty villains who keep making life difficult for
everyone else.4 Failure to act would be ‘economically criminal,
and against the basic objectives of conservation, development,
and equity, as we have seen these adequately sustained and con-
firmed by scriptural data... . "5

What we have here is capitulation to the idea that man’s eco-
nomic problems can be solved by government action, by control and
regulation of the marketplace. There is implicit faith that all prob-
lems, and especially those of an economic nature, are political
problems and can therefore be solved by political action.‘ I exper-
ienced a vivid illustration of this in a discussion with the principal
of a Reformed theological college who acknowledged that there
were employers paying “unjust” (by that he meant low) wages,
and therefore there should be legislation establishing a minimum
wage. Ignoring for the moment the inherent difficulty of defining
what is a “just” wage, his suggestion is that the solution to the
problem is government action. An economic problem is automatic-
ally perceived to have its answer in the domain of politics and leg-
islative decrees. But let us grant for a moment that there is an ele-
ment of truth in this illustration. We can readily admit there are
employers paying low wages. However, are minimum wage laws
the solution to the problem? And what, really, is the problem in
the first place?

2, Clarence B, Carson, The World in the Grip of an Idea (New Rochelle, New
‘York: Arlington House, 1979), p. 9, emphasis in original,

8, Vickers, Economics and Man, p. 343,

4, Ibid, p, 887.

5, Ibid., p. 822,

6, See Jacques Ellul, “Politicization and Political Solutions” in Kenneth 8,
Templeton, dr. (ed.), The Politicization of Society (Indianapolis, Indiana: Liberty
Press, 1977), p. 209f.; ef, Clarence B, Carson, The Flight From Reality (Irvington-
on-Hudson, New York: Foundation for Economic Education, 1969), p, 351f.
