248 Baptized Inflation

sary to re-establish the free market. Pride, human ambition,
power, greed — the evils that Dr. Vickers says precludes free
market economics — are all factors that must be overcome by pol-
iticians and Keynesian economists who have the arrogance, like
Dr. Vickers, to believe they are capable of getting us out of this
mess. The truth is that they got us into this mess by trying to get
us out of one which did not have the magnitude of the present
crisis. So there seems no real reason to believe this alternative — a
return to Biblical economics, or even the classical economists’
imitation — is about to happen. In addition, to return to sound
monetary policies, to stop monetary inflation, would produce an
immediate depression while the economy readjusts itself to the
reality of the zero-inflation situation. There is no country on earth
whose voters will accept such a scenario. They prefer to be fooled.
for a while longer, until the inescapable monetary crisis overtakes
them.

Second, the remaining alternative is to continue the present
policies, albeit with some fear and trepidation. As this seems the
most likely course of action in the immediate future, it is worth
considering the likely effects of such a possibility. In passing it is
relevant that Federal Reserve chairman Paul Volcker has sur-
prised most people for the past seven years and has kept reins on
the money supply. He of all the Federal Reserve’s staff seems most
aware of the situation and the need for a check on the money sup-
ply growth. Consequently, 1980-85 produced a recessionary cycle
in the market, albeit with some tendency at times to show signs of
growth. With the 1980 election of Ronald Reagan, there was rhet-
oric to the effect that monetarism was the new policy, and that
there would be severe control over monetary growth. How much
longer that will last is anyone’s guess. But as it was more rhetoric
than substance, any financial responsibility practiced is bound to
be short lived.

A recent article in The Economist by professor James Tobin of
Yale University, winner of the 1981 Nobel Prize for economics, has
announced the death of monetarism and the resurgence of Keynes-
