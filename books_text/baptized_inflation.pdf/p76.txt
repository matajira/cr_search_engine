52 Baptized Inflation

This intellectual defense of this market process has been known to
professional economists for over a century.®

Marx's Argument

To call the free market “economic anarchy,” or to say that it is
a “rampant and potentially anarchic individualism,”’ is mere
hyperbole on the part of Dr. Vickers. It was also hyperbolic on the
part of Karl Marx, in Das Kapital (1867), when he argued that the
“anarchy in the social division of labor” required an “authoritative
plan” to bring coherence.® It was equally hyperbolic when Freder-
ick Engels, the co-founder of Communism (Marx’s associate), ar-
gued the same way in 1877: “The contradiction between socialised
production and capitalistic appropriation now presents itself as an
antagonism between the organisation of production in the individual work-
shop and the anarchy of production in society generally.”? Hyperbole is not
economic analysis.

Iam of course not arguing that Dr. Vickers is a “closet Marz-
ist.” What I am arguing is that there is a familiar theme in the
writings of those who propose that the State’s bureaucrats serve as
overall planners for the market, whether socialists (State owner-
ship of the means of production), fascists (private ownership with
State control), or interventionists (State manipulation of the econ-
omy through taxation, spending, and monetary policy). They all
come to the market with a built-in bias, which in turn is based on
a view of God and His creation. They assume that “rationalism” is
essentially a top-down phenomenon. They assume that anything
which is not top-down rationalism is essentially irrational. It is
this perspective, which F. A. Hayek has battled for over four dec-

6, Carl Menger, Principles of Economics (New York: New York University
Press, [1871] 1981), pp. 114 f.

7. Vickers, op. cit,, p. 125; ef. p. 190.

8, Marx, Capital (Modern Library edition, reprint of the 1906 edition), pp.
891-92,

9. Engels, Socialism: Utopian and Scientific (New York: International Publishers,
1985), p. 61, This was an extract from En gels’ book, Herr Eugen Dishring’s Revolu-
tion in Science (Anti-Dihring), published in 1877.
