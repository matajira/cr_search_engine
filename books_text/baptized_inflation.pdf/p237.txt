The Great Unmentionable: Unemployment 217

the Keynesian system, the level of wage-rates and their effect on
employment, is The Great Unmentionable.“10 Regarding Dr.
Vickers’ contribution to Keynesian thought, the price mechanism
in general, of which wage rates and their effect form a part, is the
Great Unmentionable. Such scholarship, rather than enlighten-
ing the reader, has instead a propensity to confuse. And that in it-
self requires another chapter.

10. Hazlitt, The Failure of the “New Economics, * p. 331.
