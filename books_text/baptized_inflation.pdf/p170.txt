Says Law 149

Why is Dr. Vickers so interested in this bit of “ancient history”?
Because Keynes’ supposed refutation of Say was the very heart of
his supposed refutation of classical economics. If supply creates its
own demand, then why did the great depression of the 1930's go
on for a decade? Why couldn’t the supply brought to market be
sold?

The answer, of course, is that the prices asked by sellers were
too high. But why did prices stay so high? Why don’t sellers even-
tually offer lower prices? Why was Say’s Law “nullified” during
the Great Depression? Because government policy favored price floors,
meaning cartel prices. The State punished sellers who adopted pol-
icies of “cut-throat competition.” This explanation of the first four
years of the depression in the United States was what led Keynes-
ian economists to dispatch Murray Rothbard into professional
“outer darkness “9 Twenty years later, Rothbard’s economics-
based explanation was resurrected by popular (and eloquent) his-
torian Paul Johnson, in Modern Times (1983). The explanation
offered by Milton Friedman in that same year, 1963, was that
prices were too high as a result of Federal Reserve (central bank)
monetary policy, 1929-33 '°— a more acceptable theory to aca-
demic economists who generally favor monetary inflation.

What Say had argued is that if prices are allowed to move upward
or downward, supply creates its own demand. More to the point,
supply is demand. Put bluntly, if you have nothing to offer in ex-
change, you do not become a part of “aggregate demand.” Or, in
words that Dr. Vickers would regard as stylistically vulgar, “If you
ain’t got it to start with, you can’t buy nothing with it.”

Keynes’ Strategy
It was this explanation of how markets work both in theory
and in practice which Keynes had to refute in order to make way

9. Murray N. Rothbard, America’s Great Depression (Princeton, New Jersey:
Van Nostrand, 1968).

10. Milton Friedman and Anna J, Schwartz, A Monetary History of the United
‘States (Princeton, New Jersey: Princeton University Press, for the National
Bureau of Economic Research, 1968).
