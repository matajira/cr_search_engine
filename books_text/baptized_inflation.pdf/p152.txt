130 Baptized Inflation

we can both get together and vote out the government. Then we can
trade. Or we can ignore the government, increase our risks of ac-
tion, and trade anyway.

The Great Depression

What gave the Keynesian revolution its market in 1936 was
the fact that for over six years, people all over the Western world.
had not been trading very much with each other. All the unplea-
sant results of a collapsing division of labor were manifesting
themselves. People’s incomes and their net wealth dropped. Why
had this happened? More important to the average politician,
what could be done about it? Actually, the average politician had
already decided what should be done: spend government money.
Keynes came along in 1936 to provide the academic justification
for what they were already doing. (It took a world war to get the
spending up, and the price controls on, sufficient to placate the
voters. ) Mises is correct:

The policies he advocated were precisely those which almost all gov-
emments, including the British, had already adopted man y years before
his “General Theory” was published. Keynes was not an innovator and
champion of new methods of managing economic affairs. His contribu-
tion consisted rather in providing an apparent justification for the pol-
icies which were popular with those in power in spite of the fact that all
economists viewed them as disastrous. His achievement was a rationali-
zation of the policies ah-cad y practiced. ®

What had happened was that governments had done too
much for the people... earlier. They had inflated their curren-
cies from 1914 onward. They had suspended the gold standard
when the First World War began. They had agreed to an infla-
tionary version of the international gold standard, called the “gold
exchange standard ,“ at the Genoa Conference of 1922. Britain
went back on the gold standard in 1925, but at an artificially high
exchange rate for the pound, pretending that the pound was what

8, Ludwig von Mises, “Lord Keynes and Say’s Law” (1950), in Planning for
Freedom (South Holland, Ilinois: Libertarian Press, [1952] 1980), p. 69.
