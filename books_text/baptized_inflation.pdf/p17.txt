ax Baptized Inflation

position. They have ridden roughshod over two generations of
college students who fear being flunked out of school if they voice
opinions counter to the New Deal economics of their professors.
The professors have had a free ride, especially Christian college
professors. Until the Christian reconstructionists started pound-
ing on them, nobody paid any attention to them except an occa-
sional conservative donor, and he didn’t have an earned Ph.D.
from an accredited state university. Now that the free ride has
ended for a few of them, the frightened mice are banding together
in helpless outrage. “You can’t do these things!” Yes, we can.
After all, Jesus did. I am presently writing a manuscript that I
intend to call, Thou Hypocrite! Jesus’ Tactics of Direct Confrontation.
He called the Pharisees whited sepulchers. He called them hypo-
crites. He called Herod, a civil magistrate, a fox, and a female fox
at that. He did everything He could to embarrass his opponents in
front of their formerly helpless followers. Jesus was one of the
most effective verbal street fighters in history, and He was cruci-
fied for it. Yet today’s academic wolves in sheep’s clothing pretend
that He was president of the International Association of Wimps.

The Academic Seduction of the Innocent

These classroom compromisers are the products of a century
of classroom humanism. They have immersed themselves in the
humanistic methodologies of the secular universities that granted
them their advanced academic degrees. They have done every-
thing within their power to screen out students and rival faculty
members who come before them and announce that every aca-
demic discipline must be reformed, top to bottom, in terms of
what the Bible says. They will not allow such people to teach at
their colleges. They will not grant them tenure. They resent the
fact that others say in public that they, the compromisers, have
sold their spiritual birthrights for a mess of tenure.

They worry most of all when they think about the very real
possibility that most of the donors to their colleges are probably
closer in their opinions to the reconstructionists than to the cam-
pus faculty club. Why, the donors might .. . they just might...
