6 Baptized Inflation

head of Margaret Thatcher’s Conservative Party “think tank,” Prof.
Brian Griffiths, an economist at the City University of London, is
also a Christian. He acknowledges privately that he had never im-
agined that there could be such a thing as a specifically Christian
economics until he read Gary North’s Introduction to Christian Eco-
nomics in the late 1970's. He has now written Christian-oriented
materials on economics.’

This perspective could be categorized, broadly, as one of a
“free market” or /aissez-faire economy, although it would be a grave
error to omit mentioning that these authors insist that Scripture
be brought to bear on what has been traditionally known under
these titles. For these authors, “free market” does not mean an-
archy, since Scriptural principles provide the moral and legal
framework within which a /aissez-faire approach is adopted, and
from which Western capitalism originated.* Christian economic
thinking demands a rejection of economic theories that do not
stand the test of three standards: the Bible, internal consistency,
and historical verification. It therefore demands a rejection of
Keynesian economics.

The “Flexible” Legacy of Keynes

Modern economics is the economics of John Maynard Keynes,
the son of a Cambridge University economist, John Neville Keynes.
(The name is pronounced “Canes,” as in candy canes.) The younger
Keynes has been the most influential economist of this century and,
as Gary North reminds us, this speaks poorly of this century. It
matters little where we study economics outside of the Iron Curtain;
we can be sure that at least some of the theories of Keynes will be
presented and actively promoted as being the panacea for the ills

7, Brian Griffiths, Morality and the Market Place: Christian Alternatives to Capitaliem
and Socialism (London: Hodder and Stoughton, 1982); The Creation of Wealth (Lon-
don: Hodder & Stoughton, 1985),

8 By the torm Jaissez-faire is meant the general principle of non-intervention in
the economy by coercive organizations such as government-protected trade
unions, It includes non-intervention by government officials except for the pun-
ishing of those who indulge in theft, fraud, or coercion,
