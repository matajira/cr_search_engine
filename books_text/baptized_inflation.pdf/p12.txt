Foreword xu

to write a one-printing dud, but this is ridiculous! Maybe Baptized
Inflation will enable Craig Press to unload those last 250 copies.

Hedge vs. Hedge Podge

In short, it just wasn’t worth my time to write a response. So I
forgot about Economics and Man — certainly an easy thing to do.
Then, in 1985, Ian Hedge sent me the manuscript of Baptized In-

Jfiation. It was so delightful that I couldn’t resist publishing it, now

that I have my own outlets for books. Here is a book which not
only destroys Dr. Vickers’ arguments, but does so in a lively style,
with plenty of documentation, and with arguments so clear that
even a Harvard economist can follow them.

This book is similar to David Chilton’s critique of Ronald
Sider, Productive Christians in an Age of Guilt-Manipulators. It is not
only a critique, but a positive statement of just what biblical free
market economics is all about. It does little good to publish criti-
ques of obscure, unreadable books, unless the critique provides
insights into what is right. There is so much error in this world,
especially in the Ph.D.-holding, self-certifying, risk-free, tax-
supported academic world, that the only excuse for responding to
any segment of it is that out of the refutation will come positive
knowledge of the truth. That is what the reader will get when he
reads Baptized Inflation.

Hedge answers all of the Keynesians’ favorite myths: that the
free market cannot clear itself of unsold goods; that monetary in-
flation is a positive benefit to society; that the gold standard is a
“barbarous relic’; that Keynes somehow “refuted” Say’s Law; that
monetary demand creates economic production; that saving isn’t
necessarily a productive thing; that we can “spend ourselves rich”;
that the free market needs a “visible hand” of a central planning
bureaucracy to make it fair, efficient, and “Christian”; that
“deficits don’t matter”; that the free market is inherently unstable
(random); that the free market is also inherently stable (with sig-
nificant unemployment); and on and on.

The book also makes plain Dr. Vickers’ real concern: that
Christians might begin to take seriously the Bible, especially bib-
