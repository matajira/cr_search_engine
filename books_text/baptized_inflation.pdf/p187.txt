166 Baptized Inflation

War I, when all the battling nations “temporarily” abandoned.
convertible currencies, so that they could impose a massive tax
through inflation on their own helpless populations. The “gold ex-
change standard” was established by international agreement at
the Genoa Conference of 1922. This conference recommended a
policy of “economizing the use of gold by maintaining reserves in
the form of foreign balances.” The words “economizing gold”
meant “more fiat money issued than there is gold in reserve ,“ and
the words “maintaining foreign reserves in the form of foreign bal-
ances” meant that governments and central banks could buy
interest-bearing national debt securities from Great Britain — and
later, the United States — instead of holding “sterile” (non-interest-
paying) gold. All nations could then “pyramid” their own money
supplies on the basis of a small central core of gold held by one or
two major nations.

This system led to world-wide inflation. From then on, pieces
of paper were now only pieces of paper, except when governments
or central banks owned the paper; by government decree they
were now “money.” Gold and silver were no longer money, even
though for practical purposes they had not been involved directly
in every transaction. By cutting the legal ties with gold and silver,
governments were then able to print as many pieces of paper as
they deemed necessary to govern in order that they might achieve
their plans for the “Great Society.” In other words, it was the fact
that money was a precious metal which hindered plans for the
ushering in of heaven on earth. By cutting the legal ties between
gold and paper currencies, the central planners also broke the
public’s awareness of what long-term money is, and how crucial
gold convertibility is as a means of restricting the conjiscatory practices
of central governments.

How despicable! Gold must be a terrible commodity. A bar-
barous relic! And what of these people who insisted that they

4, Jacques Rueff, The Age of Gold (Chicago: Regnery Gateway, 1964), pp. 4-5,
47-48,
