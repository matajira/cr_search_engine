140 Baptized Inflation

Any Christian economist who adopts such nonsense becomes the
paid agent of the taskmasters of Egypt. Yes, I mean Dr. Vickers.

Where Does the Money Come From?

What if the economy is in a recession? According to Keynes
and Dr. Vickers, this happens because the economy is in an equi-
librium position in which resources are unused. (As Hayek said
half a century ago, the problem is not so much explaining how re-
sources are unused at any point in time, but rather how it is that
they are properly used at any point in time. ) The Keynesians love
their free market equilibrium concept, but only so long as it is an
equilibrium with unemployment, This is the only kind of free market
equilibrium that “revolutionary orthodoxy” Keynesians are will-
ing to discuss. Such an equilibrium — and Keynes never did
explain how it could exist for very long, given the profit-seeking
activities of entrepreneurs — calls forth the State to get things mov-
ing and fully employed.

Does Dr. Vickers really believe all this? Indeed he does: “Let
us therefore, in order to focus clearly and soley on the point at
issue, suppose that before the increase in investment expenditures
occurred the economy was in a position of macroeconomic equi-
librium in the sense in which that has already been defined, but
that at that equilibrium position a certain amount of unemployed
resources of manpower and equipment existed. This, then, im -
plies that any increases in expenditures that occur can be expected
to stimulate a higher level of production and call forth the higher
required level of output, without exerting any upward pressures
on the general level of prices.”15

Here we are in depressionary equilibrium. On the one hand,
he rejects the idea of an entrepreneur-driven tendency toward full-
employment in a free market: “For this reason the system cannot
be left, and Christian economic consciences cannot lightly agree to
leave it, to gyrate uninhibitedly and randomly of its own accord.” '*

15. Eoonomies and Man, p. 202,
16. Ibid, p. 234,
