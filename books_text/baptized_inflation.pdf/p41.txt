1
KEYNES, VICKERS, AND VAN TIL

No one has done more, on either side of the Atlantic, to preserve the
kernel of the logic of Keynes’s position than his Cambridge [England]
disciple, Joan Robinson, and we can acknowledge a valuable element
of truth in her observation that Keynes, having shaken us from the
blinkers of the classical postulates, brought something of morality back
into economics... . And while his own confessed philosophic prede-
lections explain the absence from his influence of any distinctively
Christian concern for the deeper explanation of things which only a
Christian perspective can provide, nevertheless Keynes did direct our
attention effectively to the inevitable instabilities of the aggregative eco-
nomic system, and, on the level of pragmatic affairs, and down in the
earthy arena of economic policy formulation, to the need for compen-
satory economic action. Here, then, was the point of departure for a
Christian economics, !

The contribution of Christian philosopher Cornelius Van Til
to twentieth-century Christian thought has centered on the fact

that there is no such thing as neutrality in the debate between
those who are for and those who are against the Christian faith.

All thought inescapably rests on certain presuppositions, meaning
assumptions that are taken for granted before organized thought
can even begin. Even the skeptic’s claim that ‘we can’t be sure
about anything” is a contradiction, in that it rests on the premise

that we can be sure about this much, at least.

1. Vickers, Economics and Man, pp. 250-51.
B
