114 Baptized Inflation

upheavals could have been solved by a return to a more realistic
ratio between gold and the pound. England had merely made the
error of restoring the gold-pound ratio to its pre-war parity, when
a new ratio, reflecting the increase in the number of pounds dur-
ing the war (i. e., monetary inflation) would have been the more
appropriate course of action.

Dr. Vickers prefers to follow Keynes and therefore conven-
iently ignores the cause of England’s money difficulties in the
1920's: the government’s legislated insistence that the public ac-
cept at face value an over-valued pound. As Galbraith observes,
“The error ... was in restoring the pound to its pre-war gold
content of 123.27 grains of fine gold, its old exchange ratio of
$4.87. In 1920 the pound had fallen to as low as $3.40 in gold-
based dollars.” * The pound had been grossly over-valued. Sure
enough, Gresham’s Law asserted itself the artificially overvalued.
currency (the pound sterling) drove out of circulation the artifi-
cially undervalued currencies (gold and foreign exchange). This is
why the head of the Bank of England, Montague Norman, came
to New York City and convinced the head of the Federal Reserve
Bank to inflate the dollar. Norman wanted U.S. interest rates to
be kept artificially low so that people would not sell pounds and.
buy dollars in order to invest in the U.S. at higher interest rates.
The Federal Reserve Bank accommodated Norman, and the
result was the inflation-fueled boom of the U.S. stock market and.
the subsequent collapse. ‘> Who was responsible? The respective
governments and their agents, the central banks (or is it the other
way around?).

What Is the Issue?

The issue here is the issuing of money. Which agency is legally
sovereign, the State or the market? If the market is sovereign,
within the limits of the State’s responsibility of monitoring and en-

14. J. K. Galbraith, Money: Whence It Came, Where It Went (Harmondsworth,
Middlesex: Penguin Books, Ltd., 1976), p. 178, n.3.

15. Murray N. Rothbard, America’s Great Depression (Princeton, New Jersey:
Van Nostrand, 1968), pp. 181-52.
