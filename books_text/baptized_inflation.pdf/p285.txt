266 Baptized Inflation

tian community. (Of course, it really isn’t a disease; it is a moral
failure.) And Dr. Vickers is a very good example of what can hap-
pen when we raise our children in the schools of Egypt.

If you want a good reason for Christian schools, read Econom-
ics and Man.

The Legacy of Keynes

We are now in a position to answer the questions raised in the
introduction to this study. Did Keynes give us “a significant logi-
cal and methodological reconstruction” of economic theory? Has
he achieved a “more complete understanding of the structure and
functioning of the economic system”? We have found no evidence
to substantiate these arguments. On the contrary, we have found.
substantial evidence to indicate what will happen to the present
financial system, thanks to the justification Keynes gave to eco-
nomic theories which have brought upon the West its greatest ever
financial crisis: a mountain of Third World debt which the West’s
banks are unlikely ever to see repaid — at least not in dollars that
are worth anything; a $200 billion annual deficit by the govern-
ment of the United States; endless inflation; endless business
cycles; and all of this accompanied by cries from graying Keynes-
ian economists that they ought to be given more power to make
other men’s economic decisions for them, that nest time ail their ideas
will work, Really. Trust them. Their promises are a lot better than
gold, which Keynes called a barbarous relic. Their promises are
as good as an IMF bond. Their word is their bond, and their word.
is “bond. ” More debt, more debt; let us spend ourselves rich.

Nor can we agree with Dr. Vickers that Keynes “gave us a new
way of seeing things.” Keynes’ new way is in fact an old way. It
dates back to the Garden of Eden and man’s original rebellion
against his Creator. Keynes’ economics is the economics of a rebel
who made no effort to submit himself to the God who made him.
It is an example of man being “as God” (Gen. 3:5) in the realm of
economics, with catastrophic results.

What about the claims of Dr. Vickers? Has he “redirected” the
thinking of Biblical economics? Again we can find no evidence
