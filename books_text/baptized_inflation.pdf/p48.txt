22 Baptized Inflation

Flashes of insight and intuition intersperse tedious algebra, An awkward
definition suddenly gives way to an unforgettable cadenza. . . . I think I
am giving away no secrets when I solemnly aver — upon the basis of
vivid personal recollection — that no one else in Cambridge, Massachus-
ets, really knew what it was all about for some twelve to eighteen
months after its publication, Indeed, until the appearance of the mathe-
matical models of Meade, Lange, Hicks and Harrod, there is reason to
believe that Keynes himself did not truly understand his own analysis.

This comment concerning Keynes’ ignorance of the implica-
tions of his own system comes from one of the founders of the
“Keynesian synthesis ,“ and a representative of a school of thought
which claimed Keynes’ name. What he is really saying is that
Keynes never adopted the rigorous methodological, textbook-type
approach to economics that his disciples adopted. There is no evi-
dence that Keynes ever acknowledged members of this school of
economics as his legitimate intellectual heirs. F. A. Hayek recalls
his final meeting with Keynes in 1946: “Later a turn in the conver-
sation made me ask him whether he was not concerned about.
what some of his disciples were making of his theories. After a not
very complimentary remark about the persons concerned he pro-
ceeded to reassure me: those ideas had been badly needed at the
time he had launched them. But I need not be alarmed: if they
should ever become dangerous I could rely upon him that he
would again quickly swing round public opinion — indicating by a
quick movement of his hand how rapidly that would be done. But
three months later he was dead.” *

On top of this, at the end of his life, Keynes could suggest that

14, Quoted in Daniel Bell's essay, “Models and Reality in Economic Dis-
course,” The Public Inierest (Special Edition, 1980), pp. 62-68, This special edition
was devoted to a reassessment of Keynesian economics, The general consensus
appears that the Keynesian ideas have not achieved their stated goals of lower
unemployment, For pragmatic reasons, there has been a shift away from the
older Keynesian ideas.

15, F. A. Hayek, “A Review of The Life of John Maynard Keynes, by Roy F. Har-
rod"; published in The Journal of Modern History (June 1962); reprinted in Hayek,
Studies in Philosophy, Politics and Economics (Chicago: University of Chicago Press,
1967), p. 848,
