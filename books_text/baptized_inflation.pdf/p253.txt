234 Baptized Inflation

generated and increased, In short, “If it ain’t produced, you can’t con-
sume it .“

The mistake Keynesians make here is to confuse what wealth
really is. A society is not rich because it has more money than it
did earlier. Adding money to the income stream redistributes
wealth, but it cannot be said (scientifically y,” anyway) that an in-
crease of money increases social wealth. (Dr. North argued this in
his book, '’ and Dr. Vickers fails to respond.) Money is, in the
final analysis, just another commodity which serves a particular
purpose in society, namely, to facilitate the division of labor.

Wealth should be defined as a// morally legitimate goods and
services (especially including knowledge) that members of a society
can put to use to achieve their ends. As these assets are increased,
so the nation may say that its real wealth is increasing. But pro-
duction requires capital, and capital is made available only when
people forgo present consumption in order to make investment possi-
ble. Long-term projects, such as building of railways, and re-
source exploration and discovery, are possible because people will
put aside some of their present production in the form of invest-
ment to make such projects a reality. When they reach fruition,
then people may or may not decide to consume their capital. But
one thing is certain: without saving, capital growth is not possible. As
Rothbard has long maintained, the great ideas that exist in any
society at any point in time cannot be implemented except by
means of capital. “Technology does, of course, set a limit on pro-
duction; no production process could be used at all without techno-
logical knowledge of how to put it into operation. But while knowl-
edge is a limit, capital is a narrower limit.... [Technology, while
important, must always work through an investment of capital.” ®

Sixth, there is no reason given to substantiate that the reciprocal
is the actual multiplier. Look again at Dr. Vickers’ words. After
stating the reciprocal is five he concludes: “We can say, therefore,
that in this case the ‘multiplier’ is equal to five” (emphasis added).

17, An Introduction to Christian Economics, p. 45.
18, Murray N. Rothbard, Man, Economy, and Siate (New York: New York Uni-
versity Press, [1962] 1975), p. 490.
