80 Baptized Inflation

many near starvation, 1945-48.49 If all prices and wages are regu-
lated by the central authorities, ¢fective control of the total econ-
omy (except the “black” or underground market, which is simply
the free market still in operation) has passed into their hands. Us-
ing price regulation (wages being the price for labor), the State is
able to decide what will be produced, bought, and sold; it can
cause profits and losses, wealth or pauperism, thereby determin-
ing who will be producers and who will be workers; and it may ac-
tually determine where people will live and how long they remain
there.”The wage control Dr. Vickers envisages is explicit. Work-
ers “might even be taxed to the full extent of the amount by which
their wage increases... exceed nationally established norms.”5!
That, dear reader, is socialism with a vengeance! And if it isn’t,
then it is fascism. It is the economic system described by lawyer
Charlotte Twight in her book, America’s Emerging Fascist Economy
(1975). “ It is certainly not “imagined capitalism.”

Even his call for managed exchange rates by “the monetary
authorities [who] have reason to believe that too wide a fluctua-
tion in the exchange rate” has occurred must be seen as a crucial
move towards the totalitarianism of a socialist State .*In the
words of F. A. Hayek, ‘Nothing would at first seem to effect private
life less than a state control of the dealings in foreign exchange, and
most people will regard its introduction with complete indifference.
Yet the experience of most Continental countries has taught
thoughtful people to regard this step as the decisive advance on
the path to totalitarianism and the suppression of individual lib-
erty. It is, in fact, the complete delivery of the individual to the
tyranny of the state, the final suppression of all means of escape
— not merely for the rich but for everybody. Once the individual

49, Nicholas Balabkins, Germany Under Direct Controls: Economic Aspects of Indus-
trial Disarmament, 1945-1948 (New Brunswick, New Jersey: Rutgers University
Press, 1964),

50. See Sudha R, Shenoy (cd.), Wage-Price Control: Myth and Reality (St, Leon-
ards, New South Wales: The Centre for Independent Studies, 1978).

51, Vickers, Economics and Man, p. 840.

52, New Rochelle, New York: Arlington House.

58, Vickers, Economics and Man, p. 238.
