5
MAN’S REBELLION AND SOCIALISM

. inthe complexities of modern times responsibility for an over-
sight of the economic health of the system must reside in the central goo-
ernment authorities acting on behalf of the people... . 1

All economic systems revolve around some pivotal point.
There is some particular theory or doctrine which becomes the
central determining point for all additional theories and ideas. It
is possible to say that all economic theories are determined by
their attitude to property rights. There are only two possibilities:
either God owns it, or someone else does. Either God delegates
control over property to His subordinates, or these subordinates
own it autonomously.

Then there is the secondary question: Is man’s stewardship (or
autonomous ownership) private or public, meaning individual or
statist?? Hither individuals have final say over property, or the
State does. This does not mean that a number of individuals may
not voluntarily cooperate to control property. In the sense we are
talking about, this would still come under the heading of private
ownership. Private ownership, in other words, is opposed to pub-
lic or State ownership. Individual men are the ones who decide to

1. Vickers, Economics and Man, p. 205.

2, This is not to imply that all theories are determined exclusively by their idea
on property rights. Other key doctrines could also be the determining factor for a
generalized theory. A particular view of Scripture, and how to interpret Scrip-
ture, will also determine which direction the development of economy theory
takes,

63
