110 Baptized Inflation

(from whence people flee back to tyranny). This is what both Rev.
Rushdoony and Dr. North have argued for twenty years, and Dr.
Vickers’ book is a large grab-bag of footnotes proving their point.

At this point, we can begin to analyze the arguments Dr.
Vickers uses in denying the existence of economic law. These con-
stitute “point three ,“

Definitions

One difficulty encountered in analyzing Dr. Vickers’ work is
similar to that found in Keynes’ General Theory. Anyone who has
tried to read either book will know that both are singularly lacking
in a logical arrangement of ideas. In addition, because there is a
deficiency in definitions of terminology used, it is a mistake to im-
agine that words are used in their historic sense or in their usual
meaning. Ideas introduced in an earlier part of Dr. Vickers’ book
are dropped for a while and returned to later on as if they had
been explained. But nowhere does Dr. Vickers really define the ter-
minology he is using. This means that it is very difficult to ascer-
tain at times precisely what Dr. Vickers is talking about. In fact, it
is almost impossible to come to grips with some of Dr. Vickers’
views. Somehow we have to make our way through the intricate
maze of ideas and weed out the various strands of his thought to
determine what he means. In order to challenge his theories,
there is the consistent need to discuss preliminary matters that
have a direct bearing on the ideas propounded.

Economic Law

For example, a typical high school knowledge of economics, or
even some ideas gleaned from the financial pages of a local news-
paper or financial journal, would provide the thought that there
are such things as economic laws, especially the laws of supply
and demand. A common understanding of these laws would be
that they are somewhat like scientific laws, in that they are a regu-
lar occurrence in the world, and come what may, the laws of supply
and demand remain unchanged.
