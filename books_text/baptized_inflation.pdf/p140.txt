Economic Law 117

Workers may be willing to do this temporarily in order to save
their jobs, if the employer and his accountant can prove the case
of imminent collapse. This in no way disproves economic law.

Local wage reductions that are not accompanied by price re-
ductions in the goods and services produced by laborers who have
their wages cut will usually result in lower sales and lower profits.
The average reader may not understand this aspect of free market
capitalism, but it is true. It is normally a mistake for businessmen
to lower the wages of his workers unless there has been a drop in
labor's productivity or a drop in the demand for labor’s output. By
reducing labor’s wages, except as a last-ditch effort to save a firm
or industry from bankruptcy, businessmen create a situation in
which the more productive employees will begin to seek better op-
portunities. Businessmen wind up with the less productive work-
ers. In short, you get what you pay for. If a businessman pays his
workers as if they were low-productivity people, he will eventually
wind up with a work force filled with low-productivity people.

Historically, capitalism has produced conditions in which high
wages, low prices, and high profits accompany each other. This is
what innovation is all about. This is why Henry Ford could revo-
lutionize the automobile industry — and also the face of the West
— when he raised workers’ wages to an astounding five dollars a
day, put them on a mass production line, and dropped the price of
the “tin Lizzie” (Model T) to where the average Joe could afford to
buy one. He became a billionaire. In short, high wages, low prices,
and high profits.

Treating Works-s as Idiots

Dr. Vickers argues that a lowering of wages may not increase
the demand for labor. '* Baloney. He is treating labor as if it were
potatoes, and potatoes as if they were Giffen goods. Labor and po-
tatoes are not Giffen goods (or services). (Nothing ever discovered
has ever acted as a Giffen good or service.)

Let us consider a situation in which lower wages will not lead.

18, Ibid., pp. 269-270,
