Sovereignty and Money 165

honest. It demands that they keep their promises on demand or
at maturity. It demands that they keep their demand liabilities safely
within the limits of their quick assets. It demands that they create no
debts without seeing clearly how these debts can be paid. If a country
will do these things, gold will stay with it and will come to it from other
countries which are not meeting the requirements. But when a country
creates debt light-heartedly, when a central bank makes rates of discount
low and buys government securities to feed its money market, and per-
mits an expangion of credit that goes into slow and illiquid assets, then
gold grows nervous. Mobile capital funds of all kinds grow nervous.
There comes a flight of capital out of the country. Foreigners withdraw
their funds from it, and its own citizens send their liquid funds away for
safety. *

The Resentment Against Gold

Governments love to inflate. They hate to have their inflation-
ary policies exposed. Gold movements expose these policies. All
this was going on long before Keynes arrived on the scene. His
General Theory, however, provided apparent intellectual justifica-
tion for these practices. Here, at long last, was a noted economist
defending all that the politicians knew from instinct: that increas-
ing the amount of money, stimulating the economy by govern-
ment expenditure (monetary and fiscal policies, in other words),
would impose great benefits on society as a whole, especially in-
cumbent politicians. There was only one hitch. People still
thought that these pieces of paper could be redeemed for the metal
which backed them. Silly people.

The solution to this problem was to ban the conversion of
paper money into gold. It was the abolition of a true gold coin
standard and the substitution of a so-called “gold-exchange stand-
ard.” The abolition of the gold coin standard took place in World

8, Benjamin McAlister Anderson, Economics and the Public Welfare: Financial and
Economic History of the United States, 1914-1946 (Princeton, New Jersey: Van Nos-
trand, 1949), p, 421, This has been reprinted by Liberty Press in Indianapolis,
Indiana,
