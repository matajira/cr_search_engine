2
NEITHER CAPITALISM NOR SOCIALISM (MAYBE)

. we shall argue that a well-designed and well-articulated
national policy on wages and prices is most decidedly necessary at this
time, To argue to the contrary ig to lean too far in the direction of a
reactionary economic conservatism, '

“All those who want to identify themselves as reactionary eco-
nomic conservatives, please stand up !” I love this sort of argu-
ment. It is so... so utterly Keynesian.

One of the problems we face today is that self proclaimed
Christian scholars have adopted philosophic relativism as their
intellectual foundation. They have been compromised by today’s
climate of intellectual opinion. This great philosophic shift of the
past 300 hundred years - from truth to relativism— has been well
documented. Commencing with René Descartes, who hypothe-
sized a radical dualism between mind and matter, between
human understanding and mathematical precision, *modern man
has backed himself into a corner where he is no longer able to
ascertain what is true and is not true. 3 Immanuel Kant’s dualism
between the phenomenal realm of scientific truth and the nou-
menal realm of human ethics and freedom* was the culmination

1, Vickers, Economics and Man, pp. 179-80.

2, E.A, Burtt, The Metaphysical Foundations of Modern Physical Science (Garden.
City, New York: Doubleday Anchor, [1982] 1954), ch. IV.

8, William Barrett, Irrational Man: A Study in Existential Philosophy (Garden
City, New York: Doubleday Anchor, [1958] 1962).

4, Richard Kroner, Kant’s Weltanschauung (Chicago: University of Chicago
Press, [1914] 1956).

3
