Behold, Our Savior! 255

subject to fit in with our ideas of “just” wages?

Arbitrary Humanist Definitions

Humanist man is the new god, determining for himself good
and evil, right and wrong, just and unjust. To the extent that
Christians adopt the conclusions of humanist man regarding eco-
nomics, they have thereby adopted the presuppositions of humanist
man. Dr. Vickers is a prime example of this self-deception. Con-
sider his moralistic language. His writings abound with descrip-
tive terms such as “justice,” “equity,” “exploitation,” “excessive, ”
“desirable,” “sufficiently meaningful: or “legitimate.” Private enter-
prise investment, for example, is incapable, according to Dr.
Vickers, of setting a “scale of charges adequate to provide a reason-
able or acceptable rate of return on capital.”® Monetary and fiscal
policy is necessary to ensure a “satisfactory rate and direction of
growth” in the economy. '°

The difficulty arises when we ask who is to define what “justice”
means; who has the wisdom to determine what a “reasonable and
acceptable” rate of return on capital is? If one person wants ten
percent return but someone else wants fifteen, is there anything
inherent in either figure which makes one more “equitable” than
the other? If one employer pays $300 per week to labor, while
another pays $350 for the same kind, is there something funda-
mentally unjust in the former case? And who shall be the one to
decide questions such as these?

Biblical Definitions

The essence of Christianity is that ‘tis God through His inspired
Word, the Bible, who defines what is just and unjust. God, being the
sole possessor of omniscience, is alone capable of determining
these things. Man, without such divine revelation, is incapable of
defining the meaning of words such as “exploitation.” Man is only
able to offer an opinion. Thus, when Dr. Vickers is calling for

9, Vickers, Economics and Man, p. 77, emphasis added.
10. Ibid., p. 294, emphasis added.
