The Marginal Propensity to Confuse 229

it. will be eighty percent of $64, or $51.20, What is happening is that we
are witnessing here what has become known as a “multiplier process.”

Perhaps it should not be drawn to attention, but he has said “sup-
pose.” So far, in other words, we have suppositions, not conclusions,
drawn from either inductive or deductive reasoning. (We might
even call Dr. Vickers’ line of reasoning “suppository.”) Dr. Vickers
is not deterred, however, for “the overall increase in income which
will result from the multiplier process can be simply calculated.”!+

The growth rate in our case was the MPC, assumed to be equal to
four-fifths. What we now have, therefore, is the conclusion that what we
shall define as the overall “national income multiplier” is five. Or to put it
in more technical terms which we can now use comfortably, the national
income multiplier is equal to the reciprocal of one minus the marginal
propensity to consume. In the present case this was equal to five. We can
say, therefore, that in this case the “multiplier” is equal to five, and the
final and overall increase in income that results from the decision to in-
crease the annual rate of investment expenditures by $100 will be five
times that amount, or $500. 3

Tf you believe all this, then how can you argue with the cartoon?

THE WALL STREET JOURNAL

 

“Believe me, the whole economy profits, we

rob someone of five grand. Then we buy some

stuff from a fence. He gives his cut to the mob.
They pay ort cops...

18, Vickers, p. 202, emphasis added.
14, Ibid, p. 208,
18, Idem., emphasis added,
