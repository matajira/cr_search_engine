118 Baptized Inflation

to an increase in labor services purchased. A firm is about to go
bankrupt. The quantity of labor demanded is about to drop. There-
fore, by lowering their wage demands, the workers keep their jobs.
The quantity of labor actually demanded increases from what it
would have been had they not agreed to the wage cut. Dr. Vickers may be
able to provide statistical evidence that a drop in wages in some
firm did not result in more workers being hired, but the statistics
conceal the economic truth: had they not consented to reduced.
wages, they would have been fired. It is once again the case of what
Bastiat called over a century ago the fallacy of the thing unseen.

‘We must begin with a reasonable presupposition: laborers are
not idiots. This may not seem like a very remarkable insight, but
Keynesian economic theory treats them as if they were idiots. The
whole Keynesian ‘solution” to the great depression was to lower the
real wages of workers by inflating the money supply, thereby forcing
workers unknowingly to accept reduced income (higher prices, but
fixed wages), instead of asking them, industry by industry, to take
pay cuts in order to save their firms and their own jobs, Keynes
thought that workers would not lower their mong wages, but that
they could be fooled into accepting lower real wages. Keynes even
admitted that this was the nature of his little game: “Having
regard to the large groups of incomes which are comparatively
inflexible in terms of money, it can only be an unjust person who
would prefer a flexible wage policy to a flexible monetary pol-
icy... .”!6 Cheat the workers, fool the workers — and anyone who
isn’t willing to go along with this nation-wide experiment in mon-
etary debasement and deliberate subterfuge is an “unjust person.”
Spare me the moral lectures, Keynes; you are not well known for
your moral vision. Not in Christian circles, anyway.

Conclusion

Dr. Vickers’ conclusion that wage reductions might not result
in an increase in the quantity of labor demanded is correct on/y if
all other factors — including price levels — drop in the same direc-

16, Keynes, The General Theory, D. 268.
