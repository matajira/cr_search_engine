176 Baptized Inflation

Gold is hated by the State officials who do not like any restric-
tions on their expansion of power through spending. They resent
the fact that it is s0 expensive to produce gold, compared to how
cheap it is to issue paper money and create computer entries.
Gold restrains them, and they resent it. Because people can move
gold from one nation to another, pressuring governments to
reduce the creation of money, the politicians hate it. Gold is sim-
ply too democratic for their tastes. It restricts the elitist power of
the central economic planners.

Here we have a great irony. In the name of Biblical justice,
Dr. Vickers would transfer enormous power to an elite of central
planners. So fearful is he of sin, that he would concentrate enor-
mous power into the hands of a technical elite — his ideological
and self-certified academic colleagues — and then allow them to
decide what is best for the economy. This officially neutral, offici-
ally scientific, officially God-ignoring elite of economists — a disci-
pline self-consciously based on atheism — is to decide what is best
for us laymen. Who says this is best? Dr. Vickers. In whose name
does he attempt to speak? In the name of Jesus. And who is not
believed in by the elite into whose hands he would deliver us?
That same Jesus, who his peers attempt to crucify daily on a cross
of differential equations. In short, Economics and Man is a long-
winded plea for a grotesque pagan idea: that sin is restrained by
the concentration of power into the hands of monopolistic elitist
planners who can fine or imprison anyone who fails to cooperate
with their plans.

But he is not a socialist, you understand. Because he says so.
