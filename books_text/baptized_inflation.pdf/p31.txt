Introduction 5

a spell that it ceases altogether to be effective as soon as it is
broken once.”* He wrote these words in the year Keynes died,
1946. He wrote them two decades prematurely, as far as the eco-
nomics profession was concerned, but Drucker is always way
ahead of the pack. His point is now correct: the Keynesian magic is
dead. But the spells and incantations are still being tried. Some-
times economists give the policies new names, such as “supply-
-side economics.” The results are the same: more government
spending and larger budget deficits.

Christian Economics

Amidst all this confusion, the Christian has a particular obli-
gation to develop a distinctively biblical economics that will pro-
vide the answers to the world’s difficulties. Political economy, or
economics as it is now known, had been of little interest to Chris-
tians until Gary North and R. J. Rushdoony brought the subject
to the attention of the Christian public with their important analy -
ses, which they offered from a self-consciously biblical perspec-
tive.5 Since the appearance of their two major early works-in 1973,
Introduction to Christian Economics and Institutes of Biblical Law, other
authors have contributed books from a similar perspective. The

4, Peter Drucker, “Keynes: Economics as a Magical System ,“ Virginia Quarterly
Review (Autumn 1946); reprinted in Drucker, Men, Ideas & Politics (New York:
Harper & Row, 1971), p. 247.

5. Gary North, An Introduction to Christian Economics (Nutley, New Jersey:
Craig Press, 1978); The Dominion Covenant: Genesis (Tyler, Texas: Institute for
Christian Economics, 1982), volume one of an intended multi-volume economic
commentary on the Bible, Volume two is to be in three parts: Moses and Pharaoh:
Domenion Religion vs. Power Religion (1985), TheSinai Strategy: Economics and the Ten
Commandments (1986), and the forthcoming book, Tools of Dominion: The Case Laws
of Exodus, all published by the Institute for Christian Economics. Rou sas John
Rushdoony, The Institutes of Biblical Law, (Nutley, New Jersey: Craig Press,
1978); The Myth of Overpopulation (Fairfax, Virginia: Thobum Press, [1969] 1978);
Politics of Guilt and Pity (Fairfax, Virginia: Thoburn Press, [1970] 1978); The Root
of Inflation (Vallecito, California: Ross House Books, 1982).

6 B, L, Hebden Taylor, Economics Mong and Banking: Christian Principles (Nut-
ley, New Jersey: Craig Press, 1978); Tom Rose, Economica: Prinaples and Policy
From a Christian Perspective (Milford, Michigan: Mott Media, 1977); John Jeffer-
son Davia, Your Wealth in God’s World: Does the Bible Support the Free Market? (Phil-
lipsburg, New Jersey: Presbyterian & Reformed, 1984).
