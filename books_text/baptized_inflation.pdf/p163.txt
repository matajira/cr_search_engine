Fiscal Policy: Disguised Counterfeiting 141

You see, there are no stabilizing forces in an unhampered free
market economy. (We are back to the Marx-Engels line of reason-
ing: anarchy of private production. ) But on the other hand, we
can have an equilibrium of unemployed resources in a free mar-
ket, an equilibrium which is so stable that government economists
know just what to do to make things better again. In other words,
the free market gyrates randomly, which is clearly unacceptable, yet
it also stabilizes for years at levels of high unemployment, which is
equally unacceptable. Somehow, I get the impression that Dr.
Vickers is determined to find the free market u-nacceptable.

We are told that an investment will “stimulate a higher level of
production .“ Fine. Then why doesn’t some profit-seeking entre-
preneur make the required investment? This question is one which
Keynes and Dr. Vickers never want to answer. For some reason, the
entrepreneur won't invest. (Maybe it has something to do with
profit.) So guess who will invest? Right! The government bureau-
crat — you know, “Mr. Innovation.” He will spend a hundred dol-
lars on something or other. It really doesn’t matter what. Pyra-
mids, you know. ‘Holes in the ground.

“The $100 increase in investment expenditure will obviously
cause an immediate increase of $100 in GNP.” Yes, it will. That is
how the statisticians define one component of the Gross National
Product: an increase in government spending, on anything. The
statistician never asks whether the expenditure is productive. All
GNP statistics assume that it is. Pyramids, you know. Holes in the
ground. It’s all GNP to the statisticians. “Abstracting now from all
the other factors we have considered as affecting the relation be-
tween GNP and disposable income, let us suppose, again to focus
on the single most important point at issue, that disposable in-
come increases also by $100. Now this will generate an increase in
consumption expenditures, the magnitude of the induced effect
depending on the size of what we have just defined as the mar-
ginal propensity to consume.”!” Presto: an increase in disposable in-
come. The economy gets rolling again.

17. Ibid., p. 202,
