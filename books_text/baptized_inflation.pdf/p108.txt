84 Baptized Inflation

essary to determine what the Scriptural teaching is concerning
property rights. The Biblical basis for private ownership was
argued in terms of the Ten Commandments. At least two of those
commandments established the right of private property,
although private property is assumed by the other eight. ?

Restrained by Whose Law?

It is indicative of Dr. Vickers’ position that he does not give
any consideration to Biblical law and the commandments of God.
On the one hand, he declares that “as to the sanctity and the con-
tinuity of the moral law there can be no argument atall.”3 Never-
theless, in attempting to disagree with the Christian Reconstruc-
tionist position that there is an abiding validity in the detailed.
laws given to Israel unless Scripture itself modifies or sets them
aside, Dr. Vickers draws the conclusion that as “we live in a fallen
society,"4 and that since “the economics with which we have to
deal is necessarily the economics of a fallen society,”> it would be
“a grave mistake ... to imagine that economic legislation, at the
conceptual or pragmatic level, is or can be legislation for a theo-
cracy, in the sense, for example, in which God’s people in the Old
Testament economy constituted a theocracy.”*

The preposterous yet inescapable implication in this line of
reasoning is that we live in a fallen society but Israel did not;
hence, we must not adopt Israel's legal code. Yet Dr. Vickers con-
tradicts this when he correctly observes that “the scriptural ad-
dress to the world is always an address to the world as sinful.””
Always? This implies that Israel was sinful after all. Dr. Vickers
unfortunately makes no attempt to reconcile the contradiction in
his thinking at this point, or at any other point, for that matter. In

2, Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Ecmomics, 1986).

8, Vickers, Economics and Man, p. 313.’

4. Ibid., p. 47; of. p. 362,

5. Ibid., p. 855; of. p. 289.

6, Ihid., p. 47.

7, Ibid, p. 119,
