The Marginal Propensity to Confuse 225

by now to find there are insurmountable problems with Keynes’
theorizing at this point, too.

The marginal propensity to consume and the multiplier proc-
ess arguments are not so much economic theory as justification for
why there should be injections of new (or taxed, or borrowed)
money into the monetary stream. Keynesians, after all, do not
want to be too vague about their proposals, or else politicians
might not expand the power of the State with “scientific precision”;
therefore, they bring mathematical precision to bear in order to il-
lustrate the benefits of their ideas. We all know that mathematics
is a precise science, so if the Keynesian can use rigorous mathe-
matics to defend his policy prescriptions, he must be on the right
track. The public is impressed. This appears to be one rationale
behind their use of mathematical formulae. Certainly, their use of
mathematics loses most of their potential readers. (But, Grand-
ma, you have such foreboding formulae !” “The better to create an
elite “priesthood of tenured ce-ntral planners, my dear.”)

It has been known to any observant person that as people’s in-
come rises, they have a tendency to save a higher percentage of
that income — other things (such as morality, religious presupposi-
tions, and lust for more toys) remaining equal. Another way of
putting the same thing is to say that as disposable income increases
— that is, income received after taxes — people are inclined to
spend a smaller percentage of it on consumption. (These are
merely two ways of saying the same thing, just as we may describe
a coin from either its obverse or reverse side. Although my
description appears different, in reality I am merely depicting the
same process. ) This phenomenon is called by Dr. Vickers, follow-
ing Keynes, the marginal propensity to consume. Why it should
be given that particular title, when it could equally have been
called “the marginal propensity to save,” is a question Dr. Vickers
does not discuss, which is a real pity. He might otherwise have
shed some light on a rather obscure passage of Keynesian liter-
ature. Either title could be used to describe the same observation;
that people save and spend some of their income, and as incomes
rise, there is a tendency for people to save more and spend less.
