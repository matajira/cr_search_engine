Says Law 159

nothing, that some revenues are better than no revenues. They
believed that businessmen will lower their asking prices when
they face situations in which they have few or no prospects of
unloading their inventory. Thus, the market will eventually clear
itself of produced goods, assuming that sellers are profit-seeking
and reasonably rational — a not unreasonable assumption.

Keynes and the Keynesians have abandoned this faith in the
rational, loss-minimizing, price-cutting activity of sellers. Keynes-
ians have never succeeded in putting any other explanation of
human action in its place, but they always deny Say’s Law. They
have never disproven it; they simply deny it endlessly. This is not
argumentation; it is rhetoric,

What the reader must understand is that this denial of price-
cutting, market-clearing actions on the part of sellers is the very
heart and soul of Keynes’ critique of free market economics. If his
understanding of Say’s Law was wrong, then his understanding of
free market economics was also wrong. His understanding of the
way people buy and sell was wrong. His recommended policies to
“assist” the market are therefore very likely to be wrong, or at the
very least, inconsistent with his critique of the free market. That,
of course, is precisely what I am arguing in this book. Keynes got
everything wrong. He returned to the errors of mercantilism — the
same errors that Adam Smith refuted, and then built modern eco-
nomics. Peter Drucker’s analysis is on target: “Keynes not only
went back to the Mercantilists in being macroeconomic. He stood
all earlier systems on their heads by being demand-centered
rather than supply-centered. In all earlier economics demand is a
function of supply. In Keynesian economics supply is a function
of demand and controlled by it. Above all — the greatest innova-
tion — Keynes redefined economic reality. Instead of goods, ser-
vices, and work — realities of the physical world and things —
Keynes’ economic realities are symbols: money and credit. To the
Mercantilists, too, money gave control — but political rather than
economic control. Keynes was the first to postulate that money
and credit give complete economic control.”2°

28, Peter F, Drucker, “Toward the Next Economics,” The Public Interest
Gpecial Issue, 1980), p. 8.
