xiv Baptized Inflation

the first two years after Economics and Man was published, occasion-
ally — very, very occasionally — some young man would write to
me and ask me if I had answers to Dr. Vickers’ arguments. I
always assumed that I didn’t really need answers to those argu-
ments. Sometimes I would ask the inquirer just which argument
offered by Dr. Vickers had impressed him, and not once did any-
one tell me precisely which idea he found thought-provoking.
This is understandable, because Dr. Vickers’ prose makes it virtu-
ally impossible to follow any of his arguments. Couple this with
the fact that Keynes’ General Theory was almost equally incoherent,
and the reader has a problem. (So did Dr. Vickers’ publisher. )

I saw no reason to try to find a publisher for a book refuting a
man who was academically unknown, at the end of his tenured-
level career, a member of a tiny Calvinist denomination, with no
followers except for a tiny coterie inside that denomination made
up of academic types who were fearful in those days (and even
these days) of appearing conservative. Not that they actually read
Economics andMan; nobody reads it. But they pretend to have read
it, and they make others think that they have understood it, just to
keep up appearances. They have to: in thirteen years, Economics
and Man is the only book-length study published by anyone, any-
where, which tries to refute the dreaded Christian reconstruction-
ists. The liberals and full-time pietistic retreatists just cannot
stand the thought of Christian reconstruction, and they feel duty-
bound to lineup behind any scholar who will take us on. Sadly for
them, the only soul willing to take up the challenge was poor,
jargon-burdened Douglas Vickers, with his tattered Keynesian
banner flying in the breeze. (Td like to tell you the symbol the
Keynesians have inherited from their master, but this is a family-
oriented book.)

I knew that Economics and Man was unlikely ever to go into a
second printing. What I never imagined was that a decade after
its publication, it has yet to sell out the first edition. It is one thing

14, He later received a full professorship at the University of Massachusetts, a
taxpayer-financed institution whose most famous former students are Dr. Bill
Cosby, a comedian, and “Dr. J,” Julius Erving, a famous basketball player.
