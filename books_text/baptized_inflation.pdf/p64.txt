2
THE HARMONY OF INTERESTS

The real kernel of the signifi cance of his [Keynes’] work lies on an
essentially methodological level, in that he gave us a new way of look-
ing at things, 0, to use Schumpeter’s felicitous phrase, a new “vision,”
which could never have been achieved as long as we wore the blinkers of
the classical and neo-classical assumptions of automatic harmonies, |

Capitalism, laissez—aire, or the free market, is an anathema to
Dr. Vickers. “Sin... is abroad in the world” and any “theory of
automatic economic harmonies”? cannot be expected to work.
The fact that no economist of repute in history has ever argued in
favor of automatic economic harmonies? is no doubt beside the
point — deliberate nit-picking on the part of his critics. Dr. Vickers
also contends that the “invisible hand” doctrine of Adam Smith
implies “that the consistent_pursuit of individual economic self-
interest would lead automatically, via the interdependent market
mechanism in the system, to the maximum benefit for society as a
whole .”> The fact that Smith did not teach such a doctrine, and
that you would be hard-pressed to find any textbook on the his-

1. Vickers, Economics and Man, p. 212.

2, Ibid, pp. 289, 355; cf. A Christian Approach to Economics and the Cultural Con-
dition, p. 119,

8, Ibid., p. 288,

4, The one exception noted in the textbooks is the mid-nineteenth-century
French pamphleteer, Frederic Bastiat. Another possible exception was the
nineteenth-century American economist, Henry C. Carey. Nobody has read
Carey, except as a historical curiosity, for a hundred years.

5. Ibid., p. 8, emphasis added; cf. p. 288.

39
