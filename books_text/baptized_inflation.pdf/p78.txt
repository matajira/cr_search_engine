54 Baptized Inflation

institutional source of a society's productivity. We see this same
“analytical myopia” (to borrow a phrase Dr. Vickers uses to
describe his opponents ') when Dr. Vickers deals with Dr. North’s
comments on interest rates being the “equilibrating device” be-
tween savings and investments. ‘4

We need to understand in advance how important the ques-
tion of interest rates is. It is the central phenomenon in guiding
men’s decisions to save or spend. No other problem is more im-
portant to solve for the overall economy — the “macroeconomic
problem” — than this one. Prof. Roger Garrison has pointed this
out. “The market’s ability to solve this more global coordination
problem — conventionally conceived as the problem of coordinat-
ing savings decisions with investment decisions — has always been
the central issue in macroeconomics.” 6

The Discount of Future Asset Value

Before launching into a discussion of interest rates, we need to
ask ourselves a few basic questions? Are men omniscient? Are
men immortal? Are men limited in time and space? Do men live
in a world of scarcity, defined as a world in which they cannot get
everything they want at zero price? Do men have to make deci-
sions about what they want? Do they make trade-offs, decisions to
take more of one scarce resource and less of another? Isn’t it legiti-
mate to describe economic decisions as the voluntary exchange of
one set of historical circumstances for another? Finally, is this eco-
nomic decision-making purposeful?

If we see man as limited, mortal, and constrained by time and
scarcity, we then have to ask ourselves another question: How do
we evaluate the future? Do we value a future asset as highly as we
value the same asset right now? Obviously, we don’t. In the exam-
ple Dr. North is fond of using, if you were to win a brand-new,

18, Vickers, Economics and Man, p. 248; ef, pp. 76, 178, 190.

14, Ibid, p. 27.

15, Roger Garrison, “A Subjectivist Theory of a Capital-using Economy,” in
Gerald P, O'Driscoll, Jr, and Mario Rizzo, The Economics of Time and Ignorance
(London: Basil Blackwell, 1985), p. 170.
