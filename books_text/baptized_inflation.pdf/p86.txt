62 Baptized Inflation

Conclusion

The evidence so far indicates that Dr. Vickers does not really
understand classical economic theory, or if he does, he does not
think his readers will, which therefore allows him a great deal of
“creative latitude” in describing classical economic theory. He has
not offered any substitute explanation for how people will be able
to decide between spending and investing, between now and then,
apart from freely fluctuating interest rates. He has not explained
why savings will not equal investments, and neither did his mentor,
Professor (a term he despised) Keynes. He has not explained how
aggregate statistical concepts such as “national income” somehow
allocate capital— decisions that are made by acting individuals.

Dr. Vickers has adopted at least a mild version of the standard

socialist refrain: “Capitalism is anarchistic, and it therefore needs
a guiding hand.” Dr. Vickers would substitute the literal clenched
Jist of the government bureaucrat for Adam Smith’s analogy of the in-
visible hand of free market orderliness. He believes that top-down
rational planning is necessary to compensate for the supposed.
blindness and greed of the market process.

The problem is that he has not offered a cogent substitute for the
competitive market’s system of self-interested service to neighbors.
If we do not expect to appeal to men’s self-interest in order to gain
our objectives, are we not living in a fantasy world? The question
is: “What system of rewards and constraints makes men’s self-
interest most productive for their neighbors?” That is the “ques-
tion of questions” that was asked by the classical economists. They
did not answer it well enough to satisfy a serious Christian econo-
mist, but certainly John Maynard Keynes, in all his terminology,
equations, and obfuscations in the General Theory, did not answer it
nearly so well as his classical and neo-classical predecessors did.
The “Austrian School” economists answered it better than any of
them. This is why Dr. Vickers never summarizes the arguments
of the “Austrian School.” He sees his job as winning a debate, not
enlightening his readers, and a debater wins no points for bringing
up his opponents’ best arguments — arguments that he is intellec-
tually incapable of answering ... or perhaps even understanding.
