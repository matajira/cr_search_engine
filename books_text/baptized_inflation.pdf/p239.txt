220 Baptized Inflation

sense. He bombards us with such senseless doctrines. So does his
disciple, Dr. Vickers. Sadly, we have to consider several of them,
just to prove to ourselves that they are nonsensical.

This is one reason why Douglas Vickers has failed to convince
Christians of the essential correctness of Keynes’ economic theor-
ies. Christians expect things to be clear. They would agree with
Eva Etzioni-Halevy’s observation that “ideas that cannot be ex-
pressed clearly are themselves muddled and therefore not worth
expressing at all.”? If we are responsible before God and men to
promote a particular policy recommendation, we expect to be
able to understand it before we impose it. Christians recognize
this, so they have paid zero attention to Dr. Vickers (except for a
handful of scholars who teach courses other than economics in
seminaries or Christian colleges). We do not find Dr. Vickers cited
by Christian authors. (We also do not find him cited by secular
authors. ) We do not find anyone arguing along the lines Dr. Vick-
ers laid down in Economics and Man. The main reason for this is
that Economics and Man is even more unintelligible than Keynes’
General Theory.

Keynes was attempting only to refute the classical economists.
Dr. Vickers is attempting to refute classical economics, plus con-
vince his readers that Keynes’ position is in essential agreement
with Biblical morality. In short, he has assigned himself the unen-
viable task of promoting as essentially Christian the incoherent
economic theories of a homosexual pervert. You may sympathize
with Dr. Vickers. I don’t, since I recognize the perversity of his
self-appointed task, but at least I understand his problem.

So dedicated is Dr. Vickers to the incoherence of The General
Theory that he has lost the ability te communicate. His style is
stodgy at best. Usually, however, his language is s0 convoluted
that the reader is left in the dark. This is where he is supposed to be
left. He is supposed to read all this gobbledygook, and then think
to himself, “My, this is all very complex, all very scientific, and all

2, Hya Etzioni-Halevy, Bureaucracy and Democracy: A Political Dilemma (London:
Routledge & Kegan Paul, 1988), p. ix.
