Foreword xiii

trade position and became a promoter of protective tariffs against
foreign imports, he thereby adopted the ancient, erroneous, and.
long-refuted policies of mercantilism. These were the trade pol-
icies founded on the principle of “beggar thy neighbor.” But
Keynes had long since decided to do a lot worse than just beggar
his neighbor.

Vickers’ Deliberate Vagueness

How much of a shock Dr. Vickers suffered when he found out
this unsavory fact about his hero, I cannot say. Being a self-
professed Christian, he could not have been pleased to find that
the founder of the economic school of thought of which he was an
obscure member, the “new economics,” or “Keynesian orthodoxy
school,” had spent his life committing this foul crime against God.
But Dr. Vickers went on bravely, still proclaiming the wisdom of
his intellectual master. He did not bother to warn his Christian
readers about Keynes’ debauched lifestyle in his book-long
defense of Keynesian economics in the name of Jesus, Economics
and Man (1976). He did feel compelled to admit that “It would be a
theological mistake, of course, to imagine that Keynes’s own work
was influenced by a confessedly Christian perspective. Quite the
contrary, as we shall see in our brief comments at a later point on
the philosophical predilections and presuppositions of certain
famous economists.” Philosophy, indeed! So much for the forthright
admissions in Economics and Man,’3

Don’t misunderstand me. I’m not saying that Douglas Vickers
is a limp-wrist economist. A limp-prose economist, unquestion-
ably, but not limp-wrist. He just had the misfortune of not recog-
nizing economic perversion early in his career, so he found him-
self in questionable intellectual and moral company when the
world discovered that Keynes was a specialist in non-economic
perversion, too.

I never wrote a refutation of Vickers’ book. I was asked to. In

18, Douglas Vickers, Economics and Man (Nutley, New Jersey: Craig Press,
1976), p. 89.
