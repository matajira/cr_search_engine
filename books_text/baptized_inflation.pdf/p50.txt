24 Baptized Inflation

In short, the burden of proof rests on Dr. Vickers’ shoulders.
Nearer to the truth are the words of Scripture. “The fool bath
said in his heart, There is no God” (Ps. 53:1). The trouble is, the
fool has a habit of sharing his foolishness with the rest of the
world. “A fool layeth open his folly” (Prov. 13:16). The question is,
has Douglas Vickers made his academic bed alongside of fools?

The Enthusiasm of Safe “Revolutionisis”

How can we explain Vickers’ commitment to Keynesianism?
As I hope this book will show, it is not Keynes’ logic or clarity
which motivated economists to join the “Keynesian revolution,”
since The General Theory is neither logical nor clear. It is also not
the success of his policies in recent years. It has more to do with
the original enthusiasm of a group of aging academics who re-
member fondly their own participation in a now-distant world of
exciting change. The “old ways” were being abandoned in the
1940's; young economists had an opportunity to rebuild their
world, even if it was only an academic world. This sense of revo-
lutionary innovation was basic to Keynes’ thinking, Harrod re-
ports. '® Keynes wrote to George Bernard Shaw in 1935, while he
was writing The General Theory: “To understand my state of mind,
however, you have to know that I believe myself to be writing a
book of economic theory which will largely revolutionize — not, I
suppose, at once, but in the course of time — the way the world
thinks about economic problems.”!* This same sense of participat-
ing in revolution was also an important aspect of the young men
who followed him. C anadian-American- Brit ish economist Harry
Johnson” once described the coming of the Keynesian revolu-
tion — which he really does not believe was a revolution, but

18, Harrod, Life of Keynes, p. 88.

19. The General Theory and After, Part Z, Collected Writings, Vol. XIII (London:
Maamillan, 1973), p. 492,

20, Johnson delivered the presidential address at the 1971 mecting of the Cam-
bridge Apostles: Richard Deacon, The Cambridge Apostles: A history of Cambridge
University’s élite intellectual secret society (London: Robert Royce Ltd., 1985), p. 178.
