15
BEHOLD, OUR SAVIOR!

At the present moment people are unusually expectant of a more
fundamental diagnosis; more particularly ready to receive it; eager to
iny it out, if it should be even plausible, But apart from this contem-
porary mood, the ideas of economists and political philosophers, both
when they are right and when they ore wrong, ore more powerful then
is commonly understood. Indeed the world is ruled by little else. Prac-
tical men, who believe themselves to be quite exempt from any intellec-
tual influences, are usually the slaves of some defunct economist. Mad-
men, in authority, who hear voices in the air, are distilling their frenzy
from some academic scribbler of a few years back... . Not, indeed,
immediately, but after a certain interval; for in the field of economic
and political philosophy there are not many who are influenced by new
theories after they are twenty-five or thirty years of age, so that the ideas
which civil servants and politicians and even agitators apply to current
events are not likely to be the newest. But, soon or Late, it is ideas, not
vested interests, which are dangerous for good or evil.

John Maynard Keynes’

Keynesianism is a fact of life, if not by name, then at least in

practice. Governments everywhere are enamored with the idea

that they can spend our way to prosperity. This world is, in
Clarence Carson’s description, in “the grip of an idea.” “The idea
is this: To achieve human felicity on this earth by concerting all efforts to-

1, The General Theory, pp. 888-84.
251
