Sovereignty and Money 167

wanted gold? Why, they, too, must be the rascals who are hinder-
ing the economic wellbeing of others. What was needed was an
economic defense of why gold and silver were the great evils in so-
ciety. Keynes assisted in providing that defense. Consequently,
we find Dr. Vickers repeating these statements disapproving gold
and silver as money.’

But the question of gold or silver as money is not just an eco-
nomic and philosophic question. At its fundamental root it is a re-
ligious issue, for the question is: Who has legitimate, God-given
authority to choose which commodity shall serve as money? The
State, as God’s delegated representative, or the free market, as the
institutional creation of acting individuals? Is the State’s role crea-
tive (“We alone create true money!”) or negative (“You have created
fraudulent money!”)? Does the State have a monopoly of money-
creation, or does it possess merely a monopoly of law-enforcement
against law-breakers (counterfeiters)? Therein lies the heart of the
matter. The issue is primarily an issue of sovereignty.

This provides the perspective with which we must view Dr.
Vickers’ disapprobation of gold and silver, not only from an eco-
nomic point of view but also a theological perspective, for Dr.
Vickers has gone to great lengths to argue against the idea that
Scripture, as the revealed will of God, gives such explicit instruc-
tion as to which commodity should be used as money. I have
already noted his defective arguments against the law of God as
the source of instruction on how God shall have his creatures live.
Yet it is the law of God that alone provides answers to the perplex-
ing problems of life. Again, we have noted Leviticus 19:35-36 and.
its call for honest weights and measures. The Scriptures thus put
parameters around man. But modern man is in revolt against the
God of Scripture, as Rushdoony has argued:

The revolt, thus, in the name of the freedom of man has been against
the constraint of any law of God certainly, and also the laws of men, The

5, For a detailed analysis of monetary theory, see Ludwig Von Mises, Ti:
Theory of Money and Credit (rvington-on-Hudson, New York: Foundation for Eoo-
nomi¢ Education, [1912] 1971).
