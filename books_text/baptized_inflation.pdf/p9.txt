xii Baptized Inflation

Keynes vs. the Puritan Ethic

What has this got to do with Keynes’ ideas on economics? A
lot, argues Deacon: “Keynes’s hatred of Puritanism is important
in the light of his economic theories. He was to become the man
who has gone down in history as the most outstanding economist
and architect of social progress of the past seventy years, though
some would dispute such an assessment. But it was his hostility to
the puritan ethic which stimulated and lay behind his economic
theories — spend to create work, spend one’s way out of depres-
sion, stimulate growth. It was also his hatred of Puritanism which
caused him in early life to devote rather more time to pursuing
homosexual conquests than to economics. More positively, his
papers to the Society were in the main nothing whatsoever to do
with economics. One such paper, often cited, was on the subject
of Beauty.” !°

Strachey wrote to Keynes: “We can’t be content with telling
the truth — we must tell the whole truth, and the whole truth is the
Devil. It’s madness of us to dream of making dowagers under-
stand that feelings are good, when we say in the same breath that
the best ones are sodomitical,”!1

Keynes’ economic principles matched his moral principles: he
didn’t believe in them. He denied that fixed economic principles
even exist. '? When, in 1930, he switched from his earlier free

10, Idem.

11, Bid, p. 65.

42, Keynes announced in testimony before a 1980 economic commission: “All
the same I am afraid of ‘principle’ Ever since 1918 we, alone amongst the nations
of the world, have been the slaves of ‘sound’ general principles regardless of par-
ticular circumstances, We have behaved as though the intermediate ‘short per-
iods' of the economist between our [one’] position of equilibrium and another
were short, whereas they can be long enough — and have been before now — to
encompass the decline and downfall of nations, Nearly all our difficulties have
been traceable to an unfultering service to the principles of ‘sound finance’ which
all our neighbors have neglected... . Wasn't it Lord Melbourne who said that
‘No statesman ever does anything really foolish except on principle?” Cited by
D.E, Moggridge, The Retur io Gold, 1925: The Formation of Economic Policy and its
Critics (Cambridge: At the University Press, 1969), p. 90. See Hedge’s discussion
in Chapter 7, “Economic Law.”
