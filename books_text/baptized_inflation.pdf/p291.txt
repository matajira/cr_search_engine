272

Fraud, 196

Free market, 6, 34-85, 47, 51, 78, 76,
108, 116, 140,259

Friedman, Milton. 149.192.202.282

Galbraith, John Kenneth, 65, 174,
221, 287
Genoa Conference, 166
Giffen Paradox, 118, 117
Gold Coin Standard, 165
Gold, 89, 98, 162, 170, 245
exchange standard, 166
Great Depression, 180
Greed, 41, 59-60, 116, 248
Gresham's Law, 174
Griffiths, Brian, 6
Gross National Product, 141

Harmony of interests, 89, 46, 89

Harrod, Ray, 19, 268

Hayek, F. A, 16, 22, 26, 52, 75, 80,
179, 192

Hazlitt, Henry, 150, 227, 264

Hegel, G. W. F., 82

Hoarding, 128, 238

Holroyd, Michael, 19

Homosexual, x, 19, 28, 220, 269

Hutt, W. H., 28, 76, 245

Idolatry, 254

Income, 221

Inflation, 28, 9, 79, 188, 148, 177,
179, 189, 198, 195, 197, 246

Inheritance, 77-78

Interest rates, 54.55, 57-58

Investment, 62

Invisible hand, 40, 42, 89

Israel, 84, 94, 99, 104-105

Justice, 255

Kant, Immanuel, 31-82
Katz, Howard S., 265, 268

Baptized Inflation

Keynes, John Maynard, ixx, xxvii, 2,
8, 12, 17,20,80,83,62, 109,222,
226
General Theory, 17, 24, 26, 94, 59,

62, 110, 185; 150, 165, 178, 219,
228, 265

Keynes, John Neville, 6

Keynesian Solution, 212

Keynesianism, 7, 28, 64, 246, 250,
258-259, 269

Knowledge, Theory of, 88

Kuhn, Thomas, 27, 147, 201

Kutiner, Robert, 208

Labor, 118

Laissez-Faire, 6, 85, 89, 41-42, 50-51,
59, 120, 185, 148

Lawless, 104

Leijonbufved, Axcl, 125
Less-Developed-Countries, 245
Luther, Martin, 71, 191, 265

Macroeconomic, 84, 54, 75

Makin, John H., 242

Marginal Propensity to Consume,
2248

Market, 205

Marz, Karl, 9, 52, 74, 76, 78, 264,
267

Mathematics, 226

Mises, Ludwig von, 16, 26, 58, 64,
69, 180, 150, 158, 157, 162, 192,
202, 267

Monetarism, 248

Monetary demand, 128

Money, 64,8588, 128, 184, 187, 158,
179, 187, 221
fiat, 154, 185
supply, 166, 170, 178, 180, 198, 195
theory of, 161, 179

Monopoly, 41

Moore, E. G., xi, 20

Moral values, 245
