204 Baptized Inflation

refusal to discuss certain aspects of market phenomena that might
mitigate against their theories. Henry Hazlitt made the observa-
tion, “In the Keynesian system, the level of wage-rates and their
effect on employment, is The Great Unmentionable.”*Itis un-
mentionable because itis closely connected to the question of
wage rates ina free society. Keynesian economists despise the
topic of the price mechanism, except as something to criticize.

On the surface, this seems like un unwarranted charge. Of
course the price mechanism is mentioned in Keynesian literature.
Why, it is even listed in the index of Dr. Vickers’ Economics and
Man. Again we are back to the problem of definition. We need to
define exactly what we mean by the phrase “price mechanism.”
More tothe point, we need a theoretical explanation of how it functions,
and. we also need cogent evidence from the world of real human
action that the theory faithfully explains the events.

Objective Results of Subjective Valuations

Prices perform an essential function in the economy. They are
the resulé ofthe subjective valuations of people acting in the mar-
ket place. More particularly, prices are what buyers are ultimately
willing and able to pay for aparticular good or service. A price
helps to inform the seller whether he is selling his goods rationally.
What isan economically rational price from the point of view of
the seller? A price which enables him to sell all the goods he offers
for sale ata profit margin which then enables him to stay in busi-
ness and achieve his other goals, including non-economic goals.
Ingufficient net revenues mean financial loss or hardship, whereas
profits indicate that he has correctly forecast what enough buyers
are willing to pay in order for him to remain in business.

Prices are the result of subjective valuation. Human beings have
a habit, though, of changing their minds and opinions on a host of
things, and on a frequent basis. Someone is willing to pay $30 for
a good book today. Tomorrow he is only prepared to spend $15 on
the book, and obtain a new Pierre Cardin tie with the money

6, Hazlitt, The Failure of the “New Economics, * p. 831.
