258 Baptized Inflation

by Dr. Vickers. This essential difference has been vividly described
by Edmund Opitz.

How can a society whose worldview includes such doctrines as Maya
[illusory world], karma [reincarnation] and caste produce the social
structure upon which the market economy is based? Accept the idea of
Maya and you exclude the idea of a rationally structured, cause and
effect universe. The doctrine of karma makes it virtually impossible for
individuals to have the necessary self-responsibility and will to succeed
which are essentials for a going-concern economy, And caste divisions in
a society are incompatible with the idea of inherent rights and equality
before the law. Capitalism is rooted in the cultural heritage of the West,
Christendom, and you can't have the fruits without the roots; you can-
not merely wish an end result — to will the end is to will the means. ‘*

Keynesianism Offers a Plan of Salvation
“The State loves you, and has a wonderful plan for your life.”

In an earlier chapter, I suggested that Keynesianism is social-
istic and authoritarian in nature. There is an inherent reason for
this, and this is why there is an inexorable move towards totalitar-
ianism whenever Keynesian theory is adopted.

The essence of free market economic theory is this: the econ-
omy is controlled by individuals who make subjective valuations.
For example, a person who decides to save some of his assets does
so because he refers to forgo present consumption in order to par-
ticipate in consumption at some point of time in the future. This is
the basis of the capital process. Without people’s willingness to
forgo present consumption there can be no capital projects which
provide goods in the future, for example, mineral exploration.

What causes people to make the kind of decisions such as sav-
ing? Obviously there are a number of factors. There are perhaps
more obvious motives such as saving for something which is

14, Edmund A. Opitz, “The Philosophy of Ludwig von Mises” in The Freeman,
(July 1980), p. 440. Published by the Foundation for Economic Education,
Irvington-on-Hudson, New York 10583, See also P. T. Bauer, Equality, the Third
World, and Economic Delusion (Cambridge, Massachusetts: Harvard University
Press, 1981),
