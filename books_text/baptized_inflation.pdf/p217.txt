196 Baptized Inflation

constructionists. ' These are Dr. Vickers’ declared intellectual
enemies, the people against whom he wrote his book.

Fourth, with Dr. Vickers’ definition in mind, his conclusion
against Gary North that there is no “single cause or a single cure
for in flation,” is an inference that Dr. North’s economic conclu-
sions are incorrect. '9 It is, however, simply a matter of definition.
Conclusions are only as good as their founding premises, and.
from his premise that inflation is a “dynamic disequilibrium,” Dr.
Vickers’ claim that North is incorrect is a logical deduction. But
understand: it is a logical deduction from a definition which can-
not possibly have any economic meaning. It has no specific con-
tent to give it predictive or analytic value. “Dynamic disequilib-
rium” tells us nothing, except that Dr. Vickers has chosen not to
present a logical alternative to Dr. North’s analysis.

On the other hand, if North’s definition of inflation is the start-
ing premise, Dr. Vickers’ conclusion is incorrect. Combine the
idea that inflation is an increase in the money supply with the in-
escapable fact that current monetary units are controlled by State
authorities, and there can only be one cause, and therefore one
cure, of inflation, according Dr. North’s definition.

Why Immoral?

To deliberately lower the value of money is to deliberately take
away someone’s purchasing power. It is theft and fraud in every
sense of the word, although when conducted on the scale which is

18, What is really needed is several decades of monetary deflation. Without
the creation of 100% reserve banking, the world will forever be at the mercy of
the money manipulators. To return fo a world of stable money and flexible pric-
ing, we will have to go through the deflationary wringer. Once the various gov-
ernment banking insurance schemes are officially abandoned, and the thrift, in-
stitutions are left on their own, there is no known way to prevent a toppling of the
banks, if the central banks slowly increase reserve requirements, This deflation-
ary policy is revolutionary, but, so is the coming mass inflation (the only possible
alternative to a return to sound money), which will inevitably be followed by un-
controlled deflation, or else a total destruction of today’s currency, with a new
currency substituted for the dead one. This is the more likely scenario, however.

19, Heid, p. 179.
