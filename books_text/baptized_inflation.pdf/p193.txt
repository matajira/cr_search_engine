172 Baptized Inflation

Third, Dr. Vickers fails to grasp the proposition that paper
currency, being a promissory note, or warehouse receipt, or what Mises
calls a “money-substitute ,“ should by Biblical definition be sérictly
controlled by the quantity of gold in existence (if the receipts are
pledges to pay gold on demand). This failure of Dr. Vickers to un-
derstand the historical relationship between paper currency and.
gold or silver is seen where he speaks of an increase in the “dollar
value of gold.” In the North-Rushdoony theoretical model of gold-
coin convertibility and 100% reserve banking, because a dollar is
equivalent to a certain quantity of gold, there can be no alteration
in the dollar value of gold. There is no way for gold to jump from
$42 an ounce to $84 per ounce. The dollar is, by legal definition, one
forty-second of an ounce of gold (assuming that this is what Con-
gress has determined), and changes in the physical quantity of
gold cannot alter this fixed, definitional relationship. All it can per-
mit is the printing of additional dollar notes in exchange for any
gold which is deposited at the Treasury, on a $42 per ounce basis.
Bring in $42 and buy an ounce of gold; bring in an ounce of gold
and receive (“buy”) $42. Take your pick.

The difference between North and Rushdoony and Dr. Vick-
ers is essentially the fact that North and Rushdoony view gold and
paper currency as a single aliernating commodity — money — with
paper currency being a one-to-one substituie for gold. The con-
sumer has a choice: to store the gold (remove it from the market
place) and circulate the paper, or “cash in” the paper and circulate
the gold. This is not Dr. Vicker’s view. He has in mind two differ-
ent money commodities, enabling him to conclude that the dollar
value of gold is alterable in the same manner that the exchange
ratio (price) of other commodities is changeable.

More to the point, Dr. Vickers is arguing against the present
fake gold standard, where the U.S. government establishes a
wholly mythical value for gold ($42 per ounce) and then conducts
no transactions in terms of this artificial price. It would be as if
Dr. Vickers were to announce that Economics and Man is now
worth $84 a copy, up from $42, at which price no copies were ever
sold either.
