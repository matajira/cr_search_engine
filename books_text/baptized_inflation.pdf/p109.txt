Antinomianism and Autonomy 8

his economic expositions, self contradiction is a way of intellec-
tual life.

Unshackling the State

Although it is true that what he says is contradictory regarding
the validity of God’s law, in reality Dr. Vickers is against the re-
quirements of the moral law of God. We have seen this already in
his willingness to forgo a Biblical definition (or even a free market
economist’s definition) of private property rights. There are two
further illustrations of this.

The jirstis his call for progressive taxation. Dr. Vickers ini-
tially draws attention to the idea that Biblical taxation consisted of
a poll tax, or head tax, which was an amount equally payable by
all males twenty years of age and over.® He also observes that tith-
ing was a proportional tax.° Yet, for the sake of what he calls “Chris-
tian desiderata of equity and justice ,“ Dr. Vickers is prepared to
argue that there ought to be “some kind of progressiveness in the
taxation scales. ... If, of course, we were legislating for an ideal
society, or, again, for a theocratic order of an earlier kind, then a
strictly proportional tax, such as the tithe, would probably be all
that would be required.”!° In other words, Biblical law is irrele-
vant to a fallen society. We must ask ourselves: “Why?”

The second example is to be found in his contentions against
North and Rushdoony over the Biblical requirements for “hard”
money, money by weight and fineness of precious metals, specific-
ally gold and silver. It would appear an appropriate method in
disputing with someone, especially when that person substanti-
ates his argument from Biblical texts, first, to set forth accurately
his arguments and the verses used as support, second, show how

8, Jbid,, p. 814, This tax was collected only during a census, and a census was
omducted only immediately prior to war. It was really a payment to God, not the
State, for each fighting-age man was to give half a shekel as “a ransom for his soul
unto the Lord” (Exodus 30:12-13). The money was to prevent a plague from God.
See James B, Jordan, “The Mosaic Head Tax and State Financing: Biblical Eco-
nomics Today, IV (June/July 1981),

9, Idem.

10. bid, p. 819,
