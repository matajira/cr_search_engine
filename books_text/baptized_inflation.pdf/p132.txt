Economic Law 109

necessary to consider, at least in outline, what might be understood as
the legitimate economic functions of the state. ®

No Appeal to Principle

Let us review. Dr. Vickers has argued that because this world.
is sin-filled, the specific provisions of God’s law, as revealed at
least in the Old Testament, no longer apply. This means that we
cannot legitimately appeal to the fixed law of God in order to
reduce the power of the State, e.g., taxation above the 10% level
or its imposition of fiat paper (or electronic) money. The State’s
law is sovereign over God’s law, or “God’s law (emeritus).”

Dr. Vickers similarly argues that because there is no “automatic
equilibrium” in a real-world economy, it is therefore illegitimate to
appeal to fixed free market principles in criticism of the State.

In short, it is Hegitimate to appeal to fixed principles in criticism of the
Siaie. This, by the way, was also the opinion of Keynes. He hated.
principles, especially in sexual affairs — and I do mean affairs — but
also in economics. Here is what he wrote in 1980 concerning eco-

nomic principles:

All the same I am afraid of ‘principle’ Ever since 1918 we, alone
amongst the nations of the world, have been the slaves of ‘sound’ general
principles regardless of particular circumstances. We have behaved as
though the intermediate ‘short periods’ of the economist between our
[one?] position of equilibrium and another were short, whereas they can
be long enough - and have been before now — to encompass the decline
and downfall of nations. Nearly all our difficulties have been traceable to
an unfaltering service to the principles of ‘sound finance’ which all our
neighbors have neglected... . Wasn’t it Lord Melbourne who said that
‘No statesman ever does anything really foolish except on principle’ ??

In Dr. Vickers’ scheme, all roads lead to the State. There are only
two roads for societies, once they have abandoned the fixed guide-
lines of Biblical law: into statist tyranny, or into lawless anarchy

8. Vickers, Economics and Man, p. 147.
9, Cited by D, E. Moggridge, The Return to Gold, 1925: The Formation of Eco-
nomic Policy and its Critics (Cambridge: At the University Press, 1969), p. 90.
