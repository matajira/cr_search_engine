Conclusion 265

Keynes’ General Theory was a book designed to confuse. We
find in Dr. Vickers a similar propensity to confuse. His “prelude”
has been orchestrated as a cacophony of unintelligible, illogical,
and meaningless arguments. In reality, his “prelude” is more like a
theme with variations — the theme being nonsense and the varia-
tions reflecting the perverted subject. I, for one, having heard the
“prelude” and paid Dr. Vickers the courtesy of attempting to un-
derstand his work, do not think it is worth the effort to remain for
the main performance and the curtain call. The words of Martin
Luther, although given in another context, are eminently ap-
plicable here. “And, since you cannot overthrow it by any argu-
ment, you try meantime to tire the reader with a flow of empty
verbiage .“8 Or, as Howard Katz has observed, The Keynesian
economists are not true experts. They cannot do anything.
Although they stake their prestige on their ability to predict (as
part of their fraud of imitating the scientist), their predictions are
a standing joke and a continual embarrassment. Neither can they
explain their theories in terms that make sense. If you have had
the experience of listening to one of today’s economists and have
come away thinking, I cannot make head or tail of what that
fellow is saying, then do not be alarmed. That is the response of a
properly functioning mind.”?

Why is it that Dr. Vickers can embrace such intellectual non-
sense and do such violence to the Scriptures which he says he be-
lieves? The answer to that is illustrated in the joke which asks:
Why are Polish cows deformed? Answer: Because they're raised
in Poland and milked in Russia. Why is Dr. Vickers’ thinking de-
formed? Because although he was raised in the Christian faith, he
received his education in the schools and universities of apostate
humanism. This is a disease common in too much of the Chris-

8, J, I Packer and ©, R, Johnston, trans,, Martin Luther, The Bondage of the
Will (Cambridge, England: James Clarke, 1957), p. 87, Or use the translation in
the Library of Christian ClassiesZuther and Erasmus: Free Will and Salvation (Phila-
delphia: Westminster Press, 1969).

9, Howard 8. Katz, The Warmongers (New York: Books In Focus, 1979), p.
259, emphasis in original,
