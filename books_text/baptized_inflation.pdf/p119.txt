Antinomianism and Autonomy 95

Let’s get this straight, The Hebrews were given the law of God.
when they fled from Egypt. They were told to enforce these laws
asa condition of their remaining safe in the Promised Land. But
we are not bound by these laws because we live in asinful world.
Now it does not take a Ph.D. in logic to come up with the next
question: “In what kind of world did the Hebrews live?” This is so
obvious that it makes me wonder whether there may be a relation-
ship between the hatred of God’s law and the ability to think
straight. This, by the way, is precisely what Van Til has argued
for years: that the hatred of God does inhibit the covenant-breaker
from thinking straight.

Dr. Vickers is not content with arguing against the idea of a
God-governed civil government which enforces God’s law instead.
of Satan’s law (for there are only two choices, after all). He goes so
far as to call any attempted application of the laws of Israel to
modern society agrave mistake. “A social philosophy which insists
on the contemporary normative status of the economic institu-
tional arrangements of theocratic Israel is gravely in danger of
overlooking two things: first, the fact that the prior economic
question in modern times is more sharply related to the problems
of economic stability, progress, and welfare in an economic struc-
ture of things which is naturally propense to disequilibrium; and
second, that we have consistently to do not with the economics of
atheocratic nation-state, but with the economics of an obviously
fallen society.”3°

Autonomy: Self-Proclaimed and Legislated

Weare still left with the question as to why there is such disap-
proval of the idea of a theocracy. There is nothing magic about the
word “theocracy,” after all. It means, literally, rule by God. The
Greek word theos, God, is combined with sratos, meaning rule or
sovereignty. A theocracy, therefore, is nothing more than the civil
government's recognition that God rules, not man, It does not
mean écclestocracy, the rule of the institutional church. It means

82, Vickers, Economics and Man, p. 362,
