100 Baptized Inflatien

Having denied God’s “royal law” (James 2:8) in favor of hu-
manistic law, modern Christianity has abandoned its basis of ap-
peal to the lost. It can no longer show a fallen world how far short
of God’s perfect standards it has fallen, and therefore can no
longer explain the need of a Savior (cf. Gal. 3:24). If man is not so
bad, he might be able to pull himself into heaven by his own boot-
straps after all.

To deny the idea of theocracy is thus to deny the law of God,
and to deny the law of God is to undermine every essential tenet
of the Christian faith. In short, we must say: No law, no God. No
law, no sin. No sin, no Savior. No Saviour, no justification. No

justification, no sanctification and adoption as sons and heirs of
the Living God. No law, no justice and righteousness, for it is the
law of God which defines what is just and righteous. No law, no
eschatological fulfillment, for there is no need to make all things
new. The centrality of the Biblical message is thus theocracy, the
rule of God, as manifested on earth as it is in heaven: “Thy will be
done.” Or, in the words of Dr. Bahnsen, “Theonomy is pitted
against autonomy; no man can take stand in between, for no man
can serve two masters (Matt. 6:24). ... We do not attempt to be
as God, determining good and evil; rather, we gladly take our
place beneath the sovereign Lordship of the Triune God. His
word, not our autonomous reasoning, is our law. Theonomy is
the exclusive normative principle, the only standard, of Christian
ethics. It is all or nothing, ethic or non-ethic, obedience or sin.”?7

Conclusion

It is instructive to note that those who deny the validity of the
idea of theocracy are usually silent on the specifics of living the
Christian life. A growing number of Christians have been vocal in
denouncing abortion, and rightly so. But to be a little more speci-
fic, if the Bible condemns abortion because it is murder, does it
also tell the civil magistrate how he should treat such murderers?
Or are the civil authorities free to determine for themselves how

37, Ibid., p. 306, emphasis in original,
