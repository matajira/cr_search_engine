The Great Unmentionable: Unemployment 214

standing economic reality. Some of them, in fact, are quite smart
when it comes to making business calculations. (In other words,
they are not all Keynesians. ) This is why the alert ones make
higher-than-average profits. The entrepreneur who first lowers
his prices in the case of a general wage decline will be at the head
of the line to reap the possible benefits, assuming there is no
change in consumers’ willingness to buy his goods. And even if
they won't buy his goods at today’s prices, they may buy them at
tomorrow%,

Raising Wages the Keynesian Way

So far, we have seen Dr. Vickers make the startling and inac-
curate claim that his hypothetical general wage decline must re-
sult in greater unemployment. What is the result, though, if we
change the situation slightly and suggest another scene? Suppose
that instead of a general wage decline, a general wage rise occurs.
Let us assume that this increase is not the result of tremendous
new demand for labor’s output. Let us return to our price-setting
bureaucrat. Perhaps our benevolent State benefactor decides that
if lowering wages causes unemployment, raising all wages and.
salaries will give increased spending power to the workers, so they
will purchase more, and this will, in turn, stimulate production,
resulting in a demand for more labor. Here, it is argued, is the
possible solution to the unemployment problem. In its essence,
this is the conclusion that Dr. Vickers’ reasoning warrants. It
should be obvious that if a lowering of wages results in unemploy-
ment (which is what Dr. Vickers argues), the solution must be to
raise wages: more money in the income-generating stream, more
demand, more productivity. Utopia, here we come.

If that were our conclusion, we would have fallen into the trap
which Dr. Vickers has set for himself. (No, come to think of it, he
has been caught for his whole career in that analytical trap. So
have all his Keynesian peers. He just wants the rest of us to fall in
with him. ) Again, there is another possible alternative result to
the one deduced above. A general wage and salary increase would
result in greater expenses for employers. They would be forced to
