Fiscal Policy: Disguised Counterfeiting 127

ways Of looking at the world are being sought to repair the logical inade-
quacies and the empirical irrelevancies of economic science. Assump-
tions that the economic world was continually in some kind of describ-
able equilibrium; that automatic harmonies existed and propelled the
economy continually to positions of maximum benefit and welfare; that
simplistic analyses that abstracted from the dynamics of real historic
time could adequately explain observable states of economic affairs .. .
these comfortable simplicities, these damaging illusions we might say,
have been fairly completely shattered, and new paradigms of economic
argument have emerged. The assumptions of equilibrium, of the pres-
ence in the economic system of so-called “risks” that could be assumed
away by the application of a calculus of probability based on postulates
of randomness and chance, of the safety in analysis of ignoring genuine
time, have had to give way to newer perspectives. An earlier crust of or-
thodoxy has crumbled.*

What is astonishing about these words is that it is virtually at
each of these points that the post-Keynesian economics revolution
against the older Keynesian orthodoxy has been aimed: at Keynes’ static
model, at his ignoring of real time, at his model’s de-emphasis of
entrepreneurship. Even more astonishing, it is precisely on these
points that the Austrian School economists have always concen-
trated their attack against Keynes, along with the static neoclas-
sical equilibrium economics tradition of which Keynes was clearly a
part. "Yet Dr. Vickers imagines that it is he and his retired
Keynesian colleagues who are in the front lines of the offensive at-
tack against stodgy conservatism. In the midst of a 20-year meth-
odological rout of his army, retired Sergeant Vickers imagines
that he is cheering on his old unit in a final charge against a nearly
defeated enemy. If he weren’t so arrogant in his confidence, he
would be a pathetic figure.

The Flow of Money

Dr. Vickers leaves us in no doubt as to where he believes the
heart of the Keynesian system lies. The “kernel” of the Keynesian
system, says Dr. Vickers, is this: “The total level of the production

4, Vickers, A Christian Approach to Economics and the Cultural Condition, pp. 22-23.
-5, Gerald P. O'Driscoll, Jr. and Mario J. Rizzo, The Economics of Time and Un-
certainty (London: Basil Blackwell, 1985).
