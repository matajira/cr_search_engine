Antinomianism and Autonomy 87

How the faithful city has become a harlot, she who was full of justice!
Righteousness once lodged in her, but now murderers. Your silver has
become dross, your drink diluted with water. Your rulers are rebels and
companions of thieves; everyone loves a bribe and chases after rewards.
They do not defend the orphans, nor does the widow's plea come before
them.#

Dr. North argues that Isaiah’s criticism of mixing dross metals
(base, cheap metals) with silver was a criticism of counterfeiting.
Discussing the North-Rushdoony exposition of Isaiah 1:22, Dr.
Vickers regrets what he perceives to be their error in arguing for a
metallic monetary system from this verse. Firsi, Dr. Vickers gives
the impression that hig interpretation of this verse is the same as Dr,
North’s, but this is not the case. Dr. North has cogently argued
that Isaiah 1; 22 lists specific sins of Israel, thereby pointing to the
underlying spiritual state of the people. '° In context, there is no
valid exegetical reason to assert, as does Dr. Vickers, that verse 22
should not be taken literally. There is a valid debating reason,
however: the text points to the immorality of monetary debase-
ment, which means the immorality of unbacked paper or credit
money, which Dr. Vickers promotes. Therefore, ignore the text.

Dr. Vickers, in contrast to Dr. North, sees these verses only as
an analogy of their spiritual condition. These two interpretations
are not the same thing. Dr. North’s interpretation implies an es-
sential unity between the actions of the people and their spiritual
state, whereas Dr. Vickers, by holding that their actions are only
an analogy, makes the spiritual life of the people and their actions
two distinct entities. Dr. Vickers’ appeal to other passages of
Scripture to illustrate this “analogical reasoning” does not neces-
sarily prove his case. Ultimately, it is the context of a particular
verse which must govern its interpretation. Therefore it is possi-
ble to agree with Dr. Vickers that in Jeremiah 6:28,30, for exam-

18, Isaiah 1:21-23, New American Standard Bible,

14, Ibid., p. 244,

45, North, op. cit., p. 4,

16, Ezk,22:18-19; Ps, 119:119: Jer. 6:28,30; Lam, 41-2.
