28 Baptized Inflation

way: “It hurts to see people come in with new tools when you thought.
you knew every thing.”

But what is Dr. Vickers’ role in all this? He was one of those
young men once upon a time. He was swept into the revolution in
far-away Australia, on the fringes of the academic world. But he
was a Christian, and he did not want to abandon his faith. So he
did what evolutionists and Freudians and other members of aca-
demic guilds have done in the past: he attempted to fuse two un-
reconcilable positions. He tried to baptize the Keynesianism of his
youth. He did not decide to reconstruct the errors of youth; he de-
cided to persuade Christian scholars to accept his baptized
Keynesianism in the name of “Christian relevance .” He tried to
herd them on board at precisely the time that the academic
world’s faith in the Keynesian paradigm had begun to fade. The
owl of Minerva once again was flying at dusk.

The “Red Curaies”

Iam reminded of an earlier revolutionary generation, the era
of the French Revolution and immediately following. In those
days, the revolutionaries needed the support of clerics. They even
adopted the language of the Bible to enlist support. As Prof. Bill-
ington says: “Already in his Plebtan Manifesto, Babeuf had begun to
develop a sense of messianic mission, invoking the names of
Moses, Joshua, and Jesus, as well as Rousseau, Robespierre, and
Saint-Just. He had claimed Christ as ‘co-athlete’ and had written
in prison A New History of the Life of Jesus Christ. Most of the con-
spirators shared this belief in Christ as sans-cullotte at heart if not a
prophet of revolution. The strength of the red curates within the
social revolutionary camp intensified the need to keep Christian
ideas from weakening revolutionary dedication.”

Who were these ‘red curates” They were priests who attempted
to fuse Jesus and “the Revolution.” These men “found an almost

25, Susan Lee, “The un-managed economy,” Forbes (Dec. 17, 1984), p. 149.
26, James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith
(New York: Basic Books; London: Temple Smith, 1980), p. 76.
