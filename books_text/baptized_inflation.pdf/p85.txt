God’s Creation and Capitalism 61

and it is in vain for him to expect it from their benevolence only. He will
be more likely to prevail if he can interest their selflove in his favor, and
shew them that it is for their own advantage to do for him what he re-
quires of them. Whoever offers to another a bargain of any kind, pro-
poses to do this. Give me that which I want, and you shall have this
which you want, is the meaning of any such offer; and it is in this man-
ner that we obtain from one another the far greater part of those good
offices which we stand in need of. It is not from the benevolence of the
butcher, the brewer, or the baker, that we expect our dinner, but from
their regard to their own interest. We address ourselves, not to their hu-
manit y but to their selflove, and never talk to them of our own necessi-
ties but of their advantages.”

Notice that Smith said that we do not expect help from our
neighbors “from their benevolence on/y.” Yes, it is true that they
may occasionally lend a helping hand, but it is more often that
they will help a lending hand. The borrower is servant to the
lender, after all (Prov. 22:7).

Those who would criticize the classical economists for their in-
ability to understand the social reality of greed and rapacity have
very little understanding of the classical economists. Those who
would criticize the market order for its inability to deal with greed
and rapacity have only tyrannies to offer in their place. How well
does socialism restrain greed? How well does Keynesian central
planning eliminate greed? How well does the Gulag Archipelago
restrain greed?

Isn’t my best defense against a greedy seller my ability to
locate an even more greedy seller? Never forget: sellers compeie
against sellers. Isn’t the genius of the free market its system of open
entry, meaning the ability of new potential sellers to enter the auc-
tion place and offer a better deal to consumers? Isn’t this a more
likely way to reduce the effects of greed than any bureaucratic
scheme? Who is to guarantee that the bureaucrats will not seek
their own interests, too, but without the restraining pressure of
open entry and new competitors? Not Keynesian economists, cer-
tainly. Not Douglas Vickers, certainly.

27, Adarn Smith, The Wealth of Nations (Modem Library, Cannan edition), p. 14.
