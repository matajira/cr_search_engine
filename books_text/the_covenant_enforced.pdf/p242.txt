204 The Covenant Enforced

But gentleness is what we should incline towards. For exam-
ple, when we see the faithful suffer, we should think, “Behold, how
God deals with His children, instructing us thereby that there is
no rest in this world, and that our happiness is in heaven, and it
is thither that we ought to lift up our hearts.” And again, if this
happens to a green tree, what will become of the dry wood (Luke
23:31)? If God does not spare those who have endeavored to
follow His Word, as we see, what will become of those who scorn
it, as the prophet says (Is. 51:21)? If judgment begins at the house
of God, most miserable will they be who have hardened them-
selves against Him, as St. Peter admonishes us {1 Pet. 4:17). And
so you see how we ought to deal in this situation.

Profiting From Suffering

Moreover, if we have patience and meckness and are teach-
able, it is certain that we shall always feel a taste of the goodness
of God in the midst of the afflictions He sends us. It is true that
often we shall be frightened by them. There will be a kind of
disquict in us that will so vex us that we shall think ourselves
utterly forlorn unless God withdraws His hand very quickly. If
He prolongs our afflictions, then we become wholly stupefied.
Our courage fails us, so that if we are not constrained by the fear
of God and by patience, so that we are wholly quiet under His
hand, we shall be always wandering. But if we hold ourselves
there and keep still, then we shall understand that He chastises
us in His mercy and goodness. As it is said in the prophet
Habakkuk: the faithful, after they have been in heaviness, do
come to understand that God is always upholding them and does
not forget them but always tempers and mitigates their afflictions,
so that they fecl His fatherly goodness toward them, and comfort
themselves with it. They can say with David (Ps. 30:5) that the
anger of the Lord is but for a moment, but His favor is for a
lifetime.

So behold, here we may always have matter with which to
cheer ourselves and to rejoice in the midst of our afflictions, so
that we may perceive indeed that our Lord will always be merciful
