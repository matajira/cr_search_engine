Sermon 151: Secret Sins 53

And why? So that men should have occasion to set their minds
the better, and not do as they are commonly accustomed, which
is to listen to what is said and yet ignore it, as if to say, “All this
is very true,” and yet make no account of it, acting like people
who have been dazed, as if a man had knocked them on the head
with a sledge hammer. So much for the first point.

Incest

And we see how God is much concerned about incest, when
He says, “Cursed be he who lies with his mother-in-law, the wife
of his father, or with the daughter of his father or mother, or with
his own daughter-in-law,” and such other degrees.t And why?
Because we see that men in all ages have overshot themselves in
this manner. God is well skilled in applying remedies according
to men’s specific diseases. Therefore, it is not for nothing that He
insists so strongly on such things. And why? Because He saw that
men needed to be held in check, and that such kinds of wickedness
would have reigned if He had not set Himself stoutly against them
and cast barriers in their way, as if He had said, “Stop there, and
don’t play the loose colt.” If He had not ordered beforehand that
men were not to rush forth into such vices, surely all would have
dissolved into chaos. And if this wickedness existed in those days,
we may be sure that we are not exempted from it nowadays.

And therefore, let us understand that since our Lord uses so
much earnestness in this manner, He is warning us to occupy
ourselves in minding these things, so that we think on them day
and night, early and late, so that we may walk in such chastity
and sobriety that our lives may not break forth into such beastly
sins, but that we may be given to God to serve Him purely, That,
I weigh, is what we have to gather from this place.

And let us not flatter ourselves. For God knows well what is
needful for us, or else He would not here have spoken of things
that are so shameful that we are embarrassed even to name them.
And what about that, then? Was not God aware of our sensibili-

4, Calvin is summarizing Leviticus 18 and 20 here.
