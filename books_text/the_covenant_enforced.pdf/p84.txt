46 The Covenant Enforced

out of the way, or to stumble in the way” (v. 18). This is a cruelty
even against nature. For the more need a man has, the more other
men ought to pity him and aid him. Here is a poor blind man,
Men see him ready to take a fall, and they do nothing to prevent
it. Those who take pleasure in such things must be of a totally
evil and corrupt disposition, so that there is not one drop of
common kindness in them. In brief, they must be lovers of all
cruelty and evil. For even the heathen greatly abhorred such
things, so that in some places such a deed was as grievously
punished as murder, theft, or other similar things. Commonly,
however, men made no law for it, the reason being that it was
held that every man ought to be sufficiently learned in his own
behalf, so that it would be superfluous to have said, “If a man see
a blind man, let him set him on his way.”

Nevertheless, as I have said before, we have to note that God
extends the matter and teaching further. In effect, His meaning
is to say, “Cursed shall he be who allows his neighbor to go astray
for want of good counsel.” For just as a blind man rushes against
things and stumbles, and goes astray if he is not led and guided
in his way, so also when we lack counsel and good advice, we are
surely in the same plight that blind men are in unless we are
rescued. Indeed, although a man may have eyes, yet if he is in a
strange and unknown country, and goes quite out of his way, so
that he is running hither and thither, and men leave him alone,
it is all the same as if they had made a blind man go out of his
way. The heathens also considered this to be so.

It is not for us to seck excuses, and say, “Why so? God is only
speaking concerning blind men.” Indeed, but even those who had
neither law nor gospel were able to see the duty, and show us our
lesson, namely that whoever does not show the way to a traveller
when he sees him out of his way, is a very monster, and a
detestable creature.

And so is he also who is stingy with the light of his candle. I
see a poor man whose candle is out, and he comes to me to light
it again. Doing this for him costs me nothing, and yet I say to
him, “You shall have nothing from me.” Are such people worthy
