90 The Covenant Enforced

being not able to deserve one morsel of bread to put into their
mouths. They are thus shut out of all. And why? Because God
has not promised anything except to them that keep all of His
entire law.

God as Judge and Father

How shall we live, then? We must hear God speak after some
other fashion; that is, we must hear Him speak the free promises
He offers to us in our Lord Jesus Christ. God in the Holy Scrip-
ture uses a double kind of speech, by which I do not mean that
He contradicts Himself, or that He is contrary to Himself, or that
one of His sayings is repugnant to any other, for God always
remains the same and, as I have said before, He changes not. All
the words that proceed from Him agree together very well, with-
out any contradiction. Nevertheless, concerning His law, there
He has spoken as a judge, and not as a father. In His law He
speaks as a judge, saying: “He who lives as he ought will not be
deceived. He will not lose his labor. For when you have done Me
service, I will cause you to prosper.” In such a way does God
speak in the person of a judge. And when we have received this
word, behold, we are confounded, no matter how things stand.
And why? Because we do not have the ability to obtain the grace
that is offered to us, and we cease not to kindle God’s wrath
against us, and therefore we shall all be damned.

What is to be done, then? God comes, speaking as a father,
and says, “Well, I am content to forbear this rigor, which I might
execute upon you by My law, even though that rigor is not overly
great, for I have given Myself so far to you as to promise what is
not your due, I have been willing, as it were, to entice you by
friendly means, but I have won nothing thereby because of your
perversity. What is the remedy, then? I come now to tell you that
if you will believe in My Son, I will release you of all your debts.
If you will receive Him as your Shepherd, that He may guide you
quietly, you will perceive that I do not desire to win you in any
way other than by gentle and friendly dealings. It is as if I should
say, ‘Come to me, and I will forgive you all your sins, and accept
