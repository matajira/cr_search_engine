8 The Covenant Enforced

there be only one single altar.

But now, if they had made an altar of hewn and squared
stones, it would have lasted forever. And what would men have
said about it? “This is the altar on which they sacrificed to God.”
And thereupon they would have thought it proper service to God
to sacrifice on it anew. And those who came long after would
have thought that the sacrifices offered there were worth more.?
This would have overthrown the order God had established among
that people. It would have brought in general confusion.

We see what befell the hill of Samaria, as the woman who
spoke to our Lord Jesus Christ declares: “Did not our fathers
sacrifice on this mountain?” (John 4:20). Because Abraham, Isaac,
and Jacob had dwelt there, the Samaritans thought that their
temple was more excellent and more holy than the temple of
Jerusalem. But it was built against the will of God. It was a
heathenish place. It was more full of filthiness and uncleanness
than any brothel. Of course, the people thought they were doing
well, but we must always consider whether God likes what we are
doing, and if He does not, woe be to us! So then, because men
always seem without reason to follow the examples they hear of,
it was requisite that there should be no altars made of polished
stones, for they would have remained in place and there would
have been sacrifices offered on them.

Now we can see what abominations proceeded from this in
Israel. Jeroboam, intending to maintain his estate, erected an
altar in Bethel (1 Ki. 12:28f.), and wanted God to be worshipped
there and sacrifices to be made to Him there. He said to the
people, “Behold, we sacrifice to God, who brought us out of the
land of Egypt.” He protested that he was not serving idols, but
he actually was serving them. He certainly was, for it was God’s
will to have His Temple built in Jerusalem, where it was; and

2. The sacrifices would be worth more because the altar was of greater antiquity
than the altar in Jerusalem. During the Renaissance, when Calvin lived and worked,
the notion that older was better was the operative rule among most humanists,
resulting in the myth that the ancient Grecks had a high and noble civilization, a
myth stilt common today.
