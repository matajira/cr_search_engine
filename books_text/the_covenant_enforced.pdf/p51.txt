Sermon 149: Altars and Ensigns 13

substance,’ even so ought we to be knit together, if we want to
be counted as the children of God. Therefore, if every man wants
to have his own separate, private supper, is this not a withdrawing
of themselves from the community and brotherhood that Jesus
Christ would have us to keep?

In Popery every man must have his altar and his chapel.
Indeed, they were of the opinion that God was much bound to
them for so doing. There should have been one common table (I
shall not go on to point out that they have turned the communion
table into an altar to sacrifice upon, which thing in itself is a
devilish abomination), but in the mean time, although they retain
the terminology, yet they will not have a common table for all the
Church.® For every man thrusts himself in, saying, “Oh, I will
have a chapel, and there I will have my devotions by myself.”
When men have come to this point, it is a horrible wasting of the
Church of God, and the building of so many altars has been the
cause of the creation of so many sects and divisions. Even if the
Papists had placed no idols in their churches, and even if they did
not have such a number of superstitions and idolatries as we see
they have, yet in doing this one thing they have broken the unity
Christ has solemnly set among His members, and in the whole
Church.

‘What must we do then? Let us endeavor to keep ourselves in
brotherly agreement and under the signs and tokens God has
given us, and let us continue in them and make use of all those
means He has given us to serve to that end. That is the teaching
we have to gather from this place.

The Duty of Praise
But let us now return to what we said in the beginning, which

7, Compare Leviticus 239-21. The offering of first sheaf is followed by the
offering of the first loaf, on Pentecost. The Spirit’s coming on Pentecost bound the
individual grains of the disciples into the united body of Christ, which is the loaf.

8 “Church” here means a local church. Obviously, Calvin realizes that a com-
mon table for the world-wide church is physically impossible. In any given locale,
however, there is a church, and that church should be united.
