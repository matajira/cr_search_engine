Sermon 149; Altars and Ensigns 15

(v. 7). Earlier on in the books of Moses, we are told that the peace
offerings served for thanksgiving, so that if God delivered His
people, if He gave them any victory against their enemies, if He
delivered them from famine or any other calamity, they sacrificed
in witness that this benefit deserved not to be forgotten. We see
then that Moses aims wholly at something we have already de-
clared, namely that the people should make an acknowledgement
of this benefit to God, after they came into the land of Canaan.
And when Moses says that it is a land flowing with milk and
honey, it is (as we have already seen heretofore) for the purpose
of stirring up the people to give glory to God, when they see that
the land is so fertile, and that God has shown Himself so liberal.
towards them.

We know that at this present time it is not so fruitful a land,
‘nor was it so fertile before their coming to it, and this is a
wonderful thing. And yet notwithstanding, the wicked have taken
occasion from this to blaspheme, as that wicked heretic who was
punished here mocked both Moses and the prophets, saying that
when they praised the land of Canaan they were but setting out
a fable.! This man shows himself (as do all despisers of God and
such enraged persons as are possessed by Satan) to scorn God’s
benefits, which men may see with their eyes. Nor did he consider
that God expressly threatened to salt that land (Ps. 107:34),
which is to say, to make it barren, so that at this present day men
see it desolate and waste; and he maintained his false opinion
despite the fact that the matter was explained to him. At any rate,
it is a dreadful thing to behold the condition of that country at the
present time, in comparison with what it is known to have been

 

9. is a bit speculative on Calvin’s part, especially in the light of Genesis
13:10, “The whole plain of the Jordan was well watered, like the garden of the
Lorn.” The area around Sodom was like Eden, but was cursed with salt for its sin,
though it is symbolically restored by the gospel in Ezekiel 47:8-10. The history of
this region can be seen as a type, and the same pattern is played out in larger scale
with Canaan as a whole, and larger still with the history of the world as a whole.

10. Michael Servetus had been executed on 27 October 1553, a couple of years
before this sermon was preached, though Calvin might be referring to someone else.
