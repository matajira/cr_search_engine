4
THE CURSE AND JUSTIFICATION

Sermon 152. Monday, 9 March 1556.
Deuteronomy 27:24-26.

24. Cursed be he who attacks his neighbor secretly. And
all the people shall say, Amen.

25. Cursed be he who takes a gift to strike down the
blood of a guiltless soul. And all the people shall say, Amen.

26. Cursed be he who does not confirm the words of
this law by doing them. And all the people shall say, Amen.

We understand from this text that what we spoke of earlier is
very true, namely that God intended to teach the people of olden
time that it was not enough for them to discharge themselves
before men, or to escape blame or punishment here, but that they
must look up higher and consider that there is a judgment seat
in heaven before which we must one day answer and make our
account. That is what we must keep in mind if we are to discharge
our duty properly. We may well trick men by putting on a fair
countenance, and we may even so order our lives that no man can
find any fault in us concerning our outward deeds; and yet all the
while, if our hearts are full of wicked lusts, if it grieves us to be
held in awe,' if we grate our teeth against God, what obedience
is that?

1. Possibly a reference to public worship.

59
