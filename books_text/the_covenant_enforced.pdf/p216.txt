178 The Covenant Enforced

that the hand of God is against us. That is in effect the matter
that is intended in this text.

Now I have mentioned before that the misery is all the more
grievous to bear when an unknown people come to pillage and
ransack, It is hard to bear when we are robbed by our neighbors,
by those who should be at amity with us. But if there comes a
strange people, there is less mercy. For when men are separated
one from another and have no intermingling at all, there is less
likelihood of any means of safety. And we see how this is often
repeated in the prophets, so that it is said sometimes that the
people who will persecute us will be a barbarous people without
language comprehensible to us (Is. 33:19); so that when we ask
them for mercy and cry alas, it will seem to them that we curse
them and they will increase their cruelty. So then, let us mark
that his speaking thus of a strange people is to make us under-
stand that God will send us such chastisers as will have no pity
or compassion towards us, who will thoroughly root us out.

And now be warned, that although the plague has not yet
come near us, we must not therefore fall asleep. For we always
measure God’s threats by our own conception of them and by the
things that offer themselves to our eyes. When men speak of war
and threaten us with the Turk, we reply, “But how can he come
at us? He is too far off.” Again, “Can such a prince invade us?
Impossible! He will be prevented by such and such a means.”
And, “The plague cannot come that way, because of such and
such a reason. It may be dealt with by all such means. It cannot
be.” When we use such kinds of shifting thoughts to comfort
ourselves, we are despising God, not in word but in deed, for we
continue unreformable and proud and do not fear that the plague
can come at us. Therefore God says that He shall stir up strange
people against us, even people of far countries. When men think
least about it, then they will wonder to see how God will come in
upon them on that side that they never thought of, and bring
them enemies to spoil them.

Let us then mark by this text that God’s scourges lie some-
times hidden and break out suddenly, so that men are taken
