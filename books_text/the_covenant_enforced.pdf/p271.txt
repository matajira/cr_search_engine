Sermon 161: God Our Fortress 233

myself.” And both the husbands and the wives should be so mad
as to say, “I will eat my own child.” Seeing that all these things
were accomplished, and that our Lord has executed such venge-
ance, let us understand that we ought not to read these things at
this day without trembling, for it is even as if God should lay forth
His previously uttered vengeance before us on a silver platter.
Moreover, let us understand that when God had pronounced such
sentence against the Jews, it was not executed at the first day. For
He waited for them with long patience, so that it seemed that no
mischief should light upon them. But when the sore was burst,
then was the rottenness perceived that lay hidden before, and the
disease was the more deadly. Also let us mark that if God does
bear with us, and afterwards He corrects us in various ways, and
yet does not strike so roughly as to come to extremity, we must
not think that we have thereby escaped His hand, but we must
return to Him, and not tarry till He sees our disobedience to be
unreformable, and so finds it needful to proceed to the extremity
of His threatenings. Thus much have we to remember touching
this text.

The Fearfulness of God

Now finally Moses says, “if you do not keep all the words of
this law to do them, and if you do not fear this dreadful and
glorious name, the Lord your God. . . .” It certainly seems at
first glance that what Moses requires of men is beyond measure,
that it is not in their power, for who is he that can fulfill all the
law of God? And again, though a man miss in some certain
points, should God use such rigor? I have told you before that he
directs this speech towards those that are wholly given over to
evil, and to such as are despisers of God, who break His law by
every means they can. To what end, then, does he require such
perfection?

Let us first of all mark that God will not have His law
chopped into pieces and sections, for He is setting forth His
righteousness unto us in it. Men therefore must not chop things
up at their own pleasure, as if to say, “I will, for God’s honor,
