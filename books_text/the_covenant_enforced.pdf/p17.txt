Publisher's Preface xvii

Negative Sanctions, Too

There are not merely positive sanctions in life, but also nega-
tive sanctions. We can expect to receive these if we do not honor

God as the sanctioning Sovereign in history:

It is certain that God will threaten often before He finally
comes to execute judgment. Let us therefore consider His jong
patience in tarrying for us (Ps. 86:15; Rom. 2:4). For if we abuse
the same, it will result in nothing other than a heaping up and
doubling of God’s wrath toward us, so much so that it would have
been better for us if He had rooted us out the first day than to
have bore with us so long. Let scoffers say that respite is worth
gold. There is no respite that we would not redeem with a hundred
deaths, were it but possible, when we have been so stubborn
against our God and so disobedient to His Word that we have
made into a laughing matter His giving us some token of His
anger.

Let us therefore consider that as long as God is sparing us
He is giving us leisure to return to Him, and that if our enemies
have left us alone, it shows His favor to us, that we might act to
prevent His wrath. But if we will neither hear Him when He
speaks nor receive His warnings, then we will need to give ear to
these His threats here set forth, and it becomes necessary for Him
to send us off to another school. It is of the wonderful goodness of
our God that when we have thus provoked Him (as we see we
do), yet He forbears us and does all to recover us to Himself, not
by forcing us with many strokes, but by attracting us after a loving
fashion, being ready to receive us to His mercy, not standing as a
judge to vex and to condemn us.

But what? When we have shown contempt for all this, it must
come to pass in the end (as I have said before) that our Lord will
stir up against us other masters, so that the wicked will rise up
against us and seck to make a slaughter of us by butchering and
murdering us, being in very deed the executors of God’s venge-
ance—of which we were warned long beforehand, though we
chose to laugh at it, continuing in our sins and wickedness, That
is why I said that as long as God speaks to us, and we condemn
ourselves and acknowledge our sins and seek atonement with our
God that we may live in peace in this world, then even if it is
