146 The Covenant Enforced

hem us in on every side, He concludes that they shall cling to us.
And why? Because if we cling to our iniquities, so that they reign
in our bones and in our marrow, and are fixed in our hearts and
minds, then must God’s curses follow us similarly. When a man
will not do away with his evil affections, but is delighted in them
and continues to soak in them, God for His part must then shut
him up in them. And when we so do, then He must send up His
curses to stick fast to us like a leprosy that they may consume us
utterly, so that they do not respond to medical plasters, being so
deeply rooted within us and in a way, incurable. Therefore, let
us be afraid at the hearing of these things.

And so we see now to what purpose Moses adds that the
curses of God shall cling to us, namely to warn us that if the
mischief has taken root, we must strain ourselves the more in
praying to God to show us the favor of the cleansing of His Holy
Spirit, whose property it is to search the bottom of our secret
affections; that it may please Him to reprove us in such a way
that this fear may cause in us a marvelous purgation, expelling
our sins from us that we may no longer be wedded to them.

And therewithal let us realize that when diseases begin to
reign, or any other afflictions, we may not hope for any end
thereof unless we cease to offend our God. It is said in a common
proverb that diseases come on horseback (that is to say, very
rapidly), and that they return on foot (that is to say, quite slowly).
But even though the reason for this is readily apparent, we con-
sider it not. And as I have said before, it is because we do not
look to the hand that smites us. Nor can God win us to Himself
by the first stroke, when He punishes us. Sure, we are full of fine
words: When a man is sick he will protest that his whole desire
is to serve God, and that if he may only recover his health, the
world will see him a new man. But all is quickly forgotten.

Therefore, it is no wonder that instead of lessening our pun-
ishments God at various times increases them and allows us to
languish in our adversities. It is because He sees that our diseases
are so deeply rooted that He must come again more often than
only once or twice to purge us of them. And therefore Jet us learn
