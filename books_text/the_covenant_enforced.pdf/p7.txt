PUBLISHER’S PREFACE
Gary North

Thave several goals in publishing this collection of John Calvin’s
sermons on Deuteronomy. The first is to provide primary source
evidence to answer the question: “Was Calvin a theonomist?”
These sermons reveal clearly that the answer is yes. Second, I am
interested in Calvin’s social theory. This question interests me
both as an historian and a social theorist. Was there something
unique about Calvin’s social theory that separated him both from
the medieval theorists who preceded him and the Lutherans who
were his contemporaries? Third, and less relevant to the broader
social and historical issues, is theology as taught in contemporary
Calvinist seminaries consistently covenantal and Calvinistic, or
has it drifted off into other paths? I say fess relevant because con-
temporary Calvinism is today a minor institutional eddy in the
broad stream of evangelicalism, a movement identifiable by the
shrunken condition of its seminaries and also of the denomina-
tions that still profess and enforce the historic Reformed creeds.

I note the title of this book, The Covenant Enforced. Where is the
covenant enforced — the covenant preached by John Calvin? The
answer is clear: almost nowhere, The systematic and self-conscious
unwillingness of Calvinists to both preach and institutionally en-
force covenant theology, beginning in the 1660’s!' and escalating
ever since,” is at the heart of the spiritual crisis of the West.

1. The Restoration of Charles II in England in 1660 marks the beginning of the
dectine,

2. See my forthcoming book, Rotten Wood: How the Liberals Captured the Presbyterian
Church in the U.S.A,

vii
