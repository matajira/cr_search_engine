54 The Covenant Enforced

ties in this area? Yes, of course; but He knew that the heart of a
man is a dreadful dungeon, and that we must be restrained as it
were by force, or else He would never be able to hem us in. So
then, let this provoke us the more to look closely at ourselves, so
that we do not overshoot ourselves in one way or another, but be
vigilant to dedicate ourselves to the service of God with all purity,
as I said before.

And moreover let us understand that lechery is in itself so
loathsome a thing before God, that even if men make no account
of the punishing of it, this will be of no help to us when we stand
before the heavenly throne. For it is no small thing that God
banishes all whoremongers and lechers out of His kingdom, as it
is said both in 1 Corinthians 6:9 (as we have seen recently),5 and
also in Hebrews 13:4.

That is what we have to bear in mind: namely, that God will
not have men to give rein to their fleshly lusts, by companying
together like brute beasts, but that every man should live chastely
jn marriage, and be so honest in that regard that nature not be
forgotten, as it is when the son-in-law companies with his step-
mother, or the father with his own daughter or with his daughter-
in-law, or when the brother marries his sister. Rather, these de-
grees of consanguinity should be observed. For without such or-
der, what would become of things? How would we differ from
bulls and asses? Thus you see what we have to gather concerning
the first point we made earlier, which is that our Lord here
condemns al} manner of unchaste dealings, and will not have men
to behave themselves lawlessly in such cases, but to dedicate
themselves unto Him and consider that their bodies are the tem-
ples of the Holy Spirit, and members of our Lord Jesus Christ,
and therefore that they must repress their wicked affections. Keep
that in mind as the first point.

Coming, then, to the second point mentioned earlier, God has
here set down such examples before our eyes as ought to make

5, From October 1555 to February 1557 Calvin’s Lord’s Day sermons were on
1 Corinthians.
