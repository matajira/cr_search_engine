Sermon 156: God’s Threats 145

us know that it is He that smites, as if He should say, “I have
gone about to draw you to Myself by fair means, but you have
not humbled yourselves before Me. Seeing then that you will not
be brought to it willingly, I shall now compel you, as if I drew
you by the hair on your heads.” Let us, I say, be admonished by
this teaching, here mentioned, to prevent the wrath and venge-
ance of God, as often as He gives advertisement of the same.

Again, when God makes the earth fruitful, let us acknowledge
it to be His work, and that there does not spring up a blade of
grass except He has put His Hand upon it. Let us then take our
daily food from Him, not cramming ourselves like brute beasts,
which fill their bellies not knowing whether there be a Creator
that causes the earth to bring them forth food; but let us under-
stand that God blesses the earth and causes it to yield fruit for
our nourishment and sustenance. Let us bear it in mind, that in
affliction as well as in prosperity we may turn our eyes ever more
unto God.

The Curses Cling to the Sinner

And it is expressly said further that God will make pestilence
cleave unto us and that He will send other diseases and that they
shall continue upon us until we are consumed by them. This
should waken us even better, seeing that God does not strike as
it were a single blow and then quit, but that His curses shali
follow us and cling to us unless we forsake our sins and wicked-
ness.

Finally, let us learn that those who stay away from God and
hide from Him day after day, only deceive themselves. For exam-
ple, we see many who, when God sends them some affliction, take
some notice of it, but soon they forget it. They shake their ears
and think it is over, and like a dog that has been stuck with a
whip they turn their backs and run away, thinking that they may
appease God by some means. Thus deals the world. But let us
beware of such stupidity, for we see what is said expressly in this

lace.
P After God has spoken of His curses, and added that they shall
