98 The Covenant Enforced

he will act to prevent it. God then, in not punishing the sins
presently committed by His children, does chastise them in the
interest of preserving them. It may be that there is a man who
day after day obeys God, and does not offend Him in some
particular sins. All the same, if he were always in prosperity, he
would forget himself, so God acts to cure such diseases by with-
drawing His blessings. He will not allow the fat to blind men’s
eyes or to hinder our coming to Him, Jest it should hold us too
long in this world. He will not allow us to become so entangled
in our delights that we fall completely to sleep in the same.

There is a second point, which is that when God forbears to
punish us for our sins, it is not because He does not always have
just cause to do so if He wished. Pick out the most perfect folk
you wish, yet if God were to deal with them in rigor, He should
lay many hard knocks in them. But we may not think so, since
we are not as clear-sighted as we should have to be to know the
faults that God sees. But we must assure ourselves that God
shows His patience and goodness in that He spares men, and
does not punish them (and by this I mean the most righteous
that can be found).

We may take as examples of this: David, when cruelly perse-
cuted by Saul and by all his various enemies; and Abraham, who
was stung and vexed in many ways, and also Isaac and Jacob;
and all the prophets, who were afflicted by the wicked and by
those who despised God, even to death; and finally the apostles
and all the faithful, who were cruelly dealt with. This was not for
their sins. No, indeed. If God had been pleased to call them to
account, He might have punished them a hundred times more
severely, and they would have had no cause to accuse Him of
cruelty. He spared them, and in so doing He did them the honor
of suffering for His name; or rather He tried their obedience, killed
their carnal affections, and drew them near to Himself. For exam-
ple, when God was pleased to make Job a mirror of patience, yet
at the same time He made him acquainted with his own frailty.
But whatever the case, He intended to make him serve as an
example for us all. We perceive then that God has other meanings
