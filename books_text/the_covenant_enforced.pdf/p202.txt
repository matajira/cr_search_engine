164 The Covenant Enforced

fashion when we have been taught by His mouth and have re-
jected His will and are so far corrupted that we make but a jest
of His word; and that whereas He sought to retain us as His
people, we have despised Him. It is very proper therefore that
we should be grievously punished; and therefore let us think that
since God does us the favor of letting us have the pure doctrine
of the Holy Scripture, the same serves to remove from us all
excuses and also to quicken us up to walk so much the more in
fear.

But with all this let us mark further that God not only uses
threats towards us, but also daily exhorts and allures us to Him-
self to reconcile us to Him, showing that on His part He is ready
to come to reconciliation if we condemn our sins and return to
His mercy. What else is the gospel that we hear everyday, except
a message of reconciliation, as St. Paul calls it in 2 Corinthians
(2 Cor. 5:18)? Seeing then that God sends us a herald to declare
peace unto us, and to show that He is ready to do away all our
offenses, let us take heed that we use this time of our salvation to
receive the grace that is offered to us in due season, as the prophet
Isaiah tells us and as St. Paul says, using the same testimony (Is.
49:8; 2 Cor. 6:2). Again, when we have been overly hard, and
God has patiently waited for us, and we still continue in our sins,
let us not think that He in the end has either given over or
forgotten His office. We must yield an account of such unthankful-
ness. When we forsake the salvation to which He has called us
and despise Him out of measure, such willful stubbornness must
of necessity come to reckoning.

Then let us stand in fear, and as often as we hear words of
the grace of God that is offered to us in our Lord Jesus Christ, let
our hearts be inclined to receive it; and let us give way to God to
come in. And when we go to Him, let it be done with bewailing
of our sins and with admission of our guilt, not only in word of
mouth but also with such grief of heart as may prove that the evil
displeases us. And when we thus come to dislike ourselves, let us
not abide until our Lord puts into execution the threats He sets
forth here, but let us turn them to our use. And when we hear the
