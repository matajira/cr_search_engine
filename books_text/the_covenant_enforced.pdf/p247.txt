Sermon 160: Signs and Wonders of Wrath 209

always proceed farther, until He has brought us to these signs and
wonders that are here mentioned.

Again, God will do the same thing when men prove slothful
and negligent (or rather, utterly senseless). If we could be sub-
dued at the first blow, God would take no pleasure in laying on
us plague after plague. But when He sees that there is so much
stoutness and presumption in us, that we will not stoop or bow
our necks, He finds it necessary to hold on until He makes us to
feel in very deed and after a strange manner that it is He before
whom we must yield our account. Let us therefore mark well how
the obstinate malice of the world is the reason why God sends
such strange corrections, to put us in fear.

And if we considered well the state of the world at this present
day, it would make the hairs of our heads stand upright. Certainly
all men sigh at the feeling of the stripes, and they complain, but
not to any amendment of life. Rather, they bite upon the bridle,
insomuch that when those who are not wholly stupid enter into a
comparison of the present with what existed before the wrath of
God was provoked, they see a great gulf, and that ought to make
us afraid. And so, let us come back again to what Moses declares,
which is that the world must be very rebellious and hardhearted
for God to augment His punishments in such a way. For such
would never come to pass were it not that men were otherwise
unreformable. Let us not accuse God of cruelty when we see Him
using exceedingly great rigor in punishing us, but let us acknow!l-
edge that our stubbornness is so great that our Lord must handle
us after that fashion, for otherwise He should never overmaster us.

That is what we have to bear in mind. That is not all,
however, for we must always fear what is to come. And since we
saw that God has thus increased His punishments not upon one
man only, but upon the whole world, let us think upon it, and
call ourselves home again lest He fall to striking us with many
blows, to our confusion and utter undoing, without giving us any
more opportunities to come to a knowledge of our sins. Let us
beware lest God’s vengeance proceed so far. And inasmuch as
we see that the trials of these present days are very great, let us
