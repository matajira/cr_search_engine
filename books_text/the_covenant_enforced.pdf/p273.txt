Sermon 161: God Our Fortress 235

does God deal in that way. But if our hearts are utterly hardened,
then will He war upon us, and destroy us without sparing, and
even though He bears with us, yet will all curn to our confusion.
That is one thing that we have to remember concerning this text.

It is also a notable point to be observed, where Moses speaks
of the yoke of iron. For he says (v. 48) that the Lord shall lay a
yoke of iron upon the neck of all unbelievers until they are wholly
consumed. And that is to the end that we should learn to receive
the corrections He sends and not kick against them. For we see
what happened to Hananiah when he mocked at the threats of
Jeremiah (Jer. 28), and tried to break the yoke that he had upon
his neck, which was a yoke of wood and cords. The prophet wore
that yoke to move the people to consider that they should all
become captives, and so to behold their own state in the person
of Jeremiah. Now this wicked one that was possessed by the devil
(to make the Word of God to be ignored) came to break the yoke
from Jeremiah, and said, “Even thus shall God break the yoke of
Nebuchadnezzar; and even if he besiege you, and do what he can
to carry you away captives into a strange land, yet shall you be
preserved by the grace of your God.” God threatened, and this
fellow mocked all His threats, and for that purpose took the
shadow of the name of God, and abused the people. Well then,
what did he win by such rebellion? It was said to the prophet
Jeremiah, “Take a yoke of iron” (Jer. 28:14), for whereas the
bondage should have been gentle and tolerable, it must now be
so cruel, that they will not be able to endure it. See here how He
deals with all such as harden their hearts against Him, and refuse
to abide and be meekened by His hand.

So then, let us be afraid of this yoke of iron. First of all, we
know that God’s yoke is amiable to all such as willingly yield
themselves to it, and allow themselves to be governed by the hand
of our Lord Jesus Christ. We have the testimony of the Son of
God, “My yoke is easy, and My burden is light. Come to Me, and
learn that I am lowly and meek.” Seeing that our Lord Jesus
declares that those who are teachable and obedient will perceive
that there is neither burden nor weight nor grief in His yoke, to
