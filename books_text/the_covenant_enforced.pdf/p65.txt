Sermon 150: Blessings and Curses 27

we do not have the ceremony spoken of in this place, yet the
substance of it must be in force among us, which is that in seeking
to serve God, we must have an eye always unto His promises.
Behold! Our God calls and allures us unto Himself. And how
does He do so? He might have commanded us in one word saying
“You owe all to Me; see therefore that you discharge yourselves
as I command.” But no, He bears with us, and displays a fatherly
goodness toward us in saying, “My children, I will not have your
service unrecompensed. Indeed I owe you nothing, yet neverthe-
less I shall be so bountiful above all that you need, if you serve
Me, that your life shall be happy, and you shall prosper in all
things.”

And besides that, there is a sovereign blessing for us concern-
ing the life everlasting. For all that we can desire or look for in
this world is nothing in comparison with the salvation we hope
for through faith, and all the blessings God promises us and offers
us concerning the life to come. Therefore, all this ought to make
us the readier and better disposed to submit ourselves to God.
Why? Because seeing that our Lord seeks nothing but our welfare
in our obeying Him, and offers us a reward for so doing, are we
not too incredibly wretched if we do not enforce ourselves to serve
God? You can see then how we ought with our good consent to
ratify all the promises contained in the Holy Scripture, that while
others think it is only lost time to do well, we may always have
this imprinted in our hearts: that there is nothing better than to
cleave unto God.

The heathenish sort think themselves very happy in following
their own lusts. When lecherous and covetous persons have scraped
together money from all sides, they think that all is well and they
rejoice in their deeds. If fornicators, who are brutish in their
fleshly lusts, are allowed to enjoy their pleasures, they wallow in
them; they are drunk with them; they are wholly bewitched by
them. If a vainglorious man comes to any dignity and is advanced
to any authority among men, he thinks there is no other joy or
happiness but to be in high estate. At the same place are all
despisers of God. And in the meanwhile, the poor faithful ones
