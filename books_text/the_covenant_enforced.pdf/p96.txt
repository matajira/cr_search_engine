58 The Covenant Enforced

watches over us, and understand the office of His Word to be
such as is written in the epistle to the Hebrews (4:12), that when
it is preached out to us, then all things must come to trial, and it
must enter in, even to the most secret thoughts of our hearts. Now
if God’s Word has such power, let us assure ourselves that much
more it has the office attributed to it. For this reason let us be
restrained by this means, and when we have served God with our
hearts, let our lives be so answerable thereunto that when we shall
come before our Lord Jesus Christ we may show that we are truly
minded to serve Him, and not to please mortal men.

Prayer

Now let us all fall down before the majesty of our good God,
with acknowledgement of our faults, praying Him to vouchsafe
to give us true repentance of them. And moreover, that He would
bear with us until He has rid us quite clean of all our sins, and
of all our spots. And that He would make us so to profit by the
things we hear, that we may learn more and more to renounce
ourselves, and to repress our wicked lusts, until He has clothed
us again with the purity of that righteousness to which He has
called us. And so let us all say, “Almighty God, heavenly Father,
etc.”
