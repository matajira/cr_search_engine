218 The Covenant Enforced

sovereignty and dominion He has over us, since we cannot find
it in our hearts to serve Him with a cheerful heart and with a
good will, and since we refuse to give ourselves over to Him. Are
we at rest? Yes, and so all our endeavors center on how to pluck
out one another’s eyes and to torment each other like cats and
dogs. And if we are not warring with men, then we are warring
against God, something far worse. And if we keep this up, let us
not be grieved when we are confounded altogether, seeing we set
ourselves up against the majesty of Him under whom we ought
to bow.

After all, we see that most commonly men make war against
God when He gives them rest. We shall see both generally and
particularly that those who have leisure to do evil are the ones
who persecute the Church and torment the poor faithful ones. As
soon as God gives them any respite, they seek nothing but occa-
sion to do hurt and to exercise their cruelty. And this is to be seen
not only in the enemies of the Church, but also in all others, both
great and small. When God has given us rest and we have made
an end of warring one against another, we fall to despising God
one way or another. Let us not wonder, therefore, if when a war
is finished, it begins again immediately. For it is necessary that
God should deal with us in such a way; otherwise, He cannot rule
us.

Thus it is said here, in verse 49, that God shall raise up a
barbarous people against such as will not be obedient to Him.
Such is God’s rule over us that He desires to be like a father to
us, rather than to be a dreadful king or prince over us. It is true
that we must do Him homage as our sovereign Lord, and that
we must behave ourselves as His people in all subjection and
humility, submitting ourselves under His yoke. But all the same
He continues to perform the office of a father toward us, and
wishes to be acknowledged as a father. For He speaks to us in a
friendly fashion, so that although His commandments are hard
for us because of our malice and rebellion arising from the flesh,
yet notwithstanding, after He has declared His will to us, He
exhorts us, warns us, rebukes us, and does all these things with
