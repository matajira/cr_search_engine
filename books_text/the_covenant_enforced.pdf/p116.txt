5
PROMISE, OBEDIENCE, BLESSING

Sermon 153. Tuesday, 10 March 1556.
Deuteronomy 28:1-2.

1. Now it will come to pass, if you will diligently hearken
to the voice of the Lorp your God, being careful to do all
His commandments that I command you today, the Lorn
your God shall set you high above all the nations of the
earth.

2. And all these blessings shall come upon you and
overtake you, if you will hearken to the voice of the Lorp
your God.

We have seen earlier how God covenanted with His people,
purposing to bind them so that everyone should know his duty
and be the better disposed to perform it. It does not matter
whether people plead guilty or not when they have done wrong,
for God here gives His determined sentence whether they consent
to it or not. Nor does He start off with a condemnation of those
who offend the law, but rather He begins with friendly promises,
thereby to lure and win the hearts of men to Himself.

Promise and Duty

And so He says, “If you hearken to My voice, to obey My
commandments, and be careful to keep them, you shall be blessed
in ali manner of ways, and you shall be surrounded through My

78
