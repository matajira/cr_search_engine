Editor’s Introduction xxv

apply the same to our own use.”!® He concludes this section of
his remarks: “Therefore, when we look at such a mirror [the
history of the Jews], let us learn to make a good use of it, and let
their example serve to seal this doctrine and to confirm it, so that
we do not test God, and so that we not continue hardhearted so
Jong that He decides to wrap us up in reproach with all the rest
of the nations of the world.”!9

A Note on Calvin’s Pessimism

The reader of these sermons will no doubt be struck by the
fact that, despite the encouraging and optimistic tone of the Bibli-
cal passage under consideration, Calvin manages to turn his re-
marks mostly to a discussion of suffering and affliction. Indeed,
Calvin assures us that earthly prosperity is less in view in the New
Covenant than in the Old, since the main purpose of earthly
prosperity is to cause us to look beyond this world to the next.

Consulting Calvin’s remarks on Mark 10:30, which says that
those who follow Christ will receive a hundred-fold “in this life,”
with persecutions, and in the world to come eternal life, we find
that Calvin states that “in the greater number of cases, those who
have been deprived of their parents, or children, . . . are so far
from recovering their property, that in exile, solitude, and deser-
tion, they have a hard struggle with severe poverty. I reply, if any
man estimate aright the immediate grace of God, by which he
Telieves the sorrows of his people, he will acknowledge that it is
justly preferred to all the riches of the world.””°

Later Reformed thought, seeking to be consistent with other,
More optimistic strains in Calvin’s thought (and in Holy Scrip-
ture), noted that while individuals may not receive the precise
promise of Mark 10:30, the church as a corporate body will
someday inherit the earth. Moreover, the goal of redemption is

18. Sermon 159, p. 191 below.

19. {#id,, p. 192. See other illustrations in the Publisher's Preface by Gary North,
above.

20. John Calvin, Harmony of the Evangelists, vol. 2, p. 407. In Calvin’s Commentaries
(Grand Rapids: Baker Book House, [19th c.] n.d.).
