Sermon 161: God Our Fortress 223

draw themselves from us for less than nothing, so that the world
will see that those who were most forward to help us, now become
most against us.

And, the reverse of this, let us understand that our Lord can
easily transform all malice. Indeed, though they be as fierce as
lions, He shall make them mild and friendly toward us. There
‘was never a nation as proud and cruel as the Egyptians. In
particular, we see how spiteful they were against the children of
Israel. It seemed that this poor people would never find any
mercy. And yet God wrought in such a way that the Egyptians
gave them all their treasures and all that was fine in their houses.
Neither gold nor silver was spared. And how did this come about?
Because they had held the Jews in bondage and had vexed them
exceedingly. They would have plucked the bread out of their
mouths, and would have cut the throats of them all. A little
earlier, they had killed their children as soon as they came out of
their mothers’ wombs, intending to destroy the entire race of
them, And now, how does it come about that they are suddenly
mild, and every man brings them those things that were most
precious in his house? It is because God converted their hearts.

And returning to the other side, it is said that God hardened
the hearts of kings when the people were to enter into the land of
Canaan (Josh. 11:20). They should have been allowed to pass,
but there were many who opposed them. How did that come
about? It was because they [Israel, apparently — ed.] had resisted
their God.

Let us therefore leam (as I have said before) so to behave
ourselves in obedience to God that men also on their parts will
be friendly towards us. For there is no one so great or mighty
that he can avoid the misery that will rise up against him when
he resists and strives against God. And we have seen that those
who are drunk with their own greatness must in the end be
punished, and not by those who are in authority or who are high
in the estimation of men, but by riff-raff and dissolute persons;
they will spit in their faces. And we have seen such happen even
to princes, and to other men of great estate. For when they will
