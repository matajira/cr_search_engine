60 The Covenant Enforced

Assault

‘Therefore let us note that when this text says, “Cursed be he
who smites his neighbor secretly” (v. 24), God is not only con-
demning the faults that come to knowledge before men, but also
condemning the crimes that lie hidden. And so, if a man has lived
in such a way that he cannot be rebuked by the world, but is
rather praised and commended, he must not use that as an occa-
sion to fall asleep, but must continually examine his heart and
consider well whether there be any hidden nook or cranny therein.
For if such a man is thoughtless and does evil, though men do
not perceive it, yet God will always perform the office of judge.
And if our hearts reprove us (as St. John says [1 John 3:20]), God
sees us even more clearly.

So then, this text serves well to humble us. And indeed, we
ought to remember one other sentence of St. John, where it is said
that whoever hates his brother secretly, even if he conceal his
hatred so that it is not perceived and rather makes a show of love,
that man shall not fail to be condemned by God (1 John 3:15).
And so you sec in effect what we have to bear in mind.

Let us not busy our heads about men’s reports. Even if a man
is well regarded here in this world, yet let him not flatter himself
on account of it. Rather let him summon himself before God and
consider whether he be at fault in his heart. Let not men bring
before God their opinions and fancies, for all such things will be
tefused; they will serve no purpose. And therefore let us walk with
unfeigned heart before God. For we know that He does not regard
the outward appearance, as it is said in 1 Samuel 16:7, but He
requires the heart and truth, as it is said in Jeremiah 5:3.

And looking at what is said here concerning murders and
fighting, we must extend the same to all other crimes. For if God
calls to account those who have done any outrage to their neigh-
bors, even though these are not known to the world, do we think
that robberies, treasons, frauds, railings, slanders, and the like
will pass by unnoticed? So then, let us learn that we cannot by
any means avoid the hand of God or escape His vengeance if we
foster vices in our hearts, though these be not visible to men.
