180 The Covenant Enforced

you shall sce” (v. 34}, it is just as what we saw in verse 29 above,
namely that men should be distracted and grope at noon like the
blind in the dark. For if we are held up, so that we gather our
spirits to call upon God and are enabled to be patient in our
afflictions, that is a great grace, and such a one as cannot be
sufficiently esteemed. But if our Lord does not encourage us to
repentance or give us wherewith to assuage and diminish our
sorrows, but all hope is taken away and we are totally forlorn so
that we see the naked sword continually before us, having no
means to remedy or succor at all, that is a dreadful threat. Never-
theless, it is not sent without cause, considering the hardness of
heart that can be seen in all men.

For until God has brought us to this condition, we are wholly
blockish. We are contrary. We are certain to complain when
anything troubles us. Indeed, sometimes we lie as beaten down,
but that does not make us to come again to Ged. For we see how
every man takes the bridle in his teeth, so that they shake off all
fear and never think upon what is set down here. And so we see
how men become blockish, Now our Lord would fain draw them
to remembrance, if they are teachable; that is to say, if they are
not willfully stubborn. For He tries all manner of ways to draw
us to Him, This is the reason why He corrects us gently, and as
it were with His little finger. But when He sees that such will not
prevail, then must He bring us to this kind of shock. For until
He has left us as men distracted, we shall never have our minds
peaceable and obedient; we shall never be won.

Therefore, let us mark well that this threat is directed to such
as do not bow under the first blow God gives them, but go from
evil to worse; for then must they of necessity come to this place.
Now, He speaks expressly of the sight of the eyes. For men blind
themselves, as I have said before, persuading themselves that they
can escape by some means. Even though we see the evil at hand,
yet we may wonder to behold how we hold on to our course and
pay no heed to it. It is just as the prophet Isaiah says, speaking
of the wicked, that when the scourge passes over the whole earth
and the storm overtakes them all, they are not one bit moved by
