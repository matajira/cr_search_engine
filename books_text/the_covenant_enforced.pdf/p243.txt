Sermon 160: Signs and Wonders of Wrath 205

to us, notwithstanding that He uses some sternness in dealing
with us, testing our patience and quickening us up to come to
Him and to labor with a view to the heavenly life. But we must
always take the long view, as David says in the thirty-seventh
Psalm (v. 1), where he exhorts us not to be grieved at the prosper-
ity of the wicked, for he knows that our eyes can be dazed by it.
When we see a wicked person at ease and having all his desires,
we conclude straight away that God has no regard to deal with
men according to their worth. When we think thus, we stagger
and are in such confusion that we do not know what will become
of us. Now David says that in so thinking, he was acting like a
beast, out of his mind, and he confesses that he was at that point
devoid of reason and judgment, affirming that he did wrong to all
the generation of God’s children (Ps. 73:22), until he came to look
into the sanctuary.

It is true that in the thirty-seventh Psalm, which I mentioned
earlier, he says, “I passed by and saw the wicked flourish and
grow high like a cedar tree of Lebanon, and when I looked again,
he had been cut down like a tree that had nothing left but dry
stock lying on the ground, so that there remained no sign of him
at all” (Ps. 37:35-36). Such changes do we see in the world, but
even if we saw none, yet must we enter into the sanctuary of
God, as it is said in the seventy-third Psalm (v. 7). And there
we must wait patiently until God makes it plain to us that all
afflictions are profitable for His children, and also that He sends
them as medicines for their health. And contrariwise, that the
prosperity He permits to those who have contempt for His law
and His justice will be converted to their great confusion. We
must therefore learn to hold our wits and senses in awe, that we
do not wander in the afflictions God sends us.

Strange and Unusual Punishments

But to return to Moses and his purposes, let us note it well
when he says that the punishments God sends upon those that
have utterly rebelled against Him, who have refused correction,
shall be as signs and wonders to them and to their posterity.
