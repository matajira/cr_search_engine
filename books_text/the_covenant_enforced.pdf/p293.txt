Sermon 162: A Plethora of Plagues 255

altogether to His holy will. And that even though there is always
much to be amended in us, and our lusts and affections do draw
us quite away from what He commands us, yet nevertheless we
may not cease to strive against them, and to yield ourselves wholly
to Him, and to enforce ourselves hereunto more and more, know-
ing that the same is our sovereign welfare. And that although He
shows us now and then some tokens of rigor in correcting us, yet
we may learn to profit thereby, to the amending of our sins, so
that we may be brought to the perfection whereunto He daily calls
us. That it may please Him to grant this grace not only to us but
also to all people and nations of the earth; etc.
