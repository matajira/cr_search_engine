124 The Covenant Enforced

(Ps. 18:11). The prophet> would have us to consider that it re-
quires a most wonderful power to shut the rain up there, for
otherwise we should be drowned out of hand, or at least we should.
be rotted by it, as I mentioned. And here, by the way, we are
admonished, as we shall note hereafter, to consider our sins and
trespasses, and to think that when God gives us extraordinary
rains, they are as punishments for our offenses. Moreover, when
the rain comes in season, let us acknowledge that there does not
fall one drop but by God’s appointment, assuring ourselves that
it is necessary for us. That is the reason why Moses expressly
adds this particular saying.

The Gift of Labor

Next he says that God shall bless the labor of our hands ({v.
12). This is to make us understand, as he has already pointed out
before, that it is not our own labor and travail that feeds us; as
we have seen in the eighth chapter (Dt. 8:17), where he says,
“When you are come into the land that God gives you, remember
that during forty years you were fed with manna that fell from
heaven, so that you should not say, ‘It is my own labor that
sustains me.’” Let us therefore learn by these texts that when we
have travailed, and bestowed our labor to till the earth, and have
considered everything that is requisite, yet nevertheless we must
lift up our eyes and not stand in our own conceit so as to say, “I
have done this; it is done by my own labor and wisdom.” Let us
rather acknowledge that it is God’s doing to give increase, and
that without Him, all our labor is in vain; so that even if we were
breaking our arms and legs [backbreaking labor], yet instead of
going forward we should be falling backward.

The sum of the matter is that we should labor in all lowliness,
and when we have travailed for our living, we should understand
that it is not in us to give the success, but that God must wholly

5. Assuming that the sermon was accurately transcribed, and Calvin did not
really say “psalmist,” then he is referring to the psalmist as a prophet in chat all of
Scripture is prophecy.
