15
EXODUS UNDONE

Sermon 163. Saturday, 28 March 1556.
Deuteronomy 28:65-68.

65. And among these nations you shall find no ease, nor
shall the sole of your foot have rest; but the Lorn shall give
you there a trembling heart, and failing of eyes, and sorrow
of mind.

66. And your life shall hang in doubt before you; and
you shall fear day and night, and shall have no assurance
of your life.

67. In the morning you shall say, “Would God it were
evening!” And at evening you shall say, “Would God it were
morning!” — because of the fear of your heart with which
you shall fear, and because of the sight of your eyes that you
shall see.

68. And the Lorn shall bring you into Egypt again with
ships, by the way of which I told you, “You shall see it never
again.” And there you shall be sold unto your enemies for
male and female slaves, but no man shall buy you.

Among the benefits that we desire in this present life, one is
to have some resting place and some harbor to lodge in. Though
a man be poor and troubled on all sides, yet if he has some little
hole to hide his head to which he can resort, he cares not for all
the rest. But if we know not where to go, nor have any friend to

236
