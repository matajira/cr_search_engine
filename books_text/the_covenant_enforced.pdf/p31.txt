Editor’s Introduction xxxi

8. At the same time, I have not taken the liberty of com-
pletely rephrasing Golding’s prose, because I did not want to risk
departing too far from Calvin’s own style. Thus, the reader will
find a certain “archaic flavor” even in the present volume.

Let me illustrate with a section from Sermon 157 (one of my
short paragraphs). Here is Golding’s original, with spelling of
obvious words modernized but no changes in words and phrasing:

Now let us come to that which Moses saith farther. He saith
that Gop will strike the despisers of his law with many diseases. He hath
spoken heretofore of fevers, and of the whotte [hot] disease, and
of the yellow jaundice, and of such others: now he speaketh of the
Itch and canker and of other worms and scabs. Where also mention is
made of the Hemorrhoids, as some do expound them; all these
foresaid things be the weapons of Gon, to punish the offenders of
the Law. To be short, they be his men of war to encounter with
us, when he seeth that we take heart of grass [grace] against him.
And truly when we favor our own lusts to violate his righteousness,
and to break the order which he hath established among us: and
when he seeth our dusts to be so inordinate which are thieves and
robbers: he armeth his people and substitutes which be the dis-
eases that are here spoken of, and other sorts.

Now here is my updated rendering:

Now let us come to what Moses says next, that God will strike
the despisers of His law with many diseases (v. 27). He has spoken
earlier of fevers and of inflammations and of the yellow jaundice,
as well as of others. Now he speaks of the itch and canker and of
other worms and scabs. Mention also is made of hemorrhoids, as
some understand it. All these things are the weapons of God, to
punish the offenders of His law. In brief, they are His men of war
to fight against us, when He sees that we take courage against
Him. And indeed, when we favor our own lusts to violate His
righteousness, breaking the order He has established among us,
and when He sees our lusts to be so inordinate as to be thieves
and robbers, then He arms His people and substitutes, which are
the diseases that are here spoken of and other sorts as well.

In this paragraph are two obscure older English phrases. The first
