Sermon 163: Exodus Undone 261

True it is, that St. Paul speaks not only of the last resurrection,
but he begins at the comfort that God gives presently to His
children to train them to arrive at the port of salvation. Now then,
they enter into the midst of death even while they are alive.

And therefore let us remember that although God docs now
and then so afflict such as have walked in His fear and have put
their trust in Him, so that they are stricken with terror and tossed
with unquieiness, yet notwithstanding He holds them up by the
secret operation of His Spirit. And moreover He comforts them
and makes them glad in the end, accomplishing what is said in
another text, that the more they were distressed, the more did
He set their hearts at liberty, that they may freely come unto Him
and so consequently makes them understand that He never for-
sook them (Rom. 5:3ff.). Behold how our Lord tempers the troub-
Jes and afflictions of the faithful, so that although they endure
ever such great pains, and do even pant in coming to Him, as
people scarcely able to open their mouths to call upon Him, yet
when they have ended their conflict against their temptations,
even though at the first they cannot get the upper hand, yet will
they get the victory in the end.

But as for those who have acted like restless horses, they
remain pent up in their miseries without any remedy, because
God continues to set Himself against them more and more until
they become frantic and gnash their teeth at Him, and finally fall
into utter despair. In short, we see what is spoken by the prophet
Isaiah daily accomplished; namely, that there is no peace for the
wicked, but that their minds are tossed like the waves of the sea
(Is. 48:22). When there is any storm, the waves are troubled and
the water is full of mud. It is the same with those who despise
God. Because they make war against Him, therefore must He also
trouble them—insomuch that even without an enemy, yea and
without anybody to frighten them, they of themselves are fright-
ened and beat and bounce themselves as if they were at war
within themselves, so that there is no calmness at all in them, but
they are full of trouble and disorder. For the faithful are enlight-
ened in the midst of their darkness, and God stilt comes to them
