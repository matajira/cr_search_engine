Sermon 154; Blessing and Affliction 103

and blessed shall you be in the field. Blessed shall be the fruit of
your body and the fruit of your land and the fruit of your cattle.”
It is certain that the chief blessing we can desire of God is in our
own persons, that He provide us such things as are requisite for
this transitory life. And that is the reason why He begins with our
persons, and then comes to our children, and then descénds to
our cattle, to the fruit of the ground, and to all provisions.

It seems at first glance that God intended to pamper His
people, as if he would set them up in a pig sty, and there cram
their bellies, to let them sleep at their ease and in brief, that they
should be here as in a paradise. But I have told you already it
behooved God to bestow His benefits more largely upon the
fathers that lived under the law, because they could not otherwise
be drawn to Him, seeing that the promises of eternal life were not
clear to them. In any case we have to note herewithal that when
God speaks after this manner, He does not mean only to give men
hope that they will be blessed in all respects, but also to admonish
us that nothing prevents us from enjoying perfect felicity in this
world, while we wait for the life of heaven, except the fact that
we are wrapped in vices and corruptions.

And in this way God meant to train the faithful, that they
should reason thus with themselves; “Let us serve God, and we
shall lack nothing. And if we are vexed, and do not have all our
desires, if the earth does not yield such fruitfulness as contents us,
or if our cattle do not prosper as we wish, let us understand that
this comes to pass because we are wretched creatures and cannot
abide the perfection of God’s blessings, so that He finds it fitting
to withdraw His hand and to give us only a portion of His
blessings, because if we should have more it would glut us, and
in the end we should be choked with it.”

This is the reason, then, why God here so greatly magnifies
His blessings. It is done, not because men have ever been blessed
in all manner of respects without feeling any vexation at all; this,
I say, has never happened. So then, why does God promise it?
As we have said before, it is to make us understand that whenever
our life is not as blessed as we wish, we should consider that God
