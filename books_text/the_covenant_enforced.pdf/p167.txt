Sermon 155: Separation Unio Blessing 129

be our God. For the true reason that men stray, and altogether
vanish into so many superstitions and idolatries, is that they know
not what they ought to worship.

We have, therefore, greatly profited from the gospel, from the
law, and from the prophets, when we have the skill to say, “This
is the God who showed Himself to Abraham, the God who showed
Himself by Moses, the God who lastly showed Himself fully in
the person of His only Son, and the same is He who is our God.”
As it is also said in the prophet Isaiah (25:9), where he speaks of
the manifestation of our Lord Jesus Christ: “Lo, this is He; lo,
this is our God.” Then let us have a settled faith, that we not be
rovers. Let us not be like little children or like wavering reeds
that are carried every which way; but let our faith be well grounded,
by taking root in our Lord Jesus Christ.

Prayer

Now let us fall down before the majesty of our good God with
acknowledgement of our sins, beseeching Him to teach us true
repentance, so that we may bewail them and be heartily sorry for
our corruption, to withdraw us from them more and more, and
therein to reform us. And forasmuch as we are not only frail but
also altogether rebellious, that it may please Him to bring us
home again to Him and to prop up our weakness with His strength,
so that we may overcome all the hindrances that serve to turn us
from Him, and that we may with perfect constancy go on to the
mark to which He has called us, until we attain to the perfection
of all righteousness. And that in the meanwhile, it may please
Him to uphold us in such a manner that even if we fail, yet He
will cease not to take us for His children and make us to under-
stand more and more that He confirms us in the trust of our
salvation. That it may please Him to grant this grace not only to
us but also to alt people and nations of the earth; etc.
