12 The Covenant Enforced

we possibly can, to maintain unity and agreement among our-
selves, as we shall declare by and by. It was God’s will that there
should be only one Temple. Why? Because He wanted it to be a
bond to hold the people together in the purity and soundness of
the faith: “We have only one God who is called upon among us,
and we must come into one certain place to sacrifice to Him, and
all of us must gather together there.” It is indeed true that we are
not nowadays tied to any such a system, but no matter what the
circumstances are, yet the substance remains for us. Let us there-
fore take heed unto every aid we have to hold us in this commun-
ion of faith, and in this unity that God requires; let us keep them
well, and let no man turn aside from them.

Concerning the outward order of things, we know that our
Lord Jesus Christ would have men to assemble themselves to-
gether. I grant that we are not bound ail to be in one place, and
men also preach in various churches in one town. Why? Because
the whole world cannot be present to hear one sermon. Yet for all
that, because of our slowness we are so bound that we must gather
ourselves together in the name of God. He who wants to stay at
home, despising the common order, and says, “I can read at
home and edify myself sufficiently there,” that man breaks asun-
der the unity of the faith and tears in pieces the body of our Lord
Jesus Christ, with all his strength.

We know that baptism was ordained to serve as a common
seal that we are the Church of God, and are governed by His
Spirit. Now if every man wants to have his baptism performed
separately, what a wicked disorder that would be!®

The Holy Supper of the Lord is also distributed as a common
food so that we should all communicate of it. We are warned by
it that we are one body. And as one loaf is made of many grains
of wheat, which are so mingled together that they make but one

6. The Bible, of course, does not insist on baptisms’ being performed in the
course of a worship service, any more than circumcisions had been. The Protestant
Reformers were anxious to restore the reality of congregational worship, and thus
sought to bring baptism into it.
