8
GOD’S THREATS

Sermon 156. Friday, 13 March 1556.
Deuteronomy 28:15-24.

15. But it shall come to pass, if you will not hearken to
the voice of the Lorp your God, to observe to do all His
commandments and His statutes that I command you this
day; then all these curses shall come upon you, and overtake
you:

16. Cursed shall you be in the city and cursed shall you
be in the field.

17. Cursed shall be your basket and cursed shall be
your kneading-trough.

18. Cursed shall be the fruit of your body, and the fruit
of your land, the increase of your cattle and the flocks of
your sheep.

19. Cursed shall you be when you come in and cursed
shall you be when you go out.

20. The Lorp shall send upon you cursing, vexation,
and rebuke, in all that you set your hand unto, until you be
destroyed, and until you perish quickly; because of the wick-
edness of your doings, by which you have forsaken Him.

21. The Lorp shall make the pestilence cleave to you,
until He has consumed you from off the land to which you
are going to possess it.

22. The Lorp shall smite you with a consumption, and

130
