Sermon 163: Exodus Undone 265

as it is said in Job (13:14) and in Psalm 119 (v. 109).

Now, when it is said that we carry our lives in our own hands,
it is to show that whereas life should bear the man, the man bears
the life; that is to say, the life is left up to the hazard of all
misfortunes.? This is how it is with all men by nature.3 At the
same time, as we have said, the faithful are not free from it, as
we see in the examples of Job and David. Notwithstanding, it is
those who despise God who most feel that their lives hang by a
thread. And why is this? Because God, in spite of their rebellion,
forces them to perceive the perils that beset them. They see very
well that all manner of creatures are their enemies, and because
of this they start when they hear but the falling of a leaf, and they
are afraid when no one pursues them, as it is said in Proverbs
(28:1).

Thus does God open the eyes of those who are puffed up with
pride, and are drunk with the delights of this world, especially
those who have hardened themselves in stubbornness against Him.
He opens their eyes that they may know how their lives are less
than nothing, and that all these shadows, lies, and vain hopes
upon which men are wont to rest are but baits to beguile them.
Behold here, I pray you, where they come to who have scorned
God and His Word and are wholly given over to this world,
reposing their trust on earthly means and on creatures. At the
last they will be caught with such fear as will teach them double
and triple to understand their own condition,

Driven by Fear
Having spoken thus (v. 66), Moses then adds {v. 67) what
they shall say: “Who will assure us that we shall see the morning?
How can we be sure we shall come to the end of the day?” When
men understand once how frail they are, and with how many
perils and miseries they are enthroned, they then have no more

2, This is a bit obscure. Calvin seems to mean that men should not have to
worry about life, since it should be a gift from God. The sinner, having taken matters
into his own hands, now must worry about his own life.

3, That is, by sinful nature.
