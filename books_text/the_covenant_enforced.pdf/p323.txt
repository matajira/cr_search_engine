ANNOTATED TABLE OF CONTENTS

Publisher’s Preface, by Gary North... 0. 6c eee vii
Was Calvin a Theonomist?, viii
What Is Theonomy?
John Calvin, Theonomist
Calvin’s Social Theory, xii
Blessings in the Small Things
A Covenantal Promise
A Visible Testimony to Our Enemies
Can Such Things Really Be?
Negative Sanctions, Too
Ethically Random History: A Non-Calvinist Theology, xviii
Meredith Kline vs. John Calvin

Editor’s Introduction, by James B. Jordan... 0. ee xxvii
Historical Context of the Sermons, xxvii
Translation, xxix
Calvin and the Law, xxxii
A Note on Calvin’s Pessimism, xxxv

1, Sermon 149: Altars and Ensigns .........05- 1
Deuteronomy 27:1-10
Spokesmen for God, 4
A Temporary Altar, 6
Temporary Ordinances, 9
The Unity of the Faith, 11
The Duty of Praise, 13
Gratitude, 14
The Duty of Obedience, 16

285
