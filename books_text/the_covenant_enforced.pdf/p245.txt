Sermon 160: Signs and Wonders of Wrath 207

heaven. And this is more fully declared in the twenty-sixth chap-
ter of Leviticus. For after He has pronounced the sentence upon
them that reject the doctrine of salvation, He says, “I will send
upon you seven times as much if I perceive that nothing prevails
with you. If My punishing you fails to amend you, I will add
thereto seven times as much” (wv, 18, 21, 24, 28) And He repeats
this sentence again in the end, and says, “If you will walk contrary
to Me, I will also walk contrary to you.”

The Hostility of God

Now this word, “contrary,” refers to the stubbornness that is
in us, which we have touched upon before. For we see very well
those adversities that befall us in this world, and wading in yet
further we confess that it is God who punishes us. But to think
on the matter in good earnest, and for each man to judge himself
by calling his sins to remembrance as often as God gives any
inkling of them — this is farthest from our thoughts. Let each man
examine himself: How many afflictions do we have during the
year that should be testimonies to us of the wrath of God, and as
it were summons to appear before Him, warning us to sue to Him
for pardon and [to beg Him] to have pity upon us? Scarcely does
any one day pass, but that a man is warned five or six times. It
is as if God should say to him, “Wretched creature, have you no
concern for your soul? Why have you no care to beseech Me to
receive you to mercy?” But scarcely once in a month will a man
enter into examination of his life to condemn himself. And if we
do it, it is but coldly. But we ought to be as ashamed and as vexed
at seeing God’s wrath as though we saw hell lie open before our
eyes. But we see that very few actually think on these things,
because each one of us forgets himself. That is the matter God
intends by His threat to walk adversely to us.

Yet we tend to continue on our course as if nothing had
happened to us. We swallow up our afflictions. They do trouble
us, indeed, when they pinch us, yet we do nothing but shake our
ears (as they say) and continue in our own way. Thus, we proceed
in hostility, yea exceedingly so, when we fail to acknowledge the
