Sermon 160: Signs and Wonders of Wrath 203

But on the other hand, if we do not perceive the reason why
God is moved to treat us rigorously, let us be content, recognizing
that He knows it is profitable for us. If we have not already
offended Him, perhaps we were on the way to doing so and He
has prevented it. All these corrections are designed to bring us
low, to the end that we should walk under Him in fear and that
our flesh should not overcome us, as is its tendency. For God
sometimes foresees the pride of a man, and then He takes away
the occasions and the objects. Besides this, He knows that a man
will be too cavalier in his pomps and delights, and therefore he
cuts off the occasion beforehand, preventing a man from doing
what he would, Seeing that our Lord provides beforehand for our
welfare after such a fashion, let us think on the faults we have
earlier committed.

Moreover, if there were no further meaning in it than to move
us to repentance, even that would be plenty, But we must always
consider how God cannot provoke us too much to come to Him,
for every least straw is enough to hold us in this world, so that
meanwhile we do not think on the heavenly life at all, or if we do
think about it, we do it so coldly that we do not travel towards it
with the kind of earnest affection we should have, Therefore, God
finds it necessary to deal out to us many afflictions. This is how
every one of us should deal with them.

Our Attitude Toward Others

Now, concerning others, we may not at first blush condemn
those whom God is punishing. We must keep in mind what is
said in the psalm (Ps. 41:1), that God shall bless the man who
deals kindly with a man in tribulation. But we have an incredible
way of jumping hastily to conclusions in this matter. As soon as
we see any poor man in misery and wretchedness, we say, “Oh,
God is plaguing him, and he must deserve it.” When we do this,
we are very rash judges. If God smites us, after He has bestowed
many stripes upon us, it is still hard for us to admit that we have
sinned; but concerning others, our tongues are ever so quick to
condemn them.
