208 The Covenant Enforced

hand of God and do not perceive that He is a judge, so that we
might condemn our sins and each of us labor and endeavor to
withdraw himself from them.

Therefore, our Lord says that He shall come against us in all
hostility. It is as if He should say, “I will cross you and thwart
you. Don’t think to gain anything by your hardness of heart and
by your kicking against Me, and by your dullness in refusing to
perceive that it is My hand you are dealing with. No, no,” He
says, “I will be as stout and as headstrong as you; yea, and more
stout and headstrong, too. For I will manifest all sorts of hostili-
ties. I will let My plagues run out at random, so that I will break
your necks, and beat you on both back and belly, and that with-
out pity.”

Now we can see how much this word ought to weigh with us,
where Moses says that the plagues shall be for signs and wonders
to all scorners. When they have given the raspberry! at the threats
of God, and have wagged their heads against the first corrections
that He sends them, and have bitten on the bridle; yet He pro-
ceeds onward still, and does not cease to wring them, but drives
them in the end to come te a reckoning. Having done their worst,
they will say openly, “It is the hand of God that presses us; these
are miracles, no ordinary thing, not according to the course of
nature.”

Now therefore let us learn not to provoke God’s wrath so far
against us. Let us allow ourselves to be tamed by Him, and let
us yield ourselves tractable and gentle as soon as He has sum-
moned us. Let us yield ourselves guilty without using any excuses,
for we shall win nothing thereby. There is nothing better than to
enter into pure and free confession, saying, “Lord, what will You
do to these frail and wretched creatures? It is true that we have
deserved a hundredfold to be sunk, but yet for all that we flee to
Your mercy. Wherefore, have pity upon us.” When we have thus
condemned ourselves, it serves to pacify the anger of God, which
will never happen if we harden our hearts. For then He will

1. In Golding, “bleared out their tongues at the threatenings of God.”
