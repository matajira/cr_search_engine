154 The Govenant Enforced

heathen have used it, and it will be a witness against them at the
last day when they will be convicted by their employment of that
ceremony, which should have taught them to look for the last
resurrection; their failure to consider it so is inexcusable. For our
burial ought to be for us an impressive mirror or portrait to show
us that we are not created to decline into corruption, as if there
were not another life and as if we should not be restored into a
new state. And it serves always for a larger declaration, which is
that mankind perishes but for a time, and that their bodies will
be renewed.

Now, since burial is a memorial of the resurrection, as I said
before, therefore it is given to men as a privilege to be buried. In
this respect there is an honest virtue in it, so that we who remain
are taught as it were by eyesight to look continually for a second
life. For the dead man also bears a certain mark in his body, that
he is as it were laid up in safekeeping until the day comes that
God will raise the dead again.

Now on the other hand, when it said that men shall not be
buried, but that they shall be eaten by beasts and fowls, it is as
much as if God meant to deprive them of the common benefit
that He had granted to all mankind, and as if He had showed
openly that both in life and in death they were accursed of Him,
And that is why it is said, “You shall be buried with the burial
of an ass” (Jer. 22:19). This was spoken by the prophet to a king,
even the king of Judah. And because he would not be corrected
in his sin, and because God had given him the honor to bear in
his lifetime the figure of Jesus Christ, and notwithstanding he had
abused the same privilege and given himself over to all kinds of
ungodliness, then “you shall be buried with the burial of an ass,”
says the prophet.

Hereby we are warned to have very great regard of all the
corrections that God sends us, even of the very least, that we may
always be stirred up to fear. For why do we pass over so many
chastisements of God without profiting from them? It is because
we shut our eyes willfully, and unless we are compelled by sheer
force and necessity, we are content to think that it is not God who
