240 The Covenant Enforced

completely, just as He said also that they would cleave to them
that were rebellious and could not be won by gentleness. These
plagues must be rooted in them, and for that reason He terms
them “certain.” Here we must consider our own hardness, that
every one of us may be more vigilant in looking to the hand of
God, and benefit ourselves from it. And let us be afraid lest God
should do what is threatened here in this text, which is that when
His normal punishments do not profit us, then He nmst proceed
against us after such a strange and horrible mamner; indeed, such
a way that will make us fearful and cause the hairs to stand on
the back of the heads of all those who but hear of it, and as the
prophet Jeremiah says, to make men’s ears tingle when the re-
ports of it come into foreign countries (Jer. 19:3).

Let us be afraid, then, lest God work on us after that manner,
when He perceives such stubbornness in us that He cannot win
us by measurable and tolerable correction. And with this let us
also take warning by this phrase, “sicknesses wretched and chronic,”
or lasting (v. 59c).2 When God has followed us to bring us to the
way of salvation and we have fled from Him, then the plagues
must also continue to the uttermost, both upon us and upon all
our descendants, so that when we think we have seen an end of
them, we shall find that we have not yet begun. Let us not,
therefore, think that we shall escape when the plague has lasted
for a while. For as we proceed on in our wickedness, so must God
proceed with His rigor and augment it more and more, according
as He sees us wax worse and worse.

When this happens, let us not do as we see most men do, who
cry out and storm at it, saying, “When will this end? Why is it
that God takes no pity on me, to ease my pains?” But when they
say this, do they have any intention of reconciling themselves to
God? Do they confess their sins and bewail them? No, rather they
defy Him at this very point. All the while they talk about their
feelings and make their complaints, but not one of them looks to
the foundation of the trouble in order to set it right.

2. The same Hebrew word for “lasting” as in the earlier phrase of the verse,
