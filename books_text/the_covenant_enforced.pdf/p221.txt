11
THE PERIL OF APOSTASY

Sermon 159. Tuesday, 24 March 1556.
Deuteronomy 28:36-45.

36. The Lorp shall bring you and the king whom you
set over you to a nation that neither you nor your fathers
have known, and there you shall serve other gods — wood
and stone.

37. And you shall become an astonishment, a proverb,
and a byword among ali nations where the Lorn will lead

Ou.
* 38. You shall carry much seed out to the field and
gather but little in, for the locust shall consume it.

39. You shall plant vineyards and tend them, but you
will neither drink of the wine nor gather the grapes; for the
worms shall eat them.

40. You shall have olive trees throughout all your terri-
tory, but you will not anoint yourself with the oil; for your
olives shall drop off.

41. You shall beget sons and daughters, but they will
not be yours; for they shall go into captivity.

42. Locusts shall consume all your trees and the pro-
duce of your land.

43, The alien who is among you shall rise higher and
higher above you, and you shall come down lower and lower.

44, He shall lend to you, but you shall not lend to him;

183
