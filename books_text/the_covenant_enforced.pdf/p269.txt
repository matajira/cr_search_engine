Sermon 161: God Our Fortress 231

time; for it would have been far better for them if they had been
dispatched out of hand.

And with all of this let us every one look to himself. Let us
not trust in the patience that God uses towards us, deceiving
ourselves therewith. Indeed, when God has been patient and
forborne us, it should give us a better disposition to resort to
Him, and to hope to find Him merciful to forgive us our faults
and trespasses, But we must not fall asleep therein, to dally with
Him and to say, “O well, He has waited this long to deal with us.
He will continue to wait some more.” We see the wicked thinking
this way. They don’t worry, for when they see that God has
spared them, they think that they are discharged. Let us beware
of that. For we see how he says that when we are well appointed,
and have fortresses as it were invincible, all must fall down on
our heads, and we shall be so much the more grievously punished.
This much we have to bear in mind from this text.

Now let us mark further that when we forget our God, we
must also forget all the course of nature, and He finds it proper
to strip us of our wits. For it is He on whom all human fatherhood
depends; He is the wellspring of it. We would not know what the
duties of parents toward their children are, or what reverence
children owe to their parents and superiors, except for the fact
that God is sovereign leader in this. If we do not refer all to Him,
He shall make us to become idiotic, so that we will not understand
what this fatherly affection toward our children should mean.
This much for one point.

But we also have to note that those men who did so devour
their own children were not so blockish as to be bereft of terrible
heart pangs. They were vexed with furious fearfulness, that made
them abhor what they did. It is true that they were carried away
by force, and had no reason in them; but yet for all that, they had
certain secret stings and prickings inwardly, and God held them
as it were upon the rack of torture, as if He were saying to them,
“What will you do, wretched creature? It would have been better
for you to have been born before your time, and for the earth to
have swallowed you up a hundred times, than that you should
