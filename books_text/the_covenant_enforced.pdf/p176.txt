138 The Covenant Enforced

serpent come in and sting you there; if you go into the field and
seek a way to escape, you shall meet a lion on the way; if you slip
aside from the lion, a bear will meet you” (Amos 5:19; cf. 9:1-4);
and to be short, whatsoever men do, when God is against them
and has become as it were their enemy, they must come to find
that they are discovered, caught, and entrapped, for they are
environed round about with these curses, with no hope to get out.
Therefore, let us not deceive ourselves in seeking worldly means
to save us, for if the hand of God is lifted up against us (as we
shall see in the song here at hand—Dt. 32:23), He has His
storehouses full of arrows, and not of three or four sorts only, but
of infinite. And if we suppose that we have escaped when we have
overcome some evil, then we deceive ourselves, for God has laid
by one hundred more. Let us then look for all manner of woe if
we disobey God’s law.

But it would be a miserable appeal if we should come unto
God by compulsion, solely for fear of His punishments. What
must we do, then? We must submit ourselves willingly unto Him,
praying Him to keep us from His wrath, and from warring against
Him, and from forcing Him to come to destroy us. That would
be as if some petty lord should attempt war against a great prince,
having neither strong town, nor munitions, nor anything else, and
yet should presume to defy one that is able to swallow him if he
but lifts his finger against him. Wouldn’t that be insane? Likewise,
when we provoke our God by setting ourselves against Him know-
ingly, He must war against us and put us in prison.

So then, let us pray Him not to allow us to be so far gone as
to fall into the practice of defying Him, but rather to enable us
simply to obey Him so that we may be crowned and encompassed
with His blessings, as it is said in Psalm 32:10, “He that puts His
trust in God shall be crowned with His benefits,” which is to say
that God will make him to possess His blessings round about him,
and he will be so thoroughly fenced on all sides with His safeguard-
ing that he will have no need to fear. Although we are laid open
to many dangers, yet shall we be preserved through His goodness.
Now, as touching what he adds, we have expounded here before.
