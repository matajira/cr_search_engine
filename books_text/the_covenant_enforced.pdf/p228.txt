190 The Covenant Enforced

Let all those therefore that are in the captivity of papistry and
mingled among the superstitious understand that the vengeance
of God lies upon them, and that they will be more and more guilty
for their serving of idols. It is a poor reply for them to say, “We
do not do it willingly. We wish that it might please God that the
right and pure religion were over all.” But for all this, our Lord
does not exempt them from condemnation. And He is the compe-
tent judge. Let us therefore rest upon His Word, and seek no
more escape routes, but let them that endure such a state under-
stand that it is now or never when it comes to turning to God,
since they are as good as drowned in their present condition and
possess a token of wrath against themselves in that He is gone far
away from them. Therefore let them think upon this, and be
moved with it to the quick, according to the meaning of God in
this text, and as we have touched on it heretofore.?

An Example to the Nations

Now Moses says that this people shall be an astonishment, a
proverb, a byword, and a ridicule among the nations in which
they will be dispersed. Here our Lord shows that as His goodness
should be displayed among the people of Israel, so that every man
should rejoice in the seed of Abraham, so should the very same
people be abhorred and detested. The promise to Abraham was
thus: All nations shall be blessed in thy seed. Of course, it is true
that we must look to our Lord Jesus Christ, who is the very bond
of the seed of Abraham, or else this blessing has no place or
ground to stand upon. Yet notwithstanding, they who were de-
scended from the race of Abraham should have been blessed by
God so that they might have been an example, that everyone
desiring grace might say, “O God, take pity on me, as upon the

2. Galvin does not make it clear what he thinks these people ought to do about
their situation, By 1556, however, the Reform in Europe was pretty well advanced,
and Calvin probably would have answered that anyone genuinely grieved by idola~
trous worship should separate from the Roman Church and join the Reform, Those
who remained in idolatry, and did no more than wring their hands over it, would
be condemned,
