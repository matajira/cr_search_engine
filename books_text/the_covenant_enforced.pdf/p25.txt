Publisher’s Preface XXV

sixteenth century were less developed than today’s discussions in
many respects, despite the far greater intellectual rigor of the
theological discussions of that era compared with ours. But this
should not blind us to the obvious: john Calvin’s covenant theology
was in fact biblically covenantal in structure. He believed in 1) the
sovereignty of a Creator God, 2) a God who reveals Himself in
history, 3) a God who lays down fixed laws, 4) a God who brings
predictable historical sanctions in terms of these laws, and 5) a
God who (probably) raises up His people to victory in history.
He did not adopt the six loci of seventeenth-century Protestant
scholasticism, with its narrow definition of theology. His Calvin-
ism was not narrowly theological; it was cultural in the broadest
sense.

In this sense, Calvin was a Christian Reconstructionist. His
is a legacy worth recovering or suppressing, depending on one’s
agenda. (When you find a Calvinist who appears to be involved
in suppression, ask yourself this question: “What is his agenda?”
Then seek the answer.)
