214 The Covenant Enforced

He takes steps to remedy it in due time. Yet notwithstanding this,
all the corrections we endure in this world proceed from our sins,
and the filth that is in us is the reason why God does not send us
abundance of good things according to our desire. The fact is that
men fall aslecp and sap themselves in earthly pleasures, so that
they are not able to consider what David calls them to, which is
to satisfy themselves with the sight of God and to rejoice in His
presence. Seeing that men cannot attain thereto, but are con-
stantly wedded to these base things, do we not deserve for God
to withdraw the plenty that He was ready to give us? For He
perceives that it would burst us, and instead of sustaining us it
would so overload us that it would bear us down to the ground.
This is the reason why our Lord does not give us gifts liber-
ally, such as we desire. After all, He is not drained dry, nor does
He fear being impoverished by sending us plenty of all manner
of good things. We know that He is a fountain that can never be
drawn dry. But He sees that we waste and devour His benefits,
and are worse than drunken, and that in addition to our riotous-
ness we are also unthankful, not only falling into forgetfulness but
also spurning His majesty and turning our backs upon Him.
Indeed, if we have the means to maintain ourselves well, we fall
to gluttony, pompousness, whoredom, and other looseness. And
to be brief, the abuses we commit in the use of God’s blessings
are as immeasurable as the sea. Therefore, when He sees such
things, He withdraws His hand and stops showing Himself liberal
towards us. This is the sum total of what we have to bear in mind.
And therefore, seeing that God for His part is always ready
to multiply us, and to give us plenty of all good things, were it
not that we are unable to bear it, we must understand that we are
like sick people who may only eat a little food at a time, being
constrained to a diet, And why? Because they are not able to keep
their food down. From this we ought to understand what Moses
is saying, that it is to our reproach. For what a shame it is that
we cannot abide that God should deal gently and in a fatherly
way with us! Behold, God offers Himself not only in words but
also in deeds, and He offers to give us all that we desire. But
