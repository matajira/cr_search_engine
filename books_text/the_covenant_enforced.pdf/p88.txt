50 The Covenant Enforced

or highly favored by the world, we dare not meddle with him.
Even if he has molested us, yet we shall sweetly swallow it up and
be very careful not to provoke him. In such a way men will put
up with all such as have the wherewithal to maintain themselves
in this world, even when those who are destitute are robbed and.
devoured. Yet notwithstanding, God is their defender, and He
says that in oppressing such folk, we make war against Him, and
therefore that He also will not be slow to lift up His arm in the
maintenance of those whom He has taken under His protection.

But men do not heed that. And thus do we not betray our
unbelief? For if we had a true and lively sense that God is not
jesting here, when He tells us that His curse shall light upon those
who mistreat the weak and such as have no champions, it is
certain that we should quake whenever we were tempted to do
evil to any poor creature that has no stay, credit, or authority in
this world. I see that this man has neither relatives nor friends. I
see that no man regards him. I see that he is undefended. Now,
if I advance myself against him, or if I trouble him, God will set
Himself against me, for He has set His mark upon the poor man
and has told me that if I meddle with such a person, He will take
the case to Himself, and I shall be regarded as having challenged
and disparaged His own majesty. If we kept this in mind, it is
certain that we should be much more restrained and held back
by His fear, than we are by all the favors and displeasures of this
world. Nevertheless, we see the clean contrary — we are very dull
in this regard.

It is certain that such cruelty as is committed against poor
folk who have no stay to lean upon, demonstrates manifest con-
tempt of God, and an utter scorning of Him, as if to say that He
is unable to execute the vengeance He has threatened. Now when
God is so lightly esteemed, do we think He will put up with it?
You see here first how this kind of outrage is even against nature.
For if we were not so caught up in our wicked affections, surely
every one of us would confess that it is much worse to have hurt
or devoured a poor weak creature than to have done harm to a
rich man who is well allied and has reserves and the power to
