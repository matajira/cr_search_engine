Sermon 156: God’s Threats 131

with a fever, and with an inflammation, and with an extreme
burning and with the sword and with blasting and with
mildew; and they shall pursue you until you perish.

23 And your heaven that is over your head shall be
brass, and the earth that is under you shall be iron.

24, The Lorn shall make the rain of your land powder
and dust; from heaven shall it come upon you until you are
destroyed.

We have seen the past several days how God entreats His
people with promises. Now on the opposite side, He adds threats.
And that is not without a good reason, for we see how slow we
are when it comes to submitting ourselves to obeying God. Our
feet are swift enough to run to evil, as the prophet Isaiah says
(59:7), and as it is spoken of in the Proverbs (1:16; Rom. 3:15},
but God cannot make us set forth one step to behave ourselves
properly, and therefore we must be compelled to it by force.!
Nevertheless, God certainly begins with gentleness and goodness.
And that is why He first sets forth His blessings to those that serve
Him. He might very well have begun with threats, but He did
not. In so doing He makes a test to see whether we are apt to be
taught, by showing Himself fatherly towards us, and by making
it to be seen that he seeks nothing other than our benefit, welfare,
prosperity, and quietness.

Promises as Motives

Thus we see how God first tests us, and if that does not avail,
He uses threats. These two belong together. Let us consider the

1. Did Calvin really say “God cannot make us set forth one step to behave
ourselves"? Maybe; at least his translator did. If so, this shows that Calvin was not
such a stickler for precise language that he refused to use common jargon to get
across his ideas. Everybody knew that Calvin believed in irresistible grace. Secondly,
Calvin is speaking of God’s “inability” in a covenantal sense, not in a decretal sense.
God is unable to get through man's sinfulness by means of covenantal promises and
threats; obviously, God is fully able to change men by means of regenerating grace.
Calvin is not so focussed upon decretal theology that he is unable to speak freely
about the covenant,
