156 The Covenant Enforced

of this world. Thus you see what this text of Moses serves for us,
where he says that the birds and the beasts shall eat our carcasses,
and that no man shall drive them away.

Now, if God gives such tokens of His wrath to our bodies,
which have no motive in themselves, what will become of our
souls, in which is the very seat of evil and the kingdom of Satan?
For our hands, our feet, our eyes, and our ears do not offend of
their own proper motion, but by the direction and provocation of
our wicked thoughts and affections. And where do all those things
lie and whence do they proceed, but from our souls? Seeing then
that the bodies, which were but instruments, do feel the wrath of
God and do answer to the same, as we see, let us not suppose
that the souls will escape. Let us therefore always look higher
than this transitory life in order to wake ourselves up, and let us
walk in such obedience that, first, after we have fought against sin
and Satan and have been exercised in patience and in various
afflictions in this world, having done our endeavor to serve God
with all diligence, our souls may be taken up to rest, and that,
second, our Lord in like manner may show the same favor to our
dead bodies,

Notwithstanding, this favor and blessing of having their bod-
ies buried does not always happen to the faithful, and it falls out
that many of the wicked are very honorably buried, even though
it is nothing to them and their state is not improved in the least
thereby. We sce what is said in Psalm 79:3 concerning those that
have faithfully served God, namely that their bodies were laid out
as a prey to the fowls and the beasts; and yet they were the
children of God. And although this is a testimony of God’s wrath
upon the wicked, yet it may oftentimes come to pass that our
Lord will use the same manner of chastisements upon His own
people, and yet it shall not harm them.

It is true that we must always humble ourselves when such a
thing happens, and we must understand that such things are as
it were marks of Adam’s sin and of the corruption that is in us,
and also of the offenses we have committed. And yet when He
permits the faithful to be deprived of burial our Lord turns this
