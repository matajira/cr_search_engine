Sermon 159: Separation Unto Blessing 417

here, saying, “If you will keep the commandments of your God,
as I do set them before you this day” (v. 9). For there is good
reason for us to yield ourselves to God’s direction, seeing He has
so bound Himself to us. And we should not only profess with our
mouths that we are His people, but also show it by framing our
whole lives agreeable to the same; and by keeping His command-
ments make it apparent that we have received the grace He offers
to us. For that is the true evidence thereof:

The Heathen Shail Fear

Now He says moreover, that other people shall see that we
are called by God’s name, and they shall fear us (v. 10). It is not
enough that God promises to make us feel that we are safe in His
keeping; but He also says that even the pagans, our mortal ene-
mies and the despisers of His majesty, shall be made to know the
same. Now it is certain that the infidels do not know the arm of
God in such a way as it ought to be known to us. They come far
short of it. For though they see, they do not see. How then can it
be possible for them to perceive that God has blessed us, that we
live by His favor, and that we are nourished through His provi-
sion? After all, they are blockish, and do not recognize that any-
thing comes to them from the hand of God. We see well how the
infidels are fed and clothed; they enjoy the light of the sun; indeed,
they have an abundance of goods. But as for the worshipping and
seeking of God, we hear of nothing of the sort among them. And
if besides their despising of the benefits of their God, they have
no understanding whence they come unto them, how should they
then know that God has named us with His own name?

They will not know it through any persuasion of mind or
through any such true understanding of it as we ought to have.
But Moses says that they shall have it proved to their faces; as for
example, we see the wicked grind their teeth when they behold
the faithful prospering, and when they see that God upholds and
keeps them. And how does this come about? Truly they will be
astonished at it, and they will not be able to think otherwise but
that God does indeed favor their adversaries — not that they take
