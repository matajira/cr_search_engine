226 The Covenant Enforced

for one month or one year or even a whole lifetime, we shall be
suddenly surprised in a morning before we can think upon it. Let
us therefore understand that as scon as God speaks, we must
have an eye to His infinite power (which is incredible to man’s
understanding), so that we tremble at His very word and yield
to Him, knowing well that if we delay from day to day, we may
be prevented from doing so, and it will then be too late. Let us
therefore be advised to humble ourselves in due time, and to crave
pardon when we have offended Him.

No Shelters From God’s Wrath

Moses says expressly that those who are disobedient to God
shall be besieged and shut up within their walls until their for-
tresses in which they trusted are beaten down (v. 52). Here we
see how God reproves the false and cursed presumption whereby
men deceive themselves, thinking that they are well protected
against Him. It is a sacrilege whereby God is robbed of His
honor, when we attribute to the creation the means of our defense.
It is certain that God serves Himself by means, and we must also
apply them to our own use, but to settle our érust upon them is to
rob God of His majesty. For He will have us to maintain that all
things are His, and to do Him homage for them.

So then, it is an intolerable presumption for us to put our trust
in creatures and to ground ourselves upon them, and yet there is
no fault more common in all the world. We can say well enough
that it is not lawful, and that we ought not to do it, but meanwhile
everyone does it. Now, let us remember ourselves, for there is
nothing that God holds more dear or whereof He makes greater
account than His honor. Neither is it sufficient for us to leave Him
the mere title and name of God. He must be known as He is; that
is to say, that all power lies in Him; that He is the fountain of all
manner of grace and goodness; that it is His proper office to
maintain and preserve us; that our life is in His hand along with
all things that pertain thereto. When we have thought well upon
this, let us take order to gather our wits about us, so that we are
not grounded upon creatures.
