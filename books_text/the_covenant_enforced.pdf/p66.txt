28 The Covenant Enforced

are mocked; they are poor persons; they are set at naught; they
hang their wings down; they do nothing but drop and pine away
in this world. These wretched souls, say some, are not well ad-
vised to take so many pains over things they do not understand,
for what profit do they have for all their travail? It seems, there-
fore, that they who seek to serve God are greatly beguiled, and
that the wicked bear sway everywhere.

But we must be thoroughly resolved on the other side, as it
is said in the prophet Isaiah, “Say to the righteous that it shall
be well with them, for they shall cat the fruit of their actions” (Is.
3:10). The prophet Isaiah would have us to fight against this
temptation. Even though the whole world should laugh the Godly
to scorn, and the wicked triumph over them, yet for all that the
faithful should not be astonished, but say to themselves, “No, no,
the righteous man shall not lose his labor; he shall not be deceived
of his expectation when he depends wholly upon the promises of
God.”

What we have to gather from this place, then, is that as often
as we read the promises in the Scripture where it is said, “Blessed
is the man who fears the Lord; those who walk in the obedience
of His Word shall be blessed; blessed is he who walks uprightly
and soundly with his neighbors, but especially those who re-
nounce the world because they have a better inheritance in heaven”
(Ps. 112:1; 119:1; Matt. 19:21); as often, I say, as we read these
things, we must be confirmed in our faith, and answer with a
good courage: “Amen, Lord; it is so. We do not dispute what You
have said. We embrace Your promises in this place, and trust
assuredly in them.” Thus you see how every man ought to enforce
himself to serve God, because He bears with us so gently, and
commands us not so precisely as He might, having all authority
over us, but applies Himself to our imperfection in order to win
us and to enjoy us.

And above all let us be mindful of this general promise: that
God calls us to Himself as His children; that He spares us and
bears with us, and does not enter into an extremity of rigor with
us; and that even though there are many faults in our works, He
