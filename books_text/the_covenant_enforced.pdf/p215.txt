Sermon 158: God’s Anger 177

things that He spoke by the mouth of Moses. Let us therefore
consider these things, and when we see so much stealing of vines,
of grain, and of such other things, let us understand that our Lord
punishes the ravenousness and extortion that is committed both
in merchandizing and in all other trades of occupation. And let
us not tarry until God punishes us, but let us walk uprightly, if
we wish for Him to be our Defender. For although all places are
full of thieves, yet our Lord will keep our goods in safety if we
walk in His fear and abstain from all manner of evil-doing. That
is what we must have our recourse to, and we must not think to
escape evil by evil-doing, but we must use the remedy that our
Lord sets forth in this text.

The same thing is said of our cattle and our children (vv.
31-32). Indeed, Moses continues to enlarge on the matter, to the
end that men should so much the better perceive what is the effect
of this speech that he employs, namely “this shall be forever
[continual].” It is as if he should say that the hand of God shall
always wax heavier and heavier. So much so that if a man is
driven from his [city] dwelling and thinks to remove to his posses-
sions and to his lands, God shall persecute him even there. And
if he thinks to cheer himself with his wife, she shall be taken from
him by force, And ifhe thinks to find some comfort in his children,
they shall be delivered into the hands of his enemies. And if he
thinks to have any recourse to his cattle, they shall be all stolen
or taken away by force. Moses therefore besets us here on all
sides, to the end that we should learn to resort unto God, since it
is in vain for us to labor to escape any other way. This is the
matter in effect that we have to bear in mind from this text.

Concerning Invasions

Now he adds, moreover, to increase the evil, that the fruits of
the earth and the labor of our hands shall be eaten by a people
that we have not known, and that we ourselves will be distracted
by the miseries that will thus light upon us (vv. 33-34). That is
to say, we shall be out of our minds, beholding the horrors before
our eyes, And being bewildered at it, we shall be forced to admit
