Sermon 151: Secret Sins 49

is done for him today or tomorrow. Sometimes he lies in his
cradle, and does not know who does him good and who does him
evil, because he has no discretion. He cannot requite the pleasure
done to him, and therefore every man leaves him alone. In the
same situation do widows stand, especially when they are poor
and are of no great reputation in the world. Every man shrinks
from them, and they are left as it were in a gob of spittle.

Now, because these things come to pass so commonly, God
purposefully takes such people into His protection, saying that if
any man wrests or hinders the right of the stranger, the widow,
or the fatherless, he shall be punished. Even if it is not accounted
of before the world, but rather such evil doers are well liked, yet
will He call them before Him and show that He cares for those
He has taken into His safekeeping. And so under one heading,
God meant to show briefly that if we tread upon such as have no
Teputation, and no means to defend themselves, and no one to
lean upon in this world, yet He reserves to Himself their avenging.
That is what we have to gather from this text.

Now, perverting or hindering the right (v. 19) is nothing other
than the unjust and causeless oppression of the feeble and weak,
and such as have no one to support and maintain them. The
manner or figure of speech here set down in Holy Scripture means
the same thing as our expression, “to dash a good case to the
ground,” and in general means to disappoint a man of his right.
Now therefore, if I rob a man, when I take to myself those things
that belong to him, when I strip him out of his substance, when
I thrust him out of all that he has, when I overmaster him and
take more upon myself than is properly mine, then I do hinder
his right. And so we see, as I said before, that in this text God
shows that He shall be the judge of all the outrages done to such
as have no means to avenge themselves, and are forsaken and
forgotten by men.

If we were well advised, surely we would be more afraid to
have God as our adversary, than all the world put together. And
indeed, we show that we give small credit to God’s Word, since
if a man is of a great family and has many friends, if a man is rich
