74 The Covenant Enforced

into favor, then will our works also be accepted. The way for us,
then, to serve God to His liking is that being justified by faith
(that is to say, having obtained forgiveness of our sins because
daily and all the time of our lives we have need of it) and also
having recourse to God's reconciliation with us of His own free
goodness by means of the death, suffering, and sacrifice of our
Lord Jesus Christ offered up to Him —having these things, the
way to serve God to His liking is to do so with this attitude of
faith. For then shall we do well, and then will He accept the
service we yield to Him. And apart from this we have no faith in
Jesus Christ.

But when we endeavor to serve God, even though there may
be things wrong, and our affections turn us now one way and now
another, yet God does not take our lives. Why? Because our sins
are not imputed to us. It is true that this curse will stand in force
in full rigor, but behold! Christ is our ransom, and pays for us,
delivering us from our cursedness, and making satisfaction to God
His Father. For we know that His death and suffering are ac-
cepted as the price and ransom of our salvation, that by such
means we should be reconciled to God.

You see then, that on the one hand it behooves us to feel our
own cursedness, that we may be afraid of God’s judgments, and
that on the other hand we must take courage, not doubting, but
seeing that since our Lord Jesus Christ answers for us, we shall
be received for His sake, and God will accept us together with
our works, even though they are not as good as they should be
but have some blots and blemishes in them, so that they deserve
to be condemned and utterly rejected. In brief, the faithful, being
justified by the grace of God, have along with it this benefit and
privilege: that God accepts their works and does not charge them
with this curse that they have deserved. That is how we ought to
put this text into practice.

Mourning and Rejoicing
But I have told you that on the one side we must mourn and
be afraid at the sight of our cursedness before God, and that on
