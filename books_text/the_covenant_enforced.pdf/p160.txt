122 The Covenant Enforced

our dullness, and He does so for the purpose of giving us less
excuse. If we are unthankful to Him and close our eyes against
So apparent a matter, we should be so much the more blamewor-
thy at the last day, and be certain to yield an account for our
willfulness in refusing to know the things that ought to be thor-
oughly known by us. Thus you see what we have to bear in mind.

St. Paul, speaking of the ignorance that prevailed in the old
world before the gospel was preached (Acts 14:16), says that all
people went astray like brute beasts. And why? Because that
doctrine was not yet taught which is the true light to show us the
way of saving health, as we shall see in the thirtieth chapter (Dt.
30:15). Notwithstanding, he adds that God nevertheless did not
leave Himself without witness (Acts 14:17). And His statement,
“He did not leave Himself without witness,” is as if He had
further said, “And how so? In that He has sent rain upon the
earth in due time and season. He has also sent fair weather. These
are God’s witnesses, which declare that just as He has created the
world, even so He upholds and preserves it. These are the wit-
nesses that speak with a loud and clear voice, saying that we
must look to God for all the nourishments that He gives us.”

This is why I said that we should mark well this saying, that
God shall open His treasure (Dt. 28:12). For although we see
what great riches God has set in this world, what various sorts of
beasts, what herbs, what trees, and what a number of all other
kinds of things, yet all these things should decay and die if God
did not send us daily from heaven the things that are requisite for
the preservation of this life. Could we abide three days without
the light of the Savior? If God did not give natural power to every
seed, what would happen? We should rapidly perish, And if noth-
ing else were involved except what I have been speaking about
concerning the earth, it would soon dry up.* These therefore are
the good treasures that God bestows upon us.

And when it is His will to show Himself a Father to us, He

4, Tn other words, God gives life to seeds. But even if we were to assume that
the seed has life in itself, it still could not grow untess God gives rain, for the earth
would dry up and the seed would die.
