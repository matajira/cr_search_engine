Annotated Table of Contents 287

5. Sermon 153: Promise, Obedience, Blessing ....... 78
Deuteronomy 28:1-2
Promise and Duty, 78
The Efficacy of Promise, 80
The Grace of Law, 84
Careful Obedience, 85
Authority of Office, 86
Comprehensive Obedience Enjoined, 88
God as Judge and Father, 90
Prayer, 94

6, Sermon 154: Blessing and Affliction. .........- 95
Deuteronomy 28:2-8
Misery and Sin, 96
Why the Righteous Suffer, 97
Blessing and Adversity, 99
The Purpose of Blessing, 101
The Perfection of the Promised Blessings, 102
The Example of David, 104
God, the Giver of All Blessing, 106
Specific Blessings of God, 107
Protection From Enemies, 109
Conclusion, 111
Prayer, 112

7. Sermon 155: Separation Unto Blessing ......... 113
Deuteronomy 28:9-14
Election, Promise, Preeminence, 115
The Heathen Shall Fear, 117
Repeated Assurances of Blessing, 119
Rain From Heaven, 120
The Gift of Labor, 124
The Promise of Dominion, 126
The Primacy of Obedience, 128
Prayer, 129
