Sermon 151: Secret Sins 45

with, and that it would have been better for them to have heeded
the curse here set down, For God has no need of the help of man.
Suppose all men dealt wrongly here, so that all things were con-
fused and put out of order, yet this saying will not fall to the
ground: The man who removes his neighbor’s boundary mark
shall be accursed, when God performs His office.

It is true that He wants men to use the sword, those into
whose hand He has placed it, and He has ordained that they do
so. And, if they are slack and slow in doing it, He will show them
that He appointed them not in vain to punish crimes and offenses.
But mortal men cannot prejudice Him. When an earthly judge
fails to discharge his duty, it does not follow that this weakens
God’s authority, or that He is bereft of fit means to execute His
office, or that He is idle. For He is not like worldly princes, who
trust to their officers and are well contented to act as blind persons
when things go amiss. “As for me,” they say, “I understand that
all things go well; and concerning my officers, my will is that they
should act faithfully to what I have commanded them.” A prince
thinks it is enough for him to have said the word, but in reality
God is looking over them, and controlling them. And even though
offenders and transgressors escape the hands of men, yet shall
they be punished at His hands in the end.

So then, let us not be worrying [like the thief] that we might
be seen, or that we might not do a quick job of our deceit; but
rather let us be worrying about this saying, “Cursed shall he be
who pulls up his neighbor's landmark.” Men may not see it at
all, but God sees it, and we can no more avoid His eyes than we
can His hand. So, let us beware of all fraud and false dealings,
assuring ourselves that our Lord watches over us, and that He
watches in such a way that He will not permit the poor to be
crushed or the simple to be outraged or devoured, without such
things being punished. Rather, He will show in the end how it is
with good reason that He claims to Himself the title of Judge of
the whole earth.

Neighborliness
Next we find, “Cursed be he who causes the blind to stray
