Sermon 150: Blessings and Curses 25

we have already declared, and as we shall see further in the end
of this chapter.

Seeing then that we are all sinners, even including the faithful,
so that when we endeavor to walk uprightly we still make many
false steps, what will become of us? It is certain that we should
all be deprived of the hope of salvation if we had nothing else to
lean upon than our own righteousness. But as J have told you,
the conditional promises here depend on the fact that God has
received us for His people and wants us to take Him as our
Father. This is grounded on nothing other than His mercy. So
then, we must be thoroughly persuaded that God will take pity
on us, even though we are wretched sinners and do not deserve
to be pitied. He will receive us as righteous and accept us, even
though we deserve to be rejected by Him. And although we can
hope for nothing but utter confusion, yet notwithstanding we are
assured of the inheritance of salvation because we are His chil-
dren. We must he thoroughly persuaded concerning this point.

So then, seeing that God has chosen us out and set us apart
for His service, we may not take license and indulge in all manner
of wickedness but rather we must endeavor to obey Him. For this
reason we must be quickened and pricked up by His promises to
serve Him. In this way we can see how the conditional promises
are not in vain with respect to us, when they are grounded on the
freely bestowed goodness of God, by which He receives us even
though we are not worthy to be received, not imputing our vices
to us. Although there are many stains and corruptions in us, yet
He hides them and does not call them to account,

And so we see now how God encouraged the people of Israel
to be of good comfort. For if He had begun in this manner with
them, saying, “Serve Me and you shall be well recompensed for
your labor,” if God should speak this simple word to us, alas!
what could we do? For even when we should try to serve Him,
we should be very far from the perfection that He commands.
Those who should run best would be but in the middle when they
should have come to the end. All of us would be discouraged
rather than have a good heart. But we must join both these things
