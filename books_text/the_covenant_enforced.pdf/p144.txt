106 The Covenant Enforced

fore, and our Lord afflicts us, let us receive the comforts that He
gives us to moderate our affliction, and then we shall continue to
walk on our way. And although we are called to endure many
things, and even though by reason of our frailty and feebleness
we are not able to overcome all temptations at the first blow, yet
notwithstanding, through the grace of God we may get the upper
hand. And when we are thus oppressed with our miseries, then
we shall esteern God’s grace in such a way that even in the midst
of darkness we shall perceive what light God gives to us, so that
we shall always feel Him to be our Father. For there is no doubt
but that He will bless us sufficiently, to the extent that is fitting
for our salvation. Thus you see what we have to do and to
practise, if we want rightly to understand what God has said,
that He shall bless His people if they hearken to His voice.

God, the Giver of All Blessing

Moreover, we are hereby warmed not to seek anything we
desire except from the hand of God. This is a very profitable
admonition. For we see how men err when they desire to be at
their ease. It is the natural inclination of all men to covet to be
this and that, and what course do they hold in the pursuit of it?
There can be no doubt about it: They turn their backs on God,
and kick against Him. He that intends to become rich will use
robbery and cruelty, deceit and wicked practices. He that desires
to attain to credit and authority will practise treason, indirect
wiles, and other sleights-of-hand. In brief, ambition rules such a
man altogether. Finally, he that would give vent to any other of
his lusts, cannot but provoke the anger of God. See at what point
we are! And in this way our faithlessness uncovers itself in every
way, as I have mentioned here before.

Therefore, we have need to be mindful of this lesson, which
is that if we wish to prosper, even in this present life, there is no
other means for it than to put ourselves, under the guidance of
God, who has all goods in His power to bestow on whomever He
pleases, We may also hope that He will not be niggardly in
distributing His gracious gifts unto us, at least if we hearken to
