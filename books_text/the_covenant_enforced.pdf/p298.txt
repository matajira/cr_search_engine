260 The Covenant Enforced

even they themsclves think so too.! But yet God always reaches
to them with His hand, and although He does not overcome their
griefs at the first, yet He holds them so that they do not fall. There
lies still some seed of God’s spirit hidden in them, which at length
manifests itself more fully, so they see clearly that God sustained
them in the midst of those extremities, and in the end it appears
to them as it were in a whole manner.

Let us mark it well then, that sometimes the children of God
are tossed and troubled with afflictions and have such grievous
and hard temptations that they think God has become their en-
emy, so that they dare not open their mouths to call upon Him,
but are utterly confounded. And even though He reminds them
of the promises of His grace, yet they cannot taste of them, but
sometimes even refuse them like unbelievers, as though they were
fully determined to shut themselves out of all hope of salvation.
When they are at this point, then our Lord works in them in such
a way as to cause them not to be overthrown completely with
temptation,

And that is what St. Paul means when he says, “We are
afflicted, but we perish not; we are oppressed, but yet no matter
how the world goes, we are not overcome, for we bear the mortify-
ing of Jesus Christ in our flesh” (2 Cor. 4:8-10). Whereas he says
that the faithful may indeed be pent up in afflictions, he says also
that even so they do not fall. For even though they stoop under
the burden, and groan as though they were crushed and broken,
yet all those heartbreaks do not cause any deadly fall. He adds
the reason, namely, that it is the mortifying of our Lord Jesus
Christ that they bear. That is, that God always separates us from
the unbelieving and from reprobates, and from the despisers of
His majesty, and that He prints in us the marks of His only Son.
Ti is true that in this case they seem as dead, but the same death
is holy, for God sanctifies it because they are members of our
Lord Jesus Christ, and thereby they attain to the resurrection.

1. That is, the righteous themselves cannot see how their lot differs from the
wicked.
