Sermon 152: The Curse and Justification 61

And so that we may get more benefit for ourselves from this
doctrine, let every one of us examine himself properly in his heart
when he comes to hear the Word of God. For it is proper for us
to be reproved inwardly, as St. Paul says in 1 Corinthians 14:25.
Every man, I say, must search his own thoughts and affections
to the very bottom.? Also, it is the peculiar office of God’s Word
to be a sharp sword, and to separate our thoughts from all our
affections and preferences, and to enter even into the marrow of
the bones, leaving nothing undiscovered (Heb. 4:12). Seeing this
is so, let us prevent God’s judgment, and not tarry until He curses
us and bans us for the innocent blood that cries out against us,
but let every one of us condemn himself as soon as he has of
fended, and let him be sorry in his heart and beseech God of His
infinite goodness and mercy to deliver us from the curse that is
denounced against all such as have so misbehaved themselves in
secret and have not been convicted thereof before men.

Taking Bribes

Next we read: “Cursed be he who takes a gift to strike down
the blood.of a guiltless soul” (v. 25). Thus Moses speaks word for
word. Nevertheless, the word sou! means life, And because the
blood is the proper seat of life, it is therefore said “the blood of a
guiltless soul.” Certainly, even ifa man is sinful, yet it is not lawful
to buy and sell his life. But as I have mentioned to you already,
God here has set before us those crimes that are most detestable,
so that we wake up, because we are not sufficiently moved when
He speaks of ordinary faults. Such things slip past us, and we think
to ourselves that it is not difficult to obtain forgiveness for them.

Now since there is so much laxity in men, therefore in this
place God has chosen some offenses that even by nature we ought

2 Calvin ought not to be taken in a perfectionistic sense here, as if he believed
that men could actually understand themselves exhaustively. Rather, in the context
of Calvin’s thought on sanctification, we should understand him to mean that we
should examine ourselves as carefully as we are able, For a discussion of this general
topic, see Ronald Wallace, Calvin’s Doctrine of the Christian Life (Tyler, TX: Geneva
Ministries, [1959] 1982).
