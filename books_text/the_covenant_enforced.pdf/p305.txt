Sermon 163: Exodus Undone 267

and troubled with disquiet. We see how the great tyrants, who
make all the world shake under them, experience this without
measure so that whereas they make others to drink a little glass
of fear and terror, they themselves must drink up a whole jar full.
They drink it down to the dregs, as the prophets say when they
speak of the greatest punishment God. sends. When those whom
God chastens* have drunk more than their fill in swallowing up
great stores of sorrows, then they who have lived at their ease and
pleasure must drink the lees, which is the bitterest of all and wilt
make them burst.

And indeed, we have an example of a heathen tyrant who
bore witness and declared that all his whole life was continual
torment. For being flattered by one who said to him, “O sir, how
happy is your state,” he replied, “Yes, and I shall make you to
understand it for yourself.” And thereupon he made him a feast,
and when he had set him at his table, he put a sword over his
head, hanging by one hair, to show to him what kind of state it
was that he had accounted so happy. The man, seeing himself in
this situation, said, “Let this kingly state be taken away from me,
for I had rather die a hundred times than be in such unquietness
and perplexity.” This I say was the confession of a heathen ty-
rant~ as if God had held him under torture — that it might be a
general lesson against all such as make war with God and trouble
the world with terror. They must, in spite of their rebellion, be
made afraid themselves, and find no rest. After many tossings and
much turmoil, hell must always await them, they must see their
graves open into which they are to fall, and they must behold the
great gulf ready to swallow them up—and in the meantime they
make no recourse or refuge to God, but flee from Him still,
whereas in fact He is the only person to whom they should have
fled for succor.

Peace With God

Now beyond what we have said already, there is another

4, Because He loves them.
