182 The Covenant Enforced

faith let us receive God's corrections wherewith He threatens us,
so that we may be preserved from that sight of which Moses here
speaks, so that our Lord does not strike us with such fear that
we cannot in any way think to receive any manifestation of His
goodness because of our sins. No, rather let us put this lesson of
obeying Him and of submitting ourselves to Him to good use, so
that we may escape the aforesaid dazzling of the eyes, and be not
so oppressed that we should become like people who are out of
their minds. Let us not come to such an extremity; neither let us
compel God to execute such threats against us.

Prayer

Now let us fall down before the majesty of our good God, with
acknowledgement of our sins, beseeching Him to vouchsafe to
make us so to understand them, that every one of us may be his
own judge and turn to Him before we are constrained thereto;
and that having willingly condemned ourselves and bewailed our
sins, we may seek to return to His obedience in such a way that
the same may serve to dedicate us wholly to Him. And that in the
meantime it may please Him that, just as He has sent us the
message of reconciliation in His gospel, He will also give us that
grace to obtain mercy from Him and forgiveness of all our sins
in the name of our Lord Jesus Christ; and that resting thereon,
we not fail to walk always in His fear; and that His bearing with
us through His fatherly goodness may not cause us to sleep in our
sins, and to flatter ourselves in them, but that every one of us may
quicken himself up, until we are quite clean rid of them. And that
since we are to pass through so much filth in this world, we may
be taught to amend our misdoings continually until we are thor-
oughly rid of them, for the full uniting of ourselves with Him,
making us partakers of His heavenly glory. That it may please
Him to grant this grace not only to us but also to all people and
nations of the earth; etc.
