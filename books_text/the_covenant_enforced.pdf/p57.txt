Sermon 149: Altars and Ensigns 19

crosses, and bright paintings, and they think that God is repre-
sented by them, but God has no liking for those things. We must
return to the Word, which is the means by which God opens
Himself to us, and He will be known by it. Let men therefore
content themselves simply therewith.

Now concerning the injunction, “Let those words be well
engraved,” hereby we are taught that God did not give His law
for a few people, but meant that it should be a common teaching
to all, both great and small, even to the most simple-minded, and
that all should be instructed by it. And if this was the case in the
time of the law, by greater reason it ought nowadays to be in force
among us who live in the New Covenant. For it is said that the
gospel is to be preached to all creatures (Mark 16:15). God will
not allow His teachings to be locked up, so that none but the
clergy should thrust their noses into it, but He wants all to be His
scholars, and the law to be written so that every man may read
it. Why? So that all men should receive instruction from it.

Let none therefore exempt themselves from the reading of it,
as we see many do, saying, “Oh, I am no clerk; I never went to
school; reading does not pertain to my occupation.” I grant read-
ily that it is not every man’s occupation to be a teacher, but who
may exempt himself from being a scholar in the word of God? A
man might as well renounce Christianity as say, “Oh, as for me,
I know neither A nor B [I am illiterate]; so how can I tell what
the law of God or any of Holy Scripture means?” Nevertheless,
the will of God as declared to us in His Word is written in letters
big enough, and although nowadays we have no heap of stones
set up for the law of God to be written and engraved upon, yet
notwithstanding our Lord meant to show in this symbolism that
when He delivered His Word it was so that we should be taught
and ordered by it, and that the teaching thereof should be com-
mon to all.

And truly, we have no less need to nourish our souls with the
word of God than we have to sustain our bodies with bread and
other daily food. Seeing that this is so, let every one of us labor
in the way, and let us be attentive to hearken to our God when
