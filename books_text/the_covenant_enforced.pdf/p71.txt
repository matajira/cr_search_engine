Sermon 150: Blessings and Curses 33

give ourselves unto Him, knowing that to this end He has taken
us out from the rest of the world, and will have us to be His own,
as it were His peculiar inheritance. All men, of course, are bound
to serve Him; but yet notwithstanding, when He calls us to Him-
self, and shows Himself our Father, does He not bind us to
Himself with a double bond? Yes, surely. Are we not then bereft
of all sense and reason, and wholly bewitched, if we are not moved
to yield ourselves over to His will, so that He may guide us and
bear rule over all our life? Let us, then, weigh well these words:
“Today you are made the people of the Lord your God, and
therefore keep His commandments.”

How are we made the people of God except by being His
Church, and by having the use of His sacraments, and that is all
the same as if He appeared among us? For we may not expect
that God should come down from heaven in His own person, or
send His angels to us. Rather, the true mark whereby He will be
known to be present among us is the preaching of His Word
purely unto us, for there can be no doubt but that then He bears
rule in our midst. So then, let this thing profit us, that we know
that our Lord receives us to Himself and will have us to be of His
own household. Seeing it so, let us take pains to obey Him in all
our life, and to keep His commandments. Let us not wander like
brute beasts as the wretched unbelievers do, because they never
knew what it was to be of the house of God.

Curse on Secret Idolatry

But now let us come to the rehearsal Moses makes of the
curses. First of all he says, “Cursed is he who makes any idol or
any molten image, or any carved image; all this is abominable to
God. And cursed is he who puts it in any secret place. And all the
people shail say, Amen.” Let us note that Moses does not specify
in this place all the curses, each one by itself, but rather sets down
certain examples to show that all those who swerve aside from the
law of God seek nothing other than to run willfully into utter ruin
and destruction.* The effect, therefore, of all this is that if we

4, It is in Deuteronomy 28 that we find the general discussion of blessings and
