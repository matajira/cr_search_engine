32 The Covenant Enforced

2:7, “For the lips of a priest should keep knowledge, and people
should seck law from his mouth; for he is the messenger of the
Lor of hosts.”? We see then that God in all times appointed
some in the office of teaching the people, and of bearing abroad
His Word.

And so, we today also need such an order, and we know
what St. Paul says on the subject, in Romans 12:6, 1 Corinthians
12:28, and Ephesians 4:11.3 And it is proved to us also throughout
ali the Holy Scripture, that God will have certain laws established
and certain men appointed to bear abroad His Word, to be
teachers in His Church, and to instruct the people in His name.
We gather from this that when God has appointed such an order,
all those who cannot allow themselves to be taught by this com-
mon order with the whole body of the Church, though they may
call themselves Christians, yet they are no more holy than horses,
for they think that it is enough for them to grant in a word that
the gospel is the Word of God, But on the contrary, we see here
that if we want to be of the body of the Church, and have God
take us for His children, we must hear the Word of God as it is
ministered to us by the ministry of men. But because this point
has been dealt with at large previously, I have only at this time
glanced at it by the way.

The Privilege of Obedience

There is also this point: “Today you are made the people of
God, if you keep His commandments” (wv. 9, 10). Therefore, just
as our Lord receives us into His house, so must we also wholly

2. It is useful to note that the Levites were not only located in Levitical cities,
but also in every Israelite town (Dt. 19:12; 14:27; Jud. 17, 19). These Levites
conducted worship and instruction every sabbath (Lev. 23:3). This was the origin
of what came to be called the synagogue.

3. These verses all teach that there are special offices in the New Testament
Church. Romans 19:6: “Having then gifts differing according to the grace that is
given tous... .” 1 Corinthians 12:28: “And God has appointed these in the church:
first apostles, second prophets, third teachers. . . .” Ephesians 4:11: “And He Him-
self gave some to be apostles, some prophets, some evangelists, and some pastors and
teachers.”
