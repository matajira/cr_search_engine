A Leiter to Paul J. Hill 5

fearful is killing, even as a member of God’s holy army, that
God mandated a special payment. While we no longer are
required to pay money to a priest, the implication is clear:
killing is a very serious matter.

A man can defend his household against an unauthorized
criminal invader (Ex. 22:2-3). He is the head of his houschold:
a covenantal office. This is not self-defense as such; it is the de-
fense of a legitimate sphere of authority, the home, by one
charged by God through the civil government to take defensive
action. But this right is never said to be universal in the Bible;
it is limited to the protection of one’s family.

A man can participate in the execution of a criminal convict-
ed of a capital crime. “At the mouth of two witnesses, or three
witnesses, shall he that is worthy of death be put to death; but
at the mouth of one witness he shall not be put to death. The
hands of the witnesses shall be first upon him to put him to
death, and afterward the hands of all the people. So thou shalt
put the evil away from among you” (Deut. 17:6-7).

In Old Covenant Israel, there was an office called the blood
avenger, which was the same as the kinsman-redeemer. This
was the man who was nearest of kin. When a man accidentally
killed another, he had to fiee to a designated city of refuge. If
the blood avenger caught the suspect en route, or outside the
walls of that city, he was authorized by civil law to execute the
suspect (Num. 35). This office no longer exists because cities of
refuge were an aspect only of Mosaic Israel.

When a corporate crime was so great that God's negative
sanctions threatened the entire nation, the state could authorize
corporate execulions. The example here is the national sin of
the golden calf. The Levites’ lawful slaying of the 3,000 men
after the golden calf incident removed the corporate threat (Ex.
$2:28). But they had specifically been called into action by Mo-
ses, the God-inspired head of the civil government. Moses
deputized them prior to their judicial action.
