34 LONE GUNNERS FOR JESUS

ordained officer who defends the household: anyone who law-
fully dwells within the home. The jurisdiction is explicit: prop-
erty (space) which the individual lawfully occupies.

If a man or woman calls for help to defend his home or
other space which by law is under his or her jurisdiction, an
outsider thereby becomes a delegated agent of an ordained,
God-authorized officer: the person who lawfully occupies that
space. The outsider could legitimately risk confronting the
invader with force if he could see that the authorized defender
has been illegally restrained, such as bound and gagged. A jury
would have the post-conflict responsibility of sorting out the
legal details, such as the legitimacy of the outside defender’s
assumption of the lawful defender’s implicit cail for help.

One thing is sure: the outsider who guns down the supposed
invader does his moral cause and judicial case no good when
he then leaves the scene of the event. Your October 7 letter
says that the state’s witnesses said that you did not run away;
you merely walked away. You also left behind a woman bleed-
ing from your attack that killed her husband, It is a felony
when a driver involved in an auto accident immediately drives
away from the scene of the accident where someone has been
injured. This is called hit and run. What is it when a lone gun-
ner leaves the scene of his crime? Gun and run?

If the supposed invader was in fact on his own property —
within the boundaries of a lawful self-defense - then the jury
should have no problem deciding who the real invader was,
and who possessed the God-ordained right of self-defense.
Your victim was a God-ordained officer in charge of his own
property. He had lawful jurisdiction within that boundary, as
described by Exodus 22:2. There was no call for help except an
implicit one, and that implicit call was issued against an unlaw-
ful invader. According the conditions specified by Exodus 22:2,
you were the unlawful invader. Also, you were not a mere thief;
you were a murderer
