24 LONE CUNNERS FOR JESUS

legitimize the theology of the lone gunner for Jesus. I scck to
ward off the theology of the anarchist revolutionary movement.

There are 50 million abortions conducted cach year. Am I
responsible to pick up a gun and shoot any abortionist any-
where on earth? Has God ordained me to cleanse the earth of
abortionists? Your theology sets no boundaries on the use of
violence. National borders have no judicial relevance for those
seeking to cleanse by force the world of abortion. Evil is evil,
wherever it is practiced. If the lone gunner for Jesus has been
given equality with the civil magistrate in protecting the lives of
the unborn, this license cannot end at a national border. The
late Ayatollah Khomeini issued a death warrant with a $5 mil-
lion reward for the man who kills Salmon Rushdie. He did not
place any geographical restrictions on this death warrant, nor
did he place temporal boundaries. ‘The death warrant is pre-
sumably still in force, and Rushdie is still at risk.

Where are the judicial boundaries of responsibility? Where
are the judicial boundaries of violence? By removing the death
penalty from the state, your theology transfers it to the lone
gunner. What restricts him? His common sense? That is what
your essay appealed to: “Common sense tells the individual that
he should protect his neighbor from unjust harm” (p. 1). But
how clearly does common sense speak in history, and how
carefully do lone gunners with a sense of mission listen to it?

Your theology offers no judicial boundaries. It offers no
boundarics on the sense of guilt m the hearts of men. There
are unborn babies dying today, all over the world. Where does
my responsibility end?

The unbounded perfectionism of your theology leads to
intense guilt and the deviant behavior such guilt can produce.
I am not guilty for my refusal to kill abortionists. I have not
been authorized by God to kill abortionists. Abortionists are not
under my God-given authority. They have not invaded an area
for which I am responsible to the point of being authorized to
kill them. But J am guilty if 1 do nothing politically to reverse
