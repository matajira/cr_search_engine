A Letter to Paul J. Hill 21

Your perfectionist, guilt-ridden tactic of self-ordained judge-
and-jury execution may make perverse sense to fanatics today,
before the technology of mass murder is on the shelves, but the
tactic clearly becomes self-defeating the day the new technology
arrives. This should tell you that your tactic is wrong today.
You obviously did not think through the implications of your
recommended plan of action. I hope that those who might
otherwise imitate you do think it through, before it is too late
for them and also for the hundreds of thousands of innocent
infants whose lives will be lost because of the political reaction
your tactic will produce in normal human beings. These people
vote. This is what your murderous perfectionism ignores.

Voices of Virtue

But there will still be a few people like yourself: self-ap-
pointed voices of virtue on Robespierre’s model, who will not
accept this political frame of reference. They will move to the
next stage. Your position is really quite mild compared to what
may come. Your position is that you, as a self-ordained and
sclf-anointed man, have the lawful authority before God to gun
down one lone practitioner of abortions and his escort, and put
a couple of pellets in his wife while you are at it. That is a mild-
mannered position compared to what may come. The real
hard-core practitioners of revolutionary virtue are going to
understand the logic of your position. You really were not
consistent. People say that you finally became consistent with
your position, but they are incorrect. You were not consistent
with your position because the real position that is consistent
with what you are saying is to put bullets into Supreme Court
Justices. The real position you are advocating is to gun down
every legislator who will not vote against legalized abortions.

Your position is really revolutionary. Open revolution, that
is what you are calling for. Your “defensive action” theology is
aimed not only at some local physician who is going to kill two
or three babies or ten babies today and more tomorrow. The
