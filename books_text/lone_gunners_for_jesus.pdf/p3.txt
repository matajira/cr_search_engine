4A LETTER TO PAUL J. HILL
Sept. 29, 1994

Paul J. Hill

Escambia County Corrections Dept.
P. O. Box 17789

Pensacola, FL 32522

Dear Mr. Hill:

Sometime in the months following the murder of the abor-
tionist in Florida, Dr. Gunn, you sent me two position papers.
One was called, Was the Killing of Dr. Gunn Just? You added this
parenthesis: “Rough draft, numerous revisions still being
made.” Obviously, you have other things on your mind these
days besides continuing the revisions of your rough draft. 1 am
responding to this paper belatedly because you seem to have
taken your own suggestions seriously enough to shoot an abor-
uonist, kill his escort, and wound the escort’s wife. That, at
least, is what you are accused of.

The subtitle on your paper is called, A Call te Defensive Ac-
tion. You also sent another paper titled, “Defensive Action” Is a
Pro-Life Organization Proclaiming the Justice of Using All Action
Necessary to Protect Innocent Life.

I did not respond to your letter or to your papers. I cannot
find your letter in my files, but I did save your two papers. I
