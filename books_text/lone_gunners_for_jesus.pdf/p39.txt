A Second Letter to Paul J. Hill 37

you conclude, there must be what I have called an implicit cry
for help from the unborn infant.

You argue that anyone has the God-given authority to inter-
vene to stop the act by killing the hired assassin. To which 1
ask: Says who? You argue that the equal ultimacy of the one
and the many authorizes any person to intervene. This is an
abstract philosophical argument in search of specific biblical
support. Your one suggested biblical example was Moses’ slay-
ing of the Egyptian taskmaster. First, Moses caught the tyrant
in the act. You did not. Second, Moses was an adopted member
of the household of Pharaoh, the supreme civil ruler. You held
no such position of authority. Third, Moses did not imagine
that his own people would immediately reject his act of interpo-
sition. You did know; you had already been excommunicated.
So, you did not act in terms of civil authority, which you did
not possess, or representative authority from the church, from
which you had been judicially excluded. You are not Moses.

Your defense then turned to history. And what history you
have turned to! You cited as a legitimizing precedent the
bloody work of John Brown (p. 13). You did not specify wheth-
er you were talking about his raid on Harper’s Ferry in 1859,
which more than any other event brought on the Civil War, or
his murder of four unarmed men at Pottawatomie Creek, Kan-
sas, in 1856. In either case, it astounds me that you would cite
Brown in your defense. Otto Scott’s book, The Secret Six: John
Brown and the Abolitionist Movement (Times Books, 1979), has
stated the case against Brown in no uncertain terms. Since Scott
was an employee of Rushdoony’s Chalcedon Foundation from
1983 until two weeks ago, it should have occurred to you that
Brown is not beloved in Christian Reconstructionist circles.

Then you cited the peasant revolts of Western Europe. That
you should cite those murderous hordes as your predecessors
reinforces my original contention: you are a revolutionary. It
was to their legacy of mass destruction that Karl Marx’s col-
Icague Friedrich Engels also appealed in The Peasant War in
