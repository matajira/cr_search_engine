22 LONE GUNNERS FOR JESUS

problem for your theology is the U.S. Supreme Court, which
has authorized the killing of a million and a half a year. If the
government is the problem -~ and it surely is the problem -
then what is the logic of your position? If you can save a life by
gunning down a local abortionist, how many lives can you save
if you gun down authorities who have legalized abortion?

You gunned down an abortionist’s private escort. What if
that bodyguard had been a policeman? What is the difference,
given your doctrine? You teach: (1) the individual’s right to
gun down local abortionists; (2) the individual's right to gun
down their bodyguards; and (3) the equal ultimacy of the indi-
vidual and the civil magistrate in saving lives. So, kill cops.

We saw such a plan in action in Columbia. The Medelin
drug cartel would gun down judges. They would gun down
policemen that opposed them. They understood that their
problem was at the top, not at the bottom, and so they offered
rewards: $1500 to killa policeman. They offered more to shoot
a judge.

This is where your position is heading. Someone will make
the jump which you have been unwilling to take. You shoot
down a local abortionist. The policeman arrests a local drug
seller. Will cither action stop the action? The people at the top
are the problem, some follower of yours will conclude. He will
sec the logic of your position: “If I have the right to gun down
a local abortionist, I have the right to gun down his accomplice.
If can lawfully fire a shotgun and wound the accomplice’s wife
because she, too, is basically an accomplice, then I have a right
to gun down the ministers who excommunicated me for public-
ly advising violence against abortionists. If I have the right to
gun down a practitioner, don’t I have a right to gun down the
judge who has authorized the practice? Don’t I have the right
to gun down the politician? Don’t I have the right to gun down
the voter who has voted for the politician, who in fact is in
favor of abortion? Don’t I have the right to gun down every-
