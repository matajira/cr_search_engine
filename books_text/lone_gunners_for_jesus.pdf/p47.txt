anarchism, 11

abortifacient, 17-21, 38-39,

41
abortion
accomplices, 22
back alleys, 8
Bible, 15-16
God’s focus, 16-17
illegal, 7-8
legalized, 17
local, 17
Main Street, 8
number, 18, 23
numbers, 7
politics, 29
protest, 19
re—criminalization, 25
resistance, to, 25-26
universal, 7, 15
Adam, 11
alleys, 8
anarchism, 13
army, 4
assassins, 36, 37, 41, 42
atonement, 9
autonomy, 7, 23

Babylon, 10
back alleys, 8

INDEX

Bahnsen, Greg, 2
baptism, 11

blood avenger, 5
bloodguilt, 9, 16, 30
bodyguard, 27
boundaries, 24, 27
Bray, Michael, 8, 10
Brown, John, 37

Calvin, John, 3
Canaanites, 4
church, 14
churches, 28
cities of refuge, 5
civil magistrate, 12, 17
Colombia, 22
common sense, 24
community
approval, 17
guilty, 9
judgment, 9
representative, 13
spokesman, 13
standards, 14-17
conscience, 20
covenant
hierarchy, 12
institutions, 11
office, 12
