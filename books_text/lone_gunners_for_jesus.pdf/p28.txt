26 LONE GUNNERS FOR JESUS

public is allowing the government to do the wrong thing in the
name of the public; and (3) the public’s mind can be changed
if non-violent tactics reveal that those who enforce the law have
to do terrible things against righteous people who are standing
up for principle.

The difference between non-violent resistance and violent
resistance is very great. Non-violent resistance says that we must
take a public stand against a public evil and suffer the conse-
quences, Non-violent resistance says that those magistrates who
represent the people as a whole are going to do evil things in
public if what they have been authorized to do is essentially
evil. Non-violent resistance against legalized abortion is aimed
at calling the public to its collective senses. But if the public no
longer has collective senses, and if the public in its heart is
murderous, then non-violent resistance is not going to work
directly. Non-violent resistance is going to call down the wrath
of God on the society.

What you forgot, and what violent resisters want to forget, is
that there is a God in this process, and He does act in history to
bring His sanctions. You were not authorized by God to repre-
sent the public. You were authorized by God to do non-violent
things and suffer the consequences personally. You were autho-
rized by God to stand in the gap and get your head beaten in,
maybe on videotape, to be broadcast on the six o’clock news.
You were authorized to get the public infuriated against the
agents who bashed your head in. ‘That is what you were enti-
tled to do. But you were not entitled to gun somebody down.
God allows the sword to be used only by someone who is or-
dained to do it. “Vengeance is mine, saith the Lord.” So says
Romans 12. Romans 1% says that the state is authorized, as a
minister of God (v. 4), to act as God’s lawful agent of ven-
geance. If the civil magistrate brings vengeance against evil-
doers, then God does not have to, and He will not bring ven-
geance in history against the society as a whole for authorizing
a civil magistrate to do evil.
