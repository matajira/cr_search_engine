38 LONE GUNNERS FOR JESUS

Germany (1850). For decades, Marx’s name was on this book,
not Engels’ name. The co-founders of Communism understood
quite well who their predecessors were and what they did. I
think you do, too.

You refused to respond to my technological argument,
namely, that with the introduction of chemical abortifacients,
the war on the unborn moves from the public clinic to the
medicine cabinet in the home. I asked you: How will those
committed to your tactics defend the unborn child when Mama
swallows something that will kill it? Will lone gunners invade
homes, shooting down mothers, thereby also killing the infants?
You prudently remained silent. 1 can hardly blame you. There
is no possible answer that conforms to your call to defend the
innocent through private execution.

You have thrown away your life in a futile attack on a technically
obsolescent medical procedure. You were fighting the old war, not
the next one. You have played the fool’s role in a deadly dra-
ma, but unlike fools in most dramas, you were armed.

This is what bad theology frequently does to those who
adopt it. Jt makes fools out of them.

What I am saying is that you autonomously gunned down
two men without cause. I am also saying that you symbolically
attacked a representative of a medical specialty that is about to
become obsolescent. I will add this: your act, if imitated, will
speed up the introduction of chemical abortifacients. This will
bring murder down to mass-discount price levels. As the price
of this bloody service falls, more of it will be demanded. As
murder gets easier and cheaper, it will become more common.

If the American abortifacient industry erects a statue honor-
ing its most distinguished marketer, you should be the subject.

You lose either way, i.e., the victimized infants lose. If your
act creates a successful political reaction against the pro-life
movement, the resulting extension of time for legalized abor-
tion will destroy untold hundreds of thousands of innocent
babies. On the other hand, if your murderous act is imitated,
