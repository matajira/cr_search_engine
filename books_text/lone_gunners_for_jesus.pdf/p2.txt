copyright, Gary North, 1994

Library of Congress Cataloging-in-Publication Data

North, Gary.
Lone gunners for Jesus : letters to Paul J. Hill / Gary North.
p. cm.
ISBN 0-930464-73-7 : $3.95
1. Abortion - Religious aspects ~ Reformed Church. 2. Hill,
Paul J. 3. Pro-life movement - Uniled States. 4. Law (Theol-
ogy) - Biblical teaching. 5. Nonviolence - Religious aspects —
Reformed Church. 6. Government, Resistance to - Religious
aspects - Reformed Church. 8. Dominion theology. 9. Re-
formed Church - Doctrines. I. Title.
HQ767.35.N67 1994
261.8°36667 - dc20 94-37301
CIP

Published by

Institute for Christian Economics
P.O. Box 8000, Tyler, Texas 75711
