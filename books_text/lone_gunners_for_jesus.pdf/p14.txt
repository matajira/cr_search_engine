12 LONE GUNNERS FOR JESUS

do us part.” In the Church, we are baptized, which symbolizes
going through the death and resurrection with Jesus Christ.
Death is always a possibility for covenant-breaking. This death
is announced through excommunication, In the state, we take
a vow, or at least implicitly we do, to uphold the law. We are
brought under the sanctions of God if we unlawfully violate an
oath of subordination. The point is: there must be, in every
covenant, a representative. This representative is ordained to
his office.

The father represents his wife and his children before God
because he holds a high covenanta! office to which he has been
ordained. The minister represents the congregation because he
has been ordained. The civil magistrate represents the cove-
nanted nation because he has been elected or lawfully appoint-
ed by those who have been elected. There is no lawful covenan-
tal office without ordination.

What you are talking about in your essay is the equal ultima-
cy of both the individual and the ordained civil magistrate in
fighting crime. This position is utter nonsense biblically. ‘There
cannot be equal ultimacy of those two because one of them has
been ordained and one of them has not. The officer has a
covenantal responsibility before God that is unique, but the
individual docs not. The officer is oath-bound to enforce the
law, while the individual is oath-bound to obey it. One of them
is at the top of the hierarchy and is invested by God with the
power to exercise the sword, while the other is not.

Your theory of civil law docs not reveal any trace of judicial
subordination. Your system of interpretation of Rushdoony’s
passage is a violation of every principle of biblical covenantal
law because there is no hierarchy in your system. Every cove-
nant has te have a hierarchy. Every legal order has to have a
judge. Every ecclesiastical order has to have a minister. But you
obviously do not believe this. When you were excommunicated,
you set up your own home church in which you were the self-
ordained authority, by which you said you had the right to
