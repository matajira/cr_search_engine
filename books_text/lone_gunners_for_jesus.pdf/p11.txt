A Letter to Paul J. Hill 9

Deuteronomy 21:1-9 is a very important passage. Bray was
correct to cite it. So were you. But you have not understood it.
That passage deals with a dead body found in a field. The
victim has been murdered, but no one knows who committed
it. The elders of the city closest Lo the field come out to partici-
pate in a sacrificial offering in order to remove the bloodguilt
from the city. They kill a heifer; the Levites then sacrifice it,
‘The text reads: “And all the elders of that city, that are next
unto the slain man, shall wash their hands over the heifer that
is beheaded in the valley: And they shall answer and say, Our
hands have not shed this blood, neither have our eyes seen it.
Be merciful, O Lorn, unto thy people Isracl, whom thou hast
redeemed, and lay not innocent blood unto thy people of Is-
rael’s charge. And the blood shall be forgiven them. So shalt
thou put away the guilt of innocent blood from among you,
when thou shalt do that which is right in the sight of the Lorp”
(Deut. 21:6-9).

Michael Bray’s citation of this passage was correct. There was
bloodguilt under the Mosaic Covenant, and the way lo escape
God's corporate negative sanctions was for both the priests and
the civil magistrates to acknowledge before God that they did
not know who had slain this victim, that they washed their
hands because of it, and they slew an animal to atone for it.
They were atoning not only for the sin of murder but also for
their own ignorance. They were making certain that bloodguilt
did not extend 10 the society. They were announcing publicly
that they did not approve of this murder. And because they did
not approve of it, and they sacrificed something valuable to
prove they did not approve of it, God brought them out from
under corporate judgment.

We must think judicially about this passage. The intent of
the passage was to show that God does not hold a community
or a society guilty for the acts of an individual that are immoral,
if the community takes appropriate actions to suppress the
action. ‘hat is, if the community passes laws against the prac-
