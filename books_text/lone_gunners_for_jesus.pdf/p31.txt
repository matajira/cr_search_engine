A Lelter to Paul J. Hill 29

anointed home church, family-church ministers who are serving
the Lord’s Supper to their families.

This should not be regarded as a letter favoring abortion. It
should surely not be regarded as a letter favoring legalized
abortion. It should not be regarded by the public as being
against non-violent resistance against abortion and legalized
abortion. It is a letter which must be understood as a call to
return to biblical law, a biblical doctrine of the covenant, and a
concept of lawful order. It is a call to return to the doctrine of
hierarchy and sanctions. I pray that this call will be taken seri-
ously, for if it is not taken seriously, we are going to enter into
a period in which vengeance is appropriated by individuals who
are in rebellion against man and God, who are going to spread
lawlessness on an individual basis into a society that is already
committed to a rebellion against biblical law and against Chris-
tian principles. Godly people will be caught in the crossfire.
The crossfire will be between the secular humanists who pay no
attention to God’s law, and therefore legalize abortion, and the
self-anointed, scH-ordained bringers of private justice who pay
no attention to civil law or church law and who, on their own
autonomous authority, are going to gun down the beneficiaries
of the secular humanist order.

We are caught between the collectivized corporate evil of
humanist civil law and the anarchistic autonomous evil of self-
ordained revolutionaries. We are caught, in other words, in a
crossfire between the one and the many. That is not where J
want to be or where I want the church to be. The church had
better begin taking steps to remove from its membership rolls
all those who call for violence against individuals m the name of
the community, when in fact in our present situation, the com-
munity stands against God and against God’s law. It does no
good to take up the sword privately against representatives of
an-evil community or against the bencficiaries of an evil com-
munity. God’s judgment threatens the entire evil community.
