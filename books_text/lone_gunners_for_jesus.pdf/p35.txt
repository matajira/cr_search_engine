A Second Letter to Paul J. Hill 33

You cited provisions regarding the moral law. Specifically,
you cited Chapter XIX of the Confession regarding God’s cove-
nant of works with Adam: “God gave to Adam a law, a covenant
of works, by which he bound him and all his posterity, to per-
sonal, entire, exact, and perpetual obedience. .. .” You then
cited the Shorter Catechism on the sixth commandment: “The
sixth commandmentrequireth all lawful endeavours to preserve
our own life, and the life of others” (A. 68). You then went on
to a discussion of the rights of policemen to defend people vs.
the rights of individuals to defend them, which you claim are
equally ultimate rights.

Your October 7 letter briefly mentions the discussion of the
sixth commandment found in the Larger Catechism. The jud-
icial question is this: What does the Larger Catechism define as
tawful? The Shorter Catechism is silent on this point, as befits a
shorter catechism. We must go to the Larger Catechism for
further enlightenment. Question 136 asks: What are the sins
forbidden in the sixth commandment? The answer is this: “The sins
forbidden in the sixth commandment are, all taking away the
life of ourselves, or of others, except in the case of publick
justice, lawful war, or necessary defence; .. .”

Aitendees of the Westminster Assembly were asked by Parlia-
ment in 1646 to add Bible verses in support of these docu-
ments, and in 1647, the Assembly did so. In the note on publich
justice, the Assembly cited Numbers 35:31, which forbids any
restitution for murder other than the public execution of the
murderer (This law surely applies to the judge who sentences
you if a jury convicts you of murder) To explain the phrase
necessary defence, the Assembly cited Exodus 22:2-3, which au-
thorizes a man to kill a thief who has invaded his home.

The issue here is self-defense against an unauthorized invad-
er of a specific boundary: a man’s home. As the Ged-authorized
defender of his home, the resident is allowed to repel the in-
vader by force, including deadly force. The judicial issue here
is two-fold: office and jurisdiction. The law identifies the God-
