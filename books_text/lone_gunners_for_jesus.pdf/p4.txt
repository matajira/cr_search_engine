2 LONE GUNNERS FOR JESUS

should have responded. Perhaps I might have persuaded you
that you were headed in a terrible direction. In ail likelihood,
though, you would not have taken me seriously. I say this
because you were excommunicated by your local church, and
you did not take that seriously. Your church asked only that
you cease speaking in public — such as on the “Donahue” show
~ in defense of the right of anti-abortionists to kill abortionists.
So, there is no persuasive reason for me to believe that you
would have taken seriously anything that I might have written.
I do not expect you to take this letter seriously. On the assump-
tion, however, that men can repent before they are cast into
hell, which is where you are clearly headed, I am responding
here.

Judicial Theology

I say that you are headed for hell because I speak judicially.
You wrote to me, presumably because I am associated with the
Christian Reconstruction movement. You are well aware that
we are noted for our judicial theology. You attended classes
taught by Dr. Greg Bahnsen in the late 1970’s at Reformed
Theological Seminary in Jackson, Mississippi. In one of your
papers, you cite a book by R.J. Rushdoony. We do think judi-
cially and speak judicially. Ask a judicial theologian his opinion,
and you should expect a judicial answer.

The New Testament is clear: when a man is excommunicat-
ed from his church, he is to be regarded by Christians as a
heathen. We are told specifically by John that we are not to
wish such a person Godspeed. “Whosoever transgresseth, and
abideth not in the doctrine of Christ, hath not God. He that
abideth in the doctrine of Christ, he hath both the Father and
the Son. If there come any unto you, and bring not this doc-
trine, receive him not into your house, neither bid him God
speed: For he that biddeth him God speed is partaker of his
evil deeds” (II John 1:9-11).
