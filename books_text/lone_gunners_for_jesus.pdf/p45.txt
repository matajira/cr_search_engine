A Second Letter to Paul J. Hill 43

to defend himself based on his defense of the unborn innocent,
this would have been prohibited. The judge ruled as inadmissi-
ble any defense based on the defense of unborn children. This
means that the judge in effect had passed sentence on him; the
jury was to serve as little more than a rubber stamp. In fact, an
American jury has the sovereign right to declare a man inno-
cent for any reason it pleases. It can judge both the law and the
facts of the case because it possesses final sovereignty when it
declares a person innocent — the law against double jeopardy.
But judges seek to thwart this absolute judicial sovereignty by
prohibiting jurors from hearing certain kinds of evidence and
certain lines of argument. Judges do not inform jurors of the
sovereign authority of the American jury to declare anything it
wants behind closed doors.

Having remained mute in his two trials, Federal and local,
Hill was declared guilty by both juries. This jury deliberated for
only 20 minutes. On November 3, the jury was ready to consid-
er sentencing, although the judge has the final say — another
way to undermine juries. At this point, Hill at last spoke up: to
warn the jury in the name of God. “In an effort to suppress this
truth, you may mix my blood with the blood of the unborn and
those who have fought to defend the oppressed. However,
truth and righteousness will prevail. May God help you to
protect the unborn as you would want to be protected.”

Here was an excommunicated man ~ judicially the equiva-
lent of a heathen — calling his jurors before the bar of God's
justice. Here was a murderer who had gunned down two men
and injured a woman, presenting himself as a God’s prophetic
spokesman. Here was a man who had left the scene of his
crime, telling the jury that it had a responsibility to defend the
oppressed. Here was a self-appointed bringer of shotgun ven-
geance warning the jury against . . . what?

The jury took four hours to come to a conclusion: death in
the electric chair. Life for life: this is God’s mandate. The jury
did its duty. The church had already done its duty.
