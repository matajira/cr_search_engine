A Letter to Paul J. Hill 19

million and a half a year. We will not know how many deaths
will be administered. It may be twice as many; we will have no
sure way of knowing. What we know is that abortion will be so
unbelievably easy that the numbers will skyrocket. There is
going to be no way to stop it by picketing. Besides, we will not
have anyone specific to picket.

What are pro-lifers going to do? Organize picketing against
Wal-Mart, which sells 10,000 products? Are we likely to get the
buyers of 9,999 of those other products not to walk in the door,
just because the store sells one product which we don’t like? Do
we think that such picketing is going to stop some murderous
mother or anyone else? Picketing will stop almost nobody. We
will not be able to target a particular practitioner any more.
There will not be a visible representative any more. There will
only be the society that wants the abortions and millions of
women who want abortions.

There are tens of millions of men and women who will not
vote to ban the sale of such a product. ‘This is our problem. It
is a political problem resting on a specific moral foundation:
humanism. The problem is judicial. It is corporate. And finally,
when the physicians are driven out of business by mass pro-
duced abortifacients, pro-lifers are going to face a new reality.
They are going to realize that it is not abortion as such that is
the covenantal problem. It is the problem of a society that has
legalized abortion.

An anti-abortion protester today may save a few lives on a
particular day. There may be a protest that saves a couple lives,
but there is still going to be a million and a half that are not
saved this year. There will be a million and a half who are
going to die, whether or not you protest. The protests are
symbolic. The protests are focusing on the evil of the act. But if
they are to be life-saving, the protests must be used to call the
whole society to its moral senses. he primary problem is the
whole seciety, not the abortionist around the corner. This is a
symbolic war which must be fought politically. When mass-
