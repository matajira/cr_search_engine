74 DOMINION AND CONMON GRACE

evil; he deviseth wicked devices to destroy the
poor with lying words, even when the needy
speaketh right. But the liberal deviseth liberal
things: and by liberal things shall he stand.

To repeat, “The vile person shall be no more
called liberal, nor the churl said to be bountiful” (v.
5). Churls persist in their churlishness; liberal men
continue to be gracious. It does not say that all
churls will be converted, but it also does not say that
the liberals shall be destroyed. The two exist to-
gether. But the language of promise indicates that
Isaiah knew full well that in his day (and in our day),
churls are called liberal, and vice versa. Men refuse
to apply their knowledge of God's standards to the
world in which they live. But it shall not always be
thus.

Exercising Biblical Judement

At this point, we face two crucial questions. The
answers separate many Christian commentators.
First, should we expect this knowledge to come instan-
taneously? Second, when this prophesied world of epis-
temological self-consciousness finally dawns, which
group will be the earthly victors, churls or liberals?

The amillennialist must answer that this parallel
development of knowledge is gradual. The postmil-
lenialist agrees. Wheat and tares develop together.
There is continuity in history.

The premillennialist (especially the dispensation-
alist) dissents. The premillennial position is that this
future era of accurate judgment will come only after
