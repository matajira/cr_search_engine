THE FAVOROF GOD 33

Consistent Living

As I argue throughout this book, it is only be-
cause ethical rebels are not fully self-consistent his-
torically in their ethical rebellion that they can main-
tain power (an external gift from God). If they were
fully self-consistent, they would without exception
all commit suicide, for all those who hate God love
death (Prov. 8:36b). A fully self-consistent ethical
rebel could be no threat to God or God’s people if
they were also fully self-consistent, as we will see
throughout eternity.

Christians are supposed to become more consist-
ent with the religion they profess. They are to imi-
tate the perfect humanity (but not the divinity) of
Jesus Christ. It is only because God's people are not
yet fully self-consistent ethically that they need the
services of sinners (the division of labor) in history —
sinners who are themselves not fully self-consistent
ethically. After the resurrection, both groups are
allowed by God to become fully consistent ethically,
and therefore they must be separated from each
other eternally. God wins, Satan loses. We win, they
Jose. End of argument. End of ethical inconsistency.

When God at last is ready to judge mankind, and
make ethically perfect all His saints, then He perma-
nently separates the saved from the lost. Perfect
saints will no longer need to rely on the productivity
of the rebels. The ethical rebels will no longer need
to be restrained by God in the working out of their
anti-God presuppositions. God’s restraint and His
gifts to the rebels then cease. He sends fre upon
them (Rev. 20:9). History will end because the
