INTRODUCTION 13

finite; man is finite. Man’s mind will never compre-
hend (surround, encircle) God. Man’s mind will
therefore never encompass any aspect of the crea-
tion, for every atom is related to God, and this
brings God back into the picture. The atom, too, is
incomprehensible by man’s finite mind. But God
comprehends Himself and His creation, so we must
go to God’s Word to begin locating the proper ways to
puzzle through any problem. As the person who
keeps turning a blade of grass over and over, getting
more knowledge of it each time, but never seeing both
sides at once, so is man’s ability to observe and think.

Van Til takes any system you hand him, and he
breaks it down into its component parts, turning the
pieces over and over in his mind, finding out what it
is and how it works. The problem is, he never puts
the pieces back together. He just leaves them scat-
tered around on the floor. “Next!”

On the floor, in pieces, they all look pretty much
alike. Go ahead. Pick up that scrap of Barthianism.
The one over there. No, no—the other one. (Wholly
other.) Doesn’t it resemble a fragment from Kant? Or
is it more like Heraclitus? Or could it really be a
direct descendent of Plato?

One thing you will recognize for sure: it’s hu-
manism.”

The Wrong Questions
Van Til has only a finite number of questions to
ask about each system, and some are his special

10, Van Til, Christianity and Barthianism (Philadelphia: Presby-
terian & Reformed, 1962).
