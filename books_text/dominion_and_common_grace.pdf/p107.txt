VAN TILS VERSION OF COMMONGRACE 91

rather Van Til. “Common grace will diminish still
more in the further course of history.”? It is he who
says that common grace is prior grace. “All common
grace is earlier grace.”® The postmillennialist pasi-
tion is that common grace is essentially future grace.
As I wrote in my original essay, which Kline's foot-
notes indicate that he read, but whose summary in-
dicates that he obviously did no read carefully:
“Therefore common grace is essentially future grace.”?
Again, “Common grace has not yet fully developed.” My
emphases were in the original essay. I wanted even
the laziest and most ill-equipped reader to under-
stand my point. Dr. Kline did not understand my
point.

(This time, to help Dr. Kline understand what I
am trying to say, I have added one-sentence, num-
bered summaries at the end of each chapter. This
kills two birds with one stone. People keep telling
Christian Reconstructionist writers that we write
books that are too difficult. We need to simplify our
books, they tell us. We need to write for the average
reader without any theological background. Well, I
have gone the extra mile. I have written it so that
even Dr. Kline can understand it.)

The postmillennialist argues that things will im-
prove over time. Anyway, most things will improve

7. Common Grace, in Common Grace and the Gospel, p. 83.
8, Ihid., p. 82.
9. Gary North, ‘Common Grace, Eschatology, and Biblical

Law,” Journal of Christian Reconstruction, IIL (Winter 1976-77), p-
45.

10. Ibid., p. 41.
