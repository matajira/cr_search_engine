212 DOMINION AND COMMON GRACE

Many people did join themselves covenantally to
Israel in the years following Joseph. They became
Hebrews. There is no other possible explanation for
the rapid growth of the Hebrews; it could not have
been accomplished by a high birthrate alone.? But
the nation as a whole remained pagan. This is why
there was no covenantally faithful Egyptians re-
maining in Egypt on the night of the Passover. There
was a dead firstborn male in every Egyptian house-
hold (Ex. 12:29). The covenant had been eliminated
by Moses’ day. Yet we know that covenant children
do persist. So the covenant of grace had never been
established in the first place. Thus, we conclude that
faithful societies had to submit to circumcision, just
as the Shechemites did (Gen. 34) in order to remain
in God’s social covenant. If they refused to become
circumcised as nations, then individuals of foreign
nations had to covenant directly with the Hebrews
and become Hebrews (Deut. 23:3).

The Egyptians then placed the Hebrews in bond-
age. They attempted to destroy the source of special
grace in their midst. They were allowed by God to
increase their evil. God increased their wealth, so

 

Religion (Tyler, Texas: Institute for Christian Economics, 1985),
Appendix A.

2. The reason why nat is that growing populations are always
characterized by large numbers of children, Yet the number of
Hebrews who conquered Canaan, 602,000 men, was almost ex-
actly the number of men who came out at the Exodus. This indi-
cates a stagnant population, and one that had been stagnant for
at least a generation before the exodus. Thus, it was converts
who added to the population prior to the enslavement. See
North, Moses and Pharaoh, pp. 22-25.
