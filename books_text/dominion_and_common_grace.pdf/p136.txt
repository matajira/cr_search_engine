120 COMINION AND COMMON GRACE

church, and they have invariably mixed in large por-
tions of Greek philosophy and Roman law. Such
concepts as moral law, natural law, and “cosmo-
nomic law” have served theologians as substitutes for
biblical civil law.

What the Bible presents is a concept of God-
revealed law which possesses at least four features:
(1) permanent judicial principles, (2) concrete case-
law ordinances, (3) specification as to when and how
to apply the specific ordinance, and (4) principles of
interpretation (a hermeneutic) that inform us how
changing historical circumstances after the resurrection
have altered the specific application of ordinances. It is
not morally legitimate for Christians to seek refuge
from their God-given responsibilities to make moral
and judicial decisions. Yet they attempt to do just
this by appealing to a vague, undefined, zero-content
system of moral law that is no longer related to the
specific case-law applications of the Old Testament.

Any attempt to escape these responsibilities is a
symptom of antinomianism, that is, anti-biblical law.
Those who cling to a zero-content, “make up your
judicial decisions as you go along, but always in the
name of New Testament ethics” sort of moral law
have adopted a form of antinomianism.

Because theonomists reject this humanism-
influenced concept of natural law—principles of law
that are unconnected to Old Testament case laws—
Reformed theologians who have not yet understood
Van Til’s rejection of all common-ground philosophy
and natural law theology have been quick to point to
the theonomists’ abandonment of some of Calvin’s
