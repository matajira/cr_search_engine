conciusion 241

the tool of the dominion covenant, biblical law, (3) the
predestinating providence of God, and (4) biblical
presuppositionalism—the self-attesting truth of an
infallible Bible.2 We should conclude, then, that
either the dissolution of culture is at hand (for the
common grace of the unregenerate cannot long be
sustained without leadership in the realm of culture
from the regenerate), or else the regenerate must
regain sight of their lost theological heritage: post-
millennialism and biblical law.

For common grace to continue, and for external
cooperation between believers and unbelievers to be
fruitful or even possible, Christians must call the ex-
ternal culture’s guidelines back to God’s revealed
law, They must regain the leadership they forfeited
when they adopted as Christian the speculations of
self-proclaimed “reasonable” apostates. If this is not
done, then we will slide back once more, until the
unbelievers at last resemble the Ik, and the Chris-
tians can begin the process of cultural domination
once more, For common grace to continue to in-
crease, it must be sustained by special grace. Either
unbelievers will be converted, or leadership will flow
back toward the Christians. If neither happens, soci-
ety will return eventually to barbarism.

Understandably, I pray for the regeneration of
the ungodly and the rediscovery of biblical law and
accurate biblical eschatology on the part of present
Christians and future converts. Whether we will see
such a revival in our day is unknown to me. There

2. Gary North and David Chilton, “Apologetics and Strat~
egy,” Christianity and Civilization, 3 (1983), pp. 107-16.
