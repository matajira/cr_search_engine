160 DOMINION AND COMMON GRACE

nal conformity to the law, but not motivated by an
inner ethical transformation.

For the unregenerate, the blessings of God be-
come the means of God’s judgment against them in
history. The external victories of covenant-breakers
become a prelude to disaster for them. The prophets
warned the victorious invading armies concerning
what God would do to them after He had used them
as His rod of discipline against Israel (Isa. 13-23;
Zeph. 2).

Common Law, Common Curse

The dual relationship between common law and
common curse is a necessary backdrop for God's
plan of the ages. Take, for example, the curse of
Adam. Adam and his heirs are burdened with frail
bodies that grow sick and die. Before the flood, there
was a much longer life expectancy for mankind. The
longest life recorded in the Bible, Methuselah’s,
Noah’s grandfather, was 969 years. Methuselah died
in the year that the great flood began.! Thus, as far
as human life is concerned, the greatest sign of God’s

1. Mcthuselah was 969 years old when he died (Gen. 5:27). He
was 187 years old when his son Lamech was born (5:25) and 369
years old when Larnech’s son Noah was bora (5:28-29). Noah was
600 years ald at the time of the great flood (7:6). Therefore, from
the birth of Noah, when Methuselah was 369, until the flood, 600
years later, Methuselah lived out his years (369 + 600 = 969).
The Bible does not say that Methuselah perished in the flood,
but only that he died in the year of the flood. This is such a re-
markable chronology that the burden of proof is on those who
deny the father-to-son relationship in these three generations,
arguing instead for an unstated gap in the chronology.
