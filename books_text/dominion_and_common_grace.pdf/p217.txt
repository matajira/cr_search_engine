FILLED VESSELS

Hath not the potter power over the clay, of the same
lump to make one vessel unto honour, and another unto
dishonour? What if God, willing to shew his wrath,
and to make his power known, endured with much
longsuffering the vessels of wrath fitted to destruction;
and that he might make known the riches of his glory on
the vessels of mercy, which he had afore prepared unto
glory, even us, whom he hath called, not of the Jews
only, but also of the Gentiles? (Rom. 9:21-24).

God has created two kinds of vessels: vessels
made to receive His honor, and vessels made to
receive His dishonor, The latter are called vessels of
wrath.

The text does not say, nor does Paul’s argument
warrant, the idea that the vessels were made by Gad
to receive either honor or dishonor, mercy or wrath,
They are not “neutral vessels,” each awaiting what-
ever the vessel itself may pour into it in history. The
analogy is that of the potter and the clay. “Therefore
