exodus, 21-13

facts, 196

failure of church?, 128

faith, 197

faithfulness, 161, 170, 173

Fall of man, 191-92, 198 (sce
also: Adam)

famine, 210

favor (“favors”), 22, 85, 93, 204
(see also: gifts)

Fell, Barry, 233

final judgment, 2, 66, 124, 214,
244 (see also: doom, crack of)

fire, ix-x, 164, 166

flood, 160-61, 207-9

forgetfulness, 161

four points of dominion, 240-41,
227-30

freedom, 70

free will, 203-4

French Revolution, 183

ghetto, 154-55
Gibeon, 168
Gibeonites, 56
gifts 4, B, 25, 26, 35, 44, 244-45,
247-48
God
attitude of, 50
comprehending by man, 13
Creator, 203
favor of, 7-8, 22, 36, 42, 50,
204, 244-45
fire of, ix-x
Bifts of, 244-45, 247-48 (see
also: gifts)
glory of, 195, 203
grace of, 2
hatred of evil men, 17-18

INDEx 289

image of, 38-39
judgment by, 3, 48, 208 (see
also: doom, crack of)
kingdom of, 1-2, 28, 66
lap of, U1, 189
law of (see: biblical law)
mercy of, 2, 18, 22
proofs of, 196
restraint of sin, 85, 59-60,
129, 192, 237
table’s crumbs, 84, 99, 167,
190, 214 :
ways of, 82
wrath of, 40, 81, 98, 204
Gog, ix
Goliath, 17
good and evil in history, 129-34
Goshen, 2it
gospel, 127
grace
gift, 4
judgment & 28
law & 28
longsuffering, 2
(see also: common grace)
Great Awakening, 174-75
Graetz, Heinrich, 22in
Greeks, 171
growth, 112, 137, 172 (see also:
positive feedback)

Ham, 221

hatred by God, 17-18, 28, 5t

Hayek, FA, 12

hearts (law written on), 96-97
(see also: Scripture Index,
Romans 2:14-15; Hebrews
8:10-1)

hell, 30

Herod, 221
