XxI¥ DOMINION AND COMMON GRACE

ment. The resurrection of the dead takes place. End
of history, Curtain call. Boos and cheers from the
heavenly host.

The Postmillennialist’s Problem

The postmillennialist argues that the kingdom of
God is to be progressively manifested on earth be-
fore the day of judgment, and therefore before the
Rapture, which he identifies with the last judgment.
Then how can these events take place? Where will
all those sinners come from? The army of Satan will
be filled with people who have been recruited from
the nations of the earth, not angels.

We need to consider several possible assumptions
that may be coloring the exegesis of either postmil-
lennialists or the questioners.

1, Does a theology of the extension of
God’s kingdom on earth require that almost
everyone on earth in the era close to that final
day be a born-again believer in Christ?

2. Can born-again believers fall from grace
and then rebel? In short, can Satan gain re-
cruits from the born-again invisible church?

3. Can unbelievers seem to be saints in the
camp of the saints, almost as spies who success-
fully invade an enemy military camp?

4, How can unbelievers possess so much
power after generations of Christian dominion?

The answer to the first two questions is “no.”
Postmillennialism does not require that all or even
