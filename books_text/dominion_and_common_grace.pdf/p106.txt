$90 DOMINION AND COMMON GRACE

Along with the hermeneutical deficiencies
of Chalcedon’s millennialism there is a funda-
mental theological problem that besets it. And
here we come around again to Chalcedon’s con-
founding the biblical concepts of the holy and
the common. As we have seen, Chalcedon’s
brand of postmillennialism envisages as the cli-
max of the millennium something more than a
high degree of success in the church's evangelis-
tic mission to the world. An additional millen-
nial prospect (one which they particularly relish)
is that of a material prosperity and a world-wide
eminence and dominance of Christ’s established
kingdom on earth, with a divinely enforced
submission of the nations to the world govern-
ment of the Christocracy. . . . The insuperable
theological objection to any and every such
chiliastic construction is that it entails the
assumption of a premature eclipse of the order
of common grace. . . . In thus postulating the
termination of the common grace order before
the consummation, Chalcedon’s postmillen-
nialism in effect attributes unfaithfulness to
God, for God committed himself in his ancient
covenant to maintain that order for as long as
the earth endures.®

It is not Chalcedon’s postmillennialists who pre-
dict the erosion of the common grace order, but

6. Meredith G. Kline, “Comments on an Old-New Error,”
Westminster Theological Journal, XLI (Fall 1978), pp. 183, 184,
