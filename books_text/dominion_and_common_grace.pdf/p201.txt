EPISTEMOLOGICAL SELF-CONSCIOUSNESS AND COOPERATION 185.

revive paganism late in the mid-fourth century was
ludicrous—it was half-hearted paganism, at best. It
failed after two years.

In the middle of the second century, a.p., Mar-
cus Aurelius, a true philosopher-king in the tradition
of Plato, had been a major persecutor of Christians;
Justin Martyr died during Aurelius’ years as em-
peror. But the emperor's debauched son Commodus
was too busy with his 300 female concubines and 300
males® to bother about systematic persecutions.
Who was more self-conscious, epistemologically
speaking? Aurelius still had the light of reason before
him; his son was immersed in the religion of revolu-
tion and escape—cultural impotence. He was more
willing than his philosopher-persecutor father to fol-
low the logic of his satanic faith. He preferred de-
bauchery to power. Commodus was assassinated 13
years after he became Emperor. The Senate resolved
that his name be execrated."

The Marxist Challenge
The African tribe, the Ik (see chapter 5), is so con-
sistent with pagan demonism that its members are a
threat to no one but themselves. Communists, on
the other hand, are a threat. They believe in linear
history (officially, anyway — their system is at bottom

10, Edward Gibbon, The History of the Decline and Fall of the
Roman Empire, Milman edition, 5 Vols. (Philadelphia: Porter &
Coates, {1776]), I, p. 144.

11, Ethelbert Staufler, Christ and the Caesars (Philadelphia:
‘Westminster Press, 1955), p. 223.
