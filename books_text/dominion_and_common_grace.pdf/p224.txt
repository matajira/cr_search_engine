208 DOMINION AND COMMON GRACE

is attached (Eph. 6:2). The specific promise is long
life. Here, above all other promises, is the one that
men universally respect. “O, king, live forever” was
a common expression of homage in the ancient
world (Dan. 2:4; 3:9; 5:10). Clearly, those of the era
before the flood were recipients of lengthening life.
Yet they were progressively evil. God finally stood it
no longer, and He killed them all, and all living ani-
mals under their covenantal jurisdiction. He made
one exception: Noah. He placed representative ani-
mals under Noah’s covenantal jurisdiction. He gave
them life.

Why did God give men incteasing common
grace if they were growing more evil? As a way to in-
crease the magnitude of their judgment at the flood.
In this case, it was a question of more water on their
heads—fiery coals came eternally. When God intends to
bring an end to a covenantally rebellious culture, He first in-
creases their power and might. This speeds up the proc-
ess of judgment. They fill up their iniquity faster and
higher. Then He destroys them in a discontinuous
act of judgment.

They had a testimony before them: Noah’s life.
They also had another: Noah’s slowly growing ark.
They of course had the work of the law in their
hearts, but they also had unique historical testimon-
ies to Gad’s covenantal curses, the testimony of eth-
ics (Noah's righteousness), God’s coming judgment
(the ark), and of the end of their inheritance (the
flood that the ark pointed to).

In this instance, the wealth of the wicked was not
laid up for the righteous, except technological knowl-

 
