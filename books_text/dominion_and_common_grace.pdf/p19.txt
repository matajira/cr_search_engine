INTRODUCTION 3

Abraham concerning the conquest of the Promised
Land: “But in the fourth generation they shall come
hither again: for the iniquity of the Amorites is not
yet full” (Gen. 15:16). In the fourth generation after
they became subservient to Egypt, the Israelites
would return. Moses’ generation was the fourth after
Jacob came down to Egypt (Levi, Kohath, Amram,
Moses: Ex. 6:16, 18, 20). They came to the edge of
the land, but drew back in fear; Joshua's generation
conquered it.

Why the delay in judging the Amorites? Their
iniquity was not yet full, God gave them time to fill it
up. He gave them continuity. Then, in Joshua’s day,
he gave them discontinuity. Judgment came at last.

So it is with the history of man. God extends time
to all men; then, at the final day (or at the death of
each person), judgment comes. Judgment day con-
firms eternal life to the regenerate, and the second
death (Rev. 20:14) to the unregenerate. Continuity is
broken by discontinuity.

Common Grace

Tf you want a four-word summary of this book,
here it is: common grace is continuity, It is also a prelude
to judgment.

The concept of common grace is seldom discussed
outside of Calvinistic circles, although all Christian
theologies must eventually come to grips with the
issues underlying the debate over common grace.
The phrase was employed by colonial American
Puritans. I came across it on several occasions when
I was doing research on the colonial Puritans’ eco-
