GOD'S RESTRAINING HAND V8. TOTAL DEPRAVITY 63.

meaning in history is judged by the standard of bibli-
cal law. Biblical law judges all men and all institu-
tions. All men are held accountable to God. Unre-
deemed men have the work of the law written in
their hearts, while Christians have the law itself writ-
ten in their hearts.

This is why the cross has meaning in history. It
combines common curse and common grace, special
curse and special grace. Christ died in order to make
history possible—to reduce the historic judgment of
God against rebellious mankind. This has benefited
covenant-breakers and covenant-keepers alike.
Cooperation among men becomes possible by means
of Christ’s sacrifice. Christ also died to bring eternal
life to His people. When God at the end of the mil-
lennial age gives up unregenerate people to their
lusts, they will revolt against Him, and the final judg-
ment will come.

In summary:

1. Law is a means of grace.

2. Law is the basis of the curses.

3. There are common grace and special
grace.

4, There are common curse and special
curse.

5. Common grace condemns rebels even
more.

6. Evil men do good through God’s com-
mon grace.

7. God's law is known specially and com-
monly.
