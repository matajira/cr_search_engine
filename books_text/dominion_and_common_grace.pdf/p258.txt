242. DOMINION AND COMMON GRACE

are reasons to believe that it can and will happen.
There are also reasons to doubt such optimism. The
Lord knows.

We must abandon antinomianism and eschatolo-
gies that are inherently antinomian. We must call
men back to faith in the God of the whole Bible. We
must affirm that in the plan of God there will come a
day of increased self-awareness, when men will call
churls churlish and liberal men gracious (Isa. 32).
This will be a day of great external blessings—the
greatest in history. Long ages of such self-awareness
unfold before us. And at the end of time comes a gen-
eration of rebels who know churls from liberals and
strike out against the godly. They will lose the war.

Common Grace Is Future Grace

Therefore, common grace is essentially future grace.
There is an ebb and flow of both common grace and
special grace throughout history, but essentially the
manifestation of all grace is in the future. It must not
be seen as essentially prior or earlier grace. Only
amillennialists can consistently hold to such a posi-
tion—antinomian amillennialists at that. Premillen-
nialists at least have the millennium in front of them.
In the amillennial scheme, the final judgment ap-
pears at the end of time against the backdrop of
declining common grace. The postmillennial view
sees this final satanic rebellion against a background
of maximum common grace. The common curse will

3. Gary North, The Sinai Strategy: Economics and the Ten Com-
mandments (Tyler, Texas: Institute for Christian Economics,
1986), pp. 86-92: “The Sabbath Millennium.”
