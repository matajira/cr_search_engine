154 DOMINION AND COMMON GRACE

God’s program of “positive feedback”—the progres-
sive sanctification of civilization. They have there-
fore abandoned an eschatology of victory in history.

Which is the primary impulse of amillennialism,
its defeatist eschatology or its antinomianism? It is
possible to make a good case for either. I think anti-
nomianism is the primary impulse. If the conditional
promises of Deuteronomy 28:1-14 are taken seri-
ously, and our empowering by the Holy Spirit is
taken seriously, then the doctrine of historical prog-
ress can be taken seriously. This progress must be-
come externalized through the biblical system of
positive feedback (Deut. 8:18). To deny such histor-
ical, institutional progress, the amillennialist must
reject biblical law. Postmillennialism is “a nice
dream,” as one Protestant Reformed Church pastor
said from the pulpit. Amillennialists can afford to ig-
nore nice dreams. Biblical law, on the other hand,
involves a direct assault on pietism, humanism,
mysticism, and all other versions of the escape relig-
ion, It cannot be ignored. It calls men out of their
monastic cloisters, their ghettos, and their sanctuar-
ies. Preach biblical law, and you will not be dis-
missed as a dreamer; you will be challenged as a fan-
atic. I think antinomianism is the underlying motive
of amillennialism.

A war is in progress—a war with humanism.
Humanism will not respect Christian sanctuaries.
Humanism must be defeated. Biblical law is the
weapon, with Christians empowered by the Holy
Spirit. If you have no weapons, you have an excuse
not to fight. You run for your ghetto. As the Jews
