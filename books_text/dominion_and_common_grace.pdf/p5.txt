This book is dedicated to
John Frame

an uncommonly gracious man,
who will no doubt conclude that
portions of this book are good,
other portions are questionable,
but the topic warrants further study.
