WARFIELD'S VISION OF VICTORY: LOST AND FOUND 265,

wholes. They abhorred “speculation” and “met-
aphysics” as unconscionable flights from the
basic realities of the physical world and the
human mind. And at least some of them
assumed that this approach could be used to
convince all rational souls of the truth of Chris-
tianity, the necessity of traditional social order,
and the capacity of scientific methods to reveal
whatever may be learned about the world.?

It should not be surprising to find that Machen,
as the last of the “Old Princetonians,” spoke of the
need of defending a “scientific theology.” His debt
to the “old Princeton,” including its experientialism,
was very great.!! The humanists of the twentieth
century have successfully called in all such debts to
nineteenth-century rationalism. The debtors went
epistemologically bankrupt.

Van Til’s approach takes the best of both Kuyper
and Warfield. In contrast to Kuyper, Van Til argues
that we can do more than preach to the natural (un-
regenerate) man. We can show him, by the premises
of his own philosophy, that he has no place to stand

9. Mark Noll, “Introduction,” in Noll (ed.), The Princeton
Theology, 1812-1921 (Grand Rapids, Michigan: Baker, 1983),
p. 31.

10. Machen, Christianity and Liberalism (New York: Mac-
millan, 1923), p. 12.

11, On Machen’s promotion of the idea of the importance of
an experience from God, see ibid., p. 67. On the “old Princeton”
and its theology of experience, see W. Andrew Hoffecker, Piely
and the Princeton Theologians (Phillipsburg, New Jersey: Presbyter-
ian & Reformed, 1981).
