ESCHATOLOGY AND BIBLICAL LAW 137

ers. Sometimes they try to do both. Their language
is the language of mysticism, but their strategy is
common-ground. It was Dooyeweerd’s outspoken
resistance to Old Testament and New Testament au-
thority over the content of his hypothesized sphere
Jaws that has led his increasingly radical, increas-
ingly antinomian followers into anti-Christian
paths.

Van Til recognized this lack of content in Dooye-
weerd’s methodology, just as he recognized the
common-ground nature of Dooyeweerd’s system,?5
but since he himself never developed an apologetic
method based on the covenantal requirements of re-
vealed biblical law, he could not thrust an exegetical
stake into Dooyeweerd’s epistemological “heart.”
Like Dracula rising from the dead, Dooyeweerd’s
philosophy keeps making reappearances, though in-
creasingly dressed up in the guerilla uniforms worn
by safely tenured professors of liberation theology —
“designer camouflage,” one might say.

Amillennialists have preached the dominion cov-
enant (“cultural mandate”), and then have turned
around and denied the efficacy of biblical law in cul-
ture. They necessarily deny the cultural efficacy of
biblical law because their eschatological interpreta-
tion has led them to conclude that there can be no
external, cultural victory in time and on earth by
faithful Christians. Epistemological self-conscious-
ness will increase, but things only get worse over

24, Van Til, “Response,” Jerusalem and Athens, p. 112.
25. Ibid., pp. 102-3.
