‘THE FAVOR OF GOD 45

Thus, the doctrine of common grace must apply
not only to men but also to Satan and the fallen
angels. This is what Van Til denies, because he de-
fines common grace as favor in general rather than gifts
in general. The second concept does not imply the
first.

God does not favor “mankind” as such. He show-
ers favors on all men, but this does not mean that He
favors men in general. Men in general rebelled
against Him in the garden. Adam and Eve, man-
kind’s representatives, brought the entire human
race under God’s wrath. God in His grace gave them
time and covenant promises, for He looked forward
to the death of His Son on the cross. On this basis,
and only on this basis, men have been given life in
history. Some have been given life in order to extend
God’s kingdom, while others have been given life
(like Pharaoh) to demonstrate God’s power, and to
heap coals of fire eternally on their heads.

In summary:

1. God hated Esau before he was born: no
favor.

2. God gives gifts to the unregenerate.

3. God heals them as a savior (I Tim. 4:10).

4, We are required to love our enemies.

5. This means we must deal with them
lawfully.

6. God tells us to deal lawfully with evil men
in order to heap coals of fire on their heads,

7. Lawful dealing by us will lead some men
to Christ.
