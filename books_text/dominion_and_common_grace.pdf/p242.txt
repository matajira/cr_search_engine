226 DOMINION AND COMMON GRACE

Eternally chained to this marble rock of Being,
Chained, eternally chained, eternally.

And the worlds drag us with them in their rounds,
Howling their songs of death, and. we—

We are the apes of a cold God."

It is obvious that he had left the faith. It is
equally obvious that he was haunted by the eternal
consequences of becoming an excommunicate in a
world that passes into eternity. “Chained, eternally
chained, eternally.” He had been an inside man.
And when he rebelled, he did so in the name of the
religion of revolution, that ancient enemy of biblical
religion.

Joseph Stalin had been a seminary student in his
youth. He spent much of his time reading forbid-
den books: Darwinian biology, Gogol, Chekhov.
One of his schoolmates recalls: “We would some-
times read in chapel during service, hiding the book
under the pews, Of course, we had to be extremely
careful not to get caught by the masters. Books were
Joseph’s inseparable friends; he would not part with
them even at meal times,”*

The vision of Western millennial hope motivated
the Chinese Communists, a vision that sprang

11. Zbid., pp. 81-83.

12. Gary North, Mars’s Religion of Revolution: The Doctrine of
Creative Destruction (Nutley, New Jersey: Craig Press, 1968).

13. Isaac Deutscher, Stalin: A Political Biagraphy (New York:
Vintage, [1949] 1962), p. 17.

14. Wid., p. 17.

15, Seung Ik Lee, The New China: An Eastern Vision of Mes-
sianic Hope (Ph.D. dissertation, University of Pittsburgh, 1982).
