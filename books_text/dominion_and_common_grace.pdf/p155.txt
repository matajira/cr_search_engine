ESCHATOLOGY AND BIBLICAL LAW 139

present is a position in Christian (normative) ethics.
They do not logically commit those who agree with
them to any particular school of eschatological inter-
pretation.””” He is correct: logically, there is no con-
nection. Covenantally, the two doctrines are inescapa-
ble: when biblical law is preached, believed, and
obeyed, there must be blessings; blessings lead ines-
capably to victory.

Perhaps he has decided that it is unwise to try to
fight a two-front war: theonomy and postmillennial-
ism. (My attitude is that it is giving away the battle
not to fight on both fronts simultaneously, which is
what this book is about.) On the other hand, per-
haps he wanted to narrow the focus of his discussion
of ethics to the question of the rightness or wrong-
ness, biblically speaking, of adopting biblical law in
New Testament times, without any consideration of
the historical consequences of the covenantal process
of positive feedback (Deut. 8:18). If this was his in-
tention, then his books go too far down the road to-
ward the issue of the empowering of Christians to
obey biblical law, As soon as you raise this issue of
the Spirit’s empowering, you raise the unified issue
of positive feedback, external growth, and long-term
victory.

To escape the postmillennial implications of this
argument, the defender of theonomy (God’s law)
would have to argue that the preaching of the law
does not necessarily have to produce a faithful, sus-

97. Greg L. Bahnsen, By This Standard: The Authority of Cod’s
Law Today (Tyler, Texas: Institute for Christian Economics,
1985), p. 8.
