5B OOMINION AND COMMON GRACE
gives all men, including ethical rebels, additional

time.

With respect to God's restraint of the total de-
pravity of men, consider His curse of the ground
(Gen. 3:17-19). Man must labor in the sweat of his
brow in order to eat. The earth gives up her fruits,
but only through labor. Still, this common curse also
involves common grace. Men are compelled to coop-
erate with each other in a world of scarcity if they
wish to increase their income. They may be murder-
ers in their hearts, but they must restrain their emo-
tions and cooperate. The division of labor makes
possible the specialization of production. This, in
turn, promotes increased wealth for all those who
labor. Men are restrained by scarcity, which appears
to be a one-sided curse. Not so; it is equally a bless-
ing.> This is the meaning of common grace: com-
mon curse and common grace go together until the
final judgment. After that, there is no more common
grace or common curse. There is eternal separation.

The cross is the best example of the fusion of
grace and curse, Christ was totally cursed on the
cross. At the same time, this was God’s act of incom-
parable grace. Justice and mercy are linked at the
cross. Christ died, thereby experiencing the curse
common to ail men. Yet through that death, Christ
propitiated God’s wrath. The cross is the source of
common grace on earth—life, law, order, power—as
well as the source of special grace.

5. Gary North, The Dominion Covenant: Genesis (Tyler, Texas:
Institute for Christian Economics, 1982), ch. 10: “Scarcity: Curse
and Blessing.”
