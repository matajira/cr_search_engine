ESCHATOLOGY AND BIBLICAL Law 141

“theonomy” with postmillennialism (though not
necessarily postmillennialism with theonomy: see
chapter six on the antinomian theology of Jonathan
Edwards).

Bahnsen has argued forcefully that any discus-
sion of the expansion of God’s kingdom must include
a discussion of the visible manifestations of this king-
dom. To speak of the kingdom of God without being
able to point to its expansion of influence outside the
narrow confines of the institutional church is mis-
leading.# This argument also is correct.

But what of a parallel argument? If we were to
argue that the greater empowering of the Holy Spirit
in the New Testament era is only a kind of theoreti-
cal backdrop to history, and therefore biblical law
will not actually be preached and obeyed in this
pre-final-judgment agé (which is the amillennialist
argument), then we would really be abandoning the
whole idea of the Holy Spirit’s empowering of Chris-
tians and Christian society in history. It would be an
argument analogous to the kingdom arguments of
the amillennialist: “Yes, God has a kingdom, and
Christians are part of it, and it is a victorious king-
dom; however, there are no visible signs of the King
or His kingdom, and Christians will be increasingly
defeated in history.” Similarly, “Yes, the Spirit em-
powers Christians to obey biblical law; however,
they will not adopt or obey biblical law in history.”

29. Greg Bahnsen, “This World and the Kingdom of God,”
Christian Reconstruction, VILL (Sept./Oct., 1986), published by the
Institute for Christian Economics.
