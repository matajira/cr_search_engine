EPISTEMOLOGICAL SELF-CONSCIOUSNESS ANO COOPERATION 199

tice what their increasingly self-conscious sui-
cida] theology leads them toward.

6. Newton’s worldview was not consist-
ently anti-trinitarian.

7. Christians adopted Newton’s worldview
uncritically,

8. This has led to confusion, as each side
has progressively become more self-conscious,

9. Marcus Aurelius was less consistent with
paganism, and therefore more of a threat to the
church, than his debauched son Commodus.

10. The Marxists are more of a threat to the
‘West than the African tribe, the Ik.

11. The Marxists have stolen a biblical out-
look, so they are more successful in recruiting
despairing savages.

12, Satan dares not become consistent with
his self-professed philosophy of existence if he
wishes to rebel against God in. power.

13. The satanists need common grace in
order to run a successful rebellion against God
and God’s kingdom.

14. We can cooperate with our ethical ene-
ties because Bible-based conclusions still dom-
inate society.

15. Biblical principles produce benefits that
unbelievers want.

16. They will cooperate with us if they want
more of these benefits, and if we are faithful
servants before God.

17. Christians are to set the agenda for co-
operative ventures with pagans. We hire their
services. They sit under the King’s table.
