ESCHATOLOGY AND BIBLICALLaW 113

linear history over pagan cyclical history that trans-
formed the historical thinking of the West.'

But the biblical view of history is more than lin-
ear. It is progressive. It involves visible cultural expan-
sion. It is this faith in cultural progress which has
been unique to modern Western civilization. This
was not due to Augustine as such, for there was an
element of otherworldiiness—a dualism between the
progress of the soul and the rise and fall of earthly
civilizations —in Augustine’s view of time.?

It was the Reformation, and especially the Puri-
tan vision, which brought the idea of progress to the
West. The Puritans believed that there is a relation-
ship between covenantal obedience and cultural ad-
vance.3 This optimistic outlook was secularized by
seventeenth-century Enlightenment thinkers,* and
its waning in the twentieth century threatens the sur-
vival of Western humanistic civilization.>

1, Charles Norris Cochrane, Christianity and Classical Culture:
A Study in Thought and Action from Augustus to Augustine (New York:
Oxford University Press, [1944] 1957), pp. 480-83.

2. Herbert J. Muller, The Uses of the Past: Profiles of Former Saci-
eties (New York: Oxford University Press, 1957), pp. 174-75.
Muller blames Augustine’s dualism of body and soul on Paul,
and argues that it was overcome in later Christian thinking by
the recovery of the Greek heritage. This has the case precisely
backwards, as Cochrane’s study indicates. It was Greek thinking
that was dualistic.

3. Journal of Christian Reconstruction, VI (Summer 1979): Sym-
posium on Puritanism and Progress.

4. Robert A. Nisbet, “The Year 2000 and All That,” Commen-
tary (June 1968).

5. Robert Nisbet, History of the Idea of Progress (New York:
Basic Books, 1980), ch. 9 and Epilogue.
