WHEAT AND TARES: HISTORICAL CONTINUITY 73.

Dominion through Superior Judgment

Isaiah 32 is a neglected portion of Scripture in our
day. It informs us of a remarkable era that is coming.
It is an era of “epistemological self-consciousness,” to
use Cornelius Van Til’s phrase. It is an era when
men will know God’s standards and apply them ac-
curately to the historical situation. It is not an era
beyond the final judgment, for it speaks of churls as
well as liberal people. Yet it cannot be an era that is
inaugurated by a radical separation between saved
and lost (the Rapture), for such a separation comes
only at the end of time. This era will come before
Christ returns physically to earth in judgment. We
read in the first eight verses:

Behold, a king shail reign in righteousness,
and princes shall rule in judgment. And a man
shall be as an hiding place from the wind, anda
covert from the tempest; as rivers of water in a
dry place, as the shadow of a great rock in a_
weary land. And the eyes of them that see shall
not be dim, and the ears of them that hear shall
hearken. The heart also of the rash shall under-
stand knowledge, and the tongue of the stam-
merers shall be ready to speak plainly. The vile
person shail be no more called liberal, nor the
churl said to be bountiful. For the vile person
will speak villany, and his heart will work ini-
quity, to practise hypocrisy, and to utter error
against the Lorn, to make empty the soul of the
hungry, and he will cause the drink of the thirsty
to fail. The instruments also of the churl are
