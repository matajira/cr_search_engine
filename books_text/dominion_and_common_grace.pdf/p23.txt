aNTRODUCTION 7

the disputed “three points” of the Christian Re-
formed Church in 1924, asserts the following:

1. Concerning the favorable attitude of God ta-
ward mankind in general and not only toward the elect,
the Synod declares that according to Scripture
and the Confession it is certain that, besides the
saving grace of God bestowed only upon those
chosen to eternal life, there is also a certain
favor or grace of God manifested to His crea-
tures in general... .3

2. Concerning the restraint of sin in the life of
the individual and of society, the Synod declares
that according to Scripture and the Confession
there is such a restraint of sin... .*

3. Concerning the performance of so-called civic
righteousness by the unregenerate, the Synod de-
clares that according to Scripture and the Con-
fession the unregenerate, although incapable of
any saving good (Canons of Dort, HI, FV:3), can
perform such civic good... .3

These principles can serve as a starting point for
a discussion of common grace,

3. R. B. Kuiper, To Be or Not to Be Reformed: Whither the Chris-
tien Reformed Church? (Grand Rapids, Michigan: Zondervan,
1959), p. 105. Van Til’s version was taken from The Banner (June
1, 1939), and differs slightly in the wording. I have decided to use
Kuiper’s summary. Van Til, Common Grace, in Common Grace and
the Gospel (Nutley, New Jersey: Presbyterian & Reformed, 1972),
pp. 19-20.

4, Idem.

5. Ibid., pp. 105-6.
