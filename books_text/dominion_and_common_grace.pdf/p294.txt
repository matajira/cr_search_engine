278 DOMINION AND COMMON GRACE

as the epistemological foundation of “Reconstruc-
tionism” in By What Standard? (1959), a decade before
the term “Christian Reconstruction” was invented.

The rapid spread of “Reconstructionist” ideas
through the Christian community can be explained
by many factors, but perhaps the most important
ones are these:

1. The rise of the six-day creationist movement
after 1960. (Rushdoony helped launch this by getting
Presbyterian & Reformed to publish Morris & Whit-
comb’s Genesis Flood, after other Christian publishers
had turned it down.) This has helped to overcome
the belief that Christians just have to make an intel-
lectual deal with evolutionism. This deal-seeking
had paralyzed Christian scholarship since 1925.

2. The visible disintegration of humanism after
1963. The enemy is no longer self-confident in the
universal logic of neutral reason. Everything is now
up for grabs, and everyone is grabbing. With their
loss of faith in universal neutral reason, humanists
are less secure about challenging the validity of a
consistent Christian world view.

3. The Roe x Wade decision in 1973, which struck
down state laws against abortion. The literal life-
and-death issue of abortion reveals the truth of Van
Til’s apologetic principle: there can be no neutrality.
Either the unborn infant lives or is destroyed. There
is no in-between, The fundamentalists are now
mobilizing, as they have not done since Prohibition,
The sin of abortion is being challenged in the name
of an explicitly biblical and Old Testament law sys-
tem. This has been the first practical (tactical) step in
