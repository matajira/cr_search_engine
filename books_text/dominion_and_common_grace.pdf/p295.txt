WARFIELD'S VISION OF VICTORY: LOST AND FOUND 279

the reintroduction of Christians’ faith in biblical law.

4. The rise of the independent Christian school
movement since 1965, To pull children out of the hu-
manist public schools is now seen as a religious duty
by millions of parents. New curricula are now
needed. Thus, there has been a quest for “recon-
structed” textbooks—books that are different from
state textbooks. The battle for the mind has at last
become a visible reality to Christians who previously
had believed in intellectual neutrality.

5. The rise of television’s mass-appeal “elec-
tronic ministries.” It is difficult to mobilize support
(read: increase donations) by means of a theology of
defeat. Supporters will not give enthusiastically to
“just another ministry of failure”, they can give to
local churches if they are moved by an eschatology of
failure. After all, it is the “Rapture” guarantee of the
local church which had brought so many people in,
1925-1975. To compete, the T.V. ministries had to
offer something new. Many offer the grim story of
humanist tyranny; the audience gets angry and
wants to fight. Why fight to lose? Thus, the language
of avowed premillennialists has become postmillen-
nial. This has “softened the market” for a revival of
Warfield’s vision of victory.

Conclusion
We are now witnessing the beginning of a true
paradigm shift, as Thomas Kuhn has called it. The
Christian community in the United States has at last
begun to adopt the intellectual foundations of a new
worldview, and this is always the first step in the re-
