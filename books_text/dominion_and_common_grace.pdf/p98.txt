82 DOMINION AND COMMON GRACE

time, the reprobate man’s self-knowledge therefore
increases. Self-knowledge is a good thing, a gift from
God. Romans 7 teaches that the increase in self-
knowledge that biblical law brings can produce in
men a sense of death, which through God’s grace
leads to life. I should think that we would associate
such an increase in the self-knowledge of the
reprobate with an increase of common grace. Yet
Van Til says the opposite: it leads to a reduction of
common grace, This is an oddity of his exposition.
There is a reason for it: his amillennial eschatology.

He says also that “God allows men to follow the
path of their self-chosen rejection of Him more
rapidly than ever toward the final consummation.
God increases His attitude of wrath upon the
reprobate as time goes on, until at the end of time, at
the great consummation of history, their condition
has caught up with their state.” But be forewarned:
he also argues (as we shall see) that the reprobate
will progressively triumph over the church in his-
tory. Thus, Van Til is arguing implicitly that Gods
increasing wrath to the unregenerate leads to their increasing
external victory over the church in history. God says, in
effect, “I hate you so much, and My hatred is in-
creasing so rapidly, that I will let you kick the
stuffing out of My people, whom I love with an in-
creasing fervor as they increase in righteous self-
knowledge.” The ways of Gad are strange . . . if you
are an amillennialist,

The condition of the reprobate is one of increas-
ing victory; then, overnight, it turns into total defeat
at the final judgment. Yet Van Til describes this dis-
