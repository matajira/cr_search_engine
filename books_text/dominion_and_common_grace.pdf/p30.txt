14 DOMINION AND GOMNON GRACE

favorites. These are the ones he usually asks. Of
course, he has Iots more in reserve. The trouble is,
he sometimes asks less appropriate questions, just
because he likes his favorites so much. Common Grace
and the Gospel suffers from this flaw. Other questions
should have been asked, but he is determined to ask
the questions he wants to ask, and others just will
not do. Even better ones.

In this book, I try to ask better questions.

Why attack Van Til? Because he is the best. If
some theological nonentity had written Common
Grace and the Gospel, it would not matter if anyone
replied to him. But it matters with Van Til. He is the
man who has reconstructed Christian philosophy in
our time, by far the most important Christian philos-
opher of all time: His dissecting and puzzling have
cut apart all the alternative systems. He has knocked
all the Humpty Dumpties off their respective walls.
But when he goes in to try to put a case of biblical
eggs in their place, he sometimes slips in the goo.

He simply slipped up (or fell down) with Common
Grace and the Gospel.

So, what is wrong with his book on common
grace? First, it is cluttered up with extraneous mate-
rial. The book is filled with questions concerning
Platonic reasoning, Roman Catholic apologetics,
and other specialized philosophical topics. But these
topics are not the heart of the debate over common
grace. As with everything else Van Til writes about,
he can use them to illustrate philosophical topics,
but in this case, this overemphasis on philosophy
misleads the reader. It steers him away from the key
