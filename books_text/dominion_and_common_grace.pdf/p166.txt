180 DOMINION AND COMMON GRACE

the Christian Party it implicitly accuses all Chris-
tians who do not join it of apostasy and betrayal. It
will be exposed, in an aggravated degree, to that
temptation which the Devil spares none of us at any
time—the temptation of claiming for our favourite
opinions that kind of degree of certainty and author-
ity which really belongs only to our Faith.”4+

This is an odd line of argumentation. First, what
he describes as a strictly political problem is in fact
the problem with any distinctly Christian institu-
tion. Christians need to do what is God’s will, but in
doing it, they exclude other acts as not being in
God's will. Yet according to his view of history, these
decisions will become clearer over time, and the
range of Christian (as well as non-Christian) choices
will become much narrower. So what is the prob-
lem? It should be easier as time goes on to build
Christian institutions of all kinds, not just political
organizations,

Second, why doesn’t this same problem of speak-
ing in the name of the accepted moral sovereign
afflict every religious, political, or ideological group?
Why single out politics? Isn’t ascertaining God’s will
equally a problem in all other institutions? Further-
more, why are Christian political coalitions so evil,
so doomed to defeat? Aren’t coalitions going on in
every area of life all the time? Besides, why is the
problem of coalitions a uniquely Christian problem?
Humanists make coalitions all the time—yes, even

34, C. S. Lewis, God in the Dack: Essays on Theology and Ethics
(Grand Rapids, Michigan: Eerdmans, 1970), p. 198.
