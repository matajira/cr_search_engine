10 DOMINION AND COMMON GRACE

favorite mistakes with the same fervency that he
clings to his favorite truths. .

This raises a much-neglected point. Van Til is an
enigma to those of us who studied under him or who
have struggled through his books. His books are

“always filled with brilliant insights, but it is very
difficult to remember where any single insight ap-
peared. They are scattered like loose diamonds
throughout his writings, but they never seem to fit in
any particular slot. Any given insight might just as
well be in any of his books—or all of them. (In fact,
it may be in all of them.) They are not systematically
placed brilliant insights. They are just brilliant. He
makes good use from them, too; he repeats the same
ones in many of his books. “No use throwing this
away after only one time; it’s almost like new. I'll use
it again!” The man is clearly Dutch.

His most effective critical arguments sound the
same in every book. Randomly pick up a coverless
Van Til book, and start reading; you may not be
sure from the development of the arguments just
what the book is about, or who it is intended to
refute. His books all wind up talking about the same
three dozen themes. (Or is it four dozen?) Just keep
reading. You will probably find his favorite Greeks:
Plato, who struggled unsuccessfully to reconcile Par-
menides and Heraclitus. But only rarely will you find
a footnote to one of their primary source documents.”

7. The remarkable thing is that Van Til knows his primary
source material better than most philosophers. As a graduate
student at Princeton University, he studied under the famous
and rigorous classical scholar-philosopher, A. A, Bowman. He
