166 DOMINION AND COMMON GRACE

and power, attempt to remove the God-fearing
sources of God’s special grace from their midst, or
attempt to Sodomize them, they have symbolically
attacked the table of the Lord. God then burns them
with fire. We see this in the AEDS epidemic that will
eliminate most homosexuals before the year 2000. It
will also bankrupt or radically transform all “public”
(socialized) health care facilities. It may even spread
to the general population.3 God will not be mocked.*+

3. Gene Antonio, The AIDS Cover-Up (San Francisco: Igna-
tius Press, 1986).

4. In 1978, the First Orthodox Presbyterian Church of San
Francisco hired as a paid organist a young man who had recently
joined the church. Once on the payroll, he told the pastor that he
was a practicing homosexual, The church fired him because of
this. “On June 14, 1979, the pastor, the congregation, and the
presbytery were sued, The organist had sued on the grounds that,
his employment was protected by the city’s gay rights ordinance.
‘Under the direction of constitutional attorney John Whitehead,
the case went to court for summary judgment in March 1980.
The judge ruled in the church’s favor, and the full course of the
suit ended by December, The congregation~with the help of
many—defrayed defense costs of $100,000. Since that time the
church has experienced a number of vandalism attacks culmi-
nating in an attempt by an arsonist to burn down the building
and the manse.” Charles G. Dennison (ed.), Orthudox Presbyterian
Church, 1936-1986 (Philadelphia: Committee for the Historian of
the Orthodox Presbyterian Church, 1986), p. 191.

They tried to burn God’s house; Ged soon burned them. A
few months later, in 1981, AIDS was first identified as an epidemic
in the United States. (The disease was originally called GRID by
public health officials: Gay Related Immune Deficiency. Pres-
sure from the homosexual community resulted in a change of its
name: Acquired Immune Deficiency Syndrome. Ah, but just
how is it acquired? AIDS is the nation’s only politically protected
disease. Judgment is coming.)
