ESCHATOLOGY AND BIBLICAL LAW 129

a person can know the goad, but still do evil (Rom.
7).

What Van Til never openly admits is this:
neither is reprobation by knowledge, including self-
knowledge. It is not simply that evil men know the
good but refuse to do it; it is that they know the bad,
but it is not bad enough for them.

Evil's Lever: Good

Covenant-breakers must do good externally in
order to increase their ability to do evil. They need
to use the lever of God’s law in order to increase their
influence. These rebels will not be able to act consis-
tently with their own epistemological presupposi-
tions and still be able to exercise power. They want
power more than they want philosophical consistency.
This is especially true of Western covenant-breakers
who live in the shadow of Christian dominion theol-
ogy. In short, thep restrain the working out of the implica-
tions of their own epistemological self-consctousness. Believ-
ers in randomness, chaos, and meaningless, the
power-seekers nevertheless choose structure, disci-
pline, and the rhetoric of ultimate victory.

Ifa modern investigator would like to see as fully
consistent a pagan culture as one might imagine, he
could visit the African tribe, the Ik (“eek”), Colin
Turnbull did, and his book, The Mountain People
(1973), is a classic. He found almost total rebellion
against law—family law, civic law, all law. Yet he
also found a totally impotent, beaten tribal people
who were rapidly becoming extinct. They were
harmless to the West because they were more self-
consistent that the West’s satanists.
