106 poMMNION AND COMMON GRACE

of common grace and God’s favor, led him to reject
the most fundamental thesis of his whole academic
career: the primacy of the ethical. He focused almost all
of his attention on the epistemological issues relating
to continuity and discontinuity when discussing
common grace, rather than focusing on the eschato-
logical and ethical issues.

I want to make my case against Van Til’s view of
common grace as clear as I can. I am arguing that
Van Til confused the fundamental category of com-
mon grace—historic continuity—with a philosephi-
cal category, epistemological continuity (“What does
man know, and how can he know it?”), He devoted
his common grace book to the problem of knowledge
in history and God’s judgment rather than the prob-
lem of ethics in history and God's judgment. He ig-
nored biblical law. He was long on Plato and short
on Moses. He took the Socratic heresy of salvation
by knowledge—“If 2 man knows the good, he will do
the good” and reversed it to mean reprobation by
knowledge: “If he knows the evil, he will do the evil.”
Here is how he argued:

1, Common grace implies the favor of God
to the unregenerate.

2. All men become more epistemologically
self-conscious over time (meaning such things
as God, man, law, and time).

3. [Implied but never stated] Epistemologi-
cal self-consciousness logically involves ethical
self-consciousness.

4. Both Christians and reprobates will act
