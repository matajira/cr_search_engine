EPISTEMOLOGICAL SELF-CONSCIOUSNESS AND COOPERATION 195

some of the principles of applied biblical law and
conforms himself to part of the work of the law that is
written on his heart. But on first principles, he can-
not agree. And even near the end of time, when men
may-confess the existence of one God and tremble at
the thought, they will not submit their egos to that
God. They will fight to the death—to the second
death—to deny the claims that the God of the Bible
has over every part of their being.

Thus, there can be cooperation in the subduing
of the earth. But Christians must set forth the strat-
egy and the tactics. The unregenerate man will be
like a paid consultant; he will provide his talents, but
the Lord will build the culture.

Common Grace vs. Common Ground

We must not argue from common grace to com-
mon ground philosophical principles, such as the
hypothetical “principle of non-contradiction” or an
equally hypothetical “natural law.” As common grace
increases over time, there will be greater and greater
separation ethically in the hearts of men. With the in-
crease of common grace, we come closer to that final
rebellion in all its satanic might. Common grace com-
bines the efforts of men in the subduing of the earth,
but Christians work for the glory of God openly, while
the unregenerate work (officially) for the glory of man
or the glory of Satan. They do, in fact, work to the
glory of God, for on that last day every knee shall bow
to Him (Phil, 2:10), The wealth of the wicked is laid up
for the just (Prov. 13:22b). But ethically speaking, they
are not self-consciously working for the glory of God.
