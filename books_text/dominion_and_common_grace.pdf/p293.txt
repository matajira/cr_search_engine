WARFIELD'S VISION OF VICTORY: LOST AND FOUND 277

was even more outspoken (with less justification)
than McClain, for he wrote 14 years later: “No self-
respecting scholar who looks at the world conditions
and at the accelerating decline of Christian influence
today is a ‘postmillenialist.’”49 The great irony here
was that much of the declining influence of Christi-
anity, 1870-1970, was the product of dispensational
theology’s implied doctrinal justification of cultural
impotence and retreat. For a century, dispensation-
alism and amillennialism combined to remove the
hope of earthly success from Christians. Then, hav-
ing castrated the sheep, they explained the unfruit-
fulness of the sheep on God's timetable. Gad suppos-
edly has decided not to bring to earth in history a vis-
ible manifestation of His kingdom prior to Jesus’
second coming. Therefore, cultural unfruitfulness is
to become the Christian way of life. God has called
us to earthly defeat. We were born to lose.
Postmillennialism was not dead, as they supposed;
it was only hibernating. Like a newly awakened (and
hungry) grizzly, the postmillennialist movement has
come out of its long winter sleep. This time, how-
ever, it is armed more securely than it was in War-
field’s day. It has a vision of victory, which was War-
field’s vision. It also has a tool of dominion, biblical
law, as well as a new self-confidence based on a bet-
ter understanding of human reason: presupposi-
tional apologetics. The presuppositional apologetic
methodology of Van Til is what Rushdoony set forth

19. Lindsey, The Late, Great Planet Earth (Grand Rapids, Mich-
igan: Zondervan, 1970), p. 176.
