* 70 DOMINION AND COMMON GRACE

tares and wheat to work out their respective destinies
with fear and trembling. This freedom can safely be
granted ethical rebels because of the greater productivity of
the righteous. The wealth of the unrighteous is laid up for
the just (Prov. 13:22b). In confidence, Christians need not
Sear the peaceful, competitive efforts of our ethical opponents.

In contrast, our opponents have every reason to
fear us—not because we are tyrants, but because the
world is structured and. governed by God in such a
way as to produce historical victory for His law-abid-
ing people. We get richer, wiser, and culturally dom-
inant when we are faithful to God’s law (Deut.
28:1-14); our opponents get poorer, more foolish, and
culturally irrelevant when they violate the terms of
the covenant (Deut. 28:15-68). This disturbs them.
They do not want a “fair fight,” meaning open com-
petition, They want control by the State and then
control over the State. They want power, for they
cannot achieve long-term dominion. They worship
the power religion.

Too many modern Christians worship in the tab-
ernacles of the escape religion, the other alternative
to the dominion religion that is required by the
Bible.* All they want is to be left alone by the God-
haters. The best way to achieve this goal, they erron-
eously believe, is to avoid confrontation with unbe-
lievers, especially political confrontation. They there-
fore remove themselves from politics.

They also feel compelled to justify their retreat
from responsibility by affirming the existence of a

4. Gary North, Moses and Pharaoh, Introduction.
