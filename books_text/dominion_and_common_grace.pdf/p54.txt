38 DOMINION AND COMMON GRACE

as summarized by Van Til. Perhaps Schilder made
some technical philosophical errors. He is dead, and
the 1924 Synod is not much healthier. So let us deal
with Van Til, whose arguments are still alive.

He has two ways to escape this conclusion that
the 1924 Synod’s statement leads to the conclusion
that God shows favor to Satan. First, he could aban-
don the Christian Reformed Church's first point,
and stop speaking of the common grace of God as
God’s favor. This he refuses to do. Second, he could
assert that there is a fundamental difference between
God’s common grace to mankind in general, and His
extension of time, knowledge, law, power, etc. to
Satan. In principle, he takes this second approach,
but never clearly and never with a detailed explana-
tion, for the exegetical and logical means of making this dis-
tinction between gifts to mankind and the same gifts to Satan
do not exist.

Moking Indistinct Distinctions

Van Til cannot make the distinction by an appeal
to the image of God in man, which Satan does not
possess. Van Til quite correctly argues that the im-
age of God in man is essential to the very being of
man, and it never departs, even in eternity: “. . . as
a creature made in God’s image, man’s constitution
as a rational and moral being has not been destroyed.
The separation from God on the part of the sinner is
ethical. . . . Even the lost in the hereafter have not
lost the power of rational and moral determination.
They must have this image in order to be aware of
