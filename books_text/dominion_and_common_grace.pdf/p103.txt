‘VAN TILS VERSION OF COMMON GRACE 87

problem. What seems obvious to a postmillennialist
is not obvious to the amillennialist. Meredith Kline
read my original essay (as we shall see shortly) and
did not even perceive its thesis. He got its argument
exactly backwards.

Van Til’s argument is the generally accepted one
in Reformed circles. His is the standard statement of
the common grace position. Yet as the reader should
grasp by now, it is deeply flawed. It begins with false
assumptions: (1) that common grace implies common
favor; (2) that this common grace-favor is reduced
over time; (3) that this loss of favor necessarily tears
down the foundations of civic righteousness within
the general culture; (4) that the amillennial vision of
the future is accurate. Thus, he concludes that the
process of differentiation is leading to the impotence
of Christians in every sphere of life, and that we can
be thankful for having lived in the period of “earlier”
grace, meaning greater common grace.*

Multiplying the Confusion
Van Til’s view of common grace as prior grace is
implicitly opposed to the postmillennialism of R. J.

4. In the late 1960's, I wrote to Van Til and asked him how he
could reconcile his view of the decline of common grace with his
colleague John Murray's postmillennial interpretation of Rom-
ans 11. He wrote back and told me he hadn't really thought about
it. Since he never subsequently commented on the problem in
print, and since he never wrote me a letter clarifying his posi-
tion, I can only assume that (1) he thought Murray was wrong,
but did not want to say so in print, or (2) he thought the question
was irrelevant, or (3) he just never thought about it again. T sus-
pect the third explanation is the most likely one.
