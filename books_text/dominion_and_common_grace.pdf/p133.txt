ESCHATOLOGY AND BIBLICAL LAW 117

Van Til argues that it is the reprobate who will
be nearly victorious in history, not the church. Only
at the end of time do the covenant-breakers face the
fact of defeat. Van Til writes: “But when all the rep-
robate are epistemologically self-conscious, the crack
of doom has come. The fully self-conscious repro-
bate will do all he can in every dimension to destroy
the people of God.”? Yet Van Til has written in
another place that the rebel against God is like a lit-
tle child who has to sit on his father’s lap in order to
slap his face. How can unbelievers try to slap God’s
face by slapping God's people if they are not meta-
phorically sitting on His lap? How can they get suffi-
cient power to injure God’s church if they have de-
nied everything God teaches about how to gain and
retain power—conforming to His external laws?

What, then, can Van Til have meant by his con-
cept of increasing epistemological self-consciousness?
Does this mean that sinners grow more consistent
with their God-denying, law-denying chaos philoso-
phy? This seems to be what Van Til has in mind—
rebellion leading to a reduction of common grace.
But then how do these rebels gain power to do their
evil work?

As the wheat and tares grow to maturity, the
amillennialist argues, the tares become stronger and
stronger culturally, while the wheat becomes weaker
and weaker. Consider what is being said. As Chris-
tians work out their own salvation with fear and
trembling, improving their creeds, improving their

9. Common Grace, in Common Grace and the Gospel, p. 85.
