THE FAVOR OF GOD 37

presses the fact that God loves all His crea-
tures. And as for the idea that God loves all
creatureliness as such, including the creatureli-
ness of the devil, this is, we believe, intelligible
only if we use it as a limiting concept.?

A limiting concept? Shades of Immanuel Kant. If
this is what the Synod “meant to teach,” then it
should have waited to say what it meant in clear lan-
guage that normal God-fearing people can under-
stand, rather than rushing a poorly worded state-
ment through the bureaucracy, and driving out a
man of the stature of Herman Hoeksema and thou-
sands of his followers.

When you face both Herman Hoeksema and
Klaas Schilder in theological debate, you had better
have your arguments ready. In this instance, Van
Til didn’t have them ready. Van Til is quibbling—
quibbling desperately. The fact is, the Synod was
wrong, and all the “limiting concepts” in the world
will not make what it said correct. God gives gifts to
Satan. God shows no favor to Satan. It does not take
a Ph.D. in philosophy or a Th.D. in theology to
make the obvious conclusion. God also gives gifts to
the non-elect, covenanted disciples of Satan. God
also shows no favor to the non-elect, covenanted fol-
lowers of Satan.

I do not want to bury myself or the reader in the
subtleties and qualifications of Schilder’s argument,

9. Idem.
