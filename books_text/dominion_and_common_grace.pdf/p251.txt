THEINSIDEMAN 235

6. Unbelievers learn from apostates con-
cerning the techniques of dominion, which
then become power religions.

7. The apostate become change agents in
Satan’s kingdom.

8. Examples are Cain, Ham, Esau, Ab-
salom, Ahithopel, false priests, Judas, and the
Jews of Jesus’ day.

9. On the last day, Satan will recruit his
troops from inside the church.

10. Historically, some of the most effective
opponents of Christianity have been former
Christians.

11. Examples are Rousseau, Robespierre,
Weishaupt, Marx, Engels, and Stalin.

12. Communism steals Christianity’s four
points of civilization building: providence,
earthly optimism, law as a tool of dominion,
and the self-attesting revelation of God.

13. Islam also steals these same four points.

14. Ancient cultures were in contact with
Israel.

15. The apostate serves as a pseudo-messiah.
