PREFACE

‘And when the thousand years are expired, Satan
shall be loosed out of his prison, and shall go out to
deceive the nations which are in the four quarters of the
earth, Gog and Magog, to gather them together to bat-
tle: the number of whom is as the sand of the sea. And
they went up on the breadth of the earth, and compassed
the camp of the saints about, and the beloved city: and
Sire came down from God out of heaven, and devoured
them (Rev. 20:8-9),

As you probably know, Christians disagree about
the doctrine of “the last things,” called eschatology
{“eskaTOLogy”]. I firmly believe that conservative
Protestants in the United States are about to get into
the biggest theological shouting match of this cen-
tury over the question of eschatology.

But there is one point that 99.9% of all Bible-
believing Christians agree on: these verses in the
Book of Revelation refer to the events immediately
preceding the final judgment. No denomination or
