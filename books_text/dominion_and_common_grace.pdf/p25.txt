INTRODUCTION 9

Do God's gifts increase or decrease over time?

Will the cultural mandate (dominion cove-
nant) of Genesis 1:28 be fulfilled?

This little book is my attempt to provide prelimi-
nary answers to these questions.

Challenging Van Til

This book is basically’ a refutation of Prof. Cor-
neljus Van Til’s book, Common Grace and the Gospel, a
compilation of his essays on common grace. It is
without question the worst book he ever wrote. It is
also one of the most confusing books he ever wrote,
given the relative simplicity of the topic. It was not
as though he was trying to analyze and refute the ar-
cane mental meanderings of some dead German
theologian. It is possible to write a clear book on
common grace.

It is not that Van Til’s book is not filled with
many important insights into many philosophical
and theological problems. The trouble is, these in-
sights are found in any of a dozen other of his books,
The vast bulk of these insights really did not belong
in Common Grace and the Gospel. If he had removed
them, he would have spared us all a Jot of time and
trouble, not to mention a lot of extra paper—and it
possibly would have spared us several of his mis-
takes. But probably not. Van Til has referred to
himself as a stubborn Dutchman.® He clings to his

6. William White, Jr., Van Til: Defender of the Faith (Nashville,
Tennessee: Thomas Nelson, 1979), p. 89.
