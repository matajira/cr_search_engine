84 DOMINION AND COMMON GRACE

of common grace in history became the worst disas-
ter in world history. It took the special grace (yet also
common grace) of the ark to keep history going.

Van Til’s view has common grace receding, and
then the judgment hits. This is not what the Bible
presents concerning common grace in history. Both
common grace and special grace to Noah increased,
but only common grace increased for unbelievers.
God had Noah construct an ark that would allow God
to remove Noah from the midst of the unrighteous.
When it was completed, God utterly removed com-
mon grace from the world outside the ark. Not only the
crumbs falling from God's table were removed, the
table itself fell on top of them. Or to put it more pre-
cisely, the water table rose to cover them, They were
baptized; first by sprinkling, and then by immersion.

Van Til sees continuity in the form of a progres-
sive removal of common grace, with the final judg-
ment culminating this steady continual process. He
specifically says that the judgment is the catching up
to them of their previous spiritually declining condi-
tion. (But how did they get the power to oppress
Christians, if God’s common grace was being removed
from history?) The discontinuity of judgment is, in
Van Til’s scheme, really simply the culmination of a
long process of declining common grace, Then why
should any reprobate be surprised when judgment
finally comes? Those in Noah's day certainly were.

What the Bible teaches is a different kind of con-
tinuity for the unregenerate: a steady increase in
common grace as a prelude to the discontinuity of
massive judgment.
