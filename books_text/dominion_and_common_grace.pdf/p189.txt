‘SUSTAINING COMMON GRACE 173

lation to them. The churl shall no longer be called liberal
(Isa. 32:5), Law will be respected by unbelievers.
This means that they will turn away from an open,
more consistent worship of the gods of chaos and the
philosophy of ultimate randomness, including evolu-
tionary randomness. They will participate in the ex-
ternal cultural blessings brought to them by the
preaching of the whole counsel of God, including
His law. The earth will be subdued to the glory of
God, including the cultural world. Unbelievers will
fulfill their roles in the achievement of the terms of
the dominion covenant.

This is why a theology that is orthodox must in-
clude a doctrine of common grace that is intimately
related to biblical law. Law does not save men’s
souls, but partial obedience to it does save their bodies and
their culture. Christ is the savior of all, especially those
who are the elect (I Tim. 4:10).

Antinomian Revivalism vs. Reconstruction

The blessings and cultural victory taught by the
Bible (and adequately commented upon by postmil-
lennialists) will not be the products of some form of
pietistic, semi-monastic revivalism. The “merely so-
teriological’ preaching of pietism—the salvation of
souls by special grace—is not sufficient to bring the
victories foretold in the Bible. The whole counsel of
God must and will be preached. This means that the
law of God must and will be preached. The external
blessings will come in response to the covenantal
faithfulness of God’s people. The majority of men
will be converted, at least during some periods of
