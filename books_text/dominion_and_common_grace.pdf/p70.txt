5 DOMINION AND COMMON GRACE

within him of his moral powers so that they can per-
form that which is ‘morally’ though not spiritually
good.”*

But what is the source of the good that evil men
do? It can be no other than God (James 1:17). He is
the source of all good. He restrains men in different
ways, and the effects of this restraint, person to per-
son, demon to demon, can be seen throughout all
eternity. Not favor toward the unregenerate, but
rather perfect justice of law and total respect toward
the law of God on the part of God Himself are the
sources of the good deeds that men who are lost may
accomplish in time and on earth.

The Knowledge of the Law

The work of the law is written on every man’s
heart. There is no escape. No man can plead ignor-
ance (Rom. 2:11-14). But each man’s history does
have meaning, and some men have been given
clearer knowledge than others (Luke 12:47-48).
There is a common knowledge of the law, yet there is
also special knowledge of the law—historically unique
in the life of each man. Each man will be judged by
the deeds that he has done, by every word that he has
uttered (Rom. 2:6; Matt. 12:36). God testifies to His
faithfulness to His word by distinguishing every shade
of evil and good in every man’s life, saved or lost.

Time for the Canaanites
Perhaps a biblical example can clarify these
issues. God gave the people who dwelt in the land of

4. A Letter on Common Grace, ibid., p. 174.
