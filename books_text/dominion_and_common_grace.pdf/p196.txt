180 DOMINION AND COMMON GRACE

rebels, so long as they remain peaceful and
subordinate to biblical law.

10. God's law is the primary manifestation
of common grace.

11. The work of the law in all men’s hearts
testifies to this universal aspect of common
grace.

12. Preaching that ignores God’s law is anti-
nomian and pietistic.

13. Pietism cannot sustain an advancing
Christian culture, for it abandons the tool of
dominion: biblical law.

14. External blessings apart from repent-
ance area prelude to covenantal judgment.
