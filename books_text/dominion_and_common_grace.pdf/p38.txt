‘22 DOMINION AND COMMON GRACE

Satan. But He does shower them with non-favorable
favors, just as He showers Satan with them.

What are some biblical examples of these non-
favorable favors? There is this affirmation: “The
Lord is good to all: and his tender mercies are over
all his works” (Ps. 145:9). The verse preceding this
one tells us that God is compassionate, slow to
anger, and gracious. Romans 2:4 tells us He is long-
suffering. Luke 6:35-36 says:

But love ye your enemies, and do good, and
lend, hoping for nothing again; and your
reward shall be great, and ye shall be the chil-
dren of the Highest: for he is kind unto the
unthankful and to the evil. Be ye therefore mer-
ciful, as your Father also is merciful.

First Timothy 4:10, cited in the introduction, uses
explicit language: “For therefore we both labour and
suffer reproach, because we trust in the living God,
who is the Saviour of all men, specially of those that
believe.” The Greek word here translated as “Saviour”
is transliterated ster: one who saves, heals, protects,
or makes whole. God saves (heals) everyone, especially
those who believe. Unquestionably, the salvation
spoken of is universal—not in the sense of special
grace, so therefore in the sense of common grace.
This is probably the most difficult verse in the Bible
for those who deny universal salvation from hell, yet
who also deny the existence of common grace.?

2. Gary North, “Aren't There Two Kinds of Salvation?”,
Question 75 in North, 75 Bible Questions Your Instructors Pray You
Won't Ask (Tylee, Texas: Spurgeon Press, 1984).
