THE FAVOR OF GoD 25

I would argue even more concretely that if there
were no specific gifts to specific individuals, they could
not develop their own historical destinies. We must
be careful in our language. We must not call these
specific gifts specific or special grace, for special
grace is redemptive grace, meaning eternally saving
grace. This form of grace is given only to God’s elect,
“According as he hath chosen us in him before the
foundation of the world” (Eph. 1:4a). I argue that we
must explain these specific gifts in history as manifesta-
tions of God’s common grace throughout history. Common
grace is therefore a form of long-term (eternal) curse
to the rebellious, and a long-term (eternal) blessing
to the righteous.

The sun shines and the rain falls on all men. This
is a manifestation of the common grace of God. But
Jesus was not simply supplying us with a common-
sense theory of the weather. Meteorology was not the
central focus of His concern. He was making an eth-
ical and judicial point: “But I say unto you, Love your
enemies, bless them that curse you, do good to them
that hate you, and pray for them which despitefully
use you, and persecute you; That ye may be the chil-
dren of your Father which is in heaven: for he
maketh his sun to rise on the evil and on the good,
and sendeth rain on the just and on the unjust”
(Matthew 5:44-45),

What was His point? The common blessings of the
weather point to the common law of God. God's blessings
must always be seen in terms of God’s general cove-
nant with mankind, and this covenant always in-
