XVI DOMINION AND COMMON GRACE

questions is what this book is all about. The correct
answers come when we gain a correct understanding
of the much-neglected doctrine of common grace.

The reader should understand in advance that
this book is not intended to present the exegetical
case for postmillennialism. I no more try to build the
case for postmillennialism here than Van Til tries to
build the case for amillennialism in Common Grace and
the Gospel. 1 simply assume it, and then get on with
the business at hand. This is an exercise in apologet-
ics, not systematics. David Chilton’s Paradise Restored
and Days of Vengeance have presented the case for
postmillennialism better than I could or any other
theologian ever has. Any critic who thinks that he
will score cosmic Brownie points by saying, “But
North doesn’t prove his eschatology” should get on
with Ais business at hand, namely, writing a defini-
tive critique of Chilton’s eschatology books, That
project will keep him busy for a few years. (Further-
more, unless he is very, very bright, and very, very
gifted stylistically, it will also end his career as a
critic when he finally gets it into print, if he can get it
into print.)
