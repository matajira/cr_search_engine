86 DOMINION AND COMMON GRACE

fact that we live in the earlier, rather than in the
later, stage of history. And such influence on
the public situation as we can effect, whether in
society or in state, presupposes this undifferen-
tiated stage of development.?

Consider the implications of what Van Til is say-
ing. History is an earthly threat to Christian man, Why?
Van Til’s amillennial-based argument is that com-
mon grace is earlier grace. Common grace declines
over time. Why? Because God’s attitude of favor
declines over time with respect to the unregenerate.
With the decline of God’s favor, the other benefits of
common grace are lost. Evil men become more thor-
oughly evil.

Then how can they win in history? If common
grace gives them law, knowledge, power, and life,
and God steadily removes common grace from
them, how are they able to win?

This incredibly simple question never appeared
in print until I published my original essay in late
1976. As far as I know, no one before my essay ever
asked any defender of the common grace doctrine
this obvious question. This gives you some indica-
tion of how people’s eschatological presuppositions
blind them to the obvious. The reason why nobody
asked the question is that until the 1960's, there were
virtually no postmillennialists around who had even
read the literature on common grace, and the only
one who had, R. J. Rushdoony, did not spot the

3. Van Til, Common Grace, in Common Grace and the Gospel, p. 85.
