142 DOMINION AND COMMON GRACE

Will the progressive manifestation of the fruits of
obeying biblical law also be strictly internal? If so,
then what has happened to the positive feedback
aspect of covenant law? What has happened to em-
powering by the Holy Spirit?

I would argue that the greater empowering by
the Holy Spirit for God’s people to obey and enforce
biblical law is what invalidates the implicit amillen-
nialist position regarding the ineffectiveness of bibli-
cal law in New Testament times. If Christians obey
it, then the positive feedback process is inevitable; it
is part of the theonomic aspect of the creation: “from
victory unto victory.” If some segments of the church
refuse to obey it, then those segments will eventually
lose influence, money, and power. Their place will
be taken by those Christian churches that obey God’s
laws, and that will therefore experience the cove-
nant’s external blessings. These churches will spread
the gospel more effectively as a result. This is the
positive feedback aspect of biblical law.

Kline attacked both of Bahnsen’s doctrines— bib-
lical law and postmillennialism—in his critique of
Theonomy,° but Bahnsen judiciously responded to
Kline’s criticisms of his postmillennial eschatology
only in an “addendum,” stating explicitly that he did
not regard this aspect of Kline’s critique as logically
relevant to the topic of theonomy.*! But Kline was

30. Kline, op. cit.

31. Greg L. Bahnsen, “M. G. Kline on Theonomic Politics:
An Evaluation of His Reply,” Journal of Christian Reconstruction, VI
(Winter 1979-80), “Addendum: Kline's Critique of Postmillen-
nialism.” Bahnsen writes: “Although Ktine’s polemic against
