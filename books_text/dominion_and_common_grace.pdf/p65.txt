GOD'S RESTRAINING HAND VS. TOTAL DEPRAVITY 49

moves people from the protection of His law, which
is designed to restrain evil (Rom. 13:1-7).

Biblical Law Is a Means of Grace

Biblical law is a means of grace: common grace to
those who are perishing, special grace to those who
are regenerate. We all benefit from God's extension
of blessings to us when we are externally faithful to
the external terms of the covenant. To use the exam-
ple of venereal disease again, when most people are
monogamous, they are protected from the spread of
these killer diseases. Ethical rebels who in other
areas of their lives disobey biblical law at least re-
main free from this particular scourge. Or to use the
example in Matthew 5, when God sends good
weather, sinners enjoy it too.

Biblical law is also a form of curse: special curse to
those who are perishing, common curse to those who
are regenerate. We are all under the legal require-
ments of God’s covenant as men, and because of the
curse on the creation, we suffer the zemporal burdens
of Adam’s transgression. The whole world labors
under this curse (Rom. 8:18-23). Nevertheless, “all
things work together for good to them that love God,
to them who are the called according to his purpose”
(Rom. 8:28). The common curse on nature is not a
special curse on God’s people.

As men, we are all under the law of the covenant
and the restraint of its law, both physical and per-
sonal law, and we can use this knowledge of law
either to bring us external blessings through obe-
dience or to rebel and bring destruction. But we
