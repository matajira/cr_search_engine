122 DOMINION AND COMMON GRACE

respects His covenantal law-order, and that Deuter-
onomy’s teaching about the positive feedback proc-
ess of covenantal law is invalid in New Testament
times. The only way for the amillennialist to avoid the
charge of antinomianism and still remain an amillennialist is
for him to (1) abandon the concept of increasing
epistemological self-consciousness, or else (2) adopt
the doctrine of an historically impotent gospel.

Here is my reasoning. The amillennialist who in-
sists that he is not an antinomian must proclaim the
legitimacy and power of biblical law. It is not enough
to claim that biblical law is ethically correct. He
must argue that God empowers the Christian to
obey it, and that this obedience produces positive
feedback. This is what the theonomic postmillennial-
ist argues, But the amillennialist is not postmillen-
nial, so he faces two very difficult questions: “If the
law is both legitimate and efficacious in history, then
why do Christians lose to the covenant-breakers? If
this defeat is not due to the failure of God’s law, then
what does fail?” There are two escape routes.

First, if Christians fail to extend the visible mani-
festation of God’s kingdom on earth because they do
not in fact become increasingly epistemologically
self-conscious over time, then their failure is not ne-
cessarily the failure of God’s law. They ignore Gad’s
law because they do not become epistemologically
self-conscious. They refuse to pick up God’s tool of
dominion. This is not the fault of the law. So their
failure must be blamed on their lack of epistemologi-
cal self-consciousness.

Second, if Christians do become increasingly
