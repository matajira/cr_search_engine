248 DOMINION AND COMMON GRACE

time, but only to the extent that they are not fully con-
sistent with their own God-defying presuppositions.

Will the cultural mandate (dominion covenant) of Genesis
4:26 be fulfilled?

There will not be perfect fulfillment in time and
on earth, for there will always be sin prior to the final
judgment. Nevertheless, there will be progressive
fulfillment over time, as men more and more con-
form themselves to Christ’s perfect humanity by
means of His law, as empowered by the Holy Spirit.
God's plan for the ages does not include visible, ex-
ternal, historical defeat for His church at the hands
of Satan’s forces. The death and resurrection of
Christ guaranteed the visible, external, historical
victory of the kingdom of God. Christ will deal with
His enemies as if they were footstools, in time and
on earth. “Then cometh the end, when he shall have
delivered up the kingdom to God, even the Father;
when he shall have put down all rule and all author-
ity and power. For he must reign till he hath put all
enemies under his feet. The last enemy that shall be
destroyed is death” (I Cor. 15:24-26).

We come at last to the two questions that I left
unanswered in the Preface.

“How can a world full of reprobates be con-
sidered a manifestation of the kingdom of God
on earth?”

“How can unbelievers possess so much
power after generations of Christian dominion?”
