30 DOMINION AND COMMON GRACE

tion for the Christian. The law rips up the kingdom
of Satan, just as it serves as the foundation for the
kingdom of God on earth.

Equal Ultimacy, Unequal Effects

Just as the idea of eternal punishment is equally
ultimate in the covenant as the idea of eternal bless-
ing, so is the idea of temporal destruction equally ul-
timate with temporal reconstruction. But there is
this difference: reconstruction is a positive process,
and expands its influence over time. Destruction is
negative, In hell, it may or may not be that men in-
crease in their rebellion throughout eternity, learn-
ing new ways to resent God and curse him. I do not
know. But in the perfect consummation of the New
Heaven and New Earth after resurrection day, men
will learn ever more about God and His creation, as
finite creatures working to understand the infinite.
Redeemed men will heap new praises to God
throughout eternity as they learn more of His glory.
He is infinitely good; there will always be more to
praise. In short, when it comes to the glory of God,
“there’s plenty more where that came from.”

Thus, while election and reprobation, blessing
and cursing, resurrection unto eternal life and resur-
rection unto the second death (Rev. 20:14) are equally
ultimate in principle in the covenant, they are not equally
ulfimate in their respective effects in eternity. The manifes-
tation of God's glory is positive in the post-resurrec-
tion New Heavens and New Earth, and negative in
the eternal lake of fire. The manifestation of God’s
glory is progressive (developing) in a positive feed-
