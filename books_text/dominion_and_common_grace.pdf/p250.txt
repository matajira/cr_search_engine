234 > COMMION AND COMMON GRACE

written in men’s hearts is a sufficient explanation of
many of the parallel myths in ancient societies. But
as time goes on, the increasing epistemological self-
consciousness of men leads to an increasing rivalry
between power-seeking empires and biblical civiliza-
tion. In our day, the chief rivals are unquestionably
epistemological first-cousins of Christianity.

Conclusion

As men seek power without God, they must
abandon the animism of the past and the nihilism of
the escapist present. They must avoid becoming in-
tellectually consistent with their own religious pre-
suppositions. They must instead be infused with a
future-oriented, law-governed, highly disciplined al-
ternative to Christianity. The inside man is the agent
of this infusion.

In summary:

1. Men’s epistemological self-consciousness
increases over time.

2. Christians become more ethically self-
conscious as they become epistemologically
self-conscious.

3. Unbelievers become less ethically self-
conscious as they become more epistemologi-
cally self-conscious.

4, The goal of power is attainable only by
external obedience to fundamental principles of
biblical law.

5. The self-consistency of Christian ethics
and Christian philosophy leads to a spread of
Christianity’s influence.
