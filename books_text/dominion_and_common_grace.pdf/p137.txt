ESCHATOLOGY AND BIBLICAL LAW 121

discussions of natural law. These critics are correct;
theonomists have indeed abandoned Calvin’s sixteenth-
century understanding of natural law. Van Til has
left them with no choice. He destroyed the intellec-
tual case for natural law.

Natural law philosophy was pre-Darwinian hu-
manism’s crucial alternative to both biblical law and
moral chaos. But there is theological progress in life,
both for the saved and the lost. The more consistent
humanists and the Vantillians have recognized that
Darwin destroyed natural law theology and philoso-
phy.” The Vantillians have also recognized that Van
Til has destroyed Princeton Seminary’s “common-
sense realism,” a disastrous eighteenth-and nineteenth-
century attempt to baptize Scottish Enlightenment
philosophy and rear the illegitimate infant in a
Christian home." Something must be built on the
ruins. Theonomists have an answer: a type of cove-
nant theology that acknowledges biblical law as the
source of Christian ethics and therefore of Christian
dominion,

Amillennialism’s Grim Choices

I am faced with an unpleasant conclusion: the
amuillennialist version of the common grace doctrine adopts
either antinomianism, or a doctrine of an historically impo-
tent gospel, or both. It argues that God no longer

10. Gary North, The Dominion Covenant: Genesis (Tyler, Texas:
Institute for Christian Economics, 1982), Appendix A: “From
Cosmic Purposelessness to Humanistic Sovereignty”

it, Mark A. Noll (ed.), The Princeton Theology, 1812-1921
(Grand Rapids, Michigan: Baker Book House, 1983).
