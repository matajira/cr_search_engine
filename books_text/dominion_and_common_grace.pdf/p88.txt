‘72 DOMINION AND COMMON GRACE

nature (Gen. 1:28; 9:1-7). They are to manage the ©
field (world). They are to work out their salvation or
damnation in fear and trembling (Phil. 2:12b),

As good people and evil people work out their
God-ordained destinies, what kind of development
can be expected? Who prospers most, the saved or
the lost? Who becomes dominant in history? Chris-
tians do. They have the tool of dominion, biblical
law, and they have the Holy Spirit.

Do they become dominant only after the Rap-
ture? No, they become dominant before the Rapture.
The Rapture takes place simultaneously with the
final judgment. Remember, the parable tells us that
there will be no premature separation of wheat from
tares; that happens only once, at judgment day.

Or will Christians become dominant only after
the final judgment, when God establishes the fully
consummated New Heaven and New Earth? No, they
will become dominant before the final judgment,
since God has already established the New Heavens
and the New Earth at Christ’s resurrection. This is
why Isaiah 65:17-20 speaks of the New Heavens and
the New Earth as a place where sinners still operate,
indicating a pre-final-judgment kingdom.®

How will Christians achieve dominion? By faith-
ful service to God. By what standard are Christians
to evaluate faithful service? By biblical law. They are
to become earthly judges—self-judges first, and then
judges in every area of life.

6. David Chilton’s Days of Vengeance discusses Revelation 21
and 22 in terms of the pre-consummation manifestation of the
New Heavens and the New Earth.
