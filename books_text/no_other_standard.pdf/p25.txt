Introduction to the Debate 9

My “Foreword” to Gary DeMar’s The Debate Over Christian Recon-
struction offers a sincere moral admonition against the continuing
slurs and misrepresentations made by certain critics of the theonomic
position.°

A variety of shorter letters to editors also aimed to correct the
record, especially over erroneous representations of the theonomic
viewpoint!

A Brief Synopsis of Theonomy”

Any conception of the role of civil government that claims to
be distinctively “Christian? must be explicitly justified by the
teaching of God’s revealed word.”? Anything else reflects what the
unbelieving world in rebellion against God may imagine on its
own. If we are to be Christ’s disciples, even in the political realm,
it is prerequisite that we abide in His liberating word (John 8:31).
In every walk of life, a criterion of our love for Christ or lack
thereof is whether we keep the Lord’s words (John 14:23-24)

20, DeMar, Debate, pp. ix-xvil.

21. For instance, see: “About Law,” Presbyterian Cuardian, vol. 47, na. 3 (March,
1978), pp. 9-10; response to a review by Walter Chantry in The Banner of Truth, issue
178 (July, 1978), pp. 30-31; “Strong Complaint” in Christianity Today, vol. 25, no. 2
(Dec, 11, 1981), p. 8; “Puritans Were Theonomists” in Naw Horizons, vol. 6, no. |
(Jan., 1985), p. 2.

22. The opening portion of this short summary of theonomic principles is taken
from the first part of my position paper in God and Politics: Four Views on the Reformation
af Civil Government, pp. 21-25. The summary has appeared in a few places, sometimes
in shorter, somctimes longer, form, Gf. By This Standard: The Authority of God's Law
Today (Tyler, Texas: Institute for Christian Economics, 1985), pp. 345-348; “Ten
Theses of Theonomy” in journey, vol. 1, no. 6 (NovDec., 1986), p. 8; “Christ and
the Role of Civil Government” in Transjormation, vol. 5, no. 2 (April-June, 1988), pp.
24M The annotation upon the enumerated principles is taken from chapter 31 in By
This Standard,

23. God’s word is of course found not only in special revelation (Ps. 19: 7-14),
but also in natural revelation (vv. 1-6). Aud to whatever degree unbelievers do civic
good, and whenever there has been anything like a reasonably just government in
non-Christian lands, it is to be credited to common grace and natural revelation,
Scripture is nonetheless our final authority. In a fallen world where natural revelation
is suppressed in unrighteousness (Rom, 1:18, 21), special revelation is needed to
check, confirm, and correct whatever is claimed for the content of natural revelation,
:Moreover, there are no moral norms given in natural revelation which are missing
from special revelation (2 Tim. 3:16-17}; indeed, the content and benefit of special
revelation exceeds that of natural revelation (cf. Rom. 3:1-2).
