Poythress as a Theonamist 299

unhelpful or emotive descriptions. He doesn’t want a “slavish”
or “wooden” or “straight-line” or “straightforward” application
of the law (e.g., pp. 225, 343, 348, 398). But since he does not offer
any clear definition or identifying marks of such “straight-line”
application (etc.), we are left with a mere slogan. Theonomy
repudiates a “wooden” or “straight-line” application of the Old
Testament law today, just as much as Poythress does. Neverthe-
less, theonomists cannot readily endorse the practical outworking of
Poythress’ approach to interpreting the law today because in
crucial ways it employs unreliable reasoning.

8. Unreliable Reasoning

A methodological flaw runs through the new Poythress book
from one end to the other, undermining much of what Poythress
wishes to say about the law. The fundamental problem is not with
Poythress’ general endorsement of the Old Testament Jaw or his
positive attitude toward using it today, nor with each and every
conclusion he draws or insight he suggests (many, many of which
are healthy and commendable), but rather with his hermeneu-
tic ~ the way in which he handles the law. Upon analysis, the manner
in which Poythress reaches his conclusions is unreliable and crippled by
arbitrariness — which makes it both ethically dangerous and theo-
logically unusable. If we are concerned for clarity, objectivity,
consistency and predictability in our treatment of the Scripture,
we must disapprove of his overall procedure for interpreting the
law. Having examined and compared the various lines of reason-
ing which Poythress uses from case to case, we must object to the
lack of systematic care in basic theological method which is dis-
played in the book for determining how — and if— the law should
be applied by civil magistrates in the modern world.

Let me state the methodological problem concisely: when your
principles are so vague and are used so dialectically that you can prove
anything by means of them (depending upon your predilection), then those
principles are as good as “proving” nothing.

Bewildering, Open-ended Vagueness
Notice, as we begin our critical analysis, that Poythress has a
