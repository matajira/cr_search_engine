118 NO OTHER STANDARD

Here we find what Kline thinks is so very obvious to every
covenant child, but which theonomists completely obscure and miss
in reading the Bible. Isracl was a unique nation, being: (1) a type
of Christ’s redemptive kingdom and (2) a holy nation set apart
by God's electing love. Kline specifically says that theonomists
deny that Israel is a type of the redemptive kingdom of Christ,
do not perceive the typological nature of the Old Testament
theocratic kingdom, say that Israel as a kingdom was just another
civil government of the world, and deny Israel’s distinctive holi-
ness as a kingdom set apart by a special redemptive covenant
unto the Lord.

This argument by Kline is a rea! scholarly lapse on his part.
Those familiar with the thconomic position will find it hard to
believe that he would actually publish something like this against
it. It is an outrageous misrepresentation of my theological posi-
tion. The reader will not find one sentence to support Kline’s
portrayal in all of Theonomy. Kline has shot his largest theological
canon at a straw man. Theonomy nowhere asserts an equivalency
between Isracl’s king and all other civil magistrates. It nowhere
loses sight of the distinction between the kingdom of God and the
kingdom of the world. Nor can Dr. Kline demonstrate that any-
thing which I have taught logically implies the views which he
falsely attributes to theonomic ethics.

Kline wants to argue against the theonomic responsibility of
the civil magistrate on the basis of typology, categorizing Old
Testament political laws with the ceremonial laws, and the intru-
sive uniqueness of the theocracy. However, cach one of these
argumentative moves has already been refuted in Theonomy
- without any rescuing response from him. Moreover, an elemen-
tary logical fallacy lics at the heart of Kline’s attempted argument
against theonomic politics.'’ Klinc wants to emphasize the discon-
tinuities between Israel and the nation, Israel and the New Testa-
ment kingdom. Theonomic politics points out that there is, never-

13, The same logical fallacy is committed in a discussion of the “parts of the
covenant” by David Neilands, “Theonomy and the Civil Magistrates” (privately
produced and distributed, in connection with a study committee on theonomy in che
Presbytery of Northern California, O.P.G., 1982).
