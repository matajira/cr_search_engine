142 NO OTHER STANDARD

(1) I cannot decide whether I agree with Robertson’s state-
ment of the difference between church-state relations in the Old
Testament and New Testament, because the statement is quite
ambiguous. It is subject to many (and conflicting) interpretations.
To say that both the church and state in the old covenant were
aspects of one redemptive, covenantal community is simply to
look at Isract, God’s chosen people, from two distinct perspectives
(religious and national). I do not deny that redeemed Israel was
typological of the church of Christ, which is today an international
body unified by religious interests. Type and antitype differ, to
be sure. But if Robcrison’s point about the one covenantal commu-
nity suggests that, unlike today, the covenant church executed
religiously heightened punishment on civil criminals, then he would
obviously be confusing the distinct perspectives already drawn by
his remark. Or if he is suggesting that the kingdom of God, of
which Israel was the anticipation, is now no longer concerned
with matters of civil justice, then he would be introducing a
sacred/secular (or nature/gracc) dichotomy which is foreign to
God’s word. The reorganization of God's people in the New Tes-
tament does not eliminate the Old Testament interest in having
righteousness pervade all departments of life, nor docs it necessar-
ily alter God’s standards of righteousness. So it is not evident
whether we should agree with the remark made in the lecture or
not.

Likewise, when Robertson says that the state of Israel was a
“redemptive” community, I cannot decide what he means. That
all in Israel were spiritually redeemed? That Israel as a national
entity originated in the political redemption from Egypt? That the
functions of the state were redemptive in nature or effect? Or
simply that those who constituted the state were part of a commu-
nity which in itself served the further end of typifying the coming
work of the Redeemer? It makes quite a difference. Similarly, how
are we to understand the claim that the “state” in the Old Testa-
ment was in a redemptive covenantal relation with the Lord?
(The fallacy of reification always looms over such discussions.)
The word ‘state’ can refer to a territory, a people, the governors
of a people, the functions of government, the specific policies of
