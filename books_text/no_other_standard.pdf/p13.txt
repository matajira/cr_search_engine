Publisher’s Preface xiii

They had waited until he was off campus to challenge him.

Professor Norman Geisler, a defender of natural law theory,
listed these criticisms of theonamy: legalism, allegorism, over-
optimism, postmillennialism, politicism, and revelationism (stress-
ing special revelation rather than gencral revelation). It is worth
noting that at that symposium, Geisler handed out an outline in
which he referred to nine published items critical of Christian
Reconstruction. Seven of these were dispensational. The eighth
was Rodney Clapp’s Christianity Today essay (Feb. 20, 1987);* the
ninth was Theonomy: A Reformed Critique, edited by William S.
Barker and W. Robert Godfrey.> This is the book written by the
faculty of Westminster Seminary.

Why isn’t Geisler willing to debate Bahnsen in public? Why
did he wait until Bahnsen was gone to offer his challenge? The
question is not difficult to answer, even for non-philosophers,
Another easy question to answer: If silence is not golden, what
color is it?

Debates Are Won By Arguments

Bahnsen has made his position clear for almost two decades.
He now replies to numerous critics who have not understood his
clear arguments or who have chosen to answer straw men of their
own creation. As Joe Louis said of one fast-footed but ill-fated
opponent in the ring, “He can run, but he can’t hide.” Most of
Bahnsen’s critics cannot even run. One by one, he overtakes them
and disposes of their arguments.

Some of these early criticisms have been cited by recent crit-
ics, notably the faculty of Westminster Seminary. One example
is the unpublished 1980 essay by Paul Fowler, which he wisely
left unpublished. Bahnsen responds to it in this book because it
has been cited favorably by several critics over the years. That a
number of these early critical reviews were quite short is also
significant. It is my view that if the critics had detailed, theologi-

4. My 1987 response is reprinted in my book, Westminster's Conféssion: The Aban-
donment of Van Til’s Legacy (Tyler, Texas: Institute for Christian Economics, 1991),
Appendix B.

5, Grand Rapids, Michigan: Zondervan Academic, 1990.
