100 NO OTHER STANDARD

getical evidence. After all, God strictly forbids us to subtract from
His commandments (Deut. 4:2). If theonomic critics wish to as-
sert that the judicial (civil) laws of the Old Testament have been
repealed, they are duty-bound to demonstrate that God Himself,
the Law-giver, has authorized such a conclusion. Otherwise, this
view must be condemned as arising from human tampering with
God's sovereign prerogatives.

Some critics venture into this dangerous arena and try to base
their repeal of the judicial laws on the distinctive character, func-
tion, or presentation of the decalogue in the Old Testament (e.g.,
its being directly uttered by God from heaven to the people, its
being engraved on stone as the epitome of the covenant). All
such efforts are exegetically and logically flawed. For example,
QO. Palmer Robertson argues that the civil law of Israel was
historically and socially conditioned — which is also true of the
decalogue as we have seen; and this fact did not keep Paul from
making authoritative use of the case laws (e.g., 1 Cor. 9:9-10).
Robertson says that the decalogue is the “core” of the Mosaic
law, but we could as easily say that the love commands are the
“core” of the decalogue (cf. Matt. 22:40) — even though they are
found outside the decalogue! ~ and not for a moment think that
the ten commandments can thereby be dismissed today. Robertson
also suggests that the civil laws of the Old Testament had a
typological or prophetic dimension ~ but the same could be said
for the ten commandments (e.g., the fourth typifying the New
Creation, the fifth typifying the perfect Son who is rewarded with
life forevermore, etc.).!5

Bruce Waltke mistakenly contrasts the judicial law to the ten
commandments, saying the wording of the latter (unlike the for-
mer) is “not restricted to time and place.” In fact, though, God
felt it important to introduce the ten commandments according

14. For instance: Fowler, pp. 39-44; Report of the Committee ta study Theonomy
(presented to Evangel Presbytery, P.C.A., meeting at Gadsden, Alabama, on June
12, 1979), pp. 9-10; Lewis Neilson, God's Law in Christian Ethics (Cherry Hill, New
Jersey: Mack Publishing, 1979), pp. 33, 37; Walter Chantry, God's Righteous Kingdom
(Edinburgh: Banner of Truth Trust, 1980), pp. 87-88.

15, Tapes: “Analysis of Theonomy.”
