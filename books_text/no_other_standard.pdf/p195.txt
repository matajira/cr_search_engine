Religious Crimes, Religious Toleration 179

does not assign eterna] punishment in the place of an Old Testa-
ment penal picture devoid of eternal jeopardy. Moreover, He-
brews 2 and 10 do not present the substitution of exclusively eternal
punishment for Old Testament civil sanctions. The notion of can-
cellation and restriction and the notion of replacement are missing
in these passages.

To the contrary, what we find is an @ fortiori argument which
builds from a lesser point to a greater one. Hebrews argues that
we need to give “greater heed” today, for if even the (lesser) law
demanded just recompense for offenses, the (greater) gospel will
all the more do so— there will be no escape from God’s wrath
{2:1-3). Hebrews 10:29 makes the @ fortiori thrust of the thought
even plainer, beginning with the words “of how much worse punish-
ment will be thought worthy. . . .” So the Old Testament civil
penalties are not being set aside but rather established by this line
of thought — established as the premised foundation for the justice
and inevitability of eternal punishment for apostates. It is precisely
because those (lesser) civil sanctions ave valid and just that one
must see that the (greater) eternal sanction will be valid and just.
The eternal is not put in place of the civil; it is argued on the basis
of the civil! If the civil sanctions could be mitigated or set aside
in any way, one might perhaps hope that the eternal penalty
might also be avoided; if the civil sanctions were somewhat arbi-
trarily harsh, then perhaps the threat of eternal damnation might
turn out to be likewise overstated. But the author of Hebrews
takes away all such false hopes. God’s penalties are never unjust
or set aside, even in the civil sphere — in every case they specified a
“just recompense” (Heb. 2:2) which any criminal had to endure
“without mercy” (Heb. 10:28). If this is true of God’s civil code,
how much more will it be true of His eternal judgment! It will justly
and without mercy condemn the apostate. So the point in He-
brews builds upon, rather than replaces, the civil sanctions of the
Old Testament. If those civil sanctions could be removed, as Neilson
suggests, then the argument of the author of Hebrews would
actually fall to the ground! Apostates might have some hope after
all.

Tt should also be pointed out that Hebrews 2:2 begins by
