340

127ff, 134, 204f, 210, 218, 226f, 232
Meat in its blood prohibited, 219
Mentally deficient, responsibility of the,

262
Mercy, prohibited in civil punishment,

254, 259
Metaphors misused, 86°
Meticulous observance of law, 276f
Milk prices, 269
Millennial eschatology, 52f
Minors, responsibility of, 262
Mishnah, new, required, 263
Misrepresentation by critics, 6, 9, 14,

22F, 38H, 116ff, 133, 139%, 1494, 171,

193, 201f, 204f, 212, 227, 262, 277
Mixing, prohibited, 237
Modification of law, requires biblical

warrant, 12, 148, 274
Moo, Douglas, 7
Moore, T.M., 191
Moral/ceremonial law distinction, 93ff

see; ceremonial law
Moral law, 94f, 282f
Moral rationale for penalties, 256f
Moral uniqueness of Mosaic law

contested, 114
Mormons, 228
Mortgages, home, 2
Mosaic administrative, distinctive

aspect, 106
Mosaic covenant, gracious nature, 76ff
Mosaic period treated as parenthesis,

125f
Motives, 286
Muether, John, 39, 48
Murder, 159, 196, 216f, 219, 256f, 269
Murray, John, 106, 318

NO OTHER

National covenant argument, 22,
112f, 170

Natural law, 152

Natural revelation, 121, 127, 135, 175,
194
and the decalogue, 125

STANDARD

as the moral standard for state, 206f,
222
relation (o special revelation, 9, 80f,
155ff, 194, 206f
Naturalists, 207
Necessity (antecedent/subsequent), 253
Neilands, David, 25, 118, 153, 156f
Neilson, Lewis, 50, 52, 65, 100, 113,
15, 134, 161ff, 171, 217, 235
Nero, 167
Neutrality
impossible, 192
in civil government, 116
New commandment, 86f
New Covenant (testament)
changes from old covenant
administration, 13
greater emphasis on grace,
inwardness, 173ff, 205, 224, 246
relation to old covenant, 12
confirms Mosaic law, 83
dismisses Mosaic law, 847
new law for new covenant, 83ff
Ninevah, 114, 120, 233
Nixon, Robin, 319
Noahic covenant, 125, 157, 210, 2181
North, Gary, 3, 7, 14, 16, 19, 21, 24,
61, 262

Oaths, 323
Obedience, as gratitude, 12
Old/New covenants, 76ff
Old Covenant (testament)

dismissed, 82

social relevance of, x, 2ff
Overstatement, as literary device, 241
Owen, John, 185f

Passover, 146f
Paul
and penal sanctions, 241f
consistency assumed in his view of
law, 97
morals of, 88f
