126 NO OTHER STANDARD

tions. By what warrant would Zorn preach against “divers weights
and measures” which Moses forbade, but which are not taken
up before or after the Mosaic “parenthesis”? By what warrant
would Zorn discipline a member of his church for bestiality, which
is not explicitly condemned prior to Moses or in the New Testa-
ment itself? What about bribes? Compensation for negligence or
injury? We could go on and on and on, Given Zorn’s espoused
principle that Mosaic stipulations are set aside today (“the whole
economy”), how can he Biblically defend the things we have
mentioned here (and much more)? He is — if consistent — simply
at a loss to present a credible and Biblically-based system of
ethics.

It appears that Zorn has two devices which he might enlist
to avoid the embarrassing bind his rejection of the Mosaic econ-
omy has put him in. (1) He might argue that the kinds of matters
mentioned in the previous paragraph are “creation ordinances”
taught to all men by general revelation (Romans 2:14-15). Or (2)
he might argue that, though they are not explicitly mentioned in
the New Testament, such matters as those referred to above are
simply “incorporated with the moral law” (Decalogue) as part of
the meaning of the ten commandments. What Zorn does not seem
to have grasped, however, is that both of these devices would just as
well justify the theonomic conclusions which Zorn wishes to reject! A
theonomist could argue, for instance, that the death penalty for
rape is (1) taught to all men by general revelation (cf. Romans
1:32), or is (2) part of the meaning of the seventh commandment
(cf. Hebrews 2:2). If Zorn rejects this claim, but insists on the
other hand (for instance) that the distinction between manslaugh-
ter and murder is a matter of general revelation or part of the sixth
commandment, he will need some very strong and exegetically
convincing argumentation to save him from the charge of arbi-
trariness and inconsistency! It does not appear from what he has
written that Zorn has any answer for this problem.

Does the Mosaic Law Apply Simply
to the Church Today?

A few years ago Richard Lewis began a critical discussion of
