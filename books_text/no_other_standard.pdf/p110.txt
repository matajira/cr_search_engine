94 NO OTHER STANDARD

falling into this category is “restorative” — reflecting God’s mercy
by which sinners are restored to Him, nourished in their salvation
(the sacraments), and urged to a separated lifestyle in the midst
of a fallen world.? In Theonomy as well as in By This Standard, the
Biblical necessity of this category distinction within Old Testa-
ment laws was argued on the basis of texts like Hosea 6:6 (“I
desire mercy, not sacrifice”) and Ephesians 2:5 (“the law of com-
mandments contained in ordinances,” which separated Jew from
Gentile).*

The second line of criticism comes from the opposite direction.
It not only recognizes the legitimacy of a ceremonial category of
Old Testament laws, but it wishes to assert a further classification
of laws which, just like the ceremonial laws, are categorically
abrogated under the New Testament — namely, the civil or “judi-
cial” laws of the Mosaic revelation. Therefore, according to this
thinking, the only Old Testament commandments which would
remain binding in the New Covenant would be the “moral law,”
which is allegedly only the ten commandments (Exodus 20:1-17).
Theonomic ethics challenges this understanding of the Old Testa-
ment civil or judicial laws, arguing that they are theologically
distinct from the ceremonial laws, that the moral law cannot be
reduced to the decalogue (its summary), and that the difference
between judicial laws and the moral law which they apply is not
principial but literary in character. Theonomy taught that we need
“to apply the illustrations given in the Old Testament case laws
to changed, modern situations and new social circumstances.”
For instance, with respect to the requirement of a rooftop railing:
“Thus the underlying principle (of which the case law was a
particular illustration) of safety precautions has abiding ethical

3. John Frame rehearses the ambiguities of the “ceremonial” rubric and reminds
us, quite correctly, that laws must be placed into this calegory after exegesis, not as
an a priori approach to their exegesis in “The One, the Many, and Theonomy,”
Theonomy: A Reformed Critique, ed. William §, Barker and W. Robert Godfrey (Grand
Rapids: Zondervan Publishing House, 1990), pp. 91-92. He thinks the “restorative”
label has some advantages.

4, Theonomy in Christian Ethics (Phillipsburg, New Jersey: Presbyterian and Re-
formed Publishing Co., 1977, 1984), chapter 9; By This Standard: The Authority of Gods
Law Teday (Tyler, Texas: Institute for Christian Economics, 3985), pp. 136, 316, etc.
