298 NO OTHER STANDARD

steps toward it, as his book appears to do.

Inherent Differences

Often enough the differences between Poythress and theonomy
are simply matters of emphasis and expression. There are, never-
theless, considerable differences as well in hermeneutical method
and in conclusions reached about the law. He says “the most
significant disputes between myself and theonomy concern . , .
what the law means. . . . Such differences may be far-reaching
in practice” (p. 335). We agree, as Poythress himself acknowl-
edges, that a proper reading and usc of any law requires grasping
its purpose in the light of the Biblical contexts where it is re-
vealed — which means distinguishing general principles from spe-
cialized circumstances (pp. 340-342). “There is no substitute for
careful study and meditation” (p. 342). We both say as much,
We both see our respective approaches to the law as Christ-
centered. Thconomic interpretation does not legalistically inter-
pret the law “in isolation” from its purpose of revealing Christ
{cf. p. $59). Poythress writes that “our discernment grows only
as we know Christ more and more deeply (Ephesians 3:17-19),
for His law reflects His just character. Such is the purpose of my
book” (p. 343). E certainly endorse, then, the purpose of his book.

What I cannot endorse at many points is the actual manner
in which he handles or interprets the law. As Poythress puts it,
we are sometimes separated by our kenneneutical approach and by
our different understanding of Old Testament details (cf. pp. 344,
349). This is not a matter of whether or not we find typology and
a full range of literary devices in Scripture. Poythress himself
quotes me as saying: “The artistic and pedagogical designs inher-
ent in the Scriptures certainly must not be ignored or despised;
however, neither must they be abused by trying to make them say
something which Scripture itself does not say” (p. 352, from
Theonomy, p. 456). It is rather a difference in how we identify those
artistic devices and then theologically apply what Scripture in-
tends by them.

Poythress finds it hard to express what is objectionable in
theonomic interpretation of the law, and he resorts finally to
