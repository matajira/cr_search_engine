The Penal Code as an Instrument of the Covenant Community 239

every single kind of sin in whatever degree — including promise-
breaking, slander, ungracious lending, etc. But that simply is
contrary to fact, in which case Johnson’s explanation is misleading
and disproven. It is also arbitrary or inconsistent. He himself says
that “certain” of the penal sanctions served this purpose. But
since all sin renders men unfit for God’s presence, it should have
been the case (on Johnson’s hypothesis) that aff sins called for the
sanction of “cutting off.” And remember that this kind of penal
sanction was said to be necessitated (“entailed”) by the high
privilege enjoyed by Israel as the covenant people of God.

“Cutting Off”

Johnson rightly identifies the church as the covenant people
of God today. But zow Johnson denies the necessity of this entail-
ment— no longer does the high privilege necessitate the penalties
prescribed by Moses. This is arbitrary. According to Biblical
teaching, the people under the New Covenant have an even greater
privilege and even greater responsibility. The “entailed” sanction
should then be at least, if not more, demanding today than it was
previously.*

A further arbitrariness is detected in the way Johnson treats
the punishment of “cutting off,” which he has said was used (only)
by the covenant community as such — in which case such penaliz-
ing is the province only of the church today. But he understands
this penalty in the Old Testament to have been part of “physical
force and penalties [used] as a means to maintain the commu-
nity’s purity and integrity.” Therefore, his reasoning would, if
sound, entail the conclusion that the church must use “physical
cutting off” as a punishment today. But of course Johnson rejects
this inference from his own premises (since the New Testament
contradicts it). The church uses non-physical cutting off to maintain
its purity and integrity (as I agree). The problem, then, is that

4. Those who tend to base their reasoning in typological argument must be
careful just here that they do not respond thoughtlessly: “Well, of course, the New
Covenant sanction is more demanding because it involves eternal cutting off fom
God.” Such an answer would relieve the logical problem at the expense of denying
that people in the Old Testament faced that same judgment for their sins.
