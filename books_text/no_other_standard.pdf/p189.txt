Religious Crimes, Religious Toleration 173

set all around us sees the (narrow) scope of “religious” matters,
in contrast to the broader understanding of most Christians — and
thereby recognize the slippery and subjective way in which differ-
ent people or groups categorize “religious” matters. The appeal
to this classification of materials in the Mosaic law is really un-
helpful. There are tremendous exegetical and conceptual prob-
lems with efforts to divide out of the larger Mosaic law a smailer
set of commandments which deal with “religious” crimes and are
not to be enforced by civil magistrates today.

Where Does Scripture Restrict the Use
of the Law for “Religious” Crimes?

According to Neilson the case for theonomic ethics docs not
paint a complete picture of the relevant Biblical factors. It has
not taken into account the whole of Biblical teaching, inclusive of
passages which weaken, contravene, or undermine the inferential
argument for theonomy.

The first consideration mentioned by Neilson is the greater
emphasis he finds in the New Testament on grace, compassion,
inward holiness, spiritual life, and newness. Even accepting his
characterization of this greater personal emphasis, however, there
is nothing incompatible with it and the need for social justice as
stipulated in the Mosaic law. The civil sanctions against “relig-
ious” offenses are not calculated to produce or compel inward
holiness or heart-felt worship of God in the first place, and thus
the emphasis the New Testament places on these latter character-
istics would not conflict with the civil necessity for the former
penalties— any more than the constructive New Testament em-
phasis on genuine interpersonal love is contrary to protective
social penalties against rape.

Neilson speaks of the forgiveness which Christ urged. He
points to the rebuke which Jesus gave to the disciples who desired
judgmental fire from heaven upon those who did not welcome
Jesus. He reminds us that the church does not advance by means
of the sword, that the sword is not to be used to vindicate God
or Christ, and that Christ’s followers are not to work for the
premature rooting up of the tares out of the world. Again, all of
