342

Providence, as doctrinal standard, 249
Purging God’s kingdom, 2366, 239, 243
Puritans, 9, 10, 14, 16, 184f

Purity of gospel compromised, 98
Purity principles of old covenant, 12

NO OTHER

Qualifications on law’s validity,
1648, 277

Ransom (substitute) for death
ty, 258
Rape, 126, 176, 216, 221, 235, 252, 268
Rausch, D.A., 8, 19, 38f, 46, 636, 267
Reconstructionism, 7, 19, 24
Redefinition of terms, 144, 150f
“Redemptive” character of laws, 98
Redemptive-historical
character of laws, 98
discontinuities, 261
interpretation, 48
outdating of law, 75
Redemptive typology argument, 123f
Reformed theology, 276
Regeneration, not prerequisite for
enforcing taw, 182
Regulative principle of worship, 71
Relative negation, 287
Relativism, 13, 296f
Religions, not afforded equal civil
rights, 186, 188
“Religious” crimes, 67H, 1624, 1718,
182M, 190, 217
Religious freedom (toleration), 185,
1866, 201
Religious questions, not judged
by state, 182i
Religious titles of civil authorities, 167
Repeat offenders, 269
Repentance, 232, 236
Respect of persons, 254
Responsibility, greater, 239
Restitution, 56, 129, 211, 261
“Restorative law”, 94
Retractions by opponents, 22, 38

STANDARD

Retrogressive ethics, 84

Reversal by critics, 276

Revolution, 13, 307f

Robbery, armed, 269

Robertson, O. Palmer, 96, 98, 100,
138%, 219, 228%, 2516F

Roman law, 241

Roman military tribunal, 128

“Rudiments”, 109

Rushdoony, RJ., 3, 7, 19, 21, 24, 184

Russell, Bertrand, 198

Ryrie, Charles, 12, 95

Sabbath regulations, 21, 71, 123, 125,
A71E
Sacred/Secular dichotomy, 10f, 142,
205f
Sacrifices, 236, 304f
imperfection of, 245f
of ald covenant, 12
pagan, satanic, 199, 200
Salem witch trials, 64
Sanhedrin, 128, 241
Santeria, 186, 1976 199f
Satanic religion, 186, 199
Satan, 197
Scalpel, rather than meat cleaver, 257,
274
Schlissel, Steve, 39
Schrotenboer, Paul, 96, 98, 107, 191,
19, 288
Scripture
as highest authority, 12, 58ff, 198
as infallible verbal revelation, 11
as sole foundational authority, 12,
13
as standard, 9f
self-referential statements of Bible,
59
sufficiency of, 11
truncated views of its authority, 10f
Second commandment, 186
Secular nature of politics, 167, 205ff
Secular courts, 128
