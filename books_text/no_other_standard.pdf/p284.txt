268 NO OTHER STANDARD

However, I am more than ever convinced now that whatever mis-
takes have been made are not big and obvious, but rather ex-
tremely subtle. Tremendous mistakes should not have proven so
difficult for competent critics to expose and refute. In the end we
will generally find far more rhetoric than cogent reasoning in
what the detractors have had to say.

Throughout this volume we have seen how the attempted
refutations of the theonomic position have turned out to be flawed
and ineffective. They have been lacking in exegetical, theological
and/or logical validity. But the really nagging and essential drawback
to the views advanced by the critics is more basically practical in
nature. By what other standard would they prefer to have our society
governed, if not by the law of God?

It is difficult to see how the critics of theonomy can hope to
bring any significant, relevant light into this very dark world of
modern society and government, if they are unwilling to turn to
the revealed wisdom of God’s laws (even as found in the Old
Testament). For instance, if God’s law against rape has been
abrogated (as they say by dismissing the Old Testament law} and
is not repeated in the New Testament, on what basis would
non-theonomists argue for a civil prohibition of this heinous crime?
If God’s revelation of a just civil treatment of convicted rapists
has been abolished (as critics of theonomy argue), then what
would non-theonomists put in its place? What should Christians
counsel legislators (police, judges) regarding this degrading crime
and seeing staggering rates of increase in our major cities? Would
they simply fall back on the wisdom of men, on personal opinion
and feeling, on the expert analyses of sociologists and psycholo-
gists, or just what? What would they do? What would they say?
It is dishonoring to Christ the King to ignore this evil, to abandon
our neighbors and society to civil injustice, to play theological
parlor-games when people are suffering. What, then, do the non-
theonomists have to offer? Nothing. Nothing which carries the
authority of God’s direction and sanction. “Christ is the answer”
is not the answer. It is unhelpful and trivial in the face of a
specific, practical challenge to show how Christianity makes any
real difference in the world.
