148 NO OTHER STANDARD

Old Testament and New Testament arrangements on this point
of church-state interest in the covenant signs (circumcision, bap-
tism; passover, Lord’s Supper), we could only know that on the
basis of the teaching of God's inscripturated word. But if this
discontinuity is taught in the Bible, then theonomists have no
difficulty with it at all; it would simply be the result of studying
every jot and tittle of the Lord’s revealed word, which Theonomy
calls upon us to do, Theonomic ethics does not maintain a priori
and necessarily that there simply cannot be alterations in the appli-
cation of the law introduced by the Law-giver Himself (e.g.,
animal sacrifices, dietary laws, etc.). But apart from such a word
from the Lord, we ourselves do not have the prerogative to intro-
duce such changes. Thus if Robertson is correct that Scripture
teaches a discontinuity regarding the church-state relation (cen-
tering on the covenant signs), then this is a working out of the
theonomic principle — not an undermining of it.

(5) You see, Robertson’s arguments regarding circumcision
and passover in the context of the church-state relation within
Israel cannot have any genuine bearing or strength until he dem-
onstrates that these covenant signs were somehow morally relevant
to the obligation to obey all of God’s laws (the theonomic thesis).
If the theonomic obligation in the Old Testament were tied to the
covenant signs or to Israel’s peculiar church-state relation, then
we might have reason to doubt that obligation when the covenant
signs are not followed (e.g., outside the church) or when that
precise church-state relation is not in evidence (c.g., in modern
American society}. However, such an argument will likely never
be convincingly developed. After the law was given at Sinai, Israel
was fully obligated to keep it, even though they wandered for
years without observing circumcision (cf. Joshua 5:2-9).

Totally apart from the covenant signs or church-state relation
in Israel, the Gentile nations around Israel were held accountable
by God to keep the law.!! Paul declares that even uncircumcised
Gentiles have a duty to follow the requirements of God’s law
(Rom. 1:32; 2:14-15), and when they do, their uncircumcision is

11. Theonomy, ef, 353-364,
