224 NO OTHER STANDARD

about it which touches on the difference between Jew and Gentile,
nor does it symbolically teach a separation between the races,
Zorn has (and can) offer no Biblical justification for this charac-
terization at all. Scripture is the theonamic control-principle for
distinguishing applicable from inapplicable laws today. And Scrip-
tural evidence is precisely what Zorn has not set forth for his
opinions,

In response to Hebrews 2:2, Zorn thinks he can escape the
theonomic significance of the passage by pointing to the a fortiori
character of the author’s teaching there. Zorn observes (quite
correctly) that the penalty for despising the gospel about which
the author warns his readers is eschatological in nature (rather
than civil}. That does not change the fact, however, that the equity
of that eschatological judgment is supported by an a fortiori appeal
from the equity of the Mosaic penal system, If the Mosaic punish-
ments are not the expression of genuine justice, which the author
to the Hebrews claims they are, then Zorn’s argument about
eschatological justice falls completely to the ground (cf. Hebrews
10:28-29). The book of Hebrews presupposes that “every trans-
gression and disobedience” received from the Mosaic law its “just
recompense of reward.” We should presuppose the same. If we
do, then the relevant question is whether penal justice should be
done today or not. If Zorn thinks that it should not, he contradicts
the Apostle Paul (Romans 13:4). If he thinks that it should, then
he cannot evade the theonomic force of Hebrews 2:2,

Were the Penal Sanctions Too Ungracious?

Zorn suggests that perhaps theonomists (such as John the
Baptist and Christ’s disciples) need to learn that the New Testa-
ment age is principally one of grace, where direct divine judgment
against sinful rebellion has been eschatologically put off.'* But the
muddled character of Zorn’s reasoning should be apparent, The
penal sanctions of God’s law which are to be executed by the civil
magistrate (1) were revealed by “the God of all grace,” (2) cannot
credibly be thought to have conflicted during the Old Testament era

14. Zorn, p. 23,
