274 NO OYHER STANDARD

It is in a theonomic approach to the Old Testament law that
one finds an explicit, consistent, and Biblically based principle of
continuity and discontinuity. Thconomy teaches that we should
presume continuity with the Old Testament law unless Scripture
elsewhere gives warrant for modification of it or laying it aside.”
For example, in Acts 10 and Hebrews 9-10 we see the use of
ceremonial features of the law altered, and in texts such as Mat-
thew 21:43; Galatians 3:7,29; Ephesians 1:13-14; and I Peter 1:3-5
we see changes relevant to the identity of God’s people today and
to the land of Israel’s inheritance. When a Christian offers scrip-
tural exegesis as the basis for not applying an Old Testament
command today, he is behaving like a theonomist. But if he
simplistically argues against applying an Old Testament com-
mand because it comes from the Old Testament (was intended
for Israel, was part of the theocracy, is not revealed in the New
Covenant, comes from the era of law and not grace, is too horrible
to follow today, ctc.), he is reasoning in a way contrary to theonomic
teaching. He is using a hermeneutical meat cleaver where a scalpel

 

import whatever theological distinctions and qualifications which are appropriate
into the matler as an interpreter and preacher of the text, but you may not read them
into that text (in the name of “exegesis,” reading them out}. Waltke as much as
theonomists and anybody else must deal honestly with the absolutistic character of
Christ’s words in Matthew 5:17-19. Theonomists see Him using a common teaching
device of laying down the general principle, but allowing for qualifications and
refinements to be brought in later. If Waltke has a better proposal, he has yet to give
it,

2. In an otherwise fine essay discussing the continuities and contrasts between
Old and New Covenants, Robert Knudsen near the end of his article gives expression
to a hasty non sequitur. He says “Thus, we may not assume that every law in the Old
Testament age without exception continues to apply until it has been revoked”
(“May We Use the Term Theonomy. . . ?” in Theonomy: A Reformed Critique, p. 36).
Nothing in the twenty preceding pages of the article offers any relevant premise from
which such a conclusion follows, however; indeed, much of what Dr. Knudsen says
about the moral continuity between the Testaments would tell against such a conclu-
sion, Knudsen’s inference (that we may not presume continuity} stands in open
contradiction to the words of our Lord in Matthew 5:17-19. Knudsen says that we
may apply a specific Old Testament law today “only if it fits.” Theonomists say that
the objective standard of appropriateness for an Old Testament taw (whether it “fits”
the New Covenant or not) can only be the word of Jesus Christ itself. We have no
extrascriptural knowledge of moral “fitness” which may be applied to God's revealed

will,
