Categories of Old Testament Law 95

validity” (pp. 540-541). I put it this way in By This Standard: “The
moral aw of God can likewise be seen in two subdivisions, the
divisions having simply a literary difference: (1) general or sum-
mary precepts of morality. . . , and (2) commands that specify
the general precepts by way of illustrative application” (p. 137).

Complaints Over the Ceremonial Law

The recognition of a ceremonial category of laws in the Old
Testament is commonplace among theologians (from Thomas
Aquinas to Charles Hodge). Nevertheless, there are critics of
theonomic ethics who demur when this category is recognized by
theonomists, even though theonomists properly insist that only
inductive Biblical exegesis can warrant placing a particular Old
Testament commandment into that category — rather than apply-
ing it speculatively or in some a priori fashion.

Some critics argue that the ceremonial category of laws is not
Biblically countenanced, that the whole Old Covenant law must
be seen as a unity. Therefore, the passing away of the sacrifices,
dietary restrictions, and other ritual elements from the Old Cove-
nant proves that the entirety of the Old Covenant law has passed
away with those things.> Lightner contends, “As an indivisible
unit the Law is not to be divided with some of it operative today
while other parts are not.”® Others say that the moral and cere-
monial laws reflect the character of God in exactly the same ways
and are thus inseparable.’ Their contention is that the moral,
civil, and ceremonial laws were not “strictly delineated” in the
Old Testament, thus making the distinction theologically “mis-

5, Jon Zens, Baptist Refirmation Review, vol. 7 (Winter, 1978), pp. 32-33, 40.

6. Robert P. Lightner, “A Dispensational Response to Theonomy,” Bibliotheca
Sacra, vol. 143 (July-Sept., 1986}, p. 238. According to this thinking (as Lightner
quotes Ryrie): “If the Law has not been done away today, then neither has the
Levitical priesthood” (p. 243). The problem with this reasoning is that it is not
warranted by Scripture: there are Biblical grounds for seeing the priesthood as put
out of gear (Heb. 7, esp. v. 28), but not for the entire law. And to reason from “some”
to “all” ig simply a fallacy in logic,

2. H. Wayne House and Thomas Ice, Dominion theology: Blessing ar Curse? (Ports
land, Oregon: Multnomah Press, 1988), pp. 42-43, 89, 100, 134.
