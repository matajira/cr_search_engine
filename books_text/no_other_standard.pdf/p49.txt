A Recognizable, Distinct Position 33

prove liars (Rom. 3:4). No man is qualified to become His coun-
selor (Rom. 11:34). If we do not see the “greater wisdom” in all
of God’s commandments, then we are the ones who need chang-
ing~ not God’s laws. We must not trade the objective standard
of sola Scriptura in our Christian ethic for the subjective standard
of personally perceived wisdom — whether the perception is that
of Chantry, House or Ice. We should all heed James’ caution
against becoming people who “speak against the law” (James
4:11) by preferring our wisdom to God’s own.

The real question is whether the approach of Chantry, House
and Ice truly reflects he Biblical concept of wisdom itself, or rather
represents a concept devised in aii extrabiblical fashion and is now
being imposed upon the Bible from outside. We must be careful
of any subtle (even well-meaning) influences which would lead
us to diminish from God’s word what He Himself has not taken
away (Deut. 4:2). The book of Proverbs is calculated to teach and
instill in us the virtue of true wisdom (1:2). The wisdom which it
gives encourages insightful and faithful application of God’s word
to the practical details of life —thus involving some flexibility, a
large dose of teachability, and proper appreciation for the general
and long-term consequences of one’s conduct and attitudes. What
is the attitude and approach of fue wisdom to these command-
ments from God? Proverbs tells us:

The wise in heart will receive commandments (10:8).

He that fears the commandment shall be rewarded (13:13).

He that keeps the commandment keeps his soul, but he that is
careless of his ways shall die (19:16).

They that forsake the law praise the wicked, but such as keep the
law contend with them (28:4).

Whoever keeps the law is a wise son (28:7).

Where there is no revelation the people cast off restraint, but he
that keeps the law, happy is he (29:18).

Tn Biblical perspective it is the epitome of foolishness to depart
from the path laid out in God’s word. It is never wise for us to
disagree with or act contrary to the divine wisdom which is set
forth in the word of God, including His obligatory law. Where
