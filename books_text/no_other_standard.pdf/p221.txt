Pluralist Opposition to Theonomy 205

some answer into the silence (lack of reference) are only begging
the question by importing their own preconceived conclusions.
McCartney makes things even worse when he says further: “With
all the New Testament’s emphasis on the internality of true religion,
it would be odd indeed to find a New Testament writer suggesting
that an external imposition of religion would accomplish any-
thing.”!! So what? The Old Testament law did not call for “exter-
nal imposition of religion.” Theonomists do not call for “external
imposition of religion.” The author is tilting at windmills, miscon-
ceptions of his own creation.

Returning to Barker, there are other difficulties in his reason-
ing as well. For instance, the thesis that today’s civil magistrates
ought not to enforce the first great commandment really proves
far too much since it would imply that the civil magistrate should
not enforce any of God’s commandments. Why is this? Because
in terms of Biblical teaching (reflected in numerous Reformed
works of theology) part of my duty toward God (thus part of what
it means to love God) includes obedience to those laws regulating
relations with other men; that is, the second great commandment
is built into the first great commandment. Scripture persuasively
declares that loving God entails loving my fellow man (e.g., 1 John
3:17; 4:8, 19; James 3:9-10). Hence the line of reasoning in Barker's
essay implicitly rules out the magistrate enforcing laws which
pertain to showing love to our fellow men (by protecting them
from theft, rape, slander, abortion, sexual deviance, etc.) as well
as to God Himself.

Can Pluralism Be Rescued From
Secularizing or Deifying the State?

It would seem that Barker’s approach could be rescued at
this point only by resorting to some version of the sacred/secular
distinction — for instance, by holding that the “secular” applica-
tions of loving-God-by-loving-my-fellow-man are to be followed
by the civil magistrate, but not the “sacred” applications of loving-
God-by-loving-my-fellow-man. We should all be well aware of the

1. McCartney, “The New Testament Use of the Pentateuch,” Theonomy: A Re-
formed Critique, p. 147.
