Flexibility Regarding the Penal Code 257

just in the case of murder, but in all the cases.

Elsewhere, Dr. Kaiser offers a different line of argument for
opposing the theonomic view of the Old Testament penal sanc-
tions. He maintains that the penalties of the law do not continue
to be an integral part of the law today. However, Kaiser makes
it clear that he does not take this position for anything like the
reasons offered by Meredith Kline: “Kline’s intrusionism does not
appear to differ much from distinctive dispensationalist approaches
to the law. . . . We are still left without any explanation as to
how these legal texts function for the contemporary Christian. . . .
Furthermore the details of the text usually are swallowed up in a
wide-sweeping generalization about the history of salvation being
fulfilled in Christ.”>

Why, then, does Kaiser himself not affirm the continuing
validity of the Old Testament penal sanctions? To his theological
credit, Kaiser’s argument attempts to be well-defined (a scalpel,
not a meat cleaver) and attempts to be exegetically based (appeal-
ing specifically to the text in Numbers 35:31). For these reasons
alone, his approach is superior to most everything else I have
needed to respond to as a theonomist. Moreover and most impor-
tantly here, if Kaiser’s treatment of the text and his logical infer-
ences are sound, then there is no reason for advocates of the
theonomic position, as theonomists, to disagree with him.

 

What Kaiser Is Doing

Indeed, what Kaiser is doing is simply an application and
carrying out of theonomic ethics~ rather than a refutation.
Theonomy teaches that the Old Testament civil law cannot be
categorically and simply dismissed as abrogated in the New Tes-
tament; Kaiser does not attempt to dismiss it in any such way (as
do dispensationalists, critics who appeal to theocratic uniqueness,
etc.). Theonomy teaches that we cannot dismiss the Old Testa-
ment civil code as somehow horrible in its severity; Kaiser is fully
in agreement: “This is not to argue that we believe that the OT
penal sanctions were too severe, barbaric or crude, as if they failed

5. Kaiser, pp. 292, 293. Of course, I applaud Kaiser for these important abserva~
tions against Kline’s treatment of the Old Testament.
