“Why do the nations rage, and the peoples imagine a vain thing?
The kings of the earth set themselves, and the rulers take counsel
together, Against Jehovah and against His Anointed. . . . Now
therefore be wise, O you kings; be instructed, you judges of the
earth. Serve Jehovah with fear, and rejoice with trembling. Kiss
the Son, lest he be angry and you perish in the way.”

Psalm 2:1-2, 10-12

“These religious laws were appropriate for Israel’s unique situ-
ation; they are not appropriate in a pluralistic society.”

Dr. Bruce Waltke,
Theonomy: A Reformed Critique (1990), p. 85

“Similarly, claims to the effect that the Old Testament state pun-
ished “religious” crimes (for example, blasphemy) overlook the
“religious” character of other crimes as well (for example, murder,
adultery). Such arguments are based on a false notion of the
secular/sacred dichotomy which is promoted by modern humanism,
and they are therefore unhelpful in theological argumentation.”

By This Standard (1985), p. 332
