260 NO OTHER STANDARD

cal basis to agree with him. His reasoning is a fallacious argument
from silence.

(2) Furthermore, his argument rests upon an incorrect prem-
ise (and needs to be at least modified, if not negated). His premise
(inferred from Num. 35:31) is that “only in the case of premedi-
tated murder” was the death penalty absolutely required. How-
ever, we know from the teaching of the Old Testament elsewhere
that this is simply not accurate. Consider two examples. Exodus
22:18 says “You shall not allow a sorceress to live.” This says
much more than simply that a convicted, practicing witch should
be assigned some civil penalty. It specifically forbids allowing
such a criminal to continue living (which she or he would do if
any other penalty than capital punishment were inflicted). The
next verse, Exodus 22:19, teaches “Whosoever lies with an animal
shall surely be put to death.” This too cannot be interpreted as
simply calling for some kind of civil penalty for bestiality. The
original text uses an idiomatic Hebrew expression that communi-
cates the certainty of that which is being required: “dying he shall
die” (or “being executed, he shall be executed”) ~ which is com-
monly and properly translated “he shall surely die.” God’s explicit
command is that the crime of bestiality shall without question be
punished with the death penalty. Variation is not allowed in cases
like witchcraft and sexual perversion. We need not pursue our
study of Old Testament penology any further in detail at this
point. This is sufficient to show that the premise of Kaiser’s
argument is in error.

So then, the argument offered by Dr. Kaiser is doubly un-

8. Given the apparent transition in Exodus 2]:29-30 to penal instructions where
the death penalty may be used, but ransom is now permitted, it has seemed to some
commentators that the death penalties in verses 12-25 (e.g., for kidnapping, violent
attacks upon one’s parents) are mandatory ones. This would not mean that all cases
which prescribe the death penalty make it the mandatory punishment; there may
be cases where the law should be read (in context, loca or wider) as making the
death penalty the maximum allowable sentence a judge may impose, allowing a
lesser sentence where circumstances warranted. However, we can only determine
this on a case by case basis, sequiring sound Biblical reasoning in each instance, before
determining that the prescription of execution is not absolutely required. (For exam-
ple, Matthew 1:19 could be used to show that justice did not demand the death
penalty in every case of sexual relations with a betrothed woman.)
