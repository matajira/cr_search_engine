Religious Crimes, Religious Toleration 175

tament penal code by pointing to its typical significance; the
fallacy in such reasoning (viz., uniquely typical history or revela-
tion, therefore uniquely binding moral norms) has been discussed
at some length already in Theonomy and in chapter 7 above.

Neilson also argues from the premise that civil punishment
for certain “religious” sins was not pre-Mosaic— whereas civil
punishment for “man to man” sins was —to the conclusion that
the former are not binding today as are the latter. Such reasoning
is blind to the import of progressive revelation, confuses moral
validity with special revelation (as though the penal sanctions in mind
could not have been the standard of justice as known through
general revelation), and surely proves more than intended (e.g.,
since the distinction between murder and manslaughter is spe-
cially revealed at Sinai and not before, it is not binding today).
Besides, the information in Genesis, though sketchy, is not devoid
of indications of temporal judgment for “religious” sins (e.g.,
Babel), social enforcement of an anti-idolatry policy (¢.g., Jacob
with his household and servants), and the civil rule of the godly
(e.g., Joseph’s dominion, even over the Egyptian pricsts).

Neilson inaccurately asserts that Scripture does not show any
civil government other than Israel's punishing “religious” sins (as
properly defined in terms of the true God) in addition to social
wrongs. The Old Testament prophets indict the idolatry of pagan
tulers — and surely not simply as private sins of individuals (e.g.,
the king of Babylon in Isaiah 14). Ezra commends Artaxerxes for
the emperor’s civil enforcement of support for the temple of God
in Jerusalem. And Paul declared in Caesar’s court (represented
by Festus) that he did not refise civil sanctions, if he were guilty
of the charges brought against him by the Jews — charges which
surely involved “religious” matters, for they centered on concern
for the temple. The Bible simply does not exhibit any intense
concern to exclude “religious” crimes (always public misdeeds as
defined by God’s revelation) from those matters of justice which
should be the concern of all civil magistrates (who, Paul says, are
“ministers of God”).?

2. One final observation here. Neilson’s appeal to the alloged reluctance of the
Roman government to prosecute “religious” matters is, even if accurate (after all,
