Who Is Lorp Over
Tue UNITED STATES?

CHRISTIAN CITIZEN knows the
answer: Jesus Christ. But if this re-
ally is the true answer, grounded firmly
on the Bible, then why is it that so few
Christians are willing to proclaim this
fact publicly, and why is it that no Chris-
tian political candidate dares mention it?
‘There is a reason: the theology of po-
litical pluralism, the dominant public the-
ology in our day.

Political pluralism is not simply a
political philosophy; it is a theology. It
is America’s civil religion. This theology
teaches that there must never be a na-
tion that identifies itself with any relig-
ion. Well, not quite. The nation of Is-
rael is grudgingly allowed to do so, as
are the Islamic nations. But no nation
is ever supposed to identify itself as
Christian. “A Christian nation is self-
contradictory!”

So we are told. But who tells us?
Secular humanists who are dedicated to
wiping out all political opposition. Also,
Christian teachers who teach in tax-
supported schools. Also, professors in
Christian colleges who attended. either
state universities or secular humanist pri-
vate universities, which are the only ac-
credited universities in the United
States that grant the Ph.D. degree.

Also, the U.S, Constitution.

This is the problem. God-fearing
Christian Americans have been told that
the Constitution teaches the absolute sepa-
ration of Church and State. They have
been told correctly. But what they have
not been told is precisely where it says
this. It does not say this in the First
Amendment. The First Amendment says
only that Congress shall make no law re-
garding religion or the free exercise
thereof. So, where does the Constitution

 

prohibit a Christian America? In a sec-
tion that has been ignored by scholars
for so long that it is virtually never dis-
cussed — the key provision that trans-
formed. America into a secular humanist
nation. But it took 173 years to do this:
from 1788 until 1961.

Political Polytheism discusses this
crucial provision in detail—the first
Christian book to do so in over two cen-
tries.

But if Christ is Lord over the
United States, yet the citizens of the
United States either publicly deny this
or are afraid to affirm it publicly, and
if the elected politicians and appointed
officers of the nation are legally prohib-
ited from pursuing the implications of
this fact, then what does this mean for
the nation? It means that God intends
to bring America under judgment.
Why? Because this nation was originally
founded as a Christian nation, cove-
nanted with God, and then it broke the
covenant. The results are predictable:

And it shall be, if thou do
at all forget the Lorn thy God,
and walk after other gods, and
serve them, and worship them, I
testify against you this day that
ye shall surely perish. As the na-
tions which the Lorp destroyeth
before your face, s0 shall ye per-
ish; because ye would not be obe-
dient unto the voice of the Lorp
your God (Deuteronomy 8:19-
20).

This book presents a new vision of
politics and a new vision of America,
a vision self-consciously tied to the
Bible. Ic challenges the political myth of
humanism: many laws, many gods.

 

 

Po.iricaL POLYTHEISM by Gary North

Hardback, 791pages, $22.50

Institute for Christian Economics

ISBN 0-930464-32-X
