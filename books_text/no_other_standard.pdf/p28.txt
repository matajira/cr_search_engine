12 NO OTHER STANDARD

2. Since the Fall it has always been unlawful to use the law
of God in hopes of establishing one’s own personal merit and
justification, in contrast or complement to salvation by way of
promise and faith; commitment to obedience is but the lifestyle of
faith, a token of gratitude for God’s redeeming grace.

3. The word of the Lord is the sole, supreme, and unchal-
lengeable standard for the actions and attitudes of all men in all
areas of life; this word naturally includes God’s moral directives
(law).

4, Our obligation to keep the law of God cannot be judged
by any extrascriptural standard, such as whether its specific re-
quirements (when properly interpreted} are congenial to past tra-
ditions or modern feelings and practices.

5. We should presume that Old Testament standing laws’>
continue to be morally binding in the New Testament, unless they
are rescinded or modified by further revelation.?”

6. In regard to the Old Testament law, the New Covenant
surpasses the Old Covenant in glory, power, and finality (thus
reinforcing former duties). The New Covenant also supersedes the
Old Covenant shadows, thereby changing the application of sacri-
ficial, purity, and “separation” principles, redefining the people of
God, and altering the significance of the promised land.

7, God’s revealed standing laws are a reflection of His im-
mutable moral character and, as such, are absolute in the sense
of being non-arbitrary, objective, universal, and established in
advance of particular circumstances (thus applicable to general
types of moral situations).

8. Christian involvement in politics calls for recognition of
God’s transcendent, absolute, revealed law as a standard by which
to judge ail social codes.

26. Standing law” is used here for folicy directives applicable over time to classes

of individuals (e.g., do not kill; children, obey your parents; merchants, have equal
measures; magistrates, execute rapists), in contrast to particular directions for an
individual (¢.g., the order for Samuel to anoint David at a particular time and place)
or positive commands for distinct incidents (e.g., God's order for Israel to extermi-

nate certain Canaanite tribes at a certain point in history).

27. By contrast, it is characteristic of dispensational theology to hold that Old
Covenant commandments should be a priori deemed as abrogated — unless repeated

in the New Testament (e.g., Charles Ryrie, “The End of the Law,” Bibliotheca Sacra,
vol. 124 [1967], pp. 239-242).
