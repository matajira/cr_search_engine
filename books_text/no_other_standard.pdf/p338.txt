322 NO OTHER STANDARD

‘We saw above how Poythress wishes to dump every imagina-
tive “connection,” “parallel” or “association” into broad Old Tes-
tament themes or motifs—in a way which is hermeneutically
illegitimate and which results in arbitrary conclusions. Likewise,
Poythress wishes to take every connotation or association he can
imaginatively draw to the word “fulfill” in Matthew’s gospel and
claim that they are all packed together into each use of the term
(and particularly at Matthew 5:17), This interpretive method is
fallacious from the standpoint of both logic and literature (despite
its popularity among some theologians). The verb “fulfill” will
have slightly different linguistic senses, depending upon the object
which is said to be fulfilled — whether it is a prophecy, or right-
eousness, or the sinful character of the fathers, or a fishing net,
or the commandments of the Law (e.g., Matthew 1:22; 3:15;
5:17-18; 13:48; 23:32). The playful imagination of the interpreter
has no a priori justification to run all those senses together — as
though every key Greck word becomes for Matthew something
of a theological “code” for saying a multitude of different things
at once.3*

Poythress wants to say that “fulfill” in Matthew 5:17 should
be assimilated to the other uses of “fulfill” in Matthew where it
applies to prophecies of the Old Testament. But the specific con-
text of the Sermon on the Mount simply does not deal with Old
Testament prophecies (even though they are surely found else-
where in Matthew’s gospel). We get into real trouble when we
overlook the obvious. Poythress (and others) who try to import
prophetic, typological “nuances” into the word “fulfill” in Mat-
thew 5:17 are doing just that — importing preconceived ideas into
the text (and context), rather than reading them out of the text.
Even when one’s theological conclusions are orthodox, this is not
exegesis. The violence done to the context of Matthew 5:17 by

34. Why would Matthew commit the silly aus pas of using his “code” word for
fishing nets?? If this is really a code word for Matthew, why does he not use it more
frequendly? in obvious places where it might be expected? (e.g., where other Synoptic
authors use the word) Other significant Greck words are used by Matthew even
more than plarvo; should they too be turned into theological code words which carry
every accumulated linguistic sense and theological association within themselves?
