248 NO OTHER STANDARD

Dr. Johnson is obviously correct that civil governments today
do not have the role of enforcing the elective, redemptive covenant
of grace. Theonomists have taught the same for years. The ques-
tions remain whether there are (non-redemptive) “covenantal”
obligations resting upon the rulers of the earth, whether all pun-
ishable liturgical idolatry is a violation only of a redemptive cove-
nantal obligation, and whether all civil offenses can be somehow
categorized as that kind of idolatry.!! Johnson does not even
address these necessary theological issues before moving ahead
(jumping) to his conclusion. The entire earth and its inhabitants
are not in elective redemptive covenant with God, and yet accord-
ing to Isaiah 24:5, they break “the everlasting covenant” by vio-
lating God’s laws. All of the kings of the earth are under God’s
wrath when they try to “break the bonds” with Jehovah, rather
than serving Jehovah with fear and kissing His Son as King
(Psalm 2:2-3, 6, 10-12}. So much, much more would have needed
to be said and discussed by Johnson before he could begin to
construct a successful argument for the conclusion which he wished
to draw,

Political Powerlessness

One last thing. Readers must not be misled by the question-
able theological reasoning used by Johnson when he notes the
“politically powerless” situation of the early church and minimal
need for directions to political rulers — both part of God’s timing

 

argument. I have also reflected upon a number of theological and exegetical prab-
Jems which could be raised against it along the way. But discussion of such things
will need to await another book {if and when I have studied enough to determine
which conclusion is the sound one.)

11. Johnson asserts without argument that “the justice of the Mosaic sanctions
presupposed the offender's privileged status and prior commitment as a member of
the Lord’s covenant.” But this is gratuitous and begs the entire question! Theonomists
maintain that the justice of those sanctions does not presuppose privileged status, as
though God has a double-standard inside and outside of Israel. It is true that God
revealed His universally just standards to those who enjoyed a privileged status with
Him. But the Bible testifies that the standards which He revealed and entrusted to
Israel were for the purpose of Israel becoming a conduit for them to the watching
(and needy) world! Cf. Deut. 4:5-8; Ps, 119:46; Isa. 2:2-4; Micah 4:2-3,
