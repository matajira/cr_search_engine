290 NO OTHER STANDARD

5:17-19 in Theonomy, but most everything they have said is already
dealt with in the responses above. The reader can judge for him-
self whether the theonomic interpretation of this text is outlan-
dishly wrong (as some try to say) or closer to the mark than what
the critics have proposed in its place. Surely we cannot miss the
stress Jesus placed on His continuity with the moral standards of
the Old Testament, each and every one of them (we must pre-
sume). All participants in the debate over God’s law must put
aside their polemics and scholarly strategies long enough to bow
to the solemn word of their Lord: “Whoever breaks the least of
these commandments and teaches men so shall be called least in
the kingdom of heaven” (Matthew 5:19). When all of the debating
is finished, each of us will still be left to deal before God with that
direct admonition.
