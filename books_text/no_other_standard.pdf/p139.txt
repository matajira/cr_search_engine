Israel’s Theocratic Uniqueness 123

When Zorn comes to what he calls “the crux hermeneutici which
separates theonomists and non-theonomists,” he says that non-
theonomists recognize that “the laws of the Mosaic economy were
part and parcel of a typological redemptive system which, after its
replacement by the New Testament age, would need New Testa-
ment revelation to tell the people of God if they yet apply.” Zorn’s
underlying (but unrecognized) dispensational hermeneutic could hardly
be confessed more clearly. Suffice it to say here that Zorn faces
all the ambiguities and difficulties that Kline does on this score.
How was the entire Mosaic economy a typological redemptive
system? Did the death penalty for kidnapping foreshadow the
redemptive work of Christ or the sacramental fellowship of His
church? If it rather foreshadows the Final Judgment (where all sin
is punished with death), what is the “redemptive typology” of the
penalty of restitution for theft? If the Mosaic civil penalties were
simply meant to foreshadow the Final Judgment, should not all
crimes (indeed, all sins) have been punished by death?

Furthermore, even if Zorn could make clear his claim that the
Mosaic laws were part of a typological redemptive system, would
it logically or theologically follow that those laws are therefore
temporary? Neither Zorn nor Kline is able to offer Biblical justifi-
cation for that line of thinking. Marriage laws are typological of
the relationship of Christ to God’s redeemed people, and the
Sabbath is typological of our eternal rest, but does that mean that
the Mosaic regulations for marriage and the sabbath have all
been abrogated in the New Testament?

A further, insurmountable problem with Zorn’s (and Kline’s)
claim that the Mosaic laws must be seen as belonging to “a
different order” from the civil laws for common nations is the
textually verifiable truth that the Mosaic laws were held out in
the Old Testament as a model for all nations and used as a
standard by which those “non-typological” nations were judged.
by God. The evidence for this is plainly set forth in chapter 18 of
Theonomy, but it has not been dealt with by Zorn or Kline at all.
It obviously poses an embarrassing refutation of their theological
inferences.

Zorn states that if theonomists saw that the typological char-
