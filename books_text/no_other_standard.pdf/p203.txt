Religious Crimes, Religious Toleration 187

by the civil magistrate into matters of religious faith and practice.
In particular it is not to maintain or even suggest (1) that civil
power should be exercised against unbelievers for their lack of
faith as such, or (2} that civil power should be exercised in or
with respect to the affairs of the church.

(1) Refusing to honor cqually all religious commitments would
not lead the Christian magistrate (legislator, etc.) to promote or
practice policies which would persecute or punish unbelievers for
their unbelief, for refusing to profess faith, for failing to attend
church, etc. That is, it would not open the door to such use of
political power as long as the objective moral standard for civil rule
was acknowledged to be God’s word. (In pagan cultures, both
ancient and modern, the civil authority has often interfered in — if
not being identified with — the functions and authority of religious
cult: e.g., Socrates’ offense against Athenian society was simulta-
neously a religious offense.) God’s law does not authorize the
magistrate to judge people’s hearts or punish their unbelief as
such, To the greatest judge in Israel Jehovah said, “man looks
on the outward appearance, but Jehovah looks on the heart” (1
Sam. 16:7). Accordingly, the Mosaic law provided the same pro-
tections for the circumcised Jew as for the uncircumcised stranger
in Israel (cf. Ex. 12:49; Lev. 24:22; Num. 15:16); the circumcised
Jew who refizsed to follow the religious ritual might be excommu-
nicated (Num. 9:13), but the uncircumcised stranger was free to
submit to the religious ceremonies (Num. 9:14; 15:14) or to choose
not to do so without civil penalty. The civil magistrate was not
authorized, nor were sanctions specified, in the law of Moses to
judge the unbelief of one’s heart. Likewise, Jesus warned against
any attempt to root the tares (sons of the evil one) out of the world
so as to leave only the wheat standing; this is God’s prerogative
alone, exercised at the end of the world, with the application — not
of civil penalty, but rather — of eternal condemnation (Matt. 13:24-
30, 37-43). In this age the sons of the kingdom (wheat) will always
live and witness in the presence of the thorny sons of the evil one
(tares), and God has not called the civil magistrate to bring it
about otherwise.!*

14. The eschatological lesson of this parable does not actually address the nature
