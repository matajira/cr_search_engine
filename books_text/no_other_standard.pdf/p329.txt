Paythress as a Theonomist 313

the Old Testament law and arbitrarily pick out from them what-
ever he wishes to retain. There is no objective operating presump-
tion or control principle evident in his interpretation of whether
and how any particular law is valid or set aside today, The
theonomic principle is that we are to presume continuity between
Old and New Testaments until the objective, exegetically-based
interpretation of Scripture teaches us that there is rather disconti-
nuity.

Take another example of interpretive caprice from Poythress.
Over and over again he alleges that the penalties of the Old
Testament law must be seen “as a shadow of the final punishment
in hell” (p. 114}. Indeed, God’s Old Testament punishments
“always” foreshadow God's final judgment; “all” the punishments
seen in Old Testament Israel are foreshadows of hell (p. 121) —and
thus they all point forward to Christ’s bearing the law’s penalty
as the substitute for sinners (pp. 125, 133, 158, 227, 259; cf. the
title of chapter 9). Two weighty criticisms can be levied against
Poythress’ thinking here. First, he confidently asserts this theologi-
cal conclusion about the foreshadowing function of Old Testa-
ment penalties, and yet he never at any point offers the slightest
exegetical evidence that the Mosaic civil penalties were intended
by the Divine Author of Scripture as foreshadows of the final
punishment (or death of Christ). The claim is hermeneutically
arbitrary. But second, we must give full weight to the way in
which Poythress insists that every single one of the Mosaic penal-
ties is a foreshadow of final judgment. Given the universal or
categorical character of the claim, we would expect that Poythress
comes to the same conclusion in each case about modern use of
the Mosaic penalties. But this is far from the case. Sometimes the
(alleged) foreshadowing function of the penalties leads to the
conclusion that they should not be carried out by the state today.
But at other times the (alleged) foreshadowing function of the
penalties does not prevent Poythress from concluding that certain
penalties should indeed be used by modern states. He himself
writes that “Christ’s sacrifice does not eliminate the responsibility
of the state to redress wrongs on its limited human plane” (p.
166) —in which case the fact that an Old Testament penalty
