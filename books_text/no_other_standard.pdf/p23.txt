Introduction to the Debate 7
individual topics.
In God and Politics: Four Views (1989) the reader will find pointed
interaction between four positions (including theonomy) which
were advanced at a conference sponsored by various reformed

groups and held at Geneva College (1987). In particular, one
should consult the interchange with “principled pluralism.”!!

The specific purpose of House Divided (1989) was to offer a rebuttal
to a book written by two dispensationalists against reconstruction-
ist thought.!2 The book was Dominion Theology: Blessing or Curse?
by Wayne House and Thomas Ice.'? Dr. Ken Gentry answered
the critique of postmillennialism, while I replied to the critique of
theonomic ethics.

In a forthcoming book to be published by Zondervan and entitled
Law, Gospel, and the Modern Christian: Five Views (ed. Wayne Strick-
land), the theonomic approach to the Old Testament law interacts
with four other positions (from modified Luthcran to dispensa-
onal), represented by Walter Kaiser, Douglas Moo, Wayne Strick-
land, and Willem VanGemeren.

A number of articles and tapes should be mentioned here as
well:

“The Authority of God’s Law” (1978) appeared in the Presbyterian
Journal as a brief answer to previous criticism of theonomy which
had been written by the editor, Aiken Taylor.'*

Ll. God and Politics: Four Views, chapters 2, 6 (by Carl Bogue), and 17.

12. “Reconstructionism” popularly names a theological combination of positions
which usually includes presuppositional apologetics, a postmillennial view of escha-
tology, and a theonomic view of ethics (cf. the name of the journal started when R,
J. Rushdoony, Dr. Gary North, and I were all working together at the Chaleedon
Foundation: The Journal of Christian Reconstruction). Dispensationalists House and Ice
wrote to warn their readers against the “curse” represented by the last two view-
points,

13, Portland, Oregon: Multnomah Press, 1988.

14. See vol. 37, no. 32 (Dec. 6, 1978) for my reply to Taylor's “Theonomy and
Christian Behavior” in issue no. 20 (Sept. 13, 1978). Taylor perpetuated his critique
in issues for Sept. 20, Nov. 1, and Dec. 6. A lengthy and detailed essay rebutting
Taylor (including his subsequent articles) was entitled “God’s Law and Gospel
Prosperity, A Reply to the Editar of the Presbyterian Journal” and was distributed
by the Session of St. Panl’s Presbyterian Church in Jackson, Mississippi. (A photo-
copy of it is available from Covenant Tape Ministry, 24198 Ash Court, Auburn
CA 95603.)

 
