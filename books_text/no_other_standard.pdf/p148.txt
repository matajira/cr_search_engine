“For what have I to do with judging them that are outside [the
church]? . . . But those who are outside God judges.”

1 Corinthians 5:12-13

“Give place unto the wrath of God, for it is written “Vengeance
belongs to me; I will recompense,’ says the Lord. ... [The
tuler] is a minister of God, an avenger of wrath to him who does
evil.”

Romans 12:19; 13:4

“Bahnsen’s advocacy of God’s law as the basis for civil authority,
with essentially the same kind of separation of church and state
that existed in Old Testament Israel, opposes this sort of plural-
ism. . . . A further distinction between church and state is called
for in our time than that which Bahnsen makes.”

Dr. William Barker,
Theonomy: A Reformed Critique
(1990), pp. 229, 232

“Bahnsen sees no difference between church-state relationships in
the Older Testament and in the New.”

Dr. Bruce Waltke,
Theonomy: A Reformed Critique (1990), p. 78

“Of course there were many unique aspects to the situation en-
joyed by the Old Testament Israelites. In many ways their social
arrangement was not what ours is today. And the extraordinary
character of Old Testament Israel may very well have pertained
to some aspect of the relation between religious cult and civil rule
in the Old Testament. Nevertheless, we will search in vain to find
any indication in the Scripture that the validity of the Mosaic law
for society somehow depended upon any of these extraordinary
features.”

By This Standard (1985), pp. 288-289
