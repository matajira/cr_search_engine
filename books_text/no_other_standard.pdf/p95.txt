Change of Dispensation ar Covenant 79

twentieth-century California. He will then, of necessity, have to
separate that class of individuals from the class of individuals
which constituted Old Testament Israel, arguing that God re-
vealed His law to the latter class or group—and only to them.>
This stands in direct contradiction to the witness of Scripture
itsclf, however. The law was not given only to Israel, but éhrough
Israe] to the world (Deut. 4:5-8; Isa. 2:3; 51:4; Ps. 2:10-11; 119:46).
Paul explicitly says “Now we know that whatever things the law
says, it speaks to them who are under the law — that every mouth
may be stopped, and all the world may be brought under God’s
judgment” (Rom. 3:19). Jesus and Paul both taught that this
same law was meant for the believers of the new dispensation
(Matt. 5:17-19; Rom. 7:12; 8:4; 1 Tim. 1: 8).

Dispensationalism’s logic is thus unbiblical. It is also inconsis-
tently maintained. Dispensationalism dispenses with the law of
Moses since it was revealed to Israel (not the church), but dispen-
sationalism does not dispense with the Psalms, which werc like-
wise revealed to Israel (not the church), Nor do dispensationalists
draw back from the promises which were revealed to Old Testa-
ment Israel (but only from the law revealed to Israel). This
arbitrariness betrays the dispensationalist line of reasoning.

The dispensational mindset is pitted against the Apostle Paul,

5. He admits that his argument logically depends upon the dispensational way of
distinguishing Israel from the church (p. 236). He mistakenly holds that Israel and
the church are not interchangeable in the Bible. However, in Galatians 6:16 Paul
directly calls the Christian church “the Israel of God” {including Gentile believers: ef.
Gal. 9:25; 4:8; 5:2). Christians are the true “Jews” (Rom. 2:28-29), the true “circum-
cision” (Phil, 3:3), the true “seed of Abraham” (Gal. 3:7, 29}, the “children of
promise” like Isaac (Gal. 4:28), the “commonwealth of Israel” (Eph. 2:12, 19).
IsraePs glory was the presence of God among them in the temple (Lev. 26:1!-12),
and the church now is that temple, indwelt by the Holy Spirit (1 Cor. 3:16; 2 Cor.
6:14-16; Eph, 2:21-22; 1 Peter . Israel was called the people of God’s own
possession (Ex, 19:5; Deut. 7:6; 14:2; 26:18}, and now the church has been given
that same designation (Eph. 1:14; 1 Peter 2:9; Titus 2:14). There is but one olive tree,
with Gentile and Jewish branches both a part of it (Rom, 11:17-18). The New
Covenant, which was made with Israel, is established with the church (Jer.31:33;
Matt. 26:28; 2 Cor. 3:3-18). It is obvious that dispensationalism’s radical distinction
between Israel and the church as one covenanted people of Ged was not developed
from within the scriptures and their manner of speaking, but is an @ priori theological
conception developed outside Scripture and now imposed upon it like a Procrustean
bed.

 

 

 

 
