Flexibility Regarding the Penal Code 263

the application of the penal code is simplistic! Of course, we can
make mistakes in applying it. This shortcoming, I trust, does not
invalidate our arguments for the need to apply the law of God with
respect to crime and punishment (which is admittedly difficult).

Anyway, Longman’s perspective is, on the whole, not far from
that of theonomy. Where he does disagree, he sometimes does so
with no argumentation or proof at all.!° Sometimes his thinking
rests upon frivolous considerations —e.g., theonomic penology
would, mirabile dictu, require “a new Mishnah”! (Can’t we just
hear early church heretics “refuting” the doctrine of the Trinity
by pointing to the dreadful eventuality that this doctrine will
require systematic theologies which are three-volumes long!) Some-
times he engages in non sequilurs—as when he draws the conclu-
sion that Christian legal judgments should be “guided by the
principles of Scripture rather than by the explicit statements of
the Old Testament” from the premise that the explicit statements
sometimes involve flexibility or difficulty in interpretation. Such
considerations provide no support whatsoever for the idea of furn-
ing away from the “explicit statements” of Scripture and substituting
more general principles! (Besides, are not “general principles”
flexible and difficult to interpret also?) Jesus did not permit us the
option of dismissing the explicit statements of Scripture — not
even a jot or a tittle of the least commandment in the law (when.
properly interpreted of course, Matt. 5:17-19).

The only theological or exegetical arguments (brief though
they be) which Longman sets forth contrary to the delimited area
where he disagrees with the theonomic view of penology have
already been answered elsewhere in this book.

10. For example, he simply dismisses as “unsuccessful” with a wave of his hand
(stroke of his pen) the detailed and exegetically based theonomic argument against
the idea that Jesus departed from the Old Testament view of divorce and revoked
the civil penalty for adultery {p. 53). There is no demonstration offered.

Il. The argument from Numbers 35:31-32 for numerous cases where a ransom
could be substituted for the death penalty is rebutted in the discussion of Kaiser's
view above. The argument that ecclesiastical excommunication replaces the civil
penalty for something like blasphemy is challenged in chapter 12. The argument
from God’s special, holy presence in the midst of Israel is analyzed in my response
