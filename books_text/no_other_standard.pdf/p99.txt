Change of Dispensation or Covenant 8&3

example of Christ Himself. Zens argues that law in these last
days “is solely in the hands of Christ, not Moses.” He argues
that since a law code must always be related to an act of redemp-
tion [where is this axiom taught?], and since evrything going on
in Israel was of a typical nature [even the sins committed? even
the mundane elements of life common to all cultures?], the law
which now binds us must be identified with the covenant in
force — rather than the former covenant which is no longer opera-
tive, unless elements of it have been incorporated into the New
Covenant, In short, a new covenant means a new law. Even the
Decalogue is abolished along with the former covenant, unless its
elements are repeated in the New Testament. “To push Moses’
law (as a totality) into the New Covenant is legalism,” Zens
concludes, for we are bound now only to the law of Christ.

Of course, the fundamental mistake in all of this discussion
is the underlying premise which denies the covenant of grace.
Zens nowhere demonstrates that Scripture teaches a presumed dis-
continuity between Old and New Covenants—a general repeal
of Old Testament principles and precepts unless repeated by the
New Testament. The very opposite presumption is the one clearly
set forth by the Bible, both Old and New Testaments (Deut. 4:2;
Matt. 5:17-19). Indeed, the very terms of the New Covenant in
Jeremiah 31 (repeated in Hebrews 8 and 10) indicate that God’s
law —not a new law, but the already well known law — will be
written upon the hearts of God’s people, Thus the Mosaic law is
confirmed in the New Covenant as an essential feature of that
covenant,

Paul speaks of Gentiles as “strangers from the covenants of
the promise,” thereby explaining why they were “alienated from
the commonwealth of Israel” (Eph, 2:12) until the blood of Christ
brought them nigh, Obviously, the Mosaic covenant was one of
these “covenants” (plural) of “the promise” (singular) made to
Abraham. The Mosaic covenant was occasioned by God’s re-
membrance of His covenant with Abraham (Exodus 2:24), and

9, “Crucial Thoughts on ‘Law’ in the New Covenant,” Baptist Reformation Review,
vol. 7 (Spring, 1978), pp. 7
