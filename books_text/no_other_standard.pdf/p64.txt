48 NO OTHER STANDARD

rather, it is the case that non-theonomists overlook the continui-
ties and overemphasize the discontinuities! It would be a whole
lot more profitable for everybody if theologians (in general) got
out of the habit of criticizing each other’s “emphasis” and paid
constructive attention to the actual premises and conclusions ad-
vanced by cach other.

Something which would make the job of theonomic critics
much, much easier is if theonomists did not try to draw careful
distinctions, qualifications, and detailed evaluations regarding their
own basic tenets and the text of Scripture — or if they just did not
believe (as they do) in a redemptive-historical reading of the
Bible.!9 (It would be hard to explain why they write such long
and detailed books, though.) Vern Poythress acknowledges: “We
do not merely assume that no changes can ever be entertained.
Bahnsen instructs us to examine patiently the particular texts and
warns us of the complexities involved.” Again: “Theonomy at its
best takes considerable note of discontinuities introduced by re-
demptive history and in particular by the coming of Christ.”2°

19. John Muether, p. 251, tries (simplistically) to dismiss the theonomic outlook
for its alleged “unwillingness to make important redemplive-historical distinctions.”
He offers no argumentation to support that judgment and gives no indication of
what important distinctions theonomists miss from scripture. Likewise, Tremper
Longman claims: “Theonomy tends to grossly overemphasize continuity to the point
of being virtually blind to discontinuity” (“God’s Law and Mosaic Punishments
Today,” Theonomy: A Reformed Critique, p. 49). “Virtually blind” to discontinuity?
Longman does not tell us just exactly what he sees that is relevant to refuting the
theonomic approach, but which theonomists blindly overlook. Longman’s co-author,
Dennis Johnson, rcadily enough corrects this accusation of gross blindness: “Both
theonomists and their critics acknowledge continuity and discontinuity between the
old covenant and the new. .. . No theonomist of whom I am aware actually
contends that the law’s applicability remained utterly unchanged by the coming of
Christ. . . . So the difference between theonomists and non-theonomists is not that
one group sees nothing but continuity between the Mosaic order and the new
covenant, while the other sees nothing but discontinuity” (“The Epistle to the
Hebrews and The Mosaic Penal Sanctions,” Theonomy: A Reformed Critique, pp. 172,
173). Johnson easily offers a number of such important discontinuities spoken of in
my writings.

20. “Effects of Interpretive Frameworks on the Application of Old Testament
Law,” Theonomy: A Reformed Critique, pp. 121, 109. The article by Poythress is not
intended as a refutation or critique of the theonomic position itself “in its best form”
and calls for no further response.
