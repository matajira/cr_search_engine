xiv NO OTHER STANDARD

cally defendable criticisms to offer, there would be several books
refuting Bahnsen’s book. (A symposium is not a book.) What is
remarkable is that there have been only two books written in
response to Theonomy, neither of which received much attention
or later citation. Bahnscn dissects both of them in this book.

The critics have neglected the old rule of politics, “You can’t
beat something with nothing.” Where are the critics’ judicial,
moral, and cultural alternatives? It is not sufficient to argue that
Bahnsen’s view of biblical law is wrong. The critics have an
obligation to their readers and to the Church in general to show
what view is correct. They never do. They remain content to leave
the Church without any authoritative proclamation to bring be-
fore the civil magistrate, This keeps the Church silent, which is
exactly what the humanists want. There is an unstated opera-
tional alliance between the humanists who hate God’s law and
the Christian pietists who hate God’s law. This alliance leaves the
humanists in control of culture and politics, and it leaves the
pietists in control of the churches on Sunday morning . . . until
the humanists decide to change the rules.

Conclusion

The issue that Bahnsen has raised is biblical law. Therefore,
the issue by extension is the legitimacy of the idcal of Christendom:
the comprehensive kingdom of God in history. The issue is the
legitimacy of the Lord’s prayer in history jor history: “Thy king-
dom come, thy will be donc, in carth as it is in heaven.” It is time
for the critics of Bahnsen’s thcological position to explain in detail
just what it is that they are offering in its place as a biblical ideal.
It is time for them to fight something very specific with something
equally specific and equally biblical. So far, they have self-
consciously avoided doing this.

But they now have more to do than this. They need to re-
spond to this book. They need to show why Bahnsen’s replies to
all of them, one by one, are inaccurate. Then they need to tell us

6. Greg Bahnsen, “This World and the Kingdom of God,” reprinted in Gary
DeMar and Peter Leithart, The Reduction of Christiqnity: A Biblical Response to Dave
Hunt (Ft, Worth, Texas: Dominion Press, 1988), Appendix D.
