284 NO OTHER STANDARD

ethics: viz., we are to presume that any Old Testament command-
ment is binding today (when properly interpreted) unless God’s
word itself teaches us otherwise. In the face of the warning from
Jesus, only a word from God Himself should be acceptable to us
as a basis for turning away from applying any commandment
from the Old Testament scripture.

Nevertheless Long does not acknowledge what appears so
obvious here. He baldly asserts: “the saying of our Lord in Mat-
thew 5:19 is surely no basis for asserting either the applicability
today of all the Mosaic law as covenant law, or the applicability
of any particular part or parts of it in its covenant configuration.”
What makes Long so “surely” deny this? What evidence does he
offer in support of his opinion? “The covenantal distinctions of
God's law . . . are not being specifically dealt with in the Sermon
on the Mount. Therefore, ‘no deduction can be drawn from these
words binding the Jewish law, or any part of it, as such, upon
Christians’.” That is, unless necessary qualifications are specifi-
cally drawn right here in the local context of the passage (rather
than elsewhere in further revelation), no basis whatsoever can be
found for answering a question about the law’s presumed validity
or invalidity today. This logic needs to be untwisted. It would
rather seem that just because no qualifications were made by Jesus
in the text before us (and will need to be gathered from other texts
of God’s word), the passage provides a clear presumption for the
validity of any and all Old Testament commandments. Just be-
cause of the unqualified character of Jesus’ declaration — and its
explicit scope (even “the least commandment”) — His declaration
does indeed provide a basis for asserting an answer to the question
of the applicability of the Mosaic law today — however it is not
Long’s preconceived and desired answer.

The desperation of Long’s maneuver comes out when he
winds up saying that Matthew 5:19 merely teaches the inviolabil-
ity “of all applicable commandments” — thus turning the stern
warning of Jesus into an empty tautology! We can be sure that
our Lord was not simply insisting upon something which is true
by definition. To interpret the verse in any way that allows us
freedom to neglect any Old Testament requirement, without defi-
