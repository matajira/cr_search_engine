66 NO OTHER STANDARD

will not feel impelled otherwise by the approach adopted by
Bahnsen.”

Now then, we must realize that when specific texts (separately
or together) are not used to settle a theological dispute, everyone
is left to choose on the basis of his own generalized impression from
the Bible as a whole! Therefore, Neilson’s “argument” against
theonomic ethics is easily and sufficiently countered by simply
saying that “the impression” I get from the Bible is different from
Neilson’s. End of dispute—if subjective impressions from the
overall effect of Scripture were the basis of our theologizing.

Controlled exegesis of the objective text of Scripture (using
historico-grammatical interpretation, along with the analogy of
faith or demand for consistency) and good and necessary conse-
quences which follow from such exegesis are the required bases
for our doctrinal decisions. The first chapter of the Westminster
Confession of Faith is eloquent to this effect and should be con-
sulted here. Feelings about the “tenor” of the Bible must eventu-
ally be anchored to Biblical éeaching (either in a specific passage
or larger set of passages) and its logical implications, or else such
feelings simply have no authority in theological matters and rea-
soning. The crucial locus of our decision-making reflection must
be the publicly discernible text of Scripture and canons of sound
argumentation, not the unsure and subjective recesses of private
feeling and impression. That is precisely why Theonomy cautioned
readers in advance against “tenor-arguments”:

“As the history of theology evidences, it is just when men are
willing to depart from Scripture’s explicit statements in favor of
their generalized, undeniably subjective, assessments of the implicit
message or meaning of the Bible that doctrinal deviation begins
to cut its wide swath. Of course, it is amazing how that imagined,
trans-textual “tenor” of the Bible (or N.T.) always seems to corre-
late so exactly with the theological milieu, ecclesiastical back-
ground, and personal opinions of the one who assesses this “tenor”;
one’s own predispositions and the ideological status quo are easily
read into this not so clearly defined and defended “tenor.” There
is a sober lesson here for contemporary evangelical and Reformed
teachers; that which we disapprobate in unorthodox theologians
