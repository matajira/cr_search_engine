Categories of Old Testament Law 107

3:23-24); the Mosaic administration pointed to and taught about
Christ’s redemptive work, but now we enjoy that redemptive work
in its actual accomplishment. But this is not the way in which
Chantry sees the passage.

Does he believe that “the law” as a tutor refers to the Old
Testament law absolutely and without qualification? No, he docs
not take that view either, for he knows very well that not all of
the law can be dismissed (e.g., the ten commandments).2” So,
Chantry does not argue that Paul released his readers from the
law in general. That view would contradict things taught in the
test of Scripture, he says. According to him, the Mosaic law
contained different clements, some of them permanent, but some
peculiar to the Mosaic setting. Thus Chantry asserts: “Only the
features unique to Moses’ administration of the grace principle
were temporary and done away at the coming of Christ” — which
is virtually true by definition. Was the decalogue part of the
unique and temporary aspect of the Mosaic law according to
Chantry? No, because we can find support for the decalogue’s
validity elsewhere in Scripture (with which I agree), However,
we can just as much find support elsewhere in Scripture for the
continuing validity of the judicial laws of the Old Testament (as
I have labored to show in Theonomy and other publications). So,
Chantry has still not demonstrated his particular point. In order
to do that, he is compelled to rest his case on a couple of theologi-
cal and logical errors.

Chantry observes that we are no longer under the schoolmas-
ter (tutor), according to Galatians 3:24-25. What part of the law
was Paul thinking about as being such a schoolmaster? Chantry
answers: “All that was restrictive, repetitive, worldly, harsh, and
rigid is no longer appropriate for the heirs of God’s house.” That
is, ancient schoolmasters were stern and restrictive, and thus it
must be the stern and restrictive aspects of the law which were

27. This is the obvious problem with those theonomic critics who appeal to
Galatians 3 and try to make the “tutor” the entirety of the Mosaic law, e.g. Robert
P. Lightner, “A Dispensational Response to Theonomy,” Bibliotheca Sacra, vol. 143
(July-Sept., 1986), p. 241; Paul Schrotenboer’s essay in God and Politics: Four Views,
ed. Gary Scott Smith (Phillipsburg, NJ: Presbyterian and Reformed Publishing Co.,
1989).
