12

AUTONOMOUS PENOLOGY,
ARBITRARY PENOLOGY

Here’s the Rub

The most distinctive aspect of theonomic ethics, if not also its
most controversial application, is its endorsement of the continu-
ing validity and social justice of the penal sanctions stipulated
within the law of God. Were it not for the fact that the theonomic
position leads to this conclusion, if one is to be logically and
Biblically consistent, many critics would not find it necessary to
try to refute the position.! Theonomy in Christian Ethics argued that
the laws of the Old Testament which defined civil justice for Israel
should be presumed to be binding today (as Jesus said in Mat-
thew 5:17-19); after all, they were a model of justice even for the
Gentile nations surrounding Isracl (as Moses said in Deutero-
nomy 4:5-8). Indeed, civil magistrates in the New Testament era
still need divinely revealed direction to carry out their God-given
duty to execute vengeance on criminal evil-doers (as Paul said in
Romans 13:1-4). It is precisely a lawful use of the law of God to
use it in the restraint of public, civil unrighteousness (1 Timothy
1:8-10), even to use the undeniably just penal sanctions found in

1. Tremper Longman ILI writes: “Most disturbing to those who are introduced
to theonomy for the first time, it seems, is its advocacy, not only of the Mosaic case
law, but also of its system of punishments. . . . Certainly che most controversial
aspect of a theonomic penology is its advocacy of the death penalty for a variety of
crimes” (“God's Law and Mosaic Punishments Today,” Theonomy: A Reformed Cri-
fique, ed, William §. Barker and W. Robert Godfrey [Grand Rapids: Zondervan
Publishing House, 1990], pp. 41, 44). Longman finds it much easier to have an open
and positive attitude about the theonomic view of Mosaic punishments less severe
than capital punishment — such as promoting restitution over imprisonment (p. 54).

2n
