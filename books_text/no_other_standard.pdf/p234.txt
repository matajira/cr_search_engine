218 NO OTHER STANDARD

and rejection of Old Testament penal sanctions, we are left with
ethical arbitrariness — which is unacceptable both within and out-
side Christian circles of scholarship."

Although Dr. Henry argues at some points that penal laws
should be presumed valid unless repealed in the New Testament,
at other points he takes the diametrically opposite approach and
reasons that New Testament silence about a penal sanction entails
its abrogation: “the New Testament . . . nowhere imposes all the
details of theocratic jurisprudence upon civil governments gener-
ally.”" Of course, he cannot have it both ways. Will he presume
continuity or discontinuity with the laws of the Old Testament?
A little of each is quite arbitrary — and unacceptable in any view-
point which is meant to be systematic and consistent.

Is the Civil Law Restricted
to the Noahic Covenant?
A recurring argument against the theonomic view of the Mo-
saic penal sanctions holds that modern statcs are restricted to the
Noahic covenant in gaining civil laws or penalties from the Old

 

Old Testament may be set aside because God Himself temporarily let some sins go
unpunished (Acts 17:30; Rom. 3:25), it not only confused divine and human preroga-
tives, it also overlocked the fact that its supporting verses do not pertain to the
suspension of aivit punishment of crime by man, either in a particular case or as a
general policy,

10. Sometimes theonomic critics address the question of which Old Testament
sanctions to endorse, but do not reflect on the significance of their own rhetoric
(allegedly against theonomy). For instance, Dan McCartney writes: “The explicit
sanctions of Old Testament civil law thus apply only insofar as they are an underlin-
ing of God’s holiness” (“The New Testament Use of the Pentateuch: Implications
for the Theonomic Movement,” Theonomy: A Reformed Critique, p. 148), What he does
not realize about his conclusion is that those who differ with it would say exactly the
same thing. The answer proposed does nothing to resolve the difference over conilict-
ing opinions as to which penal sanctions are applicable today.

11. Deut. 4:2; Matthew 4:4; 5:17-19 (etc.) all undermine such arguments from
silence, which abound among critics of theonomy. For instance, Jim Bibza asserts
“no New Testament text gives the state the kind of power” ta punish criminals with
the penal sanctions of God's law (“An Evaluation of Theonomy,” privately distrib-
uted from Grove City College, Spring, 1982, p. 7). But of course Romans 13:1-4 (cf.
wy. 9-10; cf. 1 Tim. 1:8-10) does that very thing.
