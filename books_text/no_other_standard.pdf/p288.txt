“Therefore whosoever shall break one of these least command-
ments, and shall teach men so, shall be called least in the kingdom
of heaven”

Matthew 5:19 (cf. vv. 17-18)

“The Golden Text of theonomy, Matthew 5:17, cannot aim to
establish a theonomic ethic. . . . Jesus cannot be establishing
every jot and tittle of the law, as Bahnsen’s thesis declares, and
at the same time abrogate some of the laws. The many specific
changes of the law in the New Testament seriously undermine the
thesis that the burden of proof rests on the interpreter to show
that the law is not in force.”

Dr. Bruce Waltke,
Theonomy: A Reformed Critique (1990), pp. 80, 81

“If we are to submit to God’s law, then we must submit to every
bit of it (as well as its own qualifications). . . . Every jot and tittle
of the law must be as jealously guarded as Christ guarded it
(Matt. 5:17-20). . . . The presumption would have to be continu-
ity, not contradiction.”

Theonomy in Christian Ethics
(1977), pp. 309-310, 313
