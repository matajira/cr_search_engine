6
CATEGORIES OF OLD TESTAMENT LAW

Two different lines of theonomic criticism converge on the
issue of recognizing distinctions within the various laws which are
found in the Old Testament.

One line of criticism proceeds on the premise that all of the
individual commandments of the Old Testament are of the same
nature, stand on the same level with each other, are mixed to-
gether without recognizing different categories, and therefore must
all — each and every one~ be treated in exactly the same manner
under the New Covenant, Since some of these laws are obviously
not to be observed today (for instance, the sacrificial cultus),
therefore ali of these laws must likewise not be obligatory.!
Theonomic ethics challenges this line of thinking, defending the
traditional theological distinction between “ceremonial” and
“moral” laws within the Old Testament? However the infelicity
of the “ceremonial” label is pointed out in Theonomy in Christian
Ethics. T suggest that a more accurate description of the laws

1, Unless they are repeated in the New Testament, of course—the standard
dispensational exception.

2. Inexplicably, Bruce Waltke thinks this is inconsistent with ihe theonomic view
that the Old Testament law should be presumed to continue in validity unless God’s
ward. gives reason to believe otherwise: “Theonomy in Relation to Dispensational
and Covenant Theologies,” Theonomy: A Reformed Critique, ed. William S. Barker and.
W. Robert Godfrey (Grand Rapids: Zondervan Publishing Hause, 1990), pp. 79-80.
God's word gives reason for us to belicve otherwise about the cercmonial law
precisely in such texts as Waltke tries to use against theonomy (eg., Gal. 4:9;
Hebrews)! Tt is no embarrassment to the theonomist to say that we yet observe the
underlying meaning of the old ceremonial laws — for Scripture itself tells us this (e.g.,
Heb, 9:22-24; 2 Cor. 6:17; Rom. 12:1). But Waltke conveniently omits any mention
or treatment of such things in his hit-and-run criticisms.

 

93
