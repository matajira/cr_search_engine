286 NO OTHER STANDARD

rary aspects of Old Testament law.!!

Long’s treatment of Matthew 5:17 is weighed down with
exegetical difficulties as well. He insists that the verse pertains to
Christ’s life (behavior) and teaching, even though there is nothing
in the local context which broaches the subject of Christ’s own
actions or achievements. Christ speaks of the purpose or effect of
His coming as the Messiah —the effect of that advent on the
theological issue of the law’s validity. The fact that His own
behavior and teaching were relevant to each other does not (as
Long thinks) justify reading a reference to Christ’s own life (in
addition to his teaching) info the text. Long’s treatment of “law
or prophets” is also strained. He acknowledges that this is com-
mon phraseology for the literary collection of the Old Testament
canon, but Long also wants it to denote the promise features and
the command features of Old Testament teaching. He really can-
not have it both ways, though. It may be a fine piece of exhorta-
tion to say, as he does, “the law and the prophets and command-
ments and promise go together in a unity that have their Messi-
anic fulfillment in the New Covenant mediator,” but it is deplor-
able literary exegesis. One phrase in one verse does not mean all
of these things simultaneously. Looking at the context of the verse,
it should be evident that Jesus was not dealing with the prophecies
of the Old Testament canon at all, but rather with its moral
instruction (found throughout the canon—throughout “the law
or the prophets”). There is simply no textual basis for Long to
import a reference to Old Testament promises or prophecies into
Matthew 5:17.

Long attempts to defend his exegesis of v. 17 with the unusual
suggestion that, although Jesus was speaking in Greek, He was
thinking according to Hebrew idiom — which allows a negative
assertion to be taken comparatively, rather than absolutely, That

11. Long claims that the covenantal or temporary aspects of the law are those
parls of it which pertain to sanctions and motives, pertain to community rather than
individual, or pertain to administration or configuration of the covenant. This is
open to a wide range of meanings, as well as numerous easy Biblical counter-
examples. Love is a motive in both Old and New Testaments. Murder is surely a
community concern, What is the “configuration” of a covenant all about?
