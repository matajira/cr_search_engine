Autonomous Penology, Arbitrary Penology 229

the rebuttal. A rapist in the Old Testament faced a civil punish-
ment as well as judgment on the last day. Accordingly, if today
we forego the “civil” punishment for one to be administered at the
last judgment, we not only contradict ourselves (since the punish-
ment is no longer civil and temporal at all), but we actually
collapse the civil penalty into the eternal penalty — thereby elimi-
nating the civil punishment altogether. This is not fulfillment but
abrogation {contrary to Matt. 5:17-19).

Even if, as it is sometimes hypothesized, the penal sanctions
of the Old Testament served a typological value akin to that of
the holy war of Old Testament history, this would no more invali-
date the preconsummation use of Old Testament penalties for mod-
ern states than it invalidated the preconsummation use of them for
ancient Israel. The pedagogical value of those divinely revealed
penalties is not somehow incompatible with their socio-political
use prior to the end of history.

If the Mosaic civil penaltics were really the intrusion of the
principles of final judgment into history (as Kline and others like
to say), then all sin would have been treated as a crime in the Old
Testament era — and ail crimes would have been capital offenses.
However, these logical consequences are manifestly untrue to the
Old Testament system of law, thus refuting the intrusion thesis
by modus tollens. There is no good reason for rejecting the view
that the preconsummation justice of the Old Testament penal code
for temporal society is still valid today.

Palmer Robertson’s reason for saying that the penalties of the
Old Testament could be seen as fulfilled at the last judgment is
that executing blasphemers seems too rigid for the New Testa-
ment context, and it would be problematic for the civil authority
to apply the laws against subversion to idolatry (Deut. 13) today.
But again, these are not arguments, much less biblical arguments.
They are impressions and feelings based on the current status quo
in our society. I have no doubt that until our society is converted
to a submission to God’s word, is brought to think God’s thoughts
after Him, and willingly enacts laws against subversion to idolatry
and blasphemy, the very idea of punishing such misdeeds will seem
“rigid and problematic.” But I think that our society should be
