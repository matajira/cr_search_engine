God’s Law and Civil Government Today 155

magistrate’s] civil code.” But how do we know where to draw the line
in the Lewis outlook? If he wants to put aside the “civil” or
“judicial” aspects of the Old Testament law, or if he wants to
abandon civil sanctions for “religious” crimes, just what do these
terms clearly include and exclude? Where does Scripture lay down
a discriminating principle with such concepts in mind? Is there
any rule at all by which Lewis picks and chooses those elements
of the Old Testament which he would have the state enforce today
(e.g., punishing kidnappers?) and which he would not (e.g., pun-
ishing kidnappers with death?)? His position is left completely arbi-
trary and controlled by subjective considerations which cannot
be warranted from publicly accessible, textually tested principles
revealed by the Lawgiver Himself. By comparison, the theonomic
principle is that we should presume continuity with the Old Tes-
tament moral standards, even for civil magistrates, unless God’s
word itself authorizes a modification.

By What Other Standard Should
Rulers Rule? Conscience?

Laird Harris, like other theonomic critics, does not want the
Old Testament law to be the moral standard for the state today.
What does he suggest in its place? He proposes that unbelieving
rulers should rule, rather, according to the dictates of conscience
as it has been influenced by natural revelation and common grace.3
If he means that conscience, when it has by common grace prop-
erly grasped and submitted to general revelation, should direct
the ruler today, then he would reject the written law of God as a
political standard only at the cost of preferring a less clear edition
of the same revelation over one which is more clear—or else
falsely assuming that the two forms of revelation have different
moral content. And if he means that conscience itself (having been
influenced successfully or not by common grace and natural reve-
lation) should be the standard of political morality instead of the
written law of God, then he has hardly been consistent with his

3, R. Laird Harris “Theonomy in Christian Ethics: A Review of Greg L. Bahnsen’s
Book,” Prestyterion, vol. 5 (July, 1979), pp. 10, 12, 13.
