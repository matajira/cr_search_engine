208 NO OTHER STANDARD

standing” intrude into the governing of the secular state. If Barker
calls for bringing the Christian conception of a “higher law” to
bear upon the state (to keep it from deifying itself), he cannot with
logical consistency also argue that the state should nof operate on
any distinctively Christian understanding of its duties, limits or
prerogatives over against the convictions of naturalists, positivists,
etc. Honest pluralism logically precludes a distinctively Christian
conception of the state.

Dr. Barker’s interpretation and application of the taxation
pericope in Matthew 22 does not, then, provide any good reason
or Biblical basis for us to depart from the conclusions which we
have reached earlier concerning the way in which the civil magis-
trate ought to be prejudiced in favor of Christianity in the exercise
of his public office. The words of Jesus prior to His ascension in
Matthew 22 should not be pitted against the divine pledge of
Psalm 2 that, following upon the exaltation of God’s Son, all the
kings and judges of the earth would be required to serve Jehovah
with reverence and to kiss the Son. Christian citizens certainly
should render their tribute to Caesar by paying their taxes (for
Caesar’s image is on the coin}, but civil magistrates should like-
wise render their own tribute éo God (for His image is on them, as
well as us all}. Finally, the view that the state ought to be biased
(“morally prejudiced”) in favor of Christianity does not, when
applied in a theonomic fashion (cf. chapter 10 above), rule out a
Biblically conceived religious toleration, as Barker and others
seem to fear.
