114 NO OTHER STANDARD

ing verbal dispute, Chismar and others might consult chapter 20
in Theonomy in Christian Ethics so as to distinguish clearly the
differing senses for the word “theocracy.” If the word is used for
those cultic, geo-political, and administrative aspects which were
unique to Old Testament Israel, then theonomic ethics certainly
does not maintain that we are under such a “theocracy” today.
But then, neither does theonomic ethics hold that such a “theoc-
racy” is a kind of revealed prerequisite for the moral validity of the
Mosaic laws. Scripture indicates the very opposite — that even the
non-theocratic nations around Israel were held accountable by God.
to the same moral obligations as those revealed through Moses
{e.g., Deut. 4:5-8; Lev. 18:24-30; cf Psalm 2:10-12; 119:46; Sodom,
Ninevah), these ordinances of the law being known by natural
revelation and written on all men’s hearts (Romans 1:20-21, 32;
2:11-15),

Assertions of moral uniqueness have been contested in Theonomy,
but the counterevidence has not been answered by those making
these assertions. God made a unique covenant with Israel, ruled
uniquely in Israel, made Israel a holy nation, and specially re-
vealed Himself to it—all very true. But God’s laws (made clear
in written form for a redeemed people} were not revealed only to
Israel. They were continually made known through general reve-
lation (Rom, 1:18-32; 2:14-15), and God held the pagan nations
accountable to obey them (Lev. 18:24-27; Gen. 19). Through
Israel these laws were to be made known to the other nations
(Ps, 119:46) as a model for justice and righteousness everywhere
(Deut. 4:6-8).6 So, these commandments can be considered “apart”

6. Bruce Waltke charges that “Bahnsen distorts the purpose of the law when he
alleges that one of its primary functions is to serve as a model of legislation for the
nations” because the text in Deuteronomy teaches “little more than the fact that” sin
is a disgrace to any people (“Theonomy in Relation to Dispensational and Covenant
Theologies,” Theonomy: A Reformed Critique, ed. William S. Barker and W. Robert
Godfrey [Grand Rapids: Zondervan Publishing House, 1990], pp. 81, 82). It is rather
that Waltke reduces the purposes of the law as revealed by God. If Deut. 4 shows the
nations the disgracefulness of sin, then it can surely show the disgraceftalness of civil
sin (as well as personal). There is much more to this passage than Waltke deals with
in his minimizing interpretation (note: “all these statutes,” greatness as a “nation,”
the “justice” of the laws). Moreover, Bahnsen nowhere “alleges” that serving as a
legislative model is a “primary” purpose of the law according to this text, But it is
one among others,
