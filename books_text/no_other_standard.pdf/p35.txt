2
A RECOGNIZABLE, DISTINCT POSITION

A book addressing the place of God’s law in Christian ethics
and employing the term “theonomy” could turn out to take any
number of positions on the subject. This term has been utilized
by serious Reformed scholars (Willem Geesink, Herman Bavinck,
Cornelius Van Til), popularizers (R. C. Sproul), and heretical
writers alike (e.g., Karl Barth, Pau! Tillich). There is no necessity,
then, for identifying “theonomic ethics” as the position taken in
my book Theonomy in Christian Ethics; nevertheless, that label has
come to be conveniently attached to the view advocated there.!
The position is sometimes designated “reconstructionist” as well.?

Who Speaks for the Position?

Theonomic ethics is a definable and distinct school of thought.
That school of thought is unified by certain fundamental princi-

1. Critics of the position were the first to use such nomenclature (cf. D, Dunker-
ley, “What is Theonomy? [privately distributed: MeIlwain Memorial Presbyterian
Church, Pensacola, Florida, 1978]). Aiken Taylor wrote that this outlook “has be-
come recognized by the Lie Theonomy” (“Theonomy and Christian Behavior,” The
Presbyterian Journal, vol. 37, no. 20 (Sept. 13, 1978], p. 9). The “Report of the
Committee to Study ‘Theonomy’” presented to Evangel Presbytery (P.C.A.) on June
12, 1979, stated that “in its narrower sense . . . “Theonomy’ is a definite school of
thought . . . advocated by the writings of Rousas J. Rushdoony, Gary North, and
Greg L. Bahnsen” (p. 3). Many other illustrations are available: e.g., D. A. Rausch
and D. E. Ghismar, “The New Puritans and Their Theonomic Paradise,” The Chris-
tian Century, vol, 100, no, 23 (Aug. 3-10, 1983).

2. “Reconstructionism” (cf. The Journal of Christian Reconstruction) names a broader
theological outlook which includes (at least) a postmillennial view of eschatology, a
theonomic view of ethics, and (usually) a presuppositional approach to apologetics
patterned after C, Van Til.

19
