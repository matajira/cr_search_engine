“God . . . made us sufficient as ministers of a new covenant. . . .
But if the ministration of death, engraved in letters on stones,
came with glory . . . how shall not even more the ministration
of the Spirit be with glory?”

2 Corinthians 3:6-8

“So the law is holy, and the commandment is holy, righteous, and
good.”

Romans 7:12

“As we see it, theonomy . . . overemphasizes the continuities
and neglects many of the discontinuities between the Old Testa-
ment and our time.”

William Barker & Robert Godfrey,
Theonomy: A Reformed Critique (1990), p. 11

“Our study of the New Covenant scriptures has shown us, in
summary, that there are definite discontinuities between the New
Covenant relation to the law and that of the Old Covenant. The
New Covenant surpasses the Old in glory, power, realization, and

finality. . . . The Covenantal administrations are dramatically dif-
ferent . . . but not as codes defining right and wrong behavior or
attitudes.”

By This Standard (1985), p. 168
