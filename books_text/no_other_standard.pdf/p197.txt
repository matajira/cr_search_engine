Religious Crimes, Religious Toleration 181

with those who stand against Christ,” is not one which stands in
conflict with theonomic ethics however. It is not the view of
theonomic cthics that those who reject or resist the Savior, Jesus
Christ, should be punished by the civil magistrate. Nor was it the
view of the Old Testament. In Deuteronomy 18:19, God declared
with respect to anyone who would not heed the words of the
coming prophet whom the Lord would raise up, “I will require it
of him.” In the place of these quoted words, Acts 3:19 interpre-
tively substitutes the Septuagintal words of Leviticus 23:29, thereby
showing us that the way in which God Himself would recompense
the sin of rejecting Christ’s words would be by “rooting him out
from among the people” ~ that is, by cutting him off from the
community by premature death or excommunication. Since this
was not a sanction enforced by the civil magistrate, and since the
Old Testament Jaw in Deuteronomy 18:19 did not assign the
punishment of the unbeliever to the civil authority (but reserved
it for God Himself), Neilson’s remark from Acts 3 that God has
reserved for Himself the punishment of those who stand against
Christ does not contradict or change anything in the Old Testa-
ment law. And even if it did, this would not contradict the
theonomic thesis, for this kind of New Testament alteration of the
Old Testament is precisely what the theonomic position yields to
as the only basis for departing from the law today.

Neilson asks whether “there is an intimation that civil govern-
ment is not to exercise jurisdiction over religious sins.” The an-
swer, obviously, is no. Such an intimation would be a hasty and
irrelevant generalization. Acts 3 deals with a particular religious
sin, not religious sins in general (and remember, all civil crimes
are “religious” in an important sense anyway). And Acts 3 does
not show a repeal of any Old Testament civil sanction against
“religious” offenses anyway. The fact that Deuteronomy 18:19 is
revealed “amidst pronouncements of temporal punishment for
religious abominations and false prophets,” does not indicate in
the slightest that Acts 3 is repealing those civil penalties. Acts
3:23 harmonizes with Deuteronomy 18:19, and that fact has no
bearing whatsoever on abrogating anything else in Deuteronomy
18—unless y. 19 in Deuteronomy 18 already during the Old Testa-
