80 NO OTHER STANDARD

for he spoke of “all scripture” — referring specifically to the Old
Testament, including the Mosaic law —as profitable for instruc-
tion in righteousness (2 Timothy 3:16-17), not merely for doctrine,
The dispensational assumption of discontinuity with the Old Tes-
tament also stands against the teaching of our Lord Himself, who
warned against an antagonistic attitude toward the Old Testa-
ment law: “whoever breaks the Icast of these commandments and
teaches men so shall be least in the kingdom of heaven” (Matthew
5:19),

The Universality of the Law

Contrary to this infallible dictum, dispensationalists such as
House and Ice claim that none of the Mosaic regulations or pre-
cepts are as suck universally obligatory, but were binding only on
Old Testament Israel. With conspicuous selectivity, they hold
that Christians today are under the regulations and precepts only
of the Adamic covenant, the Noahic covenant, and the New Tes-~
tament. Ice and House assert that “the Mosaic law given to
Israel” is not binding upon “any other nation not under the
[Mosaic] covenant.”6

However, the Gentiles clearly were obligated to the same moral
requirements as the Jews, even though the Jews alone enjoyed the
privileges of a special covenantal relationship with Jehovah. On
the one side, the Old Testament indicated that Israel had a
special, redemptive relationship to God, for He Himself said “You
alone of all the families of the earth have 1 known” (Amos 3:2).
On the other side, the nations of the world were morally bound
to God’s commandments, just as were the Jews (e.g., Lev. 18:24-
27). House and Ice miss the significance of this last passage by
confusing the written code of Moses (which the Gentiles did not
receive, obviously) with the moral content of that code (by which
the Bible says the Gentiles were indeed judged). Ice and House

6. H. Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse? (Port-
land, Oregon: Multnomah Press, 1988), pp. 100, 129. Note should be made of their
failure to distinguish a set of commands from the covenantal form, circumstances, pur-
Poses, and trappings in which they are found (eg,, “Since the nations around Israel
were not called to adopt the Mosaic Covenant, it seems evident that the pagan
nations would not be judged by the law of Moses,” pp. 128-129).
