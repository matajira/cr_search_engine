Autonomous Penology, Arbitrary Penology 217

der? It is strange indeed that Henry would abandon his theonomic
commitments at just this crucial ethical point.

Arbitrariness in Selectivity,

in Hermeneutical Principle
What Henry maintained is that “the particularities of statute
jaw in the Old Testament theocracy” were “valid only for the
ancient theocratic society of Israel”; “the ancient theocratic soci-
ety is not normative for Christians.” He claims that those who
would endorse the penal sanctions of God’s law for today do “not
carefully preserve a distinction between theocratic and non-
theocratic government.”® Despite such claims, though, Henry’s
position eventually begins to shift somewhat, and he relents on
any categorical rejection of the civil statutes of the Old Testament
theocracy, subsequently conceding that some “have value” beyond
Old Testament Israel. Which ones would those be? On what basis
would they be re-categorized? Can laws which (on Henry’s hy-
pothesis) are invalidated by God be arbitrarily reactivated by
human authority?? Are we not back to a smorgasbord approach
to the Old Testament law? These are the questions which (repeat-
edly) the critics of the theonomic position on the Old Testament
penal sanctions fail to answer. Given their selective endorsement

8. Henry, pp. 444, 445, 447. This line of thinking has been answered elsewhere
in this book already. In a similar fashion, Brace Waltke claims that “capital punish-
ment for religious offenses . . . were appropriate for Israel’s unique situation; they
are not appropriate in a pluralistic society” (“Theonomy in Relation to Dispensa-
tional and Covenant Theologies,” Theonomy: A Reformed Critique, ed. William $.
Barker and W. Robert Godfrey [Grand Rapids: Zondervan Publishing House, 1990],
p. 85). But how does Waltke know what is “appropriate” or not here? He does not
tell us, and so leaves us simply with his personal opinion. Waltke’s attempt to create
a special class of “religious” crimes is equally arbitrary~as pointed out by John
Frame in another article in the same volume (p. 95).

9. Some theonomic critics have suggested that the Old Testament penal provi-
sions are abrogated (or liable to abrogation) because there are instances where God
did not apply them, e.g., Cain and David (Bruce Waltke, p. 84; Lewis Neilson, Gods
Law in Christian Ethics: A Reply to Bahnsen and Ruskdoony (Cherry Hill, New Jersey:
Mack Publishing Co., 1979], p. 37). Exceptions hardly prove a general policy, of
course! Moreover, it is one thing to say that God has the authority to make such
exceptions, and quite another to presume that man is authorized to do so on his
own. When the “Report of the Special Committee to Study ‘Theanomy”” (submitted
to Evangel Presbytery, P.C.A., June 12, 1979) suggested that the penal law of the
