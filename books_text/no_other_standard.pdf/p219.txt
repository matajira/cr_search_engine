Pluralist Opposition to Theonomy 203

regarding the coin and taxation (Matt. 22:15-22).

As interesting as the discussion of Christ’s answer to His
critics is, Barker’s line of reasoning really does not demonstrate
what he set out to show. In this passage Jesus taught that it is
indeed lawful for political subjects to give tribute-money to Caesar
(cf. v. 17). To infer from that premise that it is, then, unlawful for
Caesar to give tribute to God (enforcing the civil aspects of the
first table of God’s law) is an enormous non sequitur. Barker at-
tempts to squeeze that conclusion out of Jesus’ answer by pointing
to the distinction which Jesus draws between the things belonging
to Caesar and the things belonging to God. But that distinction
in itself was nothing new — certainly not a new divine revelation,
a truth which was unknown or inoperative in the Old Testament
(e.g., Jehoshaphat’s distinction between “Jehovah’s matters” and
“the king’s matters,” 2 Chron. 19:11) —and everyone is aware
that in the Old Testament, where that distinction was taken into
account, the king was indeed obligated to show tribute to God by
enforcing the civil provisions of the first table of God’s law. Conse-
quently, Christ’s reminder of that distinction cannot in itself have
the logical force of revoking such an obligation. Barker's reasoning
does not deduce anything from the text, but rather reads it into
the text from outside.

To make his thesis plausible, Barker would also need to offer
a convincing explanation of why in the Old Testament era Gen-
tile, non-theocratic magistrates were held accountable to the first
table of the law (or first great commandment in its civil applica-
tions), but they are no longer required to do so in the New
Testament. After all, the king of Babylon was indicted (even by
the dead kings over the other nations) for daring to rule in such
a way that he was guilty of idolatry and despising his duty toward
Jehovah (Isaiah 14). Darius decreed that throughout his empire
all men “must fear and reverence the God of Daniel, for He is the
living God and endures forever” (Dan. 6:25-26). Why would
non-theocratic kings éeday be under any ess responsibility than the
Old Testament kings of Babylon and Persia?

We find the New Testament also holding unbelieving civil magis-
trates responsible to honor and act in terms of the first table of
