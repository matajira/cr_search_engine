God’s Law and Civil Government Today 157

view would be harmonious with it, I am sure. Neilands, the reader
will find, may speak of the “general” view, but actually follows a
partisan (and narrow) conception of common grace which suffers
for lack of exegetical and logical credentials (viz., the conception
advanced by Meredith Kline).

The crucial mistake to note in Neilands’ remark, however, is
the false notion that “God’s law” is restricted to “special revela-
tion,” which in turn is precluded as such from communicating
matters of “common grace.” Note the logic of his remark against
theonomy: viz., its view of common grace is deemed wrong be-
cause it includes God’s law, which is a special revelation. Both
steps in this thinking are faulty. The non-ceremonial (non-
redemptive) law of God which defines unchanging moral stan-
dards was revealed not only in the “oracles” delivered by Moses
to the Jews (Romans 3:2), but is also made known to all men
through “general revelation” according to Paul in Romans 1:18-32
and 2:12-15. The divine norms of justice and righteousness are
known by all men, having “the work of the law written in their
hearts.” This law of which Paul speaks is not limited to the Ten
Commandments (see 1:28-32). It is the same law delivered to the
Jews, but through a different medium of communication. Conse-
quently Neilands is simply wrong to reason that the law of God
advocated by theonomists for civil guidance cannot be found
except in special revelation.

Moreover, he is misled to feel that the principles of common
grace could not be found in special revelation, anyway. Does he
not recognize the provisions of the Noahic covenant (e.g., regular-
ity of seasons, execution for murder) as common grace principles?
And are those principles not described in special revelation at
Genesis 9? So there is no reason why something found in special
revelation could not also be a matter pertaining to common
grace—in which case Neilands has no cogent argument against
theonomic ethics, een if the law of God which it advocates for
civil use were somehow tied to special revelation.

Were God’s Laws Only for a
State Identical with His Kingdom?

Against the theonomic view that nations today are obligated
