212 NO OTHER STANDARD

those laws (Hebrews 2:2). It is this conclusion above all which
the critics of theonomy find unpalatable?

Critics have used a large variety of methods to avoid being
driven to an endorsement of the Old Testament penal code. In
the meantime, they have done precious little to propose an alter-
native and Biblically sanctioned approach to the punishment of
criminals in our own day, What little they have to say regarding
this subject is easily faulted for embodying the same arbitrariness
and/or tyranny which characterizes the penology of humanists.
Why should the divinely revealed standards of crime and punish-
ment found in the Bible be unacceptable? When we turn to the
arguments of non-theonomists, we do not find very compelling
answers,

Critics of the theonomic position usually take an approach to
civil justice which originates in human imagination, rather than
God's unchanging word. Consequently, those who take a position
like that of Richard Lewis unwittingly slide into cultural rela-
tivism, He ends up saying with approval that “Different times and
different geographical areas will see different laws and penalties
enacted” — precisely the relativism which, as I have said else-
where, is the logical outcome for those holding to the theological
distinctives of Meredith Kline. We are left with justice-by-
“consensus” (to use Lewis’ own word), rather than the genuine
justice of God. If majorities at different times and places deter-
mine what constitutes “justice,” then the transcendent foundation
for civil law has given way, and there is no logical barrier to

2. They sometimes overstate the theonomic position on the penal sanctions,
trying to make it sound as though these particular Old Testament commands may
not (like others) be modified, qualified or put aside on specific Biblical evidence.
Vern Poythress comments: “At a maximum are claims that penal laws require no
substantial adjustments because of the coming of Christ. Theonomy as popularly
understood invelves such a maximalist position, but it is more accurate to say that
Bahnsen’s general statements concerning penal law are qualified by the places where
he says that we should presume continuity unless we have biblical evidence to the
contrary” (“Effects of Interpretive Frameworks,” Theonomy: A Reformed Critique, pp.
120-121).

3. Personal report on “The Applicability of the Penal Code of the Old Testa-
ment” (privately distributed in connection with a study committee in the Presbytery
of Northern California, O.P.C.), p. 7.
