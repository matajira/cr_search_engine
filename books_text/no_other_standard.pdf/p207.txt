11
PLURALIST OPPOSITION TO THEONOMY

Not everyone uses the term “pluralism” in the way it is pre-
sented in the last chapter, a way consistent with historic Reformed
theology and theonomic convictions. Within the Reformed world.
today there are those who oppose the theonomic conception of
civil government and label themselves “pluralists” (or “principled
pluralists”).! The pluralist position maintains that the state ought
to honor and equally protect the substantial philosophical differ-
ences between all religious perspectives or “faith communities”
by refraining from basing state actions or legislation upon any
single one of them, instead of the plurality of them.? Accordingly,
the law of God revealed in Scripture (Old and New Testaments)
must be precluded from being the moral authority upon which

1. A public interchange between advocates of pluralism and advocates of theonomy
can be pursued in some detail in God and Politits: Four Views, ed. Gary Scott Smith
(Phillipsburg, NJ: Presbyterian and Reformed Publishing Co., 1989). At the consul-
tation on which the book is based, Dr. Gordon Spykman, James Skillen, Paul
Schrotenboer, and Gary Scott Smith represented the pluralist viewpoint, The present
author set forth the theonomic position paper, and the theonomic view was repre-
sented in the contributions made by Kevin Clauson, Carl Bogue, T. M. Moore,
Joseph Kickasola, and Gary DeMar.

2. In light of Dr. Spykman’s essay in God and Polities: Four Views, 1 should make
clear that when I speak of “pluralism” I am referring to what he designates “conjis-
sional pluralismn.” It is this notion that is controverted by many Reformed scholars.
What Dr. Spykman calls “structural pluralism” is so routinely assumed in Reformed
political thinking~and so uncontroversial (even among many non-Christian politi-
cal theorists) — that it is not at afl a distinctive or characteristic mark of his school
of thought. It does not merit special mention or need extended discussion in our
circles. Nor does “structural” pluralism in any logically sound way provide warrant
for “confessional” pluralism; they are quite different matters and answer altogether
different questions.

191
