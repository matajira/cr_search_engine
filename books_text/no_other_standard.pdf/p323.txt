Paythress as a Theonomist 307

sion is desired, he chooses this metaphor or foreshadow; if a
different kind of preconceived conclusion is desired, he chooses a
different metaphor or foreshadow. He can prove anything he likes
in this manner.

Let’s go back to the outset of Poythress’ line of thinking about
holy war and Deuteronomy 13, noticing further signs of such
arbitrariness. His discussion begins with the wholly gratuitous
claim that the city which engages in false worship has committed
“an offense against Israel, not merely against God” (p. 140). But
this is not at all what the text says. Rather, we read that an
abominable thing has been done “among” Israel (“in your
midst”) — not “against” Israel. False worship deprives God of
what is due fo Him; it does not violate any worship due to fellow
men. But Poythress asserts that the crime is “against the congre-
gation, . . . This particular crime is a crime precisely because
of the holiness of Israel” (p. 141; cf p. 302).'° No, “precisely”
because of the holiness of God Himself. The holiness of the nation
was not compromised unless it failed to respond against the idola-
trous behavior (thus becoming guilty by consent).

Moreover, Poythress is being short-sighted. Seduction to idola-
try is far more than bringing pollution into the midst of a holy
nation. It also challenges the final and ultimate authority of the
civil order in the nation, thus undermining the law of God and
dismissing the lawful exercise of the government; it creates ethical
chaos by asserting competing ethical authority and guidance in
the nation.” As such, seduction to idolatry could be treated as a

16. On the one hand Paythress asserts that Deuteronomy 13 is silent as to
whether false worship represents “injury to the state” (p. 303); yet he does not
hesitate to claim that false worship offends “the congregation.” The text is equally
sifent about that claim too, though. Moreover, Poythress says that it is not the
“state,” but the “congregation,” which carries out the penalties of Deuteronomy 13
(pp. 140, 293). This too is arbitrarily read into the text by him. Indeed, there are
textual indications that there is a civil, judicial process involved—eg., diligent
search of the evidence (v. 14), the accuser being the first to put his hand to the
execution (v. 9).

17. For instance, see Peter C. Craigie, who says about Deuteronomy 13:13-19,
“the evil-doers are ‘urban revolutionaries’ in that the action they advocated would
be contrary to the constitution of the state.” The Book of Deuteronomy (Grand Rapids:
William B. Eerdmans, 1976), p. 226.
