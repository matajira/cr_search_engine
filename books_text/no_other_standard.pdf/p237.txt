Autonomous Penology, Arbitrary Penology 221

principle. Moreover, it simply does not conform to Biblical prac-
tice. In the Old Testament, we see the Gentile nations being
condemned, not only for infractions of the Noahic covenant’s
stipulations, but especially for violations of the Mosaic law’s pro-
visions. In the New Testament we see no discriminating restric-
tion of socio-political ethics to the regulations of the Noahic cove-
nant. Jesus bound us (farmers, merchants, teachers, and mag-
istrates) to every jot and tittle of the Old Testament legislation of
God’s will, not allowing us to subtract even the least commandment
(Matt. 5:17-19). Paul was willing to be executed for anything he
had done which was “worthy of death” (Acts 25:11) and not
simply the single crime of murder. The New Testament makes it
clear that homosexuality is still “worthy of death” (Rom. 1:26,
32), as is violent cursing of one’s parents (Matt. 15:4), and many
other civil misdeeds as defined by the law of God (I Tim. 1:8-10).

Civil magistrates are to use “the sword” to avenge God’s holy
wrath against evildoers today (Rom. 13:4). If the “sword” is to
be restricted to punishing murderers according to the Noahic
covenant, though, what exact and just punishment should be
imposed by the state for rape today? Is this question left com-
pletely to human autonomy during the New Testament era since
the Noahic revelation is silent about it? That would be preposter-
ous. But non-theonomists have no way to answer this (and similar) questions
in a way which is objective, publicly predictable, morally just, non-arbitrary,
and textually justified.

But imagine now that theonomy’s critics were to begin to
work hard and creatively to devise some way to justify the state’s
use of taxation today, its distinction between manslaughter and
murder, and its laws against rape (etc.)—as well as to devise
some theological and exegetical way to show that the infractions
for which Old Testament prophets indicted the Gentile nations
(e.g., slave trafficking, violation of loan pledges) were really found
in pre-Mosaic revelation. I predict that such a project would end
up lacking plausibility, but just imagine that it were accomplished
anyway. The theonomist would simply at that point take princi-
ples and premises which were creatively used by the critic and
use the very same kind of creative hermeneutic to show that any of
