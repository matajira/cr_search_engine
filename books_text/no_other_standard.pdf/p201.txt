Religious Crimes, Religious Toleration 185

The pluralism which was promoted and defended by our
Reformed forefathers was not a civil tolerance which countenanced
and protected all religions equally, but rather a civil tolerance for
all branches and denominations within the circle of Christianity. Thus
there were implicit limits on religious tolerance for our pluralist
forefathers. For instance, consider the view of religious toleration
in the writings of John Owen, the Puritan apologist for non-
conformity, separation and ecclesiastical independency. Because
Owen wrote in defense of the Protestant dissenters, it is not
surprising that present-day pluralists like to cite him. When it
came to the relationship of civil power to matters of faith and
worship, Owen championed religious liberty, indulgence and tol-
eration because these are issues governed by the freedom of every
man’s conscience. What Owen maintained, however, is that

God hath not warranted or authorized any man . . . to punish
[any other man] for yielding obedience in spiritual things . . .
as his mind is by them apprehended (i the things themselves,
though mistaken, are such as no way interfere with . . . the findamen-
tal articles of Christian religion. . .).!°

Owen’s pluralism did not prevent him from contending else-
where that “the supreme magistrate, in a nation or common-
wealth of men professing the religion of Jesus Christ, may and
ought to exert his power, legislative and executive . . . to forbid,
coerce, or restrain such principles and practices as are contrary
to [the faith and worship of God] and destructive of them.” And
notice how this Reformed fiuralist supported his thesis in a theonomic
fashion:

Among the people of the Jews, as is known and confessed, God
appointed this as the chief and supreme care and duty of the
magistrate . . . the preservation of that worship by God com-
manded was a moral duty. . . . No revocation of this grant, or
command and institution, no appointment of any thing inconsis-

10. “Indulgence and Toleration Considered” (1667) in The Works of John Oxen,
ed. William HL. Goold (London & Edinburgh: Johnstone and Hunter, 1852), vol.
XIE, p. 530, emphasis added.

11. “Two Questions Concerning the Power of the Supreme Magistrate About
Religion and the Worship of God” (1659), ibid, p. 509.
