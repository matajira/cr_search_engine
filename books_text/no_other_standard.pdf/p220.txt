204 NO OTHER STANDARD

the decalogue. When Herod arrogantly acted in defiance of the
first commandment, permitting and receiving the crowd’s accla-
mation of himself as divine, God clearly displayed His own holy
jealousy and displeasure by striking Herod dead of worms on the
spot (Acts 12:21-23). Likewise Paul condemned the civil ruler
known as “the man of sin” because he dares to conduct his office
in violation of the first table of the decalogue, “setting himself forth
as God” (2 Thes. 2:4), When Barker argues that civil magistrates
ought to honor the second table of the law, but not the first table
today, the distinction which his thesis advocates simply does not
comport with the text of Scripture.

The same judgment applies to Dan McCartney when he at-
tempts to argue from silence against the validity of the state submit-
ting to God by honoring the civil requirements regarding idolatry.
McCartney thinks it is “perhaps worth noting that Paul never
urges people to break the physical idols of non-Christians, as a
direct application of Old Testament civil law would have us do.”
The confusions and mistakes inherent in this comment are many.
To say that Paul never urged such a thing is a long, long way from
proving that Paul did not wish for or endorse such a thing, More
to the point, McCartney has misrepresented the Old Testament
Jaw. It did not require that individual Jewish believers go just
anywhere in the world and physically smash any idol that they
found. The law’s removal of graven images envisioned civil sanc-
tion for removing and destroying them, and envisioned a nation
where such a law was recognized and honored as the law of the
land. That was quite clearly not the situation in which Paul was
living and ministering. Therefore, McCarmey’s argument from
silence (“Paul never urged. . .”) is embarrassing because Paul
would not have urged what McCartney describes — precisely out
of Paul’s respect for the actual demands of the law.

Now then, does the fact that Paul does not send out believers
on vigilante attacks against physical idols prove anything one way
or the other about whether Paul would have longed for or endorsed
nations honoring God and His law, especially the civil provisions
which honor His holy, exclusive and sovereign prerogatives (by
forbidding public idolatry)? Not at all. Those who try to read
