Categories of Old Testament Law 105

This rescues him at the cost of triviality, though. Of course, if
something is applicable “through” something else, it is not being
applied “directly.”

What do all these sloganized expressions actually mcan? The
reader gets the impression that McCartney feels he has made
some kind of important theological point (and made it against the
theonomic way of thinking), but unquestionably he really has
not. His own explanation of what this all means comes down to
this: “it [the case law] is applicable only as a working out of God’s
moral principles, as expression of God’s character revealed in
Christ” — which any theonomist could say as well. None of Mc-
Cartney’s strained discussion gives any evidence or reason to
believe that the case laws of the Old ‘Testament are inapplicable
today (when properly interpreted, of course). Moreover, the Chris-
tological “focus” of the Old Testament does nothing to preclude
the application of the judicial case law to the civil sphere. McCart-
ney emphasizes the law being applied by Christ as Head of the
church — and that is an important and primary truth. But this
same Christ is also “the only Potentate, King of kings and Lord
of lords” (1 Tim. 6:15) according to New Testament theology,
and as such Jesus Christ applies God’s law not only to the church,
but also to all kings and rulers of the earth (see Psalm 2, 72; Acts
12:21-23; 17:7, Rev. 2:27; 19:15-16; 21:24). McCartney’s New
Testament theology is too narrow and restricted.

Chantry’s Case for the
Decalogue Alone (Galatians 3-4)

Walter Chantry began his public interaction with the theonomic
position by openly using a term of negative connotation for it, the
term “legalism.” He claimed that theonomy is “seriously adrift in
the waters of legalism” because it binds on the conscience of
believers “details of O.T. theocratic law which Scripture . . .
regarded as set aside in the New Covenant.”?4

The term “legalism” commonly denotes the view that we are

23. Banner of Truth, issue 178 (July, 1978), p. 31. The same critical label of
“legalism” is tossed about by Dunkerley (pp. 5, 7), Fowler (p. 95), and others equally
disinterested in analysis and accuracy.
