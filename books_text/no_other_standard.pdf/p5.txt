With affectionate memories,
this book is dedicated (at long last) to:

MICKEY AND JUDY SCHNEIDER

And my good friends at
St. Paul Presbyterian Church
in Jackson, Mississippi
