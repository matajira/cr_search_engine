20 NO OTHER STANDARD

ples of Biblical reasoning about ethics (“ethical hermeneutics or
meta-ethics,” if you will) — rather than by unanimity in the par-
ticular application of those principles to concrete issues or cases.3
The theonomic school of thought is also publicly identifiable in
terms of certain recognized spokesmen (teachers, writers, preach-
ers) — rather than in terms of anything and everything anyone
who calls himself a “theonomist” says or does. In these respects,
the theonomic perspective is like any other school of thought with
which we deal as Christian scholars: each has its essential tenets
and known spokesmen.

This should be common knowledge, but unfortunately needs
repeating here. It will help us focus the target at which critics of
theonomy ought to be shooting. In light of these observations,
critics should bear in mind that:

1. To criticize the specific ethical conclusion reached by a
specific theonomist (that is, the particular application of theonomic
principles) is not at all the same as criticizing the theonomic view
(that is, those underlying principles themselues).*

2. Theonomists may readily disagree with each other on par-
ticular issues in normative ethics, and yet all be genuine adherents
of the theonomic perspective and agree on essential points about
how we should reach ethical conclusions.5

3. Regardless of the disagreements or realignments among theonomists over a
number of questions of application, the school of thought “is not so nebulous that
certain leading characteristics cannot be identified.” So say the editors in the "Pref
ace” to Theonomy: A Reformed Critique, ed. William S. Barker and W. Robert Godfrey
(Grand Rapids: Zondervan Publishing House, 1990), p. 9. In the same volume,
though, Bruce Waltke ridicules the theonomic project of applying the Old Testament
law to contemporary society because of the “many differences among theonomists”
{p. 80). Before Waltke goes too far down this path, he had ‘better stop and reflect
upon the many differences between the members of the Westminster faculty (with
whom he wrote this critique of theonomy) on how and whether to apply the Old
Testament to society today!

4. Such principles like: (1) one may not dismiss an Old Testament moral
injunction simply on the basis that it was found in the Old Testament, or (2) one
may not dismiss the authority of God’s moral standards for any area of life—even
society, economics, and politics, or (3) one may not dismiss the validity of God’s law
for civil magistrates based simply on its disharmony with the contemporary cultural
status quo, etc.

5, The attempt to interpret and apply the details of God’s commandments is a very
