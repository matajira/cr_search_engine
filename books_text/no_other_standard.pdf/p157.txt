Separation of Church and State 141

who do things “worthy of death” (Acts 25:11; cf. Deut. 21:22)
—even as the pagans know (Rom. 1:32; 2:14-15). These truths
would be maintained as the conclusions of theonomic thought,
even if Robertson could show that absolutely no similarity exists
between the church-state relation of the old covenant order and
the church-state relation today. Therefore, it seems to me that the
premise which he has labored to criticize in his lectures is not
my position, nor is it an indispensable assumption of my position.

Was Citizenship Coextensive
with the Sacred Community?

Robertson proposes his own conception of the relationship
between church and state in the old covenant, and he contrasts
that with his conception of the relationship between church and
state today. From that basis he criticizes theonomy as allegedly
holding to a doctrine different from that proposed by him. I have
already noted his misconception of the theonomic outlook. Thus
we can best view the argumentative strategy in this part of his
lectures as setting forth biblical truths which cannot be harmo-
nized with the theonomic thesis. His remarks will be treated along
this line, so that they are not automatically disqualified as criticiz-
ing a misrepresentation.

Robertson’s position is that, unlike today, the old covenant
church (cultic functions) and state (civil functions} were unified,
as both were in a redemptive covenant relation with the Lord.
Back then, the state was a redemptive covenant community,
whereas today only the church is a redemptive covenant commu-
nity. Thus the relationship of church to state was different in the
old covenant context than it is today. Accordingly, says Robertson,
we do not follow the Old Testament today in requiring the exter-
nal sign of the covenant (circumcision-baptism) for citizenship in
the state, and the state today does not require all citizens (males
anyway) to participate yearly in the redemptive meal (passover-
Lord’s Supper). These truths, if they be truths, cannot be squared
with a consistent following of the theonomic perspective, thinks
Robertson, and thereby the position is critically faulted. Several
things must be said in response to this line of argument.
