246 NO OTHER STANDARD

and cleansing is argued by the author of Hebrews on other grounds
than the priestly prerequisite of coming from the levitical tribe
(e.g., Heb. 9:10-12; 10:1-2).

Changed Sanctions?

Johnson comes to the point of his article by asking whether a
change in the application of the Mosaic penal sanctions has “also”
been introduced. It must be borne in mind that nothing that has
been said up to this point is either logically or theologically rele-
vant to answering that particular question. To ask whether the
penal sanctions have “also” been changed is to ask, therefore,
whether we have a textually-grounded basis for believing that
about them ~ as we “also” have such Biblical warrant regarding
the other changes (in priestly requirement, sacrifices, and cleans-
ing efficacy). Johnson offers no textual proof (or anchor) of that
opinion at all — not even one clear case that he then could use for
an argument from analogy. Rather his argument rests upon a
misreading of the a fortiori logic of Hebrews 2:2 and 10:29.

Dr. Johnson is entirely correct that these passages in Hebrews
prove (once again) how much more important and significant is
the New Covenant order than the Old. The “greater the grace
revealed in [God’s] words to his people, the greater their liability
should they disregard his voice.” Precisely. But then listen to the
way in which Johnson, without justification, narrows the premise
of the @ fortiori argument of the author to Hebrews: “At issue is
not divine justice in abstract as a model for political jurispru-
dence,” but the Lord’s expectation of people with covenantal
obligations. But that certainly is at stake, if the author’s argument is
intended to be theologically and logically sound! If in aay of the
ways God metes out punishment for transgression, He is arbi-
trary, harsh, lenient or of changing attitude, then one could indeed
entertain the possibility that the threat of eternal condemnation for
spurning so great a salvation offered in the gospel might be “es-
caped” (Heb. 2:3, the leading question here). Bué if universally
valid and unchanging justice does not characterize even the capi-
tal crimes of the Old Covenant order, then the New Covenant
(especially with its greater power or emphasis upon grace) could
