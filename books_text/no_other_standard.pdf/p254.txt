238 NO OTHER STANDARD

separate from ai! that would render a worshiper unfit to enter
God's presence” (emphasis his). Three words in particular expose
the erroneous nature of this reasoning: “certain” (some, not all),
“entailed” (theological or conceptual necessity), and “ali” (univer-
sality, emphasized by the author).

What does the Bible include in “all that would render a wor-
shiper unfit to enter God's presence”? If we think first in terms
of Biblical literature, certain key passages come to mind. In Psalm
15, David asks who may dwell in Jehovah’s holy hill (v. 1), and
his answer excludes (by implication) anyone who takes up a
yeproach against a neighbor, slanders, breaks promises, or lends
money upon interest (vv. 3-5).

In Psalm 24, David again asks who may ascend into the hill
of Jchovah and stand in His holy place (v. 3) — however his
concern here (which saps Johnson’s argument of strength) is for
the holiness of God which is appropriate to the entire world due to
God’s tabernacle on earth (vv. 3, 7-10): “The carth is Jehovah’s
and the fulness thereof, the world and they that dwell therein” (v.
1). We learn that anybody on earth who has sworn deceitfully (v.
4) is rendered unfit to enter God’s presence (typified at the taber-
nacle). If we ask what the Bible includes in “all” that would render
someone unfit for God’s presence, and we think more broadly and
theologically now, the answer would be any sin of any sort at any
time or place at all. Dr. Johnson knows this. “God is light; in Him
there is no darkness at all” (1 John 1:5). “The evil man shall not
sojourn with” Jchovah (Ps. 5:4). His “cyes are too pure to look
on evil” (Hab. 1:13). Therefore all who are under the curse of sin
will one day be ordered “Depart from Me into the eternal fire”
(Matt. 25:41) — “shut out from the presence of the Lord” (2 Thes.
1:8-9). “Nothing impure will ever enter” the cternal city of God
— “nor will anyone who does what is deceitful” (Rev. 21:27; cf.
Psalms 15 and 24!).

Back then to Johnson’s reasoning. He says that Israel’s penal
sanctions (actually, only “certain” of them) expressed a height-
ened responsibility to separate from ail that rendered a person
unfit for God’s presence. From what we have seen in Scripture,
this means that the penal sanctions in Israel were enforced against
