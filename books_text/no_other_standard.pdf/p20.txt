4 NO OTHER STANDARD

justice or socio-political morality (a guide for public reform in our
own day, even in the area of crime and punishment).

The book was met with far greater enthusiasm than I ever
reasonably expected —and a handful of very strong reviews. I
never anticipated it to be a piece of literature which appealed to
a wide reading audience. The subject matter had an academic
cast (exegetical and theological reasoning about the normative
standard of conduct), the text was long and detailed (justifying
the occasional charge of overkill), and the prose was sometimes
ponderous and at other times polemical. Still, for all those genuine
drawbacks, it obviously touched —and continues to touch—on
matters of vital moral concern to others like myself. It also occa-
sioned “spirited” opposition (to put it politely).

Yes, but what censors have done generally for pornography,
the critics have done for theonomy. They have brought it much
greater attention than it would have gained, encouraged greater
inspection, and in the end made it much more popular.

The criticisms have come in all shapes and sizes, from many
quarters, and have approached the subject from a wide variety
of angles. Some of them have been of charitable spirit and respon-
sible in their scholarship, obviously searching for fidelity to God’s
word, Many have displayed lesser qualities. This volume is my
grateful response to all of the kinds of critics — writers who in one
way or another have tried to analyze, understand, and exegeti-
cally refute the position, or (on the other hand) discredit, obfus-
cate, distort, censor, or frighten the public about what has come
to be called the theonomic viewpoint.

In some institutional settings, the more ignoble (and usually
less competent) critics have minimally gotten their way, while in
the broader marketplace of ideas, they have steadily undermined
themselves. By returning the favor — and now criticizing the crit-
ics — perhaps I can be of some small service in explaining why it
seems that the more Christians actually study the issues involved.
the more they see the Biblical strength and warrant for theonomic
ethics.

My hope is that this effort will be appreciated by friends and
foes alike, for it should in some measure clarify matters, focus on
