70 NO OTHER STANDARD

tion).!8 This is understandable, but his discussion does not estab-
lish what he hopes.

Speaking of the conflict between the operating assumption of
theonomists (continuity) and that of non-theonomists (discontinu-
ity), Johnson proposes that we can skip the issue of which side
bears the burden of proof in cases of New Testament silence.
That is, we should not accept either one of their hermeneutical
presumptions. This sounds nice, but quite clearly one cannot
logically work on both assumptions; moreover, in the face of New
Testament silence, one cannot refrain from using one operating
hermeneutical presumption or the other. It is an objective, non-
negotiable fact that presumed continuity and presumed disconti-
nuity are logically incompatible, and that there are no other
alternatives (except of course to refrain from interpreting the New
Testament at all). The option proposcd by Johnson (viz., no
choice) is simply not available, and we fool ourselves if we pretend
that we have adopted it.’*

Johnson is wrong to think that what he has proposed is
neutral regarding a choice between opcrating assumptions. He
concludes: “we must rest our convictions on the statements, not
the silences, of the New Testament.” Notice very well: the state-

13. “The Epistle to the Hebrews and The Mosaic Penal Sanctions,” Theanemy: A
Reformed Critique, pp. 172-175, It is disappointing to sce Johnson attempting to base
his argument on New Testament silence. Notice the logic of his essay: “The question
whether the penal sanctions should also instruct the state as it is charged to admini-
ster justice to persons within and without God’s covenant is not explicitly addressed
in the New Testament.” To this absolute silence he adds another, relative silence — the
fact that the New Testament less often addresses the responsibilities of political rulers
{and “more often and more explicitly” those of political subjects). And inta these
silences he reads his intended conclusion, namely “The New Testament’s minimal
direction to governmental officials does not support the view that the Mosaic penal-
ties should be enforced. . . .”

14, Johnson’s argument confuses the (antecedent) operating hermeneutical rule
with the (subsequent) results of specific interpretation. He says that the ewo general
approaches to interpretation (presumed continuity, presumed discontinuity)“. . . do
not help us understand the precise character of the continuity and discontinuity in God's
revelation.” Precisely! They are only rules of operation, not preconceived conclusions.
This would be like complaining that the rules of baseball] do not “really help us”
know anything precise about who will win the World Series, so we don’t really need
such rules anyway!
