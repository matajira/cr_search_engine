Separation of Church and State 147

tion, the Lord’s Supper (passover), I am not insisting on this
point-by-point carry-over of conceptions as a theonomist; I am
only showing that Robertson’s line of argument, which uses such
a carry-over device, is not telling against my position after all.

(3) Likewise, when Robertson argues that the theonomic per-
spective would require that the state enforce the Lord’s Supper
for every citizen on a yearly basis, he is not only misconstruing
the theonomic perspective, he is not utilizing assumptions which
are accurate for the Old Testament. As we have seen above, not
every citizen took the passover meal in Israel (the uncircumcised
sojourncrs), and further, as far as I can tell, there was no political
enforcement for the eating of passover anyway. Hence there is no
reason to think that there should be civil penalties today for failing
to come to the Lord’s Supper, even if one insists (as I do not) on
a point-for-point transference of Old Testament concepts into the
New Testament era.

(4) It is worth adding here that, even if Robertson’s attempted
reductio ad absurdum arguments against Theonomy had a more reli-
able foundation (i.e., even if Old Testament citizenship required
circumcision and the Old Testament state enforced passover par-
ticipation), there would be every reason to resist the conclusion
that Theonomy requires the state to take such an interest in baptism
and the Lord’s Supper. In the nature of the case, circumcision
and passover were regulations of a special kind; it would be the
fallacy of sweeping generalization to lump them in with ordinary
moral laws which continuc to be binding today since these regula-
tions belong in an extraordinary category — namely, the category
of “ceremonial” or “restorative” laws, With the coming of the
reality foreshadowed by the Old Testament system, with the in-
auguration of the age of the new covenant, the sacramental signs
of the covenant have been changed, and the definition of the
visible church or covenant community has been altered. The fact
that what had some unavoidable connection with civil affairs of
the state in the Old Testament now pertains only to the reorgan-
ized redeemed community, the church, would not be any embar-
rassment to the theonomic thesis.

Tf Robertson were correct that there is a discontinuity between
