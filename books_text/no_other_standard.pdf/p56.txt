40 NO OTHER STANDARD

certain efforts which have been made to chronicle the history of
the “theonomy movement” or individuals within it. Stories have
been fabricated, gossip and hear-say accepted as fact, and embar-
rassing errors about historical details propagated by critics who
were more interested in making a negative report on theonomy
than in accurate research and reporting.’ Similarly, maligning is
operative when theonomic critics tar everybody who advocates
theonomic tenets with a broad brush which applies (if at all) only
to some, Charges like this can become very personal and allege
to find an ignoble spiritual condition — such as that theonomy
has a low view of the church,’ of marriage, etc, Some writers and
teachers have been so vehemently against the theonomic position
that responsible scholarship and careful treatment of the facts
have taken a back seat to defaming the theonomic heretics. Sig-
nificantly, such opponents have usually not produced theological
analysis and refutation to show good reason for their strong
feelings.

Overkill, Vehemence, Name-calling

The critics of theonomy have hardly been a model of restraint
in speaking of their opponents. Gary Long has accused theonomy
of Judaizing the New Testament; Albert Dager asserts that it is
promoting “a modern Phariseeism.”? Ice and House published
that theonomy should be rejected simply because of the “possibil-
ity” that it might be guilty of moralism, unprincipled pragmatism,
apostasy, compromise with the world, and permeating the faith
with humanism!!° Walter Chantry accuses theonomists of speak-

7. David Watson has written of the “movement” in The Outlook, having previ-
ously written a master’s thesis on the subject. The thesis was riddled with fabrica-
tions and falsehoods. Another example of negligent historiography can be found in
House and Ice, Dominion Theology, which makes one factual blooper after another
about Rushdoony, North, DeMar, myself, etc. Cf Howe Divided, pp. 83-84.

8. E.g., Muether, pp. 253-255, 258.

9. Gary Long, “Biblical Law and Ethics: Absolute and Covenantal,” presented
to a Baptist Council in 1980, serialized in Sword and Trowel (1980-81), and published
by Backus Books of Rochester, New York; Dager, p. 200,

10. Dominion Theology, pp. 335, 339, 340, 341, 342, $44, 349-350, 356, 374-375,
377, 388, 390,
