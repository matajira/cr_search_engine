Autonomous Penology, Arbitrary Penology 227

mischievous. The civil sanctions of the law were not executed in
the death of Christ, as Paul’s post-resurrection endorsement of the
state’s use of the sword clearly shows (Romans 13:4).

Has Excommunication or Divorce
Replaced the Civil Sanctions?

From time to time non-theonomists want to argue that di-
vorce and excommunication have replaced the Old Testament
civil penalties for adultery, incest, and idolatry; to support this
claim, appeal is made to passages like Matthew 19:9; I Corinthi-
ans 5:13 and 6:9-11.!” Such appeals do not show annulment of the
Old Testament penal laws, however, for such passages direct
homes and churches in what they themselves can do regarding
such offenses; they do not nullify what the civil authorities ought
also to do. In fact, direction to homes and churches may be
needed in these areas just because the state has abdicated its legiti-
Mate responsibility regarding them. It would hardly be logically
sound to argue from what the state is not doing to what it ought
not ta do—just as it is a non sequitur to argue from directions
addressed to the church or home to a repeal of completely other
directions (not even mentioned in the passage) addressed to the
state. Moreover, it is a notorious logical fallacy to argue from
silence, especially if one is a covenant theologian. The errors
involved in claiming that in the New Testament divorce and
excommunication have replaced Old Testament civil sanctions
are discussed in Theonomy (pp. 97ff., 458M). One could just as
faliaciously argue that since the church is given direction about
disciplining thieves (e.g., I Cor. 5:11; Eph. 4:28) and is not encour-
aged to turn over converted thieves to the state for punishment
{I Cor. 6:10-11), therefore the New Testament repeals Old Testa-

ment authorization for the state to punish those who steal!

17, Exg., Peter Masters, “World Dominion: The High Ambition of Reoonstruc-
tionism,” Steord and Trowel (May 24, 1990), p. 17. When Masters argues (from
silence!) that “Nowhere in the New Testament epistles is there the slightest hint of
an ancient Israelite punishment being applied in the church” (emphasis added), he
reveals that he is not familiar with the position which he is intending to criticize,
Theonomy explicitly rejects any idea that the churck applies the civil penalties of the
Old Testament order!
