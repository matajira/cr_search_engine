6 NO OTHER STANDARD

A number of taped lectures or series, including three courses on
Christian ethics or political ethics, present the theonomic outlook
and answer questions about it.

The view has also been nicely summarized and presented by
a number of other authors over the past few years.’ The present
volume is not the first publication to respond to the critics of the
theonomic position.® Readers should be aware of the following
other resources:

The “Preface to the Second Edition” of Theonom® (1984) was a
brief, systematic answer to those who had misrepresented or at-
tempted to refute the theonomic approach to ethics up to that
point.

Chapters 29-30 in By This Standard! (1985) responded to argu-
ments against the general validity of the Old Testament law, and
then against the political use of that law. Various criticisms were
also taken up and answered in previous chapters dealing with

6, These are available from Covenant Tape Ministry, 24198 Ash Court, Auburn,
CA 95603. For example:

“The Immutability of God’s Commandments” (#339), “Paul’s View of the Law*
(#341), “Separation of Church and State” {#346}, “Capital Punishment” (#352),
“The Theonomic Thesis Presented to Dispensational Pluralists” (#171-174), “Chris-
tian Ethics” (#256-276, or #277-294), “Christian Political Ethics” (#295-302}, “Auton-
omy vs, Theonomy” (#515-518), “The Theonomic Approach to Ethics” (#303-308),
“Responsible Living Today” (#309-315), “Christian Conduct” (#316-321), “Theonomic
Ethics Explored” (#322-325), “A Theanamic View of Politics” (#326-328), “A Re-
constructionist View of Civil Law” (#329-331).

7. For example, see the following works by Gary DeMar: God and Government, 3
vols. (Atlanta: American Vision Press, 1982, 1984, 1986), The Reduction of Christianity,
with Peter Leithart (Adanta: American Vision Press, 1988); The Debate Over Christian
Reconstruction (Atlanta: American Vision Press, 1988).

8. At the end of the Preface to the expanded edition of Theonomy, I referred to a
volume which I had written and was awaiting publication: The Debate Over God's Law.
This was a very long and detailed reply to critics up to that point (1983) — actually
impractically long (e.g., over 125 pages alone on two critics of the theonomic exegesis
of Matthew 5:17-19). The intended publisher eventually decided against the expense
of publication (although making photocopies of chapters available). The present
volume will have to take the place of the previous project. This allows me to take in
a wider scope of critics (those since 1983), but does not permit the same depth of
analysis. Some portions of the earlier manuscript have been utilized or boiled down
for inclusion in the present work,

Q, Theonomy in Christian Ethics, expanded edition, pp. xi-xxvii.

10. By This Standard: The Authority of God's Law Today, pp. 303-340.
