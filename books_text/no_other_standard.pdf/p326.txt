310 NO OTHER STANDARD

Poythress to infer that the passing of Old Testament Israel as a
holy nation (with the temple, promised land, etc.) automatically
means the passing away of the civil use of Deuteronomy 13.2! His
reasoning is arbitrary and inaccurate.

Further examples of arbitrariness in Poythress’ discussion of
Deuteronomy 13 may be given. He claims that “this passage
prefigures Christ’s own war against Satanic hosts” (p. 148); “Deu-
teronomy 13 and 17 give us a foreshadowing of the triumph of
Christ’s rule through the gospel” (p. 356). But there is not the
slightest textual or literary hint of such a connection between Deuteronomy
13 and the relevant New Testament teaching. Likewise, he alleges that
the separation of Israel as a holy nation, which Deuteronomy 13
enforced, “foreshadows the judgment of the end of the age” (p.
153). But once again, there is no textual evidence provided from Scripture
Jor this claim; Poythress cites 2 Thessalonians 1:7-10, but there is
no suggestion of a connection with Deuteronomy 13 in that text
at all. This is not the authority of God speaking to us, but the
artful imagination of Poythress. Suggestions such as these, draw-
ing parallels between Deuteronomy 13 and spiritual warfare today
or the final judgment, may have some homilctical value; on that
level, they are perhaps unobjectionable.** The principled objec-

21. The Old Testament shows us that God punishes the idolatry of civil rulers
outside of Israci (e.g., Isa, 14), The rulers of the nations belong to Jehovah Himself
(Ps, 47:9), and He specifically orders all rulers and judges of the carth to serve Him
with reverence (Ps. 2:11). They receive their authority from Him (Jer. 27:5-6), and
thus they should turn around and honor “the King of heaven” (Dan. 4:34, 37). They
are looked upon, even autside the holy nation of Israel, as the servants of Gad. Civil
magistrates can even be called “gods” (Ps. 82:1) because they represent Him (cf.
Rom. 13:1, 4). It is at least theologically plausible, then, that God expected civil
rulers anywhere in the world to protect His prerogatives and punish false worship
under their jurisdictions (chus applying Deuteronomy 13). Paythress never really
addresses this in any convincing or pointed way. Why shouldi’t God expect states
ta avenge open rebellion against Him as much as He expects them Lo avenge open
rebellion against other men? There may be a Biblically grounded answer to that
question, but Poythress has yet lo give it.

22. For instance, I do not disagree with Poythress, but have taught for years, that
the manner of conquest seen in the Old Testament (viz., holy war) has changed in
the New Testament (to proclamation and persuasion) ~ without arbitrary typolo-
gies, but on the basis of the Biblical text (e.g., 2 Cor. 10:3-5). I must, however, decry
the way in which Poythress turns this theological truth into a shameful false antithe-
sis, when he asks “Do we follow the Great Commission? Or do we use state laws to
