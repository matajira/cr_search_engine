140 NO OTHER STANDARD

argues against Theonomy by adducing such differences in his third
lecture, he is not doing any damage to my position whatsoever. I
could easily grant those differences and retain the theonomic prin-
ciple, for I do not see Theonomy as first or primarily or essentially
a doctrine about church-state relations — much less a doctrine
asserting identical relations in Old Testament and New Testa-
ment.

(2) Naturally, given the above comments, I cannot agree with
Robertson’s reasoning when he says that the premise of essential
identity between church-state relations in Old Testament and
New Testament is the indispensable foundation for theonomic
conclusions about the penal sanctions of the law. Those conclu-
sions can be drawn regardless of one’s view of the church-state
relation in the Old Testament, for Theonomy argues on the basis
of the objective obligation of all magistrates to perform public
Justice. Vf the agent of God’s wrath against evildoers in the old
covenant order exemplified some unique church-state relation (even
if he were a priest-king, or if there were no difference at all
between the church and state in Israel), the fact would remain
that agents of God’s wrath today would still be bound to the
same objective standards of public justice laid down by God (even
though they are not priest-kings or do not function in a church-
state).

Theonomy’s point is that God does not have a double stan-
dard of justice in society. Rape is wrong, whether in Israel, Nin-
eveh, or New York. And punishing rapists too leniently or too
harshly is wrong for magistrates, whether in Israel, Nineveh, or
New York. If God has not revealed objective standards of justice
for crime and punishment, then magistrates cannot genuinely be
avengers of God’s wrath against evildoers. They could only avenge
their human anger against those who displease them, without any
assurance that genuine evildoers are receiving a just recompense.
In that case the “sword” would truly be wielded “in vain” (Rom.
13:4), and good people would have a real reason to fear (v. 3).
The criminal standards of the Old Testament are God’s objective
standard of public justice, prescribing for every transgression its
“just recompense of reward” (Heb. 2:2) and executing only those
