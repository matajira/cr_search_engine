Change of Dispensation or Covenant 7

cies, ritual ordinances, types and foreshadows that anticipated the
Savior and His redeeming work. The gospel proclaims the accom-
plishment of that which the law anticipated, administering God’s
covenant through preaching and the sacraments. The substance
of God’s saving relationship and covenant is the same under the
law and the gospel.

Scripture does not present the law-covenant as fundamentally
opposed to the grace of the New Covenant. For example, consider
Hebrews 3-4, According to New Testament theology, why was
God displeased with the Israelites so that they could not enter the
promised land? The answer is that they were disobedient (3:18),
but this is the same as the answer that they were lacking faith
(3:19)! They had gospel preached to them, even as we do (4:2),
but they failed to enter into God's promised provision because
they failed to have faith (4:2) — which is just to say, they were
guilty of disobedience (4:6)! You cannot pit faith and obedience
against each other in the Old Covenant; they are different sides
of the same coin — even as in the New Covenant (James 2:14-26).
Paul asks quite incredulously, “Is the law then against the prom-
ises of God?” Should the grace of the Abrahamic covenant be seen
as contradicted by the law revealed by Moses? The Apostle’s
answer is “May it never be!” (Galatians 3:21).3

The law was never intended to be a way of works-righteous-
ness, as Paul goes on to say. By her selfrighteous effort to gain
merit and favor before God by obedience, Israel “did not arrive
at the law” at all! (Romans 9:31) And why not? “Because they
sought [righteousness] not by faith, but as it were by works” (v.
32). Paul knew from his personal experience that he needed to die
to legalism, to the use of the law as a means of merit before God.
And how did Paul learn that lesson? Listen to Galatians 2:19. “I
through the law died unto the law, that I might live unto God.” It

3. For an excellent exegetical discussion of this text, see Moises Silva, “Is the
Law Against the Promises? The Significance of Galatians 3:2] for Covenant Continu-
ity,” Theonomy: A Reformed Critique, od. William S, Barker and W. Robert Godfrey
(Grand Rapids: Zondervan Publishing House, 1990), chapter 7. What Dr. Silva
teaches is perfectly consistent with and supportive of the theonomic position on the
relationship of Old and New Covenants. One only wonders, therefore, why it is
included in a “Critique” of theonomy.
