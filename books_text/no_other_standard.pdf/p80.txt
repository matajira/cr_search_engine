64 NO OTHER STANDARD

God’s commands should apply to society.6 Because I do not
endorse what he describes (but have publicly criticized some of
these very things), Rausch’s criticism simply barks up the wrong
tree. The fact remains that even adherents of the best theological
insights have (and will) make mistakes in applying them. But we
should recognize this principle of common sense: it is unreason-
able for a sick person to abandon himself to poison simply because
doctors have sometimes misapplied good medicine. Rausch and
other critics should apply that dictum by analogy to their “horrid
example” approach to criticizing theonomic ethics. I prefer imper-
fect efforts in society to use God’s righteous commandments to the
destructive (and in principle uncorrectable) use of fallen man’s
unrighteous ones. The consequences of the latter are available for all
to see today. Even as extreme an example as the Salem witch
trials (where, once, twenty people died) would not be worthy to
be compared to the one and a half million babies which are
slaughtered by American humanism every year, or the sixty mil-
lion (plus) people who have been killed by Eastern Communism
in this century. Critics of theonomic ethics have lost all sense of
proportion in selecting the theonomic theory of social ethics for
their expenditure of ad hominem criticism.

Begging the Question

Critics of theonomic ethics have over and over again offered
unwitting illustrations of the fallacy of begging the question in
their dispute with the theonomic position. Their manner of criti-
cism ends up telling us something about the critic personally — his
own beliefs and assumptions —but nothing about the objective
state of affairs in the debate or the merit of any case made on one
side or the other. The way in which critics have often done this
is by appealing to (1) their subjective impression of the thrust of
the Biblical text and/or by appealing to (2) what the Bible does
not say {ic., where it is silent). Upon such weak and flimsy
foundations ~ personal impressions and textual silence — critics

6. Douglas E. Chismar and David A. Rausch, “Concerning Theonomy: An
Essay of Concern,” journal of the Evangelical Theological Society 27, 3 (Sept., 1984): pp.
315-323.
