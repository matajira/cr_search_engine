A Recognizable, Distinct Position 25

criticisms go to the heart of the matter, Ice and House took the
low road here instead.

In short, critics of theonomy ought to focus on the generic
school of thought (its essential tenets), selecting those presenta~
tions or particular applications which are representative of its key
published proponents and which show the school of thought at its
best. Where such protocol has not been observed by those who
have written against the theonomic school of thought — and it has
been often — I will simply request the fair-minded reader to disre-
gard the criticism.

Is the Position Overstated?

David Neilands is one of many critics who object to the “un-
qualified language” used by theonomists in stating that the law
of God remains binding in the New Testament, when in fact they
turn around and make exceptions by holding that certain details
of the law are no longer to be followed.'8 However, Theonomy in
Christian Ethics explicitly stated in what sense and how the en-
dorsement of the Old Testament jaw was to be qualified (even if
the book may not have done so when and how Neilands would
demand), and so we can only invite greater attention to detail on
the part of critics who feel there is a reversal on the part of
theonomists. Moreover, if Neilands objects on principle to any
unqualified statements about the law’s continuing validity, he
would be forced to censure this declaration as well: “truly, not one
jot or one tittle will pass away from the Law until heaven and earth
pass away.” I submit that theological generalizations and state-
ments which do not explicitly and immediately mention all of their
relevant qualifications should not be universally condemned ~ lest
we condemn our Lord Himself?

 

punishment even today (pp. 39, 73-74). It is hard to find other theonomists who
agree with Rushdoony on this point (although that does not make him wrong), and
it is easy to find other thconomists who can present cogent counter-arguments to
North (although his position might be righe).

13, David Neilands, “Theonomy and Its Unqualified Language” (privately pro-
duced and distributed, in connection with a study committee on theonomy in the
Presbytery of Northern California, O.P.C., 1982).
