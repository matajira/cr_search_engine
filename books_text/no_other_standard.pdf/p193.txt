Religious Crimes, Religious Toteration W7

Has the New Testament Revoked the Sanction
Against Apostasy? (Hebrews 2, 10)

At one place in his monograph, Neilson does offer an attempt
to find a more specific Biblical repeal of the selected “religious”
offenses mentioned in his critique. Since this is the sort of counter-
argument to the theonomic position which alone can succeed, if
any can, it is incumbent upon us to give it due analysis and
reflection. Has Neilson presented a divine authorization in Scrip-
ture for an exception to the general validity of Old Testament
laws or penal sanctions — that is, a New Testament revocation for
part of God’s law?

Neilson first contends that in Hebrews 2:2-3 and 10:28ff. “there
is some plausible intimation that Moses’ law for apostasy has
been repealed.” He construes the passages “as contrasting a for-
mer temporal judgment for violating the Old Testament law with
a now far greater judgment at the great day for neglecting the
finally revealed salvation in Christ”; the punishment for apostasy
has now been “relegated to the eternal.” These two passages in
Hebrews, according to Neilson, teach that a particular sin which
used to be given civil punishment is no longer to be punished in
that way, but now awaits only eternal condemnation. The par-
ticular sin in mind is that of “rejecting Christ,” “apostatizing from
Christ,” or “neglecting the. . . salvation in Christ” ~ although
Neilson would need to speak of the Old Testament analog in
somewhat different words. It would appear that these two pas-
sages in Hebrews could be made to teach what Neilson claims
only by numerous alterations or misconceptions,

In the first place, Neilson either does not properly understand
the Old Testament penal sanction for apostasy, or’else he con-
flates two separate matters when he speaks of apostasy receiving
divergent treatment in Old and New Testaments. What is the
apostasy in view in Hebrews? It is a change of belief and commit-
ment, a retraction of profession, and a forsaking of the assem-
bly —and all of this as centered on the messianic person and
work of Jesus. What is important to observe is that the Old
Testament law did not assign civil sanctions for this kind of sin in
the first place. When we speak of the civil punishment of “apos-
