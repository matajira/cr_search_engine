Introduction to the Debate 3

The result was the book Theonomy in Christian Ethics.’ In it 1
offered a lengthy, affirmative answer to the question of whether
the moral standards (laws) of the Old Testament dispensation
were still morally authoritative today, along with New Testament
teaching, and if so, whether they provided the Christian with
socio-political norms for modern culture. It was not a novel thesis,
but one sanctioned for over five hundred years by many leading
Reformed theologians. Advocating it— and especially its political
usefulness —in the pragmatic and relativistic milieu of the late
twentieth century was, however, novel. R. J. Rushdoony must be
credited for being a courageous (and ncarly lone) voice for this
pro-nomian, covenantal view of the Old Testament’s social rele-
vance in my generation.? Although I do not agree with everything
he has written, the more I studied and reflected upon the essen-
tials of his outlook, the more I realized that they had firm Biblical
support, regardless of their unpopularity.

Theonomy in Christian Ethics argued that God’s word is authori-
tative over all areas of life (the premise of a Christian world-and-
life view). It argued that within the Scriptures we should presume
continuity between Old and New Testament moral principles and
regulations until God’s revelation tclls us otherwise (the premise
of covenant theology). It argued therefore that the Old Testament
law continues to offer us an inspired and reliable model for civil

1. Greg L. Bahnsen, Theonomy in Christian Ethics (Nutley, New Jersey: The Craig
Press, 1977), After a couple of reprints (1979, 1983), the book was published in
expanded and hardback form (Phillipsburg, New Jersey: Presbyterian and Reformed
Publishing Co., 1984). The first draft of this manuscript had actually been submitted
ta the publisher in 197] (afer my first year in seminary}, and a few sections were
revised and expanded as my master’s thesis at Westminster Theological Seminary.
After a series of typesetting and printer delays, the book appeared in 1977; by that
time 1 was finished with my doctoral exams and had taken a position teaching
apologetics and ethics at a theological seminary.

2. See for instance his massive Institutes of Riblical Law (Nutley, New Jersey: Craig
Press, 1973) and his popular cssays, Law and Liberty (Fairfax, VA: Thoburn Press,
1971). In 1974 the Journal of Christian Reconstruction began to be published twice a
year with Dr. Gary North as editor. Interestingly, the view that the principles of
God’s law (summarized in the Decalogue) provide a total view of ethical life and a
fundamental basis for social reform was advanced carlier in this century by Elton
Trueblood. Trueblood’s little book on the ten commandments carried the proleptic
tile Foundations for Reconstruction (New York: Harper and Brothers Publishers, 1946)!
