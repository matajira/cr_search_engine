44 NO OTHER STANDARD

certainly would not want to distort the precious truth of God by
mishandling it in an incorrect or oversimplified fashion. But there
is some satisfaction in knowing that for over a decade now, not
one critic who has leveled this kind of charge has given any
example of an overlooked, relevant factor which could not already
be found in my writings or lectures. (There may still be some, of
course.) Those who have suggested oversimplified reasoning have
not pinpointed the fallacious logic or poorly conceived premises
(as yet anyway),

These two different kinds of criticism have something in com-
mon: namely, neither one of these criticisms refutes the theonomic
position, een if the criticisms turned out to be warranted.!* That
is, in the nature of the case, such criticisms do not undermine he
truth of the basic theses of theonomic ethics. They simply show
that there is more to say about the subject or that it should be
given less emphasis. And that is fine. I would simply insist that
we Say as much as theonomy maintains, when (and where) it is
appropriate to deal with God’s law or the subject of the civil
magistrate.

Is the Appeal to God’s Immutability Simplistic?

The theonomic approach to ethics has been thought to be too
simplistic when it appeals to the immutability of God’s character

14, This is repeatedly the case in a recent “critique” of theonomy by certain
faculty members at Westminster Theological Seminary. Readers have been given the
impression that Theonamy: A Reformed Critique, ed. William S, Barker and W. Robert
Godfrey, is a decisive refutation of the theonomic position. Advertisements say that
they have “written the book” (tte book, not “a book”) on the subject of theonomy.
However, much of the book is given to questions of emphasis in Biblical theology,
cautions against simplistic hermeneutics, or cxhortations that all parties not be lazy
but do their difficult homework in properly interpreting and applying the Old
Testament law today (eg., articles by Frame, Poythress} — all of which may be well
and good, without demonstrating whatsoever that the distinctive tenets of theonomic
ethics are objectively mistaken. The “critique” curns out ta be of certain possible
dangers that could arise from the position, not of theonomy as a theological position
as such. After warning that theonomists (and intrusionists of the Kline-variety)
should not be satisfied with their “initial impressions” of a particular text but
understand the “whole warp and woof of God’s revelation,” Poythress recognizes
that, nevertheless, the theonomic reading of Scripture may be “true as far as [it]
goes” (p. 119).
