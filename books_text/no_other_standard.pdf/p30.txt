14 NO OTHER STANDARD

can be made against the tyranny of rulers and the anarchy of
overzealous reformers alike (#9). Since Jesus Christ is Lord over
all (cf. #3), civil magistrates are His servants and owe obedience
to His revealed standards for them (#9). There is no Biblically
based justification (cf. #5) for exempting civil authorities from
responsibility to the universal standards of justice (cf #7) found
in God’s Old Testament revelation (#10). Therefore, in the ab-
sence of Biblically grounded argumentation which releases the
civil magistrate from Old Testament social norms (cf. #5, #6), it
follows from our previous premises that in the exercise of their
offices rulers are morally responsible to obey the revealed stan-
dards of social justice in the Old Testament law (#11). This does
not mean, however, that civil rulers have unlimited authority to
intrude just anywhere into the affairs of men and societies (#11
#12); their legitimate sphere is restricted to what God’s word has,
authorized them to do—thus calling for a limited role for civil
government. Finally, Christians are urged to use persuasive and
“democratic” means of social reform —nothing like the strong-
arm tactics slanderously attributed to the theonomic program
(#12).28

In this book, ¥ will attempt to respond to many of the criti-
cisms of the thconomic position — certainly the major objections
which have been raised — in a topical fashion. I will consider first
the definition and distinctiveness of the position known as
“theonomy.” The next section of the book addresses various kinds
of logical and theological fallacies which deter the cffectiveness of

28. For example, the main thrust of a widely read article on theonomic ethics by
Rodney Clapp in Christianity Teday, vol. 31, no. 3 (Feb. 20, 1987), was captured in its
title: “Democracy as Heresy.” He recklessly accuses theonamists of seeking “the
abolition of democracy” (p. 17), when surely Clapp is aware that the word ‘democ-
racy’ is susceptible to an incredibly wide range of definitions and connotations (e¢.,
from an institution of direct rule by every citizen without mediating representatives
to a governmental procedure where representatives are voted in and out of office by
the people, to the simple concepts of majority vate or social equality, etc.). Theonomists,
are opposed to some of those ideas, but surely not to what is commonly understood
by the word: namely, democratic procedures for choosing representatives to rule.
Indeed, in reply to Mr. Clapp’s inflammatory rhetoric, Dr. Gary North very appro-
priately pointed out as a historian the irony that it was precisely our Puritan (and
theonomic) forefathers who fought for and established this kind of “democracy” in
the Western world!
