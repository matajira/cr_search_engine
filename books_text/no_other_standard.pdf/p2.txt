Other books by Greg L. Bahnsen

A Biblical Introduction to Apologetics, Syllabus (1976)

Theonomy in Christian Ethics, (1977, 1984)

Homosexuality: A Biblical View, (1978)

By This Standard: The Authority of God’s Law Today, (1985)

House Divided: The Break-Up of Dispensational Theology, (1989)
(With Kenneth L. Gentry, Jr.)

Contributions to:

Foundations of Christian Scholarship, ed. Gary North (1976)
Juemancy, ed. Norman Geisler (1979)

Evangelicals and Iuemancy, ed. Ronald Youngblood (1984)
Pressing Toward the Mark, cd. Charles Domnison (1986)
God and Politics: Your Views, ed. Gary S. Smith (1989)
