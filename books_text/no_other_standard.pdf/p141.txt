Lsrael’s Theocratic Uniqueness 125

Pre-Mosaic Revelation,
Natural Revelation and the Decalogue

Zorn contends that only those regulations or principles which
antedate the (typological) Mosaic economy remain binding in the
New Testament. But he does not offer any Biblical substantiation
of his restriction on the authority of the Old Testament (contrary
to Deut. 4:2). Nor is it clear how this restriction can be harmo-
nized with the New Testament endorsement of every scripture (2
Tim. 3:16), every command (James 2:10), even the least com-
mand (Matt. 5:19), every word (Matt. 4:4) and every letter (Matt.
5:18) of the Old Testament. Where does Zorn or any other theolo-
gian have the prerogative to cut things off at the Mosaic period?
Where does the New Testament restrict our moral obligation — or
even our standards for criminal justice (1 Tim. 1:8-10; Heb.
2:2)—to the commands of the pre-Mosaic period? What sense
would it make, if the Mosaic period is effectively a parenthesis in
God’s economy for men, as Kline and Zorn indicate, to say that
Gentile believers have now been incorporated into “the common-
wealth of Israel” (Eph. 2:11-13)? Zorn’s approach to Old Testa-
ment commandments is entirely artificial and without textual
justification.

Moreover, if only pre-Mosaic stipulations are binding today,
then Zorn will have a number of embarrassing ethical problems
to face — not the least of which is his desire to defend the Chris-
tian Sabbath. By what Biblical warrant would Zorn distinguish
between manslaughter and murder today? That distinction is not
found in the Noahic covenant, nor is it repeated in the New
Testament. By what warrant would Zorn condemn a daughter
marrying her widowed father? That detail of incest is explicitly
forbidden in neither the pre-Mosaic nor New Testament revela-

 

and theological support for saying that Paul was there referring to the ccremontal
aspect of the Mosaic economy, rather than “the whole of the Mosaic economy” (to
use Zorn’s words). This is not difficult to do. The historical setting (the Judaizing
threat, 2:11-21), the context (circumcision, 2:3-5; festivals, 4:10), the typological
character of “the law” (pointing to Christ and justification by faith, 3:24), and the
very vocabulary chosen (“rudiments,” 4:3; ef, Col, 2:16-20) all point to the ceremo-
nial law in particular. This has all been expounded elsewhere, and Zorn apparently
has no answer for it— or at least does not offer one.
