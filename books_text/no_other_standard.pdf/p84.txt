68 NO OTHER STANDARD

the New Testament expliciily says or does not say. “There is no
Scripture that states explicitly: the civil magistrate today is to
enforce all the Old Testament laws, including the penal laws.”!!

The observation is argumentatively trivial because the ab-
sence of such explicit mention of a doctrine in the New Testament
does nothing to indicate whether the doctrine is obscure or not.
Well-founded and clear doctrines, such as the Trinity, the hy-
postatic union, and the intermediate state, all lack explicit Biblical
mention, without thereby becoming questionable as to their truth,
Moreover, if it is somehow significant that the New Testament
does not explicitly mention that the civil penalties of the Old
Testament are binding today for certain “religious” offenses, it
need only be observed that an explicit denial of their abiding
validity is equally as absent from the New Testament — as Neilson
would have to admit (“but also there is no Scripture that states
specifically that the magistrate is not to enforce all the Olid Testa-
ment penal laws”). Therefore, apart from a controlling presump-
tion, appeals to silence are fallacious and prove nothing, for the
silence could equally support contradictory theses.

Nevertheless, the theonomist maintains just here that there és
a controlling presumption which should affect our conclusions
about the New Testament view of the validity of Old Testament
commandments — and that controlling hermeneutical assumption
is mandated by no less than the authority of the Lord of the
covenant Himself, speaking directly to the very question of the
law’s continuing validity (Matt. 5:17-19). The Nee Testament
teaches us that — unless exceptions are revealed elsewhere — every
Old Testament commandment is binding, even as the standard
of justice for all magistrates (Rom. 13:1-4), including every recom-
pense stipulated for civil offenses in the law of Moses (Heb. 2:2).
From the New Testament alone we learn that we must take as

li. Neilson says that the absence of an “express repeal” of an Old Testament
commandment does not scttle anything; indeed, “the very force of the new covenant
itself can operate as a revocation without express statement” (pp. 13, 14, 39). This
is contradicted by the controlling authority of Deut. 12:32. It is also terribly arbitrary
since critics who say such things do not believe that everything from the old covenant
is revoked by the coming of the new; they use this principle as a taxi, to be utilized
or dismissed according to whatever conclusion they wish to reach.
