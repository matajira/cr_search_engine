HEQMAN

 

 

 

 

 

"Have I got time for a cup of coffee?”

HIS IS ONE of those pictures

that really is worth ten thou-
sand words. It summarizes this
book’s thesis: the cultural bank-
tuptey of modern evangelicalism
and its chief cause, the doctrine of
Christ’s momentary return.

Modern evangelicalism is like
that fellow with the sign, and mod-
erm humanistic society takes its mes-
sage just about as seriously as Her-
man does. A movement that be-
lieves the message of that sign is
not going to produce a comprehen-
sive challenge that is meaningful
or even plausible to the Hermans
of this world. Christianity cannot
beat something with nothing. Peo-
ple who think they have time only
for a cup of coffee and reading a
gospel tract have nothing much to
offer a civilization in crisis.

This does not mean that
those holding the sign have no so-
cial theory. They do: a theory
that they have not developed.
They believe in a view of the
world that has been developed in
terms of philosophies other than

the Bible’s. They have imported
alien philosophies into Christian-
ity. To the extent that they at-
tempt to challenge modern man
intellectually, they are using de-
fective tools.

Millennidlism and Social Theory
presents the case for the Bible as
the sole foundation of valid social.
theory. Every social theory has a
theory of sovereignty, authority,
Jaw, rewards and punishments,
and cultural progress over time.
The Bible offers a unique version
of such a theory. But modern
Christians have rejected the idea
of cultural progress. They also re-
ject the idea of God’s sanctions in
history. Finally, they reject biblical
law. They have therefore been
forced to import humanistic sub-
stitutes for these three crucial con-
cepts. Very few of them have rec
ognized what they have done, or
have had done to them, in the
name of Christianity. This book
shows exactly what has been done,
and why it has distorted the
Chutch’s efforts of evangelism.

 

 

MILLENNIALISM AND SociAL THEORY by Gary North

Hardback, 401 pages, $14.95

Tnstitute for Christian Economics

ISBN 0-930464-49-4
