“When the Gentiles who have not the law do by nature the things
of the law . . . they show the work of the law written in their
hearts.”

Romans 2:14, 15

“God gave them up to a reprobate mind to do improper things .
who, knowing the ordinance of Ged, that they who practice such
things are worthy of death, not only do the same, but also consent
with them who practice them.”

Romans 1:28, 32

“Bahnsen underestimates the role of natural law”

Bruce Waltke,
Theonomy: A Reformed Critique (1990), p. 84

“The state is going to be judged by God. Now since this is true,
there must be criteria or a standard for this judgment. . . , It
might be natural law, but this is simply a projection of autonomy
and satisfaction with the status quo. . , , Others have gone on
to maintain that natural revelation will be the standard of judgment.
However, this either amounts to preferring a sin-obscured edition
of the same Jaw of God or to denying the unity of natural and
special revelation (and being willing to pit the one against the
other).”

Theonomy in Christian Ethics
(1977), pp. 399-400
