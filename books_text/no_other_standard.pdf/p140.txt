124 NO OTHER STANDARD

acter of Israel’s economy means that its laws must be temporary,
they would never claim that nations which were not God’s people
were obligated to follow those laws.!® The glaring oversight of
which Zorn is guilty here is that Scripture itself teaches that the
Mosaic laws were — even if of typological significance {in a way
unexplicated by Zorn) —still the moral standard for the “non-
theocratic” (or “non-typological”) nations around Israel. Scrip-
ture draws theological inferences which directly contradict Zorn’s.
Kline’s theological “model” is guilty of the same deficiency and
cannot rescue Zorn here.

This is also the fatal error in the reasoning offered by James
Skillen, Skillen observes that Galatians and Hebrews “treat Is-
rael as an integral whole, prefiguring . . . the church of Christ.
Therefore, the whole of Israel’s covenant life, and not just one
part of it, serves as the anticipatory model for the church.” Based
on this premise (which theonomists grant) Skillen illogically infers
that no one part of Israel’s covenant order may be “abstracted for
use as a separable model” for states today. The fallacy and mo-
mentous oversight here, though, is that even with Israel as “a
compact unity” (as Skillen puts it) which as a “whole” prefigured
the New Testament church, Israel’s law-code was already in the Old
Testament a “model” for non-covenanted states. Obviously, then,
it cannot be contradictory to Israel’s typological status to treat its
Jaws as a model for modern (non-covenanted) statcs cither. If
Skillen or Zorn or Kline finds this illegitimate “abstracting,” then
his argument is with Moses, not with theonomy.”°

18. Zorn only shows himself to be theologically arbitrary when he insists on the
temporary nature of Mosaic laws due to the typological elements which may be
found in or around them. After all, redemptive typology did not originate in the
Mosaic period! There are clearly typological elements in the Adami period (cf.
Matthew 4:1; Romans 16:20), the Noahic period (cf. 2 Peter 3:5-7) and the Abraha-
mic period (cf. Hebrews 11:17-19). If Zor were truly consistent in his principle that
the typological elements in a covenantal period imply that the laws of that period are
of temporary validity, then he ought to reject not only the Mosaic commandments,
but elf Old Testament laws! Logical consistency, however, is a rare jewel indeed.

19. James Skillen, The Scattered Voice (Grand Rapids: Zondervan Publishing House,
1990), p. 174.

20. According to Zorn, Galatians 3:15-4:11 teaches that “the Mosaic economy”
has been set aside in the New Testament age, charging that I must supply textual
