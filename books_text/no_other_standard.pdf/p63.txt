Spurious Targets and Misguided Arrows 47

fences around our roofs today, but we may be bound to put them
around our swimming pools.” Do the underlying principles iltus-
trated by the case laws of the Old Testament explicate, qualify,
and show us how to obey the more general commands of Scrip-
ture, as theonomists say? In his article Chismar admits that they
do. They do not reduce simply to the more general commands
(e.g., “love your neighbor”), but play a definitional role, illustrat-
ing the application of those commands, and thus helping generate
new laws from the summary principles. This is just the theonomic
position expressed in Chismar’s language.

Chismar correctly notes that there are “massive cultural/
technological/geographical differences” between the society of Old
Testament Israel and our own — differences which make the trans-
lation of Old Testament demands into contemporary applications
a difficult and challenging task. Although the difficulty has some-
times been exaggerated, Chismar is right that there will even be
some difficulty “in determining which principle is being instanti-
ated” by specific case laws. But what Chismar has pointed out is
not a unique hermeneutical problem for theonomic ethics. Such
remarks apply to every effort to bring the ancient literature of the
Bible (whether from the Mosaic, prophetic, or even New Testa-
ment periods) to bear upon our very different, modern age. Rela-
tivists insist that it is impossible. The alternative of abandoning
God’s ancient, written revelation of His will in favor of modern
wisdom may have greater simplicity, but it is treason against the
King of heaven and earth. Let us not allow the difficulty of the
task make us hesitant to give it our best, sanctified efforts.

The Schoolboy Error of “Direct” Application?

The editors, Barker and Godfrey, charge that theonomy “over-
emphasizes the continuities and neglects many of the discontinui-
ties between the Old Testament and our time.”!® This kind of
ambiguous and overgeneral remark is of little help. We are rarely
told precisely what discontinuities the theonomist has actually
overlooked or how exactly the continuities are overemphasized. A
verbal standoff can be created by simply responding that no,

18. “Preface” to Theonomy: A Reformed Critique, p. 1.
