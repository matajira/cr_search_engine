296 NO OTHER STANDARD

The incoherence of claiming to agree with both theonomy and
Kiine’s intrusionist position has, in principle, devastating conse-
quences for Poythress’ theology. As logicians know very well, from
contradictory premises one is able to prove anything whatsoever (by
means of logical addition, then disjunctive syllogism). Thus if a
system of thought incorporates inconsistent premises or princi-
ples ~ which Poythress does by agreeing yet disagreeing with the
main point of intrusionism—it may arbitrarily lead to any con-
clusion or application one wishes. This arbitrariness, depending
upon which side of the contradiction one wishes to stand upon at
any given point, would render the system of thought unreliable
and irrational.

Theological Relativism

A related major flaw in Poythress’ book is its tendency toward
an unwitting, yet unnerving, theological relativism. Poythress would
have his readers believe that the stark differences between
theonomists and intrusionists when they interpret the Old Testa-
ment law do not arise from any genuine conflict of theological
principles or exegetical reasoning. Rather these disagreements
arise from, and reduce to, the differing conceptual frameworks which
the two schools bring to the text of Scripture (pp. 316-335) — which
in turn are affected by the personalities of the individuals involved
(pp. 350-351). Making those kinds of remarks lays a congenial
basis for appealing to both sides to understand each other sympa-
thetically and not polarize the debate — to “listen to each other”
and appropriate each other’s insights (p. 352). But if Poythress is
not cautious, the price he pays for that social rapprochement will
be theological scepticism,

Surely Poythress does not wish to imply that everybody
approaches Scripture with a preconceived framework which is
neither justifiable nor correctable on the basis of Scripture it-
self— which would cut us off from ever arriving at the unadulter-
ated truth of divine revelation. Surely Poythress does not wish to
imply that differing theological frameworks are all equally legiti-
mate and complementary — which would be logically absurd. So
then, on the assumption that Kline and theonomists arrive at
