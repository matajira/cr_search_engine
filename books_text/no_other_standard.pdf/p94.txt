78 NO OTHER STANDARD

was the law itself which taught Paul not to seek righteousness and
God’s acceptance through law-works! “Law” and “Grace” may
be tags for different covenantal administrations (viz., Old and
New Covenants), but they both were administrations of God’s
grace as the way of acceptance before Him. Paul very clearly
included the Mosaic covenant ~ the “/aw” covenant, which erected
a wall between Jews and Gentiles (alienating the uncircumcision
from “the commonwealth of Israel”) —as part of “the covenants
of promise” (Ephesians 2:12).

Dispensational Reasoning
About the Law’s Jurisdiction

Robert Lightner states the essential dispensational problem
with theonomy: “Dispensationalists believe the Law of Moses in
its entirety has been done away as a rule of life.” Why this radical
conviction? “The fact that God gave the Law to the people of
Israel and not to the Church is the beginning point for dispensa-
tionalism’s difference with theonomy. All other points of disagree-
ment stem from this one.”*

Where does Scripture warrant such reasoning, this idea that
God’s truth or standards are intended only for the immediate recipi-
ents of the word He sends? Paul told the Corinthians that it was
shameful for them to be pursuing law-suits against each other
before unbelieving magistrates (1 Cor. 6:1-8). According to dis-
pensational logic, this would not be binding upon the Colossians,
since it was not revealed to them, but to the Corinthians! Such
thinking is readily reduced to absurdity. Since none of the New
Testament was revealed to twentieth-century, English-speaking
churches or believers, should we conclude that none of us is bound
to the truth and ethics of the New Testament?

To extricate himself from the problem his dispensational rea-
soning creates for him, Lightner will need to maintain that what
Paul wrote to the Corinthians was meant for a whole class of people
that extends beyond Corinth in the first century: the class of
believers in Jesus Christ, whether in first-century Colossae or

4. Robert P, Lightner, “A Dispensational Response to Theonomy,” Bibliotheca
Sacra, vol. 143 (July, 1986), pp. 235-236.
