The Origin of Higher Criticism 17

has faded in importance since the end of the Second
World War. In the immediate post-war era, biblical
criticism was an important aspect of Protestant col-
leges and seminaries. No longer, “Given a predomi-
nant concern with the present and its seemingly ur-
gent practical problems, which claim almost exclusive
attention,” he writes, “historical criticism and exegesis
have come to take very much a back place.”!?

Burying the Dead

Why, then, should I devote a book to this topic?
Because of a parallel process: while modern human-
ism has visibly begun to fragment, taking with it mod-
ern liberal theology, there has been a recovery of in-
terest within the evangelical world of real-world
questions that are best summarized under the general
heading, “Christian worldview.” The implicit dualisms
of modern fundamentalism — Old Testament vs. New
Testament, law vs. grace, letter vs. spirit, church vs.
state, Israel vs. the church, cternity vs. history, heart
vs. mind, dominion vs. rapture, culture vs. king-
dom — have begun to be cithcr discarded or at least
seriously criticized from within the camp.'? The Chris-

12. did, p. 1.

13. On the Israel-church dichotomy, see William E. Bell, A Criti-
cal Fvaluation of the Pretribulation Rapture Doctrine in Christian
Eschatology (Ph.1) dissertation, New York University, 1968). See
also John F. MacArthur, The Gospel According to Jesus (Grand Rapids,
Michigan: Zondervan Academie, 1988). This book sold over 100,000

 
