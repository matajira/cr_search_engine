Introduction 3

tures. These strategies have included such things as
1) de-emphasizing the use of the Bible in worship and
prayer and substituting church traditions and hand-
books; 2) proclaiming newer revelations that suppos-
edly are the updated Word of God; and 3) suppress-
ing the production and sale of Bibles, Another effec-
tive strategy has been the development of a tradition
of critical scholarship that seeks to prove that the Bi-
ble is not what it says it is, namely, the revealed Word
of God. Instead, scholars prescnt it as a disjointed
collection of misleading documents, deliberately re-
vised and rewritten by “redactors” and editors years
or cven centuries later than the texts initially appear
to have been written. The Bible, in short, is a hoax.

Having made their case, they then adopt the lan-
guage of praise, telling readers that, while mythical,
the Bible is nevertheless a majestic document that
deserves an important place in the varied and com-
plex history of man’s religions. In short, as hoaxes go,
the Bible is a good one, as good or better than all the
other hoaxes in man’s religious history. This is the
official “Party line” taken by every secular university
in its comparative religion and “Bible as literature”
courses, and also in most theological seminaries.

For over a century, such beliefs regarding the ori-
gin of the Bible have been common in academic cir-
cles. More important, the same beliefs have been in-
creasingly prominent in evangelical Christian circles.
And wherever such an attitude has taken root in evan-
