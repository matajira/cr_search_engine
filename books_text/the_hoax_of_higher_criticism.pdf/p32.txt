24 THE HOAX OF HIGHER CRITICISM

Apostate Deceivers

The higher critics present the Bible as a poorly
assembled patchwork of lies and myths, and then they
add insult to injury by arguing that their debunking
operation somehow elevates our view of the Bible.
For example, the internationally respected (unfortu-
nately) Bible scholar G. Ernest Wright and his co-
author argue that in the Bible, “What is important is
what this great Lord has done.”* But as soon as any-
one raises the obvious question, “What exactly has
God done?” the authors run for the cover of symbol-
ism and supposed myth, in order to escape the Bible’s
detailed account of what God has done:

This furnishes a chic to our understanding of the pre-
historic material preserved in Genesis 1-11. These tradi-
tions go far back into the dim and unrecoverable history
of Isracl; they are the popular traditions of a people, tradi-
tions which in part go back to a pre-Canaanite and North
Mcsopotamian background. For this reason there is little
question of objective history here. We are instead faced

 

source theory of Pentateuchal origins and the two-source theory of
the Synoptic interrelationships are its major results. Literary (source)
criticism has achieved a more sharply contoured profile of the vari-
ous sources and books, and the authors who stand behind them. Ft
is indispensable for any responsible interpretation of the Bible.” Edgar
Krentz, The Histoncal-Critical Method (Philadelphia: Fortress Press,
[1975] 1977), p. 50.

4. G. Ernest Wright and Reginald H. Fuller, The Book of the Acts
of Cod: Christian Scholarship Interprets the Bible (Garden City, New York:
Doubleday, 1957), p. 36.
