Conclusion Sl

‘This legacy of the internalization of the Word of God
triumphed in the modern church through the influ-
ence of twentieth-century fundamentalism: grace over
law.” Once again, we see evidence of the implicit alli-
ance between the power religion and the escape relig-
ion.

It is time for Christian scholars of the Old Testa-
ment to stop their fruitless shadow-boxing with higher
critics who will no more listen to Bible-defending schol-
ars than they have listened to Moses and Christ. It is
time for orthodox Bible scholars to go to the Penta-
teuch to find out what it says, not to discover some
new bit of evidence that Moses really and truly did
say it. There is no doubt a place in the division of
intellectual labor for linguistically skilled Christians
to defend the integrity of the Bible against the inco-
herent slanders of higher critics, but this technical
task should be put on a low-priority basis. What we
do need is a great deal of research on the chronology
of the Pentateuch ~ not on when Moses wrote the
Pentateuch, but on what was going on in the sur-
rounding nations at the time of the exodus. We need
a reconstruction of ancient chronology, one based on
the presupposition that the Bible gives us the authori-
tative primary source documents, not Egypt or Baby-
Jon. Such a project would keep a lot of linguistically

 

of the Madem World (Philadelphia; Fortress Press, [1980] 1985), ch. 3.

2. Donglas W. Frank, Less Than Conquerors: How Evangelicals En-
tered the Tiventicth Century (Grand Rapids, Michigan: Rerdmans, 1986)
