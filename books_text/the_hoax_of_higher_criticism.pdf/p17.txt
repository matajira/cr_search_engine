1

THE ORIGIN OF
HIGHER CRITICISM

For had ye believed Moses, ye would have believed
me, for he wrote of me. But if pe believe not his wril-
ings, how shall ye believe my words? (John 5:46-47).

So Jesus said to the Jewish leaders of His day in
defense of His ministry and His person. They did not
believe Him. Neither do their spiritual heirs today.

But it is not just Jews who refuse to take these
words seriously; it is also the vast majority of those
who graduate from theological seminaries today. With.
few exceptions, seminaries are staffed by professors
of literature rather than professors of Christ. They
have adopted a view of the Bible which says that the
biblical texts reveal gross errors on the part of the
Bible’s writers and editors. The critics refer to the
Bible as a myth-filled book. These classroom skeptics
and their intellectual predecessors have labored for

9
