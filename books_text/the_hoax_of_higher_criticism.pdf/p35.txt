The Techniques of Higher Criticism 27

objective fact of history in the same sense as was the cruci-
fixion of Christ. The latter was a fact available to all men
as a real happening, and pagan writers like Tacitus and
Josephus can speak of it, But in the New Testament itself
the Easter faith-cvent of the resurrection is perceived only
by the people of the faith. Christ as risen was not seen by
everyone, but only by the few, Easter was thus a reality for
those in the inner circle of the disciples and apostles. That
is not an arena where a historian can operate. Facts avail-
able to all men are the only data with which he can work,
the facts available to the consciousness of a few are not
objective history in the historian’s sense.’

‘They distinguish the “real happening” of the cru-
cifixion from the “faith-event” of the resurrection,
which was an event of a very different character. Only
“facts available to all men” — meaning facts that are
implicitly possible for all men to have seen — are “real
happenings.” This means that the resurrection was
somchow not a fact that in principle all men might
have seen and verified, in the same way that they
could have secn and verified the crucifixion. In other
words, the resurrection was not a “real happening,”
although the calculating deceivers who wrote The Book
of the Acts of God are too wise to say this blatantly, for
fear of tipping their hand. They argue that the resur-
rection was therefore not an objective historical event,
not “an objective fact of history.”®

7. Wright and Fuller, Acts of Gad, p. 25.
8. On the anti-historical concept of the resurrection-event or faith-
