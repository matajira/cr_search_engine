72 THE HOAX OF HIGHER CRITICISM,

sure. John Maynard Keynes, the most influential econo-
mist of this century (which speaks poorly of this century),
spoke the truth in the final paragraph of his General Theory of
Employment, Interest, and Money (1936):

. .. the ideas of economists and political philosophers, both
when they are right and when they are wrong, are more powerful
than is commonly understood. Indeed, the world is ruled by little
else. Practical men, who believe themselves to be quite exempt
from any intellectual influences, are usually the slaves of some de-
funct economist. Madmen in authority, who hear voices in the
air, are distilling their frenzy from some academic seribbler of a
few years back, f am sure that the power of vested interests is
vastly exaggerated compared with the gradual encroachment of
ideas. Not, indeed, immediately, but after a certain interval; for
in the field of economic and political philosophy there are not
many who are influenced by new theories after they are twenty-
five or thirty years of age, so that the ideas which civil servants
and politicians and even agitators apply to current events are not
likely to be the newest. But, soon or late, it is ideas, not vested in-
terests, which are dangerous for good or evil.

Do you believe this? If so, then the program of long-term
education which the ICE has created should be of consider-
able interest to you. What we need are people with a cested in-
terest in ideas, a commitment to principle rather than class position.

There will be few short-term, visible successes for the
ICE's program. There will be new and interesting books.
There will be a constant stream of newsletters. There will
be educational audio and video tapes. But the world is not
likely to beat a path to ICE’s door, as long as today’s
policies of high taxes and statism have not yet produced a
catastrophe. We are investing in the future, for the far side
of humanism’s economic failure. This ts a long-term invest-
ment in intellectual capital, Contact us at: ICE, Box 8000,
Tyler, TX 757.
