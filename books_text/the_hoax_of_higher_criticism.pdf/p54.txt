46 THE HOAX OF HIGHER CRITICISM

tion?"® What it could not possibly represent, in his
worldview, is a fulfilled prophecy.

Ifa person derives cthies from history, and then
scrambles the historical data by means of an erronc-
ous chronological scheme, both his cthies and his his-
toriography will flounder.? He will write such non-
sense as this: *. . . the indispensable agricultural-
fertility aspect of Baalism'® had long ago become a
traditional part of Yahweh worship, taken for granted
even by Amos and Hosea. It is a naive misconception
to suppose that the latter had achieved its final form
even at the time of Moses and the Exodus. As Morgen-
stern'! well notes, the Jewish religion is the product
of historical evolution to mect the needs of the Jewish
people ‘from the remote desert period to the present

 

 

 

8. ibid, p. 140.

9, There are few intellectual tasks more pressing on Ghristian
historians of the ancient Near East and classical Greece and Rome
than to rethink (he various chronologies prior to about 750 B.C. CI.
Gury North, Moses and Pharaoh: Dominion Religion vs. Power Religion
(Tyler, Texas: Institute for Christian Feonomics, 1985), Appendix
A: “The Reconstruction of Egypt's Chronology.”

10. Citing Ivan Engncll, Studies in Divine Kingship in the Near Fast
(Oxford: Basil Blackwell, [1943] 1967), p. 172.

Il, Julian Morgenstern, Rites of Birth, Marriage, Death and Kindred
Occasions Among the Semites (Cincinnati, Ohio: Hebrew Union College
Press, 1966), p. 64, Hany single individual was most responsible for
corrupting American Judaism by means of higher criticism, it was
the remarkable, long-lived Julian Morgenstern. For a summary of
his life, see Morris Licberman, “Julian Morgenstern — Scholar,
Teacher and Lcader,” Hebrew Union College Annual, XXXTI (1961),
pp. 1-8.
