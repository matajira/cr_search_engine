34 THE HOAX OF HIGHER CRITICISM
Criticizing Textual Criticism

The methods used by higher critics are circular:
they use their colleagues’ reconstructed literary texts
to reconstruct the biblical past, and they use their
own newly reconstructed biblical past to further re-
construct the biblical texts. On and on the academic
game goes, signifying nothing except the futile pur-
poses to which very dull people’s minds can be put.

These literary techniques are highly complex, yet
amazingly shoddy. The practitioners agree on very
little; they reach no testable conclusions; and their
required techniques absorb inordinate quantities of
time to master. Liberal Bible scholar Calum Car-
michael puts it mildly when he warns his readers:
“Historical and literary criticism is undeniably useful

  

when working with ancient sources, but not only has
it limitations, it sometimes leads nowhere. One mani-
fest restriction in its application to most biblical ma-
teria] is that the historical results hypothesized can-
not be corroborated. The speculative character of most
such results is casily overlooked because the histori-
cal mcthod is so deeply entrenched in scholarly ap-
proaches. With a little distance, we can see just how
shaky the historical method is. . . . The procedure
is a dispiriting onc, dull to read, difficult to follow,
and largely illusory given the paucity of the results
and the conjectured historical realities dotted here and
there over a vast span of time. Its most depressing
