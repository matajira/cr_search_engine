6 ‘THE HOAX OF HIGHER CRITICISM

to the fire coming from heaven to destroy the sacri-
fice. ‘Today, we face another kind of fire from the sky:
man-made, We could become the sacrifice.

If Christians believe that they can escape their
responsibility to publicly ask this question of ultimate
sovereignty, and also escape the effects of the world’s
reply to this crucial question, they are living in a fan-
tasy world. Many millions. of fundamentalists have
heen living in just such a world.’ But the world will
not allow them to live in illusionary safety much longer.

If Christians lack confidence in the integrity and
universal applicability of the Bible, Old and New Tes-
taments, then they will not be able effectively to ask
Eljah’s public question. Men must be given an op-
portunity 10 decide self-consciously on their answer.
There are now over five billion people alive today.
Without saving faith in Jesus Christ, the vast major-
ity of (hem will spend eternity in the lake of fire (Rev.
20:14}. To save them, the Holy Spirit needs to make
His move very soon. But if His own people continue
to have doubts about God’s Word, what will result
from a worldwide revival? A much greater skepticism
and a far greater sense of betrayal.

‘Thus, Christians need to regain their faith in God’s
Word. An important step in the recovery of such con-
fidence is the acknowledgment, especially in the shat-

 

3. Dave Hunt, Whatewer Happened to Heann? (Kugene, Oregon:
Harvest House, 1988),
