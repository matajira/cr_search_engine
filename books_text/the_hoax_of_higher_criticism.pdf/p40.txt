32 THE HOAX OF HIGHER GRITICISM

is always the final lurking question: Is the Bible true?
What is truth and what is just symbolic? Cannot I
have anything that is absolutely certain?” Then the
mush: “The answer must be that the symbol is the
truth. We have no other truth. We know it is not
literal truth, but we know that the biblical portrayal
is the relationship between the unknown infinite and
ourselves here and now. No precise dividing line can
be drawn between the ultimately real and the poetic
symbol, because God has not made us infinite.” In
short, they argue that because I am not infinite, and
therefore not God, J need not fear an infinite God, for
my very finicude keeps me from knowing God. To
which Paul answered many centuries ago:

For the wrath of God is revealed from heaven against all
ungodliness and unrighteousness of men, who hold [back]
the truth in unrightcousness; becausc that which may be
known of God is manifest in them; for God hath shewed it
unto them. For the invisible chings of him from the crea-
tion of the world are clearly scen, being understood by the
things that are made, even his eternal power and God-
head; so that they are without excuse (Rom. 1:18-20).

The Bible of the higher critics cannot possibly be
what it says clearly that it is: the revealed Word of the
Creator and Judge of the universe. Now, if the Bible
really isn’t what it says it is, then it must be a hoax,
oO the implicit though politely unstated accusa-

 

14, Wright and Fuller, Acts af God, p. 37.
