4 THE HOAX OF HIGHER CRITICISM

gelical churches, colleges, and seminaries, it has led,
step by step, first to theological liberalism and then
to political liberalism. Why? Because once the Bible
is abandoned as the only source of unrevised and un-
revisable stability in a world of ceascless change, there
is no other reliable rock for men to stand on. In the
words of James, “he that wavereth is like a wave of
the sea driven with the wind and tossed” (James 1:6).
Men are tossed to and fro by the winds of opinion.
In the twentieth century, the winds of opinion in the
West have been liberal: inherently skeptical, doubt-
filled, relativistic, and existentialist, challenged only
by dogmatic Communism, which is now itself in the
process of self-destructing ideologically and perhaps
even institutionally.

Relativism cannot sustain a civilization, Jet alone
reconstruct one. ‘hus, we are clearly at the end of an
era, an era that began in the West over three centu-
ries ago with the rise of Enlightenment skepticism,
Onc of the marks of that self-conscious religious move-
ment has been its commitment to the rejection of the
Bible as the inspired and authoritative Word of God.
Beginning in the late seventeenth century, Socinian-
ism (a precursor of Unitarianism) and Deism steadily
replaced Trinitarian Christianity in the thinking of
the intellectual and political leaders of the West, be-
ginning most importantly with Isaac Newton (who
at least took seriously the Bible’s historical texts),!

1, Isaac Newton, The Chronology of the Ancient Kingdoms Amended
