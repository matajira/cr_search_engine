What Is the ICE? 67

standards of righteousness ~justice— for these social insti-
tutions? Are they lawless? The Bible says no. We do not
live in a lawless universe. But this does not mean that the
State is the source of all law. On the contrary, God, not the
imitation god of the State, is the source.

Christianity is innately decentralist. From the beginning,
orthodox Christians have dented the divinity of the State. This is
why the Gaesars of Rome had them persecuted and ex-
ecuted. They denied the operating presupposition of the
ancient world, namely, the legitimacy of a divine ruler or a
divine State.

It is true that modern liberalism has eroded Christian
orthodoxy. There are literally thousands of supposedly
evangelical pastors who have been compromised by the
liberalism of the universities and seminaries they attended.
The popularity, for example, of Prof. Ronald Sider’s Rich
Christians in an Age of Hunger, co-published by InterVarsity
Press (evangelical Protestant) and the Paulist Press (liberal
Roman Catholic), is indicative of the crisis today. It has
sold like hotcakes, and it calls for mandatory wealth redis-
tribution by the State on a massive scale. Yet he is a pro-
fessor at a Baptist seminary.

The ICE rejects the theology of the total State. This is
why we countered the book by Sider when we published David
Chilton’s Productive Christians in an Age of Guilt-Manipulators
(3rd edition, 1985). Chilton’s book shows that the Bible is
the foundation of our economic freedom, and that the call
for compulsory wealth transfers and higher taxes on the
vich is simply baptized socialism. Socialism is anti-Christian
to the core,

What we find is that laymen im evangelical churches
tend to be more conservative theologically and politically
than their pastors. But this conservatism is a kind of instine-
