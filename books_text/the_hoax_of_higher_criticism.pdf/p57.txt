CONCLUSION

Jesus said unto him, Let the dead bury their dead:
but go thou and preach the kingdom of God (Luke 9:60}.

Christians have made the mistake of regarding
the debates over higher criticism as being the pecu-
liar habit of linguistic specialists and theologians. The
fact is, from the very beginning of the rise of human-
ism, there has been a war between those who defend
the Bible, espccially the Old Testament, and those
who reject this testimony. his debate throughout
most of its history involved all of culture, what we call
today a conflict between comprehensive world-and-
life views. It is only in the hands of modern scholars
that the debate has been narrowly focused on the tech-
nical issues of textual analysis. Earlier generations rec-
ognized that the debate was far more important than
modern scholars are willing to admit.

The task of the Christian scholar in defending the
Bible as the Word of God must not be narrowly fo-
cused. The debate did not originate in the university

49
