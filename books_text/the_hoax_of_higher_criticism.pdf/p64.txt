56 ‘THE HOAX OF HIGHER GRITICISM

Taylor, Charles. Rewriting Bible History (According to
Scripture). 1984. 84 Northgate Street, Unley Park,
South Australia: House of Tabor.

Thiele, Edwin R. A Chronology of the Hebrew Kings. 1977.
Grand Rapids, Michigan: Zondervan.

. The Mysterious Numbers of the Hebrew
Kings. New edition, 1984. Grand Rapids, Michi-
gan: Zondervan.

Velikovsky, Immanuel. Ages in Chaos. 1952. Garden
City, New York: Doubleday.

. Oedipus and Akhnaton. 1960. Garden
City, New York: Doubleday, 1960.

—______. Peoples of the Sea. 1977. Garden City,
New York: Doubleday.

—________.. Ramses H and His Time. 1978, Gar-
den City, New York: Doubleday.

Scholarly Journals:

Epigraphic Society Occasional Publications. 6625 Bamburgh
Dr., San Diego, California 92117.

Kronos: A Journal of Interdisciplinary Synthesis. P.O. Box
343, Wynette, Pennsylvania 19096.

Newsletters:
Biblical Chronology. Institute for Christian Economics.
