THE LITURGICAL NATURE OF MAN 201

In the ancient world, pagan kings used the cup to symbol-
ize their whole world. Often, the king’s cup was ornately de-
signed as a miniature world. His cup was used for divination
(Gen. 44:5), and the cupbearer, chief in command, tended to
the cup. When a decision had to be made, the cupbearer di-
vined for the king. Thus, the cupbearer was more than some~
one who just tested the king’s winc. He managed the king’s
estate and manipulated the king’s world from the cup.

Joseph was such a man. In this story, the cup had central
significance. When he was taken to Egypt, his first opportun-
ity to advance centered around the interpretation of a dream
(Gen, 40). And the dream spoke of the future of two men.
The one who would live was the butler, The butler was a cup-
bearer, So, Joseph's ability to divine apart from the King’s cup
made a great impression. Furthermore, it is clear from this
that the butler had good reason to keep Joseph in prison. A
man such as this would take his position.

After Joseph became a keeper of the cup, the cup reap-~
peared in the Joseph account (Gen. 44:1-5). It was the
Pharaoh’s cup that was placed in Benjamin’s sack. This ex-
plains the seriousness of the apparent crime. But there was
also an important symbolism conveyed, The chalice of Phar-
aoh was placed in the hands of the youngest, the “second”
born of the house of Israel. Egypt had fallen into the hands of
Israel, Joseph.

The Cup and History

The management of the cup affected the history of Egypt
and Israel. God draws on the imagery of the cup to judge na-
tions. The nations are made to drink of the cup (Is. 31:17-22).
Since they were not living faithful to God’s cup or sacrifice, it
became a judgment.

This process is similar to that in I Corinthians (I Cor.
11:30ff.). Here the application, however, is made to the
church. If the cup is partaken of unworthily, then it becomes a
judgment. Man’s personal history is affected. Due to the cen-
tral place of the Lord’s Supper in the church, one’s whole life
is either blessed or cursed by how the cup is observed. His in-
heritance either becomes a blessing or a curse.

In the Book of Revelation, thc cup appears time and again
as the instrument of judging history (Rev. 14:10; 16:19; 18:6).
