108 CHRISTIANITY AND CIVILIZATION

date, has yet to figure out a way to achieve unity without com-
promising Truth. This author is not prepared to give “the” an-
swer, but he has observed several issues involved in the effort
to maintain catholicity and integrity.

One, if the Reformation thought taught the world anything,
it was that human covenants cannot be absotutized. Man is totally de-
praved and bound to sin, and sometimes unto apostasy. Because
of the sinfulness of man, God therefore allows human covenants
to be terminated in a lawful manner. If unrepentant sin cannot
be resolved, tyrannies are established, and furthermore, to do
so, ultimately absolutizes man. For example, if a king breaks the
Jaw of God, he can be deposed (depending on the severity of the
sin). -If, as in the case of Charles I of England, he commits
treason, he may rightfully be put to death. The king cannot be
above the Law of God, and to absolutize the covenant between
the king and the people makes the king God.

Equally important, however, covenant breaking should be
dealt with lawfully. A human covenant should be dissolved in
a legal manner. When a king commits treason, he should be
tried by a court of law. It is nat enough for common consent of
the people to take the law into their own hands. Elected repre-
sentatives of the people and ordained by God should decide.

Applying these ideas to the Church, members of local
Churches must learn to check their movement among and be-
tween other Churches with accountability. One should not
leave under his own recognizance. Nor should he be admitted
into a Church and given communing privileges until proper
transfer has been secured. Also, local Church governments
should predetermine to respect other Ecclesiastical bodies that
are Trinitarian. Moreover, they should realize that some
problems cannot be resolved to the satisfaction of all parties
concerned. This means that members should be allowed to
transfer, even under discipline in some cases if need be. This
position allows for maximum latitude and maintains the in-
tegrity of the Church at the same time. No doubt this will not
satisfy every hypothetical situation, but in practice it will goa
long way toward maintaining purity and peace,”

20. Perhaps someone will point out that the Reformers found themselves
outside the one Church in the West. First, we must remember that the his-
torical situation was totally different. Second, they were forced out. Third,
they quickly established local Ecclesiastical government to which they were
accountable. Autonomy was only temporary.
