THE CHURCH IN AN AGE OF DEMOCRACY Ug

Corinthians with this issue. The thrust of this paper has been
primarily to speak to the issue of schism and allow Paul's de-
velopment of this theme to control the study. For, in the first
four chapters of I Corinthians, we find the major causes of
schism.

As the first section spoke to the matter of unlawful daring,
the second section speaks to unlawful bonding, fornication.
The remainder of the book, 7:1 to 16:9, develops the theme of
proper tearing and bonding.

It is not within the scope of this paper to continue the ar-
gument. But we must remember that the time of the writing
of this letter was the Feast of Unleavened Bread. It was a time
for coming under the judgment of God to be torn and re-
bonded to His Word. In an age of democracy, the Church,
more than ever before, should allow herself to be torn and re-
newed in her Covenant. The time of rebellious ways must
cease, or God may turn Western culture into another North
Africa where the Gospel has not been successful, except in a
few very isolated and temporary situations, since the seventh
century!55

35, It should be kept in mind that prior to the coming of Islam, the North
African Church had been very strong through the time of Augustine, But
mysticism and premillennialism crippled the Church in this part of the
world such that God has never given it back.
