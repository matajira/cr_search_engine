94 CHRISTIANITY AND CIVILIZATION

seemed to be contradictory to love, and they did not want a
harsh Gospel. The result: Discipline was avoided in the name
of avoiding conflict.

Finally, Paul tackles problerns relating to the sacraments
and/or proper bonding (Chapters 7-16). What do these prob-
lems have to do with the theme of life through judgment?
Several matters touch on this subject. First, there was the
problem of asceticism. This is the denial of certain earthly
prerogatives to become spiritual. Going back to our chart, we
see that spirituality is in terms of obedience not abstinence.
Remember, pagan thought attempts to leave creation,
through some mystical connection, to bring life. Essentially,
the ascetic is no different. He attempts to escape creation,
which is to try and escape judgment.

Second, Paul is concerned that the Corinthians avoid
pagan communion services (I Cor, 10) and properly cormmune
with the living God. Christian communion remembers the
Lord’s death, and here we sec attention drawn to judgment.
The Lord’s Supper is a time of judgment, and a time of life
through judgment. Hence, the second sacrament is a means
of grace.

Third, Paul talks about the meeting of the Church where
the Lord's Supper is normally to be observed. This meeting
had become chaotic to such an extent that he had to tell them
to do things in order. Chaos was corrupting the use of gifts
because the Corinthians were patterning their meetings after
the pagan festivals. For example, the gift of tongues was being
abused. Self-oriented glossalalia, however, was not new to the
pagan world..But the Biblical gift was totally unique since it
was not intended for self stimulation. In other words, the Cor-
inthians had forgotten that this meeting was not primarily for
their personal benefit. Rather, it was a time to come under the
judgment of God, and edify one another.

At the end, Paul concludes this section with the locus
classicus passage on the Resurrection. After confronting the
Corinthians with the need to face death and judgment, he
concludes by telling them Jesus faced the judgment of God
and was raised from the dead. The point being: If the Church
at Corinth would walk properly with Christ, it too would
properly face the judgment of God and find newness of life.

The remainder of this essay is devoted to a development of
the theme of life-through-judgment-tearing in the Book of
