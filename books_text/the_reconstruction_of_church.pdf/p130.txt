118 CHRISTIANITY AND CIVILIZATION

A bishop of Carthage, Felix, had been accused of being a
“traditore,” but was exonerated by Constantine, Nevertheless,
two Episcopates resulted when the people ignored the judicial
ruling and elected Majorinus as bishop of Carthage whose
successor was Donatus.*4 Cantor continues in his description
at this point.

The Donatist puritans claimed that the traditores had lost grace
and were not even Christians any longer. They demanded that the
sacramental rites be administered by priests of pure spirit, and held
that sacraments administered by unworthy priests were invalid. The
Catholic majority maintained their belief that it was the office of the
priest and not his personal character or quality that gave sacramen-
tal rites their validity. This was the pivotal point of dispute~a
church of saints as against the Catholic (universal) church. At the
end of the fourth century the great church father and native North
African, St. Augustine, mustered all his learning and. eloquence
against the Donatists in behalf of the Catholic position, but neither
the arguments of the Catholics nor even the persecutions waged by
the orthodox emperor entirely prevailed against the Donatists. They
became an underground church and disappeared only after the
Moslem conquest in the seventh century. Donatisrm reappeared
again in the west in the second half of the eleventh century. Its
absence from the Christian religious scene for several centuries
enabled the Catholic church to assert its leadership in early medieval
Europe, a task that could not have been successful had the church
followed the Donatist ideals of exclusiveness and not attempted to
bring all men into the fold and tried to civilize them.

Donatism failed to give the world a better Church because
prior to eternity holiness must be defined in terms of persever-
ance instead of perfection. The Corinthians were basing their
willingness to follow on their perception of pertection. Not only
is it wrong to build the Church on perfection, but one’s per-
ception is an even weaker foundation. Whose perception do
we trust? As one can see, perfectionism is fraught with prob-
lems, problems that cause schism.

Conclusion

A Church in an age of democracy will find that schism in-
cessantly recurs. Paul begins the argument of his letter to the

33. Dictionary of Church History, Edited by Douglas, (Grand Rapids:
Baker, 1969), p. 308.
34, Medieval History, p. 41.
