72 CHRISTIANITY AND CIVILIZATION

were emphasized. Men were depicted as victims of sin, rather
than as rebels against God. Jesus was portrayed as passive,
and submission to Him was depicted in almost blasphemously
intimate language, Surrender, emotion, and passivity charac-
terized the Savior and His people. The church, with the
notable exception of “Onward, Christian Soldiers,"!!? was
depicted as defensive, apparently unable to escape the dogged
pursuit of the gates of hell, waiting to be raptured from cer-
tain catastrophe.'3 This pietistic tone was coupled with a
theological simplicity in such songs as “Free from the law, oh,
happy. condition.”!!# Sunday’s innovation. in this area was
predictable: Jazz replaced traditional hymnology in early
twentieth-century revivals.115

When asked about his theology, Moody replied, “My
theology! I didn’t know I had any.”1!6 A survey of Moody’s
thought reveals that this assessment was not inaccurate. His
writings and sermons lack so fundamental an element as a
definition of faith.'*” In practice, Moody’s theology was openly
Arminian. The doctrines of election and total depravity were
nonsensical. He reduced nearly 2000 years of Christian theol-
ogy to a simple formula: All men can obtain eternal life with
God simply by believing the Biblical account of Christ’s sub-
stitutionary atonement. The individual had only to decide
that he would believe. Moody’s revivals became, in Mc-
Loughlin’s appropriate phrase, “electioneering for God’s
party.”"!8 Despite his shortcomings, Moody held the Bible in.
high esteem. Criticizing those who “get their religious food by
ecclesiastical spoon-feeding,” he exhorted his listeners to
“Take, read, feed on the whole word of God” and warned
them not to “throw this and that passage in the book aside.”

112. By the Anglican Sabine Baring-Gould, who was surely no revivalist!

113. Sandra S. Sizer, Gospel Hymns and Social Religion (Philadelphia: Tem-
ple University Press, 1978), pp. 29-41.

114, Quoted in McLoughlin, Modern Revivalism, p. 237.

U5, [bid., p, 422; Billy Sunday p. 83.

116. Quoted in Hofstadter, p. 108.

17. Findlay, p. 227.

U8, McLoughlin, Adodern Revivalism, p. 248, Marsden includes a picture
of an undated Moody Bible Institute tract in the format of a ballot. God has
cast His vote for man’s salvation, Satan against, and man is given the
deciding vote. Marsden, p. 100.
