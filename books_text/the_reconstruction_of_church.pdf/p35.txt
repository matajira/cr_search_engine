CHURCH RENEWAL: THE REAL STORY 23

splits). By tens and twenties families departed, probably over
the obnoxious character of one or more loud-mouthed mem-
bers, or over just which side of the church does the piano
belong on. Something important.

The faithful few held on. It was their church. They would
stay to the end. Others, with little or no conviction, went on
to more fertile territory, usually with lower doctrinal stand-
ards. And so it went, year after year.

Then there was the time no one could agree on which man
to call as pastor. So the church went without one for several
years. During this period, half the remaining members went
elsewhere.

Of course, the older people stayed. They held the posi-
tions of influence (?) in the church, and, as the saying goes,
the tenacious shall inherit the property. And then there were
those folks who could not bear to see the work that they had
supported for so many years fall apart. The witness had to be
maintained.

In desperation, the leaders of the church appealed to the
national missions organization. Send us a White Knight, they
cried. Send us a young man ‘on fire for the Lord” who will
revive our church through his Spirit-guided, single-handed
efforts in the community. In short, send us a miracle.

The missions board did its best to meet the plea of the con-
gregation. Surely this work had to be preserved at all costs.
Let us find a committed young pastor in search of a flock and
send him out into the fields white unto harvest. This church
will be renewed, if only he will follow the right course.

So the board sent out a man. A sound man. A man witha
family and a zeal to serve the people of God. This man was
new in the ministry and still laboring under an idealistic aliru-
ism, The board had judged this work to be of importance and
a work worth salvaging. Ifhe would pursue the building of the
congregation with vigor, God would certainly bless his labors
and resurrect this church. The young man became committed
to-the work. He looked at all the factors, emphasizing the
positive in his own mind as grounds for -hope (there are
already some members), and suppressing the negative (they
are close to death) as unworthy of consideration in light of the
truth that God is able to do all things, to overcome all
obstacles.

Whenever doubts entered his mind in the course of pre-
