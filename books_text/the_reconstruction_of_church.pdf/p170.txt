158 CHRISTIANITY AND CIVILIZATION

ad infinitum. The time lapse must be long enough for the sin-
ner to repent but short enough to guarantee the continuity be~
tween EM, and EMg. Situations of course differ, but gener-
ally whenever weeks and months separate the “two” excom-
munications, the church cannot avoid the charge of being
cowardly in carrying out Christ’s direct orders to excom-
municate. This is especially true when there has been no visible change
in the life-style of the EM; excommunicate. He may come to
church but this dodges the real issue: has he in fact repented
of the notorious sin?}# It may be imagined that because the
offender is still attending the worship services that this is a
lucid commentary about a heart that is soft and malleable when tt
is in fact hard and pliable. (2 Ja John Bunyan’s Mr. Pliable!)
That he lives in his notorious sin and still frequents the con-
gregational worship often shows a divided desire to run both
with the hound and with the hare! When the church allows an
immoderate space of time to elapse, it has broken the con-
tinuity by substituting EM; and EM,. Under such cir-
cumstances when there has been no judicial follow-through,
the discipline or excommunication has been aborted. So, on
this very important matter of time the elders are not left to the
arbitrary caprice of reason. The progress of the excom-
municate is the Biblical criteria: “However, to prevent ex-
treme subjectivity, their chief criterion must be the presence
or absence of visible progress, or visible responsiveness ta ad-
monition or rebuke. In other words, they must ask what visi-
ble effect the Word of God is having on the offender.”!9

The Counseling Substitute

This brings us then to the last and perhaps (considering its
rapid growth to stardom) most insidious substitute for church
discipline, and this is, counseling. Now of course we are in
favor of expostulating with the notorious sinner so that he
might enter into the repentance that makes the angels of
heaven rejoice! But this genre of counseling is Biblically

18, Sometimes the offender will not come to the church and may even
“hide-out” from the elders. In such cases, it does no good to employ EM).
The offender has already made the break. The elders must not cover-up the
seriousness of this contumacy, Unless there are compelling reasons to the con-
trary, EMz must of necessity be the next step.

19. Wray, Biblical Church Discipline, p. 14. Exaphasis his.
