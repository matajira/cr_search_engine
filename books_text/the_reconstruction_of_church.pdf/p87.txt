REVIVALISM AND AMERIGAN PROTESTANTISM 75

poverty, and a host of other social concerns would be resolved
by legislative and administrative action. Social gospelers
stressed, it must be added, that the church and individual
Christians bear responsibility to alleviate injustice and pov-
erty, but their assumptions were statist, as the subsequent his-
tory of liberal Christianity in American indicates. The social
gospel movement was not entirely negative in its impact. It
rightly stressed the wholeness of man and the dynamic nature
of the kingdom of God, but, because its foundations were hu-
manistic to the core, its effects on American Christianity have
been largely evil.

Fundamentalism, which originated from the revivalism of
Moody and Sunday, stood fast against the social gospel. Fun-
damentalism was distinguished theologically by its unwaver-
ing stance for inerrancy and its premillennial eschatology. So-
cially, it may be defined as a reaction to the influence of mod-
ernism and Darwinism in American life. From the outset,
therefore, it was a defensive movement.1%2 Pessimistic premil-
lennialism colored the fundamentalist world view. The world
is growing worse and worse and the only hope is for an immi-
nent rapture, In such a system, the only reasonable role for
the church is to save individual souls, Reconstruction of soci-
ety is a utopian, or, worse, a liberal dream.!%3

Fundamentalism, however, must nat be judged too hastily.
While it is true that Moody himself generally concentrated on
winning souls, he was not unaware of the social problems of
his day. He was especially concerned with the condition of the
working poor. Moody’s licutenants, moreover, were more so-
cially active than their leader. But, in the final analysis, for

132. Marsden, p. 6. Marsden paints out that fundamentalism is a “sub-
species” of revivalism. Marsden, pp. 38-39. Ernest Sandeen, The Roots of
Fundamentalism: British and American Millenarianism, 1800-1930 (Chicago: Uni-
versity of Chicago Press, 1970) views fundamentalism in continuity with an
older, more traditional millenarian movement, while Louis Gasper, The
Fundamentalist Movement, 1930-1956 (Grand Rapids, MI: Baker Book House,
[1963] 1981) explains the movement as a result of a confluence of factors, in-
cluding revivalism, anti-modernism, and millenarianism.

183. Marsden, pp. 38, 81. McLoughlin notes that the heightened tension
caused by premillennial eschatology enabled revivalists to increase their
output. Men responded more quickly to such an urgent call. Eschatology,
furthermore, becarne the litmus test of orthodoxy. Optimistic or kingclom-
oriented eschatologies were considered by fundamentalists to be ipso facto
modernist. McLoughlin, Moder Reoivalism, pp. 237-258.
