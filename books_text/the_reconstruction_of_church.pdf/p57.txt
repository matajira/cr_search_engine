CHURCH RENEWAL: THE REAL STORY 45

Shall I commit myself and my family to the restoration of this
work? Will my efforts, unlike the work of the Lord Jesus
Christ, be a sacrifice without redemption? Am I willing to pay
the price of an unequivocal stand on doctrinal issues? Will I
go so far as to uproot three tares through the pure preaching
of the Gospel to plant a single stalk of wheat?

Church members must ask themselves similar questions.
Ts the church worth renewing, or have I been hanging on all
these years merely out of convenience? Am I willing to pay
the price of Wednesday night bowling to support the work and
worship of the Prayer Meeting and Bible study? Am I obli-
gated?

None of these questions has neutral answers. They are be~
ing answered now in every floundering church across the
country, regardless of affiliation. Daily the church diagnoses
itself through its policies and actions (or inactions). Each
diagnosis is in the dock. The Lord Jesus Christ will come to
reward each one according to his works.

Time is running out for the American church. Either re-
newal will come through self-motivated reformation, or the
Lord God. will bring it, through persecution unknown in
American history. The golden age of the Fifties is over, and
we are now bearing the fruit of the existential Sixties and the
decadent ‘Seventies. There is no longer any time to argue
about whether the choir robes will be red or black, or whether
to have the cake sale instead of the car-wash.

Renewal is possible through transformation of doctrine,
leadership, membership, and location. It will not come
through extended prayer meetings and Youth Sundays.

The troubled church must repent of its institutional evil or
die.
