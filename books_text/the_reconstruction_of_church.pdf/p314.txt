302 CHRISTIANITY AND CIVILIZATION

baptists, like Sider,'?, Wallis,’ Simon,'* Gish,1® Roos,?
Taylor,!? Gunderson,!® and Schwartz-Nobel,'!* are virtually
the only voices emanating from Christendom on the issue of
compassion and welfare. That must change. Now. But in
order for reformed churches to formulate answers, alter-
natives, and models, we must first do the hard work of ham-
mering out a solid theology of Biblical charity. We must lay
foundations.

Those foundations begin with the Scriptural distinction
between the deserving and undeserving poor. “No theory of
helping the poor may be said to be Christian if it does not dis-
criminate among the poor. The old distinction, now despised
among social workers, between the deserving and undeserv-
ing poor, is a reflection of a Biblical theme.”?¢

This distinction becomes more than evident as we note the
Law’s provision for gleaners as opposed to sluggards (Leviti-
cus 19:9-10, 23:22; Deuteronomy 24:19-21, Proverbs 6:6-ll,
19:15, 21:25-26). Standing within the covenant, the gleaner
had the privilege of provision and care. Standing outside the
covenant, the sluggard did not. Willing to labor long and
hard, the gleaner was the recipient of regular charity. Unwill-
ing to lift a hand, the sluggard was not.

Comprehending this distinction leads us to conclude
several things without which no notion of charity can hope to
be Biblical: first, in Scripture, “the primary source of regular
charity to the poor was the practice of gleaning.”?! Biblical
charity knows nothing of promiscuous handouts to sluggards.

12. The infamous Ron Sider, author of Rich Christions (IVP).

13, Jim Wallis, editor of Sojourners Magazine, and author of Agenda for
Biblical People (Harper).

14. Arthur Simon, founder of Bread for the World, and author of a book
from Paulist Press of che same title.

15. Art Gish, contributor to Wealth and Poverty: Four Christian Views of Eco-
nomics, edited by Robert Clouse (IVP).

16. Joe Roos, publisher of Sojourners Magazine.

17, John V. Taylor, author of Enough is Enough (Augsburg).

18. Gary Gunderson, editor of Seeds Magazine.

19, Loretta Schwartz-Nobel, author of Starving in the Shadow of Plenty
(McGraw-Hill).

20. Herbert Schlossberg, Idols for Destruction (Nashville: Thomas Nelson
Publishers, 1983).

21, David Chilton, Productive Christions in an Age of Guilt Manipulators (Tyler,
‘Texas: Institute for Christian Economics, Third revised ed., 1985) p. 56.
