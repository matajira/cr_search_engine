18 CHRISTIANITY AND GEVILIZATION

greatest joy is found in the praise of God, but praising God
means throwing yourself out of yourself. It means throwing
yourself wholeheartedly into the activity of worship. It entails
effort, and we live in an age pervaded by the false notion that
worship should be effortless.

Active worship is not anarchic worship. The rules in 1
Corinthians 14, as well as the teaching of the rest of Scripture,
show us that worship should be “organized and liturgical,” not
“free and spontaneous.” Worship is like a dance. A bunch of
people jumping up and down and running ail over a room is
not a dance. Dancing requires organization. Thus, while
spontaneous “share meetings” and other more “charismatic”
occasions may help people appreciate worship more fully, this
kind of thing should be kept away from public, command per-
formance worship.

Coming to worship on the Lord’s Day is not optional. God
commands us to do Him worship. Neither you nor [ have
anything else to do on the Lord’s Day. The Lord’s Day is the
Christian sabbath, and we are to allow nothing to interfere
with our assembling together. (Such meetings as Sunday
School and other secondary meetings are not in the same
mandatory category, and it is possible that if you attend ali
such meetings, you will have not have enough time for rest.
Asa general rule, however, it is best to gather with God's peo-
ple whenever possible.)

The consequences of spotty attendance at worship are hor-
rible. Historically, the Church has excommunicated any per-
son who did not attend worship on the Lord’s Day, and who
did not have a good excuse. This is because worship is man’s
highest privilege. To stay away for any reason other than sick-
ness or dire emergency is to spit in ihe face of God.

Nowadays the Church is weak, and people lapse from
Church attendance without being dealt with. God, however,
sees it all. The Bible says that we are to be involved actively in
the Church, “not forsaking the assembling of ourselves
together, as is the manner of some, but exhorting, and so
much the more as you see the Day approaching. For if we sin
willfully after we have received the knowledge of the truth,
there no longer remains a sacrifice for sins, but a certain fear-
ful expectation of judgment, and fiery indignation which will
devour the adversaries” (Hebrews 10:25-27). Here we see that
the primary cause of apostasy (falling away from the faith,
