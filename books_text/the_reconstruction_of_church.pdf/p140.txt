128 CHRISTIANITY AND CIVILIZATION

Tivo-Tiered Church Membership

The reigning philosophy of mass democracy has captured
the minds of most Protestant Christians. They have struc-
tured their churches so as to avoid any criticism of being “un-
democratic.” The problem for non-hierarchical churches is
that there is now no effective way to screen out people from
the exercise of church authority, Unlike the Roman Catholics,
Lutherans, and Episcopalians, whose top-down hierarchical
structures serve as barriers against the theologically and bu-
reaucratically “unwashed,” congregational-type churches and
Presbyterian churches face this problem daily. Only because
the local church in our day. is so weak, ineffective, under-
funded, and culturally impotent can it escape the problem of
the “immigrants.”

Baptism is seen as the equivalent of Old Testament cir-
cumcision by most churches. Thus, baptized individuals are
granted access to the teaching of the church. Communion is
another problem. While it is understood as analogous to the
Passover, few churches really acknowledge the full extent of
this Passover-communion link, Any circumcised male could
attend Passover (Ex. 12:48); not every baptized individual is
allowed to take communion in today’s church. The modern
church has erected a major barrier to full participation in the
life of the church. Some churches require children to be a cer-
tain age before partaking. Other churches require “confirma-
tion” of teenagers. (A Presbyterian father asked his
12-year-old daughter about the doctrine of confirmation that
she had been taught in a Lutheran school, and she provided a
classic summary: “They say that you get saved when you're
baptized, and then you can then do anything you want until
you apply to get confirmed at 13.” There are Lutheran chil-
dren, I suspect, who have only a marginally clearer cancep-
tion of confirmation than she had.) Still others restrict adults
from the Lord’s Supper until they have gone through some
sort of introductory theology class.

Virtually all churches draw some sort of distinction be-
tween. baptized members and full communing members.
Those that do not make such a distinction among adults at
least make it with respect to children. Churches that allow
children to vote in church elections are rare, but they are all
marked by an unwillingness to allow infant communion. The
