270 CHRISTIANITY AND CIVILIZATION

 

Figura I

Obviously this is an architectural term that has long since
lost most of its biblical meaning for protestants in general.
Onur ghetto is not known for its cruciform cathedrals, so the
architectural imagery is not readily apparent. In this there is
something fundamentally true about that old saying, “out of
sight, out of mind.”

Much more important to most students, I myself being one
at this time, were two features each located at the two extreme
ends of the narthex. Here on the one hand were found the ad-
ministrative offices of the pastors, and on the other hand some-
thing else, something that went almost unnoticed for its architec-
tural significance. The importance of the offices for us students
lay in the fact that Calvary’s pastors were lively articulate cam-
municators — they had to be, for as we shall see later, rhetoric is
all they had going for themselves. So they spent an inordinate
amount of their time counseling the best and the brightest young
Christians in the area. Such was to be expected, in that it was a
time when young soldiers of the National Guard were shooting
real bullets at older students on the Kent State campus. Amidst
such social turmoil, who would even take notice of two small
stairways also tucked away al both ends of the narthex? And
only now, after half my life is gone, do I begin to understand the
foreboding architectural significance of these stairs in the church.

On each side of the narthex adjoining the small office was
a narrow, single stairway leading up to a large open balcony
above. On any Sunday from fall to summer the balcony was
full of students attending colleges in the area, as well as many
other young people whose parents where members of the
church. Only now do I fully understand the balcony’s power-
