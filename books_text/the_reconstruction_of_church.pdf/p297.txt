CHURCH ARCHITECTURE 285

 

Figure 12

to the house’s orientation to the east, a place traditionally
thought of as being protected by a cosmic dragon. Labyrinth
designs just like the Egyptian and Greek meandering style
abound as ornamental motifs on roof tiles, eves, and especi-
ally ritual food vessels fromm the earliest periods of Chinese
history. The meandering pattern itself is another “radical” in
the Chinese language which means “revolve,” and is shown in
figure 13. If you look at the way this sign was incorporated
into a bronze trident, as represented in figure 14, the meaning
takes on special significance when one realizes that the trident

   

 

Figure 13 Figure 14
