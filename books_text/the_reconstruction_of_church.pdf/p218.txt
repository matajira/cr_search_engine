206 GHRISTIANITY AND CIVILIZATION

because it felt so good when he stopped. . . . Mowrer’s every-
day unending efforts to atone do not satisfy. There is no
atonement in them. The attempt is really quite pathetic. Peo-
ple who have ‘graduated’ from his groups often hang around
the edges of the group. They revisit. They seem to be search-
ing for something. One said, ‘I think there must be something
more,’ "1?

One of R. J. Rushdoony’s greatest insights is that men
must confess, and they will confess in any way they can—
informing, gossiping, slandering, breaking down.’ It is all in
the original word God uses to explain confession of sin—
throwing out the hand. Man must throw out the hand of
remorse to God, however, before he finds true relief from sin.
The man who said, “God be merciful to me a sinner” did just
that.

The stoics interpreted homologia to mean living harmon-
iously with nature. Homo meant same, but /ogia, in their opin-
ion referred to logic. Confession was nothing more than an
exercise to preserve order. Like the sensitivity training
groups, they sought forgiveness to keep their inner person in
harmony. Being guilty broke down continuity with nature.
Thus, appeasing the wrath of God was not at issue.

Praise and Proclamation

The first aspect of worship that confession speaks to is con-
fession of sin. Throwing out the hand to God, however,
touches on other areas of worship. True Biblical confession of
sin is closely tied to praise and proclamation. The words we
have been studying even translate this way.

In Scripture, confession of sin leads to praise and procla-
mation. The character of admission of wrongdoing trans-
forms into something else. Therefore, praise and proclama-
tion are nothing more than confession of sin cast in terms of
thanking God or declaring His Word.

Praise. David begins in Psalm 22 and 30 talking about the
heaviness of sin, But these statements are followed by com-

17. Jay Adams, Christian Counselor's Manual (Grand Rapids: Baker, 1973),
p. 88.

18. R. J. Rushdoony, Revolt Against Maturity, (Fairfax: Thoburn Press,
1978).
