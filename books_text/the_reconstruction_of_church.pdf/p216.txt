204 CHRISTIANITY ANQ) CIVILIZATION

have to look closely at the word because of its comprehensive
relationship to worship. Confession in one way or the other
speaks to every aspect of worship.

The Greek word is a compound (homo = same, logia = word)
which is used to mean, “say the same,” “to admit what is said,”
“to confess a charge,” “to confirm the receipt of money,” “to
agree or submit to a proposal,” or “to promise.” The root
meaning is to “say the same thing.” Applied in different con-
texts, homologia and its other compounds meant contract in
legal settings, and prayer, praise, creed, proclamation, and
confession of sin in liturgical backgrounds.

The Hebrew word Aemelogia translates a word (yadeh) in
the LXX that is used to mean “to throw out the hand.” The
hand can be thrown out in a legal way to “strike hands.” In our
day it would be like shaking hands. Like Aomologza, it can also
be used in the liturgical setting to mcan raising the hand in
praise. Of course, these contexts are not mutually exclusive of
each other. Many times, indeed most, the setting for throwing
out the hand is both legal and liturgical. Also like Aomolugia,
padah can speak of throwing out the hand for prayer, procla-
mation, praise, and confession of sin,

 

Confession of Sin

It is proper to start with this aspect of confession as it
refers to worship, When men came into the presence of God,
they normally began with confession of sin. When Israel
entered worship of the Lord corporately, they began the same
way (Neh. 9:3).

Confession means to say the same thing as. God’s Word is
the first statement defining our behavior as sinful. Confession
is the second statement repeating God’s judgment on our be-
havior, In this agreement, the same thing is said. Thus, man
begins worship by mecting God at the very point of exit from
obedience.

Man must come to God at the original point of disobc-
dience. Confession confronts this disobedience thereby draw-
ing together entrance to and exit from God. It is the point of
contact. Although man tries to avoid confronting his sin, God
brings him back to the original point of exit.

Adam and Eve violated the tree of Life by cating of the tree
of the knowledge of good and evil. The tree of life becomes the
