234 GHRISTIANITY AND CIVILIZATION

general equivalent (general equity), we see the Church’s his-
toric rationale for ministerial dress. Elders or Bishops have
special calling. Like the Levites in the Old Testament, this
call should be visualized in their clothing. Nothing in the New
Testament changes this principle. The Book of Hebrews does
away with the High Priesthood, but the specific principle of
priesthood continues. As a matter of fact, Hebrews 5-7 argues
that the Melchizedekal priesthood has been established (Heb.
5:6). The fact that-the writer argues for a priesthood means
that the basic principle of priesthood continues.

We have argued in this second line of thought that the con-
cept of general equity pulls the concept of priesthood into the
New Testament. We have argued from the Old Testament to
the New Testament. Our next line of argument is from
heaven to earth.

Heaven as a Model jor Earth

Heaven is a model for earth. The Lord’s prayer sets up the
paradigm, which is contrary to much of what we see in
evangelicalism. But given the fact that we pray for heaven to
come on earth, heaven is a model for aur activity down here.

Christianity is a religion of internals and externals, and
not exclusively one or the other. Nor is Christianity just a
religion of the invisible as opposed to the visible. Sin might pit
these realms against one another, but redemption removes the
antithesis and transforms history so that it conforms to
heaven.

Heaven progressively comes down to earth and fills it to
the four corners. We pray, “Thy Kingdom come, Thy will be
done on earth, as if is in heaven.” So, our principle is that
heaven (the invisible} is a model for the (visible) earth. We can
even go so far as to say that when we look into heaven to see
what is happening, we find a model for all that we should do
on earth. The Christian world view model ought to be for-
mulated in this manner. But a Christian world view is not our
direct concern, We are discussing special clothing for elders.

Therefore, using our principle that heaven is a model for
earth, we can say that since the elders in heaven have distinc-
tive dress (Rev. 4:4), the elders on earth should wear special
clothing. Also, there are 24 elders —corresponding to 24 divi-
sions of Old Testament priesthood (I Chron. 24-25). The
