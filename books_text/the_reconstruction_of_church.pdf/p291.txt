CHURGH ARCHITECTURE 279

As Christians who believe in the historicity of the Genesis
account we see this basic motif in the garden architecture of
Eden. We are told that the mountain garden of God had an
eastern orientation to a land call Eden; that is, one would be
facing east to enter the garden. We are also told that a river
went out from Eden dividing the world according to the four
cardinal directions. This was the place where God’s special
presence was manifest to man particularly in the evenings,
and it was this place that was forbidden to man, on pain of
death, after the fall. It is important to remember that the gar-
den was present as a witness to the civilization that Cain
founded, a culture that met its end in the great flood.

Ancient enclosures and the labyrinth

If we look back to the civilizations that developed shortly
after the flood we find an essential continuity with the Edenic
pattern. The ziggurat of Ur was literally in the form of a ter-
raced garden mountain, with trees and shrubs planted on it.!°
Acommon symbol inscribed on potsherds found at Ur, as well
as at other sites throughout ancient Mesopotamia, was that of
an elaborated cross or labyrinth design known to moderns as
the swastika courtesy of the Third Reich, but was of a much
older origin than the Teutonic tribes of central Europe. In de-
scribing some of the labyrinth designs found at archeological
sites of the “Baghouz” and “Samarra” cultures, Glyde Keller
writes, “One swastika has twigs and leaves on it representing
a turning tree with branches marking the quadrants, such as
employed by megalithic temple site calendars. In some speci-
mens fish swim in a circle about the tree.”!1 The ziggurat itself
was understood as the cosmic center of the earth and part of a
larger square enclosure that defined the sacredness of its
space. Both mountain motif, and walled enclosure are repre~
sented in figures 4 and 5 (on the following page).

It should be noted that [ have superimposed an underlying
geometry on the ziggurat’s eastern face because as a visual
concept the joining of triangles as seen here represented the

10. Sir Leonard Wooley, Excavations At Ur (New York: Thomas Y. Crow-
ell Company, 1954), pp. 130-133.

11, Clyde Keller, “Tree of Life and Labyrinth.” Phe Epigraphic Sucietp: Oc-
casional Publications, vol. 5, no. 17, part Z (1978), p. 12.
