CULTURE, CONTEXTUALIZATION, AND THE KINGDOM 337

Contextualization is like proclaiming the “truth, the whole
truth and nothing but the truth” to another culture or your
own culture. The result is the transformation of culture (and
people) to be conformed to the King of all culture, and hence,
the answers to our prayers, “Thy Kingdom Come.”
