192 CHRISTIANITY AND CIVILIZATION

worship, occurred. These events made sabbath days and
times central. Pentecost was a time of offering. The first sheaf
had been offered in a wave offering on the day after the sab-
bath. The last offering in a heave offering was made on the
day of Pentecost giving God the new grain. The people had
received the harvest of God, now God received the harvest of
man. Man was expected to give of his produce before he spent
it all. God’s tithe was not to be used as man’s nest egg. His
offering was thereby a statement of dependence on God that
He would continue to provide the next year.

Pentecost carries over into the New Covenant. Paul com-
mands the Corinthian church to bring its gifts on the first day
(I Cor, 16:1ff.). Most translations render this gift as a collec-
tion. It was an offering. Why the distinction?

An offering is mandatory. It is not optional. The New ‘les-
tament believer is required to tithe his firstfruits, one tenth,
because Christ is the Melchizedekal priest who receives the
tithe of Jew and Gentile (Heb. 7:6). So, Grace is specified. We
know how much we owe God, everything. And, the way we
show God that everything belongs to Him is by giving the
firstfruits, one tenth.

So, offering is representative of a man’s whole life. The
first three centuries of the Church define a Christian as an
offerer. The excommunicated person is one who is forbidden
to offer. When time came to place the tithe before God, man
was doing more that giving that one tithe. He was offering
everything. Historically, tithing has tended to be a compre-
hensive event. One man refers to the offering event as follows:
“We might well learn from Negro and Pentecostal congrega-
tions who kinesthetically act out offering by rising and leaving
the pews, walking forward to present their gifts, circling the
church in procession and returning to their seats. Or at Com-
munion, why should not wamen of the congregation come
forward at the time of offertory, lay the cloth and set the table
as they do at home, and bring in bread they have baked? And
cannot the equivalent of offerings at the harvest festival be
much more often employed?

“The late Charles Gore once said that the offertory at an
early christian Eucharist resembled nothing so much as a
modern harvest festival. And Joseph Jungmann deduces from
a mosaic floor excavated at Aquileia that in the Constantinian
era men and women formed an offertory procession bringing
