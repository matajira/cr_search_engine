CHURCH ARCHITECTURE 287

goodness is infinite. As a potter, Thou hast made all things.
Thy sovereign goodness is infinite. . . . With great kindness
Thou dost bear with us, and not withstanding our demerits,
dost grant us life and prosperity.”* Unfortunately most schol-
ars tend to ignore this pre-dynastic time of Chinese history as
being mythologically irrelevant and expend most of their en-
ergy studying the political, economic, and religious dynamics
of empire and its worship.

The ancient cosmic and religious imagery I have been de-
scribing here has played such a prominent and consistent role
in man’s life that sore scholars of comparative religion now
speak in terms of natural symbols. The psychologist Carl
Jang called such powerful patterns of human thought, “arche-
types,” and considered their persistence in the psyche of man
to be “racial memories.”2 Memories indeed, for God has set
the cosmic stage in which “Manhome” spins with appropriate
props of His own invention, and their importance and
tenacious persistence in man’s thoughts comes partly from
man’s created being, and partly from his experience in a re-
demptive history that is controlled by God in every detail,
even down to the very last denial—and the cocks crow,

Form follows sign

I suppose this is the very center of what this essay is get-
ting at, and the key to any revalorization of church architec-
ture. No matter how exhaustive the scientific denial, no mat-
ter how complete the theolagical neglect, the patterns of the
cosmos will always be the signs and signatures of God's re-
demptive glory. As Paul wrote to the Romans, man is without

25. C. H. Kang and Ethel R. Nelson, The Discovery of Genesis: How the
Truths of Genesis Were Found Hidden in the Chinese Language (St. Louis: Concor-
dia Publishing House, 1979}, p. 16. This is an interesting and valuable work
in that it introduces one to an area of knowledge thar has largely been left
untapped by Christians, The work's basic Raw is its uncritical methodology,
and a tendency to overstate its case,

26. Louis Bouyer, Rite and Man: Natural Sacredness and Christian Liturgy,
trans. M. Joseph Costelloe (Notre Dame; University of Notre Dame Press,
1963). In the opening chapters Bouyer presents a fine critical survey of the
history of comparative religion and psychology; and considers the recent
trends by some scholars to use a more consistenlly phenomenological ap-
proach as samething of a boone to liturgical studies.
