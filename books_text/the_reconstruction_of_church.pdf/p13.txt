J. THE CHURCH IN DISARRAY

 

THE CHURCH: AN OVERVIEW

James B. Jordan

MERICANS in general have virtually no understand-
ing of the Biblical doctrine of the Church, The reason
for this has largely to do with the way American culture has
developed.! Whatever the cause of this, however, it behooves
us to try and get a general overview of what the Church is, or
is supposed to be, and that is the purpose of the present essay.

A Religion or a Way of Life?

It is very common in America to be told that “Christianity
is not a religion; it is a way of life.” This catchy statement has
an attractive ring to it, but it is not true. This statement com-
mits a logical error known technically as bifurcation or false
antithesis. The fact is that Christianity is both a religion and
also a way of life.

We are also commonly told that what we need is “real
Christianity, not just ‘Churchianity’ ” This is also a false anti-
thesis, because real “Churchianity” is Christianity, and real
Christianity is “Churchianity.”

Why are these slogans so popular in American Christian-
ity? Why do we hear them on the lips of mass evangelists,
television preachers, evangelistic workers, and theologians
alike? The reason is that so many American Christians have a
false view of the nature of God’s Church, and American
Christians have created many substitutes for the Church.
Travelling evangelists with their soul-winning campaigns are

1. A study of the decline of ecclesial consciousness in America might be
pursued from a number of angles. The reader is directed to James B. Jor-
dan, ed., The Failuze of the American Baptist Culture, Christianity & Civilization
No. 1 (Tyler, TX: Geneva Ministries, 1982); Ann Douglas, The Faminization
of American Culture (New York: Knopf, 1977); and the chapters on revivalism
in Richard Hofstadter, Anti-intellectualism in American Life (New York: Knopf,
1969),
