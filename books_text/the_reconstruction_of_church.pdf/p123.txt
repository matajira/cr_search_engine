THE CHURCH IN AN AGE OF DEMOCRACY iit

boasted of being much more than reality indicated. It is like
the church which has 2,000 in attendance on Sunday morn-
ing, but cannot draw more than a handful on Sunday evening
or Wednesday night prayer meeting. This church might beast
of many great things, but the effort is vaporous.

Four, the boaster creates activity around himself that dies
of its own momentum. At the inquisition of Peter and John,
Gamaliel made an interesting observation. He said, “Ye men
of Israel, take heed to yourselves what ye intend to do as
touching these men. For before these days rose up Theudas,
boasting himself to be somebody; to whom a number‘of men
about four hundred, joined themselves: who was slain; and
all, as many as obeyed him, were scattered, and brought to
nought... . And now I say unto you, Refrain from these
men, and let them alone: for if this counsel or this work be of
men, it will corne to nought: But if it be of God, ye cannot
overthrow it; lest haply ye be found even to fight against God”
(Acts 5:35-39).

Gamaliel argued that this movement would die of its own
natural death. If it did not, then the Jews would know that
they were fighting against God. In the same way, the boaster
creates his own world that dies. But if one boasts in God, he
creates a world that will never die. Moreover, his name will
take on new definition and meaning because it has truly been
identified with something that will last forever!

@

The Medium ts the Message. Here is a fourth cause of schism.
Mode of preaching became more important than the message.
Paul reminded the Corinthians that he did not come to them
with “persuasive words” (2:4 NASV) because the strength of
his preaching was in the message. He deliberately directs
their attention away from the medium. For the Greeks, how-
ever, great preaching conformed to the dramatic style of the
amphitheater.

Today, it is no different. Most seminaries strain to make
bores into elocutionists. Style is everything. It is no wonder
because we have a society which is entertainment oriented.
The homiletical philosophy is to “produce a great entertainer.”
P. T. Forsyth said it the best. “This is the bane of much
popular religion, and the source of its wide collapse. People
