98 CHRISTIANITY AND CIVILIZATION

The Doctrine of Definition not only implies predestina-
tion, but government, To define something is to rule over it, and
dominate it. Since man’s definition comes from God, he must
look to his creator for definition. After creation we find man
defining, or narning the animals. By this act, he is carrying
out the mandate (Gen. 1:27ff.) to dominate the animals. By
naming his newly provided wife, Adam sets up the created
pattern of the woman's receiving the name of the man,!? and
by this act, he dominates. In the New Covenant, one receives
a new Name at baptism (Matt. 28:18ff.). From the Biblical
precedent, we must interpret the renaming at baptism as plac-
ing man under a new rule and government. This redefinition
by name and calling brings me to Paul’s leading comment.

Paul leads into the letter to the Corinthians with a single
statement that runs in the face of democracy. He expresses
practical predestination via his calling. Moreover, he reminds
them of the new government under which they were placed.
Why is this antagonistic to democracy? Because it says that
Paul acts on the basis of a word/call from above, not on the
basis of man’s word, He would never have responded to their
“reports” if it were not for his calling from God.

This is the sum of what Paul will say to them as a matter of
fact. Look closely at the second verse and one will find a sec-
ond reference to the concept of calling. It is as though Paul is
deliberately emphasizing the need to find definition and func-
tion according to calling. The Corinthians must learn to oper-
ate in terms of their calling in the body of Christ, and not ac-
cording to personal inner impulse about anything. Further-
more, they have been placed under a government which is de-
fined by God. Therefore, as the Corinthians are defined by
God, their autonomous behavior is checked.

The doctrine of calling is important to the peace and pur-
ity of the Church. Too often, the heavy emphasis of modern
evangelicalism on “lay leadership” destroys the doctrine of
calling. Lay people should be involved in the work of minis-
try, but most of the time they will do more for the Church by
being the best at their calling. Paul's whole argument about

10. Today it is popular for the woman to keep her awn name. This pattern
in contemporary society is one more symptom of a world trying to deny its
Creator. Nevertheless, the Church must remember that the concept of the
woman's taking on the name of the man is rooted in orthodox Christianity.
