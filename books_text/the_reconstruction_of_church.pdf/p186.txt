174 CHRISTIANITY AND CIVILIZATION

Sunday —every time we eat His body and drink His blood.

Papa: Uh, keep your voice down, willya? They don’t talk like
that around here.

Nathan: But Jesus talked like that.
Papa: I know. But they don’t know that.
Nathan: Let's tell them.

Papa: Let’s not, OK? Not right now.

Nathan: All right. Let’s get back to how kids can become
Christians and have Communion. When they get older they
ask Jesus “into their hearts,” right? So do they just go ahead
and do it when they get to be twelve?

Papa: Not exactly. The grownups have to be sure the kids
really mean it.

Nathan: How can they know that?
Papa: The kids have to cry when they do it.
Nathan: Cry? Real tears? How do they make themselves cry?

Papa: Well, some churches spend lots of time practicing. But,
basically, they just have a preacher get up and tell real sad
stories, so sad that they make people cry. So then the kids cry,
and they walk up to the front of the church and ask Jesus to
come into their hearts. Sometimes this happens during the
summer. The kids go to a special camp where they listen to
people preach at them. Then, on the last night, they all stand
around a campfire and —

Nathan: And listen to scary stories?
Papa: No. Sad stories,
Nathan: Aw, shoot.

Papa; Then they cry, and throw little twigs on the fire, and
ask Jesus into their hearts.

Nathan: Why do they throw twigs on the fire? Do they think
they have to do that to come into the Covenant?

Papa: They think that’s how you have to do it if you’re in the
mountains, It's part of their Summer Camp Liturgy. But if
you're home you don’t need to.
