282 CHRISTIANITY AND CIVILIZATION

(u)

Figure 8 Figure 9

 

 

 

 

 

such labyrinth designs had become a religious and artistic
convention that played a powerful iconic role throughout cul-
tures of the Mediterranean.!7 It is most instructive for us to
note that the Greeks maintained the sacred enclosure’s basic
meaning int a liturgical sense, and architecturally clarified it in
the word “labrys.” The word “labyrinth” is derived from the
Greek word “labrys,” meaning “double axe,” and was used by
the ancient Greeks to describe the sacred enclosure of the Min-
otaur, a death dealing god-king that was half man and half
bull. In the double axe, the four cardinal points can be easily
imagined by extending lines from the center of the axe, out to-
ward the four points of its two edges. It appears as an X with
the two opposing sides closed off, as in figure 9. A large fresco
in the central court of Phaistos on Crete clearly associates the
idea of the labyrinth as a double axe with the enclosed court.18

17, Hans Georg Wunderlich, The Secret of Crete, trans. Richard Winston
(New York: Macmillan, 1974). Wunderlich’s The Secret of Crete, is a very scri-
ous work of revisionism in the area of ancient history, especially that of Min-
oan civilization, He believes that Crete was a religious and commercial cen-
ter for a cult of the dead that dominated the cultural life of the Mediterra-
nean area; and considers Egypt as their best customer. A geologist by pro-
fession, Wunderlich’s real contribution in this work is his critique of Arthur
Evans's faulty excavation of Knossos, and of his rather imaginative conclu-
sions based on that excavation.

18, Charles F. Herberger, The Thread Of Ariadne: The Labyrinth of The Cal-
endix of Minos (New York: Philosophical Library, 1972}, p. 35. Herberger’s
work takes its paint of departure from Homer's remark in The Odyssey, that
