TWO-TIERED CHURCH MEMBERSHIP 131

priestly caste, meaning a sacerdotal system. Ruling elders are
distinguished from teaching elders. Your average congrega-
tion selects its ruling elders by itself, but the teaching elder
must also be approved by a higher body. Thus, the teaching
elder becomes a member of a separate caste. “All elders are
equal, but some are more equal than others.”

In independent churches, the authority of the pastor is
great. There is no Presbytery or other ruling body over the
local pastor. Thus, the caste systern become more rigorous.
The congregation may be able to fire him, however, so he
risks becorning alienated from the “great unwashed,” since
new members have the same voting privileges as the older, ex-
perienced members. It compromises him, since he cannot ap-
peal beyond the congregation to a higher body for protection
or investigation of any charges (or rumors) lodged against
him. Thus, because of the lack of protection, those who vote
on all-or-nothing issues become dominant. These people may
not have the maturity to make such decisions.

The biblical answer is to unify the authority of the entire
eldership but divide the membership between thase who exer-
cise authority and those who don’t. Then the elders can serve
in whatever position their talents allow, without fear of being
swamped in a church election by members who really are not
in support of the church’s standards. The two-tiered member-
ship — voting and non-voting — can be substituted for both the
two-tiered eldership system and today’s system of two-tiered
membership: communing and non-communing.
