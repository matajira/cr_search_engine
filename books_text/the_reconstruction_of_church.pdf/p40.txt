 

28 CHRISTIANITY AND GIVILIZATION

Neither do many of his church members.

Within the congregation the preacher marries and buries
and performs functions. They are his job. Night and day. He
is paid for it.

Congregations and boards of troubled churches get a lot of
mileage out of the messianic character of the modern pastor.
He is to give his absolute everything for the church—time,
money, family, future~everything, without complaint. But
just ask the typical board member to go that extra mile at his
job without overtime or other compensation. The response is
predictable.

The board expects the pastor to perform his paid func-
tions. If he does them well without intruding on their spiritual
siestas, he gets periodic praises and raises. But let him suggest
in the mildest way that the board get off its duff and get to
work, and attitudes reverse quickly. Let the pastor suggest
that the board members are to be more than decision-makers,
more than executives, and the waters are deeply stirred. Here
is aman who threatens our entire rationale for existence. This
man is dangerous. How did he get in here, anyway?

The pastor has made the fatal mistake in the chronically
troubled church. He has taken away the rock and let the light
shine in, One way or another, the problem is solved. Either
the pastor leaves after many sleepless nights and examina-
tions of his own soul, or he capitulates to the system.

His doctrinal position softens. Distinctives he once held
dear have now become negotiable. Beliefs he once envisioned
himself dying for in the face of pagan persecution have now
been effectively amputated by his nominal brethren. He has
become, for all intents and purposes, prophetically
hamstrung. He has become the lowest of all mien ~ the spokes-
man for God who refuses to speak.

The board wants a.puppet, The board gets what they want.
Either that or they lose their pastor entirely. They did not
want renewal, at least not at such expense. They wanted to
hire renewal. They wanted to buy it with money, They
wanted a bargain.

Renewal is expensive. It comes neither cheaply nor easily. Tt
takes sacrifice, personal sacrifice. And these people have proven
that they are no longer willing to sacrifice to build the church.
The new pastor may not have a track record, but the troubled
church does. Who must bear scrutiny? Who is on trial here?
