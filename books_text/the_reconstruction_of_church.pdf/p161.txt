KINDS OF CHURCH DISCIPLINE 149

The Lazssez-Faire Substitute

In Matthew 13 Jesus speaks of the Son of Man sowing
“good seed in his field.” As a consequence of this sowing, a
large harvest of wheat springs up and is—“while men slept”—
accompanied by a virulent growth of poisonous weeds. With
what turns out to be perverted zeal, the servants of the house-
holder seek confirmation from the householder concerning
their plans to uproot the tares. Jesus vetoes the plan for two
specific reasons: (1) His compassionate solicitude for the
wheat lest they be mistakenly plucked-up with the tares. Not
the mistaking of the tares as wheat, but the mistaking of the
wheat for tares is His shepherdly concern. (2) The inevitability
of a perfect judgment at the end of the world when—to para-
phrase Augustine —all of the wolves inside the church will be
sifted while (outside the church) all of the sheep will be sepa-
rated from the wolves. As Jesus said. “The Son of man shall
send forth his angels, and they shall gather out of his kingdom
all things that do offend and them which do iniquity .. .”
(Matthew 13:41-42). Most students of Holy Writ are aware
that this parable has been used as justification for a kind of
laissez-faire approach to church discipline. The central idea
expressed is: “The church has no duty to remove scandalous
members from her body. It is, in fact a Christian duty to let both
the wheat and the tares grow together until the Day of Judg-
ment.”

Acommon method in vogue for combating this interpreta-
tion is proffered by an appeal to verse 38: “The field is the
world.” From these words it is deduced that Jesus is not speak-
ing of the church at all, but rather “the world at large.” It is
our earnest conviction that this refutation is weak in at least
two regards: (1) The growth of the wheat precedes the tares in
order of time so that it is the tares who are actually infiltrating
the domain of the wheat. (2) Verse 41 speaks of the Son of
Man sending his angels to “gather out of his kingdom all

 

erasure, (This is different than the mere parliamentary erasure that is practiced
by a number of churches today.)

Tt should also be noted that the circumstances for any one case always
differ. All we can do is offer some Scriptural guidelines. It would be wrong
to put the Bible in our own preconccived strait-jacket. This is done, for ex-
ample, when a church decides that the party guilty of scandalous sin should
be automatically censured for one year regardless of the circumstances,
