70 CHRISTIANITY AND CIVILIZATION

Revivalism from Moody to Sunday

After the Civil War a large segment of American evangeli-
calism began to retreat from its social activism. The origins of
this reversal may be partly attributed to the impact of the war
itself. The perfectionistic, postmillennial social thought and ac-
tion of antebellum. revivalists was at least partly responsible for
the bloody conflict of the ’60’s and postbellum revivalists hoped
to avoid that path. The rise of Darwinism and liberal social
Christianity, moreover, produced a fundamentalist reaction. In
order to distance themselves from the social gospelers, many
fundamentalists, particularly in the early decades of the twen-
tieth century, tended to shun any social involvement.19? At a
deeper level, the retreat from social action was a manifestation
of the inner logic of revivalism. Moody and other revivalists of
this period were cutting themselves off emphatically from the
Puritan roots af American Protestantism.

Dwight Lyman Moody (1837-1899) was the most impor-
tant evangelist —some would say the most important man~—of
the postwar period. According to Weisberger’s interpretation,
Moody brought revivalism into the Age of Grant, an urban,
industrial, materialistic age.t°# Moody's revivals made use of
an impressive public relations machinery. Prior to Moody’s
arrival in a city, advertisements were displayed on broadsides
and in newspapers, and announcements were made in
churches. Moody often drew upon the resources of local busi-
nessmen, and sometimes gained the financial banking of such
nationally renowned magnates as J. P. Morgan and Cornelius
Vanderbilt II. For the London campaign alone the expenses
totalled $140,000.15 Billy Sunday (1862-1935), whose revivals

103. Marsden, pp. 85-91. McLoughlin’s Modem Revivalism and Weisber-
ger’s They Gathered at the River bath present excellent surveys of the postwar
revivals, There are, moreover, many biographies of Moody and Sunday,
many of them in a hagiographic mold. The best study of Sunday is
McLoughlin, Billy Sunday Was His Real Name (Chicago: University of Chi-
cago Press, 1955), James F. Findlay, Jr., Dwight L. Moody: American Evangel-
ist, 1837-1899 (Chicago: University of Chicago Press, 1969) is a sympathetic,
yet scholarly study of Moody's life and thought. Marsden’s Fundamentalism is
unsurpassed as a study of the fundamentalist offshoot of revivalistic religion.

104. Weisberger, p. 270.

105. McLoughlin, Modem Revivalism, chapter 5; Hofstadter, p. 110. The
expenses for the London campaign were unusually high.
