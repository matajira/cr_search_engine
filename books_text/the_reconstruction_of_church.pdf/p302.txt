290 CHRISTIANITY AND CIVILIZATION

out, nor could they fully appreciate the consequences of an
unfortunately overly rationalistic and stoic approach to the li-
turgical life of the church, Living in the protestant ghetto at
the end of the 20th century, and looking squarely at such
aberrations as “The Crystal Cathedral,” we see them more
plainly. We see the theological exhaustion here, like some
great artistic style fragmented into faddish offshoots that
quickly lose their novelty, And if we are honest with ourselves
we know we must go back as protestants, as theologically and
historically more mature Christians, and recover an architec-
tural and liturgical heritage that our forefathers left behind
because of fear and error.

God help us one day to recover and even surpass the kind
of aesthetic and architectural committment to Christianity
that produced church structures like Hagia Sophia at Con-
stantinople. It was in this Church of the Holy Wisdom in the
10th century that leaders from the Russ of Kiev were over-
whelmed by the presence of God, From the “The Primary
Russian Chronical” comes a witness of the splendor, and a
reverence for God in worship that is all but lost on modern na-
tion states, as the representitives from Kiev tried to explain
their experience to Prince Vladimir. “Then we went to
Greece, and the Greeks led us to the edifices where they wor-
ship their God, and we knew not whether we were in heaven
or on earth. For on earth there is no such splendor or such
beauty, and we are at a lass how to describe it. We only know
that God dwells there among men, and their service is fairer
then the ceremonies of other nations. For we cannot forget
that beauty.”29

The fact that the Church of Holy Wisdom created an im-
pression that heaven and earth were coalescing is not acciden-
tal. Seripture teaches us that New Govenant worship actually
takes place in heaven. Some passages indicate that God’s
heavenly cloud comes to earth in worship, as at the Mount of
Transfiguration, and other passages speak as if the saints are
caught up to heaven during worship, as in the Book of Reve-
lation,%° It is because worship takes place in heaven that it is

29. Thomas Riha, ed., Readings in Russian Civitization (Chicago: Univer-
sity of Chicago Press, 1974) 1:9.

30. On worship in heaven as our patiern, see discussions in Jordan,
Sociology of the Church.
