THE CHURGH IN AN AGE OF DEMOCRACY 94

speaks to the proper bonding or the sacraments.

The specific theme of Corinthians, however, is found in
Paul’s comments which are judgmental or tearing in nature.
Hebrews 4:12 says, “the Word of God is quick and powerful,
and sharper than any two-edged sword piercing even to the
dividing asunder of soul and spirit, and of the joints and mar-
row.” The tearing of the Word of God is a judgmental process
that fits a basic covenanial patiem in Scripture.

The covenantal pattern in Scripture consists of (1) A Word
from God; (2) A response from man; and (3) God's evalua-
tion/judgment. Paul’s purposes, clearly seen with a cursive
reading of both letters to the Corinthians, are in the third
category. Paul spent time with, taught, and brought the Word
of God to the Corinthians (Acts 18). The “reports” (1:11, 5:1,
7:1) sent to him were a statement of their response to the Word
of God which he brought to them. The time for evaluation had
come, and so, not liking what he saw in their response, his let-
ter essentially passes judgment. And this judgment tears to
the heart of man.

Paul’s emphasis in I Corinthians points out why it is dan-
gerous to try and build a whole theology on the epistles. For
the most part, they are evaluative in nature, and the writers
presuppose that the Word from God has already been given to
these people in the Old Testament and Gospels. Paul knew
that they had received the Word of God already because he
was the one who took it to them.

Ironically, in the case of the Corinthians, judgment was
their major problem. The Corinthians did not want to be torn

. by the Word of God, and here is point of contact with our
previous discussions on the Greek mind. They were under the
influence of Greek thought, which said that life comes from
death. Death is the ultimate chaos. Fitting this in with our
chart above, death is the injection of chance from outside the
universe. But we must keep in mind that death, for the pagan,
is not the same as facing judgment. A pagan might be willing
to die, but this does not necessarily mean that he is willing to
face the judgment of God. A person who commits suicide, for
example, is most often trying to avoid some sort of judgment.
Of course, the believer knows that the individual will go to
meet his/her Maker, and face judgment anyway.

At the same time, we must state that pagan thought wants
life apart from the judgmental-death-tearing of the Word of
