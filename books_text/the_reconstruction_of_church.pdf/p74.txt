62 GHRISTIANITY AND CIVILIZATION

verting sinners.55 His emphasis, therefore, was psychological
rather than theological. In his Lectures Finney claimed to have
discovered certain laws of the mind, a knowledge of which
would enabie ministers to promote revivals more systemati-
cally.56 Finney encouraged self-examination for its efficacy in
preparing the mind for the hearing of the Word. Unless each
individual prepares himself for worship, religion will become
mechanical and little “deep heart-work” will be accomplished.”
Preaching, moreover, should be practical, direct, uncontro-
versial, conversational. “A prime object” of preaching ought to
be the conveying of the impression “that sinners are expected
to repent NOW."58 Finney stressed to preachers that “The
manner of saying it is almost every thing,” and cited a case of a
young preacher whose “manner of saying some things I have
known to move the feelings of a whole congregation.”5> The
key was to bring the listener to “the moment he thinks he is
willing to do any thing.”® Various “new measures,” including
the anxious mecting, the protracted meeting, and the anxious
bench were especially effective in bringing sinners to that mo-
ment. ®t

Finney’s theology was a mass of contradictions. Revival
was a work of man, but man must be assisted by God. Man
has free choice; his sin is merely a prejudice toward evil that
can be entirely overcome. Near the end of his life, however,
Finney admitted he had “laid . . . too much stress upon the na-
tural ability of sinners to the neglect of showing them the na-
ture and extent of their dependence upon the grace of God.”62
He came to the rationalistic conclusion that, if God com-
manded man to do something, man had the power to do it, yet
he tended to speak of conversion as an emotional experience.
Finney’s theology was unsystematic except in its thorough re-
jection of Calvinism, and his effect upon American theology
was basically to shatter the Taylorite and Beecherite preten-
sions to Calvinism. Open Arminianism was losing its stigma.

55. Cross, p. 160.

56. McLoughlin, Modem Revivalism, p. 86.
87. Finney, p. 50. See ch. IT,

58. Ibid,, p. 206.

39. Ibid., p. 212.

60. Ibid., p. 268.

61. Ibid. ch. XIV.

62. McLoughlin, “Introduction,” p. 1.
