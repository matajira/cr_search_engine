CHURCH ARCHITECTURE 289

white doves floating upon the windows, while in the parking
lot it seers every car has it’s yellow smiley face, or a “Honk if
you love Jesus” sticker on the burnper.?? How can such a pitiful
display stand up to the powerful iconography of the American
state? Ne wonder petty bureaucrats find it unintimidating to
serve notice of their federal authority over church matters in
such places as these.

This kind of iconographic struggle is not new to Christian-
ity. The old Byzantine church struggled with similar trends,
and was afraid that the icons of Christ would eventually be re-
placed by the encroaching icons of the Emperors. The early
reformers struggled against the powerful imagery of a deca-
dent Romanism so completely that they swept away a rich
Christian heritage that could have been theirs for the taking,
and more importantly, eventually lost touch with the signifi-
cance of the word made visible in the life of Christian wor-
ship. Louis Bouyer, a prominent liturgist of the Roman Cath-
olic church, acknowledges that the reformers were fighting
against a Roman church that had declined into magic and sv-
perstition; but also points out that the protestant reaction was
itself superstitious when it came to the rite of the cup and the
loaf. In tracing the consequence of this fear in protestant
church architecture he points us to the vanishing altar: “Altars
plainly visible from all sides were substituted for the altars of
the Middle Ages which had been completely shut off in a walled
chancel. In Lutheran churches the pulpit was brought as close
to the altar as possible. On the altar itself, the Bible replaced
the sacred vessels, so that the altar came to be regarded as a
kind of secondary, imperfect place for preaching. In Calvinist
churches, the pulpit, as a matter of fact, came to dominate
and eventually absorb it. In the majority of Reformed
churches, the altar soon disappeared. At most it remained as
a kind of unrecognizable relic, in the shape of a table, which
was used ritually only at intervals, and almost with an uneasy
conscience.”?8

Of course the reformers did not have the time or the
perspective that we have today in order to sort these matters

27. On this sentimentalizing trend in American Christianity, see Ann
Dougias, The Feminization of Amencan Culture (New York: Alfred A. Knopf,
1977),

28. Bouyer, Rite and Man, p. 59f.
