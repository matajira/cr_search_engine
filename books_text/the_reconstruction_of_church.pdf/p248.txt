236 CHRISTIANITY AND CIVILIZATION

clothing when he leads worship. Careful observation will not
only reveal that the Apostles wore the rainbow, but that their
dress parallels Christ’s clothing. There are some distinctions,
but the connection is there because the Apostles were given
special revelation and completed the revelation of Christ,

So, to dress like Christ and the Apostles is to say that the
minister has authority, not in his person, but in the office of
Christ, duly established by the foundational work of the Apos-
tles. Not to wear liturgical dress goes in the precise direction
the non-liturgist is concerned not to go. The pastor who
preaches in his three-piece suit speaks a theology by what he
wears. Unfortunately, it conveys his person at the one time that
he most definitely should be speaking for Christ and pushing
his own person into the background. Liturgical dress helps to
focus the congregation on the work of Christ and the Apostles
because the minister has no authority outside of them.

A quick survey of liturgical dress further makes the point.
The dress of the minister should reflect the High Priesthood of
Christ. Historically, clothing for special worship has been the
white alba and stole, the former being a covering or robe, and
the latter being a mantle or yoke of calling (see Elisha). The
stole seasonally changes to different colors to match the
Church year,

The Church calendar visually demonstrates that the
Church believes the seasons of the natural order are subor-
dinated to the redemptive order, and that time is being
redeemed and moved in a line toward final judgment. It
therefore steers the people of God away from the notion that
the world moves along naturally. The minister’s stole matches
the colors of redemptive progress to show that Christ controls
history through the management of His servants, and that is
the very point: ministers are servants of the office of Christ.
Their person is covered up and Christ stands forth.

But what about everyday clothing? Historic, orthodox
Christianity has believed that time is divided into general and
special worship. What the minister does during the week is of
amore general nature. Because he does not actually leave his
office, he should continue the principles mentioned above.
The exception is that the character of his work changes.
Around the throne of God he represents the glorious reign of
Christ. During the week the minister’s slavery to Christ
stands forth.
