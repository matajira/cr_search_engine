THE LITURGICAL NATURE OF MAN 197

we begin by a statement defining our view of God and man.
At the beginning of the service man acknowledges that he is a
sinner, and he refers to God as “the Lord and giver of life.” Fi-
nally, all this time, man should be aware that an important re-
union is taking place. It is a reunion of the marriage of heaven.
and earth.

One final comment about worship as an act of submission.
Since we make a statement of submission, and thereby
reunite with God’s people through the ages, worship is a disci-
pline. One’s view and practice of worship is indicated in the
service. It is possible for this truth to be lost one of two ways:
On the one hand, disciplined submission in the service can be
only show. In this case, it is a substitute for submission.

On the other hand, a loose view of worship is most often
an indicator of loose discipline in the church. If the service is
preacher centered, it means that the preacher is the source of
discipline. He can even dominate in such a way that the peo-
ple are confused as to what reunion is occurring. In the mid-
dle ages, the priest centered service led to a belief that reunion
with the priest was occurring. He was the one holding heaven
and earth together, so that the service was seen as orbiting
around him.

One other application: We have found that a person’s
response to the worship of the church is quite telling. For ex-
ample, we had a man who would not say the confession of sin.
He did not believe a Christian needed to confess sin, and cer-
tainly not every week. He was not here long before his rebel-
lion became more open, and he wanted to run the church.

Others have refused to sing some of the music. They do
not like it for one reason or another. But the Elders set the
agenda. If they select a certain style of music, the congrega-
tion can try to prevail on them in other ways besides open pro-
test. Actually, silence during the singing is one of the highest
forms of protest. In the final analysis, one should go to
another Church rather than protest this way.

T believe the problems occur when music is atornized. Tn
our day, tunes are elevated in importance above the words. As
a matter of fact, the early and medieval church did not want
the function, the tune, to take predominance over the form,
the words. John Calvin, for example, favored the chants of
the early church for this reason, If one sees anything in the
early music of the church, he should perceive that the music is
