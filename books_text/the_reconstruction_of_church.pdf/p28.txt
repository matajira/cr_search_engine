16 CHRISTIANITY AND CIVILIZATION

leave their homes three times a year and attend feasts in
Jerusalem. At the Feast of Tabernacles, they built little tents
to live in, grouped around the Ternple. Similarly, the New
Testament speaks of “love feasts” in Jude 12. Christians came
together for a common meal, usually connected with the sac-
rament, as we see in 1 Corinthians 11:33, In the early Church,
the love feast, or “agape” (ah~GAH-pay) as it was called, was
a big covered dish supper or breakfast, Churches today still
have such suppers, though they usually are not called “love
feasts.”

It is important to attend all such fellowship functions in
the Church, Of course, some large churches have so many
different fellowship functions that one cannot attend them all.
But where possible, it is important to get together with other
Christians at the fellowship functions appointed by the elders.
And it is important that the poorer members of the congrega-
tion be sponsored by the Church, so that they also can attend
(Deuteronomy 14:29).

The second kind of occasion which is appointed and over-
seen by the elders is diaconal. Here we place the various
works of mercy and practical matters which the Church is
concerned with. There are two kinds of diaconal labors that
we as Church members need to throw ourselves into. One is
working on the Church property at workdays set up by the elders
(or their assistants, called deacons in many churches). There
is real Spiritual value in coming out for workdays. I, for in-
stance, am a real fumble-finger when it comes to driving
nails, or doing just about any type of handyman work. All the
same, I find it of real value to come out and do whatever work
1 can when we have workdays at our Church, Similarly, your
Church may ask the men to sign up to take turns mowing the
grass, or the ladies to sign up to take meals to the sick or to
new mothers. Be sure you jump in and do your part. Don’t
think your time is too limited, or that you are too important,
and don’t try to get out of it by paying someone else to take
your part; for you will only cheat yourself. Ministering with
the hands, especially in serving the Church, is of real Spiritual
benefit.

‘The second kind of diaconal service is minisiry to the poor
and to the sick. Traditionally, the Church has tried to remember
the poor in a special way at Christmas and at Easter. This is a
wonderful thing to help with. Churches should of course
