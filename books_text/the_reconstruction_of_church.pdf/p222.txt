210 CHRISTIANITY AND CIVILIZATION

and the carth is my footstool: where is the house that ye build
unto me? And where is the place of my rest?” This is what I
call footstool theology. It describes the victory of God over all His
enemies. The New Testament says of Christ that “this man,
after he had offered one sacrifice for sins forever, sat down on
the right hand of God; from henceforth expecting [waiting] till
his enemies be made his footstool” (Heb. 10:12-13). The Greek
says “a footstool for his feet.” Jesus challenged His opponents
with a reference to David’s words concerning Christ’s turning
His enemies into His footstool, and no man ever asked Him
any more trick questions (Matt. 22:41-46). God symbolically
places His feet on top of the earth as a man sits on a chair and
rests his feet. In God's case, the chair He sits on is a throne.

If we view the earth as God’s footstool, then the tabernacle
and temple can be seen as His throne, a place of majesty and
judgment. The place of worship is described as a footstool.
“We will go into his tabernacles: we will worship at his foot-
stool” (Ps. 132:8). Kline’s description of God’s glory cloud
deals with the symbolic representations of God’s majesty.
“God's theophanic glory is the glory of royal majesty. At the
center of the heavens within the veil of the Glory-cloud is
found a throne; the Glory is preeminently the place of God's
euthronement. It is, therefore, a royal palace, site of the
divine council and order of judgment. As royal house of a
divine King, the dwelling of deity, it is a holy house, a temple.
Yet the Glory is not a stalic structure, but mobile, for the
throne is a chariot-throne, Spirit directed and propelled
through the winged beings, a vehicle of divine judgment,
moving with the swiftness of light to execute the sentence of
the King.”!

Kline argues that the creation itself is designed after the
pattern of a glory temple, and he cites Isaiah 66:1. “Similarly,
the natural heavens consisting of heaven (the firmament), and
the heaven of heavens (the cloud waters ‘above the heaven’)
are regarded as God’s royal chambers and chariot. In har-
mony with the. identification of heaven and earth as a
macrocosmic temple, the earthly tabernacle and temple that
appear in redemptive re-creation symbolism are designed, as
various architectural features show, to be a microcosmic rep-

1. Meredith G. Kline, Images of the Spirit (Grand Rapids, Michigan:
Baker Book House, 1980), pp. 17f.
