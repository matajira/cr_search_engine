90 CHRISTIANITY AND GIVILIZATION

covenantal faithfulness, in man (see above chart). Continuity
is in the Word of God, or the covenant. The Apostle Paul says
that all things “hold together m Christ” (Col. 1). Thus, contin-
uity in creation comes from God, and change results in a neg-
ative or positive direction according to man’s response to the
covenant. Obedience leads to blessing and growth—all the
imagery in the Bible about vines (Jn. 15), and trees growing
up by rivers of living water (Ps. 1)— and, disobedience leads to
decreation and curse with the unrighteous becoming like a
desert.

In other words, faithful obedience to God’s Word brings
life, and disobedience results in death. Life is not static, and
therefore change comes as man obeys the Word of God.
Science and all disciplines of the mind of man advance when
man lives according to God’s Covenant/Word.? The Bible
does not inhibit man, as the pagan would have us believe, it
provides true liberty with which to advance humanity, and the
proper presuppositions with which to observe and understand
the universe.

Therefore, the Greek mind seeks to make creation or
nature God by putting the constants in creation. And at-
tempts to manipulate God by reaching outside nature. This
was the context into which Paul brought the Gospel. As we
proceed to examine the basic theme of I Corinthians, we see
what Paul was attempting to combat in a fuller light.

Structure and Theme of I Corinthians

i Corinthians divides into three parts. At the beginning of
each major section—1:10-4:21; 5:1-6:20; 7:1-16:2—we find a
grammatical key which says in effect, “it has been reported
among you.” Thus, Paul uses these basic reports concerning
the Corinthian Church to structure his thoughts.

These three sections group under the three marks of a true
Church—the ministry of the word, the administration of the
sacraments, and the exercise of discipline. The first section in
Corinthians concentrates on the proper preaching of the Word
of God. Nothing, not even the minister, should interfere with
the preaching of the Cross. The second section addresses the
problem of the lack of discipline. And the final pericope

A, Science and Greation, pp. 138ff.
