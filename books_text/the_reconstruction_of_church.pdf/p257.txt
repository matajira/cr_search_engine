CHURCH MUSIC IN CHAOS 243

time, Hymns Ancient and Modern.

The custom spread to presbyterians, and to others to a
lesser extent, apparently “for no reason but the obscure and
irrational notion that the Church of England knew its work in
matters of liturgy.”® Routley goes on to point out that the
Anglican scholars realized their error, and began abandoning
the practice in the 1920s, and other groups have since followed
suit, though the custom is now ingrained in some American
groups. That this is perpetuated in the Trinity Hymnal is one
more unfortunate aspect of that book.

It will be simpler to comment on the custom of amening
hymns here than to return to it later in this essay, so let me
make a few remarks. Some American hymnals use amen with
serious hymns, but leave it off for gospel refrain songs. An ex-
ample is the Worship and Service Hymnal (Hope Publishing
Co.), used in many presbyterian churches. (The Trinity Hym-
nal amens all songs.) The older Service Book and Hymnal of the
Lutheran Church in America (one of the finest hymnals ever
put together), uses the following principle: “Amen has not been
provided for hymns which are didactic, hortatory, narrative,
or contemplative, but it appears, properly, at the conclusion
of hymns which end in prayer or praise.” If amen is to be
used at all, this seems a salutary rule. The Christian Re-
formed Psalter Hymnal uses no amens at all.

Eighth and finally, we look at the Responsive Readings,
Here the 7hazty Hymnal stands in direct line with an unfor-
tunate and unBiblical American tradition of responsorial wor-
ship, in two respects. First, instead of psalms, we have “selec-
tions.” These selections are sometimes happily made up of just
one psalm, but usually they are made up of two or three
psalms read one after another. The psalms are not grouped by
theme, but arc presented in order, one after another (except
for those which are skipped altogether). The results are
unintelligibly random untheological groupings which are con-
ducive only to rote use.

But second, and far worse, the responsive readings simply

9. Idem, We can be certain of one thing: Most presbyterians knew
nothing about historic liturgy, and thus were in no position to evaluate the
Anglican trends. Most conservative presbyterians still know nothing about
it,

10. Service Book and Hymnal (Minneapolis: Augsburg, 1958), p. 287.
