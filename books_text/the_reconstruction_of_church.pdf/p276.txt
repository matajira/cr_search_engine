264 CHRISTIANITY AND CIVILIZATION

(New York: Knopf, 1977; paperback from -Avon), a book
which is valuable in so many respects that I can only say one
thing about it: Buy it and read it as soon as you can.

Important is Eric Routley, Church Muste and the Christian
Faith (Carol Stream, IL: Agape [Hope Pub. Co.], 1978). I
cannot go along with everything Routley says in the way of
theology, since his perspective is that of a modernist at some
points, but his observations on Church music are both sharply
pointed and valuable. He sets forth two canons for judging
Church rausic (p. 20): “On the one hand we have the principle
that it is not the avoidance of error but the generation of good
that we are to look for. On the other hand we have the princi-
ple that ‘the Christian’s goal must be maturity in Christ, . . .
We are therefore on firm ground in saying that where church
mausic inhibits the growth of the Christian society to maturity
it is to be censured.”

Routley has been around a long time. He has written over
thirty books, including some of the most definitive studies of
hymns ever written. He is entitled to be irascible, and he is
(delightfully) so in this book. Routley blasts at organists who
employ the “cheap technique” of hiking the pitch of a hymn up
for the last stanza (p. 29). Throughout his book he skewers
pretentiousness as a cover for inadequacy, and as hindering
the maturity of the Church. But in all of this, Routley keeps
us aware that “the church’s worship is ‘a conversation which
began long before you were born and will continue long after
you are dead’... . The great church musician listens to the
conversation already going on before joining in it” (p. 89).
And that, of course, is the problem with modern “Jesus
music”: Those involved in it have no awareness at all af the
ages-long conversation of liturgy, and instead are trying to
bring into the Church the meagre coin of current worldly chit-
chat, It ought not to be permitted.

“When Vaughan Williams wrote that good taste is a moral
concern... , Lam with him and I think the church should
be. Failures of taste are ultimately failures of nerve; bad taste
argues insecurity, competitiveness, and a lust for quick
results” (p. 96). If this type of comment sharpens your ap-
petite, you will find that Routley’s book is salutary reading.

Finally (I am being selective, not exhaustive), I can rec-
ommend a careful, judicious reading of Austin C. Lovelace
and William C. Rice, Music and Warship in the Church (Revised
