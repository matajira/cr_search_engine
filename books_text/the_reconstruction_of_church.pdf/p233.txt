THE MARRIAGE SUPPER OF THE LAMB 221

and that his family was not in need of such tokens of ethical
virginity. He was the head of this family, as well as the repre-
sentative head of Israel; Ged would not permit him to escape
the consequences of such an act of rebellion.

Second, at whose feet was the bloody flesh laid? Cassuto,
the great Jewish scholar, argued that it was at Moses’ feet.16
Kosmala argues that it was the son whom Zipporah con-
fronted.1? He needed the covering. Zipporah knew that God
would pass over her son, and spare him, if he bore the mark of
blood. I agree here with Kosmaia’s interpretation.

Third, did she toss the flesh at his feet? Cassuto thinks she
touched Moses’ feet. Kosmala believes that Zipporah touched
the degs of her son. He writes: “It is important, therefore, to
make the sign on the child visible. It must be seen. That is nec-
essary for any blood-rite. When God commanded the Israel-
ites to smear the blood of the slaughtered animal on the lintel
and the door posts, it was done in order that it might be seeu:
‘When I see the blood, I will pass over you . . . and not smite
you (Ex. xii 13 and 23).”'" The language is not specific, sa we
can only guess. The main point is this: there was a blood cover-
ing, visible before God, which led God to cease the attack on
Gershom (or possibly on Moses). And this blood covering was
applied to the degs. (We have returned at this point to the sym-
bolism of the doorposts.}

Finally, whom did she call “blood-bridegroom”? Instinc-
tively, we assume it must have been Moses. Bush argued that
it was her son: “Aben Ezra remarks, ‘It is the custom of
women to call a son when he is circumcised a spouse (hathan).’
Kimchi in his Lexicon . . - concurs in the same view, which is
also supported by Schindler, Spencer, Mede, and others,”!9
The significant aspect of Zipporah’s remark is in the phrase it-
self, “bridegroom of blood,” not the person to whom she was
speaking. She was identifying the significance of the circurnci-
sion, which pointed to the bridegroom's act of mercy in providing the
blood covering needed by the wife. Without this covering, the bride
was legally unprotected. If Moses, as the husband, was un-

16. U. Cassuto, 4 Commentary on the Book of Exodus, translated by Israel
Abrahams (Jerusalem: The Magnes Press, The Hebrew University, [1951]
1974), p. 60.

17. Kosmala, of. cit., p. 24.

18. Ibid.

19, Bush, Exodus, p. 69.
