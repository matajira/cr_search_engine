THE CHURCH IN AN AGE OF DEMOGRACY 93

a form of judgment in the Church, and specifically commands
the Corinthians to “purge out the old leaven of malice” (v. 8).
This purging is the tearing side of facing judgment and ap-
pears throughout the Old Testament.

Covenant cutting in the Old Testament involved some
form of tearing both symbolically and really. But normally
those who received the sign of judgment were not torn in
pieces. When Israel was delivered from Egypt, the nation was
torn from the world to be led to the Promised Land. The Red
Sea was the theological boundary which indicated the rebirth
of the Nation at the time of Crossing. Preparation for this
departure was the removal of leaven that symbolized separa-
tion from the world (Egypt). At the first Passover, death ap-
peared on the doorposts, and ethical tearing by removal of old
leaven averted the Angel of death which literally tore the
Egyptians like wild beasts. Thus, the Feast of Unleavened
Bread was a time of separating from the world and its ways to
prepare for renewed empawerment, Pentecost (Lev. 23:15ff.).

With this Feast as the backdrop to our discussion, we see
that in each section of I Corinthians Paul develops death,
judgment, and tearing as major themes. In some cases the
Corinthians wanted to avoid death to escape their responsibil-
ities of personal and corporate judgment. In other cases, they
tried to inflict some form of death, or create a pseudo cere-
mony to escape the responsibilities of covenant life.

In the first section of Corinthians, Paul addresses the
problem of schism. Schism is literally rending the Body of
Christ. He attributes this problem to an undue emphasis on
dynamic men and their ministries in the Church. His correc-
tive, however, is the preaching of the Cross~a message of
death and judgment—which Paul says was the center of his
preaching. The Corinthians did not want “negative” preach-
ing, They wanted positive preaching that would soothe them.
They did not want to listen to the uneloquent with the right
message. They wanted flashy, attractive, charismatic preach-
ers. In other words, they thought that life would come to them
apart from facing the judgment-tearing of God.

Paul’s attention in the second section turns to the subject
of judgment in relationship to Church discipline. The Corin-
thians wanted spiritual growth without pain. So, when prob-
lems arose in the Church, as they are certain to arise in any
Church, they took the path of least resistance. Discipline
