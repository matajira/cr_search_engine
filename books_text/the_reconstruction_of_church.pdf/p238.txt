226 CHRISTIANITY AND CIVILIZATION

Rev. 22:17, 20 and Cant. 8:14. The ‘marriage supper, which is
the Holy Eucharist, is the weekly consummation of the mar-
riage. While Christ will return to end history some day, His
weekly meeting with His Bride is the ‘swift coming’ here in-
vited, Christ feeds His Bride, which is the Spiritual reverse
and correction of Adam's being fed by his wife (Gen. 3:6). In
paganism, the fact that the festival supper is the Spiritual
form of the consummation of marriage was perverted and
lost, so that sexual relations were viewed sacramentally. The
Bible nowhere teaches that sexual relations are sacramental.
Physical marital relations are analogous to Christ's love for
His bride, and the Spiritual expression of that love is seen ini-
tially in the sacrament of Holy Baptism (that is, New Cove-
nant circumcision) and repeatedly in the sacrament of the
Holy Eucharist.”2?

In this way, we see that the weekly celebration of the
Lord's Supper is, in part, a testimony of our Spiritual security
due to the tokens of virginity provided by our heavenly Bride-
groom.

27, Ibid., p. 260.
