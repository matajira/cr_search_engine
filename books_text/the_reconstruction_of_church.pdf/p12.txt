xiv CHRISTIANITY AND CIVILIZATION

maximizing its catholicity by distinguishing between voting
and non-voting members. North goes on to apply this concept
to the social problems caused by immigration. Jim West, a
pastor in the Reformed Church in the United States, argues
that the Church needs to return to the practice of excommuni-
cating apostates and rebels, and eschew ail compromising
substitutes. °

Part II deals with worship. David Chilton, a scholar in
residence at the Institute for Christian Economics, provides a
humorous introduction to this section in the form of a dia-
logue between himself and his young son, who was amazed at
the circus-like atmosphere of worship he found at an evangeli-
cal Church his family visited a while back. Ray Sutton uses
his experiences in Dallas at an experimental hyper-informal
Church to discuss the inevitability of formality and liturgy in
worship. Sutton’s other essay in this section argues that
clergymen should wear distinctive garb, thus setting a pattern
that affirms the sanctity of every calling under God. Gary
North’s essay here deals with the Lord’s Supper as a sign of
Christ’s marriage to His bride. My contribution to this section
concerns the present abominable state of music in the
Church. And James M. Peters, a computer consultant and
student of art and symbolism living in Tyler, Texas, provides
a challenging essay on the need to “revalorize,” that is,
“reinvest with proper symbolism” the architecture of our
churches.

Part IV deals briefly with the outflow of the worship of the
Church. George Grant, director of The Christian Worldview,
Humble, Texas, argues that just as God feeds and clothes us
in worship, so we must be actively involved in helping the
poor in our midst. Marion Luther McFarland, pastor of the
Ogemaw Reformed Presbyterian Church of West Branch,
Michigan, argues that the mission of the Church is to
transform all of culture, but not in the way “liberation
theology” would have us do it.
