CHURCH RENEWAL: THE REAL STORY 43

Those who are sovereignly called must visibly enter into it by
the command of God. Others are not fit to enter it. No ground
is neutral, and no decisions concerning the Covenant are au-
tonomous. Each prospective member is faced with a decision.
Submit to the Covenant or deny it. To demure is to deny.

Tithing and attendance are only two of many Covenant
obligations that must be recognized by the troubled church
longing for renewal. No less important are Sabbath-keeping,
restricted communion, and church discipline (the return to
concrete spiritual sanctions). All work together in God’s or-
dained plan for the dominion church of the Lord Jesus Christ.
No particular may be deleted without marring the whole, and
the concept of the Covenant may not be deftly abstracted
from its concrete particulars without making a mockery of the
Word of God.

Reality in Renewal

As long as the church in trouble demands so little in terms
of time and money from its members, it can continue on
almost indefinitely without making any real progress. Those
members with pessimistic outlooks can see their dreams come
true, as the church, unlike the state, withers away. It has served
them well in the limited sphere they have graciously granted
it. They have expected little from the church and have gotten
even less.

It is not the current members who will suffer for the failure
of the church, but the children and those afar off. With few
exceptions, the children of members of troubled churches
depart from the church, if not the faith itself. There really is
no continuing covenant community, just a sterile remnant
frozen in the history of the past three decades. For these ter-
minal churches it is the last remnant.

The reality of renewal is that everyone is to blame—pastors,
congregations, boards, and mission organizations. Preachers
accept intolerable situations and tolerate them. Boards are
self-satisfied searchers for the magic men who will put their
churches back on the local map. Congregations sell the work
short by refusing to invest, and missions boards. . . well,
missions boards seern content to hold the same meetings, give
the same speeches, and shift the same people from church to
church, year after year.
