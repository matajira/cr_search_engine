156 GHRISTIANITY AND CIVILIZATION

nicator was motivated by that church’s joyous acclaim of for-
nication as a holy life-style. So, it might be imagined: “Our
mourning for this brother’s sin is proof enough that we have
not Corinthianized.”

The unambiguous evidence, however, challenges the in-
terpretation that the Corinthians were revelling in wholesale
iniquity, Paul has a controversy with the Corinthians, not
because they did not mourn, but because they should have
mourned in such a way that the one “that has done this thing
might be taken away from you” (1 Corinthians 5:2). In other
words, there was mourning but there was no mournful follow-through of
excommunication. To be sure, the Corinthians were an assembly
of mirth-makers; but this does not mean that they were self-
consciously rejoicing in presumptuous sin. They were, as
Charles Hodge says, “elated with the conceit of their good
estate, notwithstanding they were tolerating in their commun-
ion a crime even the heathen abhorred.” The crucial word in
this passage (1 Corinthians 5:2) is the word fena. (translated
“that”). Because Aina is causative in force, Paul’s argument
could be filled-in to read: “. . . im order that he that hath done
this deed might be taken away from among you.” They glor-
ied in spite of the sin; it was a glorying in the “Christian” cli-
mate of the congregation without specific censure of this speci-
fic sin. They should have acted like a pestilence was among
them which called for immediate judicial action and prayer,
except in this case they. were found derelict not in prayerless
declension, but in judicial laxity. The duty of removing this
man “away from you” entails the responsibility to congregate
for the purpose of delivering “such a one unto Satan’ (verses 4
and 5). Lamentation by itself is not enough. Where it is used
to induce the condition of judicial paralysis or is substituted
for judicial action in any way, the mourning itself is Corinth-
jan in quality.

Using Excommunication Minor as a
Substitute for Excommunication Major

Calvinistic churches have rightly striven to sanctify the
seal of the covenant which is portrayed in the sacrament of the
Lord’s Supper by encouraging the broken and contrite to par-
ticipate and discouraging (or fencing) the profane from the
same. When such discouragement is directed against a pro-
