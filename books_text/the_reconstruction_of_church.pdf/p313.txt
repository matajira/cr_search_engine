THE RECLAMATION OF BIBLICAL GHARITY 301

were buried beneath an avalanche of need. We had to do
something. So we did.

We made mistakes. Lots of them. Sometimes we learned
from them; sometimes we, for quite some time, didn’t. Even-
tually, however, a pragmatic model was constructed that fit both
the Biblical precepts we'd discovered through diligent study, and
the obvious need we'd confronted through diligent labor.

What we've achieved in Houston is not the panacea for all
social ills fram now ’til evermore; but it is a start. What we've
learned in Houston is that functioning models of Biblical
charity are not only necessary, they are possible. What we've
learned in Houston is that small churches, starting with little
or no money, little or no resources, little or no staff, and little
or no experience, can put together a formidable challenge to
the modern notion that poverty is a problem too big for any-
one but the government to handle. What we've learned in
Houston is that we can really make a difference in our world,
if we only take seriously. our high calling as believers in the
Lord Jesus Christ.

Laying Foundations

Anabaptism is the current darling of the American evan-
gelical church, “Experts” as far ranging as Martin Marty and
John H. Yoder and Harvey Cox have statistically docu-
mented the overwhelming “Baptistification of the church in
America.” Invariably, the reason given for this socio-theologi-
cal drift from reformation moorings is simply that Anabap-
tism has addressed the tough issues of our times. They have
answers. They have alternatives. They have models. No mat-
ter that those answers, alternatives, and models have been
hewn from the fantasies of humanistic humanitarianism. No
matter that those answers, alternatives, and models are the
result of theological and exegetical gymnastics. No matter
that those answers, alternatives, and models are propagated
by yellowed journalism, reddened ideology, and blackened
opinionation. No matter that those answers, alternatives, and
models are simply a cover for more statist intervention.

Anabaptism’s influence has been especially felt in the area
of charity, Discussion of wealth and poverty is its forte. Ana-
