CHURCH MUSIC IN CHAOS

James B. Jordan

“Moreover, Chenaniah, the leader of the men of Levi, was
charged with the striking up of song—he carried on the in-
struction in singing because of his skill” (1 Chronicles 15:22,
New Berkeley Version).

Rita: T went to the pub. They were all singin’—all of ‘em—
. oh, some song they'd learned from the jukebox. And I
thought, just what the frig am I tryin’ to do? Why don’t I just
pack it in, stay here, and join in the singin’?
Dr. Frank Bryant: And why didn’t you?
Rita. You think I can, don’t ya? You think because you pass a
pub doorway and hear ’em all singin’, you think we’re all OK,
that we're all survivin’ with the spirit intact! I did join in the
singin’, but when.I turned around, me mother had stopped
singin’, and she was cryin’. I said, “Why’re you cryin’,
mother?” And she said, “There must be better songs to sing
than this.”
From the film Educating Rita
Produced & Directed by Lewis Gilbert
Screenplay by Willy Russell

| fa a catholic presbyterian in theology and ecclesiology. I
am a neo-puritan in my views of social ethics and eschatol-
ogy. But as I look at the present state of music and liturgy in
conservative presbyterian churches, and at the history of these
things in presbyterianism and puritanism, I find a tremen-
dous contradiction.

The contradiction is this: Those who have the best
thealogy, and who are the most committed to the inerrancy
and authority of Scripture, generally have the worst music,
and the poorest psalmody. Another contradiction lies in the
fact that Reformed and presbyterian scholars have insisted on
the centrality of the psalter in worship, yet their churches

241
