THE RECLAMATION OF BIBLICAL CHARITY 313

graphic research is, thus, of prime importance.

It is clear that the reformed church in our day needs to
pioneer Biblical charity outreaches. We are to jump into the
struggle for genuine justice and mercy with both feet. But we
are to look before we leap.

That being settled, the program of Biblical charity can pro-
ceed to specifics and incidentals. The foundations being laid, the
framework erected, and the demographics established, it is now
time to turn to such mundania as food, shelter, and jobs.

Loaves and Fishes

Short-term emergency food relief is the obvious stepping
off point for the development of a functioning model of Bibli-
cal charity. Once all the theological and theoretical homework
has been done, it is time to begin a food pantry. Food is easily
managed and inventoried. Food is easily disbursed. And,
most of all, food is a necessity. In a hungry world, food is
priority number one.

The federal response to hunger has, for years, focused on
food giveaway policies. The Food Stamp program, the School
Lunch and Breakfast programs, the Special Supplemental
Food program for Women, Infants, and Children (WIC), the
Summer Food program, the Child Care Feeding program,
the Elderly Nutrition programs, the Meals-on-Whceels pro-
gram, and the FDIC Cheese and Butter Distribution pro-
grams were designed to cradicate the awful menace of hunger
in our land. But the Messianic State’s loaves-and-fishes men-
tality is bankrupting the entire system. And still a hungry
hoard of federal dependents cry out for more. Its apparent
that the federal food programs, as monolithic as they are, are
inadequate. The State has failed. People are hungry.

In response to the hunger crisis, evangelicals have called on
the government to add still morc food relief programs.*® They
have urged legislators to honor the so-called “right to food.”*!
They demand that radical wealth redistribution programs be
enacted,*? Or, for lack of any other tactic, they supplement the
federal giveaway debacle by imitating its extravagant ces-

  

40, Glenn Hinson, Roots af Hope (Decatur, GA: Seeds, 1979).

41. Arthur Simon, Bread far the World (Grand Rapids: Eerdman’s, 1975).

42. Ronald J. Sider, Rich Christians in an Age of Hunger (Downer's Grove,
IL: IVP, 1977).
