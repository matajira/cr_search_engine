CULTURE, CONTEXTUALIZATION,
AND THE KINGDOM OF GOD

Marion Luther McFarland

ONTEXTUALIZATION is a complex and bewildering

subject. To some it means the communication of the gos-
pel from one culture to another; to others it is a new term for
an expanded idea of indigenization. To still others it is the
coming together, in a dialectical process, of the Missio Dei and
history resulting in a liberated culture. To some it is the King-
dom of Christ being brought to bear on all facets of culture
and the vehicle of this confrontation is the Christian, laboring
to see his life-style totally centered on Christ. The result
would be that this covenant community, the church, would
have a radical and revolutionary effect on its culture (Acts
17:6). The church, therefore, is struggling to see culture “pos-
sessed” by Jesus Christ who is the King of kings and Lord of
lords.

The Communication Aspect of Contextualization

In the fullness of time Jesus Christ, the Son of God, was
born of a woman. This meant that He left His place in heaven
and came to the culture that existed in Palestine at that time.
He left one “culture” and entered another. He brought the
words and concepts of the “heavenly culture” and communicated
them into the thought-forms of another culture. He did this in
such a way that heavenly ideas could be understood by earthly
man. It is interesting to note that the primary means of ac-
complishing this “cross-cultural” communication was by Jesus
Christ’s taking a human body and dwelling with mankind.
Dr. Byand H. Kato, in a paper subrnitted to the World Con-
gress on Evangelization, said that “the incarnation itself is a
form of contextualization. The Son of God condescended to
pitch his tent among us to make it possible for us to be
redeemed (John 1:14), The unapproachable Yahweh whom no

326
