 

IHL. RECONSTRUCTING WORSHIP

 

CONVERSATIONS WITH NATHAN

David Chilton

HE following is a transcript, or at least a reasonably close

version, of a series of conversations I had with Nathan,
my seven-year-old, as we visited an evangelical church service
on a recent Sunday evening. Although the discussion actually
took place in several stages (ending late that evening at
home), for literary purposes I have reconstructed the conver-
sation as if it all happened during the service. I confess that a
good portion of it did go on then, as I tried to explain evangel-
ical worship to an impressionable youngster.

Nathan: Papa, this sure is a funny liturgy.

Papa: Well, it isn’t exactly a liturgy. They don’t believe in
liturgy at this church.

Nathan: How can you not believe in liturgy? Isn’t a liturgy
just what you do in Church?

Papa: Yes. But what I mean is that they don’t believe in hav-
ing the service written down in advance.

Nathan: Why not?

Papa: They think that if they read something that’s written
down, they won't really mean it.

Nathan: Bur all they have to do is think about what it means,
and agree with it, and then they'll mean it, won't they?

Papa: Sure. But they don’t believe that.

Nathan: But somebody around here must believe it, because
we all sang from the same hymmbook. Don’t they mean it
when they sing the hymns?

Papa: Sure they do. But they think prayers are different.
Nathan: You mean that they can agree with a song that they read,
but they don’t know how to agree with a. prayer that they read?

163
