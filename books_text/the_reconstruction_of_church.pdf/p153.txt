KINDS OF CHURGH DISCIPLINE 141

liberty of hearing the Word, may be permitted to persons excom-
municate, that is permitted unto heathen. And because we are
not without hope of his recovery, we are not to account him as an
aeny but to admonish him as a brother. (Chapter XIV, Article
6
The ground of his admittance into the Christian auditory is
his “civil rights” (this is stated in an unquoted section). Our
contention is that the excormmunicate has forfeited his spirit-
ual rights, including the right to hear the word within the con-
text of the congregational worship. The Cambridge Platform errs
when it says that “we are not to regard him as an enemy, but
to admonish him as a brother.” The phrase that they have
quoted is scriptural (2 Thessalonians 3:15), but misapplied.
The censured in 2 Thessalonians are not excommunicates in
the final sense of heathen, but fallen brethren who are to be
treated as brethren. The congregational word is open to them,
but the sarne word is closed to the excommunicate although
open to other heathen (1 Corinthians 14:23-24), Until the ex-
communicate repents, we must forbid him to participate in
the fellowship and worship of the church. Our responsibility
toward him must be understood in the light of Thomas
Taylor’s counsel:

This censure does not loose the bonds of all spiritual society;
but notwithstanding it, we may and must love the excommuni-
cate in the Lord; admonish and rebuke him; pray for him,
though not with him, and receive him upon his repentance, like a
brother as before.®

The Cambridge Platform avers:

While the offender remains excommunicate, the church is to re-
frain from all member-like communion with him in spiritual
things, and also from all familiar communion with him in civil
things, farther than the necessity of natural, or domestical, or
civil relations do require: and are therefore to forbear to eat and

y with him, that he man be ashamed. (Chapter XIV, Article
3.

The strongest argument for the exclusion of the excom-
municate from even the worship services lies in the nature of
the censure and the nature of the church. The church is not
primarily a missionary society, but the holy fellowship of
God’s people. To be excommunicated means more than

6. Ibid.
