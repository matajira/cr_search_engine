102 CHRISTIANITY AND CIVILIZATION

friends to call upon. One thing is for certain, in this situation,
a Church will find out the impotence of the mother denomina-
tional Church to respond to practical needs; for, every Church
in its denomination will be hurting. Who knows, maybe God
will use this kind of circumstance to force the Church back in
a geographical direction.

Ve Twofold Witness Concept

Notice that Paul starts by affirming a unified front with
Sosthenes. Sosthenes was a converted priest (Acts 18) who had
become an elder or leader in the Church. Why does Paul go
out of his way to align himself with the leadership of the local
Church?

First, Paul knew the dangers inherent in undermining
local leadership. For an evangelist, missionary, or any other
kind of itinerant minister to do this spells death to the local
Church. And if the battle is lost at the local level, contrary to
what some revivalists might say, the whole war is lost.
Revivalism has virtually destroyed the American Church.!"
Not only. has each major revival injected a form of deci-
sionalism into the Church, which has historically pulled
‘American religion into Arminianism,!? but revivalism has
taken religion outside local Church government. The result
has been a weakening, not a strengthening of the Church, and
an almost magical view of Church growth has been adopted.
As long as the Church wants magic to revive her, she will con-

11. Charles Hodge was extremely critical of the first Great Awakening.
Believing that it crippled the American Church, he condernned many of its
irregularities and endorsed the Old Side in the Old Side/New Side con-
troversy. See Charles Hodge, The Constitutional History of the Presbyterian
Church in the United Stas (Philadelphia: American Presbyterian Press,
[1851], 1983), Part 11, pp. 102A. Also, see an invaluable work which
evaluates the Great Awakening by Nathan Hatch, The Sacred Cause of Liberty
(New Haven and London: Yale University Press, 1977). His thesis is that
the Great Awakening had died by the time of the French and Indian Wars,
and a revival of Puritanism returned a sacred cause for liberty to America
and issued into the American Revolution.

12. Some Calvinists might object, reminding me of the fact that the altar
call was not used in the Great Awakening. This is true, but conversion was
pulled away from the sacraments, ipso facto, and when this happens,
human action is sacramentalized. See my essay in the Geneva Papers, No. 25,
“The Soteriology of Baptism (ID).”
