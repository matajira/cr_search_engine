THR REGLAMATION OF BIBLICAL CHARITY 307

give God glory. But there is no glory in dead orthodoxy. Sadly,
our churches have uncritically copped either historical or can-
temporary liturgical forms with no eye toward theological, or
cultural, or situational appropriateness. Thus, there is little
motivation and even less glory.

In order for Biblical charity to see resurgence in our day,
worship must reccive the same kind of careful scrutiny it re-
ceived in the 16th-century during the Reformation. Creativity
must combine with doctrinal and historical faithfulness. Vi-
sion and conviction must hammer out forms that will unite
the peapie of God in determined activity for the Kingdom.

A whole catalogue of hymns has been gathered over the
years that underscores the precepts of the Good Samaritan
faith. Why don’t we begin to sing such classics as Bringing In
the Sheaves, Where Cross the Crowded Ways, Neve Empty Handed,
Rise Up, O Men of God, Ta the Work, Make Me A Channel af Bless-
ing, and Forward Through the Ages once again? Why don’t we
loose the motivating and equipping power of worship against
the forces of privation?

The Book of Revelation makes clear that the activity of
God's people in worship actually, and ultimately, changes the
course of history (Revelation 4-5).3! To slough through wor-
ship means that we will have to slough through history. To
participate dynamically in worship means that we will be able
to participate clynamically in history, Worship, then, must be
marshalled to the task of defeating the scourge of poverty.

Third, the missionary implications of the sacraments,
especially the Lord’s Supper, must be recovered. James B.
Jordan reminds us that, “Historically, the church has particu-
larly remembered the poor in connection with the Lord’s Sup-
per. That's because this is God’s gift to the starving. It is not
the gift of philosophy or of theology, of ideas or inward feelings.
First and foremost, it is the gift of food! Thus, for instance: the
Christian Reformed churches traditionally have a special col-
lection for the poor right after the quarterly communion meal.
And the historic churches take up food and clothing for special
gifts at Christmas and Easter, that all may feast.”

 

  

31. David Chihon, Paradise Restored (Tyler, Texas: Reconstruction Press,
1985), Chapters 17, 19, 24.

32. Private Leuer, February, 1985. The various articles written by Jor-
dan over the past lew years in Geneva Ministries’ newsletters, The Geneva
