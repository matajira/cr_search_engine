REVIVALISM AND AMERICAN PROTESTANTISM 47

for more than one hundred years thereafter, the revival was
the most important phenomenon in the religious life of Arner-
ica. Countless individuals had “life-changing experiences” in
the revival setting. Churches and entire towns were trans-
formed. The major revivalists George Whitefield, Jonathan
Edwards, Charles Grandison Finney, Dwight L. Moody, and
others—were regarded as the most important men of their
eras by many conternporaries. American politics was likewise
deeply influenced by revivalism. The correlation of the Amer-
ican War of Independence, the rise of Jacksonian democracy,
and early abolitionism with eruptions of religious enthusiasm
is hardly coincidental. Moreover, social egalitarianism, mani-
fested in the feminist and civil rights movements, was sup-
ported in its early stages by revivalism. Sentimentalism and
anti-intellectualism, phenomena which still play a major role
in American cultural life, were likewise molded in the crucible
of the revival. In short, American culture, politics, and soci-
ety, as well as American religion, are to a remarkable degree
outworkings of revivalist presuppositions.”

The critical nature of this study should not obscure the
fact that revivals, especially in the eighteenth century, made a
very positive impact. The effects of revivals have been neither
uniformly positive nor uniformly negative. Probably the most
striking illustration of the paradoxical results of revivalism is
in the arca of education. Antipathy to “book learning” has
rightly been traced to early revivals. Yet, the First Great
Awakening directly or indirectly inspired the founding of six
colleges. Fundamentalism, which grew out of revivalism,
though often depicted as militantly anti-intellectual, has de-
veloped elaborate diagrams of the millennium that challenge
the most careful students, and fundamentalist apologetics

2. A central presupposition of this study is that the political, economic,
and social structures of a given nation are concrete manifestations of an
underlying religious faith. Philosophy follows trends of religious thought;
sovicty, politics, and economics flow from philosophy. Ideas, religious ideas
in particular, determine action.

3. Sce Richard Hofstadter, Anti-indellectualism in American Life (New York:
Alfred A. .Knopf, 1963), ch. 3-5.

4, William Warren Sweet, Revivalism in America (New York: Abingdon,
{1944} 1965), p. 147. The colleges were Penisylvania, Princeton, Columbia,
Rurgers, Brown, and Dartmouth. The Great Awakening also produced a
grassroots interest in education.
