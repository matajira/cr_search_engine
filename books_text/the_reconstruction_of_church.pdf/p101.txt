THE CHURCH IN AN AGE OF DEMOGRACY 89

First, the pagan mind places the constants of life in crea-
tion. The universe is a machine governed by laws. Objects fall
to the ground, for example, because of the “Law of Gravity.”
The Christian, however, believes that God created the earth
and all there is, and the constants of life are in and through His
Word. God's Word is the only thing that does not change. Us-
ing the “Law of Gravity” analogy, the Christian says that an ob-
ject falls to the ground because God’s Word ordained it so.

But the pagan traps himself with his view of continuity. The
universe becomes an impersonal machine that has no mercy.
Although a superficial explanation of the world is provided by
such a view, pagan rationalism shuts out the possibility for
change. And here we have the dilemma in full. How does man
create laws without inhibiting change? If the world is run by
unchangeable laws, then there is no possibility for change. He
finds the basis for change in the second presupposition.

Second, the only way out, given his presupposition of con-
stancy, is to inject chance. Chance is his savior, and spontaneity
his god. The pagan who reduced God to a watchmaker, wind-
ing up the universe at the beginning of time, now goes outside
of creation to free himself. He becomes irrational, a mystic,
and even willing to dabble in the occult to reach outside the
universe. Since we live in an age of irrationalism today, one
finds that many of the leading minds of Western Civilization
“receive inspiration” from a “higher consciousness.”* Having
locked themselves in by their own faulty view of “natural law,”
modern men run to the other end of the continuum for change
and freedom. But, chance is just as tyrannical as natural law.
Thus, the other side of the pagan error is this preoccupation
with spontaneity.

For the Christian, form and freedom, law and change, are
not in conflict, yet for the pagan one can see that these are an-
tagonistic to one another. What is the difference between the
Christian and non-Christian views? The Christian mind does
not put constants, apart from God’s providential and

2, R. J. Rushdoony, The Mythology of Science (Nutley, New Jersey: Craig
Press, 1967), pp. 1f. Stanley Jaki, Scieace and Creation (Edinburgh and Lon-
don: Scottish Academic Press, 1974), pp. 536ff. Dr. Jaki is an Hungarian-
born Benedictine priest with doctorates in both physics and theology. Pri-
marily an historian of science, he ties advancement in science to the influ-
ence of Christianity, Constance Cumbey, The Hidden Dangers of the Rainbow
(Shreveport: Huntington House Inc., 1983), pp. 217-220.
