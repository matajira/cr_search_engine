126 CHRISTIANITY AND CIVILIZATION

and now that these same courts have made it illegal to exclude
the children of illegal aliens from the public schools, there is a
rising tide of anti-immigration sentiment. The larger the
welfare State grows, the greater the concern over immigrants,

Old Testament Requirements

There were no immigration restrictions in ancient Israel,
despite the threat of alien religions in Israel. Ancient Israel
was not a pure democracy. It was not a welfare State. To exer-
cise political or judicial authority in Israel, a person had to be
in a covenant with the God of Israel. The law set forth religi-
ous requirements that restricted access to such judicial au-
thority by aliens: Edomites and Egyptians could not become
full members of the congregation until the third generation;
Moabites and Ammonites could not enter until the tenth gen-
eration (Deut. 23:3-8).

It'was assumed that there would be strangers in the land.
Again and again, the law of God warned the Israelites not to
mistreat widows, fatherless children, and strangers. The
Israelites had been strangers in Egypt and had been mis-
treated; they were not supposed to mistreat strangers in their
land.

Why would strangers come to Israel? For many reasons:
trade, better working conditions, greater judicial protection,
greater safety from marauders, and ail the positive benefits
promised by God to Israel in Deuteronomy 28:1-14. Why not
take advantage of better external conditions? So confident was
God in His own covenantal promises of blessing that His law
established guidelines for dealing with the strangers He knew
would come to Israel in search of a better life. His blessings
would not be limited to internal feelings that were available
only to covenanted Hebrews. They would be available to any-
one living in His covenanted land during those periods in
which His people remained faithful to Him and to His law.

In other words, Old Testament law established two forms
of membership for circumcised people: full membership (ex-
ecuting judgment) and Passover membership. Circumcised
people could come to the Passover (Ex. 12:48), even though
they were not entitled to full membership in the congregation
of the Lord, They would hear the law in the various teaching
services, including the seventh-year service in which the
