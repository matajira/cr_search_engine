68 CHRISTIANITY AND CIVILIZATION

“and they have proved a perfect failure.” Like Finney, he criti-
cized educated ministers for their low productivity. “What has
a learned ministry done for the world?” he asked.%

After 1850 the revival was “The cutting edge of American
Christianity... adopted and promoted in one form or
another by major segments of all denominations.”®! The
revivals of the 1840’s and 1850’s brought the trends of the
earlier revivals—an emphasis on ethics over doctrine, Armi-
nianism, and interdenominational fellowship —to a climax.®?
In some cases, the individualistic emphasis of revivalism did
lead to a pietistic faith, but the distinguishing feature of
American Protestantism was its social consciousness.
Motivated by a vigorous postmillennial eschatology and
perfectionism, American Christians worked for reform of
labor and the elimination of the liquor traffic, slum housing,
and racial conflict.93 “Liberal” revivalists sought to bring all
laws into harmony with Biblical Law, but defined the Law of
God in terms of human rights.°* Radical abolitionism, which
had been dormant during the 184(’s, revived after 1850 under
the coaperative leadership of evangelicals and Unitarians and
pushed the nation to the brink of war.®> It was in this charged
atmosphere that the social gospel was born.9¢

90. Quoted in Hofstadter, pp. 102-103.

91, Smith, p. 45.

92. Ibid., p. 80.

93, Heid., pp. 148-151.

94, William Hosmer explicitly identified the Law of God with human
rights: “The fact that a law is constitutional amounts to nothing, unless it is
also pure; it must harmonize with the law of God, or be set at naught by all
upright men. . . . When the fundamental law of the land is proved to be a
conspiracy against human rights, law ceases to be law, and becomes a wan-
ton outrage on society.” Quoted in ibid., p. 206.

95. Ibid., p, 204, It was in part the evangelical and Unitarian demand for
an immediate solution to the slavery question and their heightened agitation
that precluded the possibility of a peaceful settlement. For a discussion of
the Unitarian background of abolitionism, see Rushdoony, The Nature gf the
American System (Fairfax, VA: Thoburn, [1964] 1978), pp. 49H. and 91ff.

96. Smith, chapters 10 and 13. C. Gregg Singer notes that the theological
systems of Taylor, Bushnell, and Finney provided the iramediate back-
ground for the rise of the social gospel. Singer, p. 149. He locates the root of
the problem in the “latent Pelagian tendencies” of these theologians.
McLoughlin maintains, on the other hand, that it is futile to search for the
Toots of social Christianity in revivalism. McLoughlin, Modern Revivalism, p.
526,
