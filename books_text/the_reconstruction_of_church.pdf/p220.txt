208 CHRISTIANITY AND CIVILIZATION

Conclusion

In this essay, we have sought to do two things, The first
part was devoted to the inescapability of ritual and liturgy.
The second part has attempted to lay the foundation for a
Biblical theology of worship.

Why have we included the second part? I believe that we
must do more than be critical. The first part was fairly critical
in its tone. And, once one-realizes that form is inescapable, it
becomes a matter of determining the proper form. Hopefully,
this study, along with the others in this journal, will set the
reader on the right path to finding the proper forms of worship!
