CHURCH ARGHITECTURE 295

the church. It is dawn and light from the windows on the cast
face of the church and forty two portals that ring the base of
the main dome begins to fill the vast space of the large square
nave below. What looked like a mountain from the outside
looks like a heavenly canopy from the inside. In the soft morn-
ing light, the rose and purple marble of the interior comes ta
life. Polished to mirror finish, the light is reflected from the
lower quarters of the building back up into its ceiling high
above. The main central dome curves down into four triangu-
lar points that thrust all its weight into the piers of the nave’s
corners; and stationed aloft, amidst a sky of dark blue mosaic
with gold stars, is a six-winged seraph in each corner.

Splendid are the colors at the break of day, but vast is the
space it fills. In brick and marble slab the Christian architects
of Hagia Sophia spanned a greater space in a single vault than
any before them: The outside dimensions of the great Pantheon
of Rome neatly fit into the inside this church. After a millen-
nium had passed the highest vault that Gothic architects in the
West had achieved would still not touch the gold mosaic cross
at the center of the main dome. For a Christian architect this
was the challenge of the torn veil, the fundamental difference
between the sacred structure of the Old Covenant and the re-
quirements of the New Covenant. By his body and blood
Christ had torn the way open into the Holy of Holies. Until
that time all that was required was a small space for the high
priest to enter in before the presence of God. But now that
Christ had opened the way, the court, Israel's equivalent to
the nave, was open to the Holy Place.

The basic symbolic pattern for ali of this was correctly un-
derstood by the Byzantine church: the throne of God impressed
upon creation as the dome comes down to touch the four cor-
ners of the nave. In the holy of holies, if there was one special
place that the presence for God was said to be it was that empty
space between the wings of the cherubs on the mercy seat.
And likewise, under this great canopy of Hagia Sophia was
the mercy seat of the New Covenant. Set on the eastern end of
a large ambo or elevated platform, in matching marble of
course, was a Teal gold altar, for the real gold vessels of the holy
meal that a procession of officers would bring to the altar
every Lord’s day, to the accompaniment of the choir located to
the back sides of the nave probably in the north and south col-
onnade. And most important to even the Emperor at this time
