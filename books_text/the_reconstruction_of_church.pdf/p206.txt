194 CHRISTIANITY AND CIVILIZATION

dwellings. This represented a return to the source of their
redemption. In the New Testament Christ “tabernacled” (Jn.
1:14), and became the true tabernacle among men. The
church memorializes its salvation at the Eucharist. It retaber-
nacles in the true tent of the world.

Each year a pilgrimage was made to Jerusalem. A man
would take his tabernacle and throw it on the road saying
Hosanna to God in the Highest (Ps. 118), and also say, “save
me” (v. 25). Man indicated that his way to the altar of God
condemned him. He needed salvation. When Christ came
into Jerusalem in the spring fulfilling the Feast of Tabernacles
held in the fall, the people threw their tabernacles at his feet
(Jn. 12:13-15). They recognized that he was the way to the
altar of God. They wanted his salvation. Furthermore, the
Old Covenant tabernacles at the Feast of Booths are called the
body of the believer (I Cor. 6:19). The believer throws his
tabernacle/body at the feet of Christ's Tabernacle/body as a
dedication.

Finally, the feast involved drinking of the brook (Lev.
23:40). Jesus tells a woman at the feast of tabernacles that she
will never thirst again if she drinks his water (Jn. 7:37-38).
This particular feast represented the coming in of the nations.
At the feast 70 bulls were offered representing the nations of
the world (Gen. 10). This drink is symbolized at the Eucharist.

Thus far in our discussion of the Biblical theology of wor-
ship, we have dealt with the feasts and sacrifices. They set up
the basic structure for New Covenant worship. Let us con-
tinue our development of the Biblical theology of worship by
delving into some of the particular terms associated with wor-
ship. Our study cannot be exhaustive, but only introductory.
So, we must be very selective.

The Knee

The first Biblical term we come to is “knee.” As a verb, it is
to kneel. “Bending the knee” is connected with prayer in
general and special worship (Ezra 9:4 with 10:1; Acts 7:60).
It is curious that the knee, translating the Biblical words barak
and gonu, is selected to capture the essence of man’s worship.
Positively and negatively, worship is described in terms of the
use of the knee (I Kgs. 19:18). In fact, the knee is even a sym-
bol of man himself (Job 4:4).
