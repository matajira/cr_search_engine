THE CHURCH: AN OVERVIEW 7

see Exodus 4:10ff. and Acts 20:7-9; 1 Corinthians 2:1-5; 2 Cor-
inthians 10:10.) The proclamation of the gospel needs the pas-
toral context of the whole “body life” of the Church, and par-
ticularly needs the seal of the sacraments. By its exaltation of
preaching as a charismatic art, the Reformation moved in the
direction, subtly and unintentionally to be sure, of undermin-
ing the Church itself.

As time went along, this unhealthy opposition of preaching
to sacramental pastoral ministry became more pronounced.
The Puritan opposition to prayerbook worship wound up, in
practice, often pitting preaching against a more wholistic view
of the Church.® This opposition broke out into the open, in
America, during the Great Awakening. Roaming preachers
caused tremendous disruption in the normal pastoral life of
the Church. As Hofstadter has written, “In truth, the estab-
lished ministers found it difficult to cope with the challenge of
the awakeners. The regular ministers, living with their con-
gregations year in and year out under conditions devoid of
special religious excitement, were faced with the task of keep-
ing alive the spiritual awareness of their flocks under sober
everyday circumstances. Confronted by flaming evangelists of
Whitefield’s caliber, and even by such lesser tub-thumpers
and foot-stampers as Gilbert Tennent and Davenport, they
were at somewhat the same disadvantage as an aging house-
wife whose husband has taken up with a young hussy from the
front line of the chorus.”7

Because this is so important, and because there is so much
mythology about how wonderful the Great Awakening and
subsequent revivals were, I want to insert here some com-
ments on George Whitefield; but since I dare not criticize him
myself, I shall let the eminent Charles Hodge do it for me:*

“{t is impossible to open the journals of Whitefield without
being painfully struck on the one hand with the familiar confi-
dence with which he speaks of his own religious experience,

6, It should be noted that ail the Reformers had been favorable to prayer-
book worship. The mare radical Puritans departed completely from the Re-
formation at this point.

7. Hofstadter, Antt-intellectuatism, p. 67,

8. Hodge was one of the greatest 19th century presbyterian theologians.
He taught at Princeton, which seminary had actually developed in part out
of the Awakenings. Thus, he had some sympathy for revivals, but he was
first and foremost a catholic churchman,
