THE LITURGICAL NATURE OF MAN 199

writers of the New Testament freely used the word this way,
and that in itself does not distinguish its use. The New Testa-
ment writers, however, significantly altered the object to which
one’s service was offered.

Luke explains that the apostles were “ministering to the
Lord” (Acts 13:2). Instead of bowing to the state, they bowed
to the Lord. Immediately, we see ihe major discontinuity be-
tween pagan and Biblical use. In the Greek world, worship
was state centered. In the Scriptures, Luke told of a worship
that was God centered.

Whether the reference is to the state or the sacred, how-
ever, liturgy meant some kind of service. Perhaps an order
was involved, we are not told. We have no reason to doubt
that order was used in ministry to the Lord (I Cor. 14:33). The
emphasis of the word is service as opposed to a performance to
entertain God.

Keepers of the Cup

How exacily did the Apostles serve the Lord? Did the
Lord need something which they were to provide? Were there
specific ways to minister? These are a few of the questions that
begin to arise in our minds. Unfortunately, the immediate
text does not answer them. But a study of this word in the
Scriptures provides one of the most important insights into
the theology of worship,

In the LXCX of the Old Testament (I Kgs. 10:5), this Greek
word is translated “ministers” in a parallel format to cupbear-
ers. We know from its use in secular Greek, and the use of a
closely related synonym that this word can be used this way.
But here we have a clear reference to ministers being in the
category of cupbearers.

This is not surprising since the cup plays such a central
role in the tabernacle/temple. One, cleansing was often done
frorn a cup. When the people of Israel needed to be renewed
in their covenant, the blood of an animal was placed in a bowl
and poured on therm. Two, the laver of purification water was
also called a cup (cf. Is. 22:24 & Ex. 24:6). Three, the lamp-
stand in the tabernacle had cups made of almonds with capitals
or crowns on them (Ex. 25:31-35). Finally, the blood of pass-
over was carried in a cup (Ex. 12:22). Therefore, the ministry
of the priests was very much concerned with the cup. It was
