294 CHRISTIANITY AND CIVILIZATION

At the main entrance stand three large brass doors. Be-
yond these one enters the outer narthex, and again passes
through large brass doors to the inner narthex. Here one faces
even more massive bronze doors that open into the main
nave. The doors of the narthex have vase and floral patterns,
and the smaller doors of the vestibule have ornate floral pat-
terns in a spiral motif which are bordered with classic Greek
labyrinth designs, All the doors are oll set from one another
except for the very center portal, which gives direct access to
the nave without having to alter directions. It is interesting
that this main portal was considered the door of the Emperor,
and only the other doors forced one to turn and then turn
again to enter the nave. This is reminiscent of movement
through a stylized labyrinth. And if there is any doubt that the
early church understood labyrinth designs as symbols of a
passage through the gates of death, consider the placement of
the baptistry at the west end of the church, in conjunction
with its main entrance.

The baptistry and font are placed at the south west corner
of Sophia’s main entrance. It seems to have been a common
practice in the early church to place the font at the main gate
of the church. The baptistry is a domed square with an octa-
gonal interior, and again the eight-sided interior seems to be
another popular convention during this time. This was be-
cause the church saw the resurrection of Christ as occurring
on the day alter the sabbath, the eighth day, inaugurating a
new, uniallen week for humanity. They noted that circumci-
sion in the Old Covenant was performed on the eighth day.
They noticed that Pentecost came on the fiftieth day, which
was symbolically an eighth day ({7 x 7] + 1). Thus, it was ap-
propriate to use the number eight in connection with baptism,
for baptism is the official resurrection of the Christian, his cir-
cumcision, and his personal Pentecost.

The fonts were generally of the walk-through type in the
larger churches such as Hagia Sophia, because of a desire to
symbolize all of the passages through the waters of judgment
found in the scriptures. Consider the conversion of the Rus-
sian people under Prince Vladimir. When the patriarchs of a
large family clan walked through the waters of baptism, the
entire city followed suit, including infants in their mothers
arms, Such things were not uncommon in those days.

We can now make our way into the center of worship in
