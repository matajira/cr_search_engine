 

CONVERSATIONS WITH NATHAN 167
Papa: But Nathan, I'm telling you. There’s no liturgy. People
just say “Amen” whenever they feel like it.
Nathan: WHAT? Where does the Bible say to do that?
Papa: It doesn’t.
Nathan: Then why do they do it? Aren't they afraid?
Papa: Why should they be afraid?

Nathan: Because it’s a vow, a covenant promise. Doesn't it
mean that we agree with Ged, and that if we don’t keep this
promise we are asking God to destroy us? Isn't it even a
special covenant name for Jesus?

Papa: Sure. But they don’t know that. They think it means
something else.

Nathan: What do they think “Amen” means?
Papa: They think it means “T feel good.”
Nathan: Look at that!

Papa: What?

Nathan: There are people raising their hands!
Papa: So?

Nathan: In our church, the elders raise their hands to God
when they pray. But in this church, everybody else does it,
whenever they feel like it. And they make up their own liturgy
as they go along. You know what I think?

Papa: What?

Nathan: I think that in this church everybody is an elder—
except the elders.

Papa: That may be the best description P’ve heard yet.
Nathan: You know, Pa, those elders are tricking us.
Papa: How’s that?

Nathan: They really do have a liturgy for their prayers, They
keep saying the same thing over and over again.

Papa: Really?
