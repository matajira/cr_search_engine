CONVERSATIONS WITH NATHAN 169

Nathan: Not that bad. It just looks like they want people to
notice them instead of praying. Unless— Do you think maybe
he’s just kind of sick?

Papa: We'll talk about it later. It’s time for communion now.
Nathan: What's this?

Papa: Shhh! It’s bread.

Nathan: Come on, Pa. What is it really?

Papa: It’s bread, honest. It’s a little, tiny cube of bread.
Nathan: Looks like a piece of cracker to me.

Papa: Well, sure. It is a piece of cracker.

Nathan: Should we give them some money so they can afford
bread?

Papa: They can afford it. But they want to do it this way.

Nathan: Why would anybody want to eat this? Do they like
the taste?

Papa: Probably not.

Nathan: Then why would they eat something they don’t enjoy
~—especially at Communion? We're supposed to be happy
when we eat with God.

Papa: Be quiet. It’s time to drink the cup.
Nathan: OK. Yuck! What is this stuff?
Papa: Um, it’s... .

Nathan: Tastes like grape-flavored Kool-Aid.
Papa: Grape juice, probably.

Nathan: Doesn(’t taste very good. Did they forget to buy some
wine?

Papa: No. They don’t drink wine here.
Nathan: WHAT?!

Papa: SHHH!

Nathan: Why don’t they drink wine?
