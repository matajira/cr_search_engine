TWO-TIERED CHURGH MEMBERSHIP 123

that the United States passed a comprehensive, universal im-
migration restriction law. What had happened in the mean-
time? Basically, the root cause was the rise of the welfare
State. When the treasury was opened to those who could
bring out the largest vote, a major threat to taxpayers loomed.
The immigrants were poor. Once they became politically mo-
bilized by the great political machines of the cities —-increas-
ingly, these were Democratic Party machines in the twentieth
century— Republicans saw the threat. How could the “bot-
tomless pit” of economic demands be filled? It was one thing
when Republican “captains of industry” could use immigrant
labor to reduce costs of production or to break newly formed
unions—~-unions that resented the low bids of the immigrant
laborers — but quite another when these immigrants began to
vote. Thus, the trade union movement and conservative Re-
publicans saw that they could cooperate on the political issue
of immigration. So did other people in other democratic na-
tions. Those who had already “climbed to the top of the
mountain” decided that in order to reduce “competition at the
top,” they could “pull up the ladder” and keep the huddled
masses huddling elsewhere.

Thus, it was the advent of a doctrine of “salvation by poli-
tics” which sealed the fate of prospective immigrants. When
the ballot box became a way to gain wealth—an easier road
for many than thrift, hard work, and risk-taking —those who
were already favored by the system increased their efforts to
keep out competitors. Politics is different from voluntary co-
operation. This is an important point in Oscar Handlin’s
classic study of immigration to America, The Uprooted.

One organized activity raised problems of altogether another
order. Immigrants could associate in lodges and publish news-
papers to their hearts’ content. These were voluntary activities
and had no effect upon any but their members. But when groups
formed after the same fashion entered politics, the consequences
were entirely different.

The difference sprang from the unique qualities of political
action. The end of politics was the exercise of power through the
State —in which were embodied all the socially recognized instru-
ments of control and coercion. In this realm was no room for the
voluntary; control was indivisible. The Irish who built Carney
Hospital in Boston did not thereby limit the ability of the Jews to
found Beth Israel, or of the Yankees to support Massachusetts
Generat. But an election had only one outcome and, once the
