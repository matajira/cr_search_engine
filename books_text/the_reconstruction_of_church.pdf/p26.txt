14 CHRISTIANITY AND CIVILIZATION

the oversight of elders, who are called overseers (bishops) in
Scripture. If all the elders are sick one Sunday, they can ap-
point someone else to administer the Holy Communion, for
instance,

The government of the Church has real authority. It is not
merely administrative. The analogy used in Scripture and
throughout history is that the Church, as the Bride of Christ,
is the Mother of believers. The authority of the mother is not
as great as that of the father, but mothers have real authority
all the same. Moreover, the child who spits in the eye of his
mother will certainly have to answer to his father! Even if we
dads don’t always agree with the decisions and rulings made
by mom, we always back her up in front of the kids. That is
how God deals with Christians as well. Even if the leadership
in the Church makes a mistake, the Father will always back up
the Mother to the children, unless the Mother turns into a
Whore (as in the Book of Revelation). We'd better be pretty
sure, however, before accusing any particular body of elders
of having turned the mother into a whore. If that is what we
suspect, it is better to transfer in peace, and let God deal with
the situation in His way.

Submission to this government is not optional. The Bible
says, “Remember those who rule over you, who have spoken
the word of God to you. . . . Obey those who rule over you,
and be submissive, for they watch out for your souls, as those
who must give account. Let them do so with joy and not with
grief, for that would be unprofitable for you” (Hebrews 13:7,
17). In other words, if we want God to bless us, then for the
sake of our own Spiritual wellbeing, we need to have an aiti-
tude of cheerful and willing submission to the elders of the Church we
are members of. Persons who refuse to come under the gov-
ernment of some specific church must not be served the Lord’s
Supper.

The Church today is weak. Its officers are often weak.
Rash and rebellious men within the Church can often find
reasons to cause trouble, to revolt against the appointed
leadership. There is never any excuse for this, “for rebellion is
as the sin of witchcraft” (1 Samuel 15:23). If for some reason
we cannot in good conscience remain members of some par-
ticular church, we should leave in peace, and lawfully transfer
our membership to another church.
