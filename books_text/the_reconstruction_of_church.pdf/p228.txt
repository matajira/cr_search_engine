246 CHRISTIANITY AND CIVILIZATION

What did these hovels signify? It must be stated fram the be-
ginning thac there can be multiple symbolic meanings in any
given image in Scripture, and the dwellings of the Israelites in
Egypt convey several important meanings. In both Passover
and circumcision, aif aspects of redemption are brought. But
what about doorposts? To understand the meaning of the
doorposts, these homes can be viewed as symbolic wombs,
with the doorposts as symbolic legs. With the early morning
came Israel's delivery as a new nation. It was a birth, a new
birth, into a new life, The blood was put on the doorposts the
night before; the next morning, God brought forth a new na-
tion. The symbelism of marriage and birth is consistent.”

Egyptian homes did not benefit from the blood, Egyptian
households had no tokens of virginity to display. Egyptian
homes could not provide safety. There would be death, not
life, in every Egyptian household (Ex. 12:30). The bride-
groom brought the charge of faithlessness before the court, if
we cam stretch the symbolism to cover the idea of Egypt as a
false bride —the condition of all rebellious cultures. The sen-
tence of death was brought against Egypt's firstborn, as it was
against Adam, the firstborn earthly son of God. (Adam is
called “of God” by Luke in the genealogy of Jesus: 3:38. The
words “the son” were added by the King James Version’s
translators, but the usage is basically correct. The firstborn
sons of the patriarchs were types of Adam: Ishmael, Esau,
and Reuben. They were rebellious sons, not heirs of the
promise, not possessors of the birthright. The second Son, or
the second Adam, the true first Son, Jesus Christ, becomes
the lawful heir. Without a covenant with God’s true firstborn,
Jesus Christ, the earthly firstborn must perish.) The failure to
acknowledge one’s need for the tokens of virginity— which
God alone can provide, through His grace in the work of His
son, Jesus Christ—is an assertion of man’s autonomy. Man tries to
proclaim his own virginity, his own righteousness, as the law-
ful, honorable, ethically pure bride of Christ, the bridegroom.
But the required evidence is lacking, The Father has no proof
that his “daughter” has not betrayed the family’s honor. The
sentence of death is brought against the bloodless bride.

No such sentence could be brought against Israel on the
night of the Passover. Forty years later, no such charge could

7, We can also sce the homes as cities of refuge, or as arks.
