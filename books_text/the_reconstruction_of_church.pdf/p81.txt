REVIVALISM AND AMERICAN PROTESTANTISM 69

Throughout the nineteenth century a via media between ra-
tionalism and pietism was forged by the Princeton theologians.
Though often accused of rationalistic tendencies themselves,
these men recognized the validity and necessity of “religious
affection” and attempted to ground experiential religion on
the solid foundation of revelation. Archibald Alexander
(1772-1851), who had encountered revivalism early in his life,
recognized the good and bad aspects of the revival. Without a
foundation of Christian truth, one’s experience could not be
Christian. “Every thought, motive, impulse and emotion”
must “be brought to this touchstone,” the Word of God.97
Alexander criticized historic Presbyterianism for its tendency
to divorce thealogy and experience and held that the subjec-
tive element of religion was not inherently inferior to the ob-
jective. Indeed, at times piety can correct wrong theology.%8
Later Princetonians maintained this same balance. Though
Charles Hodge (1797-1878) tended toward rationalism in his
Systematic Theology, he insisted that feelings and creed were
complementary.°? Hodge judged the validity of revivals by the
doctrines preached, the nature of the conversion experience,
and the change in the converts’ lifestyle,1°° Benjamin Warfield
(1851-1921), the last of the great Princeton theologians,
recognized that rationalism and mysticism were of the same
genus but, judging from his definition of religion as “depen-
dence on God,” apparently saw mysticism as the lesser evil 1°!
Though the emphases of these three theologians differed as a
result of different historical circumstances, they produced a
remarkably consistent and well-balanced alternative to the ex-
isting condition of American Christianity.!0? Unfortunately,
the voices of the Princetonians tended to be as voices crying in
the wilderness. After the Civil War, most American Protestants
were listening instead to the voices of revivalist Dwight L.
Moody and his musical director, Ira Sankey.

97. Quoted in W. Andrew Hoffecker, Piety and the Princeton Theologians:
Archibald Alexander, Charles Hodge, and Benjamin Warfield (Phillipsburg, NJ:
Presbyterian & Reformed, 1981), p. 24. Hoffecker demonstrates also that
personal piety was a major concer of the Princeton theologians.

98, Ibid., pp. 25, 39.

99. Ibid, p. 80.

100. Ibid, p. 72.
101. Idid., pp. 126, 111.
102. Ibid, p. 155.
