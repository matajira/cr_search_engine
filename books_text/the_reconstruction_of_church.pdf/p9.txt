INTRODUCTION XL

First was the pedagogy of court-enforced boundary. God
threatened to kill anyone who got too far out of line. He estab-
lished authorities in Church and in state with real power to

_ enjorce this. Fear is a very real factor in Christianizing a peo-
ple, for fear shapes the minds and attitudes of people. This is
obvious in child rearing. It is external confrontations that pro-
mote the development of inner restraint. Children whose par-
ents avoid confrontation and use the “nice” approach, coaxing
them into good behavior, never learn to handle frustration
and do not develop inner restraint. Thus, in terms of a Bibli-
cal view of how to transform society, the threat of the sword
(and, first, of Church discipline) is important. Reconstruction
does not come about merely through the communication of
data to the mind. That is why to each sphere of society God
commits a real boundary-enforcing power: to the parents the
rod, to the state the sword, and to the Church the power of ex-
communication?

Second, God set up a pedagogy of liturgy. The perfor-
mance of ritual actions by our whole persons restructures our
lives. Such ritual creates a context for understanding truth
when we hear it. A minimal liturgy, a mere bare bones, pro-
vides a minimal context for understanding; and an erroneous
liturgy sidetracks understanding. Because, however, the Bib-
lical view of man is wholistic, and not merely intellectualistic,
the performance of ritual acts to God’s glory, even without
fully understanding them, is extremely important. A study of
the ritual laws in Leviticus, for example, will readily demon-
strate that God almost never in context explained the mean-
ing of these rites. The performance of such ritual generates a
psychological context for receiving teaching, just as proper
and sound teaching is the proper context of any meaningful
ritual. The lack of ritual and of whole-personed worship is one
of the greatest roadblocks to understanding present in the

3. William Kirk Kilpatrick makes this point with an illustration when he
writes, “I think there can be little doubt that civil rights legislation in this
country has had the same |pedagogic] effect. Laws granting equality of ac-
cess to blacks in the South may have ben hated and grudgingly obeyed at
first. Nevertheless, in obeying the law over a long period of time certain
habits are induced which eventually alter attitudes and even bring about 4
change of heart. The law has an educative function as well as a judicial
one... .” The Kmperor's New Cloihes: The Naked Truth about the New Psychology
(Westchester, IL: Crossway Books, 1985), pp. 105f.
