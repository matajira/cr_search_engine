164 CHRISTIANITY AND CIVILIZATION

Papa: Something like that.

Nathan: Then why don’t they just memorize the prayers?
Papa: Because they think they wouldn’t mean those, either.
Nathan: Can they memorize songs and mean them?

Papa: Sure. But they think music is different. You can read or
memorize a song and still.mean it. But if you read or
memorize words without music, you won’t mean them.

Nathan: But don’t they teach their children “politeness
liturgies”? Like “please” and “thank you” and “you're
welcome,” and “yes, sir,” and “yes, ma’am”? And don't they
teach them to mean it?

Papa: Yes, but--

Nathan: And what about Bible verses? Do they memorize
Bible verses?

Papa: Of course they do.

Nathan: But they don’t mean them?
Papa: Yes, they do,

Nathan: Without music?

Papa: Sure.

Nathan: How?

Papa: Can we change the subject?

Nathan: OK. Why didn’t we confess our sins when we began
the service?

Papa: This church doesn’t believe in it.
Nathan: WHAT?!

Papa; Shhh. Keep your voice down. I mean they don’t think
the Church needs to do it.

Nathan: Don’t we need to be forgiven?
Papa: Sure. They just don’t think it should happen in Church.
Nathan: What about the Greed? Why didn’t we say the Creed?

Papa: Well, partly because it’s liturgical. They think they
won't mean it if they say it.

Nathan; We could sing it.

 
