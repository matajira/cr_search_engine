202 GHRISTIANITY AND CIVILIZATION

As God’s bow/ of wrath, history is moved according to it. Asin
the personal application of the cup, the destiny of nations is
measured by their relationship to the cup.

The reason is that the cup in the New Testament is Jesus
Christ. God’s estate and inheritance is His Son. If we receive
it, then blessing is poured out. If we or the nations reject
God's estate, we are judged.

Pagan man wants to reduce the cup from God’s estate to
something magical. In the Arthurian legends, whoever ma-
nipulates the Holy Grail has the power to rejuvenate the land,
Certainly these ideas are based on Christian realities. But
they are perversions.

There were two ways of searching for the grail. One was
through black arts and sex. The other was popularized in
the Arthurian legend where the grail was acquired through
purity and morality. In any case, the one who had the grail
could raise armies and ride to victory.

Hitler even searched for the grail. He chose the first
method, the black arts, Most historians ignore Hitler's nvolve-
ment in magic and the black arts. But Hitler believed that he
was in contact with an ancient magician Klingsor. This partic-
ular magician had been castrated in the early middle ages
because of an adulterous affair with a King’s wife. Hence, he
turned to black magic and sought to acquire the grail to defeat
the king.!* Hitler wanted to contact Klingsor for essentially the
same reason. Only, he wanted to conquer the world.

Even these perversions of the cup, indicate its relationship
to history. The correct view, however, is that liturgy is the in-
heritance of the Lord. When man serves the Lord in special
and general worship, he manages this inheritance. History is
effected.

15. In Biblical religion, sex is not a sacrament. God makes food and
water the sacraments to make man realize his dependent nature.

16. Trevor Ravenscroft, The Spear uf Destiny (New York: Bantam, 1974),
pp. 182-183, 185, Ravenscroft must be read with a very discerning eye. His
method of historicism is “higher consciousness.” Nevertheless, Hitler, it is
now known, was definitely very involved in the accult. Winston Churchill’s
adviser on Hitler, Dr. Walter Stein, had informed him of the fact. But
Churchill had the information suppressed for a number of reasons. A much
more credible work to the historical eye, and one that confirms
Ravenscroft’s thesis, is by George Mosse, The Crisis of German Ideology (New
York: Grosset & Dunlap, 1964).
