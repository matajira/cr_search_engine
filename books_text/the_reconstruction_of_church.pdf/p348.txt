336 CHRISTIANITY AND CIVILIZATION

ess. One js that culture is accepted as God-given but in the
need of transformation, rejection, or utilization by the Chris-
tian community. Second, this transformation comes only as
the Word of God is freed from any cultural overtones that are
unconsciously part of the thinking of all Christians. “Perhaps
the most fundamental ‘message’ of the ‘contextualization’ dis-
cussion for the church in the West is the need to recognize that
every expression of the gospel is culturally formed and to that
degree, subject to some distortion and in need of correction
from other cultural perspectives.”'9

The end result of contextualization, however, is not mere
abstraction and speculation. It is praxis. It is the Christian at
work seeking to transform the culture (context) into a Garden
of Eden. It will not be completed until Christ returns, but it
must be the labor of every Christian in every part of the
world. As the Lord expects His people to strive for perfection
and holiness, He also expects His covenant community to
strive to bring the Kingdom of God on earth. Neither of these
will be fully complete until Christ returns. In the meantime
each Christian is to be about the business of the King. “The
covenant authority of the Word of the great King lays its com-
prehensive claims upon the total life of the people of God. It
will not let us merely profess our allegiance to covenant. It
will curse us when we do not walk by covenant in the cultures
of the world. The Bible’s own understanding of its hermeneut-
ical role in the process of contextualization forbids us the
bondage of abstractionism and any culturally privileged
status quo. It calls us to the task of the renovation of creation
in the name of the last Adam.”#0

It is really Christ who has accomplished the task of con-
textualization as He came from the culture of heaven “con-
quering and to conquer.” For the disciple in every part of the
world: he must labor on until Christ comes in the fullness of
the Kingdom of God. For the Christian church: she must
make disciples of all nations by teaching them the whole coun-
sel of God. She must then teach the new disciples that they are
builders of a new culture by a kingdom life-style, remember-
ing that the process never ends. Imaginative innovation has
its place if a genuinely Christian life-style is to emerge.

19. Tbid., p. 24
20. Armerding, p. 113.
