4 CHRISTIANITY AND CIVILIZATION

couraged man to eat of the tree of life.

‘The tree of life was a sign or token that man dees not have
life in himself. All life comes to man from outside. It was a
sign that man is not a self-sufficient god, but a creature, de-
pendent on God. This is the meaning of food in the Bible, and
this is why when we read the Bible we find time after time that
people are hungry or thirsty, and that Ged provides food and
drink for them.

The sabbath day was the day of rest and worship. Adam
and Eve were not going to work on that day. Instead, God was
going to come and meet with them (Genesis 3:8), and they
were going to have special sacramental fellowship at the tree
of life. Of course, Adam and Eve spurned God’s offer of life,
and rebelled, eating of the forbidden tree. Thus, they were
cast out, and barred from the sacramental fruit by the flaming
sword of the cherubim (Genesis 3:22-24).

What if they had not sinned? Adam and Eve would have had
special fellowship with God on their first day, and then they
would have gone to work the next day, following the four rivers
out of Eden to the four corners of the earth, extending the princi-
ples of the central sanctuary (Genesis 1:28; 2:10-14). Periodically,
they would return to the garden for special fellowship with God.3

Life flows fram the center. The river arose in Eden, and
from there went out to water the whole earth (Genesis 2:10).
Adam and Eve were to eat of the special tree on the first day,
and then eat of general trees on the other days. They were to
have special sacramental fellowship with God at the center,
and general fellowship with Him downstream from Eden.
From this we sce that the special aspect of life has a certain
primacy over the general aspect.

What ts the Church?

When we read the Bible, we find sometimes that the word
“Church” is used to refer to all the people of God in all that

3. Itis my guess that they would have returned weekly. As the years went
hy, ‘and men moved out from the Garden of Eden, they could have carried
with them seeds from the tree of life, and set up new garden-sanctuaries in
other places. In my opinion, the separation of annual sacramental worship
from weekly sabbath-synagogue worship, which characterized the period
from the fall of man to resurrection of Christ, was a result of the exclusion of
man from Eden.
