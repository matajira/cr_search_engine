THE LITURGICAL NATURE OF MAN 185

Under form, we can put subcategories of law, nature, pre-
destination, mind, and group. Under freedom the following
concepts are perceived as opposites: Freedom, grace, freewill,
emotion, and the individual. Some might want to pull form
and freedom close together as the dotted lines indicate. But
dualistic thought can never move them together. Thus, form
and freedom, no matter how close, are still antagonistic to one
another.

Thus, modern man perceives the world in terms of a dia-
lectic, The world is in conflict with itself to the point that all it
can do is synthesize these concepts, or live in some kind of at-
tempted “balance.” Regardless of how he attempts to resolve
form and freedom, however, the one always ends up against
the other. Thus, modern man makes a preference of one over
the other. He would rather live in the realin of freedom because
his presupposition is that form destroys freedom.

Resolution in Christ

Christ resolves the tension. In a sense, sinful man does
live in a conflict between form and freedom. Set forms stifle
his creativity, he thinks, and so he rebels.

Christ settles the matter. One need only read the first
chapter of Ephesians. The phrase “in Him” occurs several
times. It is remarkable to see all the great philosophical and
practical problems resolved in all of these “in Hims.” For ex-
ample, His redemption of the world pulls the plan of God,
predestination, and the work of the Spirit into perfect har-
mony. Form does not destroy creativity and freedom. Rather,
predestination establishes it in Christ.

For that matter, “The Spirit is always to be understood in
relation to Jesus Christ, and as such it is equally the source of
reason and form, . . . But freedom of the Spirit is itself'a free-
dom bound by Jesus Christ as its norm. It is finally a Christo-
logical freedom. Thus even as we rightly claim place for free~
dom in worship, we still find ourselves theologically bound by
the Word. And we only demonstrate in our thought abour
worship what we find to be true in the experience of worship
itself: form and freedom are defined as correlative by the
Word. And the category of Janguage’ deriving from the bibli-
cal meaning of ‘Word’ comprehends both as the sine qua non
without which neither thinking about worship nor worship
