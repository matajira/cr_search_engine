276 CHRISTIANITY AND CIVILIZATION

feasting were activities that required the participation of the
whole person and symbolized his dependence on divine bless-
ing. Let us not deceive ourselves into thinking that biblical
worship is not, as it has always been, a liturgical dance that
requires a sacramental meal. Any order in worship to the ac-
companiment of music is a processional dance no matter how
slow and subtle it might appear to the eye. The greater the
pomp and ceremony, the more obvious this becomes. The
fundamental connections between dance, feast, and worship
seem essential, however, only to those Christians who have
through the years maintained a symbolic understanding of the
cosmos as God's good and perfect gift to man—a world that in
every detail signifies the presence of God and the dependence
of man. We can dance before God because we have life, and
we have life because God feeds us in every respect.

In an analysis of the sacramental nature of the cosmos,
Alexander Schmemann, a priest of the Russian Orthodox
Church, points us in the right direction when he writes: “All
that exists is God’s gift to man, and it all exists to make God
known to man, to make man’s life communion with God. It is
divine love made food for man, made life for man. God
blesses everything He creates, and, in biblical language, this
means that He makes all creation the sign and means of his
presence and wisdom, love and revelation: ‘O taste and sec
that the Lord is good.’ ”? Earth is “Manhome” not just because
it is 93 million miles from a G type star, and thus the optimum
place in the solar systern to support “carbon based life farms.”8
It is man’s home because it is here that the divinely created or-
der of things specifically bears witness to the true meaning of
man’s life. Earth is in the final analysis our special place be-

7. Alexander Schmemann, For the Life of the World (New York: St. Viadi-
mir’s Seminary Press, 1973), p. 14.

8. L have chosen terms used in modern science fiction because no other
art form expresses so clearly the platonic tendency among modern human-
ists to want to escape the confines of space and time; particularly as they are
manifest in the human body, Man’s cvolution to a higher life form is always
presented as some kind of energy being without a tangible body; as in the
films “2001; A Space Odyssey” and its sequel “2010,” the popular “Star Wars”
saga, and most blatantly in the first film in the “Star Trek” serics, Remember
Yoda's instruction to Luke concerning food and flesh? He said: “. . . lumin-
ous beings have no need of such stuff." In opposition to this trend are the
marvelously imaginative works of Paul Linebarger; better known by his pen
name, Cordwainer Smith. Linebarger invented the word “Manhome” as a
title for old Earth. In his stories, it is man's home because it is from there
that true religion is exported throughout the universe.
