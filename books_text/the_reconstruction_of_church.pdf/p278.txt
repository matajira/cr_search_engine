CHURCH ARCHITECTURE AND THE
ENVIRONMENT OF WORSHIP: REVALORIZATION
IN THE PROTESTANT GHETTO

James Michacl Peters

A professional art historian once told me that if a per-
son really wants to understand a particular style of human
invention he must first look at its most exaggerated expressions,
and ask over and over again, “What do I see? What do I see?”
Being a man of immense visual prowess he assured me that it
was all a matter of practice, a habit developed from years of fol-
lowing that simple ritual of selecting the extreme case and then
asking the proper leading question, methodically working one’s
way back to the origin of the particular style in question. Of
course such a method is a formalist technique which often
merely postpones, sometimes indefinitely, the iconographer’s
responsibility of answering that bigger question: “What does it
mean?” In any case I have through the years found this tech-
nique to be a valuable tool which forces one to look more
carefully at thmgs which are all too often taken for granted.

Considering this, and since the theme of this symposium is
the reconstruction of the church, it seems only proper that our
visual exploration of the church building should begin with a
hard look at the Studio Style of the protestant ghetto. As with
most architectural forms the elemental principle of studio
style can be expressed mathematically. You take the number
of dynamic evangelists, multiply it by the square floor space,
and divide by the number of electrical outlets. That result is
then multiplied by the size of the viewing audience and divided
by the diameter of the satellite dish. The final result is the de-
gree of studio style that an existing church structure has; or in
the case of a new building program the result determines the
degree to which the final structure must conform itself to the
primary function, which is certainly not worship. The pri-
mary function of any studio style is theater, the facilitation of
entertainment.

266
