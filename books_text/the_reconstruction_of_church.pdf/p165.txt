 

KINDS OF CHURCH DISCIPLINE 153

imagined that it is enough to pray for the Holy Spirit to convict
the heart of the offender. “He will be restored to fellowship in
this manner,” it is said. Of course this kind of argumentation
sounds very pious but once it is unmasked it will prove to be
sheer hypocrisy and piosity. For beginners, how can obe-
dience to the command of Christ be tyrannical and unloving?
To make such a claim is tantamount to charging Christ with
folly for making such ecclesiastical pronouncements as Mat-
thew 18.

Secondly, for one to adopt this attitude is really a sin. To
pray and ask the Holy Spirit to reclaim a brother without im-
posing church discipline (the divine means) is categorically
sinful. It is akin to a Christian who prays that God would con-
vert the heathen when he himself lingers in evangelistic
atrophy, or like someone who prays, “Give us this day our daily
bread” —expecting a roasted chicken to come flying into his
mouth while all the time shunning lawful employment. To
pray for the outcome without exercising the means is sin!

Will the offender be offended? Of course he will. The
choices are either offend God by not offending the offender or
offend the offender and thereby please God. In addition to this
the words of Solomon are appropriate: “Faithful are the
wounds of a friend; but the kisses of an enemy are deceitful”
(Proverbs 27:6).

The “Cant Judge” Substitute

Matthew 7:1 states “Judge not that ye be not judged” and
is thus often used as justification for the practice of “not judg-
ing,” It is fervently maintained that because we too are sinners
and cannot be judges of men’s hearts that there is very little place
left for any kind of ecclesiastical discipline within the body of
Christ. Such argumentation is freighted with innumerable
difficulties. Logically, those who say that we “cannot judge”
regarding those who possibly should be excluded from the
church should also affirm that we are incapable of exercising
the keys in admitting men into the pale of the church. The Body
of Christ would thus be a body without any members!

As for Matthew 7, the context of this passage shows us that
Matthew writes about the censure against the hypocrite.
Christ is here indicting the self-righteousness of the religious
braggadocio such as the scribe or the Pharisee. He is not con-
