THE CHURCH: AN OVERVIEW ii

commands us to submit to some particular hody of elders in a
local Church, so that we are accountable to some body of
shepherds over us. God instructs us that special fellowship
with Him at His Table is a primary source of life and blessing
for us. Yet, many Christian people are not members of any
particular Church. They attend Church, but are accountable
to nobody. Indeed, many churches maintain no roll, and ex-
ercise no government. Reformation is sorely needed.

All the same, these problems are no excuse for not becom-
ing actively involved in a local Church. The Church today is
weak, but God commands each of us to join with her and
strengthen her as best we can, for it is in the Church as
nowhere else that we will find real power for living.

A brief note on parachurch organizations: The Church
has always had specialized “parachurch” ministries. When the
Church was united, these ministries (generally monastic)
cooperated with the local churches, but were organized
differently. A study of the Levites in the Old Testament will
show that many served as pastors of local churches, but many
also served in “parachurch” orders as lawyers, musicians, and
the like. The problem for protestants is that we do not have a
unified Church. Parachurch ministries, thus, are not accoun-
table to the sacramental Church at any level. This creates ten-
sion, but that tension is no different from the tension created
by the multitude of denominations of sacramental churches.

What are the Powers of the Church?

As the “people of God,” the power of the Church is its
ethical influence as it transforms a fallen world into the
Kingdom of God in all areas of life. As an institution, the
Church historically is said to have two powers, called the
“power of order” and the “power of jurisdiction.” Let us look
first of all at the power of order.

The power of order is the power to shepherd. It is the
Church’s kingly power It includes such things as com-
municating the gospel, visiting the sick and those imprisoned
for the faith, comforting the afflicted, encouraging the saints,
reproving the wayward, and the like. These kinds of things
are done by all Christians, all “general officers” of the Church,
the royal priesthood. Special weight is attached to these
duties, however, when they are performed by special officers,
