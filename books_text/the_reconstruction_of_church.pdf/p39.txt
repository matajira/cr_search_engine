CHURGH RENEWAL; THE REAL STORY 27

stuck with the church all this time. They are ready to go; they
are ready for a dynamic program of outreach. Have you got
the vision, that kind of a program?

Providentially, the neophyte falls for this aggressive ploy.
He begins to see himself in the role of the seller, rather than
the buyer. He is the one to be proven, the one who must meet
the qualifications. And being mostly messianic in his outlook,
he fails to take a hard look at the history and status of this con-
gregation so apparently eager to propagate the message of
Jesus Christ. It must have been the former pastors, he
reasons. And he gets plenty of help here. Members are only
too glad to catalogue the weaknesses and failings of virtually
every man that has taken their church. The candidate sup-
presses the suspicion that his ministry will soon take its place
on the same chopping block.

So he tries to measure up. He details his visions for an ex-
panded ministry. He talks about home Bible studies and
evangelistic visitation—all the things the committee wants to
hear. He wants the job.

What does the committee want?

The Mechanized Ministry

Perhaps the miost devastating blow to the Protestant
church in modern America has been the gradual Romanizing
of ministerial functions. Rome teaches the doctrine of “ex
opere operato” — the sacraments operate by themselves, It is a
sort of magic. Like an antibiotic for sin.

Applied to the Protestant minister, it means that he
becomes an impersonal functionary, placed in the ministry to
marry and bury and hold hands generally. He is a lot like the
county clerk.

Ask any pastor that-has been in the ministry for a few
years, and he will tell you an amazing story. He will tell you of
the many phone calls he has received from people he has
never met, people who think his job is te marry people who
call in off the street. For a fee, of course. He regularly gets re-
quests from assorted Romans, divorcees, and fornicators
looking for a quick, convenient service in a nice little church.
Some are horrified and offended to find. that he doesn’t con-
sider himself a Justice-of-the-Peace, but a minister of the
Gospel. These folks just cannot understand the difference.
