THE CHURCH IN AN AGE OF DEMOGRACY 105

destroying the covenant by taking the curse upon Himself
which established a better covenant. So, every time the cove-
nant is ratified symbolic tearing occurs, and when the cove-
nant is broken zeaé tearing results.

Thus, Paul uses the word schize to point to the fact that the
covenant is being torn. It is almost a play on words because,
the Corinthians, on the one hand, will not face the proper
tearing by God. Yet, on the other hand, the curse was being
brought by the Corinthians themselves through divisions. Re-
jecting the tearing that leads to life, the Corinthians were lit-
erally being torn to death (I Cor, 11:30).

Paul’s warning, therefore, places their behavior in a very
serious light. He was saying that an unlawful tearing was oc-
curring, and if they did not check their behavior, apostasy
would result. In other words, the tearing that began with
schisms in the Church would grow into a complete rending of
the covenant. As the preferred reading of I Corinthians 1:13
says, “Christ has been divided!” (without a question mark).!*

This is precisely what happens in the modern Church.
Often, people become disenchanted with a local Church, begin
to cause dissension, and eventually leave. A better destination,
one without the problems of the former Church, is perceived.
In reality, schism has been consummated, and apostasy
results.1° Nevertheless, why was this tearing-curse-of-the-
covenant process taking place in Corinth? In Paul’s continuing
argument, he explains several contributing factors.

@

The Greek Model of Ministry. Prom Paul’s criticism of “I am
of Paul, I am of Cephas,” it is apparent that the Church at
Corinth had become ego-centered. Corinth was following the
Greek Aeroic-model of leadership.1* When a man demonstrated

14. See marginal reference in the New American Standard Version of the text.

15. This is not the case where liberalism forces orthodoxy out. Never-
theless, it is much better if the conservatives force the liberals out. Examples
of this are the Eureka Classis of the Reformed Church of the United States
and the Lutheran Church Missouri Synod.

16. This is not to be confused with a Christian view of heroes. Biblical
heroes are men who prove themselves by continued faithfulness, and come
to be recognized as God’s leaders. Such men are found in the “Hall of Faith”
in Hebrews 11.
