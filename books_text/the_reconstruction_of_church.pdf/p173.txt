 

KINDS OF CHURCH DISCIPLINE 161

counsel is different from mere moral suasion. The church is
obligated by Christ to enforce its judicial counsel through the
exercise of the keys; and she has the right and duty to loose
penitent sinners and bind the impenitent. But when counsel-
ing becomes a substitute for church discipline the candlestick
itself may be in danger of removal. When the church reproves the
happening, but still allaws the happening to happen, the impeachment
of the chutch through the withdrawal of the presence of Christ will un-
happily happen (Revelation 2:20).

Conclusion

It is possible that the counseling substitute may be the
most potentially dangerous substitute in Reformation
churches today. The elders may be competent to counsel, but
they are incompetent to pastor the flock as long as the key of
discipline remains only a doctrine of passive lip-service. We
believe it an incontestable truth that there has been a one-
sided emphasis on the reformation of the sinner to the exclu-
sion of responsible church discipline.

Yet, our call in this essay has not been a call for blood; but
rather a call for church holiness. In dealing with erring
brethren, the Cambridge Platform provides us with a double-
edged warning:

In dealing with an offender, great care is to be taken, that we
be neither overstrict or rigorous, nor too indulgent or remiss; our
proceeding herein ought to be with a spirit of meckness, con-
sidering ourselves, lest we also be tempted; and that the best of us
have need of much forgiveness from the Lord. Yet the winning
and healing of the offender’s soul, being the end of these
endeavors, we must not daub with untempered mortar, nor heal
the wounds of our brethren slightly. On some have compassion,
others save with fear. (Chapter XIV, Article 4)

Church discipline has been described as a “sacred
scarecrow.” It certainly has not been our intention to make it
more of a scarecrow than it already is. But the real issue that
needs to be underscored ts the glory of Christ’s name and the holt-
ness of the church. On this matter of church holiness, Louis
Berkhof leaves us with some timely words:

There is a very evident tendency to stress the fact that the
Church is a great missionary agency, and to forget it is first of all
the assembly of the saints, in which those who publicly live in sin
