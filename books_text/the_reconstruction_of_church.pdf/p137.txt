TWO-TIERED CHURCH MEMBERSHIP 125

self-government, of liberty under law, we have a heterogeneous
population no small proportion of which is sprung from races that,
throughout the centuries, have known no liberty at all, and no law
save the decrees of overlords and princes. In other words, our ca-
pacity to maintain our cherished institutions stands diluted by a
stream of alien blood, with all its inherited misconceptions respect-
ing the relationships of the governing power to the governed.

It is out of an appreciation of this fundamental fact, vague at
first, but later grown firm and substantial, that the American
people have come to sanction —indeed to demand— reform of our
immigration laws. They have seen, patent and plain, the en-
croachments of the foreign-born flood upon their own lives. They
have come to realize that such a flood, affecting as it does every
individual of whatever race or origin, can not fail likewise to
affect the institutions which have made and preserved American
liberties. It is no wonder, therefore, that the myth of the melting
pot has been discredited. It is no wonder that Americans every-
where are insisting that their land no longer shall offer free and
unrestricted asylum to the rest of the world.

The United States is our land. If it was not the land of our
fathers, at least it may be, and it should be, the land of our chil-
dren. We intend to maintain it so. The day of unalloyed welcome
to all peoples, the day of indiscriminate acceptance of all races,
has definitely ended.

You can see the shift in perspective. In earlier years, im-
migrants were economically valuable. Today, however, they
are too diverse, The author of the book, Roy L. Garis, agreed
completely. He made this remark at the end of the book (p.
353): “For America, the Japanese are a non-assimilable peo-
ple, as are all Asiatics. . . .” Yet few immigrant groups have
been more successful in assimilating into the “melting pot” of
American life, and certainly few are more productive eco-
nomically.*

The fear of immigrants has grown worse in the 1980's, as a
result of the economic and political dislocations in Mexico
and Central America. Senator Simpson of Wyoming has in-
troduced legislation to make mandatory a universal identifica-
tion card for any American who seeks employment. Now that
bilingual education bas been established by the courts as
mandatory for public schools with Spanish-speaking students,

3. Albert Johnson, “Foreword,” to Roy L. Garis, Immigration Restriction: A
Study of the Opposition to and Regulation of Immigration into the United States (New
York: Macmillan, 1927).

4. Sowell, Ethnic America, Pt. III: “Americans from Asia.”
