160 CHRISTIANITY AND CIVILIZATION

blasphemes? Paul says that his learning must take place in the
college of excommunication!

The case of Hymenaeus and Alexander, of course, does
not afford proof that no counseling had preceded their expul-
sion. What it does indicate is that there are some sins that can-
not be remedied y counseling so that the remedial means for
the “irremediable” behavior is excommunication. Although
Christian counselors should be enjoined to be hopeful in see-
ing tangible results, they must not be naive either; nor must
they always impute to themselves guilt if the offender does not
repent, perhaps thinking that the only explanation of the sin-
ner’s recalcitrance lay in a faulty counseling methodology. Our
point is that what ts often amiss in the counsel is the very fact that it is
counsel!

This leads us then to another implication of unwarranted
counseling that may at first befuddle the mind: counseling left by
itself equals sanction. Such counseling sanctions the offence even
when the content of the counsel is an unequivocal condemna-
tion of the sinner’s perversity. How can this be? Let us con-
sider Eli as illustrative of our contention. Some mistakenly
reprove Eli for dereliction of parental responsibility because
he did not rebuke his salacious sons, who—we are told—
engaged in fornicative activity and brought down the
Levitical worship to the realm of the creature. The mythology
of this interpretation is due in part to a disregard of 1 Samuel
2:22-25 where Eli is portrayed as fazthfudly reprimanding his
sons. Yet, the judgment of God upon Eli and his house comes
later when the Lord affirms: “For I have told him that I will
judge his house forever for the iniquity which he knows:
because his sons made themselves vile, and he restrained them
not” (1 Samuel 3:13), The érue ground of the indictment against
Eli consisted in the fact that he “restrained them not.” As a
parent and as high priest, Eli had the authority to de more
than just engage in vigorous jaw-boning. There was no enforce-
ment of his counsel and because of this his advice is equated
with sanction so that “the iniquity of Elis house shall not be
purged with sacrifice nor offering forever” ({ Samuel 3:14). It
is likewise when the civil magistrate reprimands the murderer
while refraining from the God-required responsibility of
wielding the sword, or when a father like Eli verbally boards his
children when in fact their rowdiness calls for a more wooden
“board of education.” The possibility of the enforcement of
