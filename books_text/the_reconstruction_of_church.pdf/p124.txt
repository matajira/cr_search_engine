U2 GHRISTIANITY AND CIVILIZATION

are hypnotized rather than converted. They are acted on by sugges-
tion rather than authority, which lowers their personality
rather than rallies it, and moves them by man’s will rather
than God's.

Unfortunately the Church plays into the hands of pagan
desire with this rather subjective emphasis of manipulation.
Even the great R. L. Dabney in his Lectures on Sacred Rhetoric
said that the key to great preaching was the wil! to
dominate .?? Are the greatest preachers the ones wha have the
greatest desire to manipulate people? {s this correct? Is this
the emphasis of Paul? To all of these questions, Paul answers
“no.” He emphasizes the message over the medium. In other
words, the correct message will go a long way for therein lies
the power. Certainly, we can say that manipulation has been
the key to many great communicators, vis a vis Adolf Hitler
and the like. Bui good communication is not the same as good
preaching.

Good preaching is first and forernost at its best when it is
simple exposition. This method directs one’s attention to the ob-
jective Word from God where God has spoken in finality, and
away from the hearer’s subjective whims. One of the most im-
portant books ever written on preaching, Thoughis on
Preaching, was penned by the first professor of Princeton
Theological Seminary, J. W. Alexander. His basic argument
was that preachers need to return to the ancient practice of ex-
pository preaching. He says the following.

The pulpit discourses of Roman Catholics as well as Protes-
tants, during several centuries, have been, for the most part,
founded on short passages of Scripture; commonly single verses,
and oftener less than more. . . . It is not a little remarkable, that
in an age in which so much is heard against creeds and systems as
contradistinguished from the pure text of Scripture, and in which
sacred hermeneutics hold so high a place in Theological educa-
tion, we should have allowed the methodical and continued ex-
position of the Bible to go almost into disuse. . . .

The expository method of preaching is the most obvious and
natural way of conveying to the hearers the import of the sacred

2. P. T. Forsyth, The Principle of Authority, 2nd ed. (London: Independent
Press, 1952), p. 51. Cited in Donald G. Bloesch, The Future of Evangelical
Christianity: A Call for Unity Amid Diversity (Garden City, New York: Double-
day, 1983), p. 98.

22, RL. Dabney, Lectures on Sacred Rhetoric (Catlisle, Pennsylvania:
Banner of Truth Trust, 1979), pp. 233-260.
