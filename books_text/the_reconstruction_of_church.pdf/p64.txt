52 CHRISTIANITY AND CIVILIZATION

results. Jonathan Edwards’s preaching of eternal damnation,
Finney’s new measures, Moody’s pathos, and Billy Sunday’s
stimulating dramatizations all represent conscious efforts to
increase productivity, that is, to win more converts. Finally,
revivalists borrowed directly from their predecessors. Finney
read Edwards; Moody and Sunday read Finney. Revivalism
produced theological change, but possibly more importantly,
it created a culture in which more radical democratic and hu-
manistic ideologies could take hold. The effect of revivalism
was as much to popularize and support departures from
Puritanism as to create them.

The Great Awakening and iis Aftermath\*

The first major outbreak of revivalism, known as the First
Great Awakening, occurred during the first half of the eight-

 

13. Joseph Tracy wrote that revivalism’s “continued regard for practical
utility led some to embrace doctrines which they judged to be convenient,
instead of doctrines which they had proved to be true; or more accurately,
perhaps, to take their own opinion of the convenience of a doctrine, for
proof of its truth.” Tracy, The Great Awakening: A History of the Revival of Relig-
ion in the Time of Edwards and Whitefield (Boston: Tappan and Dennet, 1842),

. ald,
, The fact that revivalists adapted their theology to improve their produc-
tivity should not be construed as an indictment of their character, nor should
it cast doubt on their faith. Many, including Edwards, Whitefield, Moody,
and others were very great men and many, including Beecher and Dwight,
fought the blatant heresies of Unitarianism. Without doubt they believed
they were serving the best interests of their listeners.

14. There is no complete modern treatment of the Great Awakening.
Tracy’s above-mentioned work is the standard; it is still useful because it
relies heavily on original sources. Sweet’s book is commendable for this
same reason. Edwin Scott Gaustad, The Great Awakening in Naw England
{New York: Harper and Brothers, 1957) is useful, but, like Tracy's,
somewhat outdated. More recent surveys of the revival include Cedric
Cowing, The Great Awakening and the American Revolution: Colonial Thought in
the Eighteenth Century (Chicago: Rand McNally, [197i] 1972) and Alan
Heimert's introduction to The Great Awakening: Dacuments Illustrating the Crisis
and its Consequences (Indianapolis: Bobbs-Merrill, 1967), coedited with Perry
Miller, Heimert’s interpretive Religion and the American Mind from the Great
Awakening to the Revolution (Gambridge: Harvard University Press, 1966) is a
Provocative work that challenges some of the most cherished opinions of the
relationship between the revival and the War of Independence. In addition
to Heimert and Miller's collection of documents, Richard Bushman, The
