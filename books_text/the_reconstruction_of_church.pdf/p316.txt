304 CHRISTIANITY AND GIVILIZATION

poverty and dependence more comfortable. It strives to make
productivity and independence more attainable. Any pro-
gram of welfare that does not take this into account hurts
more than it helps.

This is one of the reasons why the federal government's
war on poverty is such a dismal failure. By asserting its uni-
versal responsibility to care for the poor, by centralizing the
criteria of poverty, by bureaucratically administering relief,
by reducing the importance of local conditions and accounta-
bility, and by institutionalizing the apparatus for care, the
State has created “a permanent welfare class which owes its
survival (it thinks) to the continued generosity of the State.”24
The war on poverty will never be met with anything except
devastation and defeat simply because it does not (and can-
not) help people get on their feet. It is but a salve to momen-
tarily succor mortal wounds. It is but a drop in the bucket,

When the church, in its zeal to procure mercy for the
broken and justice for the downtrodden, goes awhoring after
more statist intervention, the result is inevitably more statist
malediction.2® And, when the church simply mimics the gov-
ernment by prorniscuously dispensing groceries and other
goods and services, the end result is little better.

A handout does not charity make!

Every effort must be made to ensure that our helping
really does help. A handout may meet an immediate need,
but how does it contribute to the ultimate goal of setting the
recipient aright? How does it prepare him for the job market?
How does it equip him for the future? How well does it com-
rnunicate the Law of God and the precepts of Biblical charity?
The kind of evangelical myopia that envisions the Scriptural
duty to the poor as a simple transfer of funds simply misses
the boat. Adherents of such short-sighted thinking obviously
do not comprehend the first thing about Biblical charity or
economics.

Biblical charity is not built upon the flimsy foundations of

28. North, p. 137.

29. See Charles Murray’s brilliant description and documentation of this
in Losing Ground (New York: Basic Books, 1984), as well as the multitudinous
works of Thomas Sowell, Walter Williams, Murray Rothbard, George
Gilder, and Gary North.
