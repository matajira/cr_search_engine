Will Christians Bring in the Kingdom? 99
penalty of death or other chastisement.*

The above passages are taken by Walvoord to refer to the
rule of Christ on earth rather than the rule of Christ from heav-
en over the earth. There is no indication in the context where
these passages are found that an earthly, bodily kingship is in
mind. God is presently judging the earth through various
means, not all of which are political or immediate. With dispen-
sationalism, there seems to be less grace during the millennium
than there is now.

For the dispensationalist, the millennium’s social order is
centralized around the earthly rule of Christ. Reconstructionists
view the present-operating kingdom as a decentralized social
order where no individual or group of individuals has absolute
power. Jesus rules from heaven and delegates limited authority
to individuals and institutional governments such as families,
churches, and civil governments. Reconstructionists maintain
that evidence will still be required to convict any person of
criminal behavior. Society will not be structured along some
type of “Big Brother” concept. The power of civil government
at all levels will be decreased considerably. This will mean a
great reduction in taxation of all citizens. All the laws set forth
in the Bible to protect those accused of crimes will be applied
and enforced. Laws protecting life and property will receive
strong advocation.

The dispensationalist sees the kingdom coming as a cata-
clysm at the end of what they propose is a future seven year
tribulation period that the church will never experience. The
Reconstructionist views the kingdom as a present reality that
manifests itself as sinners embrace the gospel and live out their
new lives in conformity to the Bible. There is no kingdom to
bring in, since we are living in the kingdom. A millennial era of
blessings will be produced by the covenantal obedience of Chris-
tians, coupled with the saving work of the Holy Spirit.

4, John F, Walvoord, The Millennial Kingdom (Grand Rapids, MI: Dunham Publishing
Company, [1959] 1967), pp. 301-2. Dave Hunt writes that “Justice will be meted out
swiftly” during the millennium. Beyond Seduction: A Return to Biblical Christianity (Eugene,
OR: Harvest House, 1987), p. 250.
