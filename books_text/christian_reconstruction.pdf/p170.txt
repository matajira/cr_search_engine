148 CHRISTIAN RECONSTRUCTION

What did Neuhaus find so aberrational? A hundred years aga,
few people would have protested Reconstructionist distinctives.
The theological climate has changed, however. (So, for that
matter, has Neuhaus since he wrote his attack.)

Yet the attacks continue. Reconstructionism is called deviant,
heretical, and so forth by its critics. Not merely wrong-headed,
excessive, exaggerated, or even historically unprecedented, but
heretical. This is strong language, far stronger than Reconstruc-
tionists use against their opponents. Yet it is the Reconstruction-
ists who are called divisive and hostile. Why? More to the point,
which doctrine of the Reconstructionists is heretical?

If the only position taken by the Reconstructionists that is
unprecedented in church history is Van Til’s assertion of the
absolute authority of the Bible over all philosophy — biblical
presuppositionalism — why do so few of the critics attack us at
the one point where we are vulnerable to the accusation of new
theology? Probably because more and more of them are coming
to agree with us on this point: the myth of humanist neutrality.

Van Til was a Calvinist. He defended his position in terms of
Calvinism. He said that Calvinism, with its doctrine of the abso-
lute sovereignty of God, is the only Christian position that can
systematically and consistently reject all compromises with hu-
manism, for Calvinism alone grants no degree of autonomy to
man, including intellectual autonomy. So, the critics have a
problem: if they accept biblical presuppositionalism without
accepting Calvinism, they have an obligation to show how this
is intellectually legitimate. They have to refute Van Til. On the
other hand, if they do not do this, yet they also remain con-
vinced that neutrality is a myth, they have to ask themselves: In
what way is Christian Reconstruction heretical?

Calvinism
Reconstructionists are Calvinists, i.e., defenders of the doc-
trine of predestination by God. This is certainly no aberration
of historic Christianity. Many of the greatest ministers and
theologians of the Christian church have been Calvinists. Many
of the greatest ministers and theologians of our own day are
Calvinists. Consider the words of Charles Haddon Spurgeon,
