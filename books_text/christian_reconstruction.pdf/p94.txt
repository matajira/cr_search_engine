72 CHRISTIAN RECONSTRUCTION

their brightest younger followers will. So do the traditional
premillennialists, but there is little that they can do about it.

Isaiah’s Millennial Vision

Autonomous man, the traditional, consistent premillennialist
says, is more powerful in history than the Spirit-empowered
gospel. Then premillennialists accuse Christian Reconstruction-
ists of having too much faith in man. But is it “faith in man” to
preach that the Holy Spirit will change the hearts of many men
in history? Is it “faith in man” to preach, with Isaiah, that a
better day is coming? “Behold, a king shall reign in righteous-
ness, and princes shall rule in judgment. And a man shall be as
an hiding place from the wind, and a covert from the tempest;
as rivers of water in a dry place, as the shadow of a great rock
in a weary land. And the eyes of them that see shall not be dim,
and the ears of them that hear shall hearken. The heart also of
the rash shall understand knowledge, and the tongue of the
stammerers shall be ready to speak plainly. The vile person
shall be no more called liberal, nor the churl said to be bounti-
ful. For the vile person will speak villany, and his heart will
work iniquity, to practise hypocrisy, and to utter error against
the Lorp, to make empty the soul of the hungry, and he will
cause the drink of the thirsty to fail. The instruments also of the
churl are evil: he deviseth wicked devices to destroy the poor
with lying words, even when the needy speaketh right. But the
liberal deviseth liberal things; and by liberal things shall he
stand” (Isaiah 32:1-8).

Must this king reign from some future earthly throne in
order for this prophecy to be fulfilled? Why? Why can’t He
reign from on high, at the right hand of God? Why can’t He
reign through the Holy Spirit and His holy people? Theologi-
cally speaking, why not?

The Reign of Jesus Christ

What kind of religion is premillennialism? What kind of
ethical system does it teach? The same as amillennialism teach-
es: the defeat of Christian civilization in history. Evil men will
