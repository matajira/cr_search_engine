What Is the Proper Response? 179

they are attracted to a theological position that teaches that
Christianity can and will transform world civilization. They are
not instinctively attracted to eschatological positions that pro-
claim progressive Christian impotence as a way of life.* When
they see a theological alternative, they adopt it.

Conclusion

It boils down to this: our critics can’t beat something with nothing.
We have offered, and continue to update, a consistent, compre-
hensive, Bible-based alternative to a collapsing humanist order.
What do our critics offer as their workable alternative? If it is
just another round of “Come quickly, Lord Jesus,” then they
are not offering much. That prayer is legitimate only when the
one who prays it is willing to add this justification for his
prayer: “Because your church has completed her assigned task
faithfully (Matthew 28:18-20), and your kingdom has become
manifest to many formerly lost souls.” This is surely not a
prayer that is appropriate today. (It was appropriate for John
because he was praying for the covenantal coming of Jesus Christ,
manifested by destruction of the Old Covenant order. His
prayer was answered within a few months: the destruction of
Jerusalem.)”

One last note: for those who still think my style is intemper-
ate and unnecessarily personal, please read Mark Edwards’ two
scholarly books on Luther’s polemics. Get a feel for what a
master polemicist and pamphleteer did, and why he successfully
launched the Protestant Reformation. Then re-read Calvin's
Institutes. When it comes to invective, I am a piker.*

26. North, Millonniatiom and Social Theory.

27. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987); Kenneth L. Gentry, Jr, Before Jerusalem Fell: Dating
the Book of Revelation (Tyler, Texas: Institute for Christian Economics, 1989),

28. Mark U. Edwards, Jr., Luther and the False Brethren (Stanford, California: Stanford
University Press, 1975); Lusher’s Last Battles: Politics and Polemtics, 1531-46 (ithaca, New
York: Cornell University Press, 1983).
