Is Revolution the Way to Advance God’s Kingdom? 41

Schaeffer is the only writer whom Geisler cites. Geisler does nat
cite a single postmillennial writer who advocates revolution, so
it is sheer bias on his part to conclude that Schaeffer is operat-
ing as a postmillennialist.

Modern postmillennial Reconstructionists are not revolution-
ary because they have a more consistently biblical view of the
future. Reconstructionists generally believe they have time, lots
of time, to accomplish their ends. Moreover, they are not revo-
lutionary because they believe that Christians achieve leadership
by living righteously. Dominion is by ethical service and work,
not by revolution. Thus, there is no theological reason for a
postmillennialist to take up arms at the drop of a hat. Biblical
postmillennialists can afford to wait for God to judge ungodly
regimes, bide their time, and prepare to rebuild upon the ruins.
Biblical postmillennialists are not pacifists, but neither are they
revolutionaries.

Biblical postmillennialism provides the Christian with a leng-
term hope. Because of his long time-frame, the postmillennialist
can exercise that chief element of true biblical faith: patience.
Because he is confident that the Lord has given him time to
accomplish the Lord’s purposes, the postmillennialist need not
take things into his own, sinful hands. The Lord will exalt us
when He is ready, and when He knows that we are ready. Our
calling is to wait patiently, praying and preparing ourselves for
that responsibility, and working all the while to advance His
kingdom. Historically, Christians who lack this long-term hope
have taken things into their own hands, inevitably with disas-
trous consequences. Far from advocating militancy, biblical
postmillennialism protects against a short-term revolutionary
mentality.

Reconstructionists believe that Christians should follow the
examples of biblical characters such as Joseph, Daniel, and
Jesus Christ Himself. Joseph and Daniel both exercised enor-
mous influence within the world’s greatest empires. But they
attained their positions by hard work, perseverance in persecu-
tion and suffering, and faithful obedience. Jesus Christ attained
to His throne only by enduring the suffering of the cross. Chris-
tians are no different. We are not to attain positions of leader-
