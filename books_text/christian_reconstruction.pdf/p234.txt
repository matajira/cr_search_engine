212 CHRISTIAN RECONSTRUCTION

Eschatology as Test of Orthodoxy,
12-15

Ethics, 50

Evangelicals, 28

Evangelism, 35, 36

Evangelism through Law, 37

Evolution, 89, 128, 166

Federalist Papers, 121
Feinberg, John S., 96
First Amendment, 120
Fools, 165-69
Foundation for Economic Educa-
tion (FEE), xi
Fowler, Richard A., 85n
Frame, John, 19
Freeman, The, xi
French Revolution, 52
Fundamentalists, 163

Geisler, Norman L.
Christian Reconstruction, 85n,
92n
Civil Disobedience, 140
Civil Government, 87, 108,
110
Creation Science, 92n
Dispensationalism, 22, 116
Schaeffer, Francis A., 140
Humanism, 90
Mosaic Legislation, 87
Natural Law, 17, 22, 108, 110,
6
Premillennialism, 116, 140
Religious Obligation, 110
Ten Commandments, 85n
Geneva Bible, 134
Genocide, 6-8
Gentry, Kenneth L., xiv, 3, 18n,
34n, 175, 179n
Germanic gods, 8

Gerstner, John, 177
Ghetto Mentality, 29
Gillespie, George, 153
God Who is There, xiii
Good Works, 35

Gorky, Maxim, 7

Gouge, William, 156
Government, 44

Grant, George, xv, 52, 92
Green, Ashbel, 153
Green Politics, 46
Greene, Bob, 184

Gross Misrepresentation, 10-12

Hal Lindsey and the Restoration
of the Jews, 3n

Hamilton, Bernice, 112n

“Hands Off,” 138-39

Harper, EA., x

Harvard, 15

Harvest House, 2

Hayek, FA, x

Healer of the Nations, 78n

Heller, Mikhail, 7n

Helms, Jesse, 166

Henry, Carl FH., 14n

Heresy, 91, 157

Heresy Hunting, 1, 8-9

Hippocratic Oath, 112

Hitler, Adolf 7-8, 121

Hoehner, Harold, 71, 89

Hodge, AA., 157

Hodge, Charles, 130, 135, 157

Holocaust, 137-39

Home Box Office (HBO), 185

Homosexuality, 18, 90

Homosexuality: A Biblical View, xiv

House Divided, 175, 177

Honest Disagreement, 19

“Honest Reporting as Heresy,” 9

Hooper, John, 153

Hoover Institution, ix
