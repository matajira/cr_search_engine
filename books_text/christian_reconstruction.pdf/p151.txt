isn’t Postmillennialism Really Liberalism? 129

Paul, however, intimates that Christ will in the meantime, by the
rays which he will emit previously to his advent, put to flight the
darkness in which antichrist will reign, just as the sun, before he is
seen by us, chases away the darkness of the night by the pouring
forth of his rays.

This victory of the word, therefore, urill show itself in this world... . He
also furnished Christ with these very arms, that he may rout his enemies.
This is a signal commendation of true and sound doctrine — that it is
represented as sufficient for putting an end to all impiety, and as des-
tined to be invariably victorious, in opposition to all the machinations of
Satan?

Calvin thus believed that the kingdom was already present, and
that it was triumphantly advancing to a great climax.

This victorious outlook was embodied in the 1648 Westmin-
ster Larger Catechism. The answer to question 191 states:

In the second petition, (which is, Thy kingdom come,) acknowledg-
ing ourselves and all mankind to be by nature under the dominion
of sin and Satan, we pray, that the kingdom of sin and Satan may be
destroyed, the gospel propagated throughout the world, the Jews
called, the fullness of the Gentiles brought in; the church furnished
with all gospel-officers and ordinances, purged from corruption,
countenanced and maintained by the civil magistrate: that the
ordinances of Christ may be purely dispensed, and made effectual
to the converting of those that are yet in their sins, and the confirm-
ing, comforting, and building up of those that are already convert-
ed: that Christ would rule in our hearts here, and hasten the time
of his second coming, and our reigning with him for ever: and that
he would be pleased so to exercise the kingdom of his power in all
the world, as may best conduce to these ends.

Jonathan Edwards (1703-1758) expected an even fuller out-
pouring of the Spirit in the future, so that “the gospel shall be
preached to every tongue, and kindred, and nation, and people,
before the fall of Antichrist; so we may suppose, that it will be
gloriously successful to bring in multitudes from every nation:

2. Quoted in Greg Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
Journal of Christian Reconstruction III, ed. Gary North (Winter 1976-1977), p. 70. Emphasis
‘was added by Dr. Bahnsen.
