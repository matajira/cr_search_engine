52 CHRISTIAN RECONSTRUCTION

This widespread hostility to biblical civil sanctions necessarily
forces Christians to accept the legitimacy of non-biblical civil
sanctions. There will always be civil sanctions in history. The
question is: Whose sanctions? God's or Satan’s? There is no neu-
trality. Only the legalization of abortion on demand has at last
forced a comparative handful of Christians to face the truth
about the myth of neutrality. There can be no neutrality for the
aborted baby. There is no neutrality in the abortionist’s office.
There is either life or death.

We need to begin to train ourselves to make a transition: the
transition from humanism’s sanctions to the Bible’s sanctions, in every
area of life. This includes politics. God has given His people
major political responsibilities that they have neglected for at
least fifty years, and more like a century. Christians are being
challenged by God to reclaim the political realm for Jesus
Christ. We must publicly declare the crown rights of King Jesus.

New Sanctions, New Covenant

Christians today have a golden opportunity — an opportunity
that doesn’t come even as often as once in a lifetime. It comes
about once every 250 years. The last time it came for Christians
was during the American Revolution era (1776-1789). The
revolutionary humanists who created and ran the French Revo-
lution (1789-95) created a satanic alternative, one which is with
us still in the demonic empire of Communism. But that empire
has now begun to crumble visibly."

A showdown is coming, in time and on earth: Christ vs.
Satan, Christianity vs. humanism, the dominion religion vs. the
power religion. You are being called by God to take up a position of
judicial responsibility on the side of Christ. One aspect of this re-
sponsibility is to render biblical political judgments.’

It is time to begin to prepare ourselves for an unprecedented

1. This economic and also ideological crumbling is real, but not necessarily irrevers-
ible militarily. The killing power of the weaponry still in the hands of Soviet generals and
admirals dwarfs anything possessed by the West.

2. George Grant, The Changing of the Guard: Biblical Blueprints for Political Action (Et.
Worth, Texas: Dominion Press, 1987).
