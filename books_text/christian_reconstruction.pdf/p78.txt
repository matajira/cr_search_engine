Chapter 6

THE FOUR COVENANTS OF GOD

That at that time ye were without Christ, being aliens from the com-
monwealth of Israel, and strangers from the covenants of promise, having
no hope, and without God in the world (Ephesians 2:12).

We examine our condition in life not simply by our present
outward condition, but by the Word of God, the Bible. History
is governed by God in terms of His eternal standards. Through
Adam, God placed all mankind under a covenant, the dominion
covenant.’ He told Adam that he must subdue the earth (Gene-
sis 1:28).

What is a covenant? God comes before man and “lays down
the law” — His law. Man must either conform to God by obey-
ing His law or else be destroyed. As He told Adam, “Eat of the
tree of the knowledge of good and evil, and you will die.” God
deals with men as a king deals with his subjects. His covenant is
to prosper us when we obey and curse us when we rebel.

First, God makes a personal covenant with man. A man’s
eternal destiny depends on how he responds to God's covenant.
God also makes institutional covenants with men. There are
three of these: family, church, and State. Each has an appropri-
ate oath. Each has laws. Each has penalties for disobedience.

As I wrote in Chapter 5, a biblical covenant has five sections:

1. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute for
Christian Economics, 1987).
