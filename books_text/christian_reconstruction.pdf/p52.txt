30 CHRISTIAN RECONSTRUCTION

ally, They call for the recovery and implementation of the older
Protestant view of God’s kingdom. This is what has made
Christian Reconstructionists so controversial. Today's Protes-
tants do not want to give up their medieval Roman Catholic
definition of the kingdom of God, and they deeply resent any-
one who asks them to adopt the original Protestant view. Their
followers are totally unaware of the origins of what they are
being taught by their leaders.

The Kingdom of God

There are a lot of definitions of the kingdom of God. Mine
is simultaneously the simplest and the broadest: the civilization
of God. It is the creation — the entire area under the King of
Heaven’s lawful dominion. It is the area that fell under Satan's
reign in history as a result of Adam’s rebellion. When man fell,
he brought the whole world under God’s curse (Genesis 3:17-
19). The curse extended as far as the reign of sin did. This
meant everything under man’s dominion. This is what it still
means. The laws of the kingdom of God extend just as far as sin
does. This means every area of life.

God owns the whole world: “The earth is the Loro’s, and the
fulness thereof, the world, and they that dwell therein” (Psalm
24:1). Jesus Christ, as God’s Son and therefore legal heir, owns
the whole earth. He has leased it out to His people to develop
progressively over time, just as Adam was supposed to have
served as a faithful leasecholder before his fall, bringing the
world under dominion (Genesis 1:26-28). Because of Jesus’
triumph over Satan at Calvary, God is now bringing under
judgment every area of life. How? Through the preaching of
the gospel, His two-edged sword of judgment (Revelation
19:15).

Reform and Restoration

The kingdom of God is the arena of God’s redemption. Jesus
Christ redeemed the whole world — that is, He bought it back. He
did this by paying the ultimate price for man’s sin: His death on
the cross. The whole earth has now been judicially redeemed. Iv has
