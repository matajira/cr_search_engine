184 CHRISTIAN RECONSTRUCTION

older a person gets, the more responsibilities he or she has. For
centuries the church addressed the areas of law, education, and
politics, to name only a few. The Bible was very much a “this-
worldly religion.” Sadly, this no longer seems to be the case.
Islam, as a rival faith, has supplanted Christianity in the vital
area of a this-world application of God’s Word. Secularism,
previously Christianity’s greatest nemesis, has been doing it for
centuries in the name of “enlightenment.” Islam with its em-
phasis on imminence and practicality is growing in influence.

Islam is practical. It is considered a this-worldly religion in
contrast to Christianity, which is perceived as abstract in the ex-
treme. Muhammad left his followers a political, social, moral, and
economic program founded on religious precepts. Jesus, however,
is said to have advocated no such program; it is claimed that the
New Testament is so preoccupied with his imminent return that it
is impractical for modern life.’

Christians have forsaken God’s Bible-revealed law as the
universal standard for righteousness in the areas of economics,
education, politics, and the judicial system. Instead, they have
adopted escapism (Jesus could return at any moment) and a
form of ethical pluralism (the Bible is only one law among
many from which to choose). Ethical pluralism means that ali
moral views are valid except any moral view that does not believe that
all moral views are valid. So, Christianity as the Bible conceives of
it is not an acceptable ethical standard on how the world should
work. The absolutist position that the Reconstructionists take is
anathema to the modern mind, and its rejection of ethical plu-
ralism makes it the scourge of the Christian community.

With pluralism you get an “anything goes” ethic. Since most
Americans (and most Christians) believe that ethical pluralism
is legitimate, they often remain silent in the midst of the storm
of moral anarchy that is battering our nation. They have been
propagandized into believing that this is the American way. Bob
Greene, a syndicated columnist with the Chicago Tribune, was
shocked when he realized that millions of parents sat and

1, Larry Postan, “The Adult Gospel,” Christianity Taday (August 20, 1990), p. 24.
