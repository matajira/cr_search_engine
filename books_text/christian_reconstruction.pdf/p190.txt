168 CHRISTIAN RECONSTRUCTION

Huxley did not answer Wilberforce according to the Bishop's
folly. He did answer him, however. Whatever else he actually
said in those ten minutes is long forgotten. It was probably for-
gotten by the next day. What was remembered was his effective
countering of the Bishop’s failure to grasp the vulnerability of
an overly clever sarcastic remark. Sometimes it is better to play
the wounded lamb than to play the wounded water buffalo, let
alone the clever comic.

Do not answer a fool according to his folly. Except, of course,
when you should.

Knocking the Stuffings Out of a Stuffed Shirt

“Answer a fool according to his folly, lest he be wise in his
own conceit” (Proverbs 26:5). There are times when a direct
response is just what the doctor ordered. You can legitimately
use ridicule in order to identify the ridiculous. This is Solo-
mon’s implicit advice, and it was Augustine's explicit advice.°
Nevertheless, you have to know how and when to exercise this
skill. You must also calculate the risk. Bishop Wilberforce ap-
parently neglected to do either.

What I have learned over the years is that we Christian Re-
constructionists are constantly confronting people with minimal
intellectual abilities and minimal professional training, as well
as (very occasionally) people with considerable intellectual abili-
ties who have decided to rush into print without first doing
their homework. They are not the type of fool identified in the
Bible: people who have said in their hearts that there is no God
(Psalm 14:1). Darwin was a fool. Huxley was a fool. Marx was a
fool. Freud was a fool. Our published critics, in contrast, are for
the most part just not very bright. And when they are bright,
they have been lazy. They let their less intellectually gifted
peers do their initial homework for them; then they cite as
authoritative these half-baked published reports.”* It is wiser to

9. City of God, XVIIL:49.

10. An example of this is provided by Dr. Peter Masters, who occupies Charles
Spurgeon’s pulpit. Dr. Masters promotes the book by House and Ice, Dominion Theology:
Blessing or Curse?, as if Bahnsen and Gentry had not demolished it in House Divided. See
