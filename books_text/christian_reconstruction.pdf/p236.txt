214 CHRISTIAN RECONSTRUCTION

Kaiser, Walter, 21

Kellogg, S. H., 154

Kelly, Gerard, 114n

Kennedy, D. James, 149

Kenotic Theology, 49

Kevan, Ernest, 107n

Kik, Marcellus, 157

Kingdom
Already-Not Yet, 96
Ascension, 61
Calvin, John, 156
vs. Church, 27-29
Civilization, 30, 154, 162
Coming, 60
Comprehensive Redemption,

28, 31
Continuity, 96
Denying Responsibility, 32
Earthly, 29
vs, Ecclesiocracy, 29
Edwards, Jonathan, 129
Hodge, Charles, 130
Holy Spirit, 31, 60-61
Judicial Redemption, 30
Judging, 27
Jurisdiction, 27
Manifestation on Earth, 27
Nearness, 96
vs. “Political Kingdom,” 27
Political Responsibility, 33-37
Present Status, 97, 129
Principles, 60
Reformation, 30-31
Roman Catholic View, 28-30
Satan’s, 60, 130
Source, 33
Transcendent, 33
Victorious Character, 128-30
Westminster Larger Catech-
ism, 129

Kline, Meredith G., xi, 54

Knott's Berry Farm, xii

Knott, Walter, xii

Knox, John, 149, 153
Koppel, Ted, 18
Kosster, John P, 166n
Kuhn, Thomas, 41
Kuiper, R. B., 13-14
Kulaks, 7

Last Temptation of Christ, 187
Latimer, Hugh, 153
Law
Alteration, 43
Blueprint, 17, 51
Excommunication, 51
Holy Spirit, 51
Jungle, 47
Legal Schizophrenia, 17
Natural Law, xi, 17
Old Testament, 43
Perpetual Standard, 16
Prison Solution, 17
Sanctions, 51
Social and Political Answers,
43
Theonomy, 19-20
Ten Commandments, 18
Van Til, Cornelius, xi
Whole Bible, 16
Law of Moses, 151
Law and Society, xv
Lee, Francis Nigel, 178
Leithart, Peter, 3, 11, 93, 169-70,
15
Lewis, David Allen, 70
Liberalism, 127-28
Liberation Theology, 82
Lightner, Robert L., 22
Lindsey, Hal
“Anti-semitism” Charge, 3, 169
False Predictions, 6
Intellectually Corrupt, 169,
178
Late Great Planet Earth, 6, 88
