Subject Index 211

Debate, 166
Defense of the Faith, x
DeJong, J.A., 122n, 156
DeMar, Gary
American Vision, xv
Debate over Christian Reconstruc-
tion, 3, 173n
Democracy, 10
God and Government, xv, 15n,
93
Legacy of Hatred Continues, 3,
Hi, 169-70
Politics, 93
Pyramid Society, 94
Reduction of Christianity, 2n,
36n, 93, 125n, 173n, 175
Reformed Theological Semi-
nary, xiv
Ruler of the Nations, 15n, 93
Democracy, 120-22
“Democracy as Heresy,” 9
Dennis vu. United States, 1090
De Regno Christi, 153
Dispensationalism
Aberrational, 13, 82, 177
Activism, 71
Anti-Reformed Heresy, 13
Anti-semitism, 138-39
Armageddon, 5
Bakker, Jim, 64
Bureaucracy, 66-69, 71, 73-74
Cataclysm Theology, 99
Church Age, 86
Democracy, 68
Denial of History, 67
Escapist Theology, xviii, 15, 70
Ghetto Theology, 173
“Great Tribulation,” 5, 67
Hunt, Dave, 15, 66
Intellectual Error, 5, 177
Israel, 4-5, 137-39
Kingdom, 98-99
Law, 69, 86

Lewis, David Allen, 70
Millennium, 73-74, 86
Novel Millennial View, 14
Pessimism, 67, 72, 88
Political Freedom (morally
corrupt), 69
Political Power, 63
Prophetic Inevitability, 6
Rapture, 71
Revived Roman Empire, 88
Revolution, 140
Sensationalism Game, 3-5
Shift, 64
Silence, 64
Social Irresponsibility, 15-16,
89
Swaggart, Jimmy, 64
Theocracy, 68
Dispensationalism in Transition, 177
Dominion
Biblical Law, 160
By Consent, 93
Ethical Service, 141-42
Inescapable.Concept, 57
Library of Congress, 65n
Religion, 52
Dominion and Common Grace, 160
Dominion Covenant: Genesis, xv,
56n
Dominion Press, xv
Dominion Theology: Blessing or
Curse?, 2, 106n, 177
Downie, Rex, 108n

Eastern Europe, 36

Economics, 50

Edwards, Jonathan, 129-30, 135
Egypt and Bureaucracy, 47
Egypt and Civilization, 47

Eliot, John, 153

English Common Law, 153
Escape Religion, xviii
