Preface xvii

Outraged Critics

Can you imagine the outrage we would hear if he were alive
today and made such a public statement? For that matter, we
can be confident that the priests and civic officials of his day
would have had a thing or two to say about such an attitude,
had he not occupied the throne. I can almost hear them:

Just who do you think you are? You claim far too much for
yourself. By what authority do you claim such wisdom? Where did
you, a mere shepherd by birth, get the idea that you excel your
teachers, let alone the ancients? Have you studied under a master?
Have you devoted years of work to a study of modern theology?
No? Well, then, it is clear to us that you are ill-equipped to evaluate
your own performance. You are an arrogant man, sir. You are
puffed up with your own importance. You think you have great
insights, but you are out of step with the latest findings of contem-
porary theologians. Your attitude is unbiblical, You lack deep spiri-
tuality. You are spiritually shallow. No thinking person is going to
take seriously anything a strutting, self-inflated character like you
has to say. Your style proves that you have nothing important to
say. Therefore, we need not examine the content of your position.

David knew the authority in history that the revealed law of
God offers to those who take it seriously and conform them-
selves to it. He announced his reliance on biblical law and the
tremendous advantage it gave him: wisdom. The law of God is a
tool of dominion, a means of spiritual advancement, and the
foundation of progress: personal, spiritual, intellectual, and
cultural. This is the message of the 119th psalm, the longest
passage in the Bible, a psalm devoted to praising the law of
God. By understanding the law of God and applying it in his
life, a person advances the kingdom of God in history. So it was
with David. He identified himself as one who had advanced
beyond previous generations. David reminded his listeners:

IT have refrained my feet from every evil way, that I might keep
thy word. I have not departed from thy judgments: for thou hast
taught me. How sweet are thy words unto my taste! yea, sweeter
than honey to my mouth! Through thy precepts I get understand-
ing: therefore I hate every false way. Thy word is a lamp unto my
