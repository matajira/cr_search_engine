x CHRISTIAN RECONSTRUCTION

early books. It put him on a retainer to write The One and the
Many (1971) after the Fund began to be shut down in 1964.
That retainer income financed his move to Southern California
in 1965.° He later dedicated the book to the administrator of
the Fund, Harold Luhnow, the nephew of the late William
Volker (“Mr. Anonymous”). It was Luhnow who had agreed to
hire him in 1962, when Luhnow fired a group of libertarian
scholars under the leadership of F A. Harper.‘

Rushdoony sent me Cornelius Van Til’s apologetics syllabus
in the fall of 1962, which I read and came to accept before 1
graduated from college that June. He hired me to come to the
Volker Fund as a summer intern in 1963, and I lived with his
family in Palo Alto.’ Essentially, I was paid $500 a month (a
princely sum in those days) to read books, It was during that
summer that I read the major works of Ludwig von Mises, F. A.
Hayek, Murray N. Rothbard, and Wilhelm Roepke. It was the
most important “summer vacation” of my life.

At Rushdoony’s insistence, I also read Van Til’s Defense of the
faith. He had brought me to work at the Fund to provide the
money for me to attend Westminster Theological Seminary in
Philadelphia, specifically to study under Van Til. I had original-
ly planned to attend Dallas Seminary. I was a hyper-dispensa-
Honalist at the time (Cornelius Stam, J. C. O’Hair), though a
predestinarian. The problem was, as I learned that fall, Van Til
never assigned any of his own books to his classes, and his
classroom lecture style was as close to Werner Heisenberg’s
indeterminacy principle as anything 1 have ever seen. 1 left
Westminster after one academic year, but not before Professor

3, He did not return to the pastorate. He sought and received formal permission to
labor outside the bounds of the Northern California Presbytery of the OPC, a status he
maintained until he left the denomination in the early 1970's.

4. Harper had answered by mail some of my questions about economics as carly as
summer, 1961, and he brought me to the Volker Fund, located in Burlingame, Califor-
nia, that fall, a semester before I heard of Rushdoony. He gave me several books at that
time, and a year later sent me Murray Rothbard’s incomparable Man, Economy, and State,
after he set up his own organization, the Institute for Humane Studies, in 1962.

5. Another staff member was Rev. C. John Miller, who later went to Westminster
Seminary as a faculty member. Miller wrote a three-volume manuscript against public
education while on the staff. It was never published.
