14 CHRISTIAN RECONSTRUCTION

John Murray, also of Westminster Seminary, wrote that the
“‘Dispensationalism’ of which we speak as heterodox from the
standpoint of the Reformed Faith is that form of interpretation,
widely popular at the present time, which discovers in the sev-
eral dispensations of God’s redemptive revelation distinct and
even contrary principles of divine procedure and thus destroys
the unity of God’s dealings with fallen mankind.”*

Premillennialism of the covenantal variety was not under
attack by these men. Kuiper made this crystal clear:

It is a matter of common knowledge that there is ever so much
more to the dispensationalism of the Scofield Bible than the mere
teaching of Premillennialism. Nor do the two stand and fall together.
There are premillennarians who have never heard of Scofield’s dis-
pensations. More important than that, there are serious students of
God’s Word who hold to the Premillennial return of Christ and em-
phatically reject Scofield’s system of dispensations as fraught with
grave error.

How can we avoid the pitfalls of “orthodoxy by eschatology,”
especially when that eschatology is the aberrational dispensa-
tionalism? If we all stick with the historic creedal formulation
that Jesus will “come again to judge the quick and the dead” a
lot more understanding will take place among those who differ
on eschatological systems of interpretation.” Christian Recons-
tructionists have been unjustly criticized by a novel millennial
view for holding an eschatological position that has both scrip-
tural and historical support.

in Edwin H. Rian, The Presbyterian Conflict (Grand Rapids, MI: Eerdmans, 1940), p. 101.
35. The Presbyterian Guardian (February 3, 1936), p. 143. Quoted in ibid., pp. 236-7.
36. The Presbyterian Guardian (November 14, 1936), p. 54. Quoted in ibid., p. 31.
37. Carl Henry wrote the following in 1947:

Furthermore, there is a noticeable shift in eschatological thinking. On the
one hand, there appears a return to a more conservative type of pre-millen-
nialism, such as that of Alford and Trench, with an accompanying tendency
to discard dogmatism on details; if this continues, the eschatological preach-
ing of next generation Fundamentalisis will concentrate on the proclamation
of the kingdom, the second coming, the bodily resurrection of the dead, and
the fature judgment, and will not concern itself too much with lesser events
(The Uneasy Conscience of Modern Fundamentalism [Grand Rapids, Eerdmans,
1947}, p. 51).

 
