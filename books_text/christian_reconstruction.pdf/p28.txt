6 CHRISTIAN RECONSTRUCTION

Bible prophecy: the New Age, demons, last-days cults, etc. It all
began with The Late, Great Planet Earth. There is big money and
personal fame in making millennial predictions, even when they
do not come true (and they never do). When they do not come
true, the author then can write another sensational book show-
ing that they really will come true. Like an addiction, sensation-
alism feeds on itself, both for the pusher and the addict.

History has recorded for us the fulfillment of this prophecy
concerning Jerusalem and Old Covenant Israel.’ But because
dispensationalism sees this as unfulfilled prophecy, an escalation
of anti-semitism in these “last days” becomes a “sign” of the
end. Anti-semitism becomes a prophetic inevitability under
dispensationalism. So, they go around looking for anti-semites
under every eschatological bed except their own.

What the sensationalists neglect to mention is that the exis-
tence in the Bible of an anti-Old Covenant Israel prophecy,
fulfilled or unfulfilled, in and of itself tells us nothing about the
intent or attitude of the person who believes it. Only the specif-
ic content of his exposition tells us what he thinks about the
proper interpretation. That a Bible expositor faithfully refers to
an anti-Old Covenant Israel prophecy says nothing about his
attitude toward Jews, either as a group or as individuals. But to
admit this is to reduce the moral legitimacy of sensationalism.

What is the result? The Road to Holocaust.

The Roots of Genocide

“Anti-semitism” has nothing to do with eschatology. There
are as many reasons for “anti-semitism” as there are reasons for
all types of national and racial hatred. Dozens of religious,
racial, and ethnic groups have been persecuted over the centu-
ries. The martyrdom of Christians began as soon as the gospel
called upon people to repent, and it continues to this day (Acts

9. Flavius Josephus, The Wars of the Jews or The History of the Destruction of Jerusalem in
The Works of Josephus, trans. William Whiston (Peabody, MA: Hendrickson Publishers,
1987), pp. 543-772. For an excerpted reading of the material pertinent to Jerusalem's
AD. 70 destruction, see David Chilton, Paradise Restored: A Biblical Theology of Dominion
(Ft. Worth, TX: Dominion Press, 1985), pp. 237-90.
