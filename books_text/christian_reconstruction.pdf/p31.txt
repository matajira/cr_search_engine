Introduction 9

ers intent on exposing every hint of error in the Christian com-
munity, describes the contemporary theological climate: “Chris-
tians are attacking Christians, charging one another with here-
sy. But are the accusations fair? Is the reasoning valid?” The
book you now are reading is about fairness. There is certainly
no doubt that Christians disagree on numerous issues. At this
point in history the Christian church has not attained theologi-
cal consensus. When disagreements arise over variant theologi-
cal positions, it is incumbent upon opposing sides to at least
represent the opposing doctrinal opinions accurately. Has this
been done? For the most part, it has not.

Honest Reporting as Heresy

Probably the most infamous attack on Christian Reconstruc-
tion was “Democracy as Heresy,” published in Christianity Tb-
day.”' I have seen its conclusions repeated in numerous articles
critical of Christian Reconstruction. In many cases this is a sub-
sequent article’s enly source of information for its critique. Are
the author’s conclusions reliable and his assessments fair? In
most cases they are not, and there is no way that they could be,
since Mr. Clapp stated that “he had not had time to read our
books in detail, for the Reconstructionists publish too much.”
There seems to be a pattern here. Self-proclaimed heresy hunt-
ers have become “experts” without reading an adequate num-
ber of the published works of Reconstructionists, and their false
reporting gets picked up by others who fail to check the reli-
ability of their sources. While Reconstructionists certainly do
publish quite a few books each ycar, they are all indexed! There
is no excuse for sloppy scholarship.

The misrepresentations of the “Democracy as Heresy” article
are too numerous to list. A single example, however, will give
you some idea how bad this article really is. On the first page of

20, Bob and Gretchen Passantino, Witch Hunt (Nashville, TN: Thomas Nelson, 1990).

21. Radney Clapp, “Democracy as Heresy,” Christianity Teday (February 20, 1987),
pp. 17-28.

22. Gary North, “Honest Reporting as Heresy: My Response to Christianity Tday”
(Tyler, TX: Institute for Christian Economics, 1987), p. 3.
