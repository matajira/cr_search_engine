Are Our Critics Honest? 159

— but must be careful even at such times not to overstep its limited
license. There must be no significant intrusion of “religion” into real
life — affairs of state, education, social action, pleasure.”

Dave Hunt should recall some of the experiences he encoun-
tered in his dealings with the Plymouth Brethren movement so
he can understand how others feel when they too are unjustly
treated. In addition, he might want to explain why his earlier
works sound suspiciously like present-day Reconstructionist
writings.

Conclusion

There is a tendency among heresy hunters to look for the
most controversial doctrines of a theological system and then to
evaluate the entire system solely in terms of the controversial
doctrines. If you are a die-hard dispensationalist, then postmil-
lennialism is going to look aberrational to you. As has been
shown, however, many fine Christians have held to a postmil-
lennial eschatology. The same can be said for the Reconstruc-
tionist’s adherence to Calvin’s doctrine of salvation and his view
of the law. Of course, this does not make the positions ortho-
dox, but it ought to make people think twice about condemning
a group of believers because they hold an opposing doctrinal
position that has biblical and historical support. The three
doctrines listed above — Calvinistic soteriology, biblical ethics,
and postmillennial eschatology — are set forth in masterful
detail in the Westminster Confession of Faith and Catechisms,
documents that have been subscribed to by millions of Chris-
tians worldwide for nearly 350 years. Until at least these docu-
ments are wrestled with, would-be heresy hunters would do well
to choose another line of work. Until they do, however, we
Reconstructionists are compelled to defend ourselves. The
question then is: How? This is discussed in Chapter 13 by Gary
North.

38. Mid., p. 191.
