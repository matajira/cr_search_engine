54 CHRISTIAN RECONSTRUCTION

have in common? Obviously, the covenant. But what, precisely,
is the covenant? Is it the same in both Testaments (Covenants)?

He began rereading some books by Calvinist theologian
Meredith G. Kline. In several books (collections of essays), Kline
mentioned the structure of the Book of Deuteronomy. He ar-
gued that the book’s structure in fact parallels the ancient pa-
gan world’s special documents that are known as the suzerain
(king-vassal) treaties.

That triggered something in Sutton’s mind. Kline discusses
the outline of these treaties in several places, In some places, he
says they have five sections; in other places, he indicates that
they may have had six or even seven. It was all somewhat
vague. So Sutton sat down with Deuteronomy to sce what the
structure is. He found five parts.

Then he looked at othcr books of the Bible that are known
to be divided into five parts: Psalms and Matthew. He believed
that he found the same structure. Then he went to other books,
including some Pauline epistles. He found it there, too. When
he discussed his findings in a Wednesday evening Bible study,
David Chilton instantly recognized the same structure in the
Book of Revelation. He had been working on this manuscript
for well over a year, and he had it divided into four parts.
Immediately he went back to his computer and shifted around
the manuscript’s sections electronically. The results of his re-
structuring can be read in his marvelous commentary on Reve-
lation, The Days of Vengeance.*

Here, then, is the five-point structure of the biblical cove-
nant, as developed by Sutton in his excellent book, That You
May Prosper: Dominion By Covenant.‘

1. Transcendence/presence of God

2. Hicrarchy/authority/deliverance

3. Ethics/law/dominion

4, Oath/sanctions: blessings and cursings
5. Succession/inheritance/continuity

3. Ft. Worth, Texas: Dominion Press, 1987.
4. Tyler, Texas: Institute for Christian Economics, 1987.
