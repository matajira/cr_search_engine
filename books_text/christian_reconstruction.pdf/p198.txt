176 CHRISTIAN RECONSTRUCTION

Why Are We So Confident?

Because we have already won the intellectual batile. Our critics
waited too long to respond. When a movement has been given
time to get over a hundred books and scholarly journals into
print, it is too late for its critics to begin responding. The critics
now have too much to read, and they refuse to do the necessary
work. When the new movement has already made converts in
the major rival organizations, in some cases the next generation
of leaders, it is way too late. When the critics keep repeating as
the gospel truth both facts and opinions that have been refuted,
line by line, in previous Reconstructionist responses, usually in
book-long responses, the critics are admitting in public two
things: (1) they have no answers to our position and (2) they
have not bothered to read our detailed replies. This is the case
today. When the critics think they can defeat the new move-
ment by attacking the leaders’ styles or personalities rather than
their ideas, it is way, way too late. The main theological battle
is already over. We are now in the “mopping-up” phase.

There is something else. Our ideas are now in wide circula-
tion. They no longer depend on the skills or integrity of any
one person. We Christian Reconstructionists practice what we
preach. We are a decentralized movement. We cannot be taken
out by a successful attack on any one of our institutional strong-
holds or any one of our spokesmen. Our authors may come and
go (and have), but our basic worldview is now complete. We
have laid down the foundations of a paradigm shift.

Our critics are so deeply committed to the prevailing Estab-
lishment — political and religious pluralism — that when the
humanist order self-destructs, it wil] take down the critics. They
have ali built their institutions on the sand of humanist social
theory and humanist social order.” What today appears to be
their great advantage — their acceptance of and by the prevail-
ing social order — will become a distinct liability in and after a
major crisis. That crisis is coming, long before Jesus does.

23. North, Millennialism and Social Theory.
