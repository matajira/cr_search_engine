28 CHRISTIAN RECONSTRUCTION

political (true). Fourth, His kingdom therefore is not political
(true only if His kingdom is identical to His church).
Question: Is His kingdom identical with His church?

Protestants and Catholics

It always astounds me when IJ hear Protestants cite John
18:36 in order to defend a narrow definition of God’s kingdom
in history. Four centuries ago, this narrow definition was the
Roman Catholic view of the kingdom. Roman Catholics equated
the kingdom with the church, meaning the church of Rome.
The world is outside the church, they said, and it is therefore
doomed. The institutional church is all that matters as far as
eternity is concerned, they argued. The world was contrasted
with the kingdom (“church”), and the church could never en-
compass the world.

In sharp contrast, the Protestant Reformation was based on
the idea that the institudional church must be defined much more
narrowly than God's world-encompassing kingdom. Protestants
always argued that God’s kingdom is far wider in scope than
the institutional church. So, from the Protestant viewpoint:

1. The kingdom is more than the church.
2. The church is less than the kingdom.

The Protestant doctrine, “every man a priest” — as Protestant
an idea as there is — rests on the assumption that each Chris-
tian’s service is a holy calling, not just the ordained priest's
calling. Each Christian is supposed to serve as a full-time work-
er in God’s kingdom (Romans 12:1). What is this kingdom? £2 és
the whole world of Christian service, and not just the instivutional
church.

What we find today is that fundamentalist Protestants have
unknowingly adopted the older Roman Catholic view of church
and kingdom. Writes Peter Masters of Spurgeon’s Metropolitan
‘Tabernacle: “Reconstructionist writers all scorn the attitude of
traditional evangelicals who see the church as something so
completely distinct and separate from the world that they seek
