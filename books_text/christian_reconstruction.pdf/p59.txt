The Pietist-Humanist Kingdom 37

stand taken by church members in many of the satellite nations.
What are we to say, that the courageous stand taken by those
pastors and churches in several denominations was totally mis-
guided, “getting religion mixed up with politics,” and an im-
proper application of biblical principles to history? Rev. Laszlo
Tokes, the Hungarian pastor who sparked the Romanian Revo-
lution, “put God at the head of Europe’s liberation movement:
‘Eastern Europe is not just in a political revolution but a reli-
gious renaissance.’ ””

Yet this is what all those who oppose Christian Reconstruc-
tionism’s view of biblical responsibility do implicitly deny. They
have forgotten that righteous civil government is a legitimate means
of evangelism, a testimony to the spiritually lost of the greatness
of God’s deliverance in history:

Behold, I have taught you statutes and judgments, even as the
Lorp my God commanded me, that ye should do so in the land
whither ye go to possess it. Keep therefore and do them; for this is
your wisdom and your understanding in the sight of the nations,
which shall hear all these statutes, and say, Surely this great nation
is a wise and understanding people. For what nation is there so
great, who hath God so nigh unto them, as the Lorn our God is in
all things that we call upon him for? And what nation is there so
great, that hath statutes and judgments so righteous as all this law,
which I set before you this day? (Deuteronomy 4:5-8).

Contemporary Christianity has long forgotten about this
tradition of evangelism through law. It has therefore forfeited
politics to the enemies of Christ, especially the modern human-
ists, who see no salvation outside the true “church” of politics —
the same way that the residents of the ancient Greek polis (city-
state) viewed salvation. Contemporary humanists do understand
the implications of evangelism through law, and like the hu-
manists of ancient Rome, they despise it. It threatens their
control in history.

7. Barbara Reynolds, “Religion is Greatest Story Ever Missed,” USA Taday (March 16,
1990), p. 13A.
