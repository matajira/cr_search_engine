What Is Christian Reconstruction? 95

Tates necessary to sustain the quest for perfect justice.”

So then, the portrayal of Christian Reconstruction as wanting
to establish a centralized political order is incorrect. We teach
just the opposite. As I’ve shown, one does not need to search for
very long to find these views expressed in our writings. They
are prominent emphases.

28, Gary North, Moses and Pharaoh: Dominion Religion Versus Power Religion (Tyler,
TX: Institute for Christian Economics, 1985), pp. 211-12.
