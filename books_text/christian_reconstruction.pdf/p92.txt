Chapter 9

THE PIETIST-HUMANIST ALLIANCE

And they met Moses and Aaron, who stood in the way, as they came
forth from Pharaoh: And they said unto them, The Lorp look upon you,
and judge; because ye have made our savour to be abhorred in the eyes of
Pharaoh, and in the eyes of his servants, ta put a sword in their hand to
slay us (Exodus 5:26-21).

Premillennialists preach escape for today — escape from
involvement in politics, plus the inevitable cultural retreat and
defeat of the church on this side of the Second Coming. They
also preach the wonders of bureaucratic power for the far side
of the Second Coming. What they mean by today’s escape is
today’s subordination to the culture of “Egypt.”

They resent anyone who would make their humanist task-
masters angry.What frightens some of the dispensational critics
is their fear of persecution. David Allen Lewis warns in his
book, Prophecy 2000, that “as the secular, humanistic, demonic-
ally-dominated world system becomes more and more aware
that the Dominionists and Reconstructionists are a rea] political
threat, they will sponsor more and more concerted efforts to
destroy the Evangelical church. Unnecessary persecution could
be stirred up.”' In short, because politics is supposedly human-
istic by nature, any attempt by Christians to speak to political
issues as people — or worse, as a people — who possess an explic-
itly biblical agenda will invite “unnecessary persecution.”

1. Lewis, Prophecy 2000 (Green Forest, Arkansas: New Leaf Press, 1990), p. 27.
