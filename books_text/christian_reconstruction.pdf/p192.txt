170 CHRISTIAN RECONSTRUCTION

Was Lindsey upset? I think so. He indicated as much at the
convention, as did his outraged secretary. He had not under-
stood our mastery of the wonders of microcomputer technology.
More to the point, he had never taken on anyone in print with
academic training in his whole carecr. He simply cannot “duke
it out” intellectually with the big boys, which is why he refuses
to debate any Christian Reconstructionist in public. He got his
head handed to him, but only a few Reconstructionists know of
The Legacy of Hatred Continues. He prefers to keep his wounds
relatively private. (He may also suspect that the very first ques-
tion I would ask him in any public debate is this: “On what
legal basis did each of your wives gain her divorce?”)? Unlike
Dave Hunt, who really is a glutton for punishment, Hal Lindsey
knows that he was beaten, and beaten soundly. He has not writ-
ten a reply. He has not revised his book. If he does reply in
print, we will respond again, but in far greater detail. He knows
this. So far, he has confined his comments to local radio talk
shows, where we have difficulty calling in, although on one
occasion, Gary DeMar did get through. Was Lindsey surprised!
The show was being broadcast locally in Texas, and DeMar was
in Atlanta. (A local listener had tipped off DeMar.)

Here is my strategy at ICE. Every critic who writes a book-
length criticism of Christian Reconstruction gets a book-length
reply within six months, if his book was not self-published. This
is guaranteed. Our reply may be released within three months.
In Hal Lindsey’s case, it took only 30 days. If the critic replies
to us in another book, he will get another book-length res-
ponse. In the case of Westminstcr Theological Seminary’s re-
markably mediocre and unfocused book, which took the sixteen
authors about five years to get into print, we produced three

12. Some readers may wonder why I keep returning to this marital fact of Hal Lind-
sey’s life. I do so because he holds the public office of minister of the gospel. The Bible
is very clear about the requirements of this office: a man is not to serve as an elder unless
ac is the husband of one wife (1 Tim. 3:2). This does no? mean “one wife at a time.”
Unless he brought biblical grounds of divorce against his ex-wives, and then proved these
charges in a church court, Lindsey is an adulterer, according to Jesus (Matt. 5:31-32). But
adaltery has become a way of life in modern Christian circles, so my accusation is
shrugged off as bad etiquette on my part. It is in such a moral environment that a man
such as Lindsey can become a national spiritual leader.
