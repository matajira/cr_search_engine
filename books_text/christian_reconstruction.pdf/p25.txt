Introduction 3

was itself refuted in great detail by two prominent Reconstruc-
tionist authors.’ Hal Lindsey continued the attack with his
poorly researched and badly reasoned The Road to Holocaust.

Most critics of Christian Reconstruction do not read carefully
what we have written; even fewer acknowledge to their reader-
ship that comprehensive answers have been given to their at-
tacks. For example, Hal Lindsey's The Road to Holocaust levels a
charge of “anti-semitism” against Reconstructionists and all
other groups that do not espouse a dispensational premiliennial
eschatology. In my book The Debate over Christian Reconstruction,
two appendixes were included that answered this charge, one
written by a Reconstructionist who happens to be Jewish! Lind-
sey nowhere in his book mentions this material. The charge of
“anti-semitism” is a lie.* But it sells.

How to Play the Sensationalism Game

As Peter Leithart and I point out in The Legacy of Hatred Con-
tinues, and as I argue in Question 10 in Part II of this book, by
using a bit of “Lindsey-logic,” dispensational premillennialism
can be made to look “anti-semitic.” This is how it’s done. It
takes very little imagination. The more trusting one’s readers
are, the less imagination it takes. There are two targeted groups
of victims: one’s opponents and one’s overly trusting readers.
We begin with a factual historical statement regarding the post-
millennial view of the Jews. We begin with the truth.

Postmillennialists have always taught that the Jews have a promi-
nent place in God’s prophetic plan prior to the so-called “rapture”: a
great number of Jews will come to Christ before Jesus’ return. Long
before dispensationalismm came on the scene in the early nineteenth
century, the conversion of the Jews was a pillar of postmillennial
thought and still is. The Westminster Assembly's Directory for the Pub-

5. Greg L. Bahnsen and Kenneth L. Gentry, Jr, House Divided: The Break-Up of
Dispensational Theology (Tyler, TX: Institute for Christian Economics, 1989).

6. Steve Schlissel and David Brown, On Hal Lindsey and the Restoration of the Jews
(Edmonton, Alberta: Still Waters Revival Books, 1990). Steve Schiissel pastors Messiah's
Christian Reformed Church in Brooklyn, New York. Rev. Schlissel is a Jewish-born
Christian Reconstructionist.
