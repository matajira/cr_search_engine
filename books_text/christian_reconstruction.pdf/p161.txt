What Role Does Israel Play in Postmillennialism? 139

Jews.’ The Jews — especially the German Jews — were responsi-
ble for the great depression.”

Wilson maintains that it was the premillennial view of a
predicted Jewish persecution prior to the Second Coming that
Jed to a “hands off” policy when it came to speaking out against
virulent anti-Semitism. “For the premillenarian, the massacre of
Jewry expedited his blessed hope. Certainly he did not rejoice
over the Nazi holocaust, he just fatalistically observed it as a
‘sign of the times.’ ”!® Wilson offers this summary:

Pleas from Europe for assistance for Jewish refugees fell on deaf
ears, and “Hands Off” meant no helping hand. So in spite of being
theologically more pro-Jewish than any other Christian group, the
premillenarians also were apathetic — because of a residual anti-
Scmitism, because persecution was prophetically expected, because
it would encourage immigration to Palestine, because it seemed the
beginning of the Great Tribulation, and because it was a wonderful
sign of the imminent blessed hope.”

From a reading of Dwight Wilson’s material, the argument has
been turned on those dispensational premillennialists who have
charged non-dispensationalists with being “unconsciously” anti-
Semitic. The charge is preposterous, especially since postmilien-
nialists see a great conversion of the Jews to Christ prophesied
in the Bible prior to any such “Great Tribulation,” while dis-
pensationalism sees a great persecution yet to come where “two
thirds of the children of Israel in the land will perish” during
the “Great Tribulation.”*

18, Zbid., p. 95.
19. idem.
20. sbid., pp. 96-97. See Wilson's comments on page 217.
21. John F. Walvoord, Israel in Prophecy (Grand Rapids, MI: Zondervan/Academie,
[1962] 1988), p. 108.
