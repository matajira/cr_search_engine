Question 4

DO CHRISTIAN RECONSTRUCTIONISTS BELIEVE
THAT WE ARE SANCTIFIED BY THE LAW?

This question needs to be answered in a no/yes fashion. No,
Christians are not sanctified by the law if one means that the
law is added to faith to save someone (the Judaizing heresy). “I
do not nullify the grace of God; for if righteousness comes through
the Law, then Christ died needlessly” (Galatians 2:21). If there is
anything that man can do to merit or retain his salvation, then
there is room for boasting. The Bible says that rebellious sin-
ners do not even add faith; it too is a “gift of God” (Ephesians
2:8).

But now apart from the Law the righteousness of God has been
manifested, being witnessed by the Law and the Prophets, even the
righteousness of God through faith in Jesus Christ for all those who
believe; for there is ne distinction; for all have sinned and fall short
of the glory of God, being justified as a gift by His grace through
the redemption which is in Christ Jesus; whom God displayed pub-
licly as a propitiation in His blood through faith. This was to dem-
onstrate His righteousness, because in the forbearance of God He
passed over the sins previously committed; for the demonstration, I
say, of His righteousness at the present time, that He might be just
and the justifier of the one who has faith in Jesus. Where then is
boasting? It is excluded. By what kind of law? Of works? No, but by a law
of faith. For we maintain that a man is justified by faith apart from works
of the Law (Romans 3:21-28).

The Christian adds nothing to Jesus’ finished work on the
cross. Jesus paid it all. The debt is entirely the sinner’s. The
