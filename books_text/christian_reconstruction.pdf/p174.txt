152 CHRISTIAN RECONSTRUCTION

same can be said about laws governing property, contracts, and
criminal sexual practices like adultery, homosexuality, and
bestiality. All of the laws governing these areas are moral laws
having a tripartite application.

Since our task in this section is to deal with “historic Chris-
tianity,” we will not survey what the Bible says about the abid-
ing validity of God’s law. This topic has been dealt with else-
where in great detail.’

Asteady confirmation of the abiding validity of God's law can
be found with the earliest of the church fathers and continuing
to our day. “Recognizing the value of the Law of God was no
innovation by the Reformers, Irenaeus [c.175-c.195] had seen it;
Augustine [354-430] knew it well; the medieval schoolmen, of
whom Aquinas [1224-1274] was the best exponent, considered
at length the application of the Law to the Christian.”"

John Calvin's (1509-1564) exposition of the law and its appli-
cation to society, including the civil magistrate, is set forth in
comprehensive detail in his exposition of Deuteronomy 27 and
28, totalling two hundred sermons in all.

After all, in reforming the city of Geneva, Calvin did not deliver
two hundred lectures on common grace or natural law, but
preached two hundred sermons on the book of Deuteronomy. He
made full and direct applications from Deuteronomy into his mod-
ern situation, without apology. He viewed Biblical law as founda-
tional and as the starting point for legal and socio-political reflec-
tion.”

The American Puritans, following in the tradition of Calvin,
believed that it was possible to govern a modern commonwealth
by the laws set forth in all of Scripture. For example, the “Puri-
tans resolved to rule the [Massachusetts] Bay Colony with a
strong hand but with a Christian heart.”!* This meant that the

10. See “Books for Further Reading and Study,” below.

11, Geoffrey H. Greenhough, “The Reformers’ Attitude to the Law of God,” The
Westminster Theological Journal $9:1 (Fall 1976), p. 81.

12, James B. Jordan, “Editor's Introduction,” The Covenant Enforced: Sermons on
Deuteronomy 27 and 28 (Tyler, TX: Institute for Christian Economics, 1990), p. xxxiii.

13. Edwin Powers, Crime and Punishment in Early Massachusetts, 1620-1692 (Boston,
