124 CHRISTIAN RECONSTRUCTION

“The State is a bankrupt institution,” he added. He admitted
that Christian Reconstruction will not work unless “you have
the vast majority of the people believing that this is the way
things should be.” (Democracy?) He told Moyers that he did not
want Amcrica to be declared officially Christian, since “nothing
official means anything unless it is personal with the people.”*

Rushdoony didn’t just stumble on this theme, either. For
years, he has been warning about an overestimation of the
power of politics. Rushdoony was warning about “messianic
politics” and the “messianic state” before Colson ever became a
Christian, In The Politics of Guilt and Pity, first published in
1970, Rushdoony described the implications of the anti-Chris-
tian idea that man is “over law.” One of the consequences is
that

Man finds salvation through political programs, through legislation,
so thal salvation ts an enactment of the state... . As a saving order, the
state concerns itself with cradle-to-grave security, because, as the
expression of man’s divinity, it must play the role of god in man’s
life. It becomes man’s savior.*

More recently, Rushdoony has noted that the “ancient, classical
view of man ... is a fertile ground for a radical statism and a
radical anarchism.” Because this view exalts man to the place of
God, “[iJn its totalitarian form, it offers us a savior state as
man’s hope.”* Notice that Rushdoony is arguing that a “messi-
anic” view of politics is essentially anti-Christian and humanis-
tic.

Gary North has emphasized the same point. In his book,
Political Polytheism, North wrote:

Every revolution needs slogans. Here is mine: politics fourth. First
comes personal faith in Jesus Christ as Lord and Savior (not just
Savior). Second comes Church renewal. There can be no successful

3. “God and Politics: On Earth As It Is In Heaven,” page 4 of transcript.

4, Rousas J. Rushdoony, Politics of Guilt and Pity (Fairfax, VA: Thoburn Press, [1970]
1978), p. 145.

5. Rousas John Rushdoony, Christianity and the State (Vallecito, CA: Ross House
Books, 1986), p. 17.
