Premillennialism’s Faith in Bureaucracy 67

This means that nothing positive that Christians do today
will survive the Great Tribulation. All our good works will
inevitably be destroyed, either pre-tribulationally (historic pre-
millennialism) or post-tribulationally (conventional dispensa-
tionalism). There will be no: institutional continuity between
today’s church with the church of the future millennium. This is
@ denial of history, for “history” in premillennial theology gets
broken; there can be no historical continuity with the millenni-
um. The Great Tribulation will intervene.’ Everything Chris-
tians leave behind will be swallowed up. This is great news, not
tribulation news, Dave Hunt tells us.

While the Rapture is similar to death in that both serve to end
one’s earthly life, the Rapture does something else as well: it signals
the climax of history and opens the curtain upon its final drama. It
thus ends, in a way that death does not, all human stake in continu-
ing earthly developments, such as the lives of the children left
behind, the growth of or the dispersion of the fortune accumulated,
the protection of one’s reputation, the success of whatever earthly
causes one has espoused, and so forth.*

This is why premillennialism is inherently, incscapably pessimis-
tic with regard to efforts of social reform.

Only Christ’s millennial bureaucracy can bring peace and
freedom in history, we are told. We Reconstructionists ask: How
can any bureaucracy make men or societies righteous? How can
a top-down bureaucratic order change the nature of man? It
cannot. Premillennialists would obviously admit this. So, the
premillennialist is left with this defense of the millennial bu-
reaucracy: men will be so afraid of the consequences of doing
evil that they will obey God's law. (Question: Did Adam obey?)

{Side question: Where will people learn about the details of
this Kingdom law? The answer is obvious — in the Old Testa-
ment, just as the Reconstructionists assert — but this answer
sounds so theonomic that they never mention it.)

3. The Great Tribulation actually took place at the fall of Jerusalem in A.D. 70. See
David Chilton, The Great Tribulation (Ft. Worth, Texas: Dominion Press, 1987).
4. Dave Hunt, “Looking for that Blessed Hope,” Omega Letter (Feb. 1989), p. 14.
