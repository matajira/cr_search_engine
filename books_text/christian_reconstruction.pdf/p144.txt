122 CHRISTIAN RECONSTRUCTION

treatment of both the citizen and the “stranger,” the majority
and the minority, because God has given one law for the people
(e.g., Exodus 22:21; 23:9).

Finally, as Gary North has recently pointed out, “Christian
reconstruction depends on majority rule.”” God uses lawful historical
means to extend His earthly kingdom. Reconstructionists thus
affirm that God’s laws should be passed and enforced according
to the rules of the democratic process. Reconstructionists do not
preach revolution or a top-down bureaucratic take-over. But
Reconstructionists also do not believe that the will of the politi-
cal majority is the final Jaw in society, If this were the final law,
then the will of the political majority would be the will of God.
The democratic majority would then be God. What Christian
could believe such a doctrine?

7. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute for
Christian Economics, 1989), p. 586.
