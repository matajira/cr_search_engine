118 CHRISTIAN RECONSTRUCTION

moral content of these two revelations, written and natural. The
written law is an advantage over natural revelation because the
latter is suppressed and distorted in unrighteousness (Romans
1:18-25). But what pagans suppress is precisely the “work of the law”
(2:14-15). Natural revelation communicates to them, as Paul says,
“the ordinance of God” about “aif unrighteousness” (1:29, 32). Be-
cause they “know” God’s ordinance, they are “without excuse” for
refusing to live in terms of it (1:20), What the law speaks, then, it
speaks “in order that ail the world may be brought under the judg-
ment of God” (3:19). There is one law order to which all men are
bound, whether they earn of it by means of natural revelation or by
means of special revelation. God is no respecter of persons here
(2:11). “All have sinned” (3:23) -- thus violated that common standard
for the “knowledge of sin” in all men, the law of God (3:20).

Reconstructionists take God’s revelation seriously: the law of
God found in both Testaments and general revelation.

Did God, as Geisler maintains, place only the Israelites under
obligation to the moral demands of those commandments speci-
fically delivered to the nation through Moses? Are Gentile na-
tions ever condemned for violating laws specifically given to
Israel? If we can find just one law that fits into this category,
then all nations are obligated to submit to God’s special written
revelation, the Bible. I will summarize the argument for you:

God gives a series of instructions to Moses for the people: “You
shall not do what is done in the land of Egypt where you lived, nor
are you to do what is done in the land of Canaan where I am bring-
ing you; you shall not walk in their statutes. You are to perform my
judgments and keep My statutes, to live in accord with them” (Levit-
icus 18:3-4). God then issues a list of Canaanite practices that were
prohibited. He commands the Israelites not to engage in incest,
polygamy, adultery, child sacrifice, profaning Jehovah’s name,
homosexuality, or bestiality (vv. 6-23). The Mosaic law outlawed all
such behavior and severely punished it. Immediately following the
long list of prohibitions, God’s word describes what disobedience will
bring: “Do not defile yourselves by any of these things; for by all
these the nations which I am casting out before you have become
defiled. For the land has become defiled, therefore I have visited its

26. Greg L. Bahnsen, "What Kind of Morality Should We Legislate?,” The Biblical
Worldview (October 1988), p. 9.
