192 CHRISTIAN RECONSTRUCTION

Theonomic Studies in Biblical Law

Bahnsen, Greg L. By This Standard: The Authority of God’s Law
Today. Tyler, TX: Institute for Christian Economics, 1985. An
introduction to the issues of biblical law in society.

Bahnsen, Greg L. Theonomy and Its Critics. Tyler, TX: Insti-
tute for Christian Economics, 1991. A detailed response to the
major criticisms of theonomy, focusing on Theonomy: A Reformed
Cnitique, a collection of essays written by the faculty of Westmin-
ster Theological Seminary (Zondervan/Academie, 1990).

Bahnsen, Greg L. Theonomy in Christian Ethics. Nutley, New
Jersey: Presbyterian and Reformed, (1977) 1984. A detailed
apologetic of the idea of continuity in biblical law.

DeMar, Gary. God and Government, 3 vols. Brentwood, Ten-
nessee: Wolgemuth & Hyatt, 1990. An introduction to the fun-
damentals of biblical government, emphasizing self-government.

Jordan, James. The Law of the Covenant: An Exposition of Exo-
dus 21-23. Tyler, TX: Institute for Christian Economics, 1984.
A clear introduction to the issues of the case laws of the Old
Testament.

North, Gary. The Dominion Covenant: Genesis. Tyler, TX:
Institute for Christian Economics, (1982) 1987. A study of the
economic laws of the Book of Genesis.

North, Gary. Moses and Pharaoh: Dominion Religion vs. Power
Religion. Tyler, TX: Institute for Christian Economics, 1985. A

study of the economic issues governing the Exodus.

North, Gary. Political Polytheism: The Myth of Pluralism. Tyler,
