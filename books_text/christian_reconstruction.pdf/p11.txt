Preface xi

John Murray’s lectures on Romans 11 converted me to postmil-
lennialism. (1 became a Presbyterian afier reading Meredith
Kline’s 1964-65 Westminster Theological Journal essays on baptism
that later became By Oath Consigned.)

In 1962, there was no Christian Reconstruction movement.
There was not even an outline of it. Over the next decade,
Rushdoony developed the fundamental theological and socio-
logical principles of what was later to become a movement. I
did sporadic work on biblical economics after 1964. He per-
suaded Hays Craig of Craig Press to publish my Marx’s Religion
of Revolution (1968). He put me on a part-time salary in 1970
($300 a month) to help me complete my Ph.D. By then, I was
writing almost every other month for The Freeman, and I was
hired by Leonard E. Read in the fall of 1971 to join the senior
staff of the Foundation for Economic Education. I completed
my doctoral dissertation while on the FEE staff.

Rushdoony had been deeply influenced by Van Til, whose
dual classification of covenant-keeper and covenant-breaker had per-
suaded him of the irreconcilable nature of Christianity and its
tivals. Rushdoony wrote By What Standard, published in 1959, as
an introduction to Van Til’s uncompromising rejection of hu-
manism. Like Van Til, Rushdoony believed that Christians need
to abandon all traces of natural law theory. But this radical
belief inevitably creates a monumental sociological problem for
Christianity — a problem that Van Til never addressed publicly
in his career:

“If Not Natural Law, Then What?”

Van Til was analogous to a demolitions expert. He placed
explosive charges at the base of every modern edifice he could
locate, and book by book, syllabus by syllabus, he detonated
them. One by one, the buildings came down. But he left no
blueprints for the reconstruction of society. He saw his job as
narrowly negative: blowing up dangerous buildings with their
weak (schizophrenic) foundations. This narrowly defined task
was not good enough for Rushdoony. He recognized early that
there has to be an alternative to the collapsed buildings. There
have to be blueprints. But where are they to be found? Step by
