Introduction 17

truly love Him we will keep His commandments (John 14:15).
Paul writes that loving one’s neighbor is the fulfillment of the
law. But Paul does not allow us to define love in our own way.
He cites the law: “You shall not commit adultery (Exodus 20:-
14], You shall not murder [20:13], You shall not steal, You shall
not covet [20:17]” (Romans 13:9).

My files are filled with numerous other attcmpts at refutation
of the distinctives known as Christian Reconstruction. None of
the critics offers what I would call a comprehensive, worked-out
alternative social theory. The closest anyone comes is Norman
L. Geisler. His answer is a variant of Greek natural law theory.
Geisler maintains that the Bible cannot and should not be used
by the nations as a blueprint for living.**

Others such as Charles Colson are a bit schizophrenic in their
rejection of Christian Reconstruction. For example, Colson says
that Reconstructionists are wrong in their application of the
Mosaic law to contemporary society. But it is Colson who turns
to the Mosaic law (Exodus 22) as the only hope to resolve our
nation’s crime problem. He asserts that the only solution to the
crime problem is “to take nonviolent criminals out of our pris-
ons and make them pay back their victims with restitution. This
is how we can solve the prison crowding problem.”” Richard
Chewning, a sometime critic of Christian Reconstruction, dis-
cusses some of the advantages of a seven-year limit on debt as
set forth in Deuteronomy 15:1-5. Why can Colson and Chew-
ning legitimately go to the Old Testament for instruction while
Reconstructionists cannot? Is it because these men pick and
choose what they like and ignore what they don’t like? This is
convenient, but it is hardly a biblical approach.

The debate is over how the Christian should live and by what
standard he should govern his life. Should the Christian turn to

41. Norman L. Geisler, “Natural Law and Business Ethics,” Biblical Principles and
Business: The Foundations, ed. Richard C. Chewning (Colorado Springs, CO: NavPress,
1989), pp. 157-78.
42. Charles Colson, “The Kingdom of God and Human Kingdoms,” Transforming Our
World: A Call to Action, ed, James M. Boice (Portland, OR: Multnomah, 1988), pp. 154-55.
43. Richard C. Chewning, “Editor's Perspective,” Biblical Principles &? Business: The
Practice (Colorado Springs, CO: NavPress, 1990), pp. 247-48.
