Conclusion 185

watched a Madonna concert originating from France and tele-
cast on HBO.

What is amazing is not what came across the screen. We live in
an anything-goes age, and to say the concert was shocking would be
incorrect, because society today is basically unshockable. No, the
show’s content, although witless and purposely vulgar, was not the
surprising thing. The surprising thing was that an insignificant
number of those parents called HBO to object to what was shown.
Apparently the parents of America have totally given up on hoping
that they can control the entertainment environment their children
are exposed to.

But it’s far worse than parents allowing their children to watch
a Madonna concert on HBO. “It’s about a country that has been
so beaten down by a lessening of standards for what is accept-
able in public and what isn’t that something like the Madonna
concert can be telecast to millions of families, and it doesn’t
even cause a ripple of controversy or complaint.”* The citizenry
has been propagandized into believing that morals are solely a
personal matter. What used to be consider gross evils are now
accepted as legitimate alternative lifestyles that ought to be
sanctioned by law.

The legalization of abortion.

The decriminalization of homosexuality.

Self-professed homosexuals running for political office
and winning.

Churches ordaining homosexuals.

The abolition of the Christian religion from public
schools and nearly every vestige of American life.

Pornographic displays of so-called “homoerotic art” paid
for by tax dollars.

The rewriting of textbooks to teach that capitalism
and communism are legitimate economic options for nations.

In addition, there is so much anti-Christian bigotry and de-

2. Bob Greene, “Madonna Concert Shows What We've Become,” Marietta Daily
Journal (August 15, 1990), p. 7A.
3. Idem.
