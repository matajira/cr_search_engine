Preface xv

and Tithing and Dominion, co-authored by Edward Powell. In the
1980's, a few of the books that he had written for the most part
around 1973 begin to appear in print: The Philosophy of the
Christian Curriculum (1981), Law and Society (1982), and Salvation
and Godly Rule (1983).° These books are more popular in tone,
with very short chapters and fewer foomote references. Rush-
doony's goal had visibly shifted: presenting the case for Chris-
tian Reconstruction to a new audience — wider, but less theolog-
ically rigorous. The new books now came from other places.

The 1980’s: High Gear Book Production in Tyler and Atlanta

In 1981, with Remnant Review and the ICE on their feet, I
began to write in earnest. This began with Unconditional Surren-
der (1981), and The Dominion Covenant: Genesis followed the next
year. ICE newsletters also multiplied. I began to recruit youn-
ger men to write books that could not be published through the
conventional Christian book industry.

At the same time, Gary DeMar went to work for American
Vision. He was hired to write what became the three-volume
set, God and Government, By the mid-1980’s, the board of Ameri-
can Vision decided to replace its founder with DeMar. At about
that time, George Grant appeared on the scene: Bringing in the
Sheaves, published originally by American Vision.

The ten-volume Biblical Blueprints series followed in 1986-
87, with the first four volumes co-published by Thomas Nelson
Sons, but abandoned soon thereafter. Dominion Press took over
exclusive rights in late 1987. This series was a self-conscious
attempt to prove that the thesis of Christian Reconstruction is
correct; biblical law does apply to real-world situations: econom-
ics, education, civil government, welfare, politics, foreign policy,
and family relations. We initially intended to write these books
for high school students, but as it turned out, they are more
geared to college students. But they are relatively easy to read.
Over half of them are structured by the Bible’s five-point cove-

6, As is true of most authors who comment on contemporary events, you can deter-
mine the original date of authorship by checking the latest dates in the books and
newspaper articles cited in the footnotes.
