xx CHRISTIAN RECONSTRUCTION

is Lord of heaven and earth, dwelleth not in temples made with
hands; Neither is worshipped with men’s hands, as though he
needed any thing, seeing he giveth to ali life, and breath, and
all things; And hath made of one blood all nations of men for to
dwell on all the face of the earth, and hath determined the
times before appointed, and the bounds of their habitation”
(Acts 17:24-26). We are all born as God’s disinherited children.

Christian Reconstructionists insist that there is no common
ground among men other than this: the image of God. While
all men know the werk of the jaw (Romans 2:15), this know-
ledge is not enough to save them.? It brings them under God’s
eternal wrath. They hinder in unrighteousness whatever truth
they possess as men (Romans 1:18). The more consistent they
are with their covenant-breaking presuppositions, the more they
hate God's law and those who preach it. The more consistent
they become with their rebellious view of God, man, law, and
time, the more perverse they become. They prefer to worship
creatures rather than the Creator:

For this cause God gave them up unto vile affections: for even
their women did change the natural use into that which is against
nature: And likewise also the men, leaving the natural use of the
woman, burned in their fust one toward another; men with men
working that which is unseemly, and receiving in themselves that
recompence of their error which was meet. And even as they did
not like to retain God in their knowledge, God gave them over to a
reprobate mind, to do those things which are not convenient; Being
filled with all unrighteousness, fornication, wickedness, covetousness,
maliciousness; full of envy, murder, debate, deceit, malignity; whis-
perers, Backbiters, haters of God, despiteful, proud, boasters, inven-
tors of evil things, disobedient to parents, Without understanding,
covenantbreakers, without natural affection, implacable, unmerciful:
Who knowing the judgment of God, that they which commit such
things are worthy of death, not only do the same, but have pleasure
in them that do them (Romans 1:26-32).

This means that natural law theory is a myth, the creation of
Hellenistic Greek philosophers to offer hope in a world in

9. Ibid., I, pp. 74-76.
