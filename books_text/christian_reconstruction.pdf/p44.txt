22 CHRISTIAN RECONSTRUCTION

Lau more than he quotes any other single author, including
himself.** Even some dispensational writers cannot avoid Re-
constructionist distinctives. J. Kerby Anderson edited a series of
articles written by Christian leaders presently or formerly affili-
ated with Dallas Theological Seminary.” While the articles are
a mixed bag, some of them would fit quite easily into the Re-
constructionist mold. One article in particular caught my atten-
tion: “The Purpose of Penology in the Mosaic Law and Today.”
Keep in mind that the title of the book is Living Ethically in the
90s, not Living Ethically under the Old Covenant 3,400 Years
Ago. The author, Gary R. Williams, writes:

It is the purpose of this chapter to compare the objectives of mod-
ern penology with those of the God-given Mosaic Law in order to
shed some light on the direction in which penological philosophy
should be moving to solve some of its current conundrums.”

His article could have been written by a Reconstructionist. Since
it was written by a dispensationalist, no one bats an eye, except
maybe those authors in this same book who espouse a different
position. I have in mind Robert P. Lightner’s “Theonomy and
Dispensationalism” and Norman L. Geisler’s “A Dispensational
Premillennial View of Law and Government.”

Conclusion

Being a good critic of contemporary religious movements is
a risky business, especially a “movement” like Christian Recon-
struction. For example, the sheer volume of material put out by
Reconstructionists necessitates at least a few years of study and
interviews. There are additional pitfalls in being a critic. Since
a number of people have taken it upon themselves to be judges
of Christian Reconstruction, they should be reminded what

54. Walter C. Kaiser, Jv., ward Old Testament Ethics (Grand Rapids, MI: Zondervan,
1983), pp. 73, 88, 117, 144, 145, 147, 148, 149, 154, 155, 157, 164, 165, 169, 196, 198,
199, 213, 229, 231, 256.

55. J. Kerby Anderson, ed., Living Ethically in the '90s (Wheaton, IL: Victor Books,
1990).

56, Tbid., p. 124.
