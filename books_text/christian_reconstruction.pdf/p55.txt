Chapter 2

THE PIETIST-HUMANIST KINGDOM

Jesus answered, “My kingdom is not of this world. If My kingdom were
of this world, My servants would fight, so that I should not be delivered to
the Jews; but now My kingdom is not from here” (John 18:36; New King
James Version).

The standard, run-of-the-mill response to the message of
Christian political responsibility rests on a faulty reading of
Jesus’ words to Pilate regarding His kingdom. What was really
involved? Jesus was explaining to Pontius Pilate how He could
be a king, yet also be standing before Pilate to be judged. Pilate
was implicitly asking: How could Jesus be a king? Where were
His defenders? Where were His troops?

Jesus’ point was clear: the source of His kingly authority is
not earthly. His kingdom is not of this world. The source of His
authority as king is from far above this world. His is a transcen-
dent kingdom.

Nevertheless, this kingdom also has earthly manifestations.
Rahab, a former pagan harlot, understood this much, for she
confessed to the Hebrew spies, “The Lord your God, He is God
in heaven above and on earth beneath” (Joshua 2:11b). She
understood that God’s kingdom was about to replace Jericho's
kingdom. She understood that God’s kingdom does have earth-
ly manifestations politically. There are millions of Christians
today who have yet to come to grips with Rahab’s confession of
faith.

At the time of His crucifixion, Jesus said that His kingdom
