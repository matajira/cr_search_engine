60 CHRISTIAN RECONSTRUCTION

Christ’s kingdom cannot and will not come until He returns
physically to reign over saints and sinners during a thousand-
year future period. This is not what He said about the arrival of
His kingdom; it is already here: “But if I cast out devils by the
Spirit of God, then the kingdom of God is come unto you”
(Matthew 12:28). He cast them out, and His kingdom came —
not in its historic fullness, but it came judicially (by law).

It is today the God-assigned task of all Christians to work out
in history the fullness of His kingdom’s principles, to manifest
it to all the world. This is the Great Commission (Matthew
28:18-20). The Holy Spirit enables Christ’s people to achieve
this goal progressively in history, before Jesus comes again to
judge the world.

It is no more necessary that Jesus sit on a physical throne in
a literal Jerusalem in order for His kingdom on earth to be
present in history than it is for Satan physically to sit on a
throne in some earthly location — New York City, perhaps? — in
order for his kingdom to be present in history. No Christian
teaches that Satan’s kingdom is unreal in history just because
Satan is not here on earth physically. Yet al! premillennialists
are forced by their eschatology to insist that Christ's earthly
Kingdom is not yet real in history, since He is on His throne in
heaven. This is totally inconsistent with what they believe about
Satan’s kingdom.

Empowering by the Holy Spirit
Because Jesus Christ is not physically present with His

church in history, the church has greater power. This was the
explicit teaching of Christ:

These things have I spoken unto you, being yet present with
you, But the Comforter, which is the Holy Ghost, whom the Father
will send in my name, he shall teach you all things, and bring all
things to your remembrance, whatsoever I have said unto you.
Peace I leave with you, my peace I give unto you: not as the world
giveth, give I unto you. Let not your heart be troubled, neither let
it be afraid (John 14:25-27).

Jesus made it plain that His disciples were better off with Him
