Books For Further Reading and Study 197

logical study of the implications of postmillennialism for eco-
nomics, law, and reconstruction.

Rushdoony, Rousas John. Thy Kingdom Come: Studies in Daniel
and Revelation. Phillipsburg, NJ: Presbyterian and Reformed,
1970. Exegetical studies in Daniel and Revelation, full of in-
sightful comments on history and society.

Shedd, W. G. T. Dogmatic Theology. 3 volumes. Nashville, TN:
Thomas Nelson, (1888) 1980. Nineteenth-century Reformed
systematics text,

Strong, A. H. Systematic Theology. Baptist postmillennialist of
late nineteenth and early twentieth centuries.

Sutton, Ray R. “Covenantal Postmillennialism,” Covenant
Renewal (February 1989). Discusses the difference between tradi-
tional Presbyterian postmillennialism and covenanta!l postmil-
Jennialism.

Terry, Milton S. Biblical Apocalyptics: A Study of the Most Nota-
ble Revelations of God and of Christ. Grand Rapids, MI: Baker,
(1898) 1988. Nineteenth-century exegetical studies of prophetic
passages in Old and New Testaments; includes a complete com-
mentary on Revelation.

Postmillennialism and the Jews

De Jong, J. A. As the Waters Cover the Sea: Millennial Expecta-
tions in the Rise of Anglo-American Missions 1640-1810. Kampen:
Jj. H. Kok, 1970. General history of millennial views;
throughout mentions the importance of prophecies concerning
the Jews.
