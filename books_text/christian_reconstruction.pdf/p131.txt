What Is the Legal Standard for the Nations? 109

work in a Christian context, where people generally understand
(1) that rebellious man’s autonomous reason should not be
trusted, and (2) that there are certain absolute values. In non-
Christian cultures, righteous natural law is an impossibility. The
reason? As the late Chief Justice Fred M. Vinson declared, in
expressing the implication of a consistent evolutionary theory of
law and justice, “Nothing is more certain in modern society
than the principle that there are no absolutes. . . .”* Natural
law depends on an existing theological framework that takes
into account God's sovereignty and ethical absolutes.

In addition there are several other problems with a natural
law ethical position. First, how does one determine what laws
found in general revelation are natural laws that conform to
God's will? Is it possible that Christian natural law advocates are
using the Bible as a grid in the construction of their natural law
ethic? But what grid is being used by non-Christians? In a
consistently evolutionary system, there can be no natural law,
only evolving law determined by those presently in power,
usually the State,

Second, how do we get people to agree on the content of
“natural Jaw” and how these laws should apply? Do we opt for
a lowest common denominator type of law like “Do good to all
men”? Should we agree that murder is wrong but not war and
capital punishment since each of these would violate the general
law of “do good to all men”? Does natural law, for example, tell
us that abortion is wrong?

Third, what if we find a common set of laws in “nature” that
contradict the Bible? As we will see below, polygamy can be
supported as a natural law ethic, as can slavery, since most
nations from time immemorial have practiced both. Would we,
if we followed natural law, give up monogamy for polygamy?

3.. Dennis v, United States, $41 U.S. 494 (1951) at 508 in Eugene C. Gerhart, American
Liberty and “Natural Law” (Boston, MA: The Beacon Press, 1953), p. 17. Time magazine
commented (July 23, 1951, pp. 67-68): “Whatever the explanation, Kentuckian Vinson's
aside on morals drew no dissent from his brethren on the supreme bench. And no
wonder. The doctrine he pronounced stems straight from the late Oliver Wendell
Holmes, philosophical father of the present Supreme Court.” Quoted in ibid., p. 165,
note 2.
