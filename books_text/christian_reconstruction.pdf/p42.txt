20 CHRISTIAN RECONSTRUCTION

ses}, Rousas John Rushdoony, Instituées fof Biblical Law]; Gordon
Wenham, The Book of Leviticus (Grand Rapids, MI: Eerdmans, 1979).
The conclusions of these books are not always consistent with one
another, and the authors’ exegesis is of uneven quality. But they are
attempting to do what most needs to be done in this area.”

Not all of the writers listed in the above quotation would call
themselves Reconstructionists, but they are wrestling with the
same issues. While a dispensationalist would dismiss the Mosaic
law in its entirety — it’s Jewish law — theologians guided by the
distinctives of the Reformation believe that all of God’s law,
even those laws given specifically to Israel, have some applica-
tion for today.

In the same issue of The Westminster Theological Journal, anoth-
er author acknowledges the contributions made by Reconstruc-
tionists in the area of biblical ethics.

One positive contribution of theonomy* is a renewed interest in
the validity of God’s law as an ethical standard. The question of the
continuity of the Mosaic law as a binding code for Christians is re-
ceiving attention from a growing segment of the evangelical commu-
nity, This increased concern for God’s revealed moral standards is a
healthy sign, much in need during these times.”

John Jefferson Davis acknowledges that the “theonomy mov-
ement has generated a good deal of discussion and controversy”
and that “some of the initial criticism appears based on a mis-
reading of the true intent of the position.” He specifically
mentions the charge of “legalism” as a common misrepresenta-
tion. Davis lists a number of positive contributions: “Theonomy
certainly represents a significant attempt in the contemporary

47. John M. Frame, “Toward a Theology of the State,” The Westminster Theological
Journal, 51:2 (Fall 1989), p. 204, note 21.

48. “Theonomy" (God's law) is often used as a synonym for Christian Reconstruction.
In reality, however, theonomy, the application of God's law to all aspects of society, is
simply one pillar of the system.

49. Douglas O. Oss, “The Influence of Hermeneutical Frameworks in the Theonomy
Debate,” ibid., p. 228.

50. John Jefferson Davis, Foundations of Evangelical Theology (Grand Rapids, MI: Baker
Book House, 1984), p. 267.
