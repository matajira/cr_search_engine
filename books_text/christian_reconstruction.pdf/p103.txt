Question No. 1

WHAT IS CHRISTIAN RECONSTRUCTION?

And they shall build the old wastes, they shall raise up the former desola-
tions, and they shall repair the waste cities, the desolations of many gener-
ations. And strangers shall stand and feed your flocks, and the sons of the
alien shall be your plowmen and your vinedressers. But ye shail be named the
Priests of the Lorp: men shall call you the Ministers of our God: ye shall eat
the riches of the Gentiles, and in their glory shall ye boast yourselves (Isaiah
61:4-6; King James Version).

Christian Reconstruction, unlike Christian “movements” in

general, has no central director, no overall, tightly controlled
strategy. What unites Reconstructionists is their commitment to

certain distinctive biblical doctrines that are fundamental to the

Christian faith and have been supported by the church for
centuries. In particular, Reconstructionists espouse the following
distinctives:

1. Regeneration — salvation by grace through faith — is man’s only
hope both in this age and in the age to come. Only new men who
reflect the image of God in Christ can bring about any significant
social change since social change follows personal change, and person-
al change can only come through regeneration. God’s sovereignty as
it relates to personal salvation and limited institutional authority is
foundational for the salvation of man and the abolition of tyranny.

2. The continuing validity and applicability of the whole law of God,
including, but not limited to, the Mosaic case laws is the standard by
which individuals, families, churches, and civil governments should
conduct their affairs.
