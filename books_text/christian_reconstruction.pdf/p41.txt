Introduction 19

“Conservatives” and “moderates” within the SBC part paths essen-
tially on their fundamental views of Scripture. Both sides believe the
Bible as originally written was infallible insofar as it tells the story of
salvation. Conservatives maintain additionally that the original auto-
graphs were inerrant in all matters, including history and science.”

If conservatives want to maintain that the Bible is authoritative
“in all matters, including history and science,” then why not
economics, law, and civil government? All talk about an iner-
rant and infallible Bible goes down the drain if these so-called
secular disciplines are categorized along with the liberal assess-
ment of “history and science.”

5. Honest Disagreement but Appreciation and Benefit

A fifth category of criticisms has done a fair job in evaluating
Christian Reconstruction and has found the position helpful
and insightful, although there are still disagreements on a num-
ber of issues. Kcep in mind, however, that no two theologians
agree on every point of any doctrine. This is why we have Bap-
tists, Presbyterians, Methodists, Pentecostals, and numerous
other theological traditions.

John Frame, a professor at Westminster Theological Semi-
nary in Escondido, California, writes that it is “necessary for us
to do careful exegetical study in the law itself in order to deter-
mine its applications, rather than simply trying to make deduc-
tions from broad theological principles.” He writes the follow-
ing about Reconstructionists and others who are attempting to
do the necessary exegetical and applicational work:

A number of writers have made good beginnings in this direction.
Sce James B. Jordan, The Law of the Covenant (Tyler, Texas: Institute
for Christian Economics, 1984); Gary North, Economic Commentary on
the Bible, so far in three volumes: The Dominion Covenant: Genesis;
Moses and Pharaoh; and The Sinai Strategy [and the most recent Tools
of Dominion} (Tyler, Texas: Institute for Christian Economics, 1982,
1985, 1986, [1990]); [Vern Poythress], Understanding [the Law of Mo-

46. "Southern Baptist Schism: Just a Matter of Time?,” Christianity Today (May 14,
1990), p. 47.
