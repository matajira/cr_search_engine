Question 2

DO RECONSTRUCTIONISTS BELIEVE THAT
CHRISTIANS WILL BRING IN THE KINGDOM OF
GOD IN HISTORY?

Anyone familiar with the historic millennial positions (amil-
lennialism, covenantal premillcnnialism, and postmillennialism)
knows that each view teaches that the kingdom has come in some
form and that it will be consummated only at Jesus’ final com-
ing when He delivers up the kingdom to His Father (1 Corin-
thians 15:23-24).

This “already—not yct” view of the kingdom is biblically
sound and has been defended by numerous Bible-believing
scholars from various millennial perspectives. Even dispensa-
tionalists are conceding that the kingdom has come in some
way.

The basic distinction here among dispensationalists is that older
ones tended to see the kingdom relegated entirely to the future.
More contemporary dispensationalists hold that the full realization
of the kingdom for Israel and the world awaits the future, but cer-
tainly spiritual aspects of the kingdom arc operational in the
church."

The Bible teaches the nearness of the kingdom in Jesus’ day.
This was the message of John the Baptist and Jesus (Matthew
3:2; 4:17, 23; Mark 1:14-15; Luke 4:16-30; 4:43; 8:1; 10:9;
Colossians 1:13). The kingdom was also manifested through the

1. John 8. Feinberg, ed., “Systems of Discontinuity,” Continuity and Discontinuity
(Westchester, IL: Crossway Books, 1988), p. 82
