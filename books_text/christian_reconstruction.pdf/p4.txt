Copyright, Gary North and Gary DeMar, 1991

 

Library of Congress Cataloging-in-Publication Data

North, Gary.
Christian Reconstruction : what it is, what it isn’t / Gary North and
Gary DeMar.
. em.
Includes bibliographical references and indexes,
ISBN 0-930464-52-4 : $25.00 (alk. paper) -- ISBN 0-930464-53-2
(pbk.) : $8.95 (alk. paper)
1. Dominion theology. 2. Law (theology) 3. Christianity and
politics -- Protestant churches. 4. Millennialism. 5. Jewish law.
I. DeMar, Gary. IL Title
BT82.25.N67 1991
231.7°6--de20 90-22956
cIP

 

Institute for Christian Economics
P. O, Box 8000
Tyler, TX 75711
