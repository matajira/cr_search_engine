What Role Does Israel Play in Postmillennialism? 137

a special place in the plan of God. It is greatly loved by God, Be-
cause of its unique role in the conversion of the Gentiles, it is to be
evangelized, not exterminated. It is to be called back to the God of
Abraham, Isaac, Jacob, and Joseph, not excluded from a place in the
world. It is to be cherished by the Church, the New Israel, not
excoriated as a “Christ-killer”; remember, the whole world crucified
Christ, for above His head were written in all the major languages
of Jew and Gentile: “King of the Jews.”

But second, the representative or covenantal view is not nation-
alistic. It does not believe there is magic in being a political unit, a
nation. Just because Israel has become nationalized has little or
nothing to do with its becoming “covenantalized”; in fact, being
politicized has always stood in its way of accepting Christ as Savior
and more importantly, Lerd, The representative view can therefore
advocate love for the Jew, while being able to reject his anti-Chris-
tian nation that persecutes Christians and butchers other people
who need Christ just as much as they. It can work for the conver-
sion of Israel without becoming the pawn of maniacal nationalism,
a racial supremacy as ugly and potentially oppressive as its twentieth
century arch enemy, Aryanism.”

The twentieth century has witnessed a great holocaust against
the Jews. What millennial position dominated American culture
during this time? It was dispensational premillennialism, not
postmillennial Reconstructionism. Dispensationalists are quick
to point out that postmillennialism fell out of favor with theolo-
gians after the first world war. Amillennialism was still promi-
nent as was covenantal premillennialism. Certainly postmillen-
nialism cannot be blamed for the holocaust since, according to
Lindsey and company, there were very few men who were
advocating the position after the first world war. Since dispensa-
tionalism did predominate during this period, what was its
response to the persecution of the Jews under Hitler's regime?

Dwight Wilson, author of Armageddon Now!, convincingly
writes that dispensational premillennialism advocated a “hands
off” policy regarding Nazi persecutions of the Jews. Since, ac-
cording to dispensational views regarding Bible prophecy, “the
Gentile nations are permitted to afflict Israel in chastisement for

11. Ray R. Sutton, “Does Isracl Have a Future?” Covenant Renewal (December 1988),
p.3.
