218 CHRISTIAN RECONSTRUCTION

Messianic Politics, 124
Moyers, Bill, 123
Natural Law, xi
Nature, 116n
One and the Many, x
Orthodox Presbyterian
Church, ix
Philosophy of the Christian Cur-
riculum, xv
Regeneration, 36, 84
St. Mary’s College, ix
Salvation and Godly Rule, xv
Social Change, 93
This Independent Republic, ix
Rutherford, Samuel, 153
Ryrie, Charles C., 88

Sabbath, 85

St. Mary's College, ix

Salvation and Godly Rule, xv

Salvation by Grace, 35

Sanctions, 51-52, 112, 126

Satan's Underground, 2

Schaeffer, Francis, xiii, 12, 121,
140-41

Schlissel, Steve, 3n

Scofield Bible, 13, 14

Scott, Thomas, 153

Seduction of Christianity, 2

Separation of Church and Society,
45

Shearer, J. B., 153

Shedd, W.G.T,, 157

Shirer, William L., 8

Sibbes, Richard, 134

Sider, Ronald, 173

Shupe, Anson, 172

Sklar, Dusty, 8

Slavery, 10, 109-10, 116

Smorgasbord Ethics, 17

Social Gospel, 82

Social Progress, 160

Social Theory, 17

Socialist-Inspired Millennium, 8

Society and State, 45

Southern Baptist Convention, 19

Southern California Presbytery
(OPC), xiii

Soviet Union, 7

Sproul, R. C., 13

Spurgeon, Charles Haddon, 148-
49, 157

Spurgeon’s Metropolitan Taber-
nacle, 28

Stalin, Joseph, 7, 50

Stam, Cornelius, x

Stratford, Lauren, 2

Structure of Scientific Revolutions,
41

Suffering, 49

Sutton, Ray, xvi, 54, 136-37

Swaggart, Jimmy, 65, 147

Ten Commandments, 18, 85, 151

“Ten Suggestions,” 18

Terrorism, 49

Tertullian, 91

That You May Prosper, xvi

Theonomy, 20

Theonomy: A Reformed Critique, 171

Theonomy in Christian Ethics, xiii,
xiv, 2, 175

This Independent Republic, ix

Thomas Nelson Publishing Com-
pany, xv

Time, 141-42

Tokes, Laszlo, 37

Toals of Dominion, 160n

‘Toon, Peter, 133-34

Turkish Massacre, 7

Unconditional Surrender, xv, 40n.
Unitarianism, xii, 177
