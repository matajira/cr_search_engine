Chapter 3

HUMANISM AND POLITICS

Now if ye be ready that at what time ye hear the sound of the cornet,
flute, harp, sackbut, psaltery, and dulcimer, and all kinds of music, ye fall
down and worship the image which I have made; well: but if ye worship
not, ye shall be cast the same hour into the midst of a burning fiery furnace;
and who is that God that shall deliver you out of my hands? (Daniel 3:15).

Then shall they deliver you up to he afflicted, and shall kill you: and ye
shall be hated of all nations for my name's sake (Matthew 24:9),

Humanism is an old religion. It is with us still. We live in an
era of humanism. Humanism is a simple enough religion. The
humanist believes the following:

1. Man, not Ged, owns the earth. Original ownership, meaning the
original title to the earth, belongs to man. Man is sovereign.

2. Man the creature rules over God the Creator. In fact, man is the
creator, for he alone understands and controls nature. Man repre-
sents only man.

3. Man, therefore, makes the rules, which means that an elite group
of men make the rules for everyone else.

4. “Man proposes, and man disposes.” He takes an oath only to
himself, He answers only to man, which means of course that the
vast majority of men answer to a handful of other men. Man is the
sovercign judge of the universe.

5. The future belongs to autonomous (self-law) man, meaning to
