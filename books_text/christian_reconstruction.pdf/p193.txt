What Is the Proper Response? 171

volumes of replies within a few months. Microchip technolo-
gy and a comprehensive paradigm make this possible. I had
one chapter written and typeset in one day and the book to the
printer’s within three weeks: only two weeks after the Westmin-
ster book was officially released to the book-buying public.'*

Remember the tar baby in the Uncle Remus story?’> That is
my working model. Punch me once, and you won't get out.
This strategy works. It is costly, but it works. When the critic
wearies of the exchange and fails to reply to our latest book-
length reply, we then tell the world: “See, he couldn’t answer
us. He clearly has no intellectually defensible position.” Tactic
or not, it really is true: the critics cannot defend their position.
It just takes some of them a long time to figure this out. There
are some amazingly slow learners out there.

Just how seriously should we take critics who begin their arti-
cles, as both Richard John Neuhaus and Errol Hulse began
theirs, with the frank admission that we Reconstructionists have
written too much for any critic to be expected to read?!* Then
they attacked us anyway!

As the Critics Multiply

Consider Richard John Neuhaus. He is liberal theologically,
but he has the reputation of being somewhat conservative cul-
turally. He is also, by neo-evangelical standards, a scholar. (At
the time he wrote his critical essay, he was still in the neo-evan-
gelical camp. A few months later, he joined the Roman Catholic
Church.) Previously, he had been unceremoniously booted out
of his former employer's New York offices.” In First Things’

18. William S. Barker and W. Robert Godfrey (eds.), Theonomy: A Reformed Critique
(Grand Rapids, Michigan: Zondervan Academie, 1990). The replies are: Gary North,
Westminster's Confession, Greg Babnsen, No Other Standard, and North (ed.) Theonomy: An
Informed Critique.

14, North, Millenniaticm and Sacial Theory (Tyler, Texas: Institute for Christian
Economics, 1990), ch. 9; “The Sociology of Suffering.”

15, He became the glue bunny — non-racial — in a recent Disney book version.

16, Richard John Neubaus, “Why Wait for the Kingdom?: The Theonomist Tempta-
tion,” First Things 3 (May 1990), p. 14; Errol Hulse, “Reconstructionism, Restorationism
or Puritanism,” Reformation Today, No. 116 (July-Aug. 1990), p. 25.

17. The Rockford Foundation.
