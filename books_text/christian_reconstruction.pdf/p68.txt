46 CHRISTIAN RECONSTRUCTION

Rushing Toward the Year 2000

The authors who are part of the Christian Reconstruction
movement are convinced that we are now moving into a period
of great turmoil historically — not just U.S. history, but world
history. Every inhabited continent is being plowed up institu-
tionally and religiously by God. This process is unquestionably’
worldwide in scope. Telecommunications now link the whole
world, and so does the New York Stock Exchange. For the first
time in human history, the whole world is operating with the
same science, technology, and mathematics. It is also struggling
with the same fundamental philosophical questions. The whole
world is experiencing the same breakdown in man’s ability to
understand and govern God’s world. God is plowing it up.

We are rapidly approaching the year 2000, an apocalyptic-
sounding year if there ever was one. The sense of urgency will
only increase from this point forward. The visible collapse of
Communism in Eastern Europe, the possible break-up of the
Soviet Empire, the threat of a military coup in the U.S.S.R., the
disarming of the West, the neutralization of Germany, the rise
of a “green” politics based on supposed ecological terrors and
the quest for political solutions to modern economic and envi-
ronmental problems, and the attempt to create a politically
unified Western Europe all point to enormous dislocations.

The New World Order (Again)

The humanists are at last proclaiming the advent of their
long-promised New World Order. In the midst of an unprece-
dented budget crisis and political deadlock, and in the midst of
a military confrontation between the U.S. and Iraq, President
Bush announced to Congress:

A new partnership of nations has begun. We stand today at a
unique and extraordinary moment. The crisis in the Persian Gulf,
as grave as it is, also offers a rare opportunity to move toward an
historic period of cooperation. Out of these troubled times, our fifth
objective — a new world order — can emerge: a new era, freer from
the threat of terror, stronger in the pursuit of justice, and more
secure in the quest for peace. An era in which the nations of the
