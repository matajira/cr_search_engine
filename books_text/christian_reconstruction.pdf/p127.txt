Are We Sanctified by the Law? 105

God is the law-word of God (Matthew 4:4 quoting the law of
Deuteronomy 8:3). Scripture is the very breath of God (2 Timo-
thy 3:16). Whatever God says is law.

For if “everything created by God is good” when “sanctified
by the word of God” (1 Timothy 4:5), then we ought to assume
that we are sanctified by the word of God, the law included.
The Spirit uses the Word of God in the sanctification process.
“And the one who keeps His commandments abides in Him,
and He in him. And we know by this that He abides in us, by
the Spirit whom He has given us” (1 John 3:24). Notice that
keeping the commandments and the Holy Spirit are not mutu-
ally exclusive. The Holy Spirit in us helps us keep the law of
God and instructs us when we either keep or break His com-
mandments. One way to tell if the Holy Spirit is in you is by
the way you treat His commandments.

‘Take away the law of God and sanctification turns into sub-
jectivism. It is no accident that Jesus said that a true disciple
will be known by others by something external, since only God
knows the heart: “You will know them by their fruits” (Matthew
7:20), “For we are His workmanship, created in Christ Jesus for
good works” (Ephesians 2:10); “so faith, if it has no works, is
dead” (James 2:17). We are saved by grace through faith, but
saving faith always produces good works.
