Humanism and Politics 41

Who Wins, Who Loses?

Thirty years ago, I took a university course in the history of
political theory, It was taught by a graduate student who had
studied political philosophy under Sheldon Wolin. Some eight
years later, when I was a graduate student in history, I had the
opportunity to attend a lecture by Professor Wolin. I had no
idea what he would be discussing, but I decided to attend.
Wolin, then of the University of California, Berkeley, later a
professor at Princeton University, gave a one-hour speech to a
small group of graduate students. Most of the others had never
heard of him.

It was the most important academic lecture I ever heard. He
introduced us to Thomas Kuhn’s crucial book, The Structure of
Scientific Revolutions, one of the most important books of the
1960's. That book became extremely important for me, as it did
for thousands of other young scholars in that era. Wolin argued
that many of the major concepts of political philosophy were
not discovered in some rational, academic manner, but have in
fact been formed during periods of religious and political con-
flict.? We live in such an era.

Finally, a quarter century after I took that undergraduate
class, I read Professor Wolin’s textbook in political theory. It is
far more than a textbook. It is a brilliant humanist defense of
political participation as something remarkably close institu-
tionally and psychologically to the sacrament of the Lord’s
Supper. He traces the history of political theory as something
bordering on being a secular substitute for worship. This was
what I had not understood so many years before: the history of
humanism is the history of man’s attempt to achieve secular salvation
through politics.

Early in his book, he makes a very important observation. 1
believe the phenomena described by this observation will be-
come increasingly important to Christians over the next few

1. University of Chicago Press, 1962.

2. Sheldon S. Wolin, “Paradigms and Political Theories,” in Preston King and B. C.
Parekh (eds.), Politics end Experience (Cambridge, England: At the University Press, 1968),
pp. 147-48.
