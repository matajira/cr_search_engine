86 CHRISTIAN RECONSTRUCTION

ment situations. Here’s a sample of Mosaic laws reaffirmed and
applied in the New Testament:

Old Testament Reference New Testament Reference
© Deuteronomy 8:3 * Matthew 4:4

* Deuteronomy © Matthew 4:7
© Deuteronomy Matthew 4:10

  

 

 

© Exodus 20:1 © Matthew 15:4

Leviticus 20:

Deuteronomy 5:16
# Deuteronomy 19:15 © Matthew 18:16; 1 Timothy 5:19
« Exodus 20:12; Deuteronomy © Matthew 19:18-19;

5:16-20; Leviticus 19:18 22:39; Romans 13:9

«© Exodus 20:13; Leviticus 19:18; e Romans 13:9
Deuteronomy 5:17

© Deuteronomy 25:4 © 1 Corinthians 9:9
© Leviticus 19:18 « Galatians 5:14
@ Deuteronomy 25:4 * 1 Timothy 5:18

But let’s suppose that only those laws repeated in the New
Testament from the Old Testament are valid. Of course, there
is no such principle of interpretation found in the Bible. And we
might go even further by stating, as the dispensationalist does,
that the church age did not begin until after Acts 2. This would
mean that laws found in the gospels would be relegated to the
Old Covenant era. They cannot be made to apply during the
“church age,” the dispensationalist wants us to believe, since
Jesus was addressing Israel, not the Gentile nations.

There was a time in dispensational theology when even the
Sermon on the Mount couid not be adopted by the church as an
ethical code. It could only be applied in the future millennium.
The Sermon was described as “kingdom law,” and since the
kingdom (millennium) had not come, these laws had to await a
future application. With all of this in mind, the church is now
left with a Bible from Acts 3 through Revelation 3 from which
he can formulate a law code.® This would mean, for example,

6. According to dispensationalists, the “rapture” occurs at Revelation 4:1. After the
church is gone, God once again deals with His earthly people, Israel. Jewish time begins
