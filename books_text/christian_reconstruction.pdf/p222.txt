200 CHRISTIAN RECONSTRUCTION

Edmonton, Alberta, Canada: Still Water Revival Books, (1876)
1990. Detailed exegetical study of the Second Coming and the
Millennium by a former premillennialist.

Cox, William E, An Examination of Dispensationalism. Philadel-
phia, PA: Presbyterian and Reformed, 1963. Critical look at
major tenets of dispensationalism by former dispensationalist.

Cox, William E. Why I Left Scofieldism. Phillipsburg, NJ: Pres-
byterian and Reformed, n.d. Critical examination of major flaws
of dispensationalism.

Crenshaw, Curtis I. and Grover E. Gunn, III. Dispensation-
alism Today, Yesterday, and Tomorrow. Memphis, TN: Footstool
Publications, (1985) 1989. Two Dallas Seminary graduates take
a critical and comprehensive look at dispensationalism.

DeMar, Gary. The Debate Over Christian Reconstruction, Ft.
Worth, TX: 1988. Response to Dave Hunt and Thomas Ice.
Includes a brief commentary on Matthew 24,

Feinberg, John A. Continuity and Discontinuity: Perspectives on
the Relationship Between the Old and New Testamenis. Westchester,
IL: Crossway, 1988. Theologians of various persuasions discuss
relationship of Old and New Covenants; evidence of important
modifications in dispensationalism.

Gerstner, John H. A Primer on Dispensationalism. Phillipsburg,
NJ: Presbyterian and Reformed, 1982. Brief critique of dispen-
sationalism’s “division” of the Bible. Expect a major work on
dispensationalism in the near future.

Halsell, Grace. Prophecy and Politics: Militant Evangelists on the
