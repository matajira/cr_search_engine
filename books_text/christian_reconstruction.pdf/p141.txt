What Is the Legal Standard for the Nations? 119

punishment upon it, so the land has spewed out its inhabitants. But
as for you, you are to keep My statutes and My judgments, and shall
not do any of these abominations, neither the native, nor the alien
who sojourns among you; (for the men of the land who have been
before you have done all these abominations, and the land has
become defiled); so that the land may not spew you out, should you
defile it, as it has spewed out the nation which has been before you”
{Leviticus 18:24-28).

The transgression of the very law which God was revealing
to Israel was ihe same law which brought divine punishment
upon the Gentiles who cccupied the land before them. “Israel
and the Gentiles were under the same moral law, and they both
would suffer the same penalty for the defilement which comes
with violating it — eviction from the land.”””

27. Greg L. Bahnsen, “For Whom Was God's Law Intended?,” The Biblical Worldview
(December 1988), p. 9.
