God and Government 49

in the existing liberal worldview. They talk tough, but their
economies are being threatened by massive debt. They talk
tough, but international terrorism now raises its ugly head,
against which the politicians can do next to nothing. There are
no more self-attesting truths in the world of humanism, except
one: that the God of the Bible isn’t possible.

It is this deeply religious presupposition that Christians
necessarily reject. It is this rejection that enrages the humanists.
Christianity, in its orthodox form, challenges all forms of the
power religion. Christianity is the religion of Christ’s kingdom
(civilization). It offers a better way of life and temporal death,
for it offers the only path to eternal life. It offers comprehensive
redemption — the healing of international civilization.’ It is the
dominion religion.’

When Christianity departs from its heritage of preaching the
progressive sanctification of men and institutions, it abandons
the idea of Christ’s progressively revealed kingdom (civilization)
on earth in history. It then departs into another religion, the
escape religion. This leaves the battle for civilization in the
hands of the various power religionists. Russia saw the defeat of
the visible national Church when the theology of mysticism and
suffering (kenotic theology) at last brought paralysis to the
Russian Orthodox Church. It had been infiltrated by people
holding pagan and humanistic views of many varieties.'° The
Church was incapable of dealing with the power religion of

8. Gary North, Is the World Running Down? Crisis in the Christion Worldview (Tylet,
‘Texas: Institute for Christian Economics, 1988), Appendix C: “Comprehensive Redemp-
tion: A Theology for Social Action.”

9, On cscape religion, power religion, and dominion religion, see my analysis in
Moses and Pharaoh: Dominion Religion vs. ower Religion (Tyler, Texas: Institute for Chris-
tian Economics, 1985), pp. 2-5.

10, Ellen Myers, “Uncertain Trumpet: The Russian Orthodox Church and Russian
Religious Thought, 1900-1917,” Journal of Christian Reconstruction, XI (1985), pp. 77-110.
She writes: “Russian pre-revolutionary religious thought was thus generally suspended
between the poles of materialist-Marxist and mystic-idealist monism. It partook of fund-
amentally anarchist Marxist and also Buddhist-style withdrawal from reality; an infatua-
tion with hedonistic classical paganism over against Christian supposedly joyless morality;
a ‘promethean’ desire to raise mankind to godlike superman status; and, concomitant to
all three, an ‘apocalyptic,’ nihilist rejection of the entire existing order in Russia in
anticipation of an imminent new, other, and betier utopian state of affairs.” Jbid., p. 93.
