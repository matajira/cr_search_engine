32 CHRISTIAN RECONSTRUCTION

life outside of the reach of Spirit-empowered restoration is an
area that was not affected by the fall of man. This, of course,
means no area at all.

Denying Responsibility

There are millions of Christians today (and in the past) who
have denied the obvious implications of such a view of God’s
earthly kingdom. Nevertheless, very few of them have been
teady to deny its theological premises. If you ask them this
question — “What area of life today is not under the effects of
sin?” — they give the proper answer: none. They give the same
answer to the next question: “What area of sin-filled life will be
outside of the comprehensive judgment of God at the final
judgment?”

But when you ask them the obvious third question, they start
squirming: “What area of life today is outside of the legitimate
effects of the gospel in transforming evil into good, or spiritual
death into life?” The answer is obviously the same — none — but
to admit this, modern pietistic Christians would have to aban-
don their pietism.

What is pietism? Pietism preaches a limited salvation: “indi-
vidual soul-only, family-only, church-only.” It rejects the very
idea of the comprehensive redeeming power of the gospel, the
transforming power of the Holy Spirit, and the comprehensive
responsibility of Christians in history. In this rejection of the
gospel’s political and judicial effects in history, the pietists agree
entirely with modern humanists. There is a secret alliance be-
tween them. Christian Reconstruction challenges this alliance.
This is why both Christians and humanists despise it.
