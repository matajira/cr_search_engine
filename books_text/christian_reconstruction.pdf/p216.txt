194 CHRISTIAN RECONSTRUCTION

ines modern views of eschatology: millennium, and tribulation.

Works Defending Postmillennialism or Preterism
Adams, Jay. The Time Is At Hand. Phillipsburg, NJ: Presbyteri-

an and Reformed, 1966. Amillennial, preterist interpretation of
Revelation.

Alexander, J. A. The Prophecies of Isaiah, A Commentary on
Matthew (complete through chapter 16), A Commentary on Mark,
and A Commentary on Acts. Various Publishers. Nineteenth-centu-
ry Princeton Old Testament scholar.

Boettner, Loraine. The Millennium. Revised edition. Phillips-
burg, NJ: Presbyterian and Reformed, (1957) 1984. Classic
study of millennial views, and defense of postmillennialism.

Brown, John. The Discourses and Sayings of Our Lord and com-
mentaries on Romans, Hebrews, and 1 Peter. Various Publishers.
Nineteenth-century Scottish Calvinist.

Campbell, Roderick. Israel and the New Covenant. Tyler, TX:
Geneva Divinity School Press, (1954) 1981. Neglected study of
principles for interpretation of prophecy; examines themes in
New Testament biblical theology.

Chilton, David. The Days of Vengeance: An Exposition of the Book
of Revelation. Ft. Worth, TX: Dominion Press. Massive postmil-
lennial commentary on Revelation.

Chilton, David. The Great Tribulation. Ft. Worth, TX: Domin-
ion Press, 1987. Popular exegetical introduction to postmillenni-
al interpretation.
