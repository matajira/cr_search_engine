94 CHRISTIAN RECONSTRUCTION

‘The Pyramid Society is a culture in which a majority of the people
spend most of their time transforming the civil sphere of government
to the near exclusion of themselves, their families, churches, schools,
businesses, and local civil governments. By changing the powers at
the top, we are led to believe that there will be a trickle-down effect
of cultural transformation that will blossom into a better society. The
problems that a nation faces, as this approach sees it, are solely politi-
cal. Change the State, and all of society will change with it. This has
been the vision of pagan empires since the building of the tower of
Babel.”

The belief in a centralized political order that critics insist Chris-
tian Reconstructionists defend is described by me as “paganism.”
Instead of a Pyramid Society, Reconstructionists advocate a
decentralized social order.

The Bible outlines a decentralized social order where power is
diffused and the potential for corruption and tyranny are minimized.
Freedom is enhanced because of the diluted strength of the one by
the maintenance of the many.”

Gary North emphasizes a similar theme in the following quota-
tion:

The biblical social order is witerly hostile to the pyramid society. The
biblical social order is characterized by the following features. First, it
is made up of multiple institutional arrangements, each with its own
legitimate, limited, and derivative sovereignty under God's universal
law. Second, cach institution possesses a hierarchical chain of com-
mand, but these chains of command are essentially appeals courts —
“bottom-up” insticutions — with the primary duty of responsible action
placed on people occupying the lower rungs of authority. Third, no
single institution has absolute and final authority in any instance;
appeal can be made to other sovereign agents of godly judgment.
Since no society can attain perfection, there will be instances of injus-
tice, but the social goal is harmony under biblical law, in terms of an
orthodox creed, God will judge all men perfectly. The State need not
seek perfect justice, nor should citizens be taxed at the astronomical

26. Gary DeMar and Peter Leithart, The Reduction of Christianity: A Biblical Response to
Dave Hunt (Ft. Worth, TX: Dominion Press, 1988), p. 305.
27. Tbid., p. 306.
