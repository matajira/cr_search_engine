Question 5

AREN’T WE NOW UNDER THE “LAW OF CHRIST”
RATHER THAN THE “LAW OF MOSES”?

Paul admonished the Corinthian church to depart from any
doctrine that had the effect of dividing Christ (1 Corinthians
1:13). Many well meaning Christians maintain false divisions
regarding the law which have the effect of dividing the Triune
God. They want to make a radical distinction between the “law
of God” and the “law of Christ” as if there are two law systems
operating in the Bible. Using this methodology, Jesus of the
New Testament is opposed to Jehovah of the Old Testament.
Jesus is a God of love, while Jehovah is a God of wrath. Jesus is
a God of grace, while Jehovah is a God of law. These are false
distinctions.

Ina similar way, the “law of Christ” is inappropriately pitted
against both the “law of Moses” and the “law of God” as if there
are three separate law systems, each in opposition to one anoth-
er. Here’s just one example of the way the “law of Christ” is
made a separate body of laws superceding the “law of God.”

Currently, God has made a new covenant with his people — the
church — and we live under the “law of Christ” (Galatians 6:2).

What do these authors mean? Are we to assume that the “law
of Christ” nullifies the “law of Moses” and the “law of God”?
What is the “law of Christ”? The authors tell us that the “law of

1, H. Wayne House and Thomas D. Ice, Dominion Theology: Blessing or Curse? (Port-
land, OR: Multnomah Press, 1988), p. 262.
