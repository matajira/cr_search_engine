108 THAT YOU MAY PROSPER

The verb yarash has three meanings when used in relation to land. The
first is lo receive the land as a gift [Lev. 20:24; RSV “inherit”,

‘The second is to occupy and organize the land according to God’s teach-
ing. . . . again and again they [the Scriptures] say that the commandments
are to be applicd “in the land to which you are going over, to possess it. . . .
that it may go well with you . . .” [Deut. 6:1-3, etc.]. Gonversely, failure to
observe the conditions of faithful tenants will mean that the rights of tenancy
of “possession” will be taken away [Deut. 4:26; 28:63]. The tenants will be
“dispossessed” [Num. 14:12].

The third meaning derives from the first two. Receiving the right of
tenancy [possessing in the sense of inheriting} and living on the land [pos-
sessing in the sense of a proper ordering of society on the land] can be
effected only if there is actual control. . . . ‘Vhe causative form of the verb
yatash means 10 cause a change in the power structure on the land . . . so
that a new social order may be set up.

Under the guidance of the Holy Spirit (Deut. 34:9), Joshua was
called to “possess” the land in this threefold sense. He was to lead
Israel to take what was theirs~not by “natural right,” but because
they were “covenantal tenants.” They were to apply the covenant,
and they were to set up a new social order under the Word of God.
They were to conquer the land by sbedtence to the law.

The conquest in this regard was rather unique. We see an exam-
ple at Jericho. Israel marched around the city, symbolically circum-
cising it, and performing a divine liturgy against the enemy. Israel
obeyed God’s word. The walls of paganism could not collapse by
brute power. God defeated the Canaanites through obedience to the
covenant, As the words of the title of this book go, “So keep the words
of this covenant to do them that you may prosper in all that you do”
(Deut. 29:9). Covenant renewal by worship and the laying on of
hands was not enough. The confirmation of one’s inheritance started
there, but it could not end there. Covenant renewal is supposed to
move out from around the throne of God and into civilization, If it
doesn’t, the inheritance is lost. Churches that are only caught up in
their liturgies forfeit society, and consequently forfeit everything.
Liturgy should translate into life. When it does, the inheritance is
fully realized.

9. BE. John Hamlin, Joshua: Inheriting the Land (Grand Rapids: Eerdmans, 1983),
pp. 9-10.
