Copyright © 1987 Ray R. Sutton
Second printing, 1992
Third printing, 1997

All rights reserved. Written permission must be secured from
the publisher to use or reproduce any part of this book, except

for brief quotations in critical reviews or articles.

All Scripture citations are from the New American Standard
Version, unless noted otherwisc.

Published by Institute for Christian Economics
P.O. Box 8000, Tyler, Texas 75711

Printed in the United States of America

 

Library of Congress Cataloging-in-Publication Data

Sutton, Ray R., 1950-

That you may prosper : dominion by covenant /
Ray R. Sutton.

- cm.

Includes bibliographical references and index.

ISBN 0-930464-11-7 (alk. paper).

1. Covenants— Religious aspects— Reformed Church.
2. Dominion Theology. 3. Law (Theology) 4. Reformed
Church—Doctrines. 5. Sociology, Christian (Reformed
Church) 6. Calvinism. 7. Bible. O.T. Deuteronomy—
Criticism, interpretation, etc. 8. Success— Religious aspects—
Christianity. I. Title.
BT155,.887 1991
231.7'6 — de20 91-22625

CIP

 
