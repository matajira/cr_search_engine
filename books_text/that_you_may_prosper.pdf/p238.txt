218 THAT YOU MAY PROSPER

3. Third Commandment: Ethics

The third section of the Deuteronomic covenant stipulates
what is involved in obeying God. The pagan system, growing out
of a “chain of being” approach, is inherently manipulative. This
commandment has to do with not manipulating the “name of God.”
When would the name of God be taken in vain? In false oath-taking.
To “swear” in the Bible is to take an oath. Jesus criticized
the Pharisees for swearing by all sorts of things and taking so
many false oaths (Matt. 5:33-37). They tried to manipulate God’s
name.

What is a name? A name in the Bible represents the person. The
power to name is the power to control, God named Adam. Adam named
the animals and Eve. This made man God's vice-regent in domin-
ion. But the power to name is the power to have authority over the thing that
is named. Therefore, any time the name of God was tampered with, it
indicated an attempt to manipulate Him. Actually, to worship a false
god re-names and re-constitutes the true God. God does not want to
be re-named, and He certainly cannot be manipulated.

The Pharaoh renamed Joseph (Gen. 41:45). Nebuchadnez-
zar’s official over the cunuchs renamed Daniel and the three Hebrew
youths (Dan. 1:7). When a man came under a pagan king’s author-
ity in the Old Testament, at least to serve in a position of leader-
ship under him, he was renamed by that king. God renamed
Abram to Abraham and Jacob to Israel, a sign of His authority over
them.

The commandment here forbids a manipulative approach to
God. It does not forbid oath-taking per se (i-e. in the courtroom). It
condemns swearing to “emptiness” or “vanity.” When someone
makes a false oath, he is attempting to manipulate God’s name for
his own end. Even though man tries, he cannot control God. It is the
other way around. Nevertheless, false oath-taking is ultimately a re-
flection on Him, making Him seem to be empty. How? When some-
one who is actually lying says, “May God strike me dead, if I am tell-
ing a lie,” and, ifhe does not fall down dead, but is later found out to
be a liar, God does not “seem” to have stood behind His name. If
man obeys God’s stipulations, however, he will not need to try to
manipulate God’s name. Blessing and whatever man needs will
come through a proper ethical relationship to Him.
