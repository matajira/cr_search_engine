xvi THAT YOU MAY PROSPER

TULIP: Total depravity, Unconditional election, Limited atone-
ment, Irresistible grace, and the Perseverance of the saints. The five
points of Calvinism are a theological application —soteriology ~ of
the five points of the covenant. Here is how they match up:

Total depravity Ethics (man’s)
Unconditional election Oath (God's)

Limited atonement Hierarchy (representation)
Irresistible grace Transcendence (sovereignty)
Perseverance of the saints Succession

Sutton began his monthly newsletter, Covenant Renewal, in Jan-
uary of 1987, Each month, he applies the five-point covenant model
to a biblical passage. At some point, he will be able to produce sev-
eral books with the exegetical evidence he has accumulated. His
newsletter provides monthly confirmation: the five-point model is no
mere “procrustean bed,” as one theonomic critic has described it. It is
in fact the key that unlocks the Scriptures.

England’s Central School of Religion awarded Sutton the Th.D.
in 1988 on the basis of That You May Prosper. In 1991, Sutton became
the president of Philadelphia Theological Seminary, the seminary
of the Reformed Episcopal Church.

Applications of the Model

As soon as I saw the next-to-last version of Sutton’s manuscript, I
spotted a mistake. Not a theological mistake: a rhetorical mistake.
He had neglected to appeal to the most important piece of biblical
evidence for his case: the Decalogue. I had just finished proofreading
the page proofs of my book, The Sinai Sirategy: Economics and the Ten
Commandments (1986). I realized that the five-point biblical model
governs the so-called two tables of the law.? The so-called First Table
is the priestly code; the so-called Second Table is the kingly code.

T hastily wrote a Preface for The Sinat Strategy, which was due at
the printers. I outlined the two sections as follows:

2. Lagree with Meredith Kline: the two tables of the law were in fact two copies:
one for God and one for Israel. Both were placed in the Ark of the Covenant, The
*owo tables of the law” do nat refer co two sections: laws one through five and six
through ten. Still, the terminology of the two tables is almost inescapable today.
