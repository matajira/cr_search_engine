The Historical Family Covenant 155

absolute authority. The check and balance on his power is the court
of the Church,

Such a system developcd because the marriage covenant is ethi-
cal. The historical marriage covenant, by the fact that there is a set
contract, limits the powers of the father, and the mother for that mat-
ter. They could not do anything they wanted with each other, or with
their children. There were ethical boundaries that defined the limits
of their power.

Unfortunately, there are Christians in places of great influence
today who continue to teach a clan view of the family, a kind of
“almost Mormonism.” They do not acknowledge the Protestant prin-
ciple of multiple authorities. They teach that the father’s word is law,
that a pagan father can, for example, keep a Christian son from
marrying a Christian woman, no matter what the girl’s parents say,
or the son’s and girl’s church courts say, This simply transfers to the
father the kind of authority which the Roman Catholic Church offi-
cially invests in the Pope. A father becomes a final court of earthly
appeal, one who can veto a marriage. This is radically anti-Biblical.
There are always multiple human courts that are established by God
to speak in God’s name, and no single earthly governmental author-
ity has a monopoly on speaking God’s covenantal word, and thcre-
fore absolute authority to create or dissolve a marriage.

Sanctions
Tn witness of all which, the said Thomas Clarke has signed and sealed
this present writing to Peter Oliver and William Bartholmew of Boston, for
the use and behoof of the said Alice Nicholls. Dated this twentieth day of
January 1664,

“Signing and sealing” is a process of sanctioning. Both parties of
this early American marriage covenant are taking a solemn oath, "The
example being used is similar to other documents of that cra. There
was some sort of ceremony; there was the actual “cutting” of the cov-
enant, just as we have already seen in Deuteronomy. It is still today
called a wedding ceremony. In our society, all three institutions partici-
pate in it. The minister officiates at the ceremony because the
Church is the “guardian” of marriage. The families draw up the con-
tract. The State is a witness because marriage is an institution of sect-
ely as well. In the event there is a divorce, or the settlement of the
family’s estate, the State may be drawn into the legal process. So
marriage does not belong exclusively to any of these institutions.
