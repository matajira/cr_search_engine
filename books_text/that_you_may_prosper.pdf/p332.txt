312 THAT YOU MAY PROSPER

trary, it is God-made. It was established definitively at the cross (in
history), affirmed visibly at the resurrection (in history), and mani-
fested supernaturally at the coming of the Holy Spirit at Pentecost
(in history). After all, Christ had told His disciples to remain in
Jerusalem until the Spirit came, “for John baptized with water, but
you shall be baptized with the Holy Spirit not many days from now.
. . . [¥]ou shall receive power when the Holy Spirit has come upon
you...” (Acts 1:5,8a). You would think from Hendrikson’s com-
ments that Christ had promised, “You shall receive impotence.”
Hendrikson’s exposition, called More Than Conquerors, is in fact a
defense of the idea of Church members as éess than conquerors in
history.

He is inescapably defending the idea that Christ’s victorious
kingdom is not realy manifested in history; it is only symbolically vic-
torious. It is only nominally victorious. The amillennialist asserts
that Christ’s authority over history is manifested only at the end of
time, after a long progression of decline for the Church, at the very
moment that the satanic forces have surrounded her (Rev. 20:8-9).6
Only at the close of history is Christ’s absolute authority manifested
in history. Thus, amillennialism’s so-called “realized eschatology”
is historically an unrealized eschatology: Christ’s Church is defeated
historically.”

To avoid the inescapable implication of covenant theology that
such a defeat of Christ’s Church is therefore also representatively (cove-
nantally) a defeat of Christ in history, amillennialists must implicitly
deny the representative character of the Church and its members’
(supposedly increasingly ineffective) cultural activities in history,
Amillennialists may verbally affirm the covenantal nature of the sac-
raments, but by denying the covenantal nature of the sacraments’
culture-transforming effects in history through the Church's visible vic-
tory, they thereby implicitly deny the covenantal nature of the sacra-
ments, for the sacraments testify to Christ’s victory in history.

The traditional Reformed or Calvinistic view of the sacraments,
I contend, is the Biblical view. Like the Biblical view of Christ’s his-
toric kingdom authority, it is covenantal. It is therefore a representative
view. In the sacraments, Christ is spiritually present, not humanly

6. R. B. Kuiper, The Glorious Body of Christ (Grand Rapids, Michigan: Eerdmans,
1958), p. 277.

7. For a critique of this amillennial view, see Gary North, Dominion and Common
Grace (Tyler, Texas: Institute for Christian Economics, 1987), ch. 5.
