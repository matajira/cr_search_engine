Revelation 257

precatory Psalms were uscd— Psalms laid out in the covenant law-
suit structure designed to bring judgment on God’s enemies (Ps. 83)
—or some other “maledictory oath.” Whatever they did, we are in-
troduced to a hierarchy in Revelation 2-3.

Second, these letters to the churches also have a fistoricad flair to
them, each church having Old Testament allusions that seem to fol-
low the history of the Old Covenant. Corsini says,

It has been sometimes noticed that there is a sort of Aistorical progression
in the Biblical allusions, a history of salvation beginning with Adam and
finishing with Christ. Each episode refers to some progressing moment in
the history of salvation . . . fitting John’s continual and central concern to
show that the coming of Christ is the perfection and replacement of the Old
Testament economy.’

The following is a brief overview of the history indicated.

1. Ephesus. Reference is made to the Fail, garden, and curse
(Gen. 2:17-3:19) in the phrases, “Remember then from what you
have fallen” (2:5), “I will grant to eat of the tree of life which is in the
paradise of God” (2:7), and “Your works, your toil and your patient
endurance” (2:2),

2. Smyrna. The historical period seems to be the captivity in
Egypt. The “ten days of testing” (2:10) refers to the 10 plagues of
Egypt (Exod. 7:14ff.). Also, Christ speaks of Himself as “dead and
has come lo life,” an allusion to the Exodus where Israel was dead
and then resurrected.

3, Pergamum, The next period is the time of the wilderness wan-
derings with reference to the “manna” (2:17; cf. Exod. 16:32ff.). Also,
during this period, there was the Balaam and Balak episode (2:14; cf.
Num. 25:1-2).

4. Thyatira. A time of prosperity and apostasy approximates
this epoch of history during the reign of the kings. The mention of “Jez-
ebel” indicates the era when Israel “tolerated apostasy” (2:20; I Kgs.
16:31ff.).

5. Sardis. The time is the latter prophetic period, judging by the
comments hinting at a small group of people, a “remnant” (3:4; Isa.
1:9, 6:13; 65:8ff.).

6. Philadelphia. The history probably refers to the return of the
Jews after exile. The “key of David” indicates that people are coming

7. ‘the Apocalypse, pp. 104-105. Emphasis mine,
