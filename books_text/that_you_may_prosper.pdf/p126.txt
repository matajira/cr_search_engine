106 THAT YOU MAY PROSPER

The followers would again pledge an oath and renew their covenant
to seal legally and ritually the transfer of power.’ In Deuteronomy
and Joshua the same process takes place. Israel is confirmed in its
allegiance to Joshua with Moses. After the death of Moses, they
have other ceremonies and meals to renew their covenant with their
new leader (Josh. 5:10; 8:30-35). The confirmation of communion is sup-
posed to be an ongoing process.

These events were, in the ancient world, a combination of civil
and ecclesiastical. Today, we separate the two governments more
clearly, but similar rituals still exist. Many churches ordain pastors,
elders, and deacons by the practice of the laying on of hands. Exist-
ing elders touch the shoulders of kneeling men who have been or-
dained but not yet confirmed. This is the act of confirmation. Some-
times they celebrate communion immediately after the laying on of
hands. In civil government, there must also be confirmation and
some variant of the laying on of hands. In this context, Moses laid
hands on Joshua (Deut. 34:9). This was a judicial act. It was not
magical. Nothing of substance flowed from Moses to Joshua. But
this legal act symbolized that Joshua was in continuity with the his-
toric covenant. Historic continuity is extremely important. To cut
oneself off from history results in fighting the same battles of the
past. It is a common saying that those who do not understand his-
tory are condemned to repeat the mistakes of the past.

For example, the modern Church generally reacts to the creeds
of the early Church, The liberals want to change them, and the
evangelicals want to curse them: “No creed but Christ,” and so forth.
What happens? The Church finds its old enemies creeping back into
history. It fights Arian theology of the third and fourth centuries in
the form of the Jehovah’s Witnesses, It fights gnostic theology in the
form of the New Age movement. So, essentially, it becomes difficult
for the Church to make progress without the confirmation process of
the historic Church.’

We should always maintain that the basis of continuity is the Word

7, Don Sinnema, Reclaiming the Land (Toronto, Canada: Joy in Learning Curric-
ulum Development and Training Center, 1977), pp. 45-46

8. This bears on the “Apostolic Succession” question. Some would argue that only
the ordination of a “certain” church is to be recognized. East and West have been
divided on this matter. But recently, there have been Catholic and Orthodox
scholars who see the succession question in a more “processional” sense, meaning the
succession is in the body “politic,” and not just in the bishop. See Alexander
Schmemann, The Historical Road of Eastern Orthodoxy (Crestwood, New York: St.
Vladimir's Seminary, 1977), p. 45.
