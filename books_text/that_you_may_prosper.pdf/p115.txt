Sanctions 95

Finally, witnesses are required for the ratification of the covenant.
The witnesses are heavenly and earthly when the covenant is cut.
They verify that an actual covenant has been created. Without
them, the covenant is not official.

This ratification process creates a transfer of name, called the
doctrine of adoption. Covenant cutting adopts the initiate into the
kingdom of God. He receives a new name and inheritance.

The next chapter advances the study to the last principle of the
covenant, continuity. In this chapter, we will focus more on inherttance.
The ratification of the covenant creates a new inheritance, but a con-
stant process of covenant renewal has to occur before the inheritance
is actualized. Several questions will be answered. What is the point
of continuity in the covenant: blood-bond, or something else? What
is the inheritance of Gad’s people? Is it spiritual or physical?
