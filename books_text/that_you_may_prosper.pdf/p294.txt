274 THAT YOU MAY PROSPER

uals may fall away, but the Church became God's final bride. The
Church as the Church will never apostatize!

From the historic point of view there are two Israels, yet one true
Israel. How so? The writer says God will make a “new” covenant with
Israel and Judah (v. 8). In one sense this “newer” covenant was made
with Israel when they returned from captivity in Babylon (Jer.
3:32). Remember, the writer to the Hebrews is quoting Jeremiah
who prophesied before and during the captivity. But in a strict sense,
the New Covenant was made with the Church. When Jesus inaug-
urated the Lord’s Supper He said, “This cup which is poured out for
you is the new covenant in My blood” (Lk. 22:20). This means the
“Israel and Judah’ of the New Covenant ts the Church. The Church is
the true son of Abraham and the new “Israel of God.” Paul says, “If
you belong to Christ, then you are Abraham’s offspring, heirs ac-
cording to the promise. . . . Neither is circumcision anything, nor
uncircumcision, but a new creation. And those who walk by this
rule, peace and mercy be upon them, and upon the Israel of God”
(Gal. 3:29; 6:16). Paul’s comments lead us to believe that there is
only one true Israel through history, the “faithful!” There are two his-
toric Israels, but one true covenantal Israel.

This brief section in Hebrews has helped us to see a very impor-
tant historic diference between the covenants. One was made with
Adam and was temporary. The second was made with Christ and be-
came permanent. The latter mediates a better judgment in history.

3. Ethics (10)

The principle of comparing Old Covenant age to the New Cove-
nant age carries into the ethical. Remember, the ethics section of
Deuteronomy speaks of the heart of the covenant as being the ful-
fillment of righteousness. This concept is pulled into the New Covenant.
Jeremiah says the Law of God will be written on the “heart” of the
New Covenant man (Jer, 31:33), Does this mean the law is not on
the heart of those in the Old Covenant? No. David says, “I delight to
do Thy will, O My God; Thy Law is within my heart” (Ps, 40:8).
What is so unique about the New Covenant?

The law was not “incarnated” in Adam. Christ became the em-
bodiment of the Law of God: the Word made “flesh (Jn. 1:1, 14). He
carried the Law in His person, unlike Adam, and became the incar-
nated Law! Again it should be stressed that there were individuals
who had the law written on their heart in the Old Testament
