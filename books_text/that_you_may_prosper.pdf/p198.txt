178 THAT YOU MAY PROSPER

praise from the same; for it is a minister of God to you for good, But if you
da what is evil, be afraid; for it does not bear the sword for nothing; for it is
a minister of Gad, an avenger who brings wrath upon the one who practices
evil. Wherefore it is necessary to be in subjection, not only because of
wrath, but also for conscience’ sake. For because of this you also pay taxcs,
for rulers are servants of God, devoting themselves to this very thing.
Render to all what is due them: tax to whom tax is due; custom to whom
custom; fear to whom fear; honor to whom honor (Rom. 13:1-7).

True Transcendence

Paul argues that magistrates are “ministers of God” (Rom. 13:4).
In the realm of the State, we find the same emphasis that we did in
the Church, The transcendent character of the covenant in the civil
realm is God, not the State itself. The State is created by God; it is a
“Divine” institution. But, it is carefully distinguished from God by
the designation, “minister.” If one ignores either, he falls off a dan-
gerous precipice on the right or left side of the truth.

The State should not be allowed to become “divinized” or a new
“god.” The Puritans believed that if transcendence is placed in the
State, then this sphere becomes “absolutized”: the new “priesthood.”
All through history, pagan cultures have tended to be statist, believing
in some kind of political salvation. One of many examples in the Bible
is Darius, King of Babylon. He set up “Emperor worship,” asking
everyone in the land to bow down and worship him (Dan. 6:7). Dan-
iel would not, and was thrown into the “lion’s den” (Dan. 6:16-24).

On the other hand, the State is created by God, and thus, as an
“institution,” cannot be the enemy of the Church (Rom. 13:1), Does
this contradict what I just said? No, We should be careful to distin-
guish “evil men” who hold office from the “institution” itself. Paul was
reminding Christians of this fact at a time when Nero was Caesar of
Rome. If Christians think the enemy is the State, they end up in-
volved in “anarchical” activity that is thwarted by God, because they
are opposing the institution that He made.

The anarchist argues along the lines of nominalism. He says that
the State does not really exist; what we cafl the State is simply groups
of people acting as individuals for certain purposes, It is not respon-
sible to God as a collective entity, for it has no separate reality. It is
not a true representative of the citizens; rather, certain people speak
on behalf of the citizens, despite the fact that citizens cannot delegate
power to any independent organization.
