The Ten Commandments 223

ment man coveted the wife of another, he was cutting into his neigh-
bor’s inheritance. In Israel, this disrupted everything because each
family received a particular piece of land and inheritance when Can-
aan was conquered under Joshua. To covet one’s covenant brother's
family and possessions was to rob the inheritance granted by the cove-
nant itsclf. Here the last commandment ends on a note of finality.

The second five commandments follow, without much explana-
tion, the basic pattern of the covenant, completing a perfect double
witness.

Priest/King Distinction

One final observation about the commandments should be
made, The fundamental difference between the two halves is that the
first has a priestly emphasis, while the second is kingly. A priest was
fundamentally a guardian of the presence of the Lord. Adam was told
to “till and guard” the garden (Gen. 2:15). We know that “guarding” is
a priestly function because the same Hebrew word refers to the
Levite’s responsibility (Lev. 5).

The first five commandments have to do with things which
priests were specifically to guard: the Lord’s transcendent/immanent
character, the worship of God, God’s name, the Sabbath, and family
inheritance. What does the last point have to do with guarding? The
priesthood was the guardian of the family. Inheritance left without
an heir went to the Levites. In the New Covenant, the care of widows
and orphans belonged to the Church, not the State. Both Israel and
the Church are called priesthoods (Ex. 19:6; I Pet. 2:5). Protecting
the family inberitance is a priestly function. This is why Jesus so
severcly chastised the Pharisees for allowing the abuse of parents’ in-
heritance (Mark 7:1ff.).

The second five commandments have to do with the oversight of
the king. He was the one, for example, to implement the discipline
for breaking these commandments. He would have also been in-
volved with “first table” offenses, but they would have come through
the priesthood, the special guardian of God’s House.

This structure, priest to king, is the way man dominates. As a
general principle, he must be a servant to be a leader. One should
not think of this process as anything other than the way of the cove-
nant. Man covenants his way to dominion. When man keeps God's
commandments, he is living by the covenant. When he lives by the

 

covenant, he has dominion.
