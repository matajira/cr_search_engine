232 THAT YOU MAY PROSPER

to every individual. Biblical society operates by representation,
avoiding tyranny on the one hand, and anarchy on the other. So
Psalm 2 raises the question, and Psalm 149 gives the ultimate an-
swer, The illegitimate will be driven off the earth, and the true legiti-
mate heirs will take possession of what rightfully belongs to them,
final continuity and discontinuity being set up.

Conclusion

Psalms has a Deuteronomic structure. We are safe in concluding
that the Old Testament continues to follow the covenantal pattern.

But does it continue into the New ‘Testament? Someone might be
saying at this point, “I agree that Deuteronomy is the structure of the
covenant in the Old Testament, but things change in the times of
Jesus, don’t they?” Do they? Let us now consider in the next three
appendixes the structure of one of the Gospels, an epistle, and the
Book of Revelation. Then we will be able to see if this covenantal
pattern extends into the age where the “New Covenant” Christian
lives!
