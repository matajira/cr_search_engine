126 THAT YOU MAY PROSPER

glory, probably the strongest image of transcendence in the Bible.

2. “Let there be an expanse in the midst of the waters, and let it
separate the waters from the waters” (1:6): Hierarchy. Waters are
arranged from the waters above to the waters below. The Noachic
Flood indicated that the waters above had primary importance, rep-
resenting Gods judgment.

3, “Let the waters below the heavens be gathered into one place,
and let the dry land appear” (1:9): Ethics. The land in the Old Testa-
ment had a central cause/effect relationship to everything else. For
example, when a murder is committed, a man’s blood “cries out from
the land” (Gen, 4:10). Point three also involves ethical and geograph-
ical boundaries.

4. “Let the earth sprout vegetations, plants yielding seed” (1:11):
Sanetions. This seed-bearing process is specifically mentioned in the
sanctions section of Deuteronomy (Deut. 28:38-40).

5. “Let there be lights in the expanse of the heavens to separate
the day from the night, and let them be for signs, and for seasons,
and for days and years... and to govern the day and the night”
(1:14): Continuity. The great lights are the instruments of continuity
from onc day to the next—continuity made visible by discontinuity.

Second Five

1, “Let the waters teem. . . let the birds fly... . And God
blessed them, saying, ‘Be fruitful and multiply’ ” (1:20-22): Transcend-
ence, Creatures of special transcendent glory are mentioned: the
birds that fly above, and the great sea monsters (Leviathan, cf. Job
3:8; 4t:1; Psa. 74:14; 104:26).

2. “Let the earth bring forth living creatures” (1:24): Hierarchy.
Animals are part of the specific hierarchy of the earth. They are used
to represent man as sacrifices (Gen. 3:21).

3. “Let Us make man in Our image... and let him rule”
(1:26): Ethics. Man rales over the animals by carrying out this do-
minion commandment. Dominion and ethics are inseparable.

4. “Be fruitful, subdue, and rule” (1:28): Sanctions. All things are
to be brought under and ratified in a covenant relationship.

5. “I have given every plant . . . and every tree” (1:29): Continu-
ity. Man is given a specific inheritance of food, the meal. Through
this nourishment, man is able to lay hold of his inheritance.

The “ten words” of creation have the same double witness of the
covenant, as do the Ten Commandments. This being the case, the
“cultural mandate” falls on the fourth word of the second series (Gen.
