xviii THAT YOU MAY PROSPER

and the Book of Revelation. He kept asking questions for many
weeks, as he restructured his manuscript.

Chilton recognized that the Book of Revelation is at least two
things: a covenant lawsuit against Old Covenant Israel and a model
for church liturgy. He used the five points of the covenant to restruc-
ture the commentary with respect to both the covenant lawsuit and
covenantal liturgy. Perhaps for academic reasons, Chilton appropriated
Meredith G. Kline’s terminology for the first two points: preamble
and historical prologue, but the actual content of the commentary
indicates that Sutton’s usage is dominant, not Kline’s. Chapter 1 is
“King of Kings,” indicating transcendence. In the Introduction to
Part 1, he writes: “The purpose of the covenantal Preamble is thus to
proclaim the lordship of the Great King, declaring transcendence and
unmanence and making it clear from the outset that his will is to be
obeyed by the vassals, his servants. Biblical treaties set forth God’s
transcendence and immanence by referring to one or more of three
activities: creation, redemption, and revelation.”’ Chapter 2, which
begins Part 2, “Historical Prologue,” is titled: “The Spirit Speaks to
the Church: Overcome!” Chapter 3 is “The Dominion Mandate.”
Chilton’s focus here is hierarchy and authority, not historical prologue.

For those who doubt the applicability of the five-point model be-
yond the Book of Deuteronomy, Chilton’s Days of Vengeance is required
reading. The book’s Foreword, written by English evangelical theo-
logian and Old Testament specialist Gordon Wenham, added au-
thority to Chilton’s creative application of Sutton’s discovery. Chilton’s
Days of Vengeance is one of the most impressive secondary support vol-
umes ever written in defense of another author's primary thesis. It
may be the most impressive back-up book ever published in the
same year as the primary book. In fact, it was published first.

Kenneth L. Gentry’s book, The Greatness of the Great Commission
(1990) also applies the five-point model to an important New Testament
theme. He adopts the acronym THEOS. He describes the ancient
suzerainty treaties by using the THEOS outline, and writes that
“God's covenant follows the same pattern.” Part II of the book con-
sists of five chapters: The Declaration of Sovereignty, the Exercise of
Sovereignty, The Terms of Sovereignty, the Commitment of Sover-
eignty, and the Continuance of Sovereignty.

7. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987), p. 49. Emphasis in original.

8. Kenneth I. Gentry, Jr., The Greatness of the Great Commission; The Christian Enier-
prise in a Fallen World (Tyler, ‘Texas: Institute for Christian Economics, 1990), p. 18.
