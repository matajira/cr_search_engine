220 THAT YOU MAY PROSPER

ened: two thousand years of compounding. So this commandment
has to do with inheritance, an issue of legitimacy. Obedient sons and
daughters receive the inheritance, the blessing of the previous
commandment.

The first series of commandments follows the structure of the
covenant, Without having to force the commandments, I believe the
reader can easily see how God ordered them around the five parts of
the covenant. The second half of the commandments does the same.

The Second Five Commandments
6. The Sixth Commandment: True Transcendence

God returns to the transcendence theme. How? Unlawful killing
of another human being was expressly forbidden because “Whoever
sheds man’s blood, by man his blood shall be shed, For in the image
of God He made man” (Gen, 9:6). In fact, the “ethical” section of the
Noahic covenant is summarized under this one commandment,
summing up all of God’s demands.

The key is in the word image. Man is the image of God. Unique
to God’s creation and unlike any other aspect of His handiwork, man.
is a picture of God. Man shows God’s transcendence and imma-
nence. To kill man is analogous to killing God. All rebellion is an at-
tempt to kill God. Satan tempted man to become like “God.” Be-
tween the lines of Satan’s offer was the idea that the true God would
be displaced (Gen. 3:1.). So the second set of commandments begins
with a commandment against eradicating God’s transcendent/
immanent representation in man,

7. Seventh Commandment: Hierarchy

The Deuteronomic covenant made a specific connection between
idolatry and adultery. The end of the second section calls attention to
the second commandment, reminding Israel of the prohibition
against “idolatry” (Deut. 4:15-19), Moses gives as a reason, “For the
Lord your God is a consuming fire, a jealous God” (Deut. 4:24).
“Jealousy” is a response to any kind of marital unfaithfulness. In-
deed, there was a special “ordeal of jealousy” (Nu. 5). Since the peo-
ple of God are His “bride,” worshipping other gods would be anal-
ogous to sexual unfaithfulness in marriage. God’s proper response
would be Sjealousy.”

The ideas of worship and marriage are expressed in the old
