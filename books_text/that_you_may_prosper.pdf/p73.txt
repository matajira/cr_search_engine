Hierarchy 53

Redemptive History

History is covenantal.? It is the story of the application of the
covenant in God's world. No covenant —no space, time, or history.
The historical section of Deuteronomy is a miniature picture of this
relationship between history and covenant. What happens in the
second segment of Deuteronomy with God’s representatives happens
on a larger scale throughout the Bible. Consider the following over-
view: a hierarchy (Deut. 1:9-18), a fall of the hierarchy (Deut.
1:19-46), a series of tests (Deut. 2:1-3:29), judgments (4:1-24), and
finally the transition from Moses to the new leadership of Joshua, a
superior hierarchy (Deut. 4:25-49). All of these parts track Biblical
history. As we saw earlier, Adam and Eve were Gud’s hierarchy.
Shortly after their creation, they fell. They faced a test and failed.
Then God judged them and promised a new hierarchy in the form of
a seed. This pattern repeats itself over and over again. We can say
there is similarity. But, there is dissimilarity, as we sce in the Bible
that Adam’s seed does not have the power to bring in a new hierar-
chy. God must send /fis Son in a miraculous way. Nevertheless, God
summarizes Biblical history in this short segment of Deuteronomy.

Thus, we can posit two principles about the redemptive history
of the Bible by comparing it to Deuteronomy (1:9-4:49).

First, just as there is sémiélarity between Deuteronomy and the
New Covenant that Jesus established, we can derive a principle of
similarity in redemptive history. The similarity is in the covenant
because the covenantal structure is repeated. Just as everything
about a human being is encoded in those first cells of his existence,
so the first beginnings of the covenant in Genesis are encoded in seed
form. The following diagram pictures the growth of the covenant
like a seed, containing everything from its first inception that it will
eventually become in a much fuller sense.

7. R. G. Collingwood, The Idea of History (Oxford: Clarendon Press, 1946), p. 52.
Collingwood said, “Any history written on Cinistian principles will be of necessity
universal [(ranscendent], providential [hierarchical], apocalyptic [sanctions], and
periodized [continuity].” I have added brackets to shaw how Collingwoud’s assess-
ment of Christian interpretation of history casily fits the covenantal scheme J am
presenting in this book. Gordon H. Clark, Historiography: Secular and Religious (Nut-
ley, New Jersey: Craig Press, 1971), pp. 244-245, even refers to Collingwood's four
points and adds a fifth: “revelation.” This would fit the third point of covenantalism
called “ethics,” or law, being “special revelation” to man.
