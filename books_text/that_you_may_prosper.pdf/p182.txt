162 THAT YOU MAY PROSPER

are easily offended because they will be the first to give offense.”*
Nevertheless, tyranny of the minority surfaces when certain mem-
bers of the congregation do not like the fact that others do not see
things the way they do. Then these “offended” ones usually try to
manipulate people with verses of the Bible wrenched out of context.
Usually, these verses are drawn from passages that talk about “deny-
ing oneself” certain things because others are “stumbling” (I Cor.
8:13; Rom, 14-15),

Perhaps the favorite verse used by the tyrannical minority is
“Therefore, if food causes my brother to stumble, I will never eat
meat again, that I might not cause my brother to stumble” (I Cor,
8:13). I know of a church where peuple left because “coffee cake” was
being used that did not meet the health food requirements of the
tyrannical minority. (These people eventually stopped going to any
church.) Does this verse apply to coffee cake or similar foods? No.
The word “stumble” means to sin. The point ts that if a Christian is
doing something to cause his weaker brother to sin, then he should
give it up. What we find, on the contrary, is that Christians are
asked to refrain from doing something allowed by the Bible. The
passage is not talking about something another person does not ap-
prove of or like.

Biblical law should rescue a true covenantal church from a
myraid of these kinds of problems. God's ethical standard provides
objective requirements that prevent the manipulative techniques of
those “saints” who have strong convictions about certain foods or
drinks that are not explicitly condemned in Scripture.

Sanctions (Rev. 2:5b)

Christ warns that if the Ephesians will not repent, then He will
remove their lampstand (Rev. 2:5b). When sin is not dealt with in a
church, God directly applies the sanctions of the covenant. That is,
if the church fails to discipline its own members, implementing the
sanctions, then God will do it for them. It may take many years for
the discipline of God to work its way out. But He will render lex

talionis (“eye for eye”). The process of Church discipline is explained
by Christ.

4, Cotton Mather, The Great Works of Christ in America: Magnatia Christi Americana
(London: Banner of Truth ‘Frust, [1702] 1979), Vol. 1, p. 50.
