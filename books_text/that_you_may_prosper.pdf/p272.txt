252 THAT YOU MAY PROSPER

uity but will lose it themselves if they don’t change.

Paul also makes 2 comment that goes all the way back to Genesis
three, “The God of peace will soon crush Satan under your feet”
(16:20). God had told Eve that her “sced” would crush the head of the
serpent (Gen. 3:15), This curse was part of the judicial sanction and
the promise of continuity in the legitimacy section. Here Paul does
the same. While cursing the “trouble-makers,” he makes a promise of
continuity that Satan will be destroyed.

One final comment. This list establishes the principle of Church
rolls and records. The Bible is full of rodis of membership. The Book of
Numbers begins and ends with one, and any time the nation is re-
constituted, lists start to appear. Why? There can be no discipline,
meaning excommunication, if there is nothing to be disciplined
from. How can someone be cast out if he is not a member? He can’t.
Today when the absence of church rolls is in vogue, the conclusion is
that there really cannot be effective discipline. Sure, discipline can
be abused, but the abuse does not nullify its use. Also, records keep
track of any judicial proceedings for future generations, The Roman
Catholic Church has a long record of all its court cases. Unfor-
tunately, Protestantism doesn’t! Since Biblical law is applied through
a “precedent” system, these records are invaluable. They help future
generations to determine how to make decisions. They help to main-
tain the proper continuity.

Conclusion

Paul follows the pattern of the covenant in his letter to the
Romans. His thought is so ordered by it that he even develops his
sub-points in this fashion. Most students of the Bible will notice that
most of the epistles, especially the Pauline ones, follow the Deuter-
onomic structure. But one last portion of the Bible remains to be
considered, perhaps the most controversial of the Bible, Revelation.
Is the last book ordered according to the covenant? It would seem
that if my thesis is right, that the five points of covenantalism are in-
deed the structure of the covenant and of the Bible itself, then the
final book of Scripture should have this pattern somewhere. Not
only does the Book of Revelation have the pattern, the whole book
follows the Deuteronomic pattern. And it is one of the most obvious
examples in the New Testament, indeed in the entire Bible.
