270 THAT YOU MAY PROSPER

The Covenantal Structure of Hebrews 8
True Transcendence

Now the main point in what has been said is this: we have such a
high priest, wha bas taken His seat at the right hand of the throne of
the Majesty in the heavens, a minister in the sanctuary, and in the true
tabernacle, which the Lord pitched, not man. For every high priest is
appointed to offer bath gifts and sacrifices; hence it is necessary that
this high priest also have something to offer. Now if He were on earth,
He would not be a priest at all, since there are those who offer the gifts
according to the Law; who serve as a copy and shadow of the heavenly
things, just as Moses was wamed by God, “See,” He says, “that you
make ail] things according to the pattern which was shown you on the
mountain.”

Hierarchy
But now He has obtained a more excellent ministry, by as much as
H¢ is also the mediator of a better covenant, which has bcen enacted on
better promises. For if that first covenant had been faultless, there
would have been no occasion sought for a second. For finding fault
with them, He says, “Behold, days are coming, says the Lord, When
T will effect a New Covenant with the House of Israel and with the
House of Judah; Not like the covenant which I made with their
fathers on the day when I took them by the hand to lead them out of
the land of Egypt; For they did not continue in My covenant, and I
did not care for them, says the Lord.”

Ethics

“For this is the covenant that I will make with the House of Israel
after those days, says the Lord: 1 will put My dws into their minds,
And I will write them on their hearts. And I will be their God, and
they shall be My people.”

Sanctions
“and they shall not teach everyone his fellow citizen, and every-
one his brother, saying, ‘Know the Lord.’ For all shall know Me, from
the least to the greatest of them,”

Continuity
“For I will be merciful to their iniquitics, and 1 will remember
their sins no more.” When He said, “A new covenant,” He has made
the first obsolete. But whatever is becoming obsolete and growing old
is ready to disappear.
