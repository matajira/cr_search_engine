60 THAT YOU MAY PROSPER

some form of manipulation through one means or another. The de-
tails are spelled out in the largest section of Deuteronomy (Deut. 5-26).

Ethics: The Cause/Effect Principle

The third section of every suzerain treaty consisted of special terms,
or laws.1 The treaty was ethical at its core. The suzerain and vassal(s)
related to one another according to the stipulations laid out. Tf the vassal
were to be consecrated to the suzerain, he had to comply with them.

Tn the ethics section of the Biblical covenant (Deut. 5-26), we
find an expansion of the Ten Commandments (stipulations). ‘The
central idea: God establishes an ethical relationship between cause and
effect. Since God and man do not have unity of being, God dictates
the terms (commandments) under which man can have a relation-
ship with Him, These terms are the standard of the covenant. Man
is called to be faithful to God by submitting to them. If he submits
(cavenant-keeping), he is blessed. If he does not (covenant-breaking),
he is cursed. (The specific results of blessing and cursing appear in
point four of covenantalism, called sanctions). He is either a covenant-
keeper or covenant-breaker. So, there is an ethical as opposed to a
metaphysical relationship to God, and this relationship culminates
in certain predictable effects. And, I might add, these effects are not
just personal; they are cultural.

Everything in the universe is tied to man’s covenant-keeping or
covenant-breaking. This basic concept of ethics is presented in Deu-
teronomy 5-26. Moses summarizes the Ten Commandments saying,
“You shall walk in al! the way which the Lord your God has com-
manded-you, that you may live, that it may be well with you” (Deut.
5:33). At another place, a passage from which the title of this hook
was taken, he says, “Keep the words of this covenant to do them that
you may prosper in all that you do” (Deut. 29:9).

The ethical cause/effect principle is a command/fulfillment pattern.
The books of Joshua and Judges are examples. The Book of Joshua-
follows the Book of Deuteronomy. It demonstrates that obedience
(fulfillment) to the commandment results in blessing. How? Simple:
Joshua obcyed Moses’ commands, and the promise was fulfilled. He
received the land. Even in the particulars we see the same pattern,
like wheels within wheels. In the first chapter, God tells Joshua to
enter the land and take it ( Josh. 1:1-9). Then Joshua tells the officers
of Israel precisely what God told him ( Josh. 1:10-18). Finally, the people

 

 

1. Kline, Structure of Biblical Authority, pp. 115f. See also, Ancient Near Eastern Texts,
pp. 2054.
