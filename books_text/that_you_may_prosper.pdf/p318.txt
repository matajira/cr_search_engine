298 THAT YOU MAY PROSPER

Conclusion

Here is the end of the Old Covenant. We have called this chapter
the “Covenant in the Flesh.” Circumcision and Passover were de-
signed to restore Israel, but they could not. They went the way of all
the Old Covenant, the way of the flesh, Christ’s appearance in his-
tory encounters a nation and world that had been consumed by the
flesh. His circumcision/death was the only thing that truly changed
the world.
