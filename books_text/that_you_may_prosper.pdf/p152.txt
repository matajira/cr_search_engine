132 THAT YOU MAY PROSPER

assimilate Christianity (o Chinese patterns of civilization. They were care-
ful to dress in the robes of Chinese mandarins, and their object was to show
that Christian doctrine and Confucianism were compatible and supple-
mented each other, ... Their efforts, after some initial success, were
brought to nothing, not by Chinese hostility, hut by the opposition of
Ghristians in Europe.’

Eventually, the missionaries were brought home because they
were converting to Confucianism — the Chinese brand of situational
ethics! This type of approach has been taken in the Third World for
a long time. For 150 years there has been intense missionary activity
in the Third World. Today, the Marxists are kicking out the Church.
How have they gained such power? Because missionaries have not
evangelized, or have not been able to evangelize, with a complete
Biblical world-and-life view. They have brought a gospel without
law, and this “lawless gospel” has not changed the world. If anything,
the resistance to Christianity bas been made worse, not better. The
missions and churches have allied themselves to a now-collapsing
humanist status quo. The Marxists are overthrowing the status quo,
and they are steadily removing the churches. They are systematic-
ally teaching the West that culture is not neutral. Although there will
be distinctives of each land, and Biblical law is necessarily cast in
many forms, Biblical culture is a culture based on the Word of God.

For example, clothing may be decorated differently, but there is
clothing! Representative government may have a “parliament” in-
stead of a “congress,” but there is government by representation.
Homosexuality is homosexuality in any land, and its unrepentant
practitioners deserve the death penalty (Rom. 1:18-32; cf. “worthy of
death” in v. 32), AIDS is AIDS in any land, and it threatens whole
populations. Christ says the “nations” are to be discipled by His law.
The skill of the missionary is in seeing God's law asserted over the na-
tion he is evangelizing. This has many expressions, but if is expressed!

This is the third section of the Great Commission Covenant.
Notice that there is an overlap in this part of the covenant. The spe-
cific way to disciple the “nations” is through baptism and instruction
(vv, 19-20a). Making a disciple begins at baptism, which is receiving
the symbolic, yet covenantally real, sanctions of Christ. Then the

7. Geoffrey Barraclough, ed., The Christian World: A Social and Cultural History
(New York: Harry N. Abrams Publishers, 1981), p. 29. Barraclough’s view of history
is Marxist — class struggle etc. —but he docs have some interesting insights.

 
