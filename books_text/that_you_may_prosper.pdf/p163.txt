The Biblical Family Covenant 143

gently to your sons and shall talk of them when you sit in your house and
when you walk by the way and when you lie down and when you rise up.
And you shall bind them as a sign on your hand and they shall be as frontals
on your foreheads. And you shall write them on the doorposts of your house
and on your gates (Deut. 6:6-9).

I do not believe Moses intended these verses to be taken aceord-
ing to the Talmudic Jewish tradition, writing the Ten Command-
ments on little pieces of paper and nailing them in special boxes to
the doorposts of one’s house. The family was to be taught the law of
God: what it was, and how it applied.

Solomon reiterates this message to families, devoting the entire
book of Proverbs to “wisdom” (hocmah). What is wisdom? It is the
ability to think and apply God’s Law. In Proverbs, Solomon tells
sons to listen to their father’s instruction in the “commandments” of
God.

My son, if you will receive my sayings, and treasure my commandments
within you, make your ear attentive to wisdom, incline your heart to
understanding; For if you ery for discernment, lift your voice for under-
standing; If you seck her as silver, and scarch for her as for hidden trea-
sures; Then you will discern the fear of the Lord, and discover the knowl
edge of God (Prov, 2:1-5),

The ethics of the family is the covenantal commandments of
God. Throughout this book, we have seen the relationship between
the covenant and ethics, the Ten Commandments. Solomon says
that God’s ethics should be so internalized that one’s “natural”
response is obedience to His Law. This is the task of the family and
parents.

Sanctions (Gen. 2:24)

Next in Genesis 2 the text says, “For this cause a man shall leave
his father and mother and cleave to his wife, and they shall become
one flesh” (v. 24). Moses adds his own editorial comment (under the
inspiration of God), which is judicial in character. All of the lan-
guage here implies some kind of legal process of oath-taking to estab-
lish a marriage union.

“Leaving” (azab) implies the termination of a covenant bond, the
same Hebrew word being used of apostasy from the covenant (Deut.
28:30; Judges 10:10; Jer. 1:16), The covenant being ended is the par-
ental bond. A new covenant or bond is formed by “cleaving” (dabag).
