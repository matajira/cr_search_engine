Continuity 115

First, there is no such thing as a “natural” catastrophe. Nature is
not neutral. Nothing takes place in nature by chance. God governs
all things. Not even a bird falls to the ground that God does not con-
trol (Matt. 10:29), What happens in nature happens as a result of
God’s involvement with the physical universe. Catastrophes are in-
cluded, Although we may not know the exact sin being judged, what
occurs results from God. Often, God may send a series of natural
judgments to warn a whole civilization that it is about to be judged
in greater finality. It is remarkable throughout the history of man
how such things come about shortly before the fall of a great empire.
Many of those are happening in the West today: herpes, the AIDS
plague, cancer, soil erosion, drought, and so on. Perhaps God is
warning that a more comprehensive judgment approaches.

Second, God uses a dex talionis system of judgment. Lex talionis
means an “eye for eye” and “tooth for tooth.” He always secures His
restitution. When the evil Haman tried to have Mordecai hanged,
God punished Haman by having him hanged from his own gallows
(Esther 7:1-10). God judges in kind. If millions of babies are mur-
dered before they are born, I think we can expect God to render lex
talionis. One provision should be made, however. God gets His resti-
tution through conversion as well as destruction. When one is con-
verted, he dies in Christ (Rom. 6:2-7). Conversion has a killing-
unto-life effect. Therefore, the West is cither standing on the edge of
the greatest revival or the greatest disaster of its history. Perhaps it
will be both: disaster followed by revival. The Reformation began in
1517, just 24 years after Columbus’ crew brought back syphilis and
began a massive, deadly infection of Europe.

Third, most modern Christians only think of judgment in terms
of the individual. But God destroys groups as well as individuals.
Moses emphasizes covenantal judgment when he says, “I will heap
misfortunes on them; I will use My arrows on them. They shall be
wasted by famine, and consumed by plague and bitter destruction”
(Deut. 32:23-24). The third person plural pronoun indicates that
God judges the group. Judgment of entire households serves as a recur-
ring Biblical example. God judged the households of Korah, Dathan,
and Abiram for rebelling against Moses’ leadership (Nu. 16:32).'*

  

14, Korah was alsa involved with Dathan and Abiram. But God often gives the
family members an opportunity to distance themselves from the sins of the one
bringing judgment on the whole family. Evidently, some of Korah’s children did not
stand with him. They did not die (Nu. 26:9-1!). Moreover, later in the Bible their
descendents stand and “praise the Lord” (Tf Ghron. 20:19).
