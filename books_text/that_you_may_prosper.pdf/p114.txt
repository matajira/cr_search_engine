94 THAT YOU MAY PROSPER

of our children are actually to be adopted. They are entrusted to par-
ents by the covenant.

‘The doctrine of adoption is totally contrary to blood-religion, not
to be confused with the Biblical requirement of blood sacrifice. Non-
covenantal religions sometimes advocate salvation by dloedline, in-
stead of blood sacrifice. Bloodline religion is racist. One race is sup-
posedly purer, or better equipped, than all the others, whether the
Arian race as advocated by the Fascists, or the Jews as proposed by
the Zionists. All racists are the same. They believe their race or even
their nation is special because of its ethnic distinction, Faithfulness
to God's covenant has nothing to do with salvation, only faithfulness
to the race, This inevitably leads to war, because impure races have
to be destroyed. They are a threat to the pure race.

The Jews had forgotten the covenant. They thought their race
was superior. If they had believed in the covenant, however, they
would have known there was nothing special about their blood. It
was like all the other bloodlines. Their greatness had been due to
covenant. And they were certainly not part of the covenant because
of their bloodline. Jesus reminded them of this fact when He said,
“But as many as received Him, to them He gave the right to become
children of God, even to those who believe in His name, who were
born zef of blood, nor of the will of the flesh, nor of the will of man,
but of Ged” (John 1:12-13). They were adopted sons. Thus, if the Bib-
lical covenant is rejected, man easily becomes a racist!

Conclusion

This concludes our analysis of the fourth principle of the cove-
nant, sancttons. The covenant is established by the reception of these
sanctions through an oath before witnesses. First, the oath has dual
sanctions, blessing and cursing, giving the covenant terms of uncon-
ditionality. When the Bible speaks of faith, it means farthfulness. Man.
is required to persevere! If he fails to do so, the oath that God takes
on Himself will hit the participant in the form of a curse. Along with
the terms of unconditionality, the covenant is both promissory and
legal. Grace and law are not in conflict.

Second, the oath is received through the consignment of a self-
maledictory oath. God pledges Himself to the participants, attaching
blessings and cursings. When man places his “amen” on the cove-
nant, he receives what God has donc. He says “amen” through cer-
tain symbols, and has them applied to his household.
