‘True Transcendence 23

He asked Belteshazzar (Daniel) what the dream meant. It meant
bad news for the king. The tree was the king himself. One of God’s
angels would cut hirn down, He would be driven into the fields, to
eat grass with the beasts, for seven years. Bul there was hope in that
vision, Daniel said, for “your kingdom will be assured to you after
you recognize that it is Heaven that rules” (Dan. 4:26b).

Daniel then offered the king a way out of this prophecy. Phe way
out is always the same for every person: obey God. “Therefore, O
king, may my advice be pleasing to you: break away now from your
sins by doing righteousness, and from your iniquities by showing
se there may be a prolonging of your pros-

  

 

mercy to the poor, in ¢
perity” (Dan, 4:27).

The king forgot, as men so often do. A year later, he was walking
on the roof of his royal palace. He saw himself as if he were God
Almighty —a familiar sin in the history of man, from Adam to the
present:

The king reflected and said, “Is this not Babylon the great, which I
myself have built as a rayal residence by the might of my power and for the
glory of my majesty?” While the word was in the king’s mouth, a voice came
from heaven, saying, “King Nebuchadnezzar, to you it is declared: sover-
eignty has been removed from you, and you will be driven away from man-
kind, and your dwelling place will he with the beasts of the field. You will be
given grass to eat like cattle, and seven periods of time will pass over you,
until you recognize that the Most High is ruler over the realm of mankind,
and bestows it [sovereignty] on whomever He wishes” (Dan. 430-32).

The dreams promised judgment came at last. But so did the res-
toration. After seven years, the king’s sanity was restored to him by
God, and he immediately praised God as the absolute Sovereign of
the world: “For His dominion is an everlasting dominion, and His
kingdom endures from generation to generation” (Dan. 4:34b).
Then the king sat on his throne again: “. . . so J was reestablished in
my sovereignty, and surpassing greatness was added to me. Now 1
Nebuchadnezzar praise, exalt, and honor the King of heaven, for all
His works are true and His ways just, and He is able to humble
those who walk in pride” (Dan. 3:36b-37).

But that was nothing compared to an even greater gift lo that
once-pagan Babylonian king. For what other king over a pagan em-
pire ever had bis own words recorded as the very Word af God to
man? Not one. The fourth chapter of Daniel is the only chapter in the
