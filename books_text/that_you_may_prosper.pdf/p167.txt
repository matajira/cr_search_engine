3
THE HISTORICAL FAMILY COVENANT

In our society, there are three basic spheres of life: Family,
Church, and State. All three are covenants: legal institutions formed
by self-maledictory oaths. Their importance is obvious. In this chap-
ter I want to apply what we learned about the Biblical covenant to
history.!

Modern man tends to forget that marriage is a covenant. It used
to be that two people did not get marriage licenses. Rather, they drew
up marriage contracts, and filed them at the city courthouse. Why
don’t people do this today? The answer is simple. Contracts make tt too
hard to secure a divorce. A contract (covenant) is much more binding
than a license. In the following, I have reproduced an old marriage
contract from 1664/1665.2

Marriage Contract
January 20, 1664/65
Plymouth, Massachusetts

This writing witnesses, between Thomas Clarke, late of Plymouth,
yeoman, and Alice Nicholls of Boston, widow, that, whereas there is an in-
tent of marriage between the said parties, the Lord succeeding their inten-
tions in convenient time; the agreement, therefore, is that the housing and
Jand now in the possession of the said Alice Nicholls shall be reserved as a

 

1. Edward McGlynn Gaffney, “The Interaction of Biblical Religion and Ameri-
can Constitutional Law,” The Bible in American Law, Politics, and Political Rhetoric, ed.
James Turner Johnson (Philadelpha: Fortress Press, 1985), pp. 81-106. Gaffney’s
thesis is that the suzerain form of the covenant was influential on Constitutional law in
this country.

2. The reason why there are two years listed is that prior to the late 1700s, the
new year came in mid-March. This is still reflected in the natnes for several months:
SEPTember, OC'Tober, NOVember, and DECember—seven, eight, nine, and ten
—when today they are the ninth, tenth, eleventh, and twelfth months, The second
year listed is the year we reckon it in retrospect, according to our calendar:

147
