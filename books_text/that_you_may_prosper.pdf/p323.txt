Covenant By the Spirit 303

Blessing

Water, however, is more than something which symbolically and
covenantally kills. Water symbolically and covenantally resurrects.
Water symbolizes the greatest dlessing that could ever come to man-
kind: life victory over sin and death. Paul says, “For if we have become
united with Him in the likeness of His death, certainly we shall be
also in the likeness of His resurrection” (Rom. 6:5).

Circumcision killed the flesh and adopted the recipient into the
household. In this sense, it resurrected. But circurncision, as we saw
at the end of the Old Covenant, failed to resurrect Israel. Christ’s
death went beyond circumcision, in that it actually resurrected the
New Israel covenantally (representationally). This is implied in the
symbolism of water in the Old Testament. When someone touched a
dead animal, the law said,

The one who touches the corpse of any person shall be unclean for seven
days. That one shall purify himself from uncleanness with the water on the
third day and on the seventh day, and then he shall be clean; but if he does
not purify himself on the third day and on the seventh day, he shall not be
clean. Anyone who touches a corpse, the body of a man who has died, and
does not purify himself, defiles the tabernacle of the Lord; and that person
shall be cut off from Israel. Because the water for impurity was not sprinkled
on him, he shall be unclean; his uncleanness is still on him (Nu. 19:11-13).

Such a ritual anticipated New Covenant baptism in several
ways. First, this Old Testament Aaptism is called a baptism (Heb.
9:10). In the great chapter on the Resurrection, Paul says, “What
will those do who are baptized for the dead? If the dead are not raised
at all, why then are they baptized for them?” (I Cor, 15:29). The
“baptism for the dead” was none other than the Old Covenant bap-
tism in Numbers 19. When someone touched anyone who had died,
he had to be symbolically resurrected, because death spread to death in
the Old Testament.

Second, notice that this baptism was applied on the third day.
Jesus was raised on the third day. He was the complete fulfillment of
what Old Testament baptism typified, the resurrection,

Third, this baptism symbolically came from adeve, which is im-
plied in the sprinkling. Resurrection is symbolized by the sprinkling
or pouring mode. Notice that all throughout the Bible, only the
pagans are drowned, or zmmersed, How were the sons of Cain killed
