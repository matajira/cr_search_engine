Five Points af Covenantalism 123

of one of the points of covenantalism out of place, or see each point
in every category, the broader context follows the five-fold pattern of
Deuteronomy. Furthermore, the repeated usc of the five points of
covenantalism becomes a cross-check on itself.3

Gonclusion

Deuteronomy has been used as the model of covenantalism,
because it is the most systematic presentation in Scripture. It is to
the covenant what Romans is to doctrine. But is this structure practi-
cal? How does it shape our view of the world? I have tried to show in
these brief introductory chapters how the covenant relates to other
parts of the Bible. Now I want to shift our focus to how the covenant
works in society, I believe it is the model for how the Christian is to live
in and evangelize society. Let us begin with a brief introduction to
this idea. Let us start with perhaps the most basic commission given to
the Christian, the Great Commission. In the next chapter we will see
that our Lord presents it in the very covenantal framework we have
been studying. This means the mandate of Christ is really dominion
by covenant.

3. See appendixes one through six for a complete study of other passages that
serve to cross-check my study of Deuteronomy.
