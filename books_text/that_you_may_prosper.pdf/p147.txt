Dominion By Covenant 127

1:28), in other words, the sanction point of the covenant. As it so hap-
pens, just before the mandate, we read, “God blessed them,” a sanc-
tion concept (blessing). Therefore, it would seem that the cultural
mandate was intended to be a process whereby the world was shaped
into a “cultus” by covenant ratification. But also noticc that the fourth
creation word (second series) is a virtual repetition of the one before
it, “Let them rule.” Why? I have stressed throughout the book that
ethics, or faithfulness to the covenant, establishes dominion. The
slight variation in Genesis, however, means the stipulations must be
ratified before the full dominion process is completed. Indeed, this is
the first thing Adam does with the woman. He brings her under the
stipulations by a special oath, “This is now bone of my bones” (Gen,
2:23).

The covenantal design of the cultural mandate is unmistakable.
Had Adam obeyed, he would have completed his covenantal com-
mission. But the Fall interrupted everything. The seventh day, the
day of special blessing, again a sanction word, was turned into a day
of cursing. The Day of the Lord became a day of judgment. Ironic-
ally, as we have already seen, “curse” not only judged the world’s re-
bellion to God’s first covenantal mandate, it also provided a means
for redemption to emerge, ultimately of course in the death of
Christ. His death accomplished the original cultural mandate by re-
ceiving the curse-sanclion, and introduced the blessing-sanction
back into the world. This appears in a special way at a final meal He
conducts, just before His ascension. It is here that He gives a new cul-
tural mandate, the Great Commission.

The Great Commission

The Great Commission is a covenant within a covenant. Look-
ing at the life of Jesus, it falls within the continuity section. Jesus
gathered His disciples to a communion meal, and prepared them for
His final dlessing (Luke 24:41-51). In this context, He gave the bless-
ing in the form of a new commission, just like Moses had done with
the nation. Jesus had reached the point of giving the adopted heirs
their new inheritance. What was it? The content of the Great Com-
mission tells us. The disciples were given the “nations” of the world
to bring to Christ, just as Isracl of old had been given the land of Pal-
estine and surrounding territories. The New Covenant people were
given the world. How? Their dominion mandate was specifically laid
out in the form of the five points of covenantalism,
