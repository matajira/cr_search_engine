TABLE OF CONTENTS

Foreword by Milion C. Fisher... 0.00000 cece eee es ix
Publisher's Preface by Gary North

Introduction ©0000... 0... ccc een es 1

 

1. COVENANT
Covenantal Points
1, True Transcendence .....-. 2.2.0.2 e ee eee eee 21
2. Hierarchy .
3. Ethics ....
4
5.

  

. Sanctions .
. Continuity..........
6. “Five Points of Covenantalism” (Conclusion: Part 1)... 119

 

Il. DOMINION

   
  

Introduction
7. ‘Dominion By Covenant”.........0..0.. 0.0000 es 194
Family
8. The Biblical Family Covenant..............000085 137
9. The Historical Family Covenant............-.-.-5 147
Church
10. The Biblical Church Covenant ...............0065 159
11. The Historical Church Covenant ................. 167
State
12. The Biblical State Covenant ..................005 177
13. The Historical State Covenant ...........0- 200 eee 195
Conclusion
14. “Little By Little” (Conclusion: Part 2).............. 202
Appendix 1—The Ten Commandments...............0-4+5 214
Appendix 2—Psalms . 225
Appendix 3— Matthew. . 233
Appendix 4— Romans . . 246
Appendix 5— Revelation .......... 5.000 ce seer e ee eee eee 253
