Appendix 9

COVENANT BY THE SPIRIT
(New Covenant Sacraments)

 

The sacraments of the New Covenant arc especially ticd to the
Holy Spirit. The Old Covenant sacraments were in the “flesh” (i.e.
“foreskin”) and ended in death. But when Christ died and rose again,
the Holy Spirit was poured out on the earth as He never had been
before. Does this mean individuals, prior to Pentecost, did not have
the Holy Spirit? No, the Holy Spirit was “hovering” over the original
creation (Gen. 1:2), leading Israel out of bondage from Egypt (Deut.
32:10ff.) and guiding the individual leaders of God’s people. Jesus
told Nicodemus that he needed to be “born of the Spirit” (John
3:1-5), When Nicodemus did not understand, Jesus rebuked him
saying, “Are you a teacher of Israel, and do not understand these
things?” (John 3:10), Being born again of the Spirit was nothing new.
Nicodemus should have understood.

The difference between the Spirit's work in the Old and New
Covenants is that the Spirit was given to the whole earth, to Jew and
Gentile in the New Covenant. This was something ew. Again it
should be understood that it was not as though the Spirit had never
gone to an “individual” Gentile, or individual Gentile nations (i.e.
Assyria). There are plenty of examples in the Old Testament where
a Gentile became a belicver (Ruth 1; II Kgs. 5:1ff.). But never be-
fore, at least since the Tower of Babel, had Gentiles been made a
part of God’s priesthood. Because the Spirit went to the Gentiles, spe-
cial signs, like speaking in tongues, accompanied it. Addressing the
Church at Corinth, Paul says, “In the Law it is written, ‘By men of
strange tongues and by the lips of strangers I will speak to this peo-
ple, and even so they will not listen to Mc,’ says the Lord. So then
tongues are for a sign, not to those who believe, but to unbelievers” (I
Cor, 14:21-22). Who were these unbelievers? Gentiles! Why was a
sign needed? Because the Spirit of God was coming to them as well.

299

 

 
