Introduction 7

These colonial Christians did not believe in works salvation,
rather in a salvation that works. Miller remarks that their view of
ethics included more than “individual honesty and charity; it in-
cluded participation in the corporate organization and the regulation
of men in the body politic.” Men were judged, in other words, on
the basis of behavior, no mattcr what their status. This gave “good”
people a true chance to have upward mobility, something they could
have never had as readily in the Old World.

Fourth, the covenant implemented a system of sanctions based
on an oath. Once an oath was made, a man was expected to keep it.
Any violation met serious sanctions. Perjury in the realm of the
State was in many cases punishable by death. Adulteration of the
marriage oath met the same end. Apostasy from the Church cove-
nant resulted in banishment. The oath and the sanctions that en-
forced it were an effective stabilizing factor in American culture.

Fifth, the covenant implied a system of continuity based on some-
thing other than blood relations. The Puritans attempted to make
experience the test for church membership. Also, a person had to be
a member of the Church to be able to vote. Granted, these were mis-
applications. The first misapplication—experientialism—led to the
Halfway Covenant, in which the grandchildren of church members
were baptized, even though their parents had never joined the
church formally. Though baptized, these “halfway covenant” chil-
dren were not regarded as church members.” The other misapplica-
tion—political—corrupted the Church. Nevertheless, we do find
hints of an extremely important aspect of a society rooted in contract
and not in status. The mechanism of contract provided social contin-
uity, and not bloed or class. America, more than any other culture,
had become a place of opportunity for the “little guy.”

These serninal ideas of the covenant shaped American society.
They created the strongest nation in the history of man, As they
have diminished, so has every sphere: Family, Church, and State. A
brief overview of these institutions in light of our five basic concepts
demonstrates what the loss of covenant has done to modern society.

16. sbid., p. 89.

17. Rdmund §, Morgan, Visible Saints (Ithaca, New York: Cornell University
Press, [1963] 1968). Morgan argues that the “experiential” tests for church member-
ship that they introduced in the early 1630s actually militated against their cove-
nantal theology, Instead of having “objective” tests, they becaiue overly subjective.
They tried to do what only God can do, that is, look into men’s hearts.
