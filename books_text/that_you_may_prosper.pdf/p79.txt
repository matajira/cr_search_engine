C. Third Point of Covenantalism

3

ETHICS
{Deuteronomy 5-26)

One of the classic contrasts of history took place at the giving of
the Ten Commandments. Moses had gone up Mt. Sinai to receive
the most succinct statement of God’s ethical system ever codified: the
Ten Words (Deut. 4:13).

Down below, at the foot of the mountain, the people waited for
Moses to return. The Bible does not tell us how long Moses was on
the mountain. The text only says that he “delayed” (Ex. 32:1). It did
not take long, however, for trouble to begin to brew.

The people were anxious. ‘Their commitment to a Biblical world
was shaky. How ironic! The only world they had known was the
“Gulag” of an Egyptian concentration camp. Their grandparents
had handed down stories of the “way it used to be,” but they had
never seen the better side of Egyptian life. Still, it was all they had
known, and Egypt continued to represent “home.”

What did they miss? What was the first thing they asked for from
Aaron, the man “tending the store” while Moses was gone? Strange
as it may sound, they wanted an idol, a god they could see. They
wanted a god they could manipulate through a golden mediator.
Somehow they persuaded Aaron to join them. It was dreadful. A
man of God, a leader, the High Priest, who had seen God defeat the
mightiest nation in the world, without a single arrow fired, was ca-
joled to forsake everything he had known to be true about Biblical
religion. The ordained mediator of God’s hierarchical system built a
dead, blind, metallic mediator for the people.

While Moses lay prostrate before his Creator, and as God etched
eternal law into stone, Aaron and the people below built and wor-
shipped a golden calf.

What is the contrast of this eventful day in history? It is the con-
flict between ethical and magical religions. Biblical religion is based on
faithfulness to the Word of God (ethics). Pagan religion is always

59
