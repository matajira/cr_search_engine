28 THAT YOU MAY PROSPER

tion, a certain legal status is imputed to the relationship. We can call
this the doctrine of imputation, a legal term meaning “to apply to the
account of.”

How does imputation work? God told Adam that the day that he
ate of the tree of the knowledge of good and evil, he would die (Gen.
2:17). He ate, yet he did not paysically die at the precise moment he
ate the “apple.” Then in what sense did he die? Some people try to
explain his death as “spiritual.” But the Bible does not speak this
way. A better explanation is that Adam's death was covenantal, in that
God imputed death to him, God counted him as dead because of the
broken covenant. Then, as Adam experienced the burdens of his-
tory, he would draw closer and closer to physical and perhaps even
eternal death. He would see the covenantal applications of death in
history. Those manifestations of covenantal death would be all
around him throughout his life, Imputation went from life to death:
from Adam's physical life to Adam’s eventual physical death.

Imputation worked the other way too: from death to life. How
could Adam be allowed by God to live? How could he fegally escape
the immediate judgment of God? Because God looked forward in
time to the death of Christ. Christ’s death satisfied God’s legal re-
quirement that Adam be destroyed that very day, body and soul.
Adam may or may not have been saved in the sense of eternal salva-
tion, but he surely was saved from immediate physical death. God
imputed earthly life to him — the life which Christ earned on the cross.
He then gave Adam and Evc a promise concerning the future (Gen.
3:15). Christ’s death had assured that future, and the promise spoke
of Christ (the seed) crushing the head of the serpent.

We find this same concept of “applying to the account of” in the
Biblical concept of redemption, What does it mean to redeem some-
thing? It means fe buy tt back. In the case of God’s redemption of His
people, He applies Christ’s “account paid” certificate to each Chris-
tian’s debt to Him. He imputes Christ’s righteousness to us. And He
could never impute redemption unless He is transcendent and pos-
sesses transcendent authority. This is why some of the Pharisees
were so angry when Christ declared “Your sins are forgiven” to a
man, They asked in their hearts, “Why does this man speak this
way? He is blaspheming; who can forgive sins but God alone?”
(Mark 2:7). They fully understood that He was claiming true tran-
scendence for Himself.

Thus, covenant theology says that man’s relationship with Gad is
