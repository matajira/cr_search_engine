6
FIVE POINTS OF COVENANTALISM

At this point it is good to stop and summarize what we have
done. Essentially, I have attempted to define the covenant by using
Deuteronomy as a model.

Perhaps the reader notices that I have not resorted to any partic-
ular “etymological” or word study. I agree with O. Palmer Robert-
son’s conclusions, former professor of Old Testament at Westminster
Theological Seminary, when he says, “Extensive investigations into
the etymology of the Old Testament terms for ‘covenant’ (berith)
have been proven inconchusive in determining the meaning of the
word,”! Trying to “word study” one’s way to a conclusive under-
standing of the covenant is, as Delbert Hillers has pointed out, “Not
the case of six blind men and the elephant, but of a group of learned
paleontologists creating different monsters from the fossils of six sep-
arate specics.”? In short, this is “dry bones” scholarship!

Even so, we are not left in the dark about this basic concept. I
have used a broader and more contextual approach, Deuteronomy is
so systematic that it has been the basis of my conclusions. Five prin-
ciples have been isolated by comparing Deuteronomy with suzerain
treaties and reflecting on the details of the Biblical text itself.

1, Q. Palmer Robertson, The Christ of the Covenants (Grand Rapids: Baker, 1980),
p. 5. Robertson has an excellent summary of various etymological findings in such
sources as Moshe Weinfeld, Theologisches Worterbuch zum Alten Testament (Stuttgart,
1973), p. 73; Leon Morris, The Apostolic Preaching of the Gross (London, 1955), pp.
62ff.; Martin Noth, “Old Testament Covenant-Making in the Light of a Text from
Mari? in The Laws in the Pentateuch and Other Essays (Edinburgh, 1966), p. 122; E.
Kutch, “Gottes. Zuspruch und Anhspruch. Berit in der alttestamentlichen
Theologie,” in Questions disputes d’'Ancien Testament (Gembloux, 1974), pp. 71;
Kuteh simply revives the basic idea of one of the earliest covenant studies by Johan-
nes Cocceius, See study by Gharles Sherwood McGoy, The Covenant Theology of
Johannes Coccetus (New Haven, 1965), p. 166. Anyone wishing to see the various
views that have been taken on the basis of pure etymology can consult these sources.

9. Delbert R. Hillers, Covenant: The History of a Biblical Idea (Baltimore, 1969), p. 7.

ng

 
