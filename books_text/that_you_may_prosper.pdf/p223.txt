Little By Little 203

But there is one striking contrast. Joshua took the land by use of the
sword, even though it played a secondary role, None of the Apostles
used the sword to spread the Gospel. Why the difference? Joshua,
although a type of Jesus Christ, was under the Old Covenant. The
Old Covenant was a covenant of the flesh, graphically portrayed in
the sacrament of circumcision.! And, if anything, the Old Testa-
ment teaches that the kingdom of Ged could not be established in the
flesh, meaning by the sword. ‘he garden of Eden was scaled off by a
“flaming sword” (Gen. 3:24), prohibiting re-entrance. Man could not
return to that particular garden by a carnal weapon because his
sword could not stand against God’s. Even David, a great man of
God, was unsuccessful in creating God’s kingdom. He was a man of
war, so he was not allowed to build the Temple (I Kgs. 5:3). When
the disciples asked Jesus, “Lord, is it at this time that You are restor-
ing the kingdom to Israel?” (Acts 1:6), they were expressing their
confusion about the nature of the kingdom of God. They still
thought it would be a political order, that is, a kingdom established
by the sword.

‘They were wrong. The next verses in Acts speak of a new
regime. The New Covenant kingdom is created by the Spirit. God
had conquered Jericho by His might, to be sure. But the Holy Spirit
had not come in all of His historical fulness. Christ had not yet come
in history. Israel needed to use the sword, but Israel ultimately failed.
The Church succeeded. In Acts, the Spirit of God went forth and
created the beginnings of a Christian world from the beitom up.

The instrument the Spirit used was evangelism, witnessing. The
role of the witness is twofold. Positively, he stands before men and
the courts of the world, and he testifies of the Resurrected and Liv-
ing Christ, Peter, John, Stephen, and Paul all became witnesses in
the courts of man, Indeed, Acts tells how God sent them before
Jewish and Roman courts, and even into prison for this reason. The
task of evangelism is the challenge of being a witnesses in the unbe-
liever’s place of holding court. As we see in Acts, this can be a place
of education or doing business, as well as an official court for passing
judgment (Acts 19 and 16). The idea is that God sends His witnesses be-
fore man’s seats of judgment to proclaim God's judgment, particularly
through Jesus Christ.

Much has been said and written about evangelism, so 1 will not

1. See Appendix 7.
