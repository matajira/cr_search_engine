The Historical State Covenant 199

enant directly with God. The document reads, “Doe by these pres-
ents solemnly and mutually in ye presence of God, and one of
another, covenant & combine ourselves togeather in a civill body
politick.” This was a covenant that they, the people, entered into
along with the king. They realized that what was happening was a
break with the system they had known in the past. Eventually, this
kind of covenantal politics would stir up England, in the situation
between Charles I and the Puritans to which I referred earlier.

Sanctions

Dread Soveraigne Lord.

The Mayflower Compact is a judicial covenant. The signers of
this compact knew they were under God’s judgment; they were
under a self-matedictory oath. The opening statement reads, “We whose
names are underwritten, the loyall subjects of our dread soveraigne
Lord.” Their belief was that the God of the Bible is a God of judg-
ment, They knew that if they violated the covenant, God would
judge them. We know from history that the early colonists set up a
society based on judgment. They re-established the sanctions of Bib-
lical law. We know from John Cotton’s Abstract of the Laws of New
England (1641) that crimes such as homosexuality, adultery, witch-
craft, bestiality, and murder were all punishable by death.? This was
a society of judgment.

Yet it was not a repressive society. Only someone who regards
living under God’s revealed laws as repressive would call Puritan so-
ciety repressive. Their mistakes with respect to human freedom were
nothing compared to modern humanism’s calculated policies in the
name of freedom. One has only to read the diaries of those carly
Puritan children. They were a people who wrote diaries, They
wanted to write down all that God was doing in their lives. These
diaries make it apparent that Pilgrim and Puritan children were ex-
tremely happy. Why? Because they were taught to face judgment
by believing in Christ. When people deal with judgment the proper
way, they can be “happy.”

The Mayfiower Compact is a sample of a judicial document that
reflected a certain view of God. This is the whole point. People who
believe in a God who judges can create a society founded on law and
judgment. Capital punishment is one of the best evangelistic tools of a society,

3. “Abstract of the Laws of New England”; reprinted in Journal of Christian Recon-
struction, V (Winter, 1978-79), pp. 82-94.
