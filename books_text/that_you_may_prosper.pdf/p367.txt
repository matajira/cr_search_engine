Index 347

sanctions and, 86
Syphilis, 115, 308

Tamar, 109

Tax Exemption, 12

Taylor, John, 187

Temple, 229

Tenancy, 88

‘Yen Commandments
creation ten words and, 125-26
covenantal structure, 214ff.
priest/king distinction, 223
structure of, 17.34, 64
structuring problems, 215-16
summary of, 60

‘Theocracy, 191

Theomorphe, 44

Third Reich, 10

Third World, 132

Thirty-Nine Articles, 5n.l1, 317f.

Thoburn, Robert L., 9n.20, 193n.13

Totem pole, 36

Transcendence
American history and, 6
authority and, 45
counterfeits of, 32-39
covenant and, 32ff.
creator/creature distinction, 244.
Cultural Mandate, 125-26
definition, 35
Deuteronomic covenant, 16
false, 33
first point of covenantalism, 21-40

‘Transfiguration, 275

Trench, R. C., 256

Trinity
name of God, 88
sacraments and, 88ff.
summary, 120

Tulchin, Joseph S., 101.25

Tasker, R. V. G., 236n.3

 

Upper room, 227
U.S. Constitution
authority structure, 200n.4
covenant structure of, 200-201
Ussher, James, 5ntL
Utopianism, 70

Van Tit, Cornelius, 26, 34, 35n.7-8,
46, 62, 89n.16, 134, 309n.3

Van Til, Henry, 1350.7

Vassal, 83

Veitch, John, 69n.8

Vigilante, 11

Visible covenant, 92

Visible sovereignty, 43

Voltaire, xvii

Von Hippel, Ernst, 10.26

Von Rad, Gerhard, 14n.29, 84, 84n.7

Vos, Gerhardus, 235n.2, 273n.3

War, 51-52
Warficld, Benjamin, 5n.9
Washington, 188
Wealth, 67
Weaker brother, 250
Weinficld, Moshe, 119n.1
Wenham, Gordon, 85n.9
Westminster Confession, xvii, 5n.7-8,

62
Whitehead, John, 10n.24, ti
Wilderness, 43, 98, 227-28
Williams, Roger, 185, 190
Witchcraft, 42
Witnesses

covenant and, 91

lawsuit and, 91

sanction and, 91
Works

faith vs., 62

justified by, 66
Worldview

covenant and, 32
World War II, 10, 63
Worship

cultus, 125

Ten Cammandments and, 217
Wrath, 120

Xerxes, 151

Yates, Francis, 74, 75n.16
Young, Brigham, 153

Zimmerman, Carle, 139
Zionists, 94
