Appendix 5
REVELATION

The Deuteronomic model of the covenant is carried into the New
Testament. Two prime examples are Matthew and Romans. Now,
last but not least is perhaps the clearest presentation of the covenant
in the New Testament. The Book of Revelation convenicntly falls
into five sections.

The Covenantal Structure of Revelation

True Transcendence (Preamble): 1:1-20

Hierarchy (Historical Prologue): 2:1-22 (7 churches)

Ethics (Stipulations): 4:1-7:17 (7 seals)

Sanctions (Ratification): 8:1-14:20 (7 trumpets)

Continuity (Succession Arrangements); 15:1-22:21 (7 bowls)

Understanding Revelation as a covenant is the single most help-
ful insight about its structure. How so? The Book of Revelation is
about an awful judgment on the carth. Fire and brimestone fall; a
great battle called Armageddon is fought; even the dragon, Satan, is
finally cast into the pit. If any book is about judgment, Revelation is.

‘The covenant model, however, connects this judgment with the
covenanial lawsuit concept.! A covenant lawsuit was brought against
someone who had made covenant with God, broken it, and been un-
willing to make amends. When this happened, God sent messengers
to file the suit—normally three, since two or three witnesses were
needed to obtain a conviction (Deut. 17:16). But in this case of God’s
lawsuit against someone, the witnesses announced a verdict already
reached in the Lord’s High Court of heaven. Revelation opens in this
context, using the covenant structure to present the terms of judgment.

1. In Chapter 14, I discussed at length the concept of “covenantal lawsuit.”

253
