Dominion By Covenant 135

the key which unlocks all the problems surrounding baptism. God’s
Name — attached through the sanction of baptism—is so powerful
that it brings life to covenant-kecpers and death to covenant-

breakers.

5. Continuity (Matt. 28:20)

The Great Commission Covenant closes with “Lo, J am with you
always, even to the end of the age” (Matt. 28:20). Christ’s presence
was the new inheritance. Deuteronomy establishes that inheritance
is received at the throne of God—where He is present. It is this
unique throne-room presence that Christ promises. Moreover, in
our study of continuity in Deuteronomy we have learned that man
receives ordination (commission), food (sacred meal of commun-
ion), and anointing. If we keep the Great Commission in context, it
is apparent that all of the features are here. The Great Commission
is given on the eve of the arrival of the Holy Spirit, in the midst of a
sacred meal.

Fifty days afier Passover was the time of Pentecost, a great feast,
symbolizing the coming of the harvest. It also referred to the time of
the Jubilee. The year of Jubilee was every fiftieth year. During this
year, the land returned to the families of the original owners, Domin-
ion was given back to the rightful heirs. Putting these ideas together,
we scc that the coming of God's Presence was the true harvest of the
world. The earth started to be given back to the ¢rue owners. The
movement from God’s throne te the edge of the world begins. What Jesus
willed at the Sermon on the Mount—“The meek shall inherit the
earth” (Matt. 5:5), meaning meek before God—actually transpires.

But first Christ had to depart in order to sit at the right hand of
God. After He gives the Great Commission, He ascends into
heaven. His presence is left behind through the Holy Spirit and the
Lord’s Supper. The Spirit is especially present at this meal. Life,
health, and healing are tied to it. If taken unlawfully, sickness and
death result. Paul says, “For this reason many among you are weak
and sick, and a number sleep [die]’ (I Cor. 11:30).

The meal itself represents the whole inheritance. What is the New
Covenant inheritance? The whole world. Jesus redeemed the world,
and the meal is the Church’s first claim on what rightfully belongs to
it. Feeding on Christ, God’s people lay hold of the One who owns all
things. As his “younger brother” (Christ being the true “first-born”
