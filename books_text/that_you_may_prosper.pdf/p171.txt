The Historical Family Covenant 151

More than likely, the same type of contract was practiced in the
Bible (Gen. 24:1-67; Sang of Solomon). This sheds much light on the
passages in Genesis where Abraham told Abimelech, the King of
Egypt, that Sarah (his wife) was his sister (Gen. 20:1ff.). Abraham
was not lying to Abimelech. He had legally adopied Sarah as his sister.
Why? The bride was the top advisor of the hushand. She shared in
the inheritance, and could rule in the father’s absence.

The role of advisor is powerful. The advisor usually wields more
influence than the person in authority. Consider, for example, who
has more power: The President of the United States or his advisory
cabinet? It is the latter. The President almost never goes against his
advisors. They are the primary source of the information that flows
to him, on which he will base his decisions, in counsel with them.
They are the people to whom the burcaucratic hierarchies report.
The Bible presents the woman as having this kind of influence over
the husband. The positive Biblical example is Esther, the Jewish
Queen of the Persian Emperor, Ahasuerus (Xerxes). When Haman,
a vicious member of the king’s court, plotted against her people and
relative, Mordecai, she used her advisory power to get him hanged
(Esth, 4:1; 7:10). This is ultimately a picture of how God’s Bride,
the Church, advises the kings of the world to get the wicked hanged
from their own gallows. But ata very basic level, it is a Biblical view
of the power of the role of the wife. It is the role which God has or-
dained for her.

The contract above also mentions that the woman was bringing
with her a sizeable estate. This may have been what was left from
the previous marriage. Maybe it was her inheritance or gift from her
father. In either case, we see the ability of the woman to deal in busi-
ness matters. The Puritan woman was often left to run the houschold
and even bring in extra income while the husband was away. This
too is a Biblical idea. The woman of Proverbs 31 was a shrewd busi-
ness woman, quite familiar with the ways of the market place. There is
nothing wrong with a woman’s working, as long as it does not jeo-
pardize her domestic responsibilities.

The hierarchy of the Puritan covenant is clear. God is the tran-
scendent authority, and the father represents Him to the family. The
wife stands with the father as his top advisor. She has delegated au-
thority and the children must submit to her because she represents
the father.
