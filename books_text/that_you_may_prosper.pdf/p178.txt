158 THAT YOU MAY PROSPER

Conclusion

This concludes our application of the covenant to the family. An
old marriage covenant has been used. I freely admit that not all mar-
riage contracts follow the Biblical outline. That is not my point. This
one did, and I think we can see in history that such a view of mar-
riage produced the seed-bed out of which our entire culture has
sprung. At no other time in American history has the family been so
strong.

But what about the Church? Is it structured according to the cov-
enant? And are there any historical examples? In the next chapter
we examine the Biblical Church Covenant.
