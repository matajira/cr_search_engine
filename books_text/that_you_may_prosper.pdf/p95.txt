Ethies 75

and use it, by knawing the links of the chains of influences descending ver-
tically from above, and establishing for himself a chain of ascending links
by correct use of the occult sympathies in terrestrial things, of cclestial im-
ages, of invocations and names, and the like.®

As above, so below. Manipulate the creation, and the forces of
the heavens ean be mastered, directed toward the magician’s goals.
Here is the vision of man as God, the one who directs the forces of
nature and the supernatural as a craftsman,

In Biblical religion, what is above (God) is fundamentally differ-
ent from that which is below (the creation). Man’s position as in-
termediary is God-given, Man must exercise dominion as God’s cov-
enantal agent, not as an autonomous master of the cosmos, The link
between heaven and earth is therefore covenantal. It is governed by
the terms of the covenant. For man to get his way, he must choose
God's way for him, Keeping the law of God is not a way to manipu-
late (MAN-ipulate) God; it is man’s way of conforming himself ethi-
cally to God. The Biblical character who failed to understand this
was Simon the magician, who sought to buy the power of God from
Simon Peter. Peter consigned him to hell. He called on him to repent
(Acts 8:14-24).

Magical, mechanical, and manipulative explanations are all
false. They define life’s problems as metaphysical, not covenantal.
According to them, man lacks something in his being; maybe he is
racially inferior; maybe he lacks the right spell; maybe he is not say-
ing the spell the right way; maybe he does net know enough. In
every casc, the problem is a flaw in his being and not his ethics. The
third section of the covenant, however, calls for submission to God in
terms of His stipulations.

Conclusion

Law is at the heart of God’s covenant, building on the principles
of transcendence and hierarchy. The temptation of the people of God
is to resort to magic. As it was in the days when Moses received the
law, so it is in our day. The conflict is still between ethics and magic.

In this chapter, | have sought to accomplish one main thing: to
establish the ethical connection between cause and effect. First, I
presented the principle. It can also be called command/fulfillment,

16. Frances A. Yates, Giordano Bruno and the Hermetic ‘Tradition (New York; Vint-
age, [1964] 1969), p. 48
