12 THAT YOU MAY PROSPER

drives through the affluent, yet barred-windowed sections of cities,
perhaps he asks himsclf, “Who's really in prison in this society?” No
oath, no sanction, no justice. The innocent are captives to thugs in
high places.

Fifth, the State faces a huge crisis over the continuity question, It
has to do with immigration laws. I have already mentioned that the
Puritans attempted to get around a blood basis of continuity by only
giving the vote to church members, and that they were unsuccessful.
Even though they failed, however, they had the right idea. The vote
should not be on the basis of blood. Yet, the way the Constitution
was written hindered what the Puritans wanted. If a person is born in
America, he will be allowed to vote at age 18. Blood, not covenant,
actually forms political continuity in the U.S. It is not a question of
citizenship. Birth should entitle one to this status. But authority to
vote should have some covenantal qualifications, Someone can be
born in the country but be totally at odds with the American system,
and yet have full voting privileges. This has always created a dilemma
for preserving the ideals of the Founding Fathers.

Again, we sce that the decline of covenant thinking has changed
a major institution, The State today is not the same as it was when
our nation was established. To become a truly Christian civilization,
it must return to the covenant.

Church

Finally, the Church has also been influenced by the decline of
covenant thinking.

God is transcendent. Not only should He be Lord over Family and
State, but certainly He should be recognized as such in the Church.
He is supposed to be Head of the Church. When He is not, the
Church is left open to attack. The State becomes “Lord” of the
Church. Anyone who doubts the State’s “Lordship” should consider
how many churches are 501 (c) (3) organizations. They have sought
tax exemption, yet the law says such exemption is automatic for
churches. In effect, they have gone to the State to ask for permission to
exist. The implication: the Church no longer takes instructions from
the true Head. It goes through an alternate “priesthood,” the IRS.

The whole issue of authority has come into the Church. Do
Church officers have any real authority? Does God hear them? Do
the people hear them? I know of a church where it was discovered
that one of the leaders was leading a double life. When he came to
