48 THAT YOU MAY PROSPER

Accountability

The representative principle makes everyone accountable to
someone. Everyone is supposed to be checked and balanced by some-
one else. If the man at the top is not accountable to any institution,
there is tyranny. If the man at the bottom is not accountable, there is
anarchy. Rebellion disrupts the whole process of representation. If
the Mediators act autonomously, everyone suffers. Adam sinned,
and destroyed the whole human race.

If special mediators rebcl, the whole covenant community
suffers. King David once became angry with the people, and decided
to chastise them by taking them into war (II Sam, 24). He numbered
the people, which was always done before battle, but he did not offer
a sacrifice. God became angry with him. David was given a choice of
one of three punishments. His decision eventuated in the death of
seventy thousand people (Il Sam, 24:15). A man can be a great man of
God, have great insights, but his attempted autonomy can make him
a man of blood.

If the covenant community (general mediators) rebels against its
anointed leaders, not only do they suffer, but the whole world
suffers. Their ecclesiastical anarchy inhibits the spread of the king-
dom. Immediately after the section on Israel's court system, their
first encounter with the Canaanites is recorded (Deut. 1:19ff.). Israel
is brought to the Promised Land to enter and possess it. They “re-
belled against the command [hierarchy] of the Lord” and would not
go in (Deut. 1:26). The consequences were disastrous. They ended
up wandering around in the wilderness for forty years. And even
more important, Canaan was not brought under the Word of God. In
other words, the “world” was not subdued because of their rebellion.

Accountability has consequences for everyone, especially the
world. There is no visible sovereignty in the world until the repre-
sentative principle is working properly inside the covenant community. It
is clear from Deuteronomy that the covenant is the training ground
of accountability for dominion in the world. Until God’s people
learned to submit to their leaders, they were not allowed to exercise
rule in Canaan. Before they could bring Canaan under the cove-
nant, they had to be trained in several principles of accountability.

Principles of Accountability

1, Rebellion forced God’s people into a wilderness experience.
Disobedience always leads there.
