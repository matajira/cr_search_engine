Introduction 9

How frightening it is to think that the future leaders of our civiliza-
tion will believe this way!

Fourth, it used to be that when a couple said their vows before
God and man, their oath was taken seriously. Divorce was socially
unacceptable. Our times tell us something about how people feel
about their marriage oath. Thirty-eight percent of all first marriages
in the United States fail. Seventy-nine percent of those people will
remarry, and forty-four percent of these second marriages will fail.

The fifth area of the covenant is continuity. Most families cannot
maintain the bond implied by this word. Indeed, studies indicate that
“Christians” are not doing well at raising up a Godly “seed.” They are
losing their children to the government school system.” They are
losing them to the humanists who write the screenplays for television
shows. They are even losing them to the humanists who teach in
Christian colleges.?!

Also, the rapid death of the “family business” points to the loss of
continuity. Each year a growing number of family businesses is ter-
minated, not because there are no living heirs, but because the heirs
are not interested. Some students of the “small business” believe this is
one of the largest causes for the collapse of the “small business.”2?

At one time the family was understood as a covenantal unit. The
loss of this idea has had staggering effects. The five foundational
concepts of covenant have proven to be critical to the family’s life or
death, sickness or health. It seems that as the traditional marriage
vows have been altered or destroyed —“In sickness and in health, for
richer for poorer till death do us part”—so has the entire institu-
tion. But the family is not the only institution that has lost its cove-
nantal moorings,

 

The State

The New Deal, the Great Society, and all the other social in-
novations of our “progressive” society have failed.23 Why? These

20. Robert L. Thoburn, The Children Trap: The Biblical Blueprint for Education (Ft.
Worth, Texas: Dominion Press, 1986).

21. Gary North, “Foreword,” to Ian Hodge, Baptized Inflation: A Critique of “Chris-
tian” Keynesianism (Tyler, Texas: Institute for Christian Economics, 1986).

22. Robert E, Levinson, “Problems in Managing A Family-Owned Business,”
Management Aids No. 2004 (Washington, D.C.: Small Business Administration,
1981), p. 3. Leon Danco, Inside the Family Business (Cleveland: The University Press,
Inc., 1980), pp. 248-250.

23. Charles Murray, Losing Ground: American Social Policy 1950-1980 (New York:
Basic Books, 1984).
