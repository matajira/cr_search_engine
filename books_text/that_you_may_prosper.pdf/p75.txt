Hierarchy 55

4, Israel, including the Levites, the house of Aaron, and
the High Priest, acted as priests to the other nations.*

All through the Old Covenant, we see that men stood between
God and their fellow men. Progressively, however, there was move-
ment to that one day when everything changed. This is implied in
the shift from Moses to Jesus. When Jesus came, the principle of
representation remained the same, but its application changed. Jor-
dan explains this change.

With the coming of the New Covenant, these dualities were trans-
formed. The New Covenant embodies the fulfillment of what the First {or
Old] Covenant with Adam was supposed and designed to bring to pass, but
never did, . . . There is no one Edenic earthly central sanctuary; rather,
the sanctuary exists in heaven and wherever the sacramental Presence of
Jesus Christ is manifest. The equivalent to being in the holy land is now to
be in the Body of Christ.2

We still come to God by representation in Jesus Christ, but we
have direct access to God. Through prayer, the Church has even
greater access than the High Priest had in the Old Testament. He
could only directly approach God once a year. We can come directly
to God any time we want! The veil of the Temple was torn at Christ’s
death, The scparation between the Holy of Holies and the sanctuary
was removed,

The Old Testament believer could pray, but His prayer life was
more restricted. The Holy Spirit has come in the New Testament,
and He intercedes in a special way. “And in the same way the Spirit
also helps our weakness; for we do not know how to pray as we
should, but the Spirit Himself intercedes for us with groanings too
deep for words; and He who searches the hearts knows what the
mind of the Spirit is, because He intercedes for the saints according
to the will of God” (Rom. 8:26-27). With the substitution of Jesus as
the High Priest of God, who now sits at the right hand of God, the
New Testament believer has a full-time intercessor or intermediary
or representative in the throne room of God. The Spirit also can now
intercede for us in a special way. The incarnation, death, resurrec-

8, James B. Jordan, Sabbath Breaking and the Death Penalty (Tyler, Texas: Geneva
Ministries, 1986), p. 32
9. Ibid., p. 17.
