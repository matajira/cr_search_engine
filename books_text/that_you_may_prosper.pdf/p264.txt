244 THAT YOU MAY PROSPER

B. Continuity

Unique to the fifth section of Matthew, Matthew attaches his
usual formula to the end of Jesus’ sermon (Matt. 26:1). But it is not
the end of the book or section, He has discussed total discontinuity,
but now he directs his attention to covenantal continuity, It begins
with the institution of the Lord’s Supper at the time of Passover.
Remember, this meal originally disinhcrited the Egyptians, and
transterred the wealth of the wicked into the hands of the Israelites.
Jesus’ meal and death is the Passover in reverse. ‘This time, the Jews
are disinherited, and the Gentiles receive the wealth of the Jews. We
can say that through Jesus’ taking of man’s discontinuity —loss of in-
heritance due to the Fall—the world receives continuity.

Jesus, of course, is the “lamb” without blemish. After the meal,
He is led to be slaughtered. The First-born Son dies—not the
first-born of the Egyptians, but the First-born of God. But this First-
born does not stay in the grave. The first-born of the Egyptians had
all died (Ex. 12:30), They were helpless, God's First-born rises again
at dawn, the time the original Passover march was to have begun.
On this Resurrection Day, however, Christ begins a triumphal
march into Jerusalem. He gathers His people and holds another
meal. This time the inheritance is regained. Continuity is created
with the covenant-keepers.

Matthew concludes his book with the Great Commission, draw-
ing both the section of the covenant and our study of Jesus’ covenant
to a close. The best statement of the Great Commission is found at
the end of Matthew. This is the new dominion charter attached to
the end of the covenant: a statement of the Church’s new inherit-
ance, We saw the same thing at the end of Deuteronomy. Jesus ap-
pears as the new Joshua commanding His army to take the land that
belongs to them. A significant shift has taken place, one from the
Land of Palestine to the werd. Since this Commission is structured
according to the covenant, I will develop it later. Nevertheless, Mat-
thew verifies that the Gospel ends in dominion in the form of another
mini-covenant, proving dominion by covenant.

Conclusion
Matthew follows Deuteronomy’s covenant structure, Without a
doubt, the structure of the two books is parallel. But the theological
ramifications are important. It means the New Covenant is not com-
