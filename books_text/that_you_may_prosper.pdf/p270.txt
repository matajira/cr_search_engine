250 THAT YOU MAY PROSPER

tion: first the Gentiles and then the Jews. This historic tone is con-
sistent with the hierarchical category of the covenant. Paul proves
that history confirms God’s hierarchy of salvation through faith in
Christ.

Paul ends the hierarchical section with Amen. This time the
“amen” is not implied, but stated (11:36). We should be careful not to
put too much emphasis on Paul's use of “amen,” because he expresses
it at other points when a shift in subject matter is not implied. Not
only does the structural marker indicate the section is finished, but
Paul now moves from the hierarchical/authority theme to a different
emphasis, indicated by his “hortatory” style. The literary shift is the
primary reason for seeing the break in Paul's thought at this point.

3. Ethics (Rom. 12:4-15:33)

Consistent with the other ethical sections of the covenant, Paul
lays out the program for conquest by setting forth stipulations, by
correcting aspects of the image-bearing offices of king and priest,
and finally by specifying his personal plan of conquest.

Paul starts with such stipulations as, “Present your bodies as a

 

living sacrifice . . . Do not be conformed to the world . . . Let love
be without hypocrisy . . . Be devoted to one another . . . Rejoice
with those who rejoice . . . Do not take revenge . . .” (12:1-21). The

following chapter even restates many of the Ten Commandments
(13:9). Clearly, the ethical tone of Paul’s letter is felt.

But Paul also addresses the two Adamic offices of the cultural
mandate. First, he addresses kings. He talks of the proper obedience
to kings or “magistrates” and their corresponding responsibility
(Rom. 13:1ff.).

Second, he addresses priests. He discusses problems in the
Church at Rome which were due to some who still wanted to apply
some of the Old Covenant’s clean/unclean boundaries. Paul's argu-
ment is; because the curse has been lifted through Christ’s death, all
food is open to man (Acts 10), The wall between Jew and Gentile is
broken (Eph. 2:11ff.). The Gentile’s food can be eaten because he is
eaten by the Gospel (see reference to “mouth” in Rev. 3:16). To continue
to maintain the dietary laws as a point of Jaw is a fundamental denial
of Christ’s Resurrection. If a person maintains them as a point of
conviction, he should be given deference and treated as a “weaker
brother” (Rom. 14:1-23),

Paul concludes this section by returning to some gencral stipula-
