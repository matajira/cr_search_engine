50 THAT YOU MAY PROSPER

7, The lack of accountability leads to idolatry. Moses makes the
connection we saw in the life of Saul. Autonomous leaders and peo-
ple end up worshipping the wrong god. Moses indicates this possibil-
ity in his last words, “So watch yourselves carefully . . . lest you act
corruptly and make a graven image” (Deut. 4:15-16). Rebellion is as
the sin of witchcraft (I Sam. 15:22-23)!

These are the fundamental principles of accountability. In time
and through history, the representative principle became a way of
life.

Appeals Court or Bureaucracy?

In Chapter One, I stressed the point that God is transcendent
and immanent. He controls everything, but He knows everything
personally. He is different from man, yet He is present with man. I
said that this leads to a fundamental difference between the Biblical
covenant structure and Satan’s covenant structure.

Satan is not all-knowing, all-seeing, or all-anything. He is a
creature. Thus, he is neither transcendent nor immanent in the way
that God is. He has power, and he can move from place to place, but
he cannot be everywhere at once.

‘This makes Satan highly dependent on his subordinates. His
chain of command must get information to him from all over the
globe. He must get information from his demonic subordinates, and
he must exercise power through them. This limits him. Therefore,
Satan’s hierarchy is top down, whereas God’s hierarchy is bottom
up. God is in no way dependent on the creation, including His hier-
archy, either angelic or human. Satan is dependent. So, in order to
exercise his power, he must place great emphasis on his hierarchy. It
is a classic burcaucracy. The being at the top exercises power
through the chain of command as a military commander exercises
power. It is a top-down chain of command.

The Church, like Christian institutions in general, is not struc-
tured along the lines of a top-down hierarchy. God knows all things,
and He controls all things. Thus, He can safely delegate responsibil-
ity to subordinates, in a way that Satan dares not delegate power.
Satan commands cthical rebels who threaten him; God commands
everything, and no one can threaten Him. Jf ts the very sovereignty of
God that is the basis of the decentralized freedom of individuals in Christian so-
ciety. It is the non-sovereignty of Satan that leads always to cen-
tralized power and bureaucracy. What Satan cannot achieve as a
