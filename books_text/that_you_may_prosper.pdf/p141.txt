Five Points of Covenantalism 121

converts it, History is covenantal. Events are not random, nor are
they disconnected from what happens in the covenant. Everything
transpires according to God’s redemptive purposes.

C. Third Point of Covenantalism: Ethics

There is an ethical cause and effect relationship. Things do not happen
mechanically, manipulatively, or magically, Covenant-keeping leads
to blessing. Covenant-breaking results in cursing. Fulfillment of
righteousness is the ethical core of the covenant. Deuteronomy 5-26
summarizes the Ten Commandments, sometimes called the “stipula-
tions” section (Deut. 5-26). This ethical relationship is not just exter-
nal compliance. God expects proper motivations (faith), standards, and
situational applications.

D. Fourth Point of Covenantalism: Sanctions

The covenant is ratified through the reception of the sanctions of
blessing and cursing by means of a se/f-matedictory oath. God takes an
oath by His own Trinitarian name. He pledges to fulfill the covenant
on Himself, This form of covenant-cutting expresses that God fulfills
His own standard of righteousness, but it also states that the participant
is expected to live consistently with the covenantal stipulations. Should
he fail to do so, one of the dual sanctions take force: cursing. Thus, the
covenant is unconditional, but there are ferms of unconditionality.

Symbols are part of this process of entrance by oath. They are
real symbols. They are not nominal, having only symbolic value.
They are not realistic, infusing grace. They are covenantal claims of
God, Their meaning and power are in His name who stands behind
them. They claim a person to life or death, depending on whether he
lives according to the terms of unconditionality. Because these sym-
bols represent God’s Trinitarian name, which is One and Many,
they are applied to individuals and households, which are one and
many.

Finally, the ratification process is actually an adoption. A change
of name occurs. God becomes the true Father by covenant. Man
ceases to be a covenantal orphan.

E. Fifth Point of Covenantalism: Continuity

After ratification, the true heirs should be confirmed. Normally
this legitimization takes place in three stages. First, the heirs are
confirmed by covenant renewal, the laying on of hands at a meal.
