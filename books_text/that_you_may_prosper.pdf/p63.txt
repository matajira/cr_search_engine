Hierarchy 43

Hierarchy: Visible Sovereignty

The second section of Deuteronomy begins with a brief state-
ment of the hierarchy among God’s people. Moses says,

How can J bear the load and burden of you and your strife? Choose
wise and discerning and experienced men from your tribes, and I will ap-
point them as your heads. And you answered me and said, “The thing
which you have said to do is good.” So 1 took the heads of your tribes, wisc
and experienced men, and appointed them heads over you, leaders of thou-
sands, and of hundreds, of fifties and of tens, and officers for your tribes,
Then I charged your judges ai that time, saying, “Hear the cases between
your fellow countrymen, and judge righteously between a man and his fel-
low countryman, or the alien who is with him. You shall not show partiality
in judgment; you shall hear the small and the great alike. You shall not fear
man, for the judgment is God's, And the case that is too hard for you, you
shall bring to me, and I will hear it” (Deut. 1:12-17).

It is really very simple. Biblical hierarchy is a series of courts with
delegated authorities over each level. The procedure is from the bot-
tom up. Since the Biblical system of authority is placed immediately
afler the transcendence section, it is the way in which God makes
His transcendence known on earth. In the ancient suzerain cove-
nants, a hierarchical and historical section (historical prologue)
would always follow the preamble, what I have called the transcen-
dence segment. The suzerain identified himself as Lord and then he
proved it by setting up his authority in history.! To be precise, he
made the newly conquered vassal a visible authority, one who repre-
sented him. This idea of “visible sovereignty” was copied from the
archetype of all sovereignty, God.

The Biblical covenant has the same pattern. This progression of
thought in Deuteronomy is very important. There is a movement
from the declaration of transcendence to the temporal demonstration
of transcendence, from the verbal to the visual, and from heaven to
earth. This progression is the same all through the Bible. It is God’s
way.

 

1. Kline, Structure of Biblical Authority, pp. 114-115. Kline refers to the treaty of Mur-
silis and his vassal Duppi-Teshub of Amurru. He says, “Such treaties continued in
an ‘I-thou’ style with an historical prologue surveying the great king’s previous rela-
tions with, and especially bis benefactions to, the vassal king.” A translation of this
treaty, along with many others, can be found in A. Goetze, Ancient Near Eastern Texts,
ed. J. B. Pritchard (Princeton: Princeton University Press, 1950), pp. 203ff.

 

 
