The Historical State Covenant 201

ethics): Article I, the legislative powers. It has a section on sanctions
(point four): Article IT, the executive power. It has a section on con-
tinuity (point five): Article V: amending the Constitution.

Conclusion

Society is designed to run according to covenantal arrangements.
Early American history is an excellent example of what can happen
when politics is built on the Word of God. As we have seen with all
the other spheres, the Mayflower Compact is a covenant document
that was used to govern the sphere of the State. It is a true image of
the Biblical covenant. This concludes not only our study of the
State, but an application of the covenant to the spheres of society.

We have one last application of the covenant. How do we take
what we have learned in this book and re-establish a covenantal soci-
ety? There is a positive and a negative approach, We want to answer
such questions as, “What do Christians do when they meet opposi-
tion? Do they revolt? Do they give up? Are they defenseless and
helpless?” No, God has provided a powerful application of the cove-
nant, In the next chapter, we shall learn how to take society fitile by
little.

 

might have been reversed historically, with the Executive at the pinnacle, had events
happened differently, or had the brilliant Marshall not been appointed Chief
Justice. In some sense, this battle for Gonstitutional supremacy is still in progress,
although in our day, the Court has retained its position at the top of the hierarchy.
The great irony here is that the Founding Fathers probably believed that the
Legislative branch would be the dominant force. This is why most of the debates
centered on the legislature.
