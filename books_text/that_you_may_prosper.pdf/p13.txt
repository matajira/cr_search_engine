PUBLISHER'S PREFACE
(1992)

by Gary North

Because of the impact which That You May Prosper has begun to
have on the broader Church, I have decided to scrap my original
Preface and substitute a more generic one. The Foreword by Dr.
Fisher presents the case for the five-point covenant model from both
a biblical standpoint and a practical one. His Foreword should silence
several academically inclined critics who have treated Sutton’s dis-
covery with something bordering on contempt. Here I wish only to
recall the conditions under which the first edition was written and
then discuss several applications of Sutton’s thesis.

The manuscript began in 1985 as a study on the symbolism of
baptism. As the editor, I told him that it needed extensive rewriting,
What I eventually got was a completely different book. The manu-
script went through at least four revisions. As it began to take shape,
I realized that Sutton had stumbled onto something of great impor-
tance. When the manuscript was completed, J was convinced that
the book would make a major theological contribution. How major?
I discussed that in my original Preface, which readers may wish to
consult. Some readers felt that I overestimated its importance. Time
will tell. (So will eternity.) One thing is sure: Sutton offers us a pre-
cise definition of covenant theology, for which the Protestant world
had been waiting for a long, long time.

Covenant Theology

Calvinism is known as covenant theology, yet it is one of the
great ironies of history that for over four centuries, no Calvinist
author before Sutton had spelled out in detail precisely what a bibli-
cal covenant is. A legal bond, yes. A personal bond, yes. A kind of
contract, yes. But the judicial details that make a covenant legally

xiii
