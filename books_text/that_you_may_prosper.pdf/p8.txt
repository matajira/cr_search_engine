viii THAT YOU MAY PROSPER

Appendix 6— Hebrews 8: Old Covenant/

 

 

New Covenant Comparison ..............6... 268

Appendix 7 — Meredith G. Kline: Yes and No............... 281
Appendix 8—Covenant in the Flesh

(Old Covenant Sacraments).........-0.0... 288
Appendix 9—Covenant By the Spirit

(New Covenant Sacraments) .........-..55 299
Appendix 10— The Covenant Structure of

the Thirty-Nine Articles 316
ANNOTATED BIBLIOGRAPHY ........ -. 319
SCRIPTURE INDEX - 323

 

GENERAL INDEX ........... : . 335

 
