Old Covenant/New Covenant Comparison 279

5. Continuity (19-13)

The last point made in this section speaks of continuity. How? By
the displacement of the Old Covenant world. The last verse of Hebrews 8
reads, “When He said, ‘New Covenant, He made the first obsolete.
But whatever is becoming obsolete and growing old is ready to dis-
appear” (Heb. 8:13). What was getting ready lo disappear? Within a
few short years, in A.b. 70, the Temple and Jerusalem would be de-
stroyed. On the basis of Christ’s death, the place of the old sacrifice
was going to be removed. What was so significant about this? Both
represented the entire Old Covenant. Remember, it was made with
Adam and through him with the “created world.” By sacrifice he was
covered for his sin. The Old Testament system was primarily a
world of animal sacrifices, all an extension of the one that temporar-
ily atoned for Adam's sin, When the Temple was destroyed, the
whole world of Adam was definitively wiped out.

A “new creation” had already begun. The Old Heavens and
Earth were definitively replaced at the cross by the New Heavens
and Earth. Paul says, “Therefore, if any man is in Christ, he is a new
creature; the old things passed away; behold, new things have come” (II
Gor. 5:17). The “new things” are the new heavens and earth referred
to by Isaiah the prophet (Isa. 65:1ff.), What are they? The created
world is transformed by a new ethical order. For example, Christ
calls the Church a “light on a mountain” (Matt. 5:14-16), and Paul
calls Christians “stars” (Phil. 2:15), How can this “cosmic” ter-
mmology be applied to people? The “new heavens and earth” is the
old made new through the Death and Resurrection of Christ. The
fall of Jerusalem marked an end of the old system, ‘These were the
“Jast days” that the Bible had been referring to for thousands of years
(Gen, 49; Dan. 2; Heb. 1:1-2). The new beginning had come in his-
tory. But what really happened when Jerusalem fell was the displace-
ment of the old order. The “apostates” no longer had a world or a place
to sacrifice. Continuity was given to the New Covenant people.
Amazingly, the Church did not have to lift the sword. All of this was
accomplished through the application of the covenant. Old Israel
was no longer the legitimate heir, but the Church, God’s new Bride.
The Old Covenant had tried to establish its heirs on the basis of
human procreation and bloodshed. Each time, God had thwarted
these “Cainish” attempts. The inheritance, continuity, and every-
thing else finally came through the covenantal faithfulness of Christ.
Dominion came by covenant.
