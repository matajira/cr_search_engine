304 THAT YOU MAY PROSPER

in the Flood? By immersion. How were the Egyptians killed at the
crossing of the Red Sea? By immersion.

God’s people are always “sprinkled.” At the Flood, the Ark was
“rained on,” or sprinkled. At the crossing of the Red Sea, the Psalm-
ist says, “The clouds poured out water” on the Israelites (Ps, 77:17).
Jesus was poured upon in the Jordan River because His baptism did
not symbolize conversion. How could it? His was a baptism into the
priesthood (Nu. 4:3). Priests were consecrated into the priesthood
through special baptisms (Heb. 6:1; 9:10; Lev. 11:25). The method
was by sprinkling. Muses says, “This is what you shall do to them to
consecrate them to minister as priests to Me. . . . You shall bring
Aaron and his sons to the doorway of the tent of meeting, and wash
them with water” (Ex, 29:4), Because there was nothing big enough at
the “door” to immerse the priests, the method was by pouring. This is
why those Reformation churches that have emphasized the priesthood
of all believers have traditionally poured or sprinkled people, rather
than immersing them.!

Christ had said, “John baptized with water, but you shall be bap-
tized by the Spirit” (Acts 1:5). When the Holy Spirit came, Peter
quoted the prophet Joel, describing this event: “‘And it shall be in
the last days,’ God says, ‘that I will pour out My Spirit upon all flesh’”
(Joel 2:28-32 quoted in Acts 2:17ff.). Holy Spirit baptism is pouring
or sprinkling because this symbolizes the blessing of the Spirit,

Circumcision blessed, but it did not have the power to bless the
“Gesh”; it only had the power to kill it, But Joel says that the Spirit
would be poured out on all “flesh” (Joel 2:28-32). Baptism is a sign
and seal of not only death, but the resurrection of flesh to become
true humanity, When man is converted, he starts to act more like a
human, and less like the “flesh,” which has the characteristics of the
“beast” (Dan. 7:1-8). Baptism is a symbol of the life-giving power of
the Spirit. This blessing leads into the next sacrament.

Communien (Food Sacrament)

Communion is a rifual meal that portrays the power of Christ over
death. Like the other covenants, the meal is the place for trans-

1. Those who baptize by immersion have argued that the Reformation churches
(Lutherans, Presbyterian, and Anglican) were still unwilling to break ritually wich
Rome, which also sprinkled, have not understood the theological reason for retain-
ing the early Church’s mode of baptism. It was the Roman Church which was in-
consistent with its own sacerdotal theology by retaining the mode of baptism used by
the early Church
