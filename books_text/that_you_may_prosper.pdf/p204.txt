184 THAT YOU MAY PROSPER

never served as an agreed-upon philosophy in politically disintegrat-
ing Greece and religiously polytheistic Rome; it began to be taken
seriously only after Rome had fallen to Christ, when Christianity
provided religious order to Western culture, But the amalgamation
between “Jerusalem” and “Athens” has never successfully overcome
the inherent errors of natural law theory, and in the late-eighteenth
century the alliance began to disintegrate, and after Darwin, natural
law theory collapsed,* never to be taken seriously again except by a
handful of conservative humanists and a few Christian intellectuals.

Let us not be misled: natural law theory rests on a self-conscious
belief in the possibility of judicial neutrality. Civil law must be neu-
tral~ethically, politically, and religiously. Civil law must permit
equal time for Satan. There are Christians who believe in neutrality;
they send their children to public schools that rest legally on a doc-
urine of educational neutrality. There are also Christians who think
abortion should be legal. This belief rests on the belief that killing a
baby and not killing a baby are morally equivalent acts; God is ncu-
tral regarding the killing of babies. That such Christians should also
adopt a theory of judicial and political neutrality is understandable.
But what is not easily understandable is that Christians who recog-
nize the absurdity of the myth of neutrality in cducation and abor-
tion cling to just this doctrine in the area of civil law and politics.
This is a form of what Rushdoony calls intellectual schizophrenia.

It is only the Christian who has the law of God itself written in
his heart, what the author of Hebrews calls a new covenant— the in-
ternalization of the old covenant (Heb. 8:7-13). For a Christian to
appeal to a hypothetical universally sharcd reason with fallen hu-
mianity is to argue that the Fall of man did not radically affect man’s
mind, including his logic. It is to argue that this unaffected common
logic can overcome the effects of sin. Anyone who believes this needs
ta read the works of Cornelius Van Til and R. J. Rushdoony.

The appeal to natural law theory is pagan to the core. It is in
some cascs a sclf-conscious revival of pagan Greck philosophy.*
Natural law theory is totally opposed to God's law.® Sadly, we find

4, R. J. Rushdoony, The Biblical Philosophy of History (Nuley, New Jersey: Pres
byterian & Reformed, 1969), p. 7, Gf, Gary North, The Dominion Covenant: Genesis
(Tyler, Texas: Institute for Christian Economics, 1982), Appendix A.

5, Archie P. Jones, “Apologists of Classical Tyranny: An Introductory Critique of
Straussianism,” Journal of Christian Reconstruction, V (Summer, 1978); Jones, “Natural
Law and Ghristian Resistance to 'lyranny,” Christianity and Civilization, 2 (1983).

6. Rex Downie, “Natural Law and God's Law: An Antithesis,” Journal of Christian
Reconstruction, V (Summer, 1978).

 
