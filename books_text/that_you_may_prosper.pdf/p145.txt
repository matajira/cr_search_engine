Dominion By Covenant 125

Francis Nigel Lee, in his helpful work, The Central Significance of
Culture, calls this segment of Scripture the “dominion charter.”? Defi-
nitely, the tone of dominion rings throughout the passage, and the
Christian Church has generally understood these words to mean
that man is to dominate culture and society for the glory of God.?
There are no sacred/profane categories inherent in creation. The
original garden had zones that were nearer to and farther away from
God, but everywhere was sacred. Corporate man, male and female,
was to spread “culture” (from cultural mandate). What is “culture”?
“Culture” comes from “cultus,” meaning worship.* Thus, the task of
dominion was to transform the world into a place of worship, and
thereby create true culture. No worship, in other words, means no
culture. Dominion is not secularized work. It is sanctified labor in-
volved in making society into a proper place to worship God. In
Adam’s case, he was to take the raw materials on the ground and
fashion a society, not just a cathedral, in concert with God’s pres-
ence. In our case, it consists of transforming the uncthical debris of
society into the glorious praise of God,

Specifically, this is done by means of the application of the cove-
nant. The original mandate falls in the context of a covenanial order.
The creation account of Genesis 1 says ten times, “Phen God said”
(Gen. 1:3, 6, 9, U, 14, 20, 24, 26, 28, 29). With “ten words” God cre-
ates the world, just as He speaks the law (covenant) with ten words
(Deut. 4:13). Could the parallel be any more obvious? To be clear,
however, let us look at the ten words of creation to see where the cul-
tural mandate falls.

First Five
1. “Let there be light” (1:3): Transcendence. Light conveys God's

2. Francis Nige) Lee, The Central Significance of Culture (Nutley, New Jersey: Pres-
byterian und Reformed, 1976), p. 10.

3. Of course many have rejected the notion that this mandate still applies in the
New Covenant age. Len Tolstoy was militantly opposed to “culture” altogether, be-
lieving that Old Testament Law had been entirely done away with. Sec the book by
the theological liberal, H, Richard Niebuhr, Christ and Cullure (New York: Harper &
Row, 1951), pp. 45-82. Nicbubn, himself sympathetic to the non-transformation of
culture, refers to “conversionists” of Church history who believed in only changing
men “subjectively,” but not the transfurmation of the world “objectively” (pp. 1908).

4. Ofcourse, there are other views of culture, stripped of this “liturgical” orienta-
tion. Nigel Lee refers to them as “materialistic and idealistic” views of the cultural
mandate: Central Significance of Culture, pp. 1-20. For an excellent presentation of a
Christian view of culture, see Klaas Schilder, Christ and Culture (Winnipeg: Premicr,
[1947] 1977)

 
