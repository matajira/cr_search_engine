52 THAT YOU MAY PROSPER

they try to run the economy by a bureaucratic system, production
bottlenecks appear, and the economy becomes more irrational. The
chain of command is supposed to be limited to warriors and battles.

We must recognize that military warfare is an exception to the
normal activities of life. War is an extraordinary measure, In the
Old Testament, the civil government numbered the people only be-
fore a military engagement. Each adult male twenty years old and
older had to bring a heave offering of half a shckel of silver. This was
a payment to make atonement (Ex. 30:12-16). When David num-
bered the pcople for the purpose of going to war, and he failed to
make this atonement, God punished him by killing 70 thousand
Israelites in a plague (II Sam. 24). He was acting like a pagan king,
shedding blood without proper atonement.

It is true that we arc in perpetual spiritual warfare in the New
Covenant (Eph. 6). It is also true that they were in perpetual spiri-
tual warfare in the old covenant. Spiritual warfare is ordinary; mili-
tary warfare is extraordinary. This is why Christians are told to pray
for civil peace (I Tim, 2:1-2), It is in peacetime that we can best do
our work of dominion, A full-time military warfare State is an un-
godly, satanic bureaucracy. In fact, war or the coming of war is used
as the primary excuse to centralize the nation, Wars and rumors of
wars are part of a fallen world, but we are not to indulge ourselves in
perpetual war for perpetual peace.

The Biblical strategy for winning the spiritual war is self-
government under God. Wars begin in the heart (James 4:1). The
best means of keeping the peace is covenantal faithfulness (Deut.
28:1-14). Suppress the war of the heart, and the result is greater
peace. The presence of the Biblical hierarchy helps men to face what
is in their hearts. As we have already seen, Matthew’s chapter on
discipline outlines a bottom-up hierarchy similar to Moses’ (Matt.
18:15ff.).

The priests of the Old Covenant did not operate a top-down bu-
reaucracy. Why, then, should we, who have the Holy Spirit, seek to
create a top-down bureaucratic hierarchy in any area of life except
the military? We have the Bible—our marching orders—and we
have prayer and therefore access to the throne room of God. There is
no reason to imitate Satan's hierarchy, as if our Commander-in-
Chief were not omnipotent, omniscient, and omnipresent. We
should instead limit earthly hierarchies to appeals courts under God.
