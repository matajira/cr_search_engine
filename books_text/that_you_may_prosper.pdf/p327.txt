Covenant By the Spirit 307

and if he refuses to listen even to the church, let him be to you as a Gentile
and a tax-gatherer (Matt. 18:15-i7).

When Church discipline is going on, the body is being “discerned,”
members are “judging one another.” Where this is not happening,
the members are being illegitimized. This makes local church di:
pline a very serious matter. The Church at Corinth was full of disci-
plinary problems. Paul had had to tell them to discipline one man
who committed incest (I Cor. 5:1ff.).

These three principles of legitimacy were important to the life of
the Corinthian Church. Why? As we have seen throughout the cove-
nants of the Bible, continuity and discontinuity are created at the
cevenantal meal. The New Covenant is no different. Offense at the
meal means loss of inheritance.

  

i-

Continuity/Discontinuity

First, continuity/discontinuity is established with the First-born.
Who is the First-born? Jesus, Paul says Christ is the “first-born
among many brethren” (Rom. 8:29). Remember, the true first-born
forms continuity. Throughout the Old Covenant it was always the
second “Adam”: Isaac not Ishmael, Jacob not Esau, etc. In effect,
they became the first-born. But Christ was both the Second Adam
and the First-born. The writer to the Hebrews says, “And when He
again brings the first-born into the world, He says, ‘And let all the
angels of God worship Him’” (Heb. 1:6). Communion sets up con-
tinuity between Christ and the Church which is also called the
“first-born” (Heb. 12:23). This continuity makes the Church the heir
of everything that belongs to Christ, the whole world. There is a
song, “We Are the World,” which implies that everyone on earth is
the world. Actually, the Church is definitively the world! Progres-
sively, the Church is becoming the world by filling it and subduing
it. In the end, anyone who is not a member of the Church will be
removed fram the earth.

Second, communion establishes continuity/discontinuity be-
tween the Church and world. The Passover Meal was a feast which
disinherited the Egyptians. In the same way, communion is the new
Passover meal which disinherits the world. How? While the Israel-
ites were cating the lamb of the Passover, the Angel of Death visited
the Egyptians. In a miraculous way, the Egyptians were defeated.
When the Church communes with Christ, keeping in mind the prin-
