138 THAT YOU MAY PROSPER

else in the meeting — suggested that we turn to the Bible for a defini-
tion, They turned to me, the resident minister, and said, “Does the
Bible define the family anywhere?” I took them to the Old Testa-
ment, and this became the third and accepted definition.

Malachi says,

The Lord has been a witness between you and the wife of your youth,
against whom you have dealt treacherously, though she is your companion
and your wife by covenant (Mal. 2:14).

The Bible defines the family as a covenant, the same Hebrew word
(berith) being used in Malachi that is used elsewhere for the God-to-
man covenant (Gen. 6:18). Once this definition is accepted, how-
ever, it affects how one views the family. It is the missing concept
that has not been addressed. Consequently, most Christians have a
defective view, and only sec the family as a series of isolated “princi-
ples” concerning “how to communicate,” “child-rearing techniques,”
and so forth, but they do not have the faintest idea how all these con-
cepts relate. In other words, they lack the integrating concept that prop-
erly outlines what the family is.

To take dominion over the family, we should begin here. Let us
analyze the first family covenant.

‘Then the Lord God said, “It is not good for the man to be alone; I will
make him a helper suitable for him,” And out of the ground the Lord God
formed every beast of the ficld and every bird of the sky, and brought them
to the man to see what he would call them; and whatever the man called a
living creature, that was its name. And the man gave names to all the cat-
tle, and to the birds of the sky, and to every beast of the field, but for Adam
there was not found a helper suitable for him. So the Lord Ged caused a
deep sleep to fall upon the man, and he slept; then He took one of his ribs,
and closed up the flesh at the place. And the Lord God fashioned into a
woman the rib which He had taken from the man, and brought her to the
man. And the man said, “This is now bone of my bones and flesh of my
flesh; She shall be called Woman, because she was taken out of Man.” For
this cause a man shall leave his father and his mother, and shall cleave to his
wife; and they shall become one flesh. And the man and his wife were both
naked and were not ashamed (Gen. 2:18-25).

How do we know that this passage refers to a covenant? In the
previous chapter, we saw that the creation days were structured ac-
cording to the covenant, creation being called into existence with fen
