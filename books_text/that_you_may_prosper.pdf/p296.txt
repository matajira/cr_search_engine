276 TIIAT YOU MAY PROSPER

Two, the fistorical principle. The historical situation changed.*
Christ redeemed the whole world. The Promised Land was no
longer “holy” ground exclusively, Thus, laws endemic to a Hebrew
republic were no longer necessary, while laws conducive to a Chris-
tian republic carried over; the redemption of Christ facilitated the
spread of the kingdom into the nations. Actually, laws tied to the
Hebrew family dropped off, whereas laws with a multi-national charac-
ter extended forward. How so? The Old Covenant was styled
around the family-unit. The Hebrew peuple were actually onc large
extended family: hence, laws like the law of the “kinsman redeemer”
and nearest of kin “avenger of death” were important. Cast as such,
both were closely connected with the preservation of the Hebrew
seed-line through which the Messiah came. Essentially, they ended
with Him.

But the avenger of death concept demonstrates the shift from
family to mudtt-national organization in the New Covenant. The New
Testament drops the family avenger of death concept. In its general
equity (general equivalent), however, it pulls through. Paul argues in
Romans that the “avenger” is the State. Notice the slide from family
to nation. The historic situation of the New Covenant changes. The
family of God expands from a nuclear unit to the people of God,
multi-national in scope. The Church replaces the role of the original
Adamic family by “making disciples of the nations” (Matt. 28:19-20).
Thus, the historic change of situation pulls over the general equity of
Old Testament law.

Three, the personal principle. Personal commitment to the law of
God decpens in the New Covenant. Quoting Jeremiah, Paul says, “I
will write them [Old Testament laws] on their hearts” (Heb. 8:10).
Transfigured Torah is etched on the heart of every New Covenant
believer (Jer. 31:33). Since David is an example of an Old Testament
man who internally possesses the law, the New Covenant intensifies
the internalization of the law of God through the dramatic work of
the Spirit of God. It comes in greater fulness in the New Covenant.

Keeping in mind the relationship between Old and New Cove-
nant, fulfillment of righteousness is clarified, It is in and through Christ.
Yet, the covenant is still ethical at its center. Man comes to God
through Christ’s fulfillment of righteousness, and then having done
so, is expected to “keep His commandments” (I John 2:3-4).

4. Law of the Covenant, pp. ff.
