272 THAT YOL MAY PROSPER

microcosms of a world controlled by heaven. But they were never
able to restore creation. The Old Testament told the story of man’s
Fall over and over again. Even when Christ came, He found a land
that was anything but space controlled by heaven. Israel was
demon-possessed and controlled from below! The chosen people had
become earthly.

Hebrews 8 says that God provided a better covenant because this
covenant is “heavenly,” meaning truly transcendent. Christ is the
New Covenant: the temple, sacrifice, and High Priest. He restores
heavenly rule to the earth again. Since the temple is in heaven, all
men have access; there are no zones of holiness kept from the com-
mon member. The throne of God is open and available. Thus, man
can go straight to the origin of transcendence. Moreover, he can act
as a true image (copy) of original transcendence. The “heavenlyncss”
of the New Covenant draws out the first contrast between the Old
and New Covenants.

2. Hierarchy (6-9)

The hierarchical section of Hebrews 8 begins, “He [Christ] is a
mediator of a better covenant” (Heb. 8:6). We have seen that hicrar-
chy has to do with the mediation of judgment to history through representa-
itves. Tn the Old Covenant, Moses and sacrifices mediated life and
death. In the end, they all mediated death because the Old Cove-
nant ends in the death of Christ. But Christ is the new mediator.
Through His priesthood the Church, He mediates life to the world
through the Word and sacrament. His mediation is far superior to
the old mediators. All men have direct access to Him, and His sacri-
fice is permanent and complete.

The second section of Hebrews 8 also weaves the issue of authority
into it. As we have already seen time and again: history and author-
ity have a dynamic relationship to one another. Just like the second
point of the covenant in Deuteronomy, the writer speaks to Israel’s
rejection of God's authority and their subsequent excommunication
(I Cor, 10:8). He refers to the days of the Exodus when Israel
apostatized and was cast out, whereas the New Covenant is differ-
ent. What is the difference? This will not happen to the New Cove-
nant Bride and people.

The first covenant had a “fault.” Notice that the flaw is not in the
covenant itself, but in the people. The text says, “Finding fault with
them” (v. 8), meaning the “new” Adams of the Exodus who turned out
