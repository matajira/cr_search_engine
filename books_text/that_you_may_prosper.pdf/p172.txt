192 THAT YOU MAY PROSPER

Ethics

All which the said Thomas Clarke does hereby promise and éind himself
to all of and perform as a covenant upon their marriage.

The marriage covenant between Clarke and Nicholls is ethical.
Both parties are responsible to provide certain tangible items. But
again it should be underscored that the Lord was the final authority
of this covenant. They were living according to some sort of tran-
scendent Jaw. Recalling what we said about the ethical character of
the covenant in the last chapter, ethical stands in contrast to magical.
We can work our way into the significance of this distinction with
our “totern pole” analogy.

Mormonism is a classic example of a modern religion that has a
magical view of authority, since it believes that man evolves and be-
comes a “god” on other planets. Lorenzo Snow, former president of
the Mormon Church, said, “As man is, God once was: as God is,
man may become.”¢ This is a totem-pole religion, It has a “chain of
being” view of life and the world. This means the family plays an im-
portant role, and specifically the father. The following diagram is
somewhat like the totern-pole analogy used carlier. This time we will
not draw a totem pole, just 2 column representing the static, ver-
tical, continuum of life.

CHAIN OF BEING VIEW OF FAMILY

GOD
FATHER:
(HIS SEED POSSESSES FAMILY VIEWED AS
BEING OF GOD) CENTRAL INSTITUTION
STATE OTHER INSTITUTIONS
HAVE LESS BEING. THEY
MUST GO THROUGH
CHURCH THE “PATRIARCH” TO.
NON BEING GET TO GOD.
(NO FAMILY)

5. Teachings of the Prophet Joseph Smith, edi. Joseph Fictding Smith (Salt Lake City:
Deseret Book Co., 1958), pp. 346-47

6. Walter Martin, The Kingdom of the Cults (Minneapolis, Minnesota: Bethany
Fellowship, 1965), p. 178.
