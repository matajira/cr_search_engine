The Biblical Family Covenant 141

begin their life together in it (Gen, 24:67), he left the region. He
remarried, journeyed east, and disappeared from covenantal history
until his death (Gen. 25:1-9). He was father of many nations:
Ishmaelites, Semites, Midianites, and so forth. But he wisely and
faithfully ceased to meddle in the lives of those sons who had gone
their own way.

The patriarchs in the Bible were not patriarchs in the sense of
men who ruled a single pyramid of families beneath them. The sons
and daughters-in-law were not expected to live in their father’s
house, or even nearby, The familiar “in-law problem,” reflected cul-
turally by the “mother-in-law joke,” is the product of sin: unbiblical
interference by would-be patriarchs and matriarchs, and smoldering
resentment and outright rebellion by resentful sons and daughters-
in-law. If men understood and honored the covenant basis of the
Biblical family model, such problems would be drastically reduced.
To put it bluntly, the in-laws’ authority stops long before and far out-
side the heirs’ bedroom door, not because of biology, but because of a
legal declaration of a new unit of government that is created by marriage.

The creation of Eve out of Adam adds another dimension to the
Biblical hierarchy. Her creation is analogous to man’s. As Adam was
created by God, she is created by God. Unlike Adam, however, she
comes into existence through the male’s own being (rib), making her
distinct from God, but forever dependent on the male for life. Not un-
til the Fall does woman appear in a totally “autonomous” and inde-
pendent role. So, the man needs the woman, and the woman needs
the man.

The hierarchy of authority places the male in the position of repre-
seniing God to the woman. Although she is man’s “vice-president,”
and top advisor, she is created to submit to man (Eph. 5:22). The
Apostle Paul uses her creation as an argument explaining why
women should nat be allowed to exercise authority over men in the
Church.

But 1 do not allow a woman to teach or exercise authority over a man,
but to remain quiet. For it was Adam who was first created, and then Eve.
And it was not Adam who was deceived, but the woman being quite de-
ceived, fell into transgression (I Tim, 2:11-14).

The Biblical chain of command runs from the man outward and
then downward to the rest of the family. Although modern man, in
all of his rebellion to the God Who created him, tries to escape this
