34 THAT YOU MAY PROSPER

Bible, man simply repeats the sin af Adam: to be as Gad.
The personal God of the Bible is too distinct for comfort. The im-
personal god of deism is too distant to be taken seriously.

False Immanence

Virtually everyone has rejected such a deistic view of god. But
what is the alternative? An anmanent god. This is the god of panthe-
ism, both Eastern and Western, This is the god who is immersed in
his creation. He is the “true reality” of creation. In certain forms of
Hinduism, he is the hidden unity that underlies the creation, and the
creation is itself said to be an illusion, maya. But in all these pan-
theistic religions, man is endowed by a “spark of divinity,” and man
will ultimately experience union with god. This is the god of monism.
God cannot be distinguished from the creation.

Guess who becomes the god of this world if pantheism’s creation-
immersed god is too, too close? You guessed it again! Man does. If
god is immersed in the creation, then man becomes a substitute god.
Man enjoys that spark of divinity. Man can also speak; pantheism’s
god is silent. Man’s ward therefore substitutes for god’s word. Man
becomes the god of the system, knowing (determining) good and evil.

So, without the transcendent, personal, covenanial God of the
Bible, man simply repeats the sin of Adam: to be as God.

The personal God of the Bible is too close for comfort. The
impersonal god of pantheism is too close to be taken seriously.

Shared Being

All non-covenantal views of transcendence and immanence
have one thing in common. They teach union of essence. God’s and
man’s “beings” run together in some way. The god of deism is so like
the world that when he separates himself from it, he can have no in-
fluence over it, He just isn’t transcendent enough to run the world
system. Similarly, the god of pantheism is so like the world that
when he immerses himself in it, he can have no influence over it.
The key words for fallen mankind are no influence. So, when fallen
man speaks of either transcendence or immanence, he means some-
thing completely different from what the Bible means. Cornelius
Van Til’s comments explain the difference.

It is not a sufficient description of Christian theism when we say that as
Christians we believe in both the transcendence and the immanence of
