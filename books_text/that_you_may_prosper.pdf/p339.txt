ANNOTATED BIBLIOGRAPHY

Baltzer, Klaus. The Covenant Formulary: In Old Testament, Jewish, and
Early Christian Writings, Translated by David E. Green. Philadel-
phia: Fortress, 1971. Baltzer is of particular help in the area of the
New Testament. He shows that the ancient covenant structure
was used by New Testament writers.

Berkhof, L. Systematic Theology. Grand Rapids: Eerdmans, [1939]
1969, pp. 262-304. Berkhof has written the best systematic theol-
ogy. He alerts the reader to the “systematic” issues: covenant of
works, covenant of redemption, covenant of grace, etc. Also, he
has a helpful section on the “dual aspects of the covenant.” To my
knowledge, he did not interact with some of the recent scholar-
ship, although his historical overview of how the sticky issues have
been handled, like “external vs. internal,” is valuable.

Brueggemann, Walter. Hosea. Atlanta: John Knox Press, 1968.
Brueggemann is certainly not orthodox, so one must read him
carefully. The use of this book, however, is that he shows how the
prophet Hosea, and others, used the covenant of Deuteronomy to
bring a lawsuit against Israel.

Chilton, David. Yhe Days of Vengeance, Ft, Worth: Dominion Press,
1987, Mr, Chilton uses the thesis of That You May Prosper to develop
the structure of Revelation, since it logically falls into five sec-
tions. He applies the covenant model throughout the book. So the
reader will not only learn about the covenant, but he will have the
Book of Revelation opened to him like no other author has done
before.

. Paradise Restored. Ft. Worth: Dominion Press, 1985.
Chilton makes several helpful comments about the covenant and
God’s way of dealing with His people. Perhaps the greatest value
is in seeing that the covenant is not inviolate. Apostasy is a Bthlical
reality.

319
