xii THAT YOU MAY PROSPER

where, as it were, as does the author. ll confess, after I wrote the
paragraph about Hezekiah, I discovered to my own amazement that
I could see the five points. You just have to switch the last two state-
ments (or verses), I challenge you to return to that paragraph, after
you've read the book, and see if you don’t think the same thing.

Fully one-third of the text of this volume is comprised of a series
of short appendixes. Disconcerting at first, perhaps, but you will
come to appreciate how the author keeps you on the track and prop-
erly paced for learning as you go through the body of the study. By
consigning peripheral issues and demonstrations relating to the cov-
enant outline to the appendix, he moves you at a steady pace
through the basic argument of his thesis, I believe you will enjoy
running the course, as did I, and you should cross the finish line the
stronger for it.
