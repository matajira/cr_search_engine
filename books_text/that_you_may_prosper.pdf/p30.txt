10 THAT YOU MAY PROSPER

programs are products of a government in violation of basic cove-
nantal principles.

First, the State has attempted to become transcendent. This
assumption of the role of God has been the “Second American Revo-
lution,” title of John Whitehead’s insightful work. Today’s foremost
Christian Constitutional authority, lawyer, and author, his thesis is
clear: the judicial procedures of the last 100 years have so completely
re-interpreted the Constitution that the intent of the original 1776
American Revolution has been lost. He refers to a comment made in
1907 by Supreme Court Justice Charles Evans Hughes —“The Con-
stitution is what the judges say it is”*®— and compares it to one made
in 1936 by the Third Reich Commissar of Justice: “A decision of the
Fuhrer in the express form of a law or decree may not be scrutinized
by a judge. In addition, the judge is bound by any other decisions of
the Fuhrer, provided that they are clearly intended to declare law.”26
Today, the Supreme Court has become our Fuhrer, We are left won-
dering, “Who really won World War II?”

Second, these quotes indicate confusion over authority. The repre-
sentative concept was a covenantal idea, as we shall see in greater
detail later. The magistrate was to represent God and the people. It
could easily be argued, however, that we no longer have a functional
representative systern. The Supreme Court, a group of appointed-
for-life officials, has the real power. The most recent example is the
infamous Roe v. Wade ruling ( January, 1973). After the states failed to
pass pro-death amendments in the early 1970s, the Supreme Court
still made death the law of the land. Do we have a representative sys-
tem? Where were our elected representatives? The Congress has the
right to overturn the future effects of any ruling by the Supreme
Court simply by withdrawing the Court’s appellate jurisdiction.2” By

24. John Whitehead, The Second American Revolution (Elgin, Ulinois: David C.
Cook Publishing, 1982).

25. Ibid, p. 20, quoting David J. Danelski and Joseph 8. Tulchin, eds., The Auto-
biographical Notes of Charles Evans Hughes, p. 143.

26. Ibid, p. 20, quoting Ernst von Hippel, “The Role of Natural Law in the Legal
Decisions of the Federal German Republic,” p. 110.

27. Article IE, Sect. 2, Clause 2. After the War Between the States, the Supreme
Court was anti-Reconstruction because its members had held their positions since
the 1840s. When a Southerner named McCardle filed against certain acts of the mili-
tary rule of the South during Reconstruction, the court accepted the jurisdiction.
Congress, on the other hand, being pro-Reconstruction, voted to determine the juris-
diction of the court, President Johnson vetoed the bill, but Congress overrode his
yeto, The Supreme Court thereupon withdrew from the case, declaring that the de-
