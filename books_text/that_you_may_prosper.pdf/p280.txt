260 THAT YOU MAY PROSPER

pattern is used to show God’s authority to speak something into ex-
istence, the building of His creation house (Gen. 1:3ff.), or the de-
struction of the world (Gen. 6:11ff.). Revelation 5:1 opens with an
“unfulfilled challenge.” This chapter shifts from heavenly adoration
to a challenge by a “strong angel” for anyone to “open a book” that
has been sent to the throne (5:1ff.). No one is strong enough to open
it, and this is the point. There is only One who comes and has the au-
thority, Jesus Christ (Rev. 5:5).

C. Ethics (Reo. 5:6-14)

The third section of the covenant usually contains stipulations
that consecrate and become the program for dominion. Through these
laws, ethical boundaries are established that separate the clean from the
unclean.

John turns his attention to the “book.” The document is not just
any book; it is the Ten Commandmenis. John explains that this scroll is
written on the front and back (5:1). The only other “book” with writing
on the front and back is the Ten Commandments. Moses writes,
“Then Moses went down from the mountain with the two tablets of
the testimony in his hand, tablets which were written on both sides;
they were written on one side and the other” (Ex. 32:15). Now we
can see why Jesus was the only One who could break the scals, The
“new song” that everyone sings says “He is the lamb that was slain”
(5:9). He was the only One “worthy” to open the book because He was
the Redeemer, the One who satisfied all the just demands of the Law.

After it has been determined that Jesus is the only One strong
enough and “pure” enough to break the seals, He comes and “takes
the book” from God the Father (5:7), applying it toward the conquest of
the enemies.

D. Sanctions (Rev. 6)

This part of the covenant has to do with judgment by means of the
sanctions, the breaking of the seals. After being broken, they cause
six awful judgments to begin to fall on the earth, the sixth coming
when martyrs at the throne of God say, “How long, O, Lord, holy
and true, wilt Thou refrain from judging and avenging the blood on
those who dwell on the earth” (6:10). Their call to be “avenged”
brings the sixth judgment, the “great day of wrath” (6:17).

The saints play an important role, therefore, in having the seals
broken. Their request for God’s vengeance brings direct judgment to
