Old Covenant/New Covenant Comparison 275

(Ps. 40:8). But as an age, the law was far from them because it was
the age of the first Adam. Before the historic outpouring of the
Spirit, there was no power to implement the law in its fulness. The
New Adam, Jesus Christ, brought a change. Since He incarnated
the Law, His followers were much more “law oriented,” God’s stand-
ard being more deeply imprinted on therm in Christ.

Christ is therefore the key to understanding the similarities and
differences between the ethics of the Old and New Testaments. The
similarities are in Christ. A question, however, is provoked by Paul-
ine literature. Paul says that the Law is nailed to the cross (Col.
2:14). Does this mean the Old Testament Law is no longer binding
on a New Testament Christian? No. There was no piece of paper on
the cross with the Ten Commandments written on it. Where were
they written? They had to be in the Person of Christ. So, when He
died they died with Him. But when He was raised, they were raised
with Him, Only, the Law of God was raised in greater glory and
power, Now when someone breaks a commandment, he is violating
Christ, not just a tablet of stone!

All of the law dies with Christ and all of it is raised with Him. So,
how do we account for differences, like clean/unclean boundaries, food
laws, and sabbath days? Christ is raised with a “transfigured” body.
Thus, the incarnated law is also transfigured, or changed. How do
we keep straight what is altered?

One, the heremeneutical principle, Old Testament law carried
through, unless the New Testament made a change. For example,
Old Testament food laws were changed. All of these pointed to the
death of Christ and created boundaries that were altcred because of
His death. The Jew/Gentile boundary had to be maintained to pro-
tect the priesthood (the Jews) from being corrupted. But after Christ
came in history, and the power of the Spirit was poured out in
greater measure because of His Resurrection, the boundary was
broken down. It was no longer needed (Eph. 2:11.). Peter was
shown a giant tablecloth with all kinds of Gentile animals on it and
told to eat. Then he helped Cornelius, a Gentile (Acts 10:1ff.). The
food laws changed, but there was still a food Jaw in the New Cove-
nant. It became the Lord’s Supper. The Church was commanded to
eat this food and not to participate in any other pagan “communion
meal” (I Cor, 10:19-20). Furthermore, it was told that “all” things had
become clean. God told the Church to eat everything just as the Gos-
pel would consume all things!
