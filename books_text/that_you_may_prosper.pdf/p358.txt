338 THAT YOU

discipleship and, 109
Congregationalists, 3
Continuity

Bible and, 107

communion and, 105

confirmation and, 105

cultural mandate, 126ff.

discipleship and, 189

discontinuity and, 110

fifth point of covenant, 96

historic, 107

legitimacy and, 103-105

summary, 121-22
Contract, 4
Contumacy, 164
Corsini, Eugenio, 254n.2, 257
Cotton, John, 199
Coulanges, Fustel de, 37, 144n.5
Covenant

accountability, 48-50

among Godhead, 3

Abrahamic, 34

baptism and, 299ff.

benediction, 113

blood relations and, 7

Book of, 2

chapters and, 4n.3

church (biblical}, 1594.

communion, 304f.

conditional or unconditional, 80

context, 122

continuity, 7, 90ff.

covenants within, 122

cross-check, 123

cutting of, 77, 78

dissolubility of, 111-12

early America, 3

enemies and, 14

eschatology and, 31iff.

establish-confirm, 268

ethics, 6

(summary), 121
family (biblical), 138¢., 904.
(historical), 8-9

forgotten, 2

Genesis, 53

Great commission, 127ff.

MAY PROSPER

growth of, 53-54, 56

hicrarchy of, 6, 43
(summary), 120

history of, 3-7

imputation and, 27f.

institutional, 109

judgment, 79, 114-16

key concepts, 6, 6-7

kingdom and, 313

law, 80-81

law or promise, 79-80

lawsuit, 13, 204ff,

“leakage” from, 93

legal dectaration, 27

legitimacy and, 103

Matthew structure of, 236ff.

metaphysical religion contrasted, 35ff.

model of, 14

multi-national, 276

naming documents, 24n.3

new, 52, 53

not social contract, 4n.3

number of, 263ff.

oath, 7, 83ff.

Old, 52

Old Covenant, 284

Old/New comparison, 263ff,

Passover, 291

primary, Itt

Promise and, 6

promissory, 80

promissory aspect, 78

Psalms, structure of, 225ff.

Puritan, 151

qualification of, 82-83

rediscovery, 1-2

representation, 6

restoration of, 27

Romans, structure of, 246f.

sacrifices and, 85-86

sanctions (summary), 122

secondary level, lil

seventy weeks and, 234-35.

shift from Old to New, 57

state, history of, 9-12

status and, 7

stones and, 87
