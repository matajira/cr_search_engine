Appendix 7
MEREDITH G. KLINE: YES AND NO

In New ‘Testament times there is no longer a simple coalescence of
the authority structure of the covenant with that of any cultural unit.

Meredith G. Kline!

In my development of the structure of the Biblical covenant, I
have particularly relied on Meredith G. Kline. Anyone who has
read his essays on the covenant structure will notice immediately
that I have adopted his outline of Deuteronomy. Clearly, my book is
not a commentary on Deuteronomy. It is unlike his work in the
Wycliffe Bible Commentary, an insightful study well worth meditating
on.? Rather, I attempt to isolate the various covenantal “principles.”

Kline and I disagree about the applications and implications of
cach of the five points of the covenant. We disagree to such an extent
that my book can legitimately be regarded as a rejection of Kline in
the light of Kline. Kline rejects the continuing New Testament au-
thority of the covenant structure that he discovered in the writings of
Balizer and Mendenhall. J, on the other hand, accept it,

The enigma in Kline appears mast strikingly in The Structure of
Biblical Authority, On the one hand, he argues that the theme of the
covenant model of the suzerain treaties appears all through the
Bible; hence, it is the structure of Biblical authority. On the other
hand, he believes that Deutcronomy, as part of the Mosaic economy,
is an “intrusion” into history.? It is therefore temporary. It cannot by
his definition be extended into the New Testament, that is, unless his

1. Kline, By Oath Consigned: A Reinterpretation of the Covenant Signs of Circumeision and
Baptism (Grand Rapids, Michigan; Eerdmans, 1968), p. 100.

2. Rev. James B. Jordan informed me about Kline's chapter back it the early
1980s.

3. Meredith G. Kline, “The Intrusion and the Decalogue,” The Structure of Biblical
Authority (Grand Rapids, Michigan: Ecrdmans, [1972] 1978), pp. 154-71.

281
