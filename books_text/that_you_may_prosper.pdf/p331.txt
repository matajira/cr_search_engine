Covenant By the Spirit 3u

Gospel, but will not Aeed it. Moreover, God’s trumpets of judgment will not
convert a world which is hardening itself in unbelief. The majority will ever
be on the side of the evil one. We most emphatically reject the dream of a
man-made era of peace, prosperity, and universal righteousness on earth
preceding the second coming of Christ. Just as definitely do we repudiate
the related idea according to which the almighty “law of evolution” will
bring about an ever-upward trend in the course of civilization... . We
repeat; the devil is not bound in everp sense, His influence is not wholy or
completely destroyed. On the contrary, within the sphere in which satan is
permitted to exert his influence for evil he rages most furiously. 4

 

When Hendrikson writes that “We most emphatically reject the
dream of a man-made era of peace, prosperity, and universal right-
eousness on earth preceding the second coming of Christ,” he is ar-
guing that there can be no covenantal, representative, victorious king-
dom role of the Church in history. He dismisses “man-made” peace
and prosperity, just as premillennialists do, with respect to the per-
ied before Christ's second coming. He then dismisses an idea which
he says is “rclated”: evolutionary humanism. This is deliberately
misleading. It is a disguised variant of that old lie that postmillen-
nialism is liberalism. Furthermore, Hendrikson’s pessimistic escha-
tology is implicitly anti-covenantal. Here is my reasoning,

Covenant theology divides men into saved and lost, a division
which is reflected in the sacraments. Covenant-breakers experience
long-term defeat in history, and covenant-keepers experience long-
term victory. We read in Proverbs:

 

‘The rightegusness of the blameless will smooth his way, but the wicked
will fall by his own wickedness. The righteousness of the upright will deliver
them, but the treacherous will be caught in their own greed. When a wicked
man dies, his expectation will perish, and the hope of strong men perishes.
The righteous is delivered from trouble, but the wicked takes his place
(Prov. 11:5-8).

What are we to say as Christians, that the success of the right-
cous in these verses is somehow man-made? This would be utter
nonsense. Yet just such a nonsensical argument is used by Hen-
drikson in his not-too-subtle rejection of postmillennial optimism.
The historic victory of the righteous is not man-made; on the con-

5. W. Hendrikson, More Than Conquerors: An Interpretation of the Book of Revelation
(Grand Rapids; Eordmans, 1940), p. 228.
