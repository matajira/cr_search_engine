The Historical State Covenant 197

Civil representatives do nol just represent man to God. They
also represent God to man. Their position is given to them by God
(Rom. 13:1ff.). They are to rule for “good” and be a “terror to evil.”
Modern government has lost this sense of transcendence and imma-
nence. It puts transcendence in man. Man has become “God” in his
own eyes. He has re-made God into his own image. There is no
doubt that the Mayflower Compact takes issue with such views.

Hierarchy

Subjects of our dread soveraigne Lord, King James.

This civil document has a hierarchy. The parties involved must
submit to authority. Revolution is not the proper way of establishing
government. It was not the method that they had used. Notice that
at the beginning and the end of the decument, it acknowledges the
King of England to be their “Soveraigne.” Revolution was not “law-
ful” for the Christian.

The Mayflower Compact created a Biblical hierarchy. These col-
onists were in submission to God, and they expected future mem-
bers of society to do the same. Their right to form this government
did not come from themselves; it came from God. They believed
that God worked through His hierarchy so they had to acquire
permission from King James to come to America. Once here, they
established a hierarchy based on the Bible. Since England had been
influenced by the same authority—the King of England is the
“defender of the faith”—we can see the same heritage in England.
But in America, these people were able to create a much more con-
sistently Biblical government.

Ethics

... And frame such just and equal laws.

The Mayflower Compact is an ethical covenant. The fact that
these Pilgrims were covenanting to build a society based on “law” in-
dicates how well they understood the covenant. Since the Biblical
covenant is ethical, we can see the connection. They wanted a soci-
ety that was “ethical.” It was not based on the “Divine right of kings.”
This was significantly different from England.

At the same time in England, the Puritans were just a few years
away from doing something that had never been done before. Led
by Oliver Cromwell, the Puritans, just a little under thirty years
after the writing of the Mayflower Compact, beheaded the King of
England, Charles I, The reason was that the king had committed
