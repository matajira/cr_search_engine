Introduction 5

Peter Bulkeley.° Other works basically reflected the same point of
view, but maybe their greatest influence was expressed in their
ereeds.? In whole or in part, these statements of faith found their way
to many different religious groups in America and England.* As cov-
enant dominated their documents, the idea was able to cross denom-
inational boundaries.?

The Anglicans were also quite influential in spreading covenant
theology. Anyone who doubts the theological links between the Puri-
tans and the Anglicans in seventeenth-ceniury colonial America
should consult Perry Milfer’s essay on “Religion and Society in the
Early Literature of Virginia.” The Anglicans’ commitment to good
Christian literature was commensurate with their dedication to the
thought of the Reformation." In 1695, Thomas Bray wrote, Proposals
For Encouraging Learning and Religion In The Foreign Plantations. What
he really had in mind was books? The result was the Society for
Promoting Christian Knowledge (S.P.C.K.) in 1699. The king gave
them their charter. One of the greatest movements in the history of
the Church began. The 8.P.C.K. disseminated a mountain of Chris-
tian literature in this part of the World. What kind of books? Mostly

6. Peter Bulkeley, The Gospel-Covenant, or the Covenant of Grace Opened (2nd ed.,
Londen, 1651). All future references to Bulkeley’s work will be to this edition.

7. The most famous Puritan doctrinal standard is the Westminster Confession of
Faith, 1640s. Even though it was produced in England, it spread to America to the
New England Puritans. Presbyterian and Congregational groups quickly began to
adopt it. The New England Puritans wrote the Cambridge Platform, very similar in
many respects to the Westminster Confession of Faith.

8. The Second London Baptist Confession of Faith 1977 is virtually a copy of the West-
minster Confession of Faith, the major differences falling in the area of baptism, etc.
Nevertheless, it is almost word for word the same, Most of the early Baptist groups
used this modified version of the Westminster Confession. W. L. Lumpkin, Baptist Con-
fessions of Faith (Valley Forge: Judson Press, [1964] 1979), pp. 235-296.

9. Benjamin Warfield, The Westminster Assembly and Its Work (New York: Oxford
University Press, 1931), p. 56, One of its greatest interpreters, he says of the West-
minster Confession, “The architectonic principle of the Westminster Confession is sup-
plied by the schematization of the Federal (Covenantal] Theology.”

10. Perry Miller, Errand Into The Wilderness (Cambridge, Massachusetts: Harvard
University Press, [1956] 1970), ch. 4.

Mi. Charles and Katherine George, The Protestant Mind of the English Reformation
(Princeton, New Jersey: Princeton University Press, 1961), pp. 50-55. Specific refer-
ence is made to such great Anglican thinkers as Bishop Ussher, who was firmly en-
trenched in covenant theulogy. For that matter, the Thirty-Nine Articles, the confes-
sional standard of the Anglican Ghurch, is remarkably similar ta the Westminster Con-
_fession of Faith. Both are products of the same pool of theology.

12, Eawin Scott Gaustad, A Religious History of America (New York: Harper &
Row, 1966), pp. 76-77.
