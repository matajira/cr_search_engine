136 THAT YOU MAY PROSPER

the Church receives what He has. Here, continuity is established.
Paul argues that Christ lays everything up for His Church:

He made known to us the mystery of His will, according to His kind in-
tention which He purposed in Him with a view to an administration suit-
able to che fullness of the times, that is, the summing up of all things in
Christ, things in heaven and on earth. In Him also we have obtained an in-
heritance, having been predestinated according to His purpose Who works
all things after the counsel of His will... . And He put all things in sub-
jection under His feet, and gave Him as head over all things to the Church,
which is His body, the fullness of Him Who fills all in all (Eph, 1:9-23).

So, the Church first encounters its inheritance at the meal of in-
heritance. The Church needs the food of Christ. It should cat it
weekly (Acts 20:7-12). Communion should not be viewed as some
insignificant ritual tacked onto the Christian faith. Jesus’ meal is the
place where the Church receives its inheritance and strength to con-
tinue on. As long as the meal is taken in faith, the Church will grow
and strengthen. Taken in unbelief, it will dic. Why? Because Christ
is really present (although not physically)! He is present everywhere in
the New Covenant, but His presence is special in the meal of inherit-
ance. Because He is everywhere, however, all of the illegitimate sons
(bastards) of the earth will eventually be driven off, or else adopted
into the covenant.

Conclusion

The Church dominates the world by means of this covenantal
mandate, extending the first dominion mandate forward. Dominion
began as a covenantal application. Christ stipulates that dominion is
still by covenant. But now it is time to be even more specific. It would
seem that if the general dominion mandate is covenantal, then the
particulars will be too. How about the main institutions of society?
We have already seen that the Great Commission involves nations
and families: the Church goes out and conquers family by family un-
til the Gospel reaches the upper political echelons of socicty.

In the next chapter, 1 will begin this covenantal application with
the family, showing how it plays a vital role in the over-all dominion
of the world. Some of the questions I will be answering are: What is
a family? How does the covenantal model resolve the divorce ques-
tion? How has the covenantal model been used in history to create
strong family ties?

I begin first by showing that the covenant is the Biblical model by
which the family is to be run.
