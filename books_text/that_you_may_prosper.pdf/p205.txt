The Biblical State Covenant 185

throughout Western history that compromised though well-inten-
tioned Christian philosophers have appealed to this Stoic concept of
natural law in support of some “neutral” system of social and political
order. Thomas Aquinas is the most famous of these scholars, but the
same mistake is common today. Roger Williams appealed to natural
law as the basis of the creation of a supposedly religiously neutral
civil government in the 1630s in New England. This is the appeal of
just about every Christian who refuses to accept Biblical law as the
legal foundation of political order and civil righteousness. The only
alternative to “one law” ~ whether “natural” or Biblical —is judicial
pluralism, a constant shifting from principle to principle, the rule of
expediency. It is the political theory of polytheism.

Pluralism

When I speak of the Biblical principle of plural authorities,
meaning plural governments, I am not speaking of what the modern
world calls pluralism, What the modern world calls pluralism is anti-
nomianism and relativism: many moralities. This ultimately means:
many gods. The Bible calls such a view polytheism or idolatry.

First, the effects of pluralism have been devastating to the Chris-
tian faith. Man is a religious creature, meaning he is covenantal.
The word “religion” comes from the Latin “religo,” meaning “to bind.”
This implies the idea of “covenant.” Nigel Lee says, “Religion is the
binding tendency in every man to dedicate himself with his whole
heart either to the true God or to an idol. In this sense all men are re-
ligious, for every man dedicates his powers to some or other object of
worship, either consciously or unconsciously.” Technically speak-
ing, “religion” is a covenantal “bond”! Man is a “religious” creature in
that he is made of a “covenantal” fabric (Gen. 1:1f.), Everything he
attempts—spending money, doing scientific research, writing a
book, as well as going to church—is religious in nature. As a matter
of fact, if a person is a non-Christian, his so-called “secular” activity
is just as religious as going to church, because he worships another
“god” through his endeavors, and thereby rejects the Christian Faith.
There is no “neutral” zone.

Thus, “pluralism” is a misnomer and a myth. Pluralism allows all
positions except the Biblical one. This is what we see in our society

7. The Central Significance of Culture, pp. 121. Also, see Henry Van Til, Calvinistic
Concept of Culture (Grand Rapids: Baker, 1959)
