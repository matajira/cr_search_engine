Revelation 255

J. True Transcendence (Rev. 1:1-20)

Indeed, Revelation beyins like the Deuteronomic covenant.
John opens, “The Revelation of Jesus Christ” (Rev. 1:1); Deuteronomy
starts with, “Moses spoke . . . all that the Lord commanded [revealed]”
(Deut. 1:3). One does not have to strain to detect the parallel be-
tween the Sinai “revelation” and the Apocalypse.? Moses had just
come from the presence of God where he had received the written
Word of God a second time. John is lifted up before the transcendent
Christ —in a graphic glorious description (Rey. 1:14-17)—to receive
the second “revelation” about the destruction of Jerusalem, the first
having been given at another mountain in the Olivet Discourse
(Matt. 24:1-51).5 The preamble of both books, therefore, opens with
statements indicating that the “words” are distinct, one of the three
ways the covenant presents the principle of transcendence.

There are also some other important parallels about the historic
situation of Deuteronomy and Revelation. Revelation is addressed
to the New Covenant people—“to the seven Churches” (Rev. 1:4)—
to tell them once again of destruction coming on the “ancient relig-
ion.” The destruction came in A.p. 70, so the contents of Revelation
had to have been written before the collapse af Jerusalem. This

 

means there were approximately forty years between the death of
Christ and the destruction of the Old Covenant capital, from the de-
finitive redemption to the actual clearing away of the old religion.
This was precisely the situation with Israel, as the nation sat on
the borders of Canaan, when Moses gave them the “second revela-
tion.” ‘There had been forty years between their definitive redemp-
tion from Egypt and the new entrance into Canaan. But the land of
Palestine represented the old ancient religions and their perversions
of the original covenant. Israel was entering with a “new” covenant,

4. Kline, Structure of Biblical Authority, pp. 73-74. Kline says, “The Book of Revela-
tion is replete with treaty analogues from its opening preamble-like identification of
the awesome Lord Ghnist; through the letters to the churches, administering Christ’s
covenantal lordship after the manner of the ancient lawsuit; on through the elabor-
ately expounded prophetic sanctions which constitute the major part of the book;
and down to the closing documentary clause and canonical curse.”

5, The key 10 this chapter is the near demonstrative pronoun, “this generation”
(Matt. 24:34). Also very important is the fact that the Markan account of the Olivet
Discourse clearly describes the same events in terms of the destruction of Jerusalem,
4.D. 70, See Chilton for a farther description. Also, J. Stuart Russell, Parousia
(Grand Rapids: Baker, 1983), pp. 66-114,
