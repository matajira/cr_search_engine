The Biblical Family Covenant 139

words, This account of the creation of woman and the first marriage
falls within such a covenantal context. The following introduces
briefly the first model of the family, It too follows the five-fold pattern,

True Transcendence (Gen. 2:18)

The section begins with a certain “revelatory” character, similar
to the Deuteronomic covenant: “Then the Lord God said’ (Gen.
2:18). God creates the first marriage and immediately distinguishes
marriage from creation, distancing Biblical religion from all ancient
pagan religions. Pre-Christian pagan religious cosmologies gen-
erally involve creation itself with the first marital union. For exam-
ple, the marriage of one “god” to another “god,” or even to man, pro-
duces offspring that populate the earth. Consequently, to worship
“god,” or the “gods,” is bound up in the family ancestory, The result
is that the worship of god is worship of the family!

Since God is transcendent, the family is not absolute, nor is it the
center of society. How ironic. Modern Christian writers frequently in-
sist that the “family” is the most important institution of society,
making it transcendent and absolute. They simply ignore the cove-
nantal aspect of Biblical religion, and they return to pre-Christian
pagan concepts of the family. Is it any wonder that we cannot tell
much difference from Christian and non-Christian teachers on the
subject? A covenantal view of the family begins here, distinguishing
God from the family, thereby placing Lordship in God and putting
the family in the proper role.

Carle Zimmerman gives modern man the best analysis of the his-
tory of the family, being a creationist and rejecting all traditional
“evolutionary theories” of its development.) His evaluation classifies
the family’s history into three categories: trustee (clan), domestic,
and atomistic. He explains that the family tracks these three “phases”
in every major periad of history, particularly in the West, although
he summarizes the same tendencies in the East.

For our purposes, however, he reminds us that the spread of

 

1. Carle G. Zimmerman, Family and Civilization (New York: Harper & Row,
1947). Although Zimmerman was a Harvard University professor and one of the
leading exports on the family, he was ignored, and still rernains ignored, because he
was 2 orvalionist. His works are mammoth and monumental, but very few take note
of him simply because he was a devout Catholic who believed strongly in creation
and rejected, particularly at the time he lived, the standard Marxist and evolution-
ary interpretations of history.
