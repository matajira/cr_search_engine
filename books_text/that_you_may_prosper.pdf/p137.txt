Continuity 17

The city of Sodom had been destroyed, but its educational influence
was still lefi on the children. Under the influence of Sodom, they be-
came spiritual sodomites. One day his two daughters caused their
father to get drunk, and then they seduced him. This incestuous re-
lationship eventually produced children: Moab and Ammon (Gen.
19;37-38). The covenantal way of expansion was inverted. The origi-
naj marital covenant said the children were supposed to “leave father
and mother” (Gen, 2:24), With their inheritance they were supposed
to take their new marital covenant and expand. Incest turns back
into the family and mutates expansion. In the case of Moab and
Ammon, they were never able to have part of the inheritance. Their
land was always just outside of the Promised Land. Lot did not dis-
ciple the future, and he and his children were disinherited.

Dispossession is a variation of disinheritance. Sometimes, when
enough of the people of God do not disciple their children in the cov-
enant, God brings a foreign power to dispossess and place them in
captivity. This slavery becomes the very instrument by God to re-
store the covenant, But note: this can only happen when there is a faithful
remnant. It is the shaw of guod faith that God should preserve the
whole group for the sake of the righteous.

For example, Daniel and his three friends were taken into
Babylon with the rest of Israel. Nebuchadnezzar and Belshazzar
wanted them to become talismans to force God to bless their king-
dom, But God forced their hand. He spared Daniel and his friends
from the fiery furnace and the lion’s den. And eventually, Cyrus
was raised up to lake them back to their inheritance. ‘The nation had
been prepared by captivity and imprisonment to receive its legacy.
Yet even in this situation, someone had discipled Daniel and his
friends (Dan. 1:8-21). Our point stands. Without discipleship, edu-
cation, and missions there is no future.

Conclusion
We began the chapter with the story of Jacob's transter of his in~
heritance to Joseph’s son Ephraim. This introduced us to the final
point of covenantalism: continuity. I defined it as a confirmation of
the true heirs that transfers inheritance. This confirmation has three
parts. First, there must be cavenant renewal. Normally, this is some
kind of worship service where laying on of hands judicially transfers

16. The biological results of incest are often some kind of deformity.
