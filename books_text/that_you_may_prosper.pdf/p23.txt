Introduction 3

I heard him. I will never forget a statement he made that has kept
ringing in my ears for nearly the last twenty years. He said, “We are
at the end of Christian civilization, and therefore, we are at the end
of civilization.”

Many would not agree with the premise of his comment, but
how many would really contest that we are at the end of a culture?
Most people sense it. Most Christians know it. Most secular intellec-
tuals won’t deny it. And I think the general populace would not hesi-
tate to admit that the Christian mores that undergirded this culture
for the Jast two hundred years are all but gone. No doubt many
would applaud this decline. But Christians are left asking, “How do
we recover our Biblical roots?”

Unfortunately, I don’t hear a clear solution coming frorn any sec-
tor. Some say evangelism. Many say small groups. A few say liturgy.
Others cry for political action. Certainly all of these have their
proper place, but when are we going to look to the Bible for a model?
When are we going to say, “Does the Bible tell of a time like ours,
when Biblical influence was lost, and then recovered?” When are we
going to look to see how éhey, the people in the Holy Scriptures, did
it? I believe the Bible tells of a time such as that. The Bible really
does have the answer, and the solution is right in front of our noses,
Tt is the covenant, and Josiah’s experience telis the story. Our fore-
fathers knew the story. It’s time we remember what they knew.

Our Biblical Heritage

We should never forget that covenant was the single most impor-
tant theological idea in early America. Not only the Puritans, but
virtually all Protestants came to the New World with this concept at
the center of their theology and practice. Congregationalists, Pres-
byterians, Anglicans, Continental reformed groups, and indepen-
dents were children of the Reformation. Federal theology, as cove-
nant thinking had been called on the continent, had taken root at the
time of the Middle Ages. In many ways, the dawning of the Refor-
mation was a revival of this ancient theology. Slowly it seeped into
European and British cultures, but not deep or fast enough.

When these diverse, yet similar, Protestant groups came to
America, they implemented what many Europeans had wanted for
centuries, Their rationale for applying the covenant was simple. The
members of the Godhead related by covenant.2 Since heaven is a

2. Edmund S. Morgan, The Puritan Dilemma (Boston: Little, Brown and Com-
pany, 1958), p. 93
