188 THAT YOU MAY PROSPER

Abuse of Gad’s name destroys any notion of true justice. It de-
stroys the whole covenantal fabric of civilization. Here is the impor-
tance of the oath, So, without explicitly mentioning the oath, Paul
draws greater attention to its necessity. His comments imply the male-
dictory oath in determining guilt. Then the application of the sword
becomes part of the process of creating continuity and discontinuity.

The magistrate is involved in the removal of those who practice
evil by means of the sword. The wicked are supposed to experience
discontinuity in their lives, The righteous maintain social continuity.
But what are the “evil practices” in the New Covenant Age for which
a person can receive the sword? The rationale for answering this
question is the same as the “ethics” section. By examining what the
“wrath of God” is poured out against in Romans 1:18ff., we can begin
to sec what the magistrate “avenges” in Romans 13:1ff.

Paul's list at the end of Romans | is very similar to the capital
offenses of the Old Testament. His summary includes idolatry,
homosexuality, murder, and teenage incorrigibility to name only a
few (Rom. 1:29-31). All of these offenses are capital crimes in the Old
Testament. Of course, even in the Old Testament the death penalty
is the maximum and noi necessarily the mandatory penalty."' The excep-
tion is murder because the only appropriate restitution would be to
render a “life for a life.” The “maximum, not mandatory” principle
would also apply in the New Testament.

As we shall sce below, Paul includes several “less significant”
offenses in the “worthy of death” category: untrustworthy, unloving,
deceitful, boastful. Thus, the New Covenant view of the State is not
to the end that every nation would become another “Hebrew Repub-
lic.” There are New Testament similarities and dissimilarities be-
tween Old and New Testament sanctions that should be noted.

Old and New Testaments

First, Paul’s Romans 1:18-32 language indicates that New Testa-
ment penal sanctions are similar io the Old Testament. The vast major-

1. Jordan, The Law of the Covenant, pp. 148A, Rev. Jordan makes the point that
adultery did not always require the death penalty by referring to Joseph and Mary.
He quotes the Bible text, “Joseph, being a just man... was minded to put her
Mary} away privately” (Matt. 1:19). Joseph could privately put away (divorce)
Mary since she was pregnant and “apparently” had committed adultery. Due ta the
fact that this option was apen to Joseph, Jordan concludes that the death penalty
was “maximum but not mandatory.” This could also explain why David was not put
ta death after his sin with Bathsheba.
