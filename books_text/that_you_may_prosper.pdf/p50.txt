30 THAT YOU MAY PROSPER

Understand, there is no conflict between objective and subjective
in the mind of God. God's subjective evaluation of His subjective
standards carries objective meaning. Something can be objectively
true only because the whole universe is sustained and interpreted by
a totally personal, totally subjective Creator. Christianity destroys
the false dualism between subjective and objective, between per-
sonal and impersonal.

Man’s covenant with God can only be declared by a transcendent
legal act. All other covenants are formed on the same principle. Let
us examine some cases in which this Biblical insight is the basis of
social relationships.

Judicial Theology in Society
The principle is this: transcendent declaration is what officially cre-
ates or destroys a relationship; it imputes the status of covenant life
or covenant death to the union.

Imputation of Covenant Death

Let’s begin with covenant death. If you were talking to me, and I
dropped dead on the spot, would you immediately bury me? I hope
not! You would need to call a coroner and have me declared legally dead.
Is this some unnecessary “legal fiction”? No. This principle of declar-
ing someone legally dead is built on the first point of covenantalism.
Who knows, you might “think” I am dead, when in reality I might be
in a deep coma. I could be buried alive. (This legal” way of thinking
makes good practical sense.) So life and death are covenantal. They
are not primarily physically determined.

It is true that I am not objectively dead because the coroner
declares me to be dead. He has objective standards to apply. What
we must say is that God establishes life and death, and He defines
life and death, and progressively as men discipline themselves to
God's covenantal standards, they can better apply these God-given
standards in history. From a legal standpoint, however, society must
delegate to someone the legal authority to declare sameone dead. He
may make a mistake, as men do, but he has the legal authority to
make this mistake, for he has the legal authority to make the declara-
tion. Without this delegated authority, a lot of murderers could
escape earthly justice.
