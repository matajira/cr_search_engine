282 THAT YOU MAY PROSPER

whole intrusionary premise evaporates. So, according to him, there
are only bits and pieces of the structure in other places of the Bible,
even the New Testament.

As a matter of fact, his references to other segments of the Scrip-
ture triggered me to look for the Deuteronomic structure in the New
Testament. But he fails to see that the five-fold arrangement of Deu-
teronomy is a covenant model in adi of its parts in aif of the Bible.
‘Thus, he is not able to come up with a precise covenant model for all
of the Scripture. Is it a five-point model? Or is it a six-point model,
with “depository arrangements” added?* He is not sure. I am not
sure either; but I can be a lot more confident than he is. I find no six-
point structure anywhere in the Bible; I see a five-point structure
repeatedly,

Nevertheless, I am greatly indebted to him, as I am sure he is to
Baltzer and Mendenhall, upon whom he largely depended for his in-
sights. If he can make good use of the discoveries of a pair of theolog-
ical liberals, I suppose I can make good use of the work of an amil-
lennialist who rejects (or does nothing with) all five points of the cov-
enant. Let me make myself clear by comparing his use of each of the
five points with my use of them.

Transcendence

One major difference is my treatment of the first part of the cove-
nant, as indicated by the word “transcendence.” This difference is
easy to pinpoint. Kline does not discuss the topic; I make it the cove-
nant’s fundamental point: the Creator-creature distinction. He does
not develop the theme that the distinction between what God said
and what Moses said points to the doctrine of transcendence. He
says only this: “Ancient suzerainty treaties began with a preamble in
which the speaker, the ane who was declaring his lordship and de-
manding the vassal's allegiance, identified himself. The Deuteron-
omic preamble identifies the speaker as Moses (v. 1a), but Moses as
the earthly, mediatorial representative of Yahweh (v. 3b), the heav-
enly suzcrain and ultimate Lord of this covenant.”> That is all he
says—no development, nothing. This is a good insight, as I have

 

 

4, Ibid., pp. 49-51.
5, Meredith G. Kline, Treaty of the Great King: The Covenant Structure of Deuteronomy
(Grand Rapids, Michigan: Eerdmans, 1963), p. 50.
