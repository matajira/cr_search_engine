Introduction 13

church he was a good husband and loving father. Meanwhile, he was
leading the life of a gambler and adulterer across town. The pastor
wanted to have the man removed. Members objected and instead
removed the pastor.

How about ethics in the Church? Docs the Church know the dif-
ference between right and wrong? Probably not. For decades a cer-
tain theology has taught that Christians are not supposed to obey the
Ten Commandments. The thinking gocs, “That’s in the Old Testa-
ment. God was one way in the Old and another in the New. I’m nat
under law, but grace.” Now after so many years of this kind of think-
ing and preaching, Christians do not know how to live. Take a look
at the number of /arge churches in your town that are involved in the
pro-life movement. This issue should be fairly clear cut. Killing
babies is wrong! Right? I imagine that your town is like all the rest.
The big churches are not interested in the pro-life issue. They will
not take a stand because it is too controversial. They are not moti-
vated to take a stand because they do not know right from wrong.
‘They really do not believe abortion is murder. If they did, they would
call for the death penalty for abortionists and abortion-seeking
mothers. How else is murder to be treated in Old and New Testa-
ments (Rom. 1:29-32)?

Now we come to the areas of oath and sanction in the Church.
Church membership is serious. Most churches require some sort of
membership commitment and/or vows, But the real test of the oath
of allegiance to the Church is discipline. Just mention the word, and
no one knows what you are talking about. Jesus taught that some
may have to be put out of the Church (Matt. 18:15-18). When was
the last time you heard of a church doing such a thing? Is this be-
cause everyone in the pew (or pulpil!) is so good? Hardly. Recently
in Oklahoma, a woman was excommunicated for adultery—as a
matter of fact she committed adultery with the mayor of the city. Her
behavior scandalized the Gospel of Jesus Christ. She could have
been forgiven, of course. But she did not want repentance. She
wanted to live in open sin and have all the benefits of a member in
good standing. The church cast her out. She sued the church and
won, The State upheld her violation of a sacred oath to the Church!

Finally, continuity has also been a concern in the Church. The
Church has faced the problem of maintaining continuity of belief in
the midst of diverse religious opinions. Most churches give voting
privileges to anyone who can commune. But since not everyone who
