MISSION: UNDERCOVER RADIO

Harry Caul

HANCES are that if you've got a communications receiver
(or even a scanner) you’ve come upon the results of the
rather unusual work I do. At the least, you’ve probably noted
the efforts of others who compete with me in my endeavors.

To put it in its simplest and bluntest terms, I supply infor-
mation, training, and various other services for persans and
groups interested in establishing long and short distance radio
communications without benefit of licenses from the host gov-
ernments involved. I’m not at all talking about the bootleg
pseudo-Hams and quasi-CB’ers who populate the airwaves;
those guys are horsing around while my clients are deadly
serious. Nevertheless, it is a pursuit which keeps me very
busy. It is impossible to imagine the Jarge number of folks
who have come:to feel that they want to have as much com-
munications security, privacy, and secrecy as possible—even
to the point of not appearing in anyone’s computer as the
licensee of a radio transmitter (assuming they could become
licensed in the first place).

Who might be such a client? Maybe the operators of a dia-
mond mine in Africa wishing to discreetly exchange market
and production information with their people in Europe. Or,
it could be an office of a multi-national corporation wanting to
exchange certain delicate information with other offices in the
world. You name it and there are those who want to exchange
messages in total privacy; information on strategic metals,
technologies, industrial data, information on financial mat-
ters such as stocks and bonds, bank accounts, currency ex-
changes, energy resources, and a couple of dozen other topics
—and they want to do it via the back door without passing the

Copyright 1982 by Popular Communications, Inc. Originally published
in the Sepcember 1982 issue of Popular Communications, (Published monthly
by Popular Comminications, Inc., 76 North Broadway, Hicksville, NY
11801, subscription price $12.00 per year.)

223
