214 CHRISTIANITY AND CIVILIZATION

IRS definition of a nonprofit, nontaxable organization and
whittles it down from there. “If the Radical Vegetarian League
of Ann Arbor came to us with a request for a microcomputer
network, we'd turn them down,” Vermilion says. “On the
other hand, we aren’t going to limit ourselves to totally safe
projects either. Apple Computer is an adventurous and pro-
gressive company.” Vermilion says he'd like to make at least
some grants to politically or socially active groups involved in
human-rights causes and to “certain types of peace organiza-
tions if they’re largely educational and are addressing needs
for citizen awareness and conflict resolution.

“We're obviously going to stay away from groups that
would be inappropriate for the corporation to back, but we
will be in the forefront when it comes to helping groups whose
aim is to benefit communities in innovative ways,” he says.

Apple and its fellow sponsoring companies — Hayes Micro-
computer Products, Software Publishing Corporation, South-
western Data Systems, Tymshare, and Visicorp—-recently sup-
plied $35,000 worth of computer equipment to help eight
community organizations set up communications networks.

As much as computers have been taken up by activists
throughout the country, it would be a mistake to assume they
have won over the hearts and minds of the entire activist com-
munity. Indeed, a strong current of ambivalence about using
the new technology runs through some of the more ideologically
oriented groups. On the one hand they recognize the utility of
microcomputers, but on the other hand there's resentment over
unresolved problems created by computer technology. “We don’t
want to be doing public relations work for the computer
manufacturers,” says Marcy Darnovsky, who is a member of
the Community Memory Project and is also active with the
Abalone Alliance, an antinuclear power group.

“Computers are being developed for things we don’t like:
they’re making jobs dull or eliminating jobs altogether, they’re
collecting information about people, and they're being used
by the military to kill people,” says Darnovsky, who also edits
the Journal of Community Communications, a counterculture
magazine that publishes critical analyses of the social effects of
technological advances in communications. “In terms of the
disarmament movement people need to be aware of these
negative aspects of computer technology,” she concludes.

Political reservations are not the only kind of apprehen-
