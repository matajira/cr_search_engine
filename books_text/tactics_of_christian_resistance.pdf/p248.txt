202 CHRISTIANITY AND GIVILIZATION

ligated to take a stand in order to uphold justice for himself
and his church. But his theology (usually based upon a
misunderstanding of Romans 13) will not let him stand or at
the very least renders him ineffective and he fights as “one who
beateth the air.”

What principles must guide such a Christian? In order to
answer that, consideration must be given to more fundamen-
tal questions: What relationship does the Chruch have with
the State? Are they friends or enemies? Is there perhaps a
master-servant relationship between them? These questions
are dealt with in the passage cited in Chronicles 26.

We have here a remarkable passage. It is the case of King
Uzziah who is at first honored by the Lorp. We are told that
“he did that which was right in the sight of the Lorn” (vs. 4)
and “he sought God in the days of Zechariah .. . and as long
as he sought the Lor, God made him to prosper” (vs. 5).
This “prosperity” is clearly outlined. He built towers and
fortifications (vs. 9) and organized the army under capable
men of war (vs. 12). He equipped his army with the proper
weaponry (vs. 14). God “helped him” against Israel’s covenant
enemies (vs. 7). As a result, his fame was spread far and wide
(vs. 15). But again, it is emphasized that his strength and
prosperity were not his own; his God “marvelously” helped
him. In faet, the word translated “marvelous” is used
elsewhere to describe God’s “extraordinary” works (cf. Job
37:5, 14). This is the type of help he received from the Lorp as
an obedient head of state ~ obedient, that is, unto the precepts
of the Lorp. In establishing a strong defensive posture for
Israel he illustrates one of the purposes of a good ruler, ie. to
do his utmost to secure the safety of his people. It should be
noted thai he specifically defended the church of God against
the covenant enemies who had long tormented her, such as
the Philistines (vs. 6-7). This is one type of application Peter
refers to when he says that rulers are sent by God “for the
punishment of evildoers.” (Philistines are certainly included
among “covenant-breakers” before God.)

However, the heart of King Uzziah was lifted-up in pride
and we are told that he transgressed against the Lorp and
went into the temple in order to burn incense upon the altar.
This offering of incense at the altar of God was expressly pro-
hibited to everyone (including kings) except the Levitical
priests ordained of God unto that function (Num. 8:19, 24).
