290 CHRISTIANITY AND CIVILIZATION

know you have any firm political views or understand the first
thing about your constitutional rights. In fact, it would be a
good idea to stand in front of a mirror and practice assuming a
“blank look” before going to jury duty. Third, when the judge
asks if anyone on the jury panel has a problem with the particu-
lar laws applicable to the case the panel is up for, just don’t re-
spond. You are not bound by oath to answer the judge’s every
whim. If only those individuals who agree with a particular law
are chosen, then what good is the right of jury nullification?

The bottom line is, do whatever you can within the bounds of the
law to be seated on the jury. Once the jury is seated, and they find
out that you are a committed Christian and a concerned
citizen, there is nothing they can do.

At that point, do what you are supposed to do. Sit in judg-
ment of fact and law. If the man is guilty of breaking a just
law, convict him. If not, then turn him loose. Make the fol-
lowing creed your creed when you are called to jury duty.

“The Juror’s Creed”®

I will not allow myself to be a juror unless I am certain that I
can protect the rights of the innocent as well as proclaim the guilt
of the criminal.

I will remember that when I take my oath I become a judge
and the judge becomes a referee.

1 will honor my obligation to be a judge and to judge both the
law and the facts as is my right and my duty.

J will not allow the referee, the prosecutor, or the other jurors
to talk me out of doing what I know to be right.

I will always be mindful that the accused is innocent until I
vote otherwise.

I will claim my right to interrogate the witnesses to eliminate
doubt because I will not vote against the accused if I have any doubt.

If I become aware that the Constitutional or other rights of
the accused are not being honored, I will automatically vote in
favor of the accused, unless 1 am completely satisfied that harm
has truly been done to others by the accused.

{ will vote in favor of any.defendant who is prevented from
presenting to me all of the evidence and testimony he relies on.

I will vote in favor of any defendant who is prevented from
telling me why he helieves I should find him not guilty.

May I never be an instrument of harm to anyone who does no
harm, or who has broken no law.

68. I don’t know where this “creed” originated. It was sent to the Institute
of Christian Economics after being copied.
