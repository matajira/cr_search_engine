REBELLION, TYRANNY, AND DOMINION 59

duced to a form of servitude in this foreign land. While Jacob
is never called a slave, the verbal root meaning “slave service”
is repeatedly used to describe his work. Laban’s treatment of
Jacob parallels in certain respects Pharaoh’s later treatment of
the Hebrews. Although Laban initially welcomed Jacob,
there came a change in Laban’s attitude which resulted in
Jacob’s reduction in status.3° After earning his wives, Jacob
labored six additional years (31:41), the period of slave service
(Ex, 21:2). Jacob was oppressed, we are told (Gen. 31:39f.).
God saw his affliction (31:12,42), even as He saw the affliction
of the Hebrews in Egypt (Ex. 3:7). In violation of custom
(Deut. 15:12-15), Laban would have sent Jacob away empty-
handed (Gen. 31:42), Even though Jacob had earned Leah
and Rachel, Laban acted as though they were slavewives
given by him to Jacob and so should not go free with their
husband (31:43; Ex. 21:4,7). Actually, it had been Laban who
reduced the women from a free to a slave status by using up
their insurance money (Gen. 31:15). Jacob did not steal from
Laban, but he did act to protect his interests, and God blessed
him in it (30:28-43). Finally, when things really got bad due to
the envy of Laban and his sons, Jacob simply fled. Again,
God prospered him in this, threatening Laban if he harmed
Jacob.

When Esau came out with 400 armed men to kill him,
Jacob bought his present-oriented brother off with a series of
handsome gifts. In all these things we see Jacob acting in a
shrewd and non-confrontative manner. There was no rebel-
lion in him. He sought to avoid trouble, and when trouble
came, he acted in a shrewd and wise manner to turn it away.
Jacob showed himself to be a master of deception and avoidance
when dealing with tyranny. He knew that now was not the
time to fight, and that God would invest him with dominion
when He and His people were ready. Jesus had the same phi-
losophy: “I say to you, do not resist him who is evil, but
whoever slaps you on your right cheek, turn to him the other
also. And if any one wants to sue you and take your shirt, let
him have your cloak also. And whoever shall force you to go
one mile, go with him two” (Matt. 5:39-41). The evil man is

30. This seems to be the meaning of Gen. 29:15. Cf. David Daube and
Reuven Yaron, “Jacob's Reception by Laban,” Journal of Semitic Studies
(1956):60-62. A family member would not have worked for wages, so Laban
here excludes Jacob from the family.
