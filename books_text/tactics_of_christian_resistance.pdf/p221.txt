CONFRONTATION WITH BUREAUCRACY 178

number of students who enroll. The most important tactic is to
use judicial harassment. It is expensive for tiny, struggling
schools to hire top-flight legal talent. The states, in contrast,
are using tax money to send in waves of lawyers to do their best
to tie up private schools in red tape and restrictions. How can a
small school expect to win? How many well trained, effective
lawyers are there who are familiar with the legal issues in-
volved? Not many.

What we need in education is precisely what we need in
every other area of life: decentralization. We need an army of
dedicated Christian school headmasters who are ready to say
“no” to the bureaucrats and defend their schools successfully in
court. We do not need large national organizations that are
easy to infiltrate, buy off, sidetrack, or frighten. Such organiza-
tions are run by bureaucrats, not fighters. The bureaucrats of
the State see bureaucrats in large private organizations as their
allies. But small local schools are driving the bureaucrats crazy.
They are springing up everywhere. They do not report on what
they are doing or where they are. These schools are like hornets.
There are too many of them to fight effectively one by one.

The tactical problem facing Christians is this: How can we
gain the benefits of a centralized, well-paid organization, yet
avoid the’ concomitant bureaucratization? How can we
mobilize the army, yet keep all the troops in the field, constantly
sniping at the enerny? How can we train local men to carry the
battle to government officials, yet make certain that the local
people are ready and able to fight successful battles? We do not
want precedents going against us because the local head-
masters and their lawyers were not well prepared. We already
face a situation where the civil governments are attacking
schools continually in order to get adverse legal precedents.

Obviously, few churches and Christian schools can afford
to hire local lawyers at $100 per hour. Besides, lawyers face
the problem of specialization. They have to educate them-
selves in a new field. There are cases to read, arguments to
master, in state after state. There is no doubt that since the
late 1970s, there has been a coordinated effort on the part of
Federal, state, and local education officials to limit the Chris-
tian schools. The state attorneys are no longer being surprised
by new arguments of the defense lawyers, as they were in the
early 1970s. Precedents are going against Christian schools
today. Prosecuting attorneys know who the better-known
