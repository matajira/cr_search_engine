4 GHRISTIANITY AND CLVILIZATION

They cheerfully seek to execute their duties toward God, to-
ward themselves, toward one another, and toward humanity,

Toward God, consistent Christians seek to obey ail His
commandments and to live according to ali His counsel as re-
vealed in ail His Holy Scriptures, both in the Old and in the
New Testaments, Toward themselves, consistent Christians
seek to condemn and to castigate their own (de-humanizing)
sins and to consecrate to Christ their own God-given
(humanizing) virtues. Toward onc another, consistent Chris-
tians seek to promote their mutual dedication to the Lord and
His commandments and their enmity toward Satan and his
devices. Toward humanity, they seek to promulgate God's
Kingdom in all of His realms and to demand the uncondi-
tional surrender of every man, wornan, and child (including
professing Christians), together with all their possessions, to
the all-embracing Kingship of the Lord Jesus Christ, and to
reign together with Him both in this present age now and
tomorrow and (still more) in the age to come.

Strategy

Consistent Christians must clearly understand their ulti-
mate goals and plot their strategy of conquest accordingly.

1, They must acknowledge that their program must at
every stage be subject to the revealed will of the Triune God,
whereby He would have us demand that all men willingly
submit to His authority right now, even though some will only
(unwillingly) submit after the last judgement. This should en-
courage us not to be discouraged if we do not see vast
numbers accepting His authority during our own lifetime. At
the same time, we have no right to assume that God does not
desire to transform all men presently alive into His Kingdom.
All men are His creatures and therefore all men owe Him
perfect obedience, and all categories of men (communists,
Zionists, Moslems, etc.) are, from the human viewpoint,
saveable, provided they repent and turn from their sins and
thenceforth serve the Lord Jesus Christ.

2. Although the coming of the Lord draweth nigh, it is not
for us to set dates, but rather to carry on working for Him as if
He were not yet to come for another thousand years or more
—as indeed He might not. Consequently, Christians dare not
be idle in any area of life as they wait for the Lord’s appearance.
