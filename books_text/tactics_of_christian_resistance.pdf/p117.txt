REBELLION, TYRANNY, AND: DOMINION 71

around. They were disappointed; Mr. Nixon’s conscience was
not sufficiently seared to permit him to act like a Democratic
Party politician, guilt-free, Bible-believing Christians had
high hopes for Jimmy Carter. Nced we add that they were dis-
appointed by the decisions made by Mr. Carter’s mother,
sister, and wife? And then the whole New Right got behind
Ronald Reagan, who by his appointments betrayed them be-
fore he even took office, and has now signed a bill, updating
social security, which directly taxes the churches.

I never did like the self-righteous whine of those Vietnam
war era pseudo-folk-songs, but may we be excused if we sing
one refrain of “When will they ever learn?”

Frankly, I believe that in all of this God has, as always,
been gracious to us. Are Christians in this country ready to
take charge? Heaven forbid! Virtually none of them knows
the first thing about the law of God, by which they are called
to govern,5+ Most of them do not even acknowledge the sover-
eignty of God.55 Few have any experience in governing, since
their churches have no courts, being at best mere preaching
points (where they have not degenerated into spas and literal
circuses). The most powerful New Christian Right people are
personality-cult oriented, one-man shows (and by shows I
mean shows: radio shows, television shows, and the putting
on of shows).

Thankfully, increasing numbers are seeking to be faithful
in smal] things. They are forming elders into genuine church
courts and conducting trials for offenders. They are studying
the law of God, which He gave to Israel and which is sure
wisdom for us. They are working with Christian lawyers to set
up Christian reconciliation and arbitration commissions,
dealing with divorce, with business conflicts, and with other
sticky situations. To the extent that they are involved in

54, When the Bible says chat ihe law is written on the hearts of believers,
it does not mean something magical. The law has w be learned, believed,
internalized, meditated on (Ps. 1, 119), and applied where possible. In this
way it issues in wisdom, and becomes part of the warp and woof of a
person’s life.

55. Not only are most Arminian, but increasingly popular is a new
Pelagianism which denies that God even knows in aclvance what we are
going to do. ‘This notion is pushed in several very large international Chris-
tian youth organizations, most prominently Youth With a Mission and
Agape Force, as well as in certain young denominations.
