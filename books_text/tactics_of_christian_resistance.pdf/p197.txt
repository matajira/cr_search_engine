CONFRONTATION WITH BUREAUCRACY 151
things “by the book”—a book which was written in the past in
terms of past conditions. The bureaucrat is rewarded for not
making a mistake in terms of a set of complex written formal
rules and unwritten informal rules, while the entrepreneur is
rewarded by consumers for accurately forecasting the future
in an uncertain market. Thus, bureaucrats seldom act rapidly
enough to stamp out a growing evil—“evil” being defined as
anything which threatens the smooth operation of the bureau-
cracy and the continued advancement of the bureaucrats.
They act in a fzecemeal fashion. They prefer to escalate, to re-
spond to recent conditions.

Bureaucracies seldom attempt to stamp out a movement.
They attempt to buy off the opposition’s leaders, or convince
the movement to impose self-regulation in terms of imposed
bureaucratic guidelines. They increase pressures on visible
manifestations of opposition movements. But most employees
of bureaucracies are slow-acting, benefit-seeking, work-
avoiding people who expect to be rewarded by merely staying
on the job and not making a procedural mistake. Few of them
are dedicated pursuers of institutional enemies, except in
cases where personal vendettas are involved. But personal
vendettas are always aimed against personal opponents or
against one limited segment of the opposition. Long-term per-
sonal vendetias against whole movemenis are almost never sustainable by
a bureaucracy. If nothing else, the dedicated agent gets pro-
moted, or he moves to another agency, or he breaks an inter-
nal organizational rule and becomes vulnerable to skilled vic-
tims who know the law, or to personal rivals within the
organization who are willing to cooperate with his victims in
order to unseat him. If nothing else, he eventually retires or
dies on the job.

The typical bureaucrat does not understand religious or
ideological dedication. He does not understand what it is
which motivates members of dedicated organizations.1¢
Nothing in his world prepares him for dealing with such com-
mitted people. He is used to scaring people with threats
against their business, or threats of fines. He is not used to
people who take on the bureaucracy, who fight by every legal

 

16. An indispensable study of dedication is Douglas Hyde's book, Dedica-
tion and Leadership (Notre Dame, Indiana: Notre Dame University Press,
1956). Hyde was a former Communist organizer. His book is a study of
Communist leadership techniques.
