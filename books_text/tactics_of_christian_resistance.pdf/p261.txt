COMPUTER GUERRILLAS 218

sion that enters the picture. In a study of 28 computerized
grass-roots groups, University of Wisconsin researcher
Timothy Haight found that while on the whole the groups had
few problems using computers, there were exceptions. Com-
puters for Christ, which serves fundamentalist church con-
gregations, is one. “Several of the ministries were concerned
that computers were a manifestation of the Beast of the
Apocalypse,” Haight reported.

Apples us. Chain Saws

If there is such a presence, it’s probably being felt by U.S.
Bureau of Land Management (BLM) officials in southern
Oregon, where a stubborn band of computer-wielding home
owners has shown that the personal computer is a match for
the chain saw. As of October of last year, the group had forced
the BLM to cancel a routine timber sale. Since then they’ve
entangled more sales in a web of protests and appeals that
could end up changing forestry practices throughout the
country. What’s happening in Oregon is a textbook case of
what a small band of seemingly powerless citizens can do with
a little help from their friends . . . and a computer or two.

Richard Bach’s best-selling book of yesteryear, Jonathan
Livingston Seagull, is a story about a seagull who does impossi-
ble things because he believes he can. It may sound corny to
make the analogy, but that’s pretty much what happened
when a group of rural neighbors came together over the
Bach’s computer and tock on the U.S. Bureau of Land
Management.

The tale actually began a couple of years ago when
Richard and his wife Leslie left Los Angeles and moved to the
idyllic Little Applegate Valley in the southwestern corner of
Oregon. “We looked all over the country for a couple of years
and finally found the spot,” Bach says, adding that it’s some-
thing “right out of a travel guide to Shangri-la.”

They bought a 20-acre parcel on a hilltop with a
panoramic view across the 15 miles to California, built a little
house there, and then listened in dismay as neighbor Diane
Albrechtsen dropped a bombshell: “They’re going to cut all
the trees down and they won’t grow back.”

The Bach’s were disappointed but philosophical. They
said, “Well, it looks like we moved to the wrong place and now
