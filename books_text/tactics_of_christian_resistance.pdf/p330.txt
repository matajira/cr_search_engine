284 CHRISTIANITY AND CIVILIZATION

The paralegal tax litigant és “rendering unto Caesar.”

Another passage Mr. Jordan refers to Matthew 17:24-27.
In this passage Mr. Jordan points out that Jesus miraculously
provides tax money for the disciples, even when they are not
actually liable for the tax. The message of Christ, according to
Mr. Jordan, is that since we are not in bondage to money, we
needn’t worry about having the money to pay our taxes and
provide for our daily necessities. God will always provide for
us. Once again, the assumption that the motivation to be in-
volved in the paralegal litigation movement is entirely
monetary is not entirely correct. To be sure, there are some
paralegal litigants who believe they are saving money by not
paying taxes. They are mistaken, however, because it is going
to cost the paralegal litigant more to fight, than to submit to
the unjust demands of the IRS. The main body of this paper
has been dedicated to showing one way that “We, the People”
can roll back some of the abuses of big government. The
motivation is political reform, not “pure personal covetousness.”

The third passage that Mr. Jordan refers to is Romans
13:1-7. He points out that Christians are “nod told that each
Christian ought to obey God’s civil law, willy-nilly, trying tc
force the hand of the magistrate. Rather, we are told to submit
to the existing order, pay our taxes, pray for the conversion o1
the magistrate, and proclaim the gospel to him and to all
men.” Further, Mr. Jordan remarks that “Paul does not say in
verse 6, ‘Pay taxes whenever rulers are servants of God,’ bui
rather he says to ‘pay taxes because rulers are servants of God,
whether they know it or not.” In response, I want to em-
phasize that the tax litigation movement is not setting aside
any law tor any reason. Its advocates contend simply that they
are not required to pay the income tax. The paralegal litigant
is not setting aside a law because the present administrators
are not enforcing God's law and Biblical political theory, nor
is he setting aside a law because they are simply not observing
the Constitution. He contends forcefully that he is obeying the
law, and insisting that the administrators of the law do the
same.

APPENDIX B: How to Use the Jury for Political Reform

Western. Civilization’s recognition of a right to a trial by
jury goes all the way back to the English Magna Carta in 121£
