WHO MAKES CHURCHES TAX EXEMPT?

Douglas F. Kelly

HE tax-exempt status of Churches is becoming a major

controversy in the United States. Many are asking the
question: What right does the Church have to be exempt from
taxation anyway?

Of course a matter such as taxation or exemption of the
Church is first of all a historical question. One has to look at
the historical context in order to give a proper interpretation
of our present status. It is clear that religiously, culturally,
legally, politically, etc. the American colonies are the direct
extension of Medieval, Christian Europe — particularly as this
tradition was mediated through the Protestant Reformation of
the 16th century and the Puritan movement of the late 16th
and 17th centuries.

An illustration of this relationship is the fact that English
Common Law precedents are binding in American practice
unless they are specifically set aside by American Law. For in-
stance, the Federal Courts claimed treasure that one Mr.
Fisher had found in a sunken Spanish Galleon off the coast of
Florida belonged to the Federal Government on the basis of
British precedent. Supposedly, the Federal Government is an
heir to the rights of King George ITI over all treasure found in
coastal waters. We may not agree with this particular inter-
pretation, but it is an illustration of the fact that American
jurisprudence cannot be understood without the European
and English background.

Now to come specifically to tax exemption of the Christian
Church. In the Roman Empire, when the Church first came
into prominence, it was treated by the Roman civil authorities
as a legitimate Jewish sect, and as such, the Church (like the
Jewish religion) was (in the equivalent of that day) fully ‘tax

Reprinted from Chalcedon Report No. 204, August, 1982. Copyright by
Chalcedon, P.O. Box 158, Vallecita, CA 95251.

229
