460 CHRISTIANITY AND CIVILIZATION

How a Congressional or Legislative Office Functions

You can tell a great deal about what your Congressman’s
intentions are by looking at how he organizes his office. Con-
gressmen and Senators in Washington have a certain staff
allowance and a certain office allowance. They have a lump
sum of money they can spend however they feel best to help
them carry out their dutics. They may have as many offices as
they want. And you can tell by looking at the configuration
what your Congressman is interested in doing. If he has six
different regional offices, and a minimum crew in the
Washington office, then you can know that he is more in-
terested in mending fences back home than he is in his
legislative function, On the other hand, many Congressional
offices are very top heavy in Washington; for instance, they
may have umpteen lawyers on the staff and numerous
research assistants. That office is interested in being a
legislator. Once you know what type you have, you have a
better idea how to proceed to lobby him. If he’s very interested
in what the folks back home are thinking, that's where you
ought to put your pressure. If he’s very Washington oriented,
and has most of his researchers in Washington, then that gives
you the clue that if you’re going to get to him, you must get to
the people in the Washington office.

Let’s now look at various sources of pressure that can be
brought to bear on legislators.

Media

Obviously, the media are a major source of pressure. Some
staffer in every Congressional office reads the hometown news-
paper every day. The folks who read it in the district office call
the Washington office every day and give an account of what
the editorials said, and the letters-to-the-editor, and the news
topics. Indeed, they will keep very close to what the hometown
press says. That’s why it is worth your time to get letters to the
editor in your hometown paper. Learn how to write succinct,
thoughtful, interesting Ictters to the editor, and then send them
in. Practice writing them until you learn how.

Most Congressional offices also read the New York Times
and the Washington Post, faithfully, every day. The Con-
gressmen and their staff also watch Good Morning America and
