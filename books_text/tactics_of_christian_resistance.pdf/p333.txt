CITIZENS’ PARALEGAL LITIGATION TACTICS 287

the right. In a dissenting opinion, Washington D.C. Chief
Federal Judge Bazelton challenges the hypocritical judicial
position regarding jury nullification:

Deliberate lack of candor . . . sleight-of-hand . . . a haphaz-
ard process. Is it true that nullification which arises out of ignor-
ance is in some sense more worthy than nullification which arises
out of knowledge? Nuilification can and should serve an import-
ant function in the criminal process. Trust in the jury is, after all,
one of the cornerstones of our entire criminal jurisprudence, and
if that trust is without foundation, we must re-examine a great
deal more than just the nullification doctrine. The noble uses of
the power provide an important input of our evaluation of the
substantive standards of criminal law. The reluctance of juries to
convict under the prohibition and fugitive slave laws told us
much about the morality of those laws. A doctrine that can pro-
vide us with such critical insights should not be driven under-
ground, We should grant the defendant’s request for a nullifica-
lion instruction, or at least permit the defendants to argue the
question before the jury. /f revulsion against the government has reached
a point where a jury 1s unwilling to convict, we would be far beiter advised
to ponder the implications of that result than to spend our time devising
stratagems which let us pretend that the power of nullification does not exist.
(US v Dougherty, 473 F.2d 1113 at 1130; 1972).

It should be obvious to any discerning reader that the
right of juries to nullify a law for the particular case they are
sitting in judgment on is very important. The most vivid way
of describing this important practice is to look at jury duty as
a “vote.”§7 In this country, the right of choice and a free vote is
very important and closely guarded. If a voter walked into a
voting booth, and there found a local sheriff’s deputy sitting
next to the voting machine, he would think that very unusual.
If, however, the deputy began to dictate to him how he should
vote, the irate voter would probably throw the deputy out on
his ear!

Yet, every time a jury is seated in this country, the judge does
Just what that hypothetical deputy tried to do! And no one iries to
stop him! The judge tells the jury, “You must uphold the law
to the ‘nth’ degree. You are only authorized to determine
whether the facts of the case meet the requirements of the law
for conviction.” The judge is telling the jury how to vote! Although T
am not a “conspiracy buff,” the effort on the part of the court
system to squelch the right of jury nullification is the closest

87. This “vote” includes both trial jury and grand jury duty.
