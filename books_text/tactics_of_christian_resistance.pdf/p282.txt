236 CHRISTIANITY AND CIVILIZATION

ments were deleted from the Treasury Appropriations Bill.
Those two amendments would have continued the withhold-
ing of funds from the IRS, to prevent the agency from im-
plementing its program to investigate and approve of church
schools and churches. The IRS now has the funds to vestigate
churches and educational ministries.

It is increasingly obvious that Christians must give serious
thought as to how to resist the Internal Revenue Service’s in-
vasion of traditional legal immunities (sometimes called “rights”)
possessed by American citizens under the U.S. Constitution
and common law. We no longer have the luxury of ignoring
the leviathan at our door. The beast has taken up residence at
the gate of the Church, and either he will be resisted and
driven away, or the institutional Church as we know it will he
compromised under its usurped sovereignty. We need to ask
ourselves as Christians: In what ways may we lawfully resist
the IRS? How are we to do so without being revolutionaries
and anarchists? Can Christians legitimately resist the IRS as
individuals, or must we submit as individuals and only resist
as churches? If we can legitimately resist as individuals, then
how can we do so and not end up like Gordon Kahl, the tax
protestor who shot and killed two federal marshals and a
sheriff, and who was finally blown up and burned beyond
recognition after a gun battle with county authorities in a
remote section of Arkansas?

It is my religiously held conviction that the Internal
Revenue Service is the Aeart of the present expression of the
messianic State. If unconstitutional activities of the IRS can be
resisted successfully, then we will have taken the first step to
rolling back government oppression and tyranny. The IRS
cannot be ignored. The Church does not have a choice whether
or not to fight the IRS. Either she fights, or she capitulates. There is
no middle ground. The Church must confront the beast of
messianic statism, eyeball to eyeball, and repel it. She must do
so, however, lawfully and constructively, not lawlessly.

Furthermore, there is a growing body of evidence to in-
dicate that the IRS can be successfully resisted by individual
citizens, as well as by churches. This essay is designed to in-
troduce Christians to this body of evidence. We can learn how
to resist the beast of messianic civil government through a
careful study of legal resistance techniques that have been
used successfully against the IRS by certain citizens’ paralegal
