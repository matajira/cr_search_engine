REBELLION, TYRANNY, AND DOMINION 79

evasion (and it has done both, even in recent years), then
Christians have good reason to believe that the exercise of
force will be employed by these powers, and thus submil.
There is nothing immoral, however, from a Biblical point of
view, with evading the draft or evading taxation, since decep-
tion is the proper way to deal with tyrants. It is pretty hard to
do, however, and the cost in psychological worry and distress,
not to speak of the cost if one is caught, renders draft evasion
and tax evasion unwise.

We must keep in mind that the pagan is primarily in-
terested in power. This means that the maintenance of force (the
draft)® and the seizure of money (excessive taxation) are of ab-
solute primary interest to him. If we think these are the most
important things, then we will make them the point of
resistance (becoming “tax patriots” or some such thing). To
think this way is to think like pagans, For the Christian, the
primary things are righteousness (priestly guarding) and
diligent work (kingly dominion). Generally speaking, the
pagans don’t care how righteous we are, or how hard we
work, so long as they get their tax money. This is why the
Bible everywhere teaches to go along with oppressive taxa-
tion, and nowhere hints at the prapriety of tax resistance. As
far as the pagan state is concerned, taxes are about the must
important thing, since they finance everything else. We
advised not to make an issue at that point, lest we become like
them, and because we are sure to lose any canfrontation on
that issue (after all, they presently have power). We know that
righteousness and work will overcome pagan power eventually,
so we can afford to ignore the tax issue. The pagans will give
up the Christian school battle long before they will give up the
tax Issue.

This is not even to note that tax resistance accomplishes
nothing positive anyway. Politically if the income tax were
overcome by tax protests, some other more efficient and sub-
tle form of taxation would replace it (maybe a Value Added

 

 

   

 

61. A forthcoming (1984) issue of Christianity and Civilization, now in
preparation, deals in depth with the draft, in a symposium an war and
revolution,

62. Fora brief discussion of relevant passages and concepts in the area of
taxes, see James B. Jordan “The Christian and ‘Tax Strikes,” in Biblical Keu-
nomics Taday TV :2 (April/May 1981), available for a conwribution fram ICE,
Box 8000, Tyler, TX 75711.
