294 CHRISTIANITY AND CIVILIZATION

given every priority in the court’s docket, so the appeal will be
expedited in every way. The law provides for the recovery of court
costs and lawyer's fees and any other litigation fees in cases against the
government, provided the government loses. Finally, when facing a
suit in Federal court, the government is required to file an an-
swer within 30 days, unless it wins an extension by proving
“exceptional circumstances.”

Before we reproduce a sample FOIA request letier, it is
important that you are aware of the nine exemptions allowed
in the Act. These exemptions should not be looked at as deter-
rents, but merely as circumstances under which an agency
may choose to withbold information. All claims of exemption
are subject to appeal.

EXEMPTION ONE: “(a) matters that are specifically au-
thorized under criteria established by an Executive order to
be kept secret in the interest of national defense or foreign
policy and (b) are in fact properly classified pursuant to such
Executive order.” Two comments are necessary concerning
this exemption. Under the Act, after receiving any request for
documents that are classified “SECRET,” the agency must re-
review the documents to determine whether or not the docu-
ments still warrant the “SECRET” classification. Furthermore,
always remember that a court may examine any document to
verify the claim of “national security.” In some past cases, the
danger wasn’t national security, but political embarrassment.

EXEMPTION TWO: “Matters that are related solely to
the internal personnel rules and practices of an agency.” This
exemption has been very strictly interpreted. It only exempts
material that is solely related to internal management or
organization. In other words, staff manuals instructing
employees on how to perform their jobs, or even job descrip-
tions are nol exempted.

EXEMPTION THREE: “Matters that are specifically ex-
empted from disclosure by statute.” This includes personal in-
come tax returns (when the request is made by a third party),
applications for patents, and completed census forms.

EXEMPTION FOUR: “Matters that are trade secrets
and commercial or financial information obtained from a per-
son and privileged and confidential.” Note the use of the con-
junction “and.” The material exempted here must be trade
secrets and commercial or financial information and obtained
from a person (that is not from a government agency) and
