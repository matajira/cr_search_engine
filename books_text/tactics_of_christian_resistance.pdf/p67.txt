BIBLICAL TACTIC FOR RESISTING TYRANNY ai

must be put to death. God’s secret will may be that the
murderer, like Cain, should escape alt merely human retribu-
tion. And how does all this work out in practice in human
history as God's eternal decrees are being worked out in time
and space? Well, God calls and raises up civil elders,
magistrates, to whom he gives the power of the sword and
clothes with His authority to take human life within the terms
of his law. And He places further restrictions by giving rules
of evidence such as a plurality of witnesses to establish guilt in
acapital crime. Then, if in God's providence He has raised up
faithful rulers subject to His revealed will, and He providen-
tially provides the proper testimony and evidence to obtain a
conviction, it obviously was His secret will that that particular
murder should be officially executed for his sin. But in spite of
the revealed will of God that requires his death, if any other
than a lawful magistrate upon due process should take his life,
that in icself would not be vindicatory justice but merely com-
pounding the act of murder. Now the problems of resistance
to tyranny and of reformation must be approached exactly the
same way.

God's revealed will may place the curse of His displeasure
on tyrants and usurpers, but this in no way is. sufficient to
undergird a movement of resistance. Like the woman taken in
adultery, tyrants may well deserve to be stoned to death. But
who has the moral authority to throw the first stone? Cer-
tainly not the guilty subjects who have brought Gods wrath
and curse down on their society by their wickedness! We must
always remember that although the tyrant may be worthy of
death, he is still, like the arch traitor Judas Iscariot, God’s in-
strument to fulfil God’s secret counsel. We must be careful not
to jump the gun lest we be found to be opposing the counsels
of the Most High. In due time when He is finished with His
purposes in. an instrument, then and then alone will He per-
sonally raise up an avenger, an Ehud, who will dispatch the
tyrant. But He will raise up an avenger that will meet all the
requirements of His revealed will. An avenger clothed with
the proper legal and moral authority who can without sin ex-
act the full penalty of God's righteous judgment on such sins.
Vengeance is mine, saith the Lord, I will repay, and He does,
and Reformed Christians should shudder to attempt to steal
that prerogative from the Almighty!

Now these exact principles also apply to the question of
