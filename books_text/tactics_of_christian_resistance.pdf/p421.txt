TACTICS USED BY REFORMERS 375

The Counter-Reformation

Within a generation these developments had created an
entirely new intellectual climate, had spurred the study of
Greck, Latin, and Hebrew, and switched the brightest of the
Humanists into new avenues of thought.

That these avenues were religious reflected the European
tradition and environment, and had the effect of pushing the
ancients and their presumed glories back toward the cemetery.
Persons appeared who scoffed at Aristotle, though Calvin was
not one of these; Calvin himself was one of the new breed.

Despite exceptions like George Buchanan, the great poet
of Scotland, the imtellectuals separated along generational
lines. Calvin became a Reformer, and Melancthon became a
protégé of Luther, but most older Humanists remained, for
the most part, within the safe folds of the Roman Catholic
Church.

That Church fell upon great difficulties. The Emperor
Charles V had loosed his soldiers upon Rome in a famous
Sack. Few events in history have had a greater impact upon a
people than the Sack of Rome upon the Italians. It came ata
time when they had dissipated nearly all their virtue in the
cesspool of the Renaissance. “After the loss of youth, love, and
hope,” Burckhardt wrote, “the only noble concept leit in the
wreck of character was honor.” That had been the last refuge
of the pagans as well. It went hand in hand with vengeance,
and with taking the law into one’s own hands.

The Sack occurred in 1527, and broke the nerve of Italy,
which has never recovered. The glittering days were over, and
ltalians lived under the contempt of French and Spanish con-
querors. The Vatican remained, however, a great power. The
Pope had his army and navy, his soldiers, his riches, and his
potent network of ecclesiastical courts and laws, of reigning
Cardinal and Bishop-Princes, of prelates in high places in
other courts, a diplomatic service, and a centuries-old creed
with enormous international properties and prestige.

The ferment of the Reformers created a counter-
Reformation, in which the Popes, shrewd in the lures of the
world, turned to art. The Reformers, in reaction, recoiled
from beauty as a snare, an illusion; a worldly diversion of the
devil. Their protests took the form of attacking churches and
pulling down crucifixes, burning paintings, invading and
