EDITOR’S [INTRODUCTION xix

was charged to appear in court.

Your readers, in my opinion, may be reading about an issue that
is not the real issue when they buy Sileven’s publication, (Letter
dated 5/23/83)

It is not Sileven’s supporters who fail to understand the
issue. It is this public school official who does not understand.
The issue is sovereignty: God vs. Satan, Christianity vs. secular hu-
manism, family vs. State, It is the B.A. in education which ap-
palls parents—a third-rate union card in an academic field
which is the Jaughing stock on every university campus in
America. Yes, Sileven was hauled out of a church. The school
was a ministry of his church, and parents, as sovercign agents
over their children, delegated the authority to instruct their
children to this church ministry. Those parents are sovereign
over the education of their children, not the State.?

These issues concerning legitimate sovereignty are at last
being drawn by American conservative Christians. The fact
that old-line (and humanist-infiltrated) Roman Catholic or
Lutheran schools long ago compromised on this sovereignty
issue provides an additional reason why it is not these schools
that have experienced the unprecedented, explosive growth in
Christian education since 1965. They believe that “religion is
an added part of the curriculum,” do they? Well, they are in-
correct: religion is nat an “added part.” Religion ts the ultimate
foundation of ail curricula, The question is: Which religion? The
battle over sovereignty in education is really a battle to decide
this crucial question: Whose religion will undergird education in this
nation, the “State-parent’s” religion or the covenant parents’ religions?

Nevertheless, Christians should be forewarned: millions of
conservatives and Christians do not recognize the unportance of the sov-
ereigniy issue. We cannot expect automatic support from our
supposed “natural allies.” This is why we must prepare
carefully before we launch any program of resistance. A
resistance program is an educational program and a public
relations program. It also involves important legal issues.

Constitutional law specialist John Whitehead has written,
several important books about this battle, most notably the
best-selling book, The Second American Revolution (David C.
Cook, 1982). In an essay published in The Journal of Christian

9. R. J. Rushdoony, Intellectual Schizophrenia: Culture, Crisis and Education
(Philadelphia: Presbyterian & Reformed, 1961).

 

 

 
