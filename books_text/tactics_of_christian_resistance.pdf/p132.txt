86 CHRISTIANITY AND CIVILIZATION

There is no defense against such perils without a vigorous
intellectual effort that enables us to discriminate between the
true and the bogus. What assumptions lie behind the stand-
ard appeals to security and prosperity, justice and equality?
Whose ends are being served? Whose gods are being served?
What are the consequences of the measures that this group
would like to put into effect? We shall not be able to deal with
questions like these until we prepare intellectuals who are able
to penetrate with clarity the “advertising” that Eliot warned
about, which permeates the output of all of our influential in-
stitutions. We have not yet devised a way to train them with-
out making use of the major advertising agencies—~the
universities and seminaries of the reigning idolatries.

When the cultural life of antiquity collapsed with the
Roman Empire, and a centuries-long era of darkness followed,
it was a corps of Christian intellectuals who kept the
manuscripts, and the skills to use them, from disappearing
from the face of the earth. We cannot say how long or how
serious the present decline will prove to be, but once again
Christians can stand in the gap against barbarism. Just as the
biblical doctrine of creation demystified the world and made
science possible, so other aspects of the faith are needed to
destroy the follies of the modern idolatries. We must demystify
the nation so that patriotism dees not serve as an excuse for
killing people. We need to demystify the state so that it cannot
do evil with impunity. We need to demystify wealth and poy-
erty so that they do not remain principles of human worth.

None of this can be done without the intellectual
sophistication to detect the special pleading and the false
assumptions of the intellectual world arrayed against us. We
need it to defend against tendentious proposals accompanied
by “studies” that purport to buttress the most destructive
utopia-mongering. Christian intellectuals need the courage
and confidence to stand fast, if need be, against the near-
unanimous weight of scholarly fashion,

Christ Against the World

To use the language of militance in deseribing Christian
responsibility in the world is to advocate a view of societal
relevance that is far from unanimously held. By and large,
Calvinists of one kind or another have supported it. The
Arminian and Pietist positions, on the other hand, are much
