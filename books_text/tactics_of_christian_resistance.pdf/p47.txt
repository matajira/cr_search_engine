I._ FUNDAMENTAL STRATEGIES OF
CHRISTIAN RESISTANCE

THE CHRISTIAN MANIFESTO OF 1984
(an answer to the Communist Manifesto of 1848
and to George Orwell’s Nineteen Kighty-four)
A victory blueprint for world take-over by a.p. 2000 by Christian
conquerors who “Preach the gospel!” ans “Subdue the earth!”

Francis Nigel Lee

And God said, ‘Let Us make man in Our image, after Our likeness:
and let them have dominion over the fish of the sea, and over the fowl
of the air, and over the cattle, and over all the earth, and over every
creeping thing that creepeth upon the earth’ So God created man in
His own image, in the image of God created He him; male and female
created He them. And God blessed them, and God said unto them,
‘Be fruitful, and multiply, and replenish the earth, and subdue it: and
have dominion over the fish of the sea, and over the fowl of the air,
and gree every living thing that moveth upon the earth’ (Genesis
1:26-28).

And Jesus came and spake unto them, saying, ‘All power is given
unto Me in heaven and in earth. Go ye therefore, and teach all na-
tions, baptizing them in. the name of the Father, and of the Son, and of
the Holy Ghost: Teaching them to observe all things whatsoever I
have commanded you: and, lo, I am with you alway, even unto the
end of the world’ (Matthew 28:18-20).

Introduction

A specter is haunting the stage of world history—the
specter of consistent Christianity. All the powers of the
world have cntered into an unholy alliance to exorcise this
specter: American pragmatists and European humanists,
Russian communists and Red Chinese revolutionaries, Islam
and Zionism, freethinkers and freebooters, anti-intellectual
pietists and incorrigible antinomians.

It is high time that consistent Christians should openly, in

First published in 1974 as part of the epilogue to Dr. Lee's massive work,
Communist Eschatology —A Christian Philosophical Analysis of the Post-Capitalistic
Views of Marx, Engels and Lenin (Nutley, NJ: Craig Press, 1974), pp. 840-848.

1
