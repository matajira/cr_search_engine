LEVERS, FULGRUMS, AND HORNETS 403

Prohibition movement, anyway. They have not bothered to
examine the Bible in a search for the first principles of society
that lead to progress and wealth. They have allowed human-
ists to capture the seats of influence and power in every in-
stitution. As Campus Crusade for Christ’s brochure for its In-
ternational School of Theology puts it: “According to a 1980
Gallup. Poll, 50 million Americans identify themselves as
born-again Christians. Yet where is our effect on our
nation?"! Some 50 million self-professed “born again” Chris-
tians have become, in effect, a political minority, while an
elite group of humanists has taken over the key institutions of
society.

Today, slowly but surely, Christians are beginning to
realize that there is no neutrality before God.? This means
that there is no neutrality possible in law, politics, and educa-
tion. Thus, it is cither the principles of the Bible that will rule
supreme on earth, or other principles opposed to the Bible.
But some principles must be supreme.? There are no moral
vacuums, intellectual vacuums, or power vacuums. To cite an exam-
ple: Law makers and courts have only two choices concerning
abortion: a baby in its mother’s womb is going to be allowed
to live or someone is going to be allowed to abort it. There is
no “neutral” place for the baby to hide, or for us to hide from
this question. Christians finally have been forced by the pres-
ent lawlessness of our legal system to come to grips with a
moral, legal, and political issue which cannot be decided
neutrally.

Another example is education. It is now becoming clear to
a growing minority of Christians that education is not
neutral.* A few of them now see that the public schools are the
tax-supported established church of secular humanism, where
humanism’s priesthood indoctrinates our children. A new, un-
familiar set of problems now faces American Christians. How

1, New Distinctives in Theological Education (1982).

2. For a popular discussion of this point, see Franky Schaciler, A Time for
Anger: The Myth of Neutrality (Westchester, IL: Crossway Books, 1982).

3. R. J. Rushdoony, Bp What Standard? (Tyler, TX: Thoburn Press,
[1958] 1983).

4. R. J. Rushdoony’s book, Iniellectual Schizophrenia: Culture, Crisis and
Education (Philadelphia: Presbyterian & Reformed, 1961), was one of the
early books to defend this thesis. See also Alan Grover, Ohia’s Trojan Horse
(Greenville, SC: Bob Jones University Press, 1977).
