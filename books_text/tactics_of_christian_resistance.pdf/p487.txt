TOOLS OF BIBLICAL RESISTANCE 441

have target practice on Sunday. Virginia’s Kennesaw prece-
dent was enacted in 1658 when the Colony required every
householder to have an-operable firearm in his house. In 1673,
Virginia law provided for government purchase of a firearm
for the indigent, although repayment was required as the per-
son was able.

The Massachusetts legislature required freemen and in-
dentured servants alike to own firearms, and in 1644 went so
far as to impose a six shilling fine upon a disarmed citizen.

After successfully throwing off the chains of a standing army,
the former colonists made sure that the Constitution of the
new Republic protected the people from such an affliction.

In Elliot’s Debates in the Several Siate Conventions published in
1826, we have a precise record of what the framers meant by
“militia” and the language of the second amendment. George
Mason, author of the Virginia Bill of Rights, pointed out that
the British had tried “to disarm the people — that was the best
and most effective way to enslave them . . . by totally disus-
ing and neglecting the militia.”

Who are the militia, Mason asked? “They consist now of
the whole people, except a few public officers.”

Patrick Henry put it this way: “The great object is that
every man be armed and everyone who is able may have a
gun.”

In a pamphlet aimed at swaying Pennsylvania toward
ratification of the Constitution, Noah Webster argued: “Be-
fore a standing army can rule, the people must be disarmed as
they are in almost every kingdom in Europe. The supreme
power in American cannot enforce unjust laws by the sword,
because the whole body of the people are armed, and consti-
tute a force superior to any band of regular troops that can be,
on any pretense, raised in the United States.”

(If anyone doubt the continuing efficacy of Webster’s state-
ment, consider the Afghan resistance to Soviet invasion. For
nearly two years a poor people armed with mostly World War I
rifles have been able to stalemate the most sophisticated army
of aggression the world has ever seen.)

The author of the second amendment, James Madison,
mentioned in Federalist Paper 46 that: “The advantage of be-
ing armed, which the Americans possess over the people of all
other countries [and that] notwithstanding the military
establishments in the several kingdoms of Europe, which are
