192 CHRISTIANITY AND CIVILIZATION

Vietnarn War, the What's Left professor at Purdue had an en-
tire class waving slide rules at one another, shouting about
ballistics and trajectory patterns, proving and disproving one
hypothesis and then the other.

The astrophysicists usually won, It’s a shame they weren't
running the war.

Helpless, dejected, the What's Left at Purdue were looked
down upon by their peers at more radically chic universities.
For a few brief months, it looked like things might be happen-
ing at Purdue when a student demonstration ended in the ar~
rest of more than twenty students. But then the Administra-
tion expelled them all and Purdue’s short-lived revolution was
over.

Just imagine how difficult it was to get published! Pro-
fessors have always lived by the “publish or perish” rule, but
what respectable New Left publication was going to print an
article by a professor from a campus like Purdue? A librarian
from Bob Jones had a better chance!

One might make the case that the Great Bongo-Bongo Inci-
dent had to happen. It was inevitable.

Tt all started when in my weekly column for the Purdue Ex-
ponent, I criticized the sloppy scholarship of another article
aimed directly at me personally. He pointed out that I had
once been a student of his and that I could only have been
talking about him when I spoke of the Political Mythology
Department. Furthermore, I represented everything that was
wrong with this country (“Mr. Johnson, you do not think, you
do not feel, you do not care, you do not . . .”).

Needless to say, a student columnist rarely gets the
privilege of having a professor go off the deep end and engage
in a personal attack, in a student paper, no less! I was ecstatic!
Thad not intended to aim my article at any individual, least of
all Professor Raymon, But when he responded with such
wild-eyed rhetoric, I played it for all it was worth.

In the next issue, I pointed out that I wasn’t speaking about
anyone in particular in my first article. Furthermore, my
description of the Political Mythology professor was so un-
complimentary, that one must have a severely damaged self-
image to identify with such a caricature.

That did it. He came completely unglued. His next letter
went on and on. (The first letter had been so wordy and
pointless that the editors had several times been forced to
