126 CHRISTIANITY AND CIVILIZATION

safe, obscure, uncontroversial, but most of all, non-theacratic
Legge.

There are other examples of Dr. Schaeffer's dependence
on Rey. Rushdoony’s researches, going all the way back to
1963, when Dr. Schaeffer delivered two lectures based on an
early spiral-bound version of Rev. Rushdoony’s This Independ-
ent Republic. (The lectures, entitled “Relativism in the 20th
Century,” are available on four cassette tapes for $16 from
L’Abri Cassettes, P.O. Box 2035, Michigan City, IN 46360;
we recommend them highly.) But the spiral-bound version of
This Independent Republic was published a decade before his
book which advocated the imposition of Old Testament civil
law, The Institutes of Biblical Law (1973). It was not yet clear in
1963 that Rev. Rushdoony is a proponent of theocracy, nor
that he is a believer in an optimistic eschatology. Dr. Schaeffer
has subsequently seen fit not to footnote his books.

This, of course, is to say nothing of Dr. Schaeffer's vast in-
debtedness to the work of Cornelius Van Til, under whom he
studied apologetics at Westminster Theological Seminary in
the 1930s. Consider, for example, Dr. Schaeffer's quasi-
Vantillian treatment of the relationship of the Trinity to the
One-Many problem in He Is There and He Is Not Silent (pp.
14-20). Again, Dr. Schaeffer’s published works contain exactly
zero references to Van Til. (To obtain Prof. Van Til’s little-
known, still unanswered mimeographed critique of Dr.
Schaeffer, “The Apologetic Methodology of Francis A.
Schaeffer,” write to Geneva Divinity School for information,
708 Hamvasy Ln., Tyler, TX 75701.)

The point here is not to catalogue every missing footnote
in the Schaeffer literature. Nor is it to single out Dr. Schaeffer
for special censure in this regard, for the same can be said of
virtually every prominent leader of the so-called “Christian
Right”: Without admitting it, they are getting much of their
material, their insights, even their slogans, from the Christian
Reconstructionists. But since Dr. Schaeffer is widely’ pub-
lished and rightly regarded as a major Protestant evangelical
philosopher and scholar by the fundamentalist world, one
might expect more candor from him. Readers of Christianity
and Civilization should understand what is going on in the
revival of Christian activism, and what has been going on in
certain cases for two decades.

What seems to be the case is this: the Christian Recon-
