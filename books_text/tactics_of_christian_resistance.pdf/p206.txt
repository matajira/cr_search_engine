160 CHRISTIANITY AND CIVILIZATION

are in a strong bargaining position. Any time the interviewer
can insert some “action footage” during a program of “talking
heads” (discussion format), he wil! be tempted to schedule the
interview. This is also true of secular talk format shows. Try to
get on local secular talk shows. Few people watch these shows,
but some do, and the local bureaucrats are not really certain
how many people, or which people, are watching. It makes
them nervous.

Ifyou can get ona talk show, be sure to use your videotape
machine to record the show off your T.V. set. This tape can be
played at public meetings later on. The program has been
produced professionally by a “neutral third party,” so it carries
more weight.

Obviously, the angle you need to stress on a T.V. inter-
view is freedom of speech. This is the be-all and end-all of the
broadeaster’s worldview. If you can constantly stress that the
State is restricting your freedom of speech, you have scored
points with the broadcaster, since his self-interest (and
ideology) is at stake when freedom of speech is threatened. Do
not allow yourself to be deflected from this theme. The tape of
your televised interview may be used later on in an ad-
ministrative hearing or even in court. You must establish your
case early, and never deviate from it: you are defending your first
amendment ltherties.

The Press Release

Before the Second World War, newsmen did not rely on
press releases for their stories. Today, it is the first thing they
look for. Therefore, the first thing a church or school should
do after calling the news media is to prepare a two-page,
double-spaced press release. Why double spaced? Because
newsmen find double spacing easier to read and quote from.
Keep the paragraphs short—no longer than four lines each.
Why? Same reason. If necessary, use subheads (the way I do
in this essay). Make it easy for the reporter to get the story in a nut-
shell. Remember, the invading bureaucracy probably did not expect
this, and it will take time for the bureaucrats to get their official state-
meni cleared. Meanwhile, the newspaper is devoting more
space to your side of the story because you have provided the
reporter usith more copy.

‘This means that your church needs a photocopy machine that
