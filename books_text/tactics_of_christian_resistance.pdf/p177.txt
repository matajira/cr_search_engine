APOLOGETICS AND STRATECY 131

dreds of thousands have fought to the death—and won. The
two Humanist Manifestos are not so romantic as the Communist
Manifesto, but they too give a specific plan of action for world
domination, and they, too, have been very effective. Even
most evangelicals think in terms of humanist categories by
now.

Then there was Nigel Lee’s Christian Manifesto of 1984
(printed elsewhere in this volume), originally published in
1974. It positively drips with confidence and assurance of
victory —~ the inevitability of Christianity. And it gives a pro-
gram for accomplishing its goals. It deals with strategy and
tactics. It is written in terms of a clear, objective, identifiable
standard. It is highly motivational. With Nigel Lee’s
manifesto, you could raise an army. What kind of army will
we raisc with Dr. Schaeffer's manifesto? And what will it
accomplish if we do?

Is this too harsh? Normally, a book’s potential is no greater
than its message. The message we need is an answer to a ques-
tion Lenin asked—a question which had been asked by
numerous Russian revolutionaries before him. Lenin’s an-
swer to it overthrew a government: What Is to Be Done? If our
“manifestos” fail to answer this absolutely crucial question, we
should not be surprised when alien religions take the podium.

Is Dr. Schaeffer's manifesto worthless? Not at all. Its chief
value will be in its “toughening up” quality, mentioned
previously. -There will be some who will read it, become
motivated by it, and then realize that there musi be more.
They will realize that Schaeffer's position, although well
stated, is untenable. It hesitates between two incompatible
views: a society built in terms of the myth of neutrality and a
society built upon the specifics of God’s revealed law.

“Equal Time for Jesus”

A Christian Manifesto has fundamental weaknesses, as we
have seen. But these weaknesses are not simply random. They
are part and parcel of a pattern of weaknesses that has afflicted
apologetics in general, and Dr, Schaefler’s apologetics in par-
ticular, from the beginning. We have already mentioned these
three weaknesses: 1) an unwillingness to make a definitive

48. Gary North, “What Kind of Army?" Christian Reconstruction, IV
(July/Aug., 1980).
