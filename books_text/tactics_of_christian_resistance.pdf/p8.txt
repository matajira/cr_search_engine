WHO MAKES CHURCHES TAX-EXEMPT?
By Douglas F Kelly. co. 0. ccc ccc cece eects 229

CITIZENS’ PARALEGAL LITIGATION TACTICS
By Michael R. Gilstrap.. 0.110200 cece cece eres 233

PART III: OFFENSIVE TACTICS OF CHRISTIAN
RESISTANCE

A CHRISTIAN ACTION PLAN FOR THE 1980s
By Pat Robertson. 0.0.0 c cece SOF

THE CHURCH AS A SHADOW GOVERNMENT
By Ray R. Sutton. . . 313

TITHING: FINANCING CHRISTIAN RECONSTRUCTION
By James Bo Jordans... cca cece eee BIS

‘ TACTICS OF RESISTANCE USED BY THE
PROTESTANT REFORMERS

   

By Otto J. Scott. 000 coc ect tteee eee 371
THE USE OF MONEY: A Sermon

By John Wesley... 0.2.0 390
LEVERS, FULCRUMS, AND HORNETS

By Gary North. 0.0.0 ccc cece ccc ec ec eee ceeceeeeeyeeees 401
TOOLS OF POLITICAL RESISTANCE

By Lawrence D. Prall. 0006060 ccc ce cv eve cee eee e eee 432
POLITICAL RESISTANCE TACTICS

By Paul M. Weyrich, 00. ee cee 449
HOW TO LOBBY

By Connie Marshner. 00.6000 c erect e eee 459
Contributors 2.0... e cents 470

 

About Christianity and Civilization - 472
Publication Schedule 2.2... 0.6 0-202. c cece eee eee eee 479
