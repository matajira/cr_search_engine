xxiv CHRISTIANITY AND CIVILIZATION

sin-free, but it means that he honored the terms of the cove-
nant and made continual burnt offerings as sacrifices to God
for his sin and the sins of his family (1:5). He was a shining ex-
ample of righteousness.

God called Satan’s attention to Job. Satan’s response was
immediate: he appealed to the doctrine of economic determinism,
a variation of the environmental determinism which Adam
used to justify his actions: “The woman whom thou gavest to
be with me, she gave me of the treé, and I did eat” (Gen.
3:12). He is faithful, Satan argued, because God provides him
with external blessings. In short, God has bought him off.
God is, in effect, paying tribute to Job. It is Job who is sover-
eign here, not Gad. Remove the benefits, and He will curse
God. By implication, Satan said that God's followers are in it for
the money, and that if God should stop providing benefits, all his people
will rebel. If the perfect man does, then they all will. The key is
economics: without benefits to offer His followers, God would
be a Commander without an army.

Satan repeated this argument with another perfect man,
Jesus. He offered Him the kingdoms of this world in order to
gain Jesus’ worship.!! his attempt failed, for Jesus
answered: “Get thee hence, Satan: for it is written, Thou shalt
worship the Lord thy God, and him only shalt thou serve”
(Matt. 4:10). This is the eternally binding response. Job never
denied it. A curse of God never came from his lips, despite the
pleadings of his faithless wife (2:9).

Job misunderstood the cause-and-effect nature of faithful-
ness and blessing. He was not an economic determinist, as
Satan is. He did not worship God because of the economic
benefits God poured out upon him, He lost them all, but he

il. Did Satan own the kingdoms of the world? No, God owns the earth
(Ps, 50:10-I1), But Satan did hold temporary title as a steward because of
Adam’s transfer of title to him at the Fall. Adam decided not to act as Gad’s
steward, thereby placing himself under Satan’s power. In this sense, Satan
could offer Jesus the world’s kingdoms as a /easehold. But Ee would have re-
mained an ethical subordinate under Satan. After Satan's defeat at the cross,
he possessed everything as a squattér; title reverted to the perfect man,
Jesus, and to His disciples, through the Jubilee principle (Luke 4:16-21).
Jesus announced the Jubilee Year immediately upon His return from
Satan's temptations in the wilderness. In principle, (he temptation was over
with respect to stewardship under God. The only ternpration remaining was
the temptation not to go to the cross. After His resurrection, all power was
transferred to Him (Matt, 28:18).
