56 CHRISTIANITY AND CIVILIZATION

takes his men and rescues Lot. We may see this as analogous
to vigilante action only if we understand that it was not based
on an abstract principle (free enterprise versus socialism), but
on covenantal, familial responsibilities. It is, in fact, an example
of the kinsman-redeemer/avenger-of-blood principle. Abram
was Lot’s next of kin, and it was his lawful responsibility to
rescue him if he could. Because kidnapping is a capital crime
(Ex. 21:16), Abram could lawfully kill men, in Chedorlaomer's
army, in his rescue of his kinsman,?¢

Abram simply rescues Lot. He does not take over the gov-
ernment of the area, and in fact refuses any power when it is
offered to him. To be sure, in the battle some people probably
died; but this should be seen as a survival operation, not as a
type of resistance or revolution. Abram did not worry about
what he had no control over. He did not bite off more than he
could chew. He had enough forces to deliver Lot, and he did
so. God had told him He would give him the land in His good
time. Abram was willing to be patient and await investiture
by God.

Avoidance as a tactic is seen in the life of Isaac. This is
clear from Genesis 26. Again a famine drives the patriarch

26. Abram was living in Hebron, where he had placed a sanctuary-altar.
Later Hebron became a city of refuge. Abram was Lots cily of refuge, as God
had offered to be Cain’s earlier. When Abram leaves Hebron with his men
and travels hundreds of miles to recover Lot, he is extending the boundaries
of the city of refuge to cover his kin.

I dare not go into this here; space and my own lack of requisite know!-
edge forbid it. I can say, however, that the blood avenger in Scripture is an
agent of the land, called up by blood spilled on the land (figuratively in the
case of Lot). The land appoints the next of kin, not the civil magistrate. The
killer may flee to a city of refuge, a sanctuary, where he will be tried by
officials of church and state (since these were Levitical cities). Abram’s
placement of altars around the land was also a placement of sanctuaries.
With the death of Christ, all che land is definitively cleansed, so that blood
no longer defiles the land in this sense, Romans 13 states that the magistrate
is to be God's bleod avenger. Whether this means that the family is no
longer permitied to be involved is a good question. For centuries the
churches functioned as sanctuaries. Someone needs to take this up as a proj-
ect and see how the church historically has applied the city of refuge princi-
ple to church buildings, and whether or not a Christian civilization might
have a place for avengers of blood. One thing the Biblical system did was
put on the apparently guilty man some burden of proof to show his in-
nocence. He had to flee to the sanctuary, and then plead his innocence be-
fore a tribunal. There is a Jot that needs exploring here.
