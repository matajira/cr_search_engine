174 GHRISTIANITY AND CIVILIZATION

There are many reasons for this escalation of pressure.
The private school movement is now visibly threatening the
survival of the modern humanist State’s most important insti-
tution, the public school. The public school is the humanist’s
equivalent of the established church. The priesthood of
state-certified leachers no longer has its monopoly. The source
of new recruits to the humanist State is being reduced drastic-
ally. The inner-city schools are already doomed. By 1980,
under 27% of the schoolchildren in the Los Angeles city
schools were white; four years earlier, the proportion had
been about 40%. Forced busing had been ideologically consis-
tent with bureaucratic equalitarianism, but it had also been
politically suicidal. The Los Angeles city school system will
not get back its white, middle-class students. They are now in
the suburbs or in private schools.

Within a decade, the public school systems of the major
cities will be overwhelmingly composed of poorly trained,
poorly motivated minority students whose academic skills will
be not be adequately developed by the modernist education
methods, The academic standards of the public school prod-
ucts have been falling since 1963; each year, test scores on the
Scholastic Aptitude Tests (SATs) drop lower. Guns are being
aimed at the schools.

To stem this bloodletting, the cities are desperate. As
white middle-class families depart, in order to escape forced
busing {forced racial and above all social integration), the tax
base of the large cities erodes. Parents who have their children
in private schools are no longer interested in voting in favor of
huge bond issues for the construction of more public schools.
Who cares if public school teachers are denied higher salaries?
Not the parents of students in private schools. The tax revolt
against large-city public schools is now a reality. The cities are
now in a downward spiral. The more liberal (“feed me, house
me, clothe me”) the voting patterns in the cities, the more the
taxpaying, employed citizens flee to the suburbs. They try to
escape from. the politics of envy.?!

To stop the spread of private schools, the bureaucrats are
creating an endless series of regulations that are designed to
raise the costs of private education and thereby reduce the

2. Gary North, Successful Investing in an Age of Enay (Ond ed: P.O. Box
8204, Ft. Worth, TX: Steadman Press, 1983),
