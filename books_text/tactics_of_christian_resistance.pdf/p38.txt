XXXVI CHRISTIANITY AND CIVILIZATION

We will start with the system because there is no other place to
start from except political lunacy. It is most important for those of
us who want revolutionary change to understand that revolution
must be preceded by reformation. To assume that a political
revolution can survive without a supporting base of popular
reformation is to ask for the impossible in politics.

Men don’t like to step abruptly out of the security of familiar
experience; they need a bridge to cross from their own experience
to a new way. A revolutionary organizer must shake up the
prevailing patterns of their lives—agitate, create disenchantment
and discontent with the current values, to produce, if not a pas-
sion. for change, at least a passive, affirmative, non-challenging
climate.

“The revolution was effected before the war commenced,”
John Adams wrote. “The Revolution was in the hearts and minds
of the people. . . . This radical change in the principles, opin-
ions, sentiments and affections of the people was the real
American Revolution.” A revolution without a prior reformation
would collapse or become a totalitarian tyranny.*

If a humanist like Alinsky understood this about the
nature of man and social change, we Christians should at
least give heed to his conclusions concerning tactics. Not
bombs but protests and petitions. Not guns but getting people
involved in dragging their feet. We need a positive program of
changing people’s minds about God, man, and law; about
family, church, and State, not to mention the economy.” We
also need a negative program of successful resistance techniques
that will get the State off our backs long enough for us to go
about the work of positive reformation. Meanwhile, we can
gum up the works. That literally happened under Alinsky.
Some Christian college was foolish enough to allow students
to invite him to speak on campus. A group of disgruntled
students met with him after his speech, “How can we change
this place? We can’t do anything. We can’t smoke, dance, go
to movies, or drink becr. About all we can do is chew gum.”
Alinsky told them, “Then gum is your answer.”

He told them to get 200 or 300 students to buy two packs
of gum each. Chew both packs simultaneously every day, and
then spit out the wads on campus walks. As-he said, “Why,
with five hundred wads of gum I could paralyze Chicago, stop

22. Saul Alinsky, Rules for Radicals: A Practical Primer for Realistic Radicals
(New York: Random House, 1972), pp. xxi-xxii.

23. Gary North, Unconditional Surrender: God's Program for Victory (2nd ed.;
Tyler, TX: Geneva Divinity School Press, 1983).
