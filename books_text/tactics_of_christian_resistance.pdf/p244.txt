198 GHRISTIANITY AND CIVILIZATION

parents and administrators couldn't be happier. Things were
so difficult then.

Some commentators have suggested that the tight job
market is responsible for the prevailing quiet. Possibly. But
revolutionaries didn’t care about job markets. They had no
intention of working for the “tascist corporate state.” So why
has it gotten so quiet? I suspect what has happened is not
really new. It seems that the people who start revolutions
seldom are the ones who finish them.

No, the job market didn’t extinguish the campus wars, nor
did the collapse of the Vietnamese war effort. The revolution
on campus was over because somebody won.

The victory for the New Left did not occur because their
arguments were cither true or compelling. Their victory was
assured because we fought the wrong battle. It began when
we joined forces with the Old Liberalism in what was essen-
tially a traditionalist battle.

As the new authoritarians began to impose their world-
view, and exclusively their world-view, on the academic com-
munity, we fought back. But instead of articulating an ade-
quate defense of our own world-view, we appealed to
“fairness.” We admitted the legitimacy of their position, asking
only that our own views be given “equal time.”

We opted for peaceful co-existence. And we lost.

The greatest failure was among those individuals and
groups which we loosely classify as “conservative.” American
conservatism had, by the late sixties, become intellectually
bankrupt. The only vitality left in che movement was among
Austrian school economists and the Chicago school monetar-
ists, Foolishly, conservatives made the same mistake Marx
did in assuming that all of life’s problems were essentially eco-
nomic, In so doing, conservatism became essentially irrele-
vant, attempting to answer problerns concerning free speech,
pornography, the draft, homosexual “rights,” drug use, au-
thority, etc. using economic arguments. We were prepared to
let the marketplace solve our moral dilemmas.

“Do what you want to do as long as you don’t hurt anybody”
went from the script of the counter-culture stageplay “HAIR,”
to become the guiding principle of the conservative move-
ment. To be painfully blunt, political conservatives didn’t
have any answers. [f it had not been for the paranoia and
foolishness which the New Left brought to us on such a grand
