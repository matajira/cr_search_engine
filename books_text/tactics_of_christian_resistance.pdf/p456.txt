440 CHRISTIANITY AND CIVILIZATION

been fighting with pistols and slingshots.

Men need motivation. The existence of the satellite net-
work offers men motivation. They can join together in a co-
ordinated effort to rol] back humanism at every level. This is
the approach I call brush-five wars.!° It can work well for legal
resistance, but it can also work for political action, education,
and almost everything else. It is a fundamental tool of
resistance. But it takes a combination of centralized strategy and
local mobilization and execution. It takes, in short, the application
of the One and the Many. principle.

The Fulcrum

Those who have been most successful in developing the
lever of satellite communications and cable TV distribution
have been skilled practitioners of media communications.
Understandably, there has been a tendency to emphasize the
impact of the media. But a medium needs a message; it is not
(contrary to McLuhan) itself the message. The message has,
until recently, been limited: personal salvation, personal heal-
ing, family solidarity, and musical entertainment.

Those who have been in the “fulcrum production business”
have been inept at developing levers. They have not written
widely read books, nor have they pioneered the use of TV
communications or motion pictures. The few exceptions: the
Schaeffers, the various Christian counscllors— Dobson,
Gothard, and Adams—and the six-day creationist movement
(including the old Moody science films), But with respect to
positive programs of Christian reconstruction, there has been
no successful program so far-~graphically or politically.

What is now needed is a bringing together of the lever and
the fulcrum. Those who have built up large audiences must
begin to join hands with those who have developed specific
programs of reconstruction: education, legal defense, political
training, etc. The “electronic churchmen” have got to begin to
target specific segments of their audiences who are ready for
specific programs; first by education, second by organiza-
tional mobilization. In short, we need feel, hands, and eyes, and
each subgroup within various large television audiences needs

10. See my essay elsewhere in this journal, “The Escalating Confronta-
tion with Bureaucracy.”
