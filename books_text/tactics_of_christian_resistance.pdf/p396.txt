350 CHRISTIANITY AND GIVILIZATION

with members under another bishop’s charge without com-
munication to all elders concerned. This keeps one bishop
from becoming too dominant. Otherwise, the wandering
minister might end up representing whole areas and becom-
ing a miniature pope. In fact, this is precisely what happened
at Rome. It was not so much due to the bishop's travel into
other areas. Constant appeal by others in the empire to the
bishop of Rome gave him more and more influence.

One sees the same concept in practice in the early republic
of the Roman Empire. When Caesar crossed the Rubicon,
the other Senators wanted to kill him.™ According to early
Roman law, a Senator of one area was not allowed to crass
the borders of adjacent territories with his armies. Reason:
the empire feared what Julius Caesar eventually accomplished
one man, a tyrant, controlling the empire.

As any church attempts to carry out discipline, it will en-
counter this problem. People simply leave and go to a
neighboring church, perhaps of the same denominational
affiliation. Walking out into another ecclesiastical jurisdiction
may seem to solve the immediate problem, but it rends the
church and creates a worse problem. It leaves differences
unreconciled.

Discipline is necessary to the being and well-being of the
church. In an essay of this size, only preliminary and in-
troductory considerations appear. Discipline, however, is not
the extent of the church court. The court can. also function
pastorally. It should be a place of reconciliation.

C, Reconciliation

Recently, the U.S. Supreme Court has reminded the
lower couris that reconciliation outside court should be en-
couraged, The overload is too much for the system. Tradi-
tionally, arbitration on just about anything except a felony, or
direct involvement with the government, is acceptable.

Arbitration is sorely needed in the church. Of all places,
man ought to find reconciliation before God’s throne. Often
one does not see this expressed in churches. Most churches do
not have a systematic procedure. Unlike the early church
whose elders met at the church on Saturdays to hear disputes,
people in the church do not know what to do, or where to go.

52. Michael Grant, The Tivelue Caesars (New York: Scribners, 1975), p.
36.
