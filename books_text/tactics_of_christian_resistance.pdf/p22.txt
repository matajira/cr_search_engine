xxii CHRISTIANITY AND CIVILIZATION

LR.S, begin to attack Christian schools once again, as it at-
tempted to do in August of 1978? If the ILR.S. is successful in
shutting down Christian schools with this ploy, can we then
expect attacks on “racially imbalanced” churches? (Perhaps
not: there are too many all-black churches that will create a
firestorm of protest if such nonsense is attempted.)

Let us consider another possibility: Aomosexuality. Is it
public policy, as affirmed by Congress and the courts, to for-
bid all discrimination based on sex? Even though the Equal
Rights Amendment failed? If so, then churches that excom-
municate (or refuse to admit to membership) homosexuals
and lesbians face the removal of their tax exemption. (If we
look at this from a narrowly economic standpoint, this will
tend to subsidize physicians who specialize in the treatment of
AIDS. Should they therefore be regulated by the LR.S., since
they will be the recipients of an implicit subsidy?) This exam-
ple is only one of hundreds I could conceivably enumerate.
The point is clear: the threat of the removal of tax exemption is a club
which the bureaucrats will learn to swing selectively. The question is:
What will be the response of the churches?

The necessary first response is clear: churches must insist
that they possess tax inununily, not tax exemption. The words
“tax exempt” transfer too much power to the civil government.
The State can begin to assess the churches’ stand on every
conceivable public policy issue, with the courts or Congress
determining what is acceptable public policy this week. The
churches are in a unique category: total immunity from any
taxation by any agency of civil government. Church boards
must defend the total immunity from taxation; then they must
fight, case by case, to maintain this sovereignty. If they
capitulate to any civil government on any tax, they have ad-
mitted their liability to taxes in general.

When the church buys something, if must not pay the sales
tax. For cash, the seller ought to provide a discount. In any
case, the seller owes the tax, nol the church. Lf necessary, agree in
advance to pay more than retail, so that the seller implicitly
collects the tax and pays it, but under no circumstances should any
tax subiotal appear in any bill presented to a church. Uf the seller
wants to do business with a church, he will have to learn not
to put tax subtotals on bills submitted to the church.

Tf a bill must be paid to a public utility, and some tax is
mentioned on the computerized bill, the church should pay
