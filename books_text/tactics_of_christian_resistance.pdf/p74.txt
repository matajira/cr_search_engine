28 CHRISTIANITY AND CIVILIZATION

the word of God, and constitutions of this kirk, and that many
other sad and fearful consequences have followed unto the pro-
faning of all the ordinances of God, and rendering them for the
most part barren and fruitless to us.

Art, 5. The base love of the world, and covetousness, which
hath made not only the body of the people, buc many ministers,
more to mind their own things than the things of Jesus Christ;
and many masters, rulers, magistrates, officers and soldiers in ar-
mies, exceedingly to abuse their power unto the exercising of in-
tolerable oppression of all kinds on the poor, to the grinding of
their faces, and making their lives bitter to them; which fountain
of covetousness did also produce great insolences and oppressions
jn our armies in England and Ireland, and the fearful perjuries in
the land in the matter of valuation and excise.

Of particular interest are these excerpts from Article 9
condemning a latent hypocrisy and a bitter and railing spirit
under these trials.

The rejecting of discoveries of guiltiness, and causes of the
Lord’s contending with us, and of our duty in reference
thereto; . . . neglecting the means tending to peace, and to the
preventing the effusion of more blood, from pride and bitterness
of spirit against those who had invaded us.

And finally, this last article:

Deep security, impenitency, obstinacy and incorrigibleness,
under all these, and under all the dreadful strokes of God, and
tokens of his indignation against us, because of the same; so that
whilst he continues to smite, we are so far from humbling our-
selves and turning to him, that we wax worse and worse, and sin
more and more.

These articles were followed by thirty some pages of fine
print cataloging in detail the sins under these ten heads and
thoroughly setting forth the testimony of the word of God with
respect to each sin. What follows is really amazing, for there is
then appended a lengthy confession of their own sins entitled,
“A Humble Acknowledgment of the Sins of the Ministry of
Scotland.” The preface reads as follows:

Although we are not ignorant that mockers of all sorts may
take occasion by this acknowledgment of the sins of ministers to
strengthen themselves in their prejudices at our persons and call-
ings, and turn this unto our reproach, and that some may
misconstrue our meaning therein, as if we did thereby intend to
render the ministry of this church base and contemptible, which
is far from our thoughts, we knowing and being persuaded in
