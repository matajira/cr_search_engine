406 CHRISTIANITY AND CIVILIZATION

What is the Christian approach to the problem of the One
and the Many? We must begin with the doctrine of the Trinity. God
is three and He is one. He is both unity and diversity. Neither
unity nor diversity is paramount in the created realm. Neither
unity nor diversity is to be divinized. Every institution is to
reveal both unity and diversity. The guide to the proper
balance or mixture of unity and diversity is found in biblical
law.

As we seek to reconstruct the humanist culture of our day,
bringing it back to Jesus Christ, we must honor the principle
of unity and diversity in the construction of alternative Christian
institutions. These reconstructed institutions are eventually to
replace the humanist institutions of our era. Rather than an
appeal to armed revolution, American Christians initially
need to use the existing legal order—which was originally
Christian in origin — to recapfure these institutions or else (e.g.,
tax-supported education) abolish them.

This was Horace Mann’s strategy when he created the first
state school system in Massachusetts in the early 1830s. He
wanted to replace sectarian Christianity with a universal in-
stitution, the public school, which was to be organized in
terms of the supposedly universal principles of natural law.”
“Neutral” natural law was to replace biblical morality as the
standard of civil justice.

Humanists in the United States since the early 1800s have
sought to capture those offices that bear authority in the West:
the judges, the pastors, and the teachers. The mark of authority
of these offices is the right of the office-holder to wear a black
robe.® The humanists have been remarkably successful in
their strategy. They have completely fooled Christians into
believing that the authority of the “robes” has nothing to do
with the biblical principles that originally undergirded these
God-ordained offices. The humanists have clothed their own
anti-God principles with the robes that they stole from the
Christians, and the Christians have been so hypnotized by the
sight of the robes that they have not fought to return the robes
to the rightful owners, namely, those who profess faith in, and
conform their authoritative judgments to, the law of God.

7. R. J. Rushdoony, The Messianic Character of American Education (Nutley,
NJ: Craig Press, 1963), ch. 3.

8, Gary North, “Capturing the Robes,” Christian Reconstruction, V1
(Sepi./Oct., 1982). Published by the Institute for Christian Economics.
