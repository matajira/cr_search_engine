EDITOR’S INTRODUCTION xli

for so long. He offered him the opportunity to become a new
Abraham, a new father of multitudes—a great gift in the an-
cient world. But Moses turned down this generous offer. He
did not seek revenge. Instead, he pleaded for the lives of the
Israelites.

He did not deny the obvious, namely, that they were a
stiffnecked bunch, He did not appeal to God on their behalf in
terms of what God owed them for their righteousness; they
had no righteousness. But he did appeal to God on their
behalf in terms of what God did owe them because of His
promise to Abraham, God had said that they would inherit
the land in the fourth generation after they went down into
Egypt (Gen. 15:16). The time had come for God to fulfill His
word. The honor of God was at stake.

The parallel passage in Numbers 14 is even more explicit
concerning the honor of God:

And Moses said unio the Lorp, Then the Egyptians shall
hear it (for thou broughtest up this people in thy might from
among them); and they will tell it to the inhabitants of this land:
for they have heard that thou Logo are seen face to face, and that
thy cloud standeth over them, and that thou goest before them,
by day time in a pillar of a cloud, and in a pillar of fire by night.
Now if thou shalt kill all this people as one man, then the nations
which have heard the fame of thee will speak, saying, Because the
Lorn was not able to bring this people into the land which he
sware unto them, therefore hath he slain them in the wilderness

(vy, 13-16),

Then Moses pleaded for mercy. But he made il clear that
this mercy would be offered by God in order to uphold the
honor of His own name. If He destroyed His people after hav-
ing delivered them from the Egyptians, then His reputation
would be in jeopardy. He was stronger than the Egyptians’
gods, but not stronger than the gods of Canaan. The Egyp-
tians would tell the enemies of God in the promised land of
the weakness of the God of Israel. They would tell of a God
who slays His own people because of His own inability to
deliver His enemies into their hand.

This is the most effective prayer available in any program
of Christian resistance, It still works as well as it did in Moses’
day. If His people challenge rival gods in His name, and are
ready to sacrifice everything they own for the cause of God,
then God can be called upon to uphold His name. The ethical
