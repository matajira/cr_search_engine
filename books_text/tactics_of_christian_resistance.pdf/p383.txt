THE CHURCH AS A SHADOW GOVERNMENT 337

God. Rehoboam trusted in his cities, and found that the
pagan could match power with power on that level. In the
end, he deserted the Law of Gad.

Keys of the Kingdom

Perhaps one can now comprehend why Christ spoke of
Hell as the city. Since the fall of man, the city was a place of
initiation, manipulation, oppression, and seduction. With the
coming of Christ, however, that changed. He gave the Keys of
the Kingdom with which the church would pull down the
gates of hell.

Before we proceed to the Keys of the Kingdom, some gen-
eral observations should be made about the city. They will
help to direct our attention to specific tactics for implementing
the Keys.

I. The City: A Weakness, Need, and Strategy

First, as to an all encompassing weakness, the city is
parasitic. It is basically dependent on outside resources to sur-
vive. The city absolutely cannot live by itself. “And this,
moreover, characterizes all of those works of man by which he
seeks autonomy. Everything takes its life from somewhere
else, sucks it up. Like a vampire, it preys on the true living
creation, alive in its connection with the creator. The city is
dead, made of dead things for dead people. She can herself
neither produce nor maintain anything whatever. Anything
living must come from the outside. In the case of food, this is
clear. But in the case of man also, we cannot repeat too often
that the city is an enormous maneater. She does not renew
herself from within, but by a constant supply of fresh blood
from outside.” Thus, we can say that the city needs the
theology, law, and economics of the church to survive.

Second, as to a basic need of the city, during the middle
ages the church gave something to the city that could nat be
found elsewhere—a program for law and order. Cities were
not only created by the spread of Christianity as the Law of
God established reliable centers of commerce, but it trans-
formed them from military institutions. The characteristic
clement was not the wall, “It was its charter. The commercial

33. Ibid., p. 150.
