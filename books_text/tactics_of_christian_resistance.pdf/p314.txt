268 CHRISTIANITY AND CIVILIZATION

tected under the Fourth Amendment guarantee against un-
reasonable searches and seizures. The only effective way to
deny the IRS access to these records is to not have them.

2) DEAL AS MUCH AS POSSIBLE IN CASH. The
immediate response of the new paralegal litigant when these
first two points are raised is to complain of the inconvenience
of a life without checks. A most peculiar argument! How
many hours do people spend during the year balancing the
check book? How much does the bank charge peaple annually
to deposit their money in their bank and maintain a checking
account? It doesn’t cost that much to purchase money orders
for those payments that must be made by mail, and there are
ways to recycle out-of-town checks. The inconvenience is in
making the change, not in living without checks. Remember,
checks can be traced. . . cash can’t.

If you wish to test my hypothesis concerning checks and
the information they reveal, pull out of your file one month’s
worth of cancelled checks. Try to assume that you don’t know
anything about yourself, and then thumb through the checks
one by one. What can you find out about yourself from your
cancelled checks? Where you attend church? The name ol
your mortgage company? The name of companies with which
you do business? Personal characteristics (did you buy a case
of shotgun shells last month)? Don’t fool yourself, the IRS is
expert in using bank records. They will find things you never
dreamed of from bank records. Most important, however, i:
the fact that they will find new leads for more information from
the bank records.

The bottom line, if you are going to assume a paralega
litigant position, is get out of banking and deal primarily ir
cash. Even if you are not willing to become a paralega
litgant, it is still a good idea to leave as little “paper” behine
you as possible. Cash transactions have many advantage
regardless of the direction you choose.

Dealing With The IRS

If you have ever been called in for an “audit,” then yo
have had some experience in dealing with an administrativ
agency. When you assume a paralegal litigant pasition, how
ever, you will get Jots of experience in administrative prc
cedures! The observations below will help you in preparir
