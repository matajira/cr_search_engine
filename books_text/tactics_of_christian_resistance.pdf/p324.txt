278 CHRISTIANITY AND CIVILIZATION

The exercise of liberty and enforcing of the Constitution ir
what resistance to the IRS is all about. The last fifty years ha:
seen an enormous growth of civil government and accompa
nying governmental abuses. If this generation does not take
the initiative to roll back our gargantuan civil government
the liberties which our Founding Fathers fought and died tc
secure will be lost. The oppressor has the upper hand at thi:
point. Inactivity on the part of “We, the People” will only
strengthen the government’s unlawful position.

Fighting the Federal Government is like opposing ar
enemy tank coming down the street. It does no good to stanc
out in the middle of the street and scream, “Stop! What you
are doing is unconstitutional!” If you stand out in the middle
of the street, the tank will simply run over you, or shoot it
cannon and biow up your house, killing you and your family
We must be willing offensively to disable the tank. We must take
our litigational bazookas and maneuver ourselves to get z
clear shot of the tank’s side. Then we must be willing to senc
an accurate shot into the vulnerable section of the tank:
tread, thus disabling the tank.

One may do battle with the civil government and end uy
with “tank treads” on his back. Eventually, however, the “tank’
of civil government will be disabled. The liberties given to u:
by God will once again be secure from tyrannical usurpation
Individual resistance to unjust taxation is merely onc
“guerrilla” tactic mobilized against government. It is not for
everyone, but it is a legitimate form of Christian resistance tc
tyranny when approached properly. If you wish to join in thi:
fight, please refer to Appendix D for more information or
how to get started.

On the other hand, ecclesiastical resistance to the IRS i:
not an option. The Church must never be compromised. The en
croachments of the LRS on behalf of the messianic State must be resiste
at all costs. Anything less is faithlessness.

APPENDIX A: Is the Tax Movement Sinful?

After outlining in detail the rationale for individua
resistance to the IRS, it is now important to comment briefly
on the sentiment in orthodox Christian circles that thi:
strategy is in some way sinful, or at the very least not an activ
ity that a believer should be involved in. As I said, my com
