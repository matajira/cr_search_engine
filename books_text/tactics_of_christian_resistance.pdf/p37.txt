3 i
EDITOR'S INTRODUCTION XXKVIL

toes this late in the twentieth century? Haven’t the humanists
been doing far worse things to Christians and non-Christians
for decades? Why revolt now? Why begin to adopt the lan-
guage of apocalyptic romanticism at this late date? Are we in
fact not viewing a kind of spiritual adolescent's rebellion—the
temper tantrum of the formerly uninvolved, socially uncommit-
ted, and economically pampered youth who finds that the world
no longer conforms to his will through his parents’ sacrifices
for him? Are we not viewing a revolt against maturity?

Before we can even think about ruling a nation we must
learn how to exist under the present order and still accomplish
the bulk of our goals. Like the Israelites under the Babylo-
nians and Assyrians, we must learn how evil the gods of our
conquerors are—and how evil their law-orders are—before
we can call upon the name of the Lord and expect deliver-
ance. Like the church under the Roman Empire, we roust be
subordinate before we can lead. We are in a wilderness; we
need to bring ourselves under the dominion of the law of God
in our local relationships before we can expect to be put in
seats of power nationally.

Alinsky’s Tactics

Saul Alinsky was one of the really effective humanistic
radicals of the 1960s. He did not throw bombs. He did not
spout rhetoric. He simply taught people how to use the bu-
reaucracies’ own red tape to tie them up in knots. This is why
he was so effective, and why his book, Rules for Radicals (4972),
is so useful for an understanding of the principles of successful
resistance, despite its humanistic bias. His words are worth
considering:

Let us in the name of radical pragmatism not forget that in
our system with all its repressions we can still speak out and de-
nounce the administration, attack its policies, work to build an
opposition political base. True, there is still government harass-
ment, but there still is that relative freedom to fight. I can attack
my government, try to organize to change it. That’s more than I
can do in Moscow, Peking, or Havana, Remember the reaction
of the Red Guard to the “cultural revolution” and the fate of the
Chinese college students. Just a few of the violent episodes of
bombings or a courtroom shootout that we have experienced here
would have resulted in a sweeping purge and mass executions in
Russia, China, or Cuba. Let us keep some perspective.
