260 CHRISTIANITY AND CIVILIZATION

 

“gross income,” “taxable income,” “wages,” “employee,”
“employment,” “employer,” “self-employment,” “exempt,”
“allowance,” “money,” “dollars,” “payment,” ete. Examples of
broper factual terms used in federal tax matters arc: work,
labor, property, sale, gift, exchange, receive, receipt, spend,
expense, cost, purchase, check, paycheck, negotiable instru-
ment, tender, Creator, cic. All of the “legal-fiction” terms must
be carefully considered by the paralegal litigant before using
any of them, especially in reference to himself. The law is very
wicky at this point, and extreme caution is recommended.

‘Tam not a Person Liable for Income Tax”

We are now ready for the major argument in defense of
the contention that most individuals are not required to pay
“income tax,” and that the present “income tax” system is un-
constitutional and should be outlawed.

Around the first of every year, the Internal Revenue Ser-
vice mails out its annual “1040 package,” which includes sam-
ple forms and an instruction booklet. Most Americans view
this “service” as simply a component part of the IRS's collec-
tion process. In reality, the IRS is “requiring” individuals
unknowingly to volunéeer information about themselves,
especially concerning their personal and business financial
matters. The place then to begin in challenging the IRS is
with this request for information.

The first thing that one should consider when any federal
agency asks for information is an appeal to the Privacy Act of
1974. Even if you are not involved in the paralegal litigation
movement, it is extremely beneficial to be familiar with the
opportunities open to you under the Privacy Act, and ways in
which you may use it. For more information on this prelimin-
ary strategy, see Appendix C.

For the sake of argument, let us assume that you have de-~
cided to become involved in the paralegal litigation move-
ment. Your first appeal should be to the Privacy Act before us-
ing the arguments below. One extremely crucial ally that all
paralegal litigants must learn to use is “Lawyer Delay.” He
needs to be employed as frequently as possible.*?

Included in the 1040 instruction booklet every year is a

50. O:

winnin,

 

¢ of my personal mottos in this fight is, “If! haven't lost, then Fm
“Lawyer Delay” can be of great help if he is used properly.

 
