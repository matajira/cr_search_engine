422 CHRISTIANITY AND CIVILIZATION

should become the first line of defense for Christians in the
war against the humanist State.

I can see a market for training tapes and manuals among
smal] businessmen whose enterprises are threatened by bu-
reaucratic intimidation. They simply don’t know where to
start when they are hit by an official demand for compliance
—a demand which may well be unconstitutional. What if
every local church had videotaped training programs to assist
small businessmen, if only to delay a crisis until the right law-
yer could be located?

It is time to fight back, at every level, with every tool available. A
Christian legal defense program could increase the cost to the
State of infringing on our Constitutional liberties. It could
also lower our costs of defense, especially initial costs. If
Christians think they cannot afford to fight, many of them will
quit.

Hornets and Revival

Christians pray for revival, It is doubtful that they are
ready for revival. Consider the implications of a revival today.
The myth of neutrality has faded, not only in Christian circles
but also on the campus. The work of Thomas Kuhn in the
physical sciences and the work of Karl Marx and others in the
social sciences have destroyed men’s faith in neutral human
reason, The educated man’s faith in natural law has been
destroyed by the Darwinian revolution.}

We now face a crucial problem. If there were a great out-
pouring of Christian faith tomorrow, what would be the an-
swer of the Christian world to the inevitable question of the
newly converted: “How should we then live?” To say, as most
Christians do, that “the Bible has the answer to every ques-
tion,” implies that Christians have studied the Bible and have
asked a lot of questions which the Bible has answered. Is this
the case? Have Christians actually gone to their Bibles in
search of answers to life’s questions? “How should we then
live?” is a broader question than “How should I treat my

wife?” or “Should I stop being a prostitute?” or even “Is the re-

quired mode of baptism by immersion only?” Do we have a

 

15. Gary North, The Dominion Covenant: Genesis (Tyler, TX: Institute for
Christian Economics, 1982), pp. 24f., 300, 4094.
