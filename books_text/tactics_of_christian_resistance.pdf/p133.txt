THE NEW COMMUNITY 87

more ones of withdrawal than of confrontation. John Howard
Yoder, one of the more eloquent defenders of these positions,
urges Christians to consider that they are not the guardians of
history and thus should refrain from grasping for the levers by
which they hope to move society in the desired direction.’

In some respects this disinclination to change society mir-
rors the early monastic movement, which recoiled in horror
from the excesses of a disintegrating empire and resolved to
remain unspotted. Many of the followers of Karl Barth, those
who are determined to view temporal events sub speciae aeter-
nitatis, have found themselves unable to make distinctions be-
tween competing claims for allegiance. Bemused by the call
for putting spiritual values first, they are unable to move
decisively in the struggles that mark modern life. This is
another version of the neo-Kantian dualism, helpless to
reconcile the worlds of matter and of spirit. It was no wonder
that a number of Lutheran bishops supported Hitler in 1934;
they had difficulty seeing how Christian faith could inform
material action. Reinhold Niebuhr never tired of pointing out
this persistent weakness in Barthian thinking.

Christians who resist acknowledging any close corre-
spondence between their faith and the direction that history
takes strangely echo the postion taken by the reigning human-
ist establishment. As Richard Neuhaus has pointed out, their
stand is precisely that of the modern secularists who wish to
banish Christian ideas from influencing public policy. This
understanding of Christian action aids its enemies by reinfor-
cing the notion of the supposed irrelevance of Christian faith.

Biblical teaching, in contrast, insists that faith and works
are inseparable, that the interior dimension, if it is not a
sham, must have its effect on the external world, The “salt” of
people changed by the gospel must change the world. In the
prophetic tradition, turning away from false gods had to be
accompanied by a resurgence in the doing of justice, At its
better moments, the church has made enormous differences in
the way society functioned. In the midst of Hitler’s program to
kill the incurably ill in 1941, Bishop Galen published a sermon
that exposed the practice and caused widespread revulsion
throughout Germany. Galen was spared only because propa-

5. John Howard Yoder, The Politics of Jesus (Grand Rapids, MI: Eerd-
mans, 1972), pp. 234-38.
