298 CHRISTIANITY AND CIVILIZATION

Sample Privacy Act Request to Amend Records

Date

Name and Address of Government Agency
Washington, D.C. Zip Cade

RE: Privacy Act Request to Amend Records

Dear (agency head or Privacy Act officer):

By letter dated __...-» «ss T requested access to
(age the same description as in the original letter).

In reviewing the information gent to me, I found that it
was Unaccurate) (incomplete) (outdated) (not relevant to the
purpose of your agency).

Therefore, pursuant to the Privacy Act of 1974, 5 U.8.c.
582s, I hereby request that you amend my record in the
following manner: (Describe errors, irrelevance, etc.).

In accordance with the Act, I look forward to an
acknowledgement of this request within 10 working days of
its receipt. If you wish to discuss this matter, my telephone
number is (890) 128-4567,

Thanking you in advance for your time and
sonsideration, Iam,

Sincerely,

Signature
Name

Address

City, State, Zip

Tf you have trouble determining the location of any per-
sonal records, go to a law library and consult the Federal
Registers compilation of Privacy Act notices. It contains (a)
descriptions of all Federal record systems, (b) descriptions of
the kinds of data covered by the systems, (c) categories of in-
dividuals to whom the information pertains, (d) procedures to
follow at the different agencies, and (e) specification of the
agency official to whom you should write.

As important as knowing what information the govern-
ment presently has about you, is the prevention of being forced
to give the government any more information. The Privacy
Act, you will remember, places restrictions upon how the govern-
