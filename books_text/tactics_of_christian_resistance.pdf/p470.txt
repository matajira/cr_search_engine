424 CHRISTIANITY AND CIVILIZATION

replace the Canaanites overnight, however evil the Canaan-
ites were. “And [ will send hornets before thee, which shall
drive out the Hivite, the Ganaanite, and the Hittite, from be-
fore thee. I will not drive them out from before thee in one
year; lest the land become desolate, and the beast of the field
multiply against thee. By little and little I will drive them out
from before thee, until thou be increased, and inherit the
land” (Ex. 23:28-30), Deliverance demands dominion.

We are no different in our struggle to take the land. We are
unprepared to occupy the land overnight. The world must roll
along during the period of transition.. If revival comes over-
night, how will we reconstruct the institutions of our era? Will
we tell men to continue as they did before conversion? Then’
we are not preaching comprehensive repentance from com-
prehensive sin. If we tell them that they are new creatures,
but that they need not change the daily routines of their lives,
including their work’s goals and routines, then we are not
really saying, “Behold, ail things have become new.” If we
stand mute and tell them nothing, then we are not preaching
at all. Preaching the gospel means preaching total reconstruction.
Nothing lies outside the judgment of God.

Nevertheless, we must expect to reconstruct “Canaan”
overnight. We must begin to prepare Christians to begin to
take the reigns of power, at every level, in every institution,
across the face of the earth, before we call on God to bring an
overnight revival. What we need is revival with hornets. We
need a steady spread of the gospel, meaning an expansion of
the rule of Christ’s people. We need on-the-job training. Let the
hornets of our age—herpes, drought, war, depression, infla-
tion, doubt, fear, and consternation— confound the Canaan-
ites and drive them out of the seats of power. Let us recapture the
robes. But it will not happen in a year.

We should not expect revival overnight. If some wave of
emotional outpourings should come before Christians are
ready to lead, then either it is a false revival, or else we will be
hard-pressed to give direction to the new converts. Let us not
pray for overnight revival unless we are ready lo offer comprehensive
counsel overnight, Let us strive to raise up Christians who will be
ready to offer counsel overnight, in every area of life, at every
level of leadership. If we are not invelued in a program of raising up
such comprehensive counsellors, then God will not take seriously our call
for overnight revival.
