282 CHRISTIANITY AND CIVILIZATION

James B. Jordan. In an article published in Bidiical Economics
Today (Vol. IV, No. 2, April/May 1981) titled “The Christian
and Tax Strikes: Pros and Cons,” Mr. Jordan offers some
“principles which will help the reader as he or she comes to
grips with the tax rebellion of the 1980s.” He goes on to point
out that the basic thrust of the essay is that “the Bible clearly
teaches that Christians are to submit to the powers that be
and pay whatever taxes are required of them; but citizens of
the United States of America may properly raise the question
of just precisely what is required of them, and in raising this
question may work for reform.”

The frst main argument is that the “Tax Strike” is il-
legitimate because it seeks spiritual reform through political
means. Mr. Jordan argues that “Tax strikes as a Kingdom
method are contrary to the teaching of Jesus . . . and Paul.”
He points out that true liberation has to do fundamentally with
right worship. When Israel was liberated from Egypt, it was
not a political liberation. God insisted that Israel be released
in order that they might worship Him. When a culture
recognizes who the true God is, and worships Him rightly,
then there follows as a result righteous government, lower
taxes, and a regular Sabbath rest. Mr. Jordan argues that
these resulis are not the means to such liberation. He then
asserts that “the tax strike and ‘liberation theology’ obscure
this fact.”

First of all, I wholeheartedly agree that the worship of the
true God is the fundamental element in any cultural reform.
It is my personal belief that the “Theonomic” or “Christian
Reconstruction” movement has not been more successful pre~
cisely because it has neglected the areas of ecclesiology and
liturgy. Be that as it may, this criticism is not entirely ac-
curate.

Linking “the tax strike” and “liberation theology” may be
valid in some cases, but we believe that it does not apply to
the paralegal tax litigation movement. The theology of libera-
tion believes in the regeneration of a culture through political
reforms. The paralegal litigation movement is simply
endeavoring to see laws observed that are already on the
books. Its advocates do not believe in political regeneration of
any sort. The paralegal litigation movement is, to be sure, a
