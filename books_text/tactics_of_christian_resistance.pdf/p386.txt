340 CHRISTIANITY AND CIVILIZATION

the announcing of the Year of Jubilee. Christ applies the Year
of Jubilee eschatologically to the entire New Covenant Age
(Lk. 4:16ff.). The trumpeter of the Old Testament becomes
the herald of the New. This connection between blowing the
trumpet and proclaiming the gospel, which the Bible makes,
clarifies how the judgments of Revelation come. The heraiding
of the Gospel draws the Lord near the earth. When He comes, those
who do not know Him are consumed. This phenomenon hap-
pens time and again in history.

After Elijah defeated the prophets of Baal, God drove him
into a cave (I Kgs. 19:9). God passed by, but he was not
harmed. Elijah was hidden in the cleft of the Rock. The
significance of this unusual event in the midst of victory un-
folds as we understand the relationship of the coming of the
Gospel and judgment. Elijah had just been victorious. Con-
ceivably, a revival had taken place. The false gods were
defeated. But this scene shows Elijah that the consuming fire
in the glory/cloud around God would have burned him also, if
it were not for the place of salvation which the true God pro-
vided. Moreover, we see that the coming of the Lord brings
salvation to those in the cleft, but the destruction to all others.

In extra-Biblical history this is confirmed. Each resurgence of
Christianity is accompanied by cataclysmic judgment. One example
will suffice. The infamous 14th century marked a period of
wars, farnine, and disease (the Black Death).*? Yet the world
was also entering a Renaissance because the Lord was draw-
ing near in the heralding of the Gospel. It was the beginning
of the pre-Reformation with the ministries of Wycliffe and
Hus. Therefore, the Gospel must not be separated from judg-
ment.

The Gospel tells of the judgment which came on Christ.
But the sound of this trumpet speaks of judgment which wll
come on the one who does not trust in Christ’s death for salva-
tion. Our study of the trumpet/ram’s horn explains.

The ram’s horn was also blown whenever the people were
drawn near to the Lord for judgment. In Exodus the sound of
the trumpet brought the nation to the mountain to reccive

42. Barbara W, Tuchman, A Disiant Mirror (New York: Alfred Knopf,
1978), pp-5328. Tuchman, a master historian, captures the significance of
the calamitous 14th century. Interestingly, she compares it ta the 20th
century — believing this century holds the same potential for catastrophe and
conquest.
