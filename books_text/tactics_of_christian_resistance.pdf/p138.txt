92 CHRISTIANITY AND CIVILIZATION

passivity displayed by the Indians is properly a trait of pan-
theism, not of Christianity.

There is an almost infinite number of ways one can stand
against the powers. We are not limited to either acquiescence
or law-breaking, Once we reject passivity, we can consider
how to carry on the struggle. There are speeches, demonstra-
tions, petitions, withholding of services, letter-writing, mar-
ches, economic boycott, selective disobedience, refusal to
serve the state, ignoring of government directives, stalling
and obstruction, overloading the administrative system with
excessive compliance, and so on.? Should ihe system worsen,
of course, there is always the possibility of making oneself
vulnerable to prosecution.

Prior to sabotaging the establishment, however, we should
consider how to change its course. Perhaps we could turn the
powers away from idolatry and toward the establishment of
the rale of justice. Proclaiming the gospel is fundamental to
this, As the idolatries almost universally recognize, changing
society without changing people is futile. The church’s
teaching function has to include a more biblical understand-
ing of society if it is to influence the provision of justice.

In our effort to do this, is it possible to pursue wholeheart-
edly the program of one party or movement while recognizing
its contingent nature? Not if “contingent” is understood to
mean the opposite of “absolute.” Inasmuch as these move-
ments are driven by ideological forces that are to some degree
in conflict with Christian faith, we are able to accord them at
best only partial support. Our loyalty will always be suspect
among those groups, and rightly so, for we are ready to
change from support to opposition as soon as they depart from
some approximation of justice by biblical standards. The
danger in becoming “Christians for X,” as Richard Neuhaus
has well said, is that of becoming mere appendages to
“Americans for X.” This can only encourage millions of Chris-
tians, wary of being used by hostile forces, to turn away from
their responsibility to work toward creating a just society.

The rival movements provide no help in understanding
contemporary events. The common labels are as worthless

9. For the theory of nonviolent resistance, and scores of concrete
strategies, see Gene Sharp, The Politics of Non-Vialeat Action (Boston: Porter
Sargent, 1973).
