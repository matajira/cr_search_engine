TERMITE TACTICS; OR HOW TO
BE A 98-POUND SAMSON

Wayne C, Johnson

N 1969, the liberal intelligentsia were sharply divided be-

tween the Old Left and the New Left. At most campuses,
the faculties were divided between these two warring factions,
struggling for supremacy. At Purdue University, things were
different.

Purdue missed the revolution. Situated amidst the corn-
fields of Northern Indiana (and with Indiana University
siphoning off most of the sociology majors and artsy-craftsy
types}, Purdue University was the eye of the storm, alma
mater of the astronauts, home of science, technology, and
agriculture, where people learned how to make and do things.
Wisconsin, Columbia, and Kent State had the Old Left and
the New Left. Purdue had the What’s Left.

The What’s Left were a pretty sad bunch, caught in the
Twilight Zone of a campus where the students were stub-
bornly more conservative than their professors. War, for in-
stance, was not an issue at Purdue. It was a science. The pro-
fessor foolish enough to bring the Vietnam issue before his
class saw it immediately wrenched from his grasp as the class
chose up sides. On the one hand, were the Navy ROTC
cadets arguing passionately for sea power. The astrophysi-
cists, who today are actually developing it, were hinting at the
coming “high frontier.”

Dumbfounded, the What's Left professor would sit back help-
lessly as the nuclear engineers joined the fracas, pointing out the
efficacy of strategic nuclear weapons. And then would come the
slide rules. Hundreds of slide rules. Thousands of slide rules.

This was 1969 and Hewlett-Packard was just launching the
mini-calculator revolution. For thousands of engineering
students, the problems of life were still solved by the furrowed
brow and hunched shoulders, firgers working feverishly at the
white plastic slide rule. Within minutes of bringing up the

191
