COMPUTER GUERRILLAS 213

Perhaps even more important, he thinks, was the 1980 federal
election. The computerized campaigns some conservative
groups used to target candidates for victory or defeat impressed
many politically active people. “It brought home the power of
the computer as a tool,” Johnson says.

Another factor in this growing use of computers is the
parallel phenomenon of computer professionals shedding
their apolitical pasts and becoming involved in social ac-
tivism. Laurie Foster, the spark plug of Peace Net, is a good
example. A professional developer of minicomputer systems
software, Foster says she was galvanized into action by a
speech given by Helen Caldicott of Physicians for Social
Responsibility. (Caldicott is the Australian physician who has
been protesting nuclear bomb tests in the Pacific Ocean.)
Foster donated her services to get Peace Net organized. She’s
drawn up an elaborate analysis of the Net’s software and
hardware needs and is accumulating whatever she can beg,
borrow, or steal. “I don’t have a history of activism, so when [
get involved, you know people are really coming out of the
woodwork,” she says.

Computer engineer Lee Felsenstein, designer of the
Osborne 1 portable computer, is another active professional.
Felsenstein is one of a number of hardware and software
engineers belonging to a Berkeley, California, group called
the Community Memory Project. To give computing power
to people who wouldn't normally have access to computers,
Community Memory is developing a system of free computer
terminals to be placed in bookstores, laundromats, and other
public locations. The project is being financed in part by
Pacific Software, a commercial venture that has developed
and is now marketing a relational database called Sequitur for
mini- and microcomputers.

So far microcomputer manufacturers haven't gone out of
their way to encourage the kind of computer activism that has
a political edge to it, but this too may be changing. Through
its public affairs department, Apple Computer Inc. has begun
to donate equipment for computer communications networks
to various worthy groups, including some that are promoting
social change. In the first year Apple hopes to seed 20 to 25 of
these networks with three to five computers each, according to
Mark Vermilion, who oversees the project.

What constitutes a worthy group? Apple starts with the
