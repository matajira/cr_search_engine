382 CHRISTIANITY AND GIVILIZATION

their duty to organize a Protestant regime over the objections
of the Catholic Queen Regent. As part of his campaign, Knox
also issued his famous and disturbing Blasts of the Trumpet
against what he termed A Monstrous Regiment of Women, in
which Catherine d’Medici, Bloody Mary, and the Queen
Regent of Scotland served as horrid examples of ail the
frailties and follies of the female sex, which Knox regarded as
unworthy of rule over males or kingdoms.*°

Knox's books, which created a sensation, were printed in
Geneva. Some of them embarrassed Calvin, who maintained
a close and friendly correspondence with the Duchess of Fer-
ara, an ardent Calvinist and daughter of King Louis XI,
but Calvin was not the autocrat of Geneva that history pic-
tures. He had no real control over the printers, who like their
breed to this day, would print whoever and whatever would
bring in a profit, irrespective of viewpoint, tone, or truth.

If Knox embarrassed in some minor ways, however, he
was a brilliant success in many others. He landed in Scotland,
and everywhere he toured he left Calvinist crowds that
attacked the old Roman Catholic churches and rose against
the old regime. By 1559 Scotland was the first completely
Calvinist nation in the world; a tremendous triumph both in-
ternally and externally; one that changed the mind of the
British Isles forever, and through Britain, the world.

The loss of Scotland was a diplomatic blow to France that
was accompanied by the rise of Calvinist influence within
France itself, Unlike the situation in Scotland, where Knox
had successfully enlisted the majority of the Scots nobility, the
Calvinists had enlisted only part of the French nobility. They
were opposed most effectively by the powerful Guise dynasty,
acting through both the Duc and the Cardinal of Lorraine.
The Calvinists of France, therefore, had to walk softly.

The Calvinist Underground in France

Each member of a congregation usually had to swear an
oath of secrecy regarding the identities of his fellow-worshipers.
In several instances, and even at one Synod (also secret),
some conversation was devoted to the difficulties such an oath

20. This work appeared in 1558, and created more of a stir than any
single book since Luther’s Theses.
