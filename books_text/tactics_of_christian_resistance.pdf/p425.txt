TACTICS USED BY REFORMERS 379

bourgeoise. The intellectual nature of Protestantism and its
reliance upon literacy and reasoning militated against can-
didates from the lower classes in its earlier stages. The city of
Geneva did not, of course, consist entirely of candidates for
the Calvinist church. The majority of French refugees who
flocked into the city were merchants or artisans or displaced
nobles whose livelihoods were not involved with preaching,
These individuals retained, like refugees around the world, a
keen interest in developments in their native land. Many
came and went on silent journeys, and it would be naive to
assume that either Calvin or his associates kept aloof from
these private citizens.

This was especially not practical because Calvin was
never the ruler of Geneva. He had immense powers of persua-
sion and his personality was apparently formidable. But he
had no armed men, no special authority over the city except
eloquence, reason, and moral force.

He relied upon those intangibles at a time of brute power
and widespread cruelty so persuasive, so savage, and so gross
in its applications, that an ordinary person of that time would
be considered nearly monstrous today in his attitude toward
punishments, floggings, executions, pain in general.

The Secret Army

When students graduated from the Academy in Geneva,
they were assigned various posts, sometimes in France. These
were dangerous trips; so dangerous that when the permanent
ministers of Geneva went to France they left their families
behind, and the city allowed those families to keep the
ministerial house and even receive the minister's salary.’

Those sent to France were given an accredited letter.
These letters were demanded by the receiving congregations;
without them, a young preacher could be turned away, and
this wa3~an insportant factor in Calvin’s system of discipline
and control. On the other hand, the letters had to be well con-
cealed, for if discovered enroute to a congregation, the bearer
could be put to death. In 1533, five Lausanne students en-
route had been betrayed, and all five were burned to death,
one after another, at the same stake in Lyon.

17, Bid, p. 38.
