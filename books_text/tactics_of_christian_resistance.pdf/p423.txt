TACTICS USED BY REFORMERS 377

Calvin and Geneva

The process started in Germany and moved across
Europe, encompassing Switzerland. In Switzerland was the
city of Geneva. Geneva was an Episcopal seat ruled by a
Bishop who was usually connected by birth to the House of
Savoy. Because the Bishop was usually a noble, he seldom
lived in Geneva, which was managed by an episcopal Coun-
cil, headed by a Vicar, The city’s noble families came from
Savoyard ruling classes. Below the Council were several hun-
dred ordained priests in seven city parishes. The city had
church hospitals and schools, legalized prostitutes, ecclesiasti-
cal courts, and a pattern of living typical of the Renaissance.
That is to say, prosperous but immoral, physically comfor-
table but spiritually unhappy.

The Reformation changed that. Geneva swung with the
new tide, and rebelled against the Bishop and the House of
Savoy. The city Council, achieving independence, negotiated
with other cities and finally allied with Berne. Berne was a
great military power at a time when the Swiss soldiers were
considered the most efficient in Europe. Berne came to
Geneva’s aid when Savoy threatened it, and even won sur-
rounding territories for the Genevans. After various struggles
the Bishop fled, followed by the majority of his priests, and
the city turned Reformist. Church property was seized,
church hospitals closed, and charity became secular. The city
formally announced its new beliefs in 1536, the year that
Calvin’s Institutes were printed in Basel.

Farcl, an older and brilliant Protestant, was well aware
that Geneva needed a structure to hold its new faith together,
and persuaded Calvin to come to the city. That was no easy
task: “... things were very disorderly,” Calvin said, “the
Gospel consisted mostly of having broken idols . . . there
were many wicked people.”1? An entire district of the city was
oceupied-by prostitutes, under the rule of their own Reine du
bordel, the Brothel Queen.18

Calvin, Farel, and their associates launched a strict series
of regulations that, within a few years, led to a harsh reaction.

12. Robert M. Kingdon, “Was the Protestant Reformation a Revolution?
The Case of Geneva,” in Transition and Revolution.

13, Will and Ariel Durant, The Reformation (NY: Simon and Shuster,
1957), p. 469.
