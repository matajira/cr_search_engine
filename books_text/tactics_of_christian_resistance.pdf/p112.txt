66 CHRISTIANITY AND GIVILIZATION

When the brothers attacked Joseph, the first thing they
did was strip off his robe (37:23). Then they threw him into a
pit. This was the beginning of Joseph’s humiliation, his
passage into the “deep sleep”-like trauma of suffering, from
which he would learn wisdom, and from which he would be
resurrected and invested with authority.

Joseph was sold to a household in Egypt. The first phase of
his service was in the house of Potiphar (39:1-7). Joseph did
not see his enslavement as a cause for resentment or bit-
terness. We cannot imagine him throwing spanners into the
works, or sand into the machinery. Rather, he served dutifully
and well. As a result, lazy Potiphar gladly entrusted more and
more of the household responsibilities to Joseph. Soon, it was
really Joseph who was in charge, and Potiphar “did not con-
cern himself with anything except the food which he ate” (y,
6). Potiphar had the name of master (and ultimately its power
as well), but he had a slave mentality and lived as a slave, a
slave of food. Joseph had the name of slave, but he was a do-
minion man, and he ruled in life. The point was not lost on
the wife of Potiphar; she knew who the real power in the
house was.

Like the camp followers of all ages, the wife of Potiphar
tried to cleave te the man of power, but Joseph was not only a
faithful servant of Potiphar, he was also a faithful servant of
Ged (39:7-12). Lying with the wife of Potiphar would not only
have been a sexual sin; it would also have been an act of in-
surrection, as we have seen. When the wife of Potiphar grabbed
Joseph’s robe, she was grabbing for his dominion; in terms of
Biblical custom, she was not trying to strip him nude or pull
him to her room, but she was trying to get him to spread his
cloak over her.*® Rather than abuse his authority, Joseph for-
sook it and fled. It is better not to possess invested authority at
all than to abuse it.*? Joseph could have attained premature
authority and power had he gone along with her, but it would

46. Gompare Ruth 3:9; Ezckiel 16:8.

47, The term “garment” corm: times in this paragraph, highlighting
its importance to the story, There seem 10 be parallels between this story and
the “attack on the bride” theme, in that when the woman’s sin is in danger of
expos she Satanically blames the righteous man. The sexual roles are
reversed, as is the identity of the deceiver, In a larger sense, however, all
God's people are the bride, and the seduction of Joseph to sin is equivalent
seduction of the bride to infidelity.

 

  
 

 
