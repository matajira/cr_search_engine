80 CHRISTIANITY AND CIVILIZATION

Tax, as in Europe), because the state is not about to give up
either the military or social welfare programs, the conser-
vatives insisting on the former, and the liberals demanding
the latter. The cost to the individual of “saving my tax money”
is greater than the cost of simply paying, when we consider
the cost of worry, of a fearful wife (very common), and the cost
in time and money of fighting for one’s “rights” in tax court. It
is a pointless battle for the individual to engage in, but an all-
important battle for the church to fight, if the church is directly
taxed,

The Christian resists the powers that be primarily by
avoiding them. In our day, the state is not yet wholly tyran-
nical in the sense that Nebuchadnezzar or Nero were. Thus,
there is a place for resisting the devil, hoping he will flee from
us. The question of when to resist and when to capitulate re-
quires wisdom and discernment to answer in any given situa-
tion, but the boundary line is at the point of the actual exer-
cise of force.
