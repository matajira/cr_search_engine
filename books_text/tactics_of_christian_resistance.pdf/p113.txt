REBELLION, TYRANNY, AND DOMINION 67

not have been permanent and he would eventually have been
put to death for it. The temptation before Joseph, thus, is
analogous to the temptation before Adam and Ham, to seize
power unlawfully,

The vengeance of the wife of Potiphar landed Joseph in
prison. There again, however, he ruled in life (39:20-23).
Because of his effective and responsible service to those in
charge, Joseph was soon put over the entire prison. He had
the position of prisoner, but he was exercising dominion.
From that position he could do much good. By being a good
slave, Joseph acquired mastery.

From prison Joseph was elevated to Pharaoh’s right hand.
The narrative of Joseph’s prison experiences in Genesis 40
shows the means whereby he was enabled to rule in the midst
of enslavement: He understood and applied the Word of God,
which came to him in the form of dreams and to us in the form
of Holy Scripture. Because he understood God’s principles
whereby He rules the world, and because he was able to apply
them accurately to the situation in which he found himself,
Joseph proved of inestimable valuc to every master who
employed him. In time he was exalted to second in command
over all Egypt: “And Pharaoh said to Joseph, ‘You shall be
over my house, and according to your mouth all my people
shall kiss; only in the throne | will be greater than you. See I
have set you over all the land of Egypt.” And Pharach took off
his signet ring from his hand and put it on Joseph’s hand, and
clothed him in garments of fine linen, and put the gold necklace
around his neck. And he had him ride in his second chariot;
and they proclaimed before him, ‘Bow the knee.’ And he set
him over all the land of Egypt. Moreover, Pharaoh said to
Joseph, ‘I am Pharaoh; yet without your permission no one
shall raise his hand ot foot in all the land of Egypt” (Gen.
41:40-44), From this position, Joseph was able to feed the en-
tire world (41:57).

The story of Joseph illustrates patient faith and its reward.
It ends the book of Genesis and brings its theme to a literary
climax. We know that Joseph’s authority was temporary and
not complete; we know that Christ’s now is both, But the story
of Joseph shows us that the road to victory, dominion,
mastery, and judicial authority, is through service, the hum-
ble service of a slave. Through service and suffering, God
purges and destroys indwelling sin in the believer (not com-
