CONFRONTATION WITH BUREAUCRACY 159

criminal cases involving church officers will be released—that
the church itself is not Constitutionally subject to “fishing ex-
peditions” by any public official. Appeal repeatedly to freedom
of speech. This stands up better than freedom of religion in to-
day’s courts. Remind him of the “chilling effect”—a key
phrase in any trial—his actions are having on people who only
want to exercise their Constitutional rights in speaking out.

Another good tactic for the church officer is to say on
camera that he realizes that this official is probably acting
under orders from his superiors, and that the church officer
has no personal ill feelings toward the official. However, the
church officer explains, he simply cannot in good conscience
cooperate with the official, since it would mean giving up the
church’s Constitutional rights. The church officer must make
it clear to the viewing audience, including some future judge and
jury, that he has no authority to give up the church’s Constitu-
tional immunities against unlawful actions being taken by the
invading official’s superiors.

Consider the problem of the enforcing official. Either he is
acting without authorization, in which case there will be big,
big trouble for him if this videotape is aired, or else his
superiors really are pushing him into a very difficult position
—a position fromm which he desperately wishes to extricate
himself. He can still retreat gracefully at this point. Every
fiber of his bureaucratic being is telling him to retreat. All ofa
sudden, this deal does not look so easy. Discretion is the better
part of valor. Run!

The more excitement there is on screen, the more likely
this videotape will be used by local television stations. After
all, few programs ever have a T.V. crew on the premises in the
midst of the action. This really is “action news.” Even if no
local station runs the film, CBN might. The moment the offi-
cial leaves, start calling every news media source you can
think of. Tell them that you have a videotape of everything.

Warning: be sure you record the proceedings on the fast
Speed setting on the videotape machine. You want the highest
guality reproduction you can get. This means high-speed re-
cording. (If churches have %-inch tape machines, all the bet-
ter, but these are expensive and bulky. They are not required.)

If you are in a community which has a Christian television
station, make a bec-line to see the manager. He needs filler. If
you can come on a talk show and bring your videotape, you
