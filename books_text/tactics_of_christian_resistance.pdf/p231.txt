CONFRONTATION WITH BUREAUCRACY 185

little school. We need your help. I am asking you all to come back
next [ date }, and next time, please bring one or two extra people
with you. We want to get as many people here as we can.”

‘That really scares the bureaucrats. They know that by
deferring a decision, they could face a monster crowd. They
have no idea how you managed to get out this large a crowd.
They are afraid that you really will bring in twice as large a
crowd the next time. The longer they delay, the worse it gets
for them. Their way of life —“demand, threaten, conceal, and
delay”—is being threatened by your ability to mobilize people.

Prayer

Throughout any program of resistance, churches need to
be in prayer. I have waited until the closing pages of this essay
to discuss prayer. Prayer needs to be specific. People need to
know what, specifically, to pray about. The steps I have out-
lined so far provide a lot of specifics.

The first prayer request is always for the peace of the church.
This should be a weekly prayer in every church all of the time:
*] exhort therefore, that first of all, supplications, prayers, in-
tercessions, and giving of thanks, be made for all men; for
kings, and for all that are in authority; that we may live a
quiet and peaceable life in all goodness and honesty. For this
is good and acceptable in the sight of God our Saviour” {I
Tim. 2:1-3). Any church which refuses to pray this publicly on
a regular basis is asking for trouble because it is not taking
Paul’s exhortation seriously. We are to pray for peace—not a
peace based on compromise, but peace based on the State’s
willingness to allow us to go about our affairs, building the
kingdom of God.

When the State begins to inhibit our actions in building
up God's kingdom institutions, then we must begin to pray
more specifically. We must pray for the success of specific
kingdom projects: building a Christian school, starting a new
church in a zoned area of town, having Bible studies in homes
in zoned areas. This is a real problem. Writes Rushdoony:

sAs early as the 1940's, the Federal Council of Churches [now
the National Council—-G.N.] of-Churches of Christ, together
with local councils of churches and the Home Missions Council
of North America developed a“Masier Plan” for the location and
relocation of Protestant churches. Dr, H. Paul Douglass, director
