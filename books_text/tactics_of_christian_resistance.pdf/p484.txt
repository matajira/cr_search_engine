438 CHRISTIANITY AND CIVILIZATION

occasional concerned citizen voices objection to some tax or
regulation, the local politician can point to Washington, or
perhaps to the state capital. [t is from there, the uppity citizen
will be told, that “we were told we had to do it to you.” The
higher level of government “mandated” the program,

The concerned citizen will find it almost impossible to dis-
cover who im the state capital, let alone in Washington, is lord-
ing it over him and eating out his sustenance. And the con-
gressman can throw up his hands and sigh that the program is
“mandated.” It is considered bad taste and politically irrespon-
sible to suggest that Congress “un-mandate” its whoppers.

Ultimately, there is no middle ground in this battle. As
Christ said in the Sermon on the Mount, “No man can serve
two masters; for either he will hate the onc, and love the
other; or else he will hold to the one, and despise the other”
(Matthew 7:24). Yet, Christians are trying to serve two
masters indeed when they try to instill biblical precepts in
their children while most of the week the priests of secular hu-
manism are teaching their ethic and destroying the child’s
belief in God and His law. What is not taken care of at school
is handled by the TV.

We are on the threshold of overt prohibitions against
teaching Christianity. Already in the name of the non-
Constitutional doctrine of “separation of church and state”
and “academic freedom,” Christianity cannot be taught in
public institutions. Depending on which judicial circuit one
lives in, it may even be illegal to use a public school room for a
Christian meeting.

It is so true that the power to tax is the power to destroy.
Even Christians often buy the pagan notion that the church’s
tax exemption is a “benefit” bestowed by government. This
view has already lead to substantial restrictions on Christian
schools via IRS regulations. Forgotten is the Christian notion
of our founding fathers that the church’s tax exemption is not
a benefit, but a recognition that the state has no authority
over the church. (That is the first amendment statement of
church-state relations.)

At the moment, the battle centers around the effort by the
benevolent humanists in Washington to prevent any church
school from “benefitting” from tax exemption if it practices
racial discrimination. Since there is a large consensus in the
U.S. which disapproves of racial discrimination, the real issue
