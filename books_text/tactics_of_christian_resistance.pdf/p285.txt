CITIZENS’ PARALEGAL LITIGATION TACTICS 239

God, and those which exist are established by God, There-
fore, he who resists authority has opposed the ordinance of
God; and they who have opposed will receive condemnation
upon themselves.” Paul enjoins us to submit to the governing
authorities, which includes, but is not limited to, civil govern-
ment. Christians are, as a general rule, to submit to all consti-
tuted authority whether in the state, the church, or the family.
A revolt against ail constituted authority is, says Paul, tanta-
mount to rebellion against God. Those who so rebel, accord-
ing to Paul, merit God’s condemnation.

Vic Lockman was such a man. He rebelled against the
civil authorities by passing counterfeit money, clearly an il-
legal and inflationary activity. His rebellion carried over into
the chureh, where he refused to submit to the lawful govern-
ment of Christ’s church which he had formally and publicly
agreed to honor. Eventually, the church under whose disci-
pline he had placed both himself and his family had no other
alternative but to declare him excommunicate. It happened to
him just as Paul warned; he brought condemnation down
upon himself.

Earlier we asked the question: How can we avoid being
like Vic Lockman? We know that we must resist the unjust
encroachments of the State, but how do we avoid bringing the
condemnation of God down upon us in the process? To put it
another way, what are some signs of illicit rebellion that we
should recognize and guard against?

In answer to that query, we must first of all note that zebel-
ion is ultimately a denial of God's sovereignty, Paul notes that in
Romans 13 quoted above. We noted earlier that the concept of
“sovereignty” is the basic issue in Christian resistance. When
one denies God's sovereignty, it is inescapable that an asser-
tion of man’s sovereignty must be simultaneous. (The follow-
ing implications can be implicit or explicit.) To deny God's
sovereignty is to deny His transcendence, His control and
jurisdiction over all reality. To be in rebellion is to assert the
righteousness of man over against the righteousness of God.
To be in rebellion is to deny all responsibility to God, and in
some sense to assert a responsibility to man. To be in rebellion
is to deny to God unlimited. and unconditional power or au-
thority, and to assert the same regarding man.

In the history of the world, the denial of God’s sovereignty
has resulted in one of two positions, each elevating man to
