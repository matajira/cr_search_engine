CONFRONTATION WITH BUREAUCRACY 179

iberties on the one hand, and to raise the costs to the State of in-
fringing upon our Constitutional liberties, on the other.

Our goal should be to make it almost prohibitively expen-
sive for bureaucrats to initiate unconstitutional attacks on our
little institutions. If we can do this, then the State will begin to
reduce its reliance on judicial harassrnent to drive innocent
victims out of existence,

Ihave a dream, as one former media manipulator once said.
Thave a dream of fearless Christian schoo] headmasters walk-
ing arm-in-arm with fearless laymen, whose legal training has
been sufficient to equal $1,400 worth of legal talent. I have a
dream of avoiding the use of defense lawyers in 60% of the
harassment cases. I have a dream of headmasters being able
to hold out until the last minute before having to hire any law-
yer, and then paying him as little as possible to do his
preliminary homework. I have a dream of making it so expen-
sive for prosecuting attorneys to take on a Christian school
that they will spend more of their time prosecuting murder-
ers, rapists, anc burglars, if only because they will spend less
time and achieve greater success, case for case, than prosecut-
ing Christians. I have a dream of desperate local education
officials, bogged down in a mountain of paper, trying to figure
out how all these evils came upon them. I have a dream of
weary judges reading defense motions to dismiss, and being
driven to distraction by skilled defense lawyers who follow law-
yer William Kunstler’s tactic of objecting to everything the
prosecution says ali day long. And I have a dream of being able
to buy the basic tools in two manuals for a total of $200.

My dream would be the State’s nightmare.

Why is it that no lawyer has produced this sort of pro-
gram? I wish I knew. The money is there. The institutional
pay-off is there. It is clear that the clients will soon be there, if
harassment escalates. Why don’t we see Christian school
defense manuals, and anti-abortion tactical manuals, and
how to de-incorporate your church manuals? Why do Chris-
tian legal groups feel compelled to do everything “in-house,”
and not decentralize the whole Christian defense system
through the use of training manuals? Have our Christian
lawyers adopted the mentality of empire-builders? Have they
all decided that if they cannot personally oversee a case from
start to finish that it is better that the victims not be defended?
Tt looks that way.
