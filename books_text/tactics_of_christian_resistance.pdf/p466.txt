420 CHRISTIANITY AND GIVILIZATION

a very valuable asset, a mailing list which is specifically de-
signed for political campaigns. He screens the candidates by
controlling access to his data base.

By develuping local data bases, Christians can become enormously
influential. Over the next decade, direct-mail campaigns will
become even more important than they are today. Christians
need to learn the techniques of building, maintaining, and us-
ing. local public opinion data bases. Few local groups are to-
day assembling such data bases. Those groups that do will ex-
ercise political leverage way out of proportion to their
numbers over the next decade.

Legal Defense

The Rutherford Institute could be used to produce motiva-
tional and training videotapes for pastors and headmasters. A
series of four or five half-hour programs could be broadcast by
the 700 Club during prime time. These films would en-
courage viewers to send in $50 to buy a legal defense manual that
would enable them to gain extra time when any bureaucratic
agency begins to interfere with their Constitutional liberties.
The manual could generate income for both CBN and the
Rutherford Institute, and the films would begin to mobilize
the viewers for the battles in the courts that are already here.

The Rutherford Institute could use its share of the funds
from sales of the defense manual to finance its program of
training local Christian lawyers in the techniques of a Constitutional
defense of religious rights. There is a great need to train up
lawyers in each region wha know the proper legal defense ap-
proach. We need a LAbri for lawyers. If they know what to do in
advance, and if they are continually receiving updated mate-
rials on court decisions, these lawyers will not have to charge
their Christian clients $100 an hour to learn the field. They
will also win more cases.

The present approach to defending the schools and churches is all
wrong. It is centralized, even more than the humanists’ program
of attack is. The Federal bureaucrats use local officials to
serve as their agents. In contrast, Christians have a handful of
overworked lawyers who run all over the country trying to de-
tend everyone. They cannot prepare their cases properly, and
they are losing. This establishes legal precedents that later go
against other Christians, What is wrong here? Clearly, it is
