456 CHRISTIANITY AND CIVILIZATION

his objection, we wouldn’t have gotten his vote. I may be
wrong, but in many cases with the people who have a small
vision of a large problem, it pays to listen, because they are
sincere. There is no question in my mind that this Senator
was sincere.

There is also a sort of subcategory to the sincerely uncommit-
ted, and not all legislatures have it. But they may if they have
old members. The U.S. Senate has a number of examples.
Senator Len Jordon of Idaho was one, as was Senator Milton
Young of North Dakota in his final years. These are people
who are very inflexible. They become so touchy that any kind
of lobbying at all will tend to send ther in the opposite direc-
tion. Perhaps they want to feel that they are not going to be
bought by anybody and nobody is going to contro] them, and
so on. If you have somebody genuinely like this, it is best to
stay away from him. As a matter of fact, what I used to do in
the case of Len Jordon, I would put out the word to the op-
position that we just couldn’t pin down Len Jordon, and were
worried about his vote. They would go all out and try to get
him. And as soon as they did that then he would come running
right into our hands. If you have somebody unusual like that,
make sure the opposition pressures him, and you'll get his
vote. If you pressure thern, they go the other way, regardless
of their history or their philosophy, their previous votes and
everything else.

To know who’s in what category you must get information
from inside the legislature. People ask me, “How do you come
up with a chart like this?” My answer is, you have got to work
with somebody on the inside and force him to put legislators
into these categories. A lot of times people don’t want to do
that. But if they have a reasonable good power of observation,
you will be able to get them to break down your state
legislature or your City Council or whatever it is into these
categories. Then you'll know who to work with. But be sure
you pick the right person to talk to. Some people don’t have
good powers of observation.

The hardest work in the legislative battle is with the in-
sincerely uncommuted. The insincerely uncommitted have only
one issue that’s important to them: their re-election. The people
who manage to get their votes are the people who make the
linkage between their issue and the legislator’s future. In the
past, these people tended to virtually and completely go to the
