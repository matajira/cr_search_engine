176 CHRISTIANITY AND CIVILIZATION

defense witnesses are and what they will say. There are no
more easily won cases. The enemy is well-armed in this bat-
tle. Our people are poorly armed, except when the very best,
most prominent defense attorneys have been hired. (The most
prominent of these attorneys are not always the best, especially
when their tiny organizations are fighting hundreds of cases.
Never hire an attorney to fight your case if he has over five
potential trial cases in progress.) There are few of these men
to go around. They ask and receive high fees, too, or are forced
to raise the money from hard-pressed donors.

Yet the enemy has problems, too. First, the religious tradi-
tions of the United States stand against them. So do the legal
traditions. Second, there are only so many top-Hight prosecuting
attorneys. The government lawyers at the local level are not
usually “the best and the brightest.” If they were really good, they
would be in private practice making three times the pay. Third,
the state still faces the threat of jury tials, and these juries are
sometimes filled with people who are sick and tired of being kicked
around by bureaucrats. So the war is not over. Christians and
independent school supporters have the principles on their side,
and the civil government has both the initiative and the money.

What we need is to take advantage of our greatest
strength: numbers. We have many schools and churches that
need their independence. If we could get the State to commit
more men and resources into the fight, we would find that the
quality of our opponents would drop. Their best legal talents
would be tied up in other court battles.

The court system is becoming vulnerable. Courts are tied
up today in a series of endless appeals.?? It is becoming very
expensive to prosecute a case successfully these days, which is
why defense Iawyers are getting reduced sentences or
suspended sentences for their clients through plea-bargaining
(pleading guilty to lesser crimes), The accused agree to plead
guilty to lesser charges rather than tie up the courts in long
cases to prove that the accused committed a major crime. So
far, Christian pastors and Christian school headmasters have
not been willing to play this plea-bargaining game. There-
fore, it will tie up more of the State’s economic resources if we
stand firm. If we do not capitulate, but force the prosecutors

22, Macklin Fleming, The Price of Perfect Justice (New York: Basic Books,
1974).
