376 CHRISTIANITY AND CIVILIZATION

stopping the Mass. .

Tt was a violent, cruel, and energetic age. The Renaissance
had seeped from Italy through France and into England and
Scotland, into the principalities of Germany and the reaches
of Hungary, Poland, and Spain. In Calvin's France, the Queen
Mother, Catherine d’Medici, born in Florence of parents who
died of syphilis before she was a month old, maneouvered
along the lines of Machiavelli. Descended from Lorenzo the
Magnificent, granddaughter of one Pope and niece of
another, Catherine operated with the help of a private troop of
aristocratic whores who seduced men into her purposes.

In that atmosphere, where poison was considered mer-
ciful, youthful Reformers like Calvin lived amid dreadful
perils. The Ghurch, alarmed at the numbers of books appear-
ing in vernacular tongues, spurred the authorities nto sup-
pressions and punishments. This reaction was evoked by a
flood of printed works “which are estimated to have reached
four million by the end of the fifteenth century and between
1500 and 1533, eighteen more million. In 1533 there were
eighteen editions of the German Bible printed at Wittemberg,
thirteen at Augsburg, thirteen at Strasbourg, twelve at Basel,
and so on.”

By this time people were being punished for possessing
these volumes, and tortured and executed for selling and
printing them. In Paris, during the six months ending in
June, 1534, twenty men and one woman were burned alive.
In the following year the Sorbonne, still under the administra-
tion of the Church, obtained a royal ordinance forbidding
printing altogether.#!

By then it was, of course, far too late. The Church had
been morally declining for a long time. The effects of the
Renaissance had traveled from southern to northern Europe,
and France was in a state of advanced rot. Printing had pro-
vided the Humanists, the young, the unhappy, the uncertain,
and the searching with the means of knowledge and discus-
sion formerly unknown. The new learning traveled with the
speed of light, and brought in its wake the real people of the
Book—the Protestants, whose ranks came from those who
could read: the nobility and the bourgoisie.

10. Smiles, of. cit., p. 28.
M1. Did,
