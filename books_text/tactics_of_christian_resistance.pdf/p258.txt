212 CHRISTIANITY AND CIVILIZATION

spewed out by basement-dwelling mainframe computers.

Nobody really knows how much grass-roots computer ac-
tivism is going on out there, and much of what is happening is
tentative. But many such endeavors are off to a good start,
and the idea is beginning to spread like Pac-Man. Consider
this cross section:

* The Virginia Coalition, a statewide network of action groups,
is using a computer for mailing lists and get-out-the-vote cam-
paigns.

eA Washington state group has formed an alternative
Grange called Tilth that offers farmers agricultural informa-
tion through a microcornputer network.

* Neighborhood community groups in Buffalo, Chicago,
Denver, and Atlanta are using microcomputers to set up job-
skills banks, to fight discriminatory lending practices, and to
get funds for neighborhood rehabilitation projects.

* New York’s Telecommunications Co-operative Network is
one of a mushrooming new breed of service bureaus that pro-
vide a wide range of nonprofit and community-activist groups
with various computer and telecommunications services such
as discounted equipment, cheap rates for computer timeshar-
ing, electronic mail, and teleconferencing. “Demand is
greater than we expected,” says managing director Bob Loeb.
The Co-op is eyeing plans to expand nationwide.

¢ The Disarmament Resource Center, an umbrella organiza-
tion for some 30 peace groups in northern California, has cre-
ated a microcomputer network called Peace Net that offers
electronic mail, a shared database of antiwar materials, and
comprehensive administrative service to coordinate antiwar
activities. “We believe technology can be redirected to serve
our needs,” says director Martha Henderson. “We want to use
it directly in the work of peace.”

Why all of this computer-inspired activity? “It’s a great
equalizer,” Steve Johnson says of the microcomputer. Johnson,
a young Oregon researcher who runs Portland’s Rain Resource
Center, a clearing house for community resources informa-
tion, has been keeping tabs on grass-roots computing for some
time now. He reports a surge of interest in microcomputers
over the past year, for which he cites several reasons: more
people know how to use computers, prices are lower, and the
notion of microcomputers as “hobby” machines is changing.
