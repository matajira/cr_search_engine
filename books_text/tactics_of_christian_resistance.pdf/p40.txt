xl GHRISTIANITY AND CIVILIZATION

The details of each tactic are frequently useful, though in
some cases his humanist recommendations cannot be used.

Interested leaders should locate a copy of the book and
read it. Then they can examine these tactics in the light of the
Bible to see which tactical principles are in conformity to
God's law— the same way Christians need to judge all other
products of non-Christian scholarship. The point is: most of
these tactics work incredibly well. The second point is:
American Christians have not thought about these matters for
well over a century —not since the era immediately preceding
the Civil War. They are not ready to lead. They are not ready
to “throw out the humanists” and replace them in every area of
life, from the police’ force to the physics laboratory.

But they can chew gum.

The Prayer That Never Fails

And the Loro said unto Moses, I have seen this people, and,
behold, it is a stiffnecked people. Now therefore let me alone, that
my wrath may wax hot against them, and that I may consume
them: and I will make of thee a great nation. And Moses
besought the Lorn his God, and said, Lorp, why doth thy wrath
wax hot against thy people, which thou hast brought forth out of
the land of Egypt with great power, and with a mighty hand?
Wherefore should the Egyptians speak, and say, For mischief did
he bring them out, to slay them in the mountains, and to con-
sume them from the face of the earth? Turn from thy fierce
wrath, and repent of this evil against thy people. Remember
Abraham, Isaac, and Israel, thy servants, to whom thou swarest
by thine own self, and saidst unto them, I will multiply your seed
as the stars of heaven, and all this land that I have spoken of will I
give unto your seed, and they shall inherit it for ever. And the
Lorp repented of the evil which he thought to do unto his people
(Ex, 32:9-14),

God was angry. He complained bitterly to Moses. He
offered Moses revenge against those who had criticized him

28. Anyone who criticizes me for recommending Alinsky's book ought to
have an even better book or manual of resistance tactics to substitute in its
place. He also needs a track recurd: some bureaucracy, somewhere, that he
personally has helped to bring to a grinding halt. Criticism from armchair
resisters whose lives and the organizations they control are characterized by
continual capitulation to bureaueracies of alll kinds should not be taken very
seriously.
