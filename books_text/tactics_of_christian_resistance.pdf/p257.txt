COMPUTER GUERRILLAS 2u1

The battle over radar was a personal feud for Paul Lutus,
the highly successful professional microcomputer program-
mer who developed Apple Writer, Graforth, and other best-
selling software. Despite its insignificant scale, though,
Lutus’s battle illustrates the computer's potential for a broader
social function. A microcomputer may be used to augment
your own resources in what Lutus calls “computer activism.”

The Computer as Social Guerrilla

Indeed, one of the more important aspects of the informa-
tion revolution we hear so much about may not be a personal
computer in every home, the automated office of the future,
or even so-called computer literacy for our children; the most
important thing may be the ramifications of the computer as
social guerrilla.

Socially active computing is a departure from most
previous antiestablishment computer activity, which has
tended to be either destructive or quixotic. Until recently
we've seen either outright sabotage by employees or terrorists
with axes to grind or the meddling of sometimes playful,
sometimes disgruntled individuals quietly burrowing into
monolithic mainframe computers at universities and corpora-
tions, This sort of sabotage has been the work of expert
programmers—known as “hackers” in the trade—who have
the skill and know-how to break into large computers and
roam around electronically, erasing and changing data or
sometimes just leaving a “Kilroy was here” mark as a sort of
electronic vandalism.

But now legitimate political and social activists of all
stripes are increasingly recognizing the power a microcom-
puter can lend to their efforts to achieve change. Groups and
individuals from Alaska to Atlanta and from Buffalo to Bur-
bank are beginning to see how their Apples, PE1s, TRS-80s,
North Stars, and Vector Graphics can help them regain con-
trol of their lives and the environment we all share. They’re
talking to cach other on their machines and plugging into
commercial databases or setting up their own; they’re writing
letters to congressmen, they're publishing political-action
newsletters, and in some cases they're even making their own
analyses of government and corporate figures, no longer let-
ting the institutions intimidate them with “official” results

TEM POF ALE SUM OTT Saar

OITA aw re
