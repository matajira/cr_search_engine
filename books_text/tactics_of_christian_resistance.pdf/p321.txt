GITIZENS’ PARALEGAL LITIGATION TACTICS 275

Church of Jesus Christ does not need tax exemption! The Church is a
separate institution accountable solely to the Lord Christ. Jn
order for the Church to need “tax-exemption” it would first have to be
under the jurisdiction of the State for the State can only tax that which it
has jurisdiction over, The Church must never, ever become ac-
countable to the State, and therefore, subject to taxation. To
surrender to any other “head” other than Christ is spiritual
adultery, and I fear the Church of the twentieth century is
guilty of just that.

At this point, the distinction between “immunity” and “ex-
emption” needs to be emphasized. As previously noted, “ex-
emption” is a privilege granted by the Federal Government.
When a church claims that it is “exempt,” even if it asserts that
it is “automatically exempt,” the church is implicitly submitting to
the jurisdiction of the State. Furthermore, in applying for and ac-
cepting a privilege from the government, the church is being
subsidized by government. In a recent Supreme Court decision,
Regan v. Taxation with Representanon of Washington (May, 1983),
Justice William Rehnquist wrote, “Both tax exemptions and
tax-deductibility are a form of subsidy that is administered
through a tax system. A tax exemption has much the same
effect as a cash grant to the organization of the amount of tax
it would have to pay on its income.”

It is extremely important to understand the dynamic of
the government’s position. Justice Rehnquist rightly recog-
nizes that when the government grants a fax-exemption, if tr
allowing that party to keep money it would ordinarily have paid to the
IRS, The government's position is that the “tax-exempt”
organization owes the tax dollars, but the government is
graciously allowing them to keep their money. Justice Rehn-
quist is correct when he says, “A tax exemption has much the
same effect as a cash grant to the organization.” Churches
must learn to accept this reasoning, and not naively to
scream, “You can’t do that!”

Hence, the importance of our contention that the Church
is not “tax-exempt.” If the Church is not “tax-exempt,” how-
ever, what is it? The Church of Jesus Christ, because it is a
separate institution under God, is immune from the State’s
jurisdiction. It is, therefore, immune from Federal taxation.
The Church does not pay income tax or any other tax to the
State because it is not under the State’s jurisdiction.

This distinction quite possibly could prove to be one of the most tm-
