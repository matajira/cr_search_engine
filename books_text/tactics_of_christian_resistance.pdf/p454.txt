408 GHRISTIANITY AND CIVILIZATION

The Lever

Leverage: investors use it, politicians use it, anyone seeking
to influence the outcome of an event uses it. [t is the factor
which gives the one who possesses it more impact on events
than he could have without it. In business, it means OPM
(other people’s money). In polities, it can mean money, or
votes, or trading something the other man wants for what you
want. It may mean “calling in past debts.” In religion, it
means prayer and conformity to God’s will.

“Leverage” comes from the word “lever.” “Give me a long
enough lever and a firm spot to stand on,” Archimedes sup-
posedly said, “and I can move the world.” But Archimedes
would have needed more than a lever and a firm spot to stand
on; he would have necded a fulcrum. A lever with no fulcrum
is like a child’s see-saw with no fulcrum: a useless device that
sits on the ground. To begin the process of recapturing the in-
stitutions of authority, we need some means of centralized
communication. In the American Revolution, Sam Adams's
Committees of Correspondence served this function. In our
day, the direct-mail newsletter industry is the equivalent of
Adams’s Committees. In the future, the microcomputer tele-
communications industry will become crucial. But by far the
most powerful untapped communications resource today is
the CBN Saiellite.

Isolated individuals need to know that they are not alone.
The isolation of churches and Christian day schools today is
very great. There are a few co-ordinating organizations, but
they are very weak. A good way to go bankrupt is to mail un-
solicited materials to churches or Christian schools. Head-
masters have not been willing to fight until their own schools
are being attacked. By then, it is too late. The Sileven case is the
first one in which this pattern of isolation has been broken.

We have to examine the cause of Sileven’s notoriety. One
word describes the difference: television. Several of the “elec-
tronic churchmen” devoted time on their shows to Sileven’s
plight. This media coverage did for Sileven what it did for
Martin Luther King. It brought to light what the bureaucrats
were doing, I was amused at the ABC ‘I'V report. They inter-
viewed one woman, a supporter of the local public school,
who said, “E think Nebraska’s affairs should be run by
Nebraskans, not all these outsiders.” This was the same argu-
