44 CHRISTIANITY AND CIVILIZATION

to cause man to mature in God-likeness. The rest of Scripture
confirms this for us, in that when men are invested with
special office as judges, they are called gods: “God takes His
stand in His own congregation; He judges in the midst of the
gods. How long will you judge unjustly and show partiality to
the wicked? . . . They do not know nor do they understand;
they walk about in darkness; all the foundations of the earth are
shaken, I on My part said, ‘You are gods, and all of you are
sons of the Most High. Nevertheless, you will die like men’ ”
(Psalm 82:1,2,5,6,7a). Jesus cites this passage in John 10:34.
The rulers of Israel are called gods in Exodus 21:6; 22:8,28.
This language may make us nervous, because we are so used
to thinking of man’s making himself into a god as sinful~and
rightly so. It is God alone who can invest men properly with
the robe of judicial godhood, and it is the essence of original
sin for man to seize that robe for himself and seek to make
himself into a god (a judge).

What about the phrase “knowing good and evil”? Again,
in context, God has been said to pronounce things good, as
we have seen. Thus, for man to get knowledge of good and
evil would, in context, mean that man has the privilege of making
judicial pronouncements. Indeed, the rest of Scripture confirms
this. Solomon, the first fulfillment of the Davidic Son-
covenant and the most splendid type of Christ, prays to be
given “an understanding heart to judge Thy people, to discern
between good and evil. For who is able to judge this weighty
people of Thine?” (1 Kings 3:9). God grants this kingly re-
quest (notice that Solomon does not assume that he already
possesses this discernment), and iramediately we see Solomon
exercise His judgment (v. 28), We may also look at what the
wise woman said to David in 2 Samuel 14:17: “For as the angel
of God, so is my lord the king to discern good and evil.” In
other words, man’s judicial authority is a copy of God’s. The
angel of God has wisdom to “know all that is in the earth” (v.
20), and this knowing entails seeing: “My lord the king is like
the angel of God, theréfore do what is good in your sight” (2
Sam. 19:27). Infants do not have the wisdom to know good
and evil in this judicial sence (Deut. 1:39), and frequently the
aged lose this capacity due to senility (2 Sam. 19:35). Thus, it
is not moral knowledge but judicial knowledge that is involved.

Now we can better understand the dragon’s temptation.
“True,” he says, “you are already morally like God. But as you
