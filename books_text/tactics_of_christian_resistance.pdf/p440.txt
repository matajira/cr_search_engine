394 CHRISTIANITY AND CIVILIZATION

mind, any more than our body. For neither may we hurt this:
we must preserve, at all events, the spirit of a healthful mind.
‘Therefore, we may not engage or continue in any sinful trade;
any that is contrary to the law of God, or of our country. Such
are all that necessarily imply our robbing or defrauding the
king of his lawful customs. For it is, at least, as sinful to
defraud the king of his right, as to rob our fellow subjects: and
the king bas full as much right to his customs, as we have to
our houses and apparel. Other businesses there are, which,
however innocent in themselves, cannot be followed with in-
nocence now; at least, not in England; such, for instance, as
will not afford a competent maintenance, without cheating or
lying, or conformity to some custom which is not consistent
with a good conscience: these, likewise, are sacredly to be
avoided, whatever gain they may be attended with provided
we follow the custom of the trade; for, to gain money, we must
not lose our souls. There are yet others which many pursue
with perfect innocence, without hurting either their body or
mind; and yet perhaps, you cannot: either they may entangle
you in that company, which would destroy your soul; and by
repeated experiments it may appear, that you cannot separate
the one from the other; or there may be an idiosyncrasy, —a
peculiarity in your constitution of soul, (as there is in the
bodily constitution of many,) by reason whereof that employ-
ment is deadly to you, which another may safely follow. So I
am convinced, fromm many experiments, I could not study, to
any degree of perfection, either mathematics, arithmetic, or
algebra, without being a deist, if not an atheist: and yet others
may study therm all their lives, without sustaining any incon-
venience. None, therefore, can here determine for another;
but every man must judge for himself, and abstain from what-
ever he, in particular, finds to be hurtful to his soul.

We are, thirdly, to gain all we can, without hurting our
neighbour. But this we may not, cannot do, if we love our
neighbour as ourselves. We cannot, if we love every one as
ourselves, hurt any one ix Ais subsiance. We cannot devour the
increase of his lands, and perhaps the lands and houses thern-
selves, by gaming, by overgrown bills (whether on account of
physic, of law, or any thing else). . . . Neither may we gain by
hurting our neighbour im Ais body... . [such] is dear bought
gain. And so is whatever is procured by hurting our ueighbour
in his soul: by ministering, suppose, either directly or indirectly,
