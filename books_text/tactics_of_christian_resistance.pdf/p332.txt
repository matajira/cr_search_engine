286 CHRISTIANITY AND CIVILIZATION

obsolete practice. As alluded to, it is beginning to be used to-
day, albeit unselfconsciously. In some areas, the civil govern-
ment is finding it difficult to get convictions for marijuana
violations and other so-called “morals” laws. In Georgia, In-
diana, and Maryland, jury nullification is still a constitutional
part of state judicial process.

Jurists and courts throughout the history of our nation
have affirmed and acknowledged its existence. Listed below
are a few selected comments by respected judges and lawyers.

The jury has the right to judge both the law as well as the fact
in controversy. (Chief Justice John Jay, U.S. Supreme Court Georgia
a. Brailsford, (1794).

The jury has the right to determine both the laws and the
facts. (Samuel Chase, Supreme Court Justice, 1804. Signer of the
Declaration of Independence).

There are five separate tribunals to veto laws: representative,
senate, executive, judicial, and jury. It’s the right and duty of juries
to hold all laws invalid that are unjust or oppressive, in their opin-
ion, Ifa jury does not have this right, the government is absolute and the peo-
ple are slaves (emphasis mine). Is it absurd that twelve ignorant men
should have the power to judge the law, while justices learned in
the law should sit by and see the law decided erroneously? The jus-
tices are untrustworthy and are fond of power and authority. To
allow them to dictate the law would surrender all property, liberty,
and rights of the people into the hands of arbitrary power.” (Lysan-
der Spooner, excerpt from “An Essay on the Trial by Jury,” 1852).

Jury lawlessness is the great corrective of law in its actual ad-
ministration. (Dean Roscoe Pound, 44 Am L Rev 12 at 18; 1910).

‘The jury has the power to bring in a verdict in the teeth of
both law and facts. (Oliver Wendell Holmes, Horning 2 DC, 254
U,S. 135, 138; 1920).

If the jury feels the law is unjust, we recognize the undisputed
power of the jury to acquit, even if its verdict is contrary to the
law as given by the judge and contrary to the evidence. This
power of the jury is not always contrary to the interests of justice.
(US o Moylan, 417 F.2d 1002 at 1006; 1969).

‘The pages of history shine on instances of the jury’s exercise
of its prerogative to disregard instructions of the judge; for exam-
ple, acquittals under fugitive slave law. (US x Dougherty, 473 F.2d
1113 at 1130; 1972).

None of the four post-1895 references, when taken in their
full context, encourages jury nullification. They simply

recognize il as part of our history which refuses to die. One
taodern judge, however, has categorically written in favor oi
