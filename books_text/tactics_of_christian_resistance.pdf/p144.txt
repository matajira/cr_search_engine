98 CHRISTIANITY AND CIVILIZATION

five years ago French poet Charles Péguy declared that the
true revolutionaries of the twentieth century would be the
fathers of Christian families. He must have meant that the in-
fusion of meaning and sanity into family nurture has enor-
mous potential to thwart the march of the idolatries.

At the other end of the scale, it it getting ever more
difficult to disguise the intellectual sterility of the modem
movements that a century or two ago moved triumphantly
away from their biblical underpinnings. Czech Marxist phi-
losopher Milan Machovec has expressed frustration at theolo-
gians overly enamored of “dialogue” who fail to speak boldly
enough about the distinctives of Christian faith. Although an
atheist, he believes that the “dynamic” of the West lies in its
allegiance to a transcendent God who relativizes present
achievements. His goal is to find a secular equivalent for God,
and thus rescue the moribund idolatries of communism from
their predicament. Yet, the West itself has fallen victim to
those idolatries, and only a return to the same transcendent
God can rescue it.

In the New Testament, the metaphors commonly used to
describe the church’s external relations were those of war, The
ethic of the early church made it inevitable that strife would
come from its refusal to conform to the reigning idolatries. On
the other hand, dialogue is for the church the great metaphor
of decline and defeai, a dispirited acknowledgment that one
does not have the truth. It is expressed on the popular level by
the currently faddish emphasis on peace, security, and pros-
perity as the normal outcomes of Christian faith, ‘Chis debased
form of Christianity is unable to comprehend the contem-
porary meaning in the incident wherein Christ branded as
satanic Peter’s refusal to accept the reality of the coming
crucifixion. The same idea is often found in the Pauline
literature, “When the people say, ‘There is peace and security,
then sudden destruction will come upon them . . . and there
will be no escape” (I Thess. 5:3).

Their new-found minority status in a world headed for the
brink of disaster holds the promise of providing more excite-
ment than most Christians are expecting. Once again in the
West we live under conditions the early church knew in-
timately, and perhaps we can understand better than most of
our predecessors the meaning of passages in the New Testa-
ment dealing with these conditions.
