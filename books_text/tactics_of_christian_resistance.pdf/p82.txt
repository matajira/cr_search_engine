36 CHRISTIANITY AND CIVILIZATION

church, in terms of the same laws, can be forbidden to dis-
criminate with respect to creed! ‘This would mean equal time
for all creeds, including humanism and atheism, in every
church. In the Worldwide Church of God case, the court held
that a church and its assets belongs, not to the members
thereof, but to all citizens!

Third, the position of Hollings; Reagan; before him,
Carter; the Justice Department; the Internal Revenue Ser-
vice; the Labor Department; the Treasury Department; and
the several states is that the only “freedom” that the church
can have is that activity which the state chooses to tolerate.
Toleration on any and all activities is subject to regulation,
controls, and oversight.

That is, of course, totalitarianism. The fact is that
religious liberty is dead and buried; it needs to be resurrected.
We cannot begin to cope with our present crisis until we
recognize that religious liberty has been replaced with
religions toleration, The limits of that toleration are being
steadily narrowed. If Christians are silent today, the Lord will
not be silent towards them when they face the judgment of
His Supreme Court. There is a war against Biblical faith,
against the Lord, and the men waging that war masquerade it
behind the facade of non-discrirnination, subsidies, legitimate
public interest, and so on.

All this is done in the name of one of the most evil doc-
trines of our time: public policy. Nothing contrary to public
policy should have tax exemption, and, some hold, any right
to exist. Today, public policy includes homosexual “rights,”
abortion, an established humanism, and much, much more.
The implication is plain, and, with some, it is a manifesto: No
one opposing public policy has any rights, The public policy
doctrine is the new face of totalitarianism. It has all the echoes
of tyrannies old and new, and with more muscles.

What is increasingly apparent is that the triune God of
Scripture, the Bible itself, and all faith grounded thereon, are
contrary to public policy. Christianity has no place in our
state schools and universities; it does not inform the councils
of state; every effort by Christians to affect the political proc-
ess is called a violation of the First Amendment and “the
separation of church and state.” Our freedom of religion is
something to be tolerated only if we keep it between our two
ears. A war has been declared against us, and we had better
