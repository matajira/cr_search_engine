CITIZENS’ PARALEGAL LITIGATION TAGTICS 267

From that point, the paralegal litigant must tailor his “ag-
gressive non-filing” to his own situation. If he is an independ-
ent contractor, then his strategy will be different than if he is
contracting with a party that “withholds” a portion of his prop-
erty from his paycheck and deposits it with the IRS. The pro-
cedures for either course of action are, however, outside the
bounds of this paper.

Financial Privacy

Nothing could be further fram the truth than to believe
that the IRS is simply after “tax-dollars.” They are after informa-
tion. The “1040 Form” should be called a “confession,” and not
a “return.” In the last 15 years, the IRS has usurped powers
and jurisdiction far removed from its duty as a tax collector. It
has invaded the hitherto “separated” realms of education, the
family, and the Church. Whaz can explain this incredible ex-
pansion? As Congressman George Hansen points out, “The
IRS embodies the political realities of the selfish human desire
to dominate others. Thus, the end of this gigantic pretense of
officialdom is power — pure and simple.”°8 The information the
IRS receives from the “taxpayer,” and it is primarily financial
information, is used to gain power and control.

The solution? Restrict their fow of information. By not
“filing” a 1040 Confession, the paralegal litigant severely
restricts the flow of information. By practicing financial privacy,
the paralegal litigant makes it even more difficult for the IRS
to gather information about him, Although there is not time
to go into detail, several important aspects of financial privacy
must be mentioned.

1) GET OUT OF BANKING! The number-one source of
information for the IRS is bank records. Although bank
records are technically personal property, they are not pro-

 

paycheck. In order to assume a litigation position in “good faith” after the IRS
has collected “withholding,” the paralegal tax litigant must file for a refund af
all withholding and Social Security tax on the grounds that they collected it
fram someone who is not a “person liable.” If the IRS refuses to refund his
money, he must then file suit in District Court for the refund. The technique,
letters to IRS, documentation, etc. is available in the form of a research report
frorn Dr, George Arlen. For inore information, see Appendix D.

58. George Hansen, How the IRS Setzes Your Dollars and How io Fight Back
(New York: Simon & Schuster, 1981), pp. 15f.

 
