404 CHRISTIANITY AND GIVILIZA'TION

can they create an alternate set of private educational institu-
tions? How can they reduce the tax burden associated witk
the financing of the public schools? Can Christian schools
take tax money and simultaneously escape public control over
their schools? Can they take tax credits and still avoid control?
Most important, should they seek to take over the control oi
the public schools, either to close them down or to make there
decent again. Which?

Christians are beginning to face the reality that the myth
of neutrality helped to hide from them for centuries: religion in-
volves morality, morality involves civil law, civil law means politics,
and politics means power. There is no way that Christians can
escape the implications of this syllogism and still remain
faithful to the comprehensive gospel and its comprehensive salvation.

The word “salvation” comes from the same root as “salve.”
A salve is a healing ointment. Salvation is also a means of heal-
ing. Christians have always recognized that salvation pro-
vides healing for the soul. Some Christians have recognized
that salvation can also involve the healing of the bedy. But
what only a handful have recognized is that salvation also in-
volves the healing of the body politic. Because Christians have
generally failed to recognize this last form of healing, they
have allowed the humanists to capture the political process by
default, The same process of default has led to the humanists’
capture of our educational institutions. This process of “defeat by
default” must be reversed.

We want to influence events. How can we do it? It will
take a multi-tiered program of Christian reconstruction. This
program will involve several aspects. While they may not be
able to be achieved simultaneously, given our limited
resources, the goal should he to develop a full-scale program
which eventually will include all six:

Spiritual awakening
Education
Motivation
Communication
Legal confrontation
Political mobilization

How can we achieve these goals? We must recognize and
acknowledge without any qualification whatsoever the sover-
eignty of God. We rnust submit ourselves to His will. We must
