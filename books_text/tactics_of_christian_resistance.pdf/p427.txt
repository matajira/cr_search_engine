TACTICS USED BY REFORMERS 381

from which news was disseminated. The flow of information
from France to Geneva was systematic and organized. In
effect, Calvin and his associates monitored France.

Their connections were, of course, more widespread than
France alone. In the 1550s John Knox of Scotland arrived in
Geneva and was more deeply impressed by Calvin and the
order he had created in that city than with anyone he met or
heard since the early days of his conversion. Knox had grown
up in lawless Scotland, where vice of every description reigned
with notoriety; he marveled forever over the reins that Calvin
had persuaded the Genevans to accept. In Geneva, Knox
became a Calvinist, and the structure that Calvin created
served as Knox’s model when he returned to Scotland.

In 1556, Knox, having lit a series of fires in Scotland,
returned to Dieppe and to Geneva. His letters home were
signed “John Sinclair,” his mother’s name.!? In Geneva, Knox
joined a congregation of English refugees (from Bloody
Mary), where he was elected a minister. In his conversations
with Calvin and Beza, Knox questioned the traditional doc-
trine of obedience to lawful rulers, irrespective of their
policies. He believed, mainly on the basis of the Old Testa-
ment, that Christians had a duty to rise against the rule of
unbelieving rulers. Judged in that light, virtually all the
monarchs of Europe were usurpers. Calvin and Beza were not
willing to go that far—at least in public.

In private, however, Calvin from a distance and Beza per-
sonally encouraged the House of Navarre, close to the Protes-
tant cause, to compete for control with Catherine d’Medici
and her royal sons. In effect, Geneva silently followed a policy
that Knox more boldly proclaimed.

Knox was not the only Calvinist moving in this direction
in regions heyond the immediate environs of Geneva, The
Calvinists of Holland were inching their way toward a break
with Catholic Spain and its constricting rule. There were
regions in Germany where Calvinists were breaking with both
temporal rulers.

Scotland, however, remained an outstanding example of
the effectiveness of Calvinist principles when boldly applied.
Knox returned there in 1559, after—by correspondence —
having indoctrinated the nobility with the idea that it was

19. Hasper Ridley, john Knox (Oxford: Oxford University Press, 1968),
p. 241,
