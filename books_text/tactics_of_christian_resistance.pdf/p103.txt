REBELLION, TYRANNY, AND DOMINION 57

into the land of a pagan lord. Again deception is used as a tac-
tic to protect the bride. In this case, Abimelech the king
notices that the relationship between Isaac and Rebekah is
more than fraternal, and calls him to account for it. God’s
hand of protection is here, but in the background this time.
Again the king Satanically tries to blame Isaac for a sin that
one of his own people might have committed: If one of the
people had taken her and lain with her it would have been your
fault for not telling us she was your wife. (!!!)

Again YHWH blesses the patriarch (vv. 12-14) and this
brings on the envy of the wicked, who stop up his wells and
otherwise persecute him, eventually asking him to leave their
area (vv. 14-16). We don’t see Isaac raising up his fist, asserting
his constitutional rights, or otherwise contesting the power
given over by God to the Philistines. Unlike the Philistines of
Samson’s day, these men were not invaders, and though
bullies, they had as legitimate a claim to the turf as Isaac did
(though they did not have Isaac’s eschatological guarantee).
Isaac simply avoids thern. Later, in other quarrels with the
powers that be, Isaac again avoids trouble (vv. 18-22), He is
rewarded when God does finally make room for him.

Isaac avoids suicidal and revolutionary action, and God
blesses him in it. In time, the pagans realize that God is with
Isaac, and they come, desiring to have peace with him (wv.
23-33). Had Isaac defied the powers, he would have lost
everything; through humility, deference, and a foregoing of
his “rights,” Isaac came to be a power in the community.

Tsaac had two sons. They were twins and struggled in the
womb: the righteous Jacob against the wicked Esau. (Had
Jacob not been regenerate at this point, he would not have
fought with Esau.) Esau was a hairy man, signifying a bestial
nature which was his in life. Jacob was a “perfect” man, ac-
cording to the clear meaning of the Hebrew of Genesis
25:27.27 From the beginning Jacob knew that he was ap-
pointed to inherit the covenant of God. Esau had no interest
in it, but Jacob’s spirituality desired it earnestly. Like Adam
and Ham, Esau was a completely present-oriented man.
When he came into camp one day, he could not wait twenty

27. Determined to misinterpret the life of faithful Jacob, commentators
and translators alike refise to render ish fam here as “perfect man,” as they
do of Noah in Gen. 6:9 and Job in Job 1:1, or as “blameless,” as they do of
Abram in Gen. 17:1.
