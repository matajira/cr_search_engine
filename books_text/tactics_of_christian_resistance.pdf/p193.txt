CONFRONTATION WITH BUREAUCRACY 147

answer shall ring down through the ages: “We shall stuff their
mouths with gold.” So the Labor Party did, and the medical
leadership capitulated, just as Bevan had predicted.

Then came the grim reality: larger and larger case loads,
coupled with a salary freeze. The medical profession was
trapped. They had been given the initial benefits to gain their
cooperation, and then the trap was sprung.

So it has been with pastors in the United States since 1913.
Their churches were for seventy years exempted from all
property taxation. No longer. Beginning in 1913, the churches
were exempted from having to pay taxes as employers. No
longer (as of 1984). Pastors have received remarkable income
tax exemptions, such as the dual exemption on housing: a
tax-free housing allowance, plus the standard interest-rate
deduction on all mortgage interest on their homes. No longer.
This two-pronged exemption was available only to pastors—a
special subsidy to silence the opposition. And it has worked!
The pastors did not feel the pain that higher-income con-
gregation members did. They did not preach against the
envy-based nature of the “progressive” (graduated) income
tax. Now they are being forced into high brackets as a result
of the price inflation created by fiat money. Will they protest
successfully now?

The churches’ tax exemptions were acknowledged by
English common law—a law-order heavily influenced by the
Bible —in the days of lower taxation. In the old days, the ex-
emption did not mean that much. The legal right to receive
tax-deductible donations did not mean much of anything in
the days prior to the Federal income tax. Today, however, the
churches are as hooked on tax-deductible donations as any
heroin addict is hooked on heroin. They will suffer
withdrawal pains if they lose their various exemptions. They
are too deeply in debt to gct their income cut off without
suffering great pain. In short, the statists bought the silence of the
churches, and they will continue to try to do so. Ifa church gets
invalved in conservative politics— politics that would defend
biblical law, or even historic Constitutional religious liberty ~
it risks disaster. (The National Council of Churches will be
left alone, of course.) The State now claims that tax exemp-
tion is in fact a tax subsidy—a very large subsidy, given the
level of taxation generally. It also claims ihe legal right to reg-
ulate all institutions that receive such “subsidies.”
