170 CHRISTIANITY AND CIVILIZATION

The average bureaucrat does not want trouble. This is a
law of bureaucracy. He wants a fat salary, good-looking
secretaries, and not much to do. He does not want a moral
confrontation. Some humanist leaders want a confrontation,
but the people they hire to do their dirty work are not the best
quality personnel. They are the C+/B~ graduates of their col-
leges, or worse. Bureaucracy corrupts them. It makes them
soft.

Now, how about a $500,000 personal law suit against some
$23,000-a-year bureaucrat for false charges, defamation of
character, or false arrest? How about a second law suit for in-
fringement on Fourteenth Amendment-protected civil rights?
How about a third for his willful denial of First Amendment
Constitutional guarantees? Not law suits against the bureau-
cracy which employs him, but againsi him personally, How
about three or four different computerized law suits, which
can be pulled out of the church’s microcomputer for each suc-
cessive pastor or schoolmaster who is locked up to initiate?
You say it might be a bit expensive for the bureaucrat to de-
fend against in court? You say it might tie him up for several
years? Well, isn’t that a shame. Perhaps his successor will
think twice the next time he starts looking for instant victims
of bureaucratic tyranny.

Christians have to start increasing the costs to bureaucrats for
their unconstitutional infringements on Constitutional rights. It is just
this simple, and just this complex. It involves a willingness of
Christian leaders to commit themselves to several trouble-
some, high-profile, high pay-off, media-designed resistance
tactics that will make it hot for a lot of overpaid bureaucrats.
It is the responsibility of Christians to count the costs of
resistance, and then, under God's guidance, to force the op-
position to count the costs. It is the responsibility of Christian
leaders to make sure that the costs to the opposition are
higher, compared to the expected pay-off, than the costs are to
the church of Jesus Christ, compared to the expected pay-off.
We are fighting for Christ’s kingdom; we have the Constitu-
tion behind us, even if judges are humanists or corrupt; we
have our testimony to proclaim, What are moderate costs for
us are unacceptably high cosis for Civil Service-protected bu-
reaucrats. Sue the bastards!
