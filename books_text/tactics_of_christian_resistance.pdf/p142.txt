96 GHRISTIANITY AND CIVILIZATION

plundering of [their] property,” and those who live now can do
the same, rather than cheapening their resistance. Ayn Rand
caught perfectly the power that a clear conscience gives one
who is persecuted, in the words she put in the mouth of Dr.
Ferris, a scientist who served the state in its quest for power:

... there is no way to disarm any man except through
guilt... . If there’s not enough guilt in the world, we must cre-
ate it. If we teach a man that it’s evil to look at spring flowers and
he believes and then does it—we'll be able to do whatever we
please with him. He won't defend himself. He won't feel he’s
worth it. He won't fight. But save us from the man who lives up
to his own standards. Save us from the man of clean conscience.
He's the man who'll beat us.

One theme that emerges from the literature of resistance
against the Soviet tyranny is that external power silences and
conquers those who are willing to be conquered. Solzhenitsyn
and Bukovsky gave innumerable examples to show that sub-
mission is not a foregone conclusion in the face of inexorable
force: it is an act that is engaged in willingly by those who
could do otherwise. Political authorities whose final appeal is
their ability to kill or imprison their opponents cannot easily
cope with people who say that that is not the final appeal, but
only one appeal among many. This appears on the surface to
be courage, but it is really something much more profound
and powerful. Faith makes it possible to be relatively
indifferent to the secondary considerations while exercising
supreme care about the main consideration.

In the midst of persecution, the community of believers is
the main source of strength for Christians. Their unity is of
capital importance, but this has nothing to do with the ecclesi-
astical gigantism that currently accompanies the great
weakness of the churches. Organizational unity often serves
as a pernicious substitute for the organic unity that ought to
mark the body of Christ and has made it easier in the past for
alien influences to subvert the church. The Living Church in
the Soviet Union could be made a part of the state apparatus
with little trouble once Stalin had moved his own people into
the top leadership. The Nazi revolution similarly found the
unified state church easy to take over, while the authorities
could deal only with great difficulty with the lay-dominated
decentralized churches.

Speculation on how best to meet the threat of disaster
