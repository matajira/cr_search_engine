366 CHRISTIANITY AND CIVILIZATION

that can accommodate true love feasts, orchestras and choirs,
sacramental worship in the round, and even places for sacred
dancing. Work needs to be done in music, training in psalm
singing and chanting, the development of competent choirs
and orchestras, writing music truly worthy of the worship of
God (as opposed to the cheap junk of the last century or so).
The development of a professional class of theologians and Biblical
lawyers, who can speak to the legal questions of our day and
retrain our civilization in the Word of God, is also a task of the
tithe. And of course, the general care and retraining of the
poor and helpless is a task of the tithe as weil.

Should the Tithe Always Go To The Church?

51. Because of the incredible failure of the church in our
day, it is very easy to make a case for giving the tithe to
parachurch organizations (non-sacramental teaching orders),
I believe that the question here must be approached with care.
My thesis is that the elders of the gate (the local church) should
in normal healthy times administer the tithe, and they may
use it in part to support various agencies; but that in times of
apostasy the tithe must go to the Lord, and this may mean
giving it to non-sacramental teaching organizations.

52. It will not do to say that the general office of all
believers means that the tithe may be given wherever the in-
dividual wants. Nor will it do to say that the special office in
the church is to be given the tithe under any and all cir-
cumstances, Rule in the church, including the disposition of
the tithe, is representative or covenantal. Ordinarily, the elders of
the gate (church) should determine the disposition of the tithe.
Members should nat try to designate where their tithe is to be
used. They may, of course, give gifts above the tithe for cer-
tain purposes.

53. When the special officers in the church apostatize, or
become so delinquent that the general officers (members} come
to believe that the tithe properly should be redirected, then the
power of the general office comes into play. Of course, ideally what
should happen is that the true Christians should form a true
church, and direct their tithes there. This is not always possi-
ble, and people rightly choose to give part of their tithe to the
local church and part of it to faithfid prophetic organizations
outside the strict parameters of any particular church.
