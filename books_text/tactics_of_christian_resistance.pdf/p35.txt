EDITOR’S INTRODUCTION XXXV

speeches to visibly enthusiastic but small audiences filled with
people who have not counted the costs of revolutionary
violence. Perhaps there will eventually be a bloody revolu-
tion, but not until the Federal government is much, much
weaker. Furthermore, this bloody revolution will not be led by
Christians, although Christians may well serve as someone
else’s misguided cannon fodder in the battle. If Christians try
to lead “a bloody revolution,” they will find that their
associates are not always Christians, are seldom reliable, and
are not willing to pay the price that the American revolu-
tionaries paid, let alone the price paid by the suicidal Russian
revolutionaries and nihilists of the 1870s and 1880s. There
will be no “Christian” bloody revolution; at most, there will be
sporadic acts of violence committed by tiny, strategically un-
skilled, and socially impotent terrorist groups that call them-
selves Christian—people who are unable to build anything
more lasting than a “hit squad.” Such sporadic terrorism does
not reconstruct a civilization. It does call forth repression by
the State, however—repression which is cheered by the
average voter, including most conservative Christians, who
will want to distance themselves from Bible-quoting “crazies.”

The warning of Christ concerning the exorcism of a single
demon should be in our minds when we hear romantics cry
out for revolutionary violence in order to achieve a single
“housecleaning,” a ome-time violent overthrow of the
“Establishment” which will forever rid the republic of evil
rulers:

When the unclean spirit is gone out of a man, he walketh
through dry places, seeking to find rest; and finding none, he
saith, I will return unto my house whence I came out. And when
he cometh, he findeth it swept and garnished. Then goeth he,
and taketh to him seven other spirits more wicked than himself;
and they enter in, and dwell there: and the last state of that man
is worse than the first (Luke 11:24-26).

Christians need not turn to bloody revolution in this coun-
try; they are in the majority. They need to understand God’s
word, and to have the courage to apply it in every area of their

19, Ronald Seth, The Russian Terrorists: The Story of the Narodniki (London:
Barrie & Rockliff, 1966), James H. Billington, Fire in the Minds of Men, ch.
i4: “The Bomb: Russian Violence.” Cf. Avrahm Yarmolinsky, Raad t
Revolution: A Century of Russian Radicalism (New York: Collier, 1962), chaps.
i-l4,
