264 CHRISTIANITY AND CIVILIZATION

IRS, I have only given you the Constitutional background on
the unconstitutional tactics of the Internal Revenue Service
and the paralegal litigants’ response. A full explanation of haw
to resist the 1RS would require an additional manuscript of
five to eight times the length of this one. In our study, we have
reviewed indirect and direct taxes in detail. The intricacies of
“fact, law, and determination,” however, have not been fully
explored. Nor have we examined IRS administrative prac-
tices. When one assumes a paralegal litigant position, he
needs to know how to terminate Social Security, request a re-
fund, stop withholding, handle an administrative appeal, deal
with audits and summons, appear in Federal court, appear in
iax court, file petitions to tax court, quash a summons, and on.
and on and on. I]t is impossible in an article of this length to go
into all of the practical aspects of the paralegal litigant posi-
tion in detail.55> We can, however, explore some preliminary
considerations that will help if you choose to assume a
paralegal litigant position.

The Acquisition of Knowledge

The mast important objective for the new paralegal litigant should be
the acquisition of knowledge. One must make it his goal to under-
stand not only the traditional tax position, but have a gencral,
well-informed view of the tax laws and procedures. From the
earlier discussions in this article, it can be seen that our “per-
ceptions” of taxes and Federal tax laws are tainted. Notions
previously accepted as “gospel” must be re-examined with spe-
cific reliance upon LAW, FACT, and DETERMINATION.

Extreme care must characterize the activities of the
paralegal litigant. Knowledgeable, well-informed activity can
be successful in the long-run. Sloppy, ill-informed activity will
land a paralegal litigant in the poor house, jail, or both.

As noted carlier, the paralegal litigation movement is
about 35 years old. During that time, many different
strategies have been used by various paralegal litigants. Some
have been more successful than others, but most of thern have
had one thing in common: they Aave granted jurisdiction to the IRS
by signing a form. It was not necessarily the 1040 Form, but
most often it was. It is sometimes necessary to sign a govern-

 

553. For information on how to obtain more information, see Appendix D,
