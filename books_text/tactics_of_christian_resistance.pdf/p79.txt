RELIGIOUS LIBERTY VS. RELIGIOUS TOLERATION 33

This “privilege” called for concessions to the state.

These took a variety of forms. It could mean that the state
appointed or controlled the bishops (Protestant or Catholic}.
It meant that only the state could give permission for a
meeting of a church’s national convocation or general
assembly. In a variety of ways, establishment meant an
establishment under the state’s control. At its best, the church
was turned into a privileged house-slave; at its worst, the
church was simply a part of the bureaucracy, and the working
pastors were rare and alone. Sooner or later, an establishment
meant subservience and bondage to the state.

Second, the tolerated church became a parasite, because it
was dependent too often on state aid to collect its tithes and
dues. It lived, not because of the faith of the people, but
because of the state’s subsidy. As a result, the state church
served the state, not the Lord, nor the Lord’s people. (When
the states turned humanistic and, losing interest in their cap-
tive churches, began to cut their “privileges” and subsidies,
revivals broke out in many established churches as a result!)

Third, the tolerated or established church became a
persecuting church. It could not compete with its now illegal
rivals in faith, and so it used the state to persecute its com-
petitors. Both Catholic and Protestant establishments built up
an ugly record in this respect. Meanwhile, their humanist foes
could criticize their intolerance and speak of this inhumanity
as a necessary aspect of Christianity!

Fourth, religious toleration leads to intolerance, as it
should now be apparent. Toleration is licensure; it is a state
subsidy, and those possessing it want a monopoly. Hence, in-
toleration of competitors results, and the church becomes
blind to all issues save monopoly. In 17th century England, for
example, the blindness of the Church of England under Arch-
bishop Laud, as he fought the Puritans, was staggering. How-
ever, when Cromwell came to power, the Presbyterians
became a one-issue party, the issue being the control and
possession of the Church of England. Had they triumphed,
the evils of Laud would have been reproduced. Cromwell
balked them; later, the Presbyterians undermined the Com-
monwealth and helped bring in the depraved Charles II, who
quickly ejected them from the Church of England.

In colonial America, uneasy semi-establishments existed.
Technically, the Church of England was the established
