IL. OFFENSIVE STRATEGIES OF
CHRISTIAN RESISTANGE

 

 

A CHRISTIAN ACTION PLAN FOR THE 1980s

Pat Robertson

CTOBER, 1979 marks the 50th anniversary of the Great Depres-

ston of 1929—~an event whith did more to shape the existing
framewark of U.S. government policy than any other single event in re-
cent history. Out of the depression came a powerful central gov-
ernment; an imperial presidency; the enormous political
power of newspapers, radio, and later television; an anti-
business bias in the country; powerful unions; a complexity of
federal regulations and agencies designed to control and, in
many instances, protect powerful vested interests; and, more
importantly, the belief in the economic policy of British
scholar John Maynard Keynes, to the end that government
spending and government “fine tuning” would guarantee
perpetual prosperity.

New Deal Keynesian policies did reverse the economic
tragedy of the Depression (with the help of World War II). But
many now feel that the “cure” of the 30s ts the cause of the sickness of the
70s, In fact, many knowledgeable observers are contending
that forces unleashed in the post-Depression days have so weakened
Western civilization, and the United States in particular, that a radical
change is on the way. Some feel that 1979 may be the watershed year
for the Western nations as we now know them.

Five Critical Problems America Must Face Now

As we enter the next decade, here are some of the in-
herited problems which must be dealt with:

(1) Inflation

Deficit spending by the federal government, which
seemed so appropriate in the Depression and later during

A paper originally published by Pat Robertson in 1979. Reprinted by
permission.

304
