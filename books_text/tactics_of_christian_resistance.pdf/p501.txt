POLITICAL RESISTANCE TACTICS 455

would bang out 30 unprinted amendments. Other Senators
would jump to get on Jack Miller’s unprinted amendments
because whoever would show him enough accommodation
would wind up getting his vote in the end. These amendments
did things sometimes like rearranging the commas. You
would sit in wonder at some of these armendments because he
seldom looked at the big picture, but usually dealt with some-
thing small. In sum, it pays to try to accommodate that in-
dividual because if you can reach his small objection, he will
probably come your way.

To give an example: I was in a meeting with one newly
elected Senator who won in 1980, and the Hyde Amendment
was coming up on the Senate floor. Connie Marshner and I
had gone over there to visit him on a different matter, that
didn’t have anything to do with the Hyde Amendment. We
were talking to hin and all of a sudden he said, “By the way,
where do you people stand on the Hyde Amendment?” (The
Hyde Amendment prevents Federal funding of abortions.) To
me, that is as clearcut an issue, as you will find in the country.
From my philosophical perspective, it is unbelievable that
somebody wouldn’t have a clear cut view of that, one way or
the other. But anyway, he said, “Where do you people stand?”
Both of us said we favored the Hyde Amendment. He said, “I
would like to be with the Hyde Amendment too, but... .”
He got up and started pacing back and forth and said, “but
you know, I don’t see that right to lifers who are concerned
about that, are doing anything for the poor people who are
brought into this world. This bothers me, and I don’t see how
Tcan vote for that when I don’t see anything being done to
help them once they're born.” Now, to me that is, first of all,
confusing issues, and second, a perception that doesn’t coin-
cide with reality. But it was his concern. It so happened, we
had some very recent information of a program that Senators
Jepsen, Armstrong, and others were involved in, which is
now called the STEP Foundation. It is a foundation to help
the poor, using the resources of the conservative churches. So
we told this Senator about this foundation for the poor, and it
was like taking a great load off of his shoulders. While we
were talking to him the bell rang and he had to leave, and
when I looked at the roll call the next day in the Congresstonal
Record, he had voted right. I am absolutely convinced, that if
we hadn’t been there at that particular time, and able to meet
