412 GHRISTIANITY AND CIVILIZATION

developers in each camp been too concerned with imitating
the other. But now the levers are in place, and the fulcrums
are as ready as they need to be at this moment in history.
Christian viewers are not nearly so passive any more. They
see clearly the threat of humanism for the first time. There-
fore, it is time to meet the newly felt needs of these viewers.
There is always a need for larger audiences and more foot-
notes, but there is a far greater need today to get the existing
footnotes in bite-sized portions to the. existing hungry
multitudes. Christ fed the multitudes with two fish and five
loaves of bread; we can feed them with our existing body of
materials. While they are digesting what we can deliver today,
the fulcrum experts can crank out more footnotes.

The “End Run”

The humanists captured the mainline denominations, the
universities, the major news media, the entertainment media,
and the public schools. In short, humanists capiured the giant in-
stitutions, But look at what is happening. The generalized in-
stitutions are losing their share of the market. What is clearly
taking place is a shift: from the. generalized to the specialized, from
the large to the small. Life and Look along with the original
weekly Saturday Evening Post, did not survive. The proliferation
of special-interest magazines and newsletters has enabled
advertisers to target specific audiences and increase their
revenues per advertising dollar spent. Now the same phenome-
non is taking place in the television industry. Like the Model T
Ford, which could not compete once General Motors offered
five or six cars with numerous models, so is the modern TV
network. The networks look strong today, just as the Model T
locked in 1914. Looks are often deceiving.

The humanists captured the national political parties, but
today we find that single-interest voting patterns are tearing
the national parties apart. Direct-mail campaigns allow these
groups to target their audiences, producing more votes per in-
vested dollar, and more ulcers per elected politician. What
Alvin Toffler has predicted in The Third Wave, and what John
Naisbett has predicted in Megatrends: Ten New Directions Trans-
forming Our Lives, is the coming decentralization. In short, in
the face of excessive centralization and statism, we are seeing
a countertrend, or better put, multiple countertrends. We are
