CONTRIBUTORS

Harry Caul writes tor Popular Communications magazine.

David Chilton works for the American Bureau of Economic
Research, Tyler, Texas, and is the author of Productive Christians in
an Age of Guilt-Manipulators.

Louis DeBoer is a minister in the American Presbyterian Church.

Michael R. Gilstrap is the Administrator of Geneva Divinity
School Press.

A. Richard Immel is a contributing cditor of Popular Computing
magazine.

Wayne C. Johnson is a specialist in political direct-mail campaign-
ing, and resides in Sacramento, California.

James B. Jordan, Th.M., is an instructor in theology at Geneva
Divinity School, Tyler, Texas.

Douglas Kelly, Ph.D., is a Research Associate of the Chalcedon
Foundation, and currently teaches at Reformed Theological
Seminary, Jackson, Mississippi.

Francis Nigel Lee, Ph.D., Th.D., is a Professor of Theology at

Emmanuel College, Queensland University, Australia.

Connie Marshner is a researcher at the Free Congress Foundation,
Washington, D.C., and has been active in the Pro-Family
movement.

Gary North, Ph.D., is the editor of Remnant Review, an economic
newsletter, His most recent book is The Last Train Out.

Lawrence D. Pratt is the national lobbyist for Gun Owners of
America, and co-owner of Political Data Systems, Inc.

Pat Robertson is the creator of the CBN television network, and
hast of the “700 Club”
