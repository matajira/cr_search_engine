vi CHRISTIANITY AND CIVILIZATION

Four centuries earlier, in December of 1170 a.p., Thomas
Becket, Archbishop of Canterbury, was martyred for the same
holy cause. The story, in its bare outlines, is known because of
the film Becket,3 but eyewitness accounts of the events provide
us a transcript of the actual conversation between Becket and
his murderers on the day of his murder.*

Four officers of King Henry II came to see the Arch-
bishop. Becket had disciplined some lower bishops for rebel-
lion against the Church, men who had sworn allegiance to the
State and had affirmed that the King was head of the Church.
The four royal officers demanded that Becket remove the dis-
cipline pronounced against these men. Becket refused, on
principle.

At this point, one of them took another tack, “From whom
do you hold your see?”

Thomas replied, “The spiritualities frorn God and my lord
the pope;5 the temporalities frorn my lord the king.”

“So you do not recognize that everything you possess you
have received from the king?” asked the officer.

“Not at all, but I have to render to the king what is the
king’s, and to God what is God’s. I will not spare anyone who
violates the laws of Christ's Church.” At this, the knights
sprang to their feet, waving their arms and gritting their
teeth.

That night they returned, and murdered Thomas Becket
in the cathedral.

3. There are some inaccuracies in the film; Becket, for instance, was not
a Saxon, but a Norman.

4. See Richard Winston, Thomas Backe (New York: Knopf, 1967) pp.
3578.

5. Protestants should keep in mind that at this time in history, the pope
was premier bishop of the entire Western church,
