xV1 CHRISTIANITY AND CIVILIZATION

Norman Lear’s People for the American Way organization,
dedicated to battling Jerry Falwell’s Moral Majority, turns out
to be heavily funded by Hugh Hefner’s Playboy Foundation.’
(can see the headline now: “Norman Leer”) But the looming
battles have higher stakes than winning debate points.
Families and churches are literally coming under attack.

The attacks should not surprise Christians. The battle
over sovereignty has been going on since the temptation of
Adam. It was the issue which confronted Job. Not until he
understood its implications was he restored to his former con-
dition of prosperity.

“And the Lord said unto Satan, Hast thou considered my ser-
vant Job, that there is none like him in the earth, a perfect and an
upright man, one that feareth God, and escheweth evil? Then
Satan answered the Lord, and said, Doth Job fear God ‘for
naught? Hast thou not made an hedge about him, and about his
house, and about all that he hath on every side? Thou hast blest
the work of his hands, and his substance is increased in the land.
But put forth thine hand now, and touch all that he hath, and he
will curse thee to thy face” (Job 1;8-21).

The Bible is a book about warfare. The warfare began in
the garden of Eden, and it will not end until the final judg-
ment. The essence of this warfare is ethical: Who will be cursed
“by a man, God or Satan? Who will be praised by the works of
a man’s hands, God or Satan? Who will stand firm for God
ethically, in order to march forward dominically? This con-
frontation involves every aspect of life, for the whole world is
at.stake. There is no neutral territory. God claims total sover-
eignty over the whole creation, and so does Satan, although it
is basic to Satan’s strategy to give rebellious man the impres-
sion that Satan is standing for multiple sovereignty, and
therefore standing up for man. But the heart of his confronta~
tion is to demonstrate that God does not possess the total sov-
ereignty that God, as Creator, unquestionably claims to
possess. If God does not possess total sovereignty, then He is
not what He says He is, and therefore Satan, as the most
beautiful and powerful of created beings, has sovereignty

7. Christian Enguirer (April, 1983), p. 6. Playboy Foundation gave PAW
$40,000 in 1981. It also donated to Gatholics for a Free Choice ($10,000),
Religious Coalition for Abortion Rights ($10,000), National Abortion
Rights Action League ($15,000), and the American Civil Liberties Union
($50,000).
