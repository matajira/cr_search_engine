430 CHRISTIANITY AND CIVILIZATION

Pat Robertson is in a position to reshape Latin America
within a generation. The satellite is a powerful tool.

Goncluston

By using the lever of the CBN satellite communications
network, and by using the existing skills of a group of outside
Christian experts in several academic and practical fields,
CBN is in a position to change the direction in which the
United States is headed. By using Spanish-speaking and
perhaps even Portuguese-speaking translators, it is con-
ceivable that the whole Western hemisphere could be trans-
formed. The Marxists cannot match this. The cults cannot
yet match this. Why not press our advantage while we possess
it? The satellite is temporarily an exclusively Christian educa-
tional tool. The only other group that seems to be making
effective use of satellites to change society is the pornography
industry. Pornography works against the humanists’ culture
more than it works against dedicated Christians, who are less
likely to become addicted.

There are still communities in which cable has not been
installed, or cable systems that do not carry CBN. By adver-
tising the educational program, CBN might persuade individ-
ual churches and schools in these areas to install a reception
dish. They will not merely be missing talk shows; they will be
missing an opportunity to reshape the American Constitu-
tional republic. If CBN could make available at “cost plus ship-
ping” satellite reception dishes, as well as cheap videocassette
recorders, to local churches and Christian day schools, CBN
could broaden its markct for all the other programming.

In short, CBN has within its reach the position of number-one in-
ternational Christian educator. The whoie decentralized Christian
education system could be intellectually restructured by
midnight-to-five broadcasts of supplemental classroom pro-
grams for the Christian day school movement, not just in the
United States but all over the world. It could begin with
kindergarten and carry through graduate school. We could
see the creation of independent Christian schools in regions
where they do not exist today. Any threat to the schools could
be met almost overnight by full-scale mobilization.

The lever is already in place. The fulcrum is waiting on
the sidelines. The game is in progress. The score is humanists
