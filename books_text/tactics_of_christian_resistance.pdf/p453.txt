LEVERS, FULCRUMS, AND HORNETS 407

Humanists wear most of the robes today, not Christians.

The mpth of neutrality has been the primary tool of decep-
tion used by the humanists in this strategy. “Robes” are sup-
posedly nothing more than neutral symbols of authority;
whoever wears them has the right to impose any law-order—
especially an anti-Christian law-order —on all those under his
rule. Thus, America’s institutions — indeed, the West’s institu-
tions since the French Revolution —have been totally restruc-
tured by humanists to conform to anti-God presuppositions.

What should Bible-based institutions look like? They must
be simultaneously unified and diverse. How? The biblical an-
swer is found in I Corinthians 12, where Paul sketches the
outline of church government. Christ is the head; we are the
body. Eyes should not complain that they are not feet, and
feet should not complain that they are not eyes. All receive
their purpose and meaning frorn the head. They are part of an
integrated whole. But that whole is neither “essentially” cen-
tralized nor “essentially” fragmented. The whole is based on a
covenant: an agreement among men, under God, to work
together in specified and limtied ways, to achieve specified and
limited goals, in order to subdue the earth to the glory of God.
No single institution has absolute power, for.no institution is
divine. Institutions are supposed to be ministerial, not
soteriological. God saves men; institutions minister to men,
under God.

Local organization is important; general (central) goals
and strategies should grow out of co-operation. Acts 15
records the results of the Jerusalerm council, where a few
general guidelines were imposed on all church members, but
not many. Any voluntary institution which is organized in
terms of the empire principle—a top-down chain of command —
will become paralyzed. But any organization that ignores the
need for some degree of centralized decision-making will
become scattered. Yet centralization must be limited. As
Lamennais, the early 19th-century French social philosopher,
once cormmented, “Centralization induces apoplexy at the
center and anemia at the extremities.”°

9. Cited by Robert A. Nisbet, The Sociolugical Tradition (New York: Basic
Books, 1966), p. 113.
