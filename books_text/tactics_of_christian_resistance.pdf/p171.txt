APOLOGETICS AND STRATEGY 125

Rushdoony.

The conflict of Christianity with Rome was thus political
from the Roman perspective, although religious from the Chris-
tian perspective. The Christians were never asked to worship
Rome’s pagan gods; they were merely asked to recognize the
religious primacy of the state, As Francis Legge observed, “The
officials of the Roman Empire in a time of persecution sought to
force the Christians to sacrifice, not to any heathen gods, but to
the Genius of the Emperor and the Fortune of the City of Rome;
and at all times the Christians’ refusal was looked upon not as a
religious but as a political offense... .”

Schaeffer:

The early Christians died because they would not obey the state
in a civil matter. People often say to us that the early Christians
did not show any civil disobedience. They do not know church
history. Why were the Christians in the Roman Empire thrown
to the lions? From the Christian's viewpoint it was for a religious
yeason. But from the viewpoint of the Roman State they were in
civil disobedience, they were civil rebels. The Roman State did
not care what anybody believed religiously; you could believe
anything, or you could be an atheist. But you had to worship
Caesar as a sign of your loyalty to the state. The Christians said
they would not worship Caesar, or anybody, or anything, but the
living God. Thus to the Roman Empire they were rebels, and it
was civil disobedience. That is why they were thrown to the lions.

Francis Legge in volume one of his book Forerunners and Rwals
of Christianity from 330 B.C. to A.D. 330 writes: “The officials of
the Roman Empire in times of persecution sought to force the
Christians to sacrifice, not to any heathen gods, but to the Genius
of the Emperor and the Fortune of the City of Rome; and at all
times the Christians’ refusal was locked upon not as a religious
but as a political offense.”

Striking parallels, don’t you think? Especially when you
look up what Dr. Schaeffer does say in his footnote. He cites
only Legge. Now, you should understand that Legge’s book is
not often cited by scholars; the modern edition is published by
University Books, a relatively obscure publishing firm which
has specialized in scholarly reprints of books dealing with the
occult. In 1964, when Legge’s book was reprinted — the edition
cited by Dr. Schaeffer in his footnote— Rev. Rushdoony was a
member of the Mystic Arts Book Club, University Book’s mail-
order marketing arm; he even persuaded one of the authors of
this essay to join this book club in 1965. In 1965, Dr. Schaeffer
was in Switzerland. Can we reasonably guess who found this
citation in Legge? But who gets cited by Dr. Schaeffer? Nice,
