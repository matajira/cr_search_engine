TITHING 367

54, The tithe goes to the Lord (Lev. 27:32; Mal. 3:8). Whena
church ceases to set forth the law of the King, to make present
the reality of the Lord, we are obliged to cut off giving it the
tithe. To give the tithe to apostates is to rob God. Thus, in
some seasons of the history of the church, the tithe will need to
go to parachurch institutions, but only because these are
really more fully Levitical than the so-called church itself.

55, Sometimes 2 Kings 4:42-44 is pointed to in this
regard. The people evidently brought the tithe to the prophets
in Northern Israel. This is interpreted as due to an apostasy
on the part of the Levites. While I think that this situation is
roughly analogous to what has been set forth in #s 53 and 54
above, it is not as parallel a situation as it might seem. Nor-
thern Israel was cui off from Jerusalem and the central
Levitical work. It was a separate nation. Many if not most of
the Levites migrated from Northern Israel to Judah. The
prophets and the schools of the prophets were stmply the churches of Nor-
then Israel. Of course, they were not the national church, for
the officially approved cult of Northern Israel was calf wor-
ship, Baalism. The prophets formed a remnant church, not a
parachurch organization.

56. It was the elders of the gate who directed the local
tithe. to the poor and to the local Levite (Deut. 14:28f.).
Similarly, in early America, the churches contributed part of
the tithe to support the American Tract Society, the American
Bible Society, and various other tithe-agencies, such as those
dedicated to missions among immigrants. As the churches
became more institutional and less evangelical, local churches
were expected to give only to denominational benevolences.
With the splitting of the traditional and now apostate churches
in the early years of the twentieth century, the fundamentalist
groups frequently returned to the practice of supporting
“parachurch” tithe agencies. Thus, God’s general principles
have been applied in varying ways due to circumstances.

How to Tithe

57, Since all life and strength comes frorn God, we owe a
tithe on whatever our hands produce. The tithe is a return to
Him for the strength and capital He has given us. The tithe is
paid on the increase, what we make with what we have been
given. Those who are paid wages and salaries have little prob-
