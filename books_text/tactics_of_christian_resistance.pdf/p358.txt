312 CHRISTIANITY AND CIVILIZATION

The Keys to Success: Faith and Diligence

The Communists, who espouse a false religion, after only
60 years dominate the world. The reason is simple—they
were dedicated to their cause and they worked at it. Except for
our Lord’s return we cannot expect our nation or our world to
be freed from tyranny in one year or even 10 years. But if we
are faithful and diligent, with His blessing, it will be done.

“The kingdoms of this world are become the kingdoms of
our Lord, and of His Christ; and He shall reign for ever and
ever” (Rev. 11:15).
