258 CHRISTIANITY AND CIVILIZATION

In order to understand the relationship between FACT
and LAW, the following chart is helpful.®

/

DETERMINATION

 

oF PAGE

 

FACT LAW

 

 

The triangular chart above illustrates the proper applica-
tion of LAW to FAC'L It points out the connection that causes
certain and specific law, which is recited in the proceeding or
in a brief, to be applicable to the certain and specific facts
which have been introduced into evidence. “Determina-
tions” are those decisions, judgments, orders, or other com-
pleted action taken at the end of a proceeding. A “determina~
tion” is arrived at by a plenary consideration of the relation-
ship of the “Findings of Facts” and “Conclusions of Law”
presented in the proceeding.

48, This chart was devised by Dr. George Arlen and inchided in a non-
published report tided “IRS Administrative Procedures and Appeals.” For
more information about Dr. Arlen and his research reports, please see
Appendix D.

49. “Evidence” is “any species of proof, or probative matter, legally
presented at the trial of an issue, by the act of the parties and through the
medium of witnesses, records, documents, exhibits, cancrete objects, ete.
for the purpose of inducing belief in the minds of the court or jury as to their
contention” (Black's Law Dictionary, p. 498). In other words, “evidence” is
matters of fact conclusively proven to be so through the mediums mentioned
above, and specifically bearing upon the case.
