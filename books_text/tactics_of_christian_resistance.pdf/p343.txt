CITIZENS’ PARALEGAL LITIGATION TACTICS 297

The Privacy Act

As was noted earlier, the Privacy Act is designed to give
citizens more control over what information is collected by the
Federal Government about them and how that information is
used. The Act accomplishes these two related purposes in five
basic ways:

1. The act requires all government agencies publicly to
report the existence of records maintained on individuals.

2. Information contained in these record systems must be
accurate, complete, relevant, and up-to-date.

3. Procedures are outlined whereby individuals are given
the opportunity to inspect and correct inaccuracies in almost
all Federal files about themselves.

4. Information about an individual gathered for one pur-
pose must not be used for another purpose without the in-
dividual’s consent.

5. Agencies must keep accurate accounting of the disclo-
sure of records and these disclosures must be made available
to the subject of the records (although there are certain excep-
tions to this rule).

In this day and time, anyone who has had any dealing
with the Federal Government has a file kept on him
somewhere in the records of some agency. If you believe that
an agency has kept records on you, then simply write to that
particular agency and ask ther, They are required io inform pou
whether they have files on you. Once the specific files are discov-
ered then the procedures are basically the same as with the
FOIA. Write a letter along the same lines as the FOIA sample
letter above. Insert “Privacy Act of 1974, 5 U.S.C. 522a” in
place of the FOIA information. Most of the same ground
rules apply to Privacy Act requests as with FOIA requests.

Once you receive the records kept on you, then use the
Privacy Act provisions for ensuring the information is ac-
curate, complete, relevant, and up-to-date. You should always
challenge the inclusion of any information in your file describing your
religious or political beliefs, activities, and associations; that is unless
you voluntarily gave that information at some point in the
past. The reason for this specific challenge is that the Privacy
Aci prohibits the maintenance of information regarding the way an in-
dividual exercises his First Amendment freedoms.
