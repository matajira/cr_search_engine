HOW TO LOBBY 463

local issues which a Congressman can’t avoid knowing about.
Then you do have people who take an issue to heart and make
it their personal cause. Then they know what they think about
it. But in general, for the run of the mill issues, most votes are
cast that way, by relying on staff analysis and recommendation.

Staff work is certainly how they answer their letters. A
Senator sees very few letters. The staff writes them. Therefore
the staff is in the powerful position of putting words into their
mouth. Staff also writes statements for newsletters, speeches
for debates, comments for newsletters. Staff keeps the Con-
gressman’s calendar. Congressmen only rarely write those
committee reports or the amendments to bills; the staff does
all of that. The staff is more powerful than the legislator in
some instances.

Don’t get the impression I’m belittling staff. Not at all. A
reliable, honest, hardworking staff person is as valuable a
friend as a legislator can have. Former staffers occasionally
run for Congress themselves, and do as well or better—a job
than anyone else. The reality is that an efficient staff person
becomes very powerful. Power, like handguns, is morally
neutral. Whether it’s good or bad depends on how it is exer-
cised. Being a staffer to a legislator is an opportunity ta serve
the Lord. Discharging properly your duties there has intrinsic
benefit to the nation, as well as the intrinsic value to your soul
of doing any job well and faithfully. And right now, there is
not enough conservative staff to go around. If you have any
sons or daughters who are conservative, college-educated,
and looking for a career, talk to them. Urge them to come to
Washington. It takes three or four years for somebody to get a
foundation of Hill experience, and to learn how to be
effective, but once those skills are mastered, if the individual
is committed, there’s no limit to how far he or she can go.

The jast couple of years, the conservative movement had
the problems of being asked by good newly elected conser-
vatives to help find them a staff. The bottom of the barrel was
scraped in 1981 on Capitol Hill by legislators who were look-
ing for conservative staff. There are plenty of liberals. For ex-
ample, Strom Thurmond, a strong conservative Republican,
of South Carolina, is the Chairman of the Judiciary Commit-
tee in the Senate. His chief staffer is a former Kennedy staffer,
because there wasn’t anybody else to take the job. There you
are talking about a position for which experience is the critical
