APOLOGETICS AND STRATEGY 129

pose of the First Amendment” (idid.). But is any of this possi-
ble? Can we achieve a godly society under pluralism? Have
we achieved it in the past under pluralism? Furthermore, is he
correct in his assessment of the First Amendment?

On this point about the purpose of the First Amendment
being “freedom for all religion,” we believe that Dr. Schaeffer
is mistaken, and we know of a dandy little book that will cor-
rect his understanding on this point. The book is called A Chris-
tian Manifesto, and on pages 34-39 it effectively demonstrates
that the United States began, defectively but substantially, as a
Christan nation (or, more precisely, a union of Christian
states)*8 —that, in fact, the First Amendment actually protected
the existence of established state churches. Admittedly, the
relationship of Christianity to the civil government in the
early life of our nation was somewhat ambivalent and confus-
ing, but not nearly so ambivalent and confusing as A Christian
Manifesto is.

Once we get freedom of religion going, however, Dr.
Schaeffer says, “Reformation Christianity would compete in
the free marketplace of ideas* (p. 136f.). Just like in the
Bible—remember? From this point of view, God handed
Moses the Ten Suggestions, and said: “Run this up the
flagpole, Moses. Set it out there in the Free Marketplace of
Ideas. Of course, being a Libertarian at heart, I've got to
acknowledge that some people might not like My ideas. They
might want to sacrifice their children to Molech instead. Well,
that’s the way the civilization crumbles.”

We should not mix categories. Religious competition is
not a commercial activity—or shouldn’t be, anyway. A “free
marketplace” is a place to sell soap. When it is used as a
metaphor for religious confrontation it is either meaningless
or deceptive. In fact, it is the triumph of Christianity alone
which ensures the existence of a free marketplace for soap.
But remember: When Dr. Schaeffer argues against theocracy and for
“religious liberty,” he is adoocating neutrality. Similarly, when Dr.
Schaeffer argues against neutrality, he is advocating theocracy. This
was the point of the essay on the myth of neutrality in the first

45, The Constitution was not meant to create a unitary State, but rather
aunion of stares. Therefore, its purpose was not lo create a unitary national
religion, It allowed the various Christian states to maintain their varying
Christian traditions. In fact, it proAthited the central government from interfer-
ing with these varying, but explicitly Christian, traditions.
