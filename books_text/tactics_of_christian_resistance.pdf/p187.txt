Il, DEFENSIVE STRATEGIES OF
CHRISTIAN RESISTANCE

 

 

THE ESCALATING CONFRONTATION
WITH BUREAUCRACY

Gary North

Now there arose up a new king over Egypt which knew not Joseph.
And he said uato his people, Behold, the people of the children of
Israel are more and mightier than we. Come on, let us deal wisely
with them; lest they multiply, and i¢ come to pass, that, when there
falleth out any war, they join also unto our enemies, and fight against
us, and so get them up out of the land. Therefore they did set over
them taskmasters to afflict them with their burdens. And they built
for Pharaoh treasure cities, Pithom and Raamses. But the more they
afflicted them, the more they multiplied and grew. And they were
grieved because of the children of Israel (Ex. 1:8-12}.

HE Pharaoh of Moses’ day understood the threat to

Egypt posed by the rapidly growing population of the
Israelites. But he had become dependent on their productivity.
Instead of killing them, he enslaved them. He wanted the
fruits of their labor; he was not willing to pay the necessary
cost of eliminating this threat to his kingdom. His strategy
failed; the more he enslaved them, the faster their numbers
grew. There was an escalation of oppression which was matched
by an escalation of counterforce capabilities. In the case of the
Hebrews, their counterforce response was not only effective,
it was one of the few sources of pleasure they possessed. They
gave it everything they had.

Throughout history, this is a recurring experience. Bureau-
cratic kingdoms are essentially parasitic. They are forever in search
of new hosts to sustain them. They are always dependent on
the more productive members of society to provide them with
surplus wealth. Furthermore, as bureaucracy grows, it ab-
sorbs more and more production. The capital reserves of
the civilization are drained off into unproductive activities.
Kingdoms always construct pyramids of one sort or another —
vast projects that produce nothing except a sense of awe and
resentment.

141
