APOLOGETICS AND STRATEGY 119

usually come from other people —-people who fail to show up
in his footnotes. Before we open that can of worms, however,
we need to understand Dr. Schaeffer’s basic argument in 4
Christian Manifesto.

Dr. Schaeffer begins his treatise by observing that the
various problems confronting Christians in our society ~ the
single issues over which Christians become agitated—are
really “bits and pieces” of a much larger whole. The basic prob-
lem is that a radical change in worldview has taken place—a
paradigm shift (to use Thomas Kuhn's now common phrase*")
“away from a world view that was at least vaguely Christian in
people’s memory (even if they were not individually Chris-
tian) foward something completely different” (p. 17). This
“different something” is a relativistic philosophy in which au-
tonomous man and his choices are regarded as ultimate (i.e.,
humanism).

The American apostasy from Christianity into humanism
has had grievous results in all areas of life, and most obviously
perhaps in civil government and law. It is Christianity, Dy.
Schaeffer insists—Reformation Christianity especially —
which has given us what he calls the form-freedom balance of
obligations and rights in government. “There is a balance
here which we have come to take as natural in the world. It is
not natural in the world” (p. 25). In moving away from the
Christian worldview, apostate man has sought to find the
basis of law in himself, in his own reason and experience. and
the inevitable consequence of thus deifying man has been both
anarchy and tyranny.

The humanists push for “freedom,” but having no Christian con-
sensus to contain it, that “freedom” leads to chaos or to slavery
under the state (or under an elite), Humanism, with its lack of
any final base for values or law, always leads to chaos. It then
naturally leads to some form of authoritarianism ta control the
chaos (pp. 29f.).

In opposition to humanism, the Reformation stressed the
only base for law is “God’s written Law, back through the New
Testament to Moses’ written Law. . . . The base for law is not
divided, and no one has the right to place anything, including
king, state or church, above the content of God’s Law” (pp.

41. Thomas Kuhn, The Structure of Scientific Revolutions (Chicago: Univer-
sity of Chicago Press, 1962).
