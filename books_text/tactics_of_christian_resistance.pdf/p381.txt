THE CHURCH AS A SHADOW COVERNMENT 335

III. Oppression

A third characteristic of the city: The people of God found
no vest in the city. [t terrorized them. One bitter moment ex-
plaining this fear is actually the first experience they had at
building —in Egypt. The name of Egypt in Hebrew is Mizraim
— suffering or sorrow. Mizraim was the son of Ham, and the
theological son of Cain, both men of sorrow. God brought His
people to this land. Initially they dominated, but failed to
carry out the cultural mandate. Thus, even though their land
became a garden~—the name Goshen means garden— Egyptian
opposition reduced them to slavery on their first building
project.

The names of the cities they built were Ramses and
Pithom. Pithom means “house of Thom,” the sun god. More
importantly, they were graineries. Grain reminds of the oc-
cupation of Cain. He was a aller of the ground, When he
turned murderer, growth became an obsession. Out of the
Cainitic cultures originated the fertility cults. In Egypt, God
demonstrated in the exaltation of Joseph to power that he
brought growth as well as famine. When Israel retreated from
growing and extending the cultural mandate over Egypt, they
ended up serving pseudo growers, building graineries for a
foreign god.

The name Mizraim implies slavery. Israel did not learn
building in freedom, so God made them learn by building
pagan edifices. Eventually, a deliverer, Moses, emerged from
the training of pagans. Not their skills, however, delivered
Isracl. Moses was taken to another training ground, the
desert, where he learned the Word of God. The church must
not forget this experience. God has done it before, and He will
do it again.

IV. Seduction

The final characteristic of the pagan city is seduction. The
first two cities that come to our mind are Sodom and Gomor-
rah. The clearest example, however, is the urban influence
over Solomon and his sons. Scripture says of Solomon, “And
this is the reason of the levy which King Solomon raised: for
to build the house of the Lord, and his own house, and Millo,
and the wall of Jerusalem, and Hazor, and Megiddo, and
Gezer .. . and all the cities of store that Solomon had, and
cities for his chariols, and cities for his horsemen, and that
