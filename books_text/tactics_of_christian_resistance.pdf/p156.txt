110 CHRISTIANITY AND CIVILIZATION

nothing. We can have confidence in Him.

The early church conquered the Roman Empire,: and
Augustine laid the theological and philosophical foundation of
Western civilization, He offered a new view of history, a linear
view in contrast to the cyclical views of pagan culture.#
Augustine was a predestinarian who warred against the free-
will doctrines of Pelagius.2 Martin Luther launched the Prot-
estant Reformation in the name of a totally sovereign God;
and he warred against the free-will doctrines of Erasmus.%
The predestinarian views of John Calvin, John Knox, Martin
Bucer, and most of the other Protestant Reformers are well
known. The Puritans who founded New England were all
Calvinists. Even Charles Spurgeon, hero of modern Baptists,
was a staunch predestinarian.*4 In our century, Christian in-
tellectual leaders who have held this belief include J. Gresham
Machen, who led the fight against theological liberalism in the
1920s and 1930s, R. J. Rushdoony, and Francis Schaeffer.

There will be many Christian resisters who read these
words and who say to themselves, “You don’t need to believe
in predestination in order to resist tyranny.” That is quite
true. You also don’t need to be a Christian to resist, tyranny.
But in order to: replace tyranny with a functioning biblical
alternative civilization, you nced to have confidence in the
power of God to back up your efforts— a God who has decreed
the success of His cause, in time and on earth.

Few major movements in history have ever succeeded in
extending their dominion over large chunks of this earth with-
out adopting some version of providence, meaning predestin-

21. Charles Norris Cochrane, Christianity and Classical Culture (New York:
Oxford University Press, [1944] 1987), pp. 4808.

22. The Anti-Pelagian Works of St, Augustine (3 vols.; Edinburgh: T. & T.
Clark, 1872-76).

93. Martin Luther, The Bondage of the Will (London: James Clarke & Co.,
1957),

94, Tain Murray, The Forgotten Spurgeon (Edinburgh: Banner of Truth
Trust, 1966). Incredibly, John R. Rice, a mid-twentieth-century American
fundamentalist who edited books by Spurgeon, dcliberately (and without
warning to the reader) eliminated all references to predestination in his
edited versions of Spurgeon’s writings. When called to account for his ac-
tions by Rev. C. W. Powell of Anderson, California, Rice wrote back that
he would never give any impetus to this doctrine under any conditions, in-
cluding his editions of the writings of Spurgeon. (I have seen this letter; this
is not an apocryphal story.~G. N.)
