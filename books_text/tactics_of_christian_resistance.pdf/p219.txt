CONFRONTATION WITH BUREAUCRACY 173

Cutting the chain lets the other side know they are in for a long,
comprehensive battle —a battle which pou iniend to win. They are not
used to people who cut chains. If they threaten to arrest the
person who uses the chain cutter, get everyone involved.
Everyone ought to have a crack at it: a squeeze here, a nick
there. It will not be clear just who it is who is responsible for
cutting the chain. Besides, it is probably only a misdemeanor
to cut the chain. Be ready to pay the fine and replace the
chain, but only after a long court battle proves you were not
within your Constitutional rights—a very expensive trial,
which will involve several people (preferably pastors), or
several trials, held one at a time. You either get a “Burlap
Eight” media event, or a ridiculously long series of trials. And
if you lose, buy the police a new chain. Big deal. But let them
know, if they try to chain up your church again, you intend to
cut it again. And again. The only way they will get you to stop
cutting the chain is to make chain-cutting a capital crime.
Dead men don’t cut chains.

This is psychological warfare, public relations warfare,
county budget warfare. Every time they try to infringe on your Con-
stitutional liberties, it 1s going to cost them a bundle, Cutting a chain
is an act of defiance, but it is a very specific kind of defiance. It
is defiance in the name of God. It is defiance which will be
respected, if not understood, by the average voter. He just
cannot understand why a church needs to be chained up. You
have won over a voter when you act on principle in a polite,
peaceful, and relentless way. You demonstrate by the
relenilessness of your opposition that you are fighting on the basis of
principle. Only madmen or principled men would keep cutting
achain, You must demonstrate to the voters that you are prin-
cipled, and you can also score points with the bureaucrats if
they suspect that you might be a bit crazy.

Brush-Fire Wars

The escalation of Federal, state, and local pressure against
Christian schools has reached a crisis stage. (The escalation of
pressure against churches is in the early stages.) Day after
day, headmasters are being put under new bureaucratic regu-
lations. No longer is the battle against Christian schools being
left to amateur prosecuting attorneys. The bureaucrats are
bringing out their biggest guns.
