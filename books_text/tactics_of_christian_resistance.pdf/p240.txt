194 CHRISTIANITY AND CIVILIZATION

“Mizz Baxter Goes Bonkers”

Of course, her actual narne wasn’t Mizz Baxter, it was Mizz
Something-Else, but on the odd chance that this publication
makes it through the censor at one of those small sanitariums
in downstate Illinois, I just wouldn’t want to be responsible
for pushing her over the edge . . . again.

It all started when Mizz Baxter took her sabbatical leave
from the Northwestern University Philosophy Department
back in the early seventies. That was probably a good idea.
Deciding to spend her vacation teaching at Purdue, however,
was definitely a bad idea (the first of several).

To fully appreciate Mizz Baxter’s situation, one must
remember that the Jate sixties and early seventies were a time
of turmoil on our nation’s college campuses. There were riots
and demonsirations, civil disobedience and just a hint of
revolution in the air. Heady stuff for an old fellow traveller
like Mizz Baxter, who had all but given up hope that she
would live to sec the collapse of the fascist superstate and the
dawn of proletariat victory. (Don’t laugh, there were lots of
people that talked that way back then. Really.)

Then it happened. She was walking across the mail from
University Hall to Stewart Center when, glancing down, she
saw painted those words that were to change her life.

“CUBA WEEK. HEAR THE TRUTH ABOUT COM-
MUNISM IN CUBA FROM FORMER AMERICAN DIP-
LOMAT TO HAVANA, PAUL BETHEL.”

The time and place of Bethel’s speech was noted below, in
smaller letters, along with the name of the sponsoring
organization, To most students, it was just another watercolor
message to be washed away with the next rainstorm. But
when Mizz Baxter read those words, she was immediately
gripped with that mixture of fear and exhilaration that only
the combat soldicr can truly know. The Enemy was here.

There wasn’t time to waste. Revolutionary action must be
bold and imaginative. Later that morning, she would awaken
her Introductory Philosophy students to the coming conflict.
She would not dwell on mere factuality, but rather sublimate
those facts to the necessities of dialectical thought. She would
tell a lie.

No, not a lie. A bone fide whopper.

As she stood before the class and began to tell them of the
