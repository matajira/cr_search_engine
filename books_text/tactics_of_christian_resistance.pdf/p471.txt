LEVERS, FULCRUMS, AND HORNETS 425

Household Evangelism

The importance of the household in evangelism is not gen-
erally understood. We invite people to worship in God’s
house, but we fail to see that we are also to invite people into
our homes as a way to introduce outsiders to the household of
faith. They see the whole Christian man within the framework of
a crucial area of responsibility and dominion. They see the outwerk-
ing of the household’s faith, from the discipline of the children
to the orderliness of the house. This testifies to the existence of
Gad’s order. This calls to the attention of unregenerate people
the possibilities of peace and tranquility in a key biblical in-
stitution, the family.!7 They see the outworking of God’s social
order in a familiar and universal institution.14

The success of neighborhood Bible studies in bringing millions
of people to Christ testifies to the importance of the household.
But few Christians seem to understand why home evangelism
works so well. They do not understand the theology of the house-
hold, that the household is an aspect of the kingdom of God.
The Hebrews were forced to come to grips with this aspect of
the kingdom because of Passover. The father became a priest
in the household who directed family worship and trained the
children through the ritual of the Passover (Ex. 12:26-28). The

father as household priest is not taught in churches today.

The problem we face today is the lack of skilled Bible
teachers for these neighborhood study programs. Few men
have the biblical knowledge and self-confidence necessary for
them to initiate a weekly Bible study in their homes. They ex-
pect church officers to lead such worship services. There are
not enough church officers to accomplish this.

The existence of the CBN satellite offers a solution to the
problem on a scale never before possible. Here is how it could
be used.

Videotaped Bible Studies

CBN creates a series of half-hour Bible study packages.
These would be specific and practical in nature. They could

17, James B. Jordan, “God's Hospitality and Holistic Evangelism,” The
Journal of Christian Reconstruction, VUL (Winter, 1981).

18. R. J. Rushdoony, Foundations of Social Order (Fairfax, VA: Thoburn
Press, [1968] 1978). See my analysis of this book in Preface 1
