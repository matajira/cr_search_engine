THE GHURGH AS A SHADOW GOVERNMENT $23

given so much immediate authority and power that it was un-
prepared to train civil magistrates to step into their rightful
positions. Although men sat on the throne, the church, as an
institution, was stil] in control of western civilization. A politi-
cal tyranny was replaced by an ecclesiastical one. By the time
of the Reformation, the princes of Europe supported the rise
of the reformers, not so much because they were committed to
reformation theology, but because they saw this as their op-
portunity to escape the hold of the Roman Catholic Church
on the state. Now the flow of history indicates that political
tyrannies threaten western civilization.

For better or for worse, therefore, the church can create or
abolish tyranny. In an age when western civilization stands on
the edge of destruction, the question is “how can the church
participate in the abolition of tyranny?”

Our position is the church can be the main instrument of
change. The weapons God has given her, make the church the
most potent army on earth. She is a parallel government—at
present she is a shadow government. The state no longer rules
for Christ. The church as a shadow government can make the
difference,

The Theology of the Kingdom

Ideas are at the base of everything. They have conse-
quences. For the church to function as a shadow government,
she must grasp the theology of the kingdom of God to under-
stand her role as a parallel government.

First, God’s Kingdom refers to both rule and realm.
Scholars have used the original language of the Bible to argue
that kingdom is either domain or dominion, but not both. *
An exegetical key found in the word glory, however, settles the
problem. Paul says to the church at Thessalonica, “That ye
would walk worthy of God, who hath called you unto his
kingdom and glory” (I Thess. 2:12)..5 It appears that the
Aposile is reiterating the coextensiveness of these two terms

14. S. Aslen, “‘Reign’ and ‘House’ in the Kingdom of God in the
Gospels,” Naw Testament Studies 98 (1962): 215-240. Compare another article
which differs by G. E. Ladd, “The Kingdorn of God—Reign or Realm,”
Journal of Biblical Literature 81 (1962): 230-248.

15, Paul S. Minear, Jmages of the Church in the New Testament (Philadelphia:
Westminster Press, 1977), pp. 119-135.
