TITHING 369

fairly be counted as foundation capital, and thus expenses
necessary to the production of the profit, should be paid be-
fore the tithe is calculated.

63. What about advertising? Advertising has two purposes:
to set forth a business before the eye of the market, and to ex-
pand the business to provide an increase in profit. The first
function is a necessary capital investment, and should be paid
before the tithe is calculated; but the latter is an expansion,
and should only be engaged in after the tithe has been paid.
What would be a fair way to calculate the difference? I suggest
the following. First, Mr. X (a businessman) should subtract
his advertising expenses from his gross income for the year,
Then he should work out the percentage of his capital ex-
penses over against his profit. Then, that percentage of his
gross income which went to necessary capital expenses is the
same percentage of his advertising which was necessary to
keep him at last year’s level, and should be deducted from his
net profit before he tithes.

‘Yo take a simple example: Christian Enterprises took in
$100,000 last year. During the year, CE spent $10,000 in
advertising. CE had $45,000 in capital expenses (rent, power,
raw materials), and thus $45,000 in profit. Thus, CE can
properly assume that of its $10,000 advertising, half was nec-
essary to get the money to pay for capital expenses, and half
contributed to profits, Thus, half of the $10,000 was a capital
expense, and so $5,000 should be added to overall capital ex-
penscs. This leaves profit at $50,000, so that a tithe of $5,000 is
owed to God before the business makes any moves to expand.

64, What about taxes? Clearly a man owes God His tithe
before he owes the state a tax. On the other hand, confiscatory
taxation, more than 10% of income, can be viewed as a plague
of locusts or as the damage caused by an invading army. (See
1 Samuel 8, where tyranny is expressed as a government
which takes 10% or more in taxes, thus making itself god.) In-
crease for the year can only be calculated in terms of what is
left after the locusts have damaged the crop. In terms of this,
it might be proper to consicler taxes as part of basic capital ex-
pénsés, rent paid to the invading army, and to pay the tithe on
what remains after taxes. I suggest, however, that a man in-
clude in his capital expenses only that amount of tax that goes
over 10% of his taxable income. In that way, his conscience
can be clear, for he is ascribing to tyranny only what tyranny
