H4 CHRISTIANITY AND CIVILIZATION

demonstrate in every area of thought and culture that there is
no common ground between believer and unbeliever, other than
the image of God in each man—an image twisted by sin. In
short, Christians must abandon the sinful and biblically illogical quest
Jor a common ground philosophy.® And to the extent that they are
successful in this endeavor, they will lose what little influence
and protection they have in today’s world.

In short, Prof. Van Til’s pessimistic eschatology has not
only colored his analysis of common grace, it has gutted it.
The world will not be blessed, in his view, by the progressive
dominion over all things by Christians. He does not believe in
the possibility of progressive dominion by Christians. Prof.
Van Til’s interpretation of the implications of Christianity’s
progressive abandonment of common-ground philosophy
Jeads directly to historical pessimism. The church will be
persecuted by the dominant humanistic order. The more we
Christians are successful in persuading unbelievers (as well as
other Christians) that there is no common ground between
their philosophies and Christianity, the more we will suffer
persecution. When we win intellectually, we will lose culturally.

The point should be, rather, that-as the humanists become
more consistent with their philosophies— philosophies
grounded in the presuppositions of chaos, randomness, and
chance —ihey will become increasingly impotent, not the
Christians. For example, the tax-supported public schools in
the United States are collapsing, not the parent-financed
Christian’ schools. The Bible teaches that Christians can
become victorious when they become faithful to God's law in
every area of life (Deut. 8; 28). Prof. Van Til has undercut the
power which his apologetic methodology can impart, for he
has adopted an eschatology of shipwreck.

But we need not be burdened with this weakness in Prof.
Van Tils system. His eschatological pessimism makes itself
felt only in his writings on common grace. Others have
adapted his basic methodology—the denial of common
ground philsophy —to more positive uses. A restructuring of

33. R. J. Rushdoony, “The Quest for Common Ground,” in Gary North
{ed.), Fhundations of Christian Scholarship: Essays in the Van Til Perspective
(Vallecito, CA: Ross House Books, 1976).

34. Gary North, “Common Grace, Eschatology, and Biblical Law,’ The
Journal of Christian Reconstruction, IT. (Winter, 1976-77); “Eschatologies of
Shipwreck,” Christian Reconstruction, II (Jan./Feb., 1979).
