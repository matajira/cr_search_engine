COMPUTER GUERRILLAS

A, Richard Immel

A best-selling author, his wife, and neighbors battle the U.S. Gov-
ernment to a standstill over a proposed timber cut that would denude
parts of their scenic little valiey in southern Oregon.

A national environmental organization blocks a $5-billion power
plant project and successfully prods utility regulators in five states to
reorder their priorities by putting conservation and alternative energy
sources ahead of new power plant construction.

A rural refugee from the city, irked by a speeding ticket from a
radar-wielding country cop, proves that the radars used by police
across the U.S, exceed microwave safety limits established hy the fed-
eral government.

Y themselves these events aren't particularly earth-shaking;

little guys have been rattling the establishment’s cage for
years, and once in a while they even win. But there’s another
common thread here that couldn’t have existed more than
four or five years ago: in all of these cases the little guys turned
to microcomputers to even the odds. None of them, in fact,
would have had a prayer without the new technology.

Author Richard Bach used his Apple II computer mainly as
a word processor in his fight with the Bureau of Land
Management in Oregon. “It was absolutely indispensable,” he
says. “We never could have written the things we had to write
without it.”

A homebrewed computer model of energy use was the only
feasible way for the activist group known as the Environmen-
tal Defense Fund (EDF) to stand up to the utility companies
and their voluminous and expensive mainframe computer
analyses. “The idea was to rub their faces in their own
numbers,” says Dan Kirshner, EDF's economic. analyst.

Copyright 1983 by A. Richard Immel. Originally published in the July
1983 issue of Popular Computing. (Published monthly by McGraw Hill, Inc.,
70 Main St., Peterborough, NH 03458, subscription price $11.97 per year.)

210
