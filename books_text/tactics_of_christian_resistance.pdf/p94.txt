48 CHRISTIANITY AND CIVILIZATION

Noah’s investiture with office. As a token of that investiture,
man is now permitted to cat the meat of the animals originally
slain to provide coverings (Gen. 9:2,3). The éovering with
skins had signified to Adam that someday investiture would
come; eating—~ingesting—the flesh of the animals signifies
that such an investiture is now taken into the life of man.18

Before the Flood, wild animals had hunted men and eaten
them; fierce dinosaurs had roamed the earth, signifying the
lifestyle of the Cainites over against the lifestyle of the
righteous, who were their prey. Now, however, man is given
power to hunt and eat the animals (Gen. 10:9), and they are
made afraid of man, This signified the ascendency of true
Godly men over the ungodly beastlike men.!4

Man was sinful from his youth (Gen. 8:21), and when that
youth had matured to full age, God had to destroy the world,
so corrupt had it become. Now, however, God institutes the
righteous civil authority to restrain evil, so that such a matura-
tion in corruption will never again take place. The youth will be cut
off, either in death or in circumcision, before he reaches full
age in evil. When Noah’s youngest son (Ham) attacks him,
Ham’s youngest son (Canaan) is cursed to become a slave,’
showing the ascendency of the saints over the wicked, of true
men over wild animals, and illustrating how the wickedness of
man’s youth would be restrained.

 

there was no state in the world at that time. This argument, however, would
also apply in the case of Noah, since there was not yet a slate in the world
then either, but only a family. Also, in the Bible the avenger of blood is the
next of kin, so there is some relationship between the family and the execu-
tion of the death penalty.

13, The period trom creation to the year alier the Flood, when the robe
owed, lasted 1657 years according to the chronology of the Bible.
ss is 33 jubilees of 30 years plus 7 years. 33+ 7 =40. This kind of
reasoning with numbers abounds in Genesis; cf. Cassuto, Genesis, vols. 1 &
TI, comments on Genesis 5 and IL.

14, The Bible tells us that someday the lion will lie down with the lamb,
and that the lion will eat straw like the ox (Is. 1:6,7; 65:25), ‘Phere is no rea-
son to believe that this will not physically come to pass. Foundationally,
though, animals are symbols of humanity, unc this signifies peace in che
social realm, Man’s robe was supposed to be vegetable (linen), not animal
(wool); but the death and ingestion of animals was introduced to signify that
man’s investiture would come through the death (shed blood) of a
Substitute. After the death and resurrection of Jesus Christ, the symbol of
progressive investiture and salvation returned from the animal realm
(sacrifices) to the vegetable realm (bread and wine).

  

  
    

    
 

 

 

    

 

 

     

 
