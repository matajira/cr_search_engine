136 CHRISTIANITY AND CIVILIZATION

pose God-defying laws on our fellow citizens, as Michael
Gistrap describes in his essay in this journal. Nullification by
colonial juries is what drove the British to despair in the years
preceding the American Revolution: They could not easily get
juries to convict colonial violators of the statutes of the British
Empire's bureaucracy, especially in matters regarding smug-
gling. Ii was so “bad” in the late seventeenth century, that
England had to set up Admiralty Courts in 1696 that alone
were empowered to try cases regarding smuggling violations.
These special courts did not allow trial by jury.*? The
American Act of 1764 expanded the authority of these courts
to all intercolonial trade, not just ships trading on the high
seas. Furthermore, the court where the trial was to be held was
in remote Halifax, Nova Scotia.*? Then came the revolution.
Theocracy is government by God’s law—not just in the
realrn of civil government, but afl government. It is not a wop-
down imposition of biblical law by an elite of priests, but, in
contrast, @ botiom-up imposition of biblical standards over every area
of life—areas not regulated by civil law for the most part—by
Christians who are morally responsible and legally em-
powered to make decisions. As the process of dominion ex-
tends the authority of Christians over more and more areas of
life, we will see the creation of a comprehensive theocracy.
A truly biblical theocracy will not come as the result of
some sort of “palace revolution,” but as a result of millions of
God-fearing people working to extend the reign of Christ over
every area of life. In all likelihood, the last imstitutions
brought under the rule of biblical law will be the central civil
governments of the world. (No, come to think of it, the central
civil government will be the next-to-last institution captured by
Christ; the last institution will be the tax-supported university.)
In short, we are arguing that the theocracy of secular
humanism now reigns in the West. What Christians must
recognize, and then learn to resist, is theocratic humanism. In
the United States, theocratic humanism is a system of rule by
a tiny minority of humanists over a vast majority of confused,
intellectually compromised, hesitant Christians. What we

 

52. Charles M. Andrews, The Colonial Period uf American History (4 vols.;
New Haven, Connecticut: Yale University Press, [1938] 1964), IV. ch.
VIII: “The Vice-Admiralty Courts.”

53. Bernard Knollenberg, Origins af the American Revolution, 1759-1766
{New York: Free Press, [1961] 1968), ch. 15.
