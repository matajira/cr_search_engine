THE NEW COMMUNITY 85

predominance. Scholarship is neither to be feared nor its
products given the exalted status of sacred texts. If the
wisdom of the first century was “foolish” and “doomed to pass
away” (1 Cor. I, 2), so is what passes for wisdom in the present
age. The New Testament writers were conscious that their
teaching was despised by the cultured Greeks of the dominant
civilization, The spirit of the age—any age—is always op-
posed to the spirit of Christ.

Tn their uncompromising determination to proclaim truth,
Christians must avoid the intellectual flabbiness of the larger
society. They must rally against the prevailing distrust of rea-
son and the exaltation of the irrational. Emotional self-
indulgence and irrationalities have always been the enemies
of the gospel, and the apostles warned their followers against
them (Col, 2:18).

Paramount among the difficulties in Christian thinking is
the dominance of anti-Christian assumptions in the “best” of
the surrounding culture. Those who think Christians can
easily use the world’s artifacts and methods in the creation of a
new synthesis underestimate the all-persuasiveness and
subtlety of alien and hostile influences. T. 5. Eliot, who was
much concerned about this problem, warned that “paganism
holds all the most valuable advertising space.” He feared that
as long as Christians were a tolerated minority, the un-
conscious pressures of intellectual conformity would more
gravely complicate their survival than would the plainly
perceived dangers of active persecution.*

Since ours is not so much a pagan (which is to say a pre-
Christian) society as it is a post-Christian one, the dangers are
all the more serious. The forces of idolatry do not urge us to
worship Zeus but rather use the language that for many cen-
turies has been associated with the Christian church. Pro-
found religious differences may on the surface appear trivial,
and one who points. to them runs the risk of being called a
hair-splitter. Just as an observer in the seventh century before
Ghrist would be hard-pressed to distinguish the altar of the
Lord from one dedicated to Baal, so we are faced with similar
confusion when devotees of the idol state use the language of
Christian compassion in their evangelistic mission.

4. T.8. Eliot, The fdea of a Christian Society and Notes Toward the Definition of
Culture (New York: Harcourt, Brace, Harvest Books, 1940), p. 18.
