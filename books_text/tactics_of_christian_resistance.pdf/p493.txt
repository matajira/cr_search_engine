TOOLS OF BIBLICAL RESISTANCE 447

Christ told us, “Occupy till I come.” Either we occupy, or
we will be occupied by His enemies.

Continued uninvolvement of the Christian community
could well put us in the position that some of our brothers
have already faced in the U.S. —run a pro-atheist “Christian”
school or go to jail. It would be a pity to have to fight the
American Revolution a second time. It would probably be
harder, because rather than a King with a funny accent living
several thousand miles away, the present situation is, as Pogo
put it, “I have seen the enemy, and he is us.”

It is an indictment of German Christians that they sur-
rendered their government to an occultist like Hitler. With
none of the higher powers ruled by Godly men, and with
Hitler’s Gestapo in complete control of the guns, Germans
could only practice civil disobedience and suffer the conse-
quences if caught. Harbor a Jew in one’s house and ride with
him to the concentration camp. When Israel rose up against a
Parliament that oppressed through law severed from Christ, it
was through higher powers (governments). Israel did not
rebel under the leadership of self-appointed “freedom
fighters,” nor did the Americans. The American Revolution,
as well as the restorations of Israel, were precisely that—
restorations. Restorations of government under the law, not
of men.

American Christians should welcome, and encourage,
such movements as the Sagebrush Rebellion. The “Sagebrush
Rebellion” is an effort by Western state legislatures to re-
establish state authority over public lands now controlled by
the Federal government. Since the movement has been lead
by duly constituted authorities —the state governments —the
Rebellion is proper. Movements such as the Sagebrush Rebel-
lion are blessed opportunities to encourage state and local
politicians to relearn their proper responsibilities and regain
authority they have surrendered to the Federal government.

By the same token, Christians should be in the forefront of
efforts to arnend the Constitution in order to limit the excesses
of Congress, the Supreme Court, and the Federal bureau-
cracy. Our efforts should also include support of legislation
designed to withdraw jurisdiction from the Supreme Court
under the powers of Article III Section I.

Biblical resistance to tyranny is not something that can be
put of by American Christians. First we must pray. Pray for
