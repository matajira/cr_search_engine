148 CHRISTIANITY AND CIVILIZATION

Orthodox Christians must deny, on principle, that the State grants
or establishes the tax-exempt status of the church of Jesus Christ. This
status derives from the sovereignty of the church under God.
The State is wise to acknowledge what God has established,
but the State has not created this tax-exempt status.

Tt is true, however, that the State has created the tax-
deductibie status of gifts to the church. This is different from
tax-exemption. Church members may well have to give up the
tax-deductible status of their gifts to the church. If the State un-
wisely revokes the tax-deductible status of gifts to a God-
fearing Christian church, then members are required to tithe
only the after-tax portion of their incomes. They did not pay
the tithe in Old Testament times on crops eaten by the locusts;
similarly, Christians are not required to pay a tithe on produc-
tion which is confiscated today by other kinds of locusts.'?

The churches of the West did not cry out against the
escalating welfare State, and so the most important single
potential source of opposition to the State was silenced in the
United States, from 1865 until about 1980. Only recently have
a minority of conservative and fundamentalist pastors begun
to sound the alarm, and only in the United States. Conservative
pastors allowed the State to escalate its war against personal freedom for

12. R. J. Rushdoony denies this principle in the book he co-authored
with Edward Powell (and, just for the record, which my tithe financed
through the Trinity Presbyterian Ghurch of Fairfax, Virginia). Rushdoony
concludes Tithing and Dominion (Vallecito, CA: Ross House Books, 1979),
with these words: “The law of the tithe, unlike the Sabbath law and works of
necessity, has no qualifications ar exceptions. God's tax must be paid.
Because God has prior claim on us, this tax is computed before the state
takes its tax, We cannot bring a blemished offering to the Lord: He calls this
evil (Mal. 1:8). Similarly, we cannot give anyone priority over God without
blemishing our approach to the Lord” (p. 143). It should be pointed out that
Ed Poweil the co-author of the book, categorically disagrees with Rush-
doony on this point and has written a privately circulated paper to that
effect. I agree here with Poweil. The tithe is on the increase a man ex-
periences; what is confiscated is not counted as part of the increase, Wealthy
people in many Northern European nations are taxed at rates of 98% or
even 102% of their income. Ave they evil for paying? Foolish, perhaps, but
not evil. The State simply steals their increase; they owe Gad no tithe. Mr.
Rushdoony has not thought through his position carefully. I do not disagree
with him publicly often, but here we must break with his argumentation.
We do not need to create unnecessary guilt for God-fearing people who tithe
on their after-tax increase in nations that do not permit charitable deduc-
tions from taxable income.
