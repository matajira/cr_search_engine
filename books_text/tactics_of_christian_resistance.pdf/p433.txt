TACTICS USED BY REFORMERS 387

the attacks upon the Calvinists. Men, women, and children
were murdered; the King himself stood in the window of the
palace with an arquebus and fired upon people from the
Louvre. The massacre lasted three days and thousands of
bodies were flung into. the Seine.

As news of this proceeding reached other towns and cities
of France, similar massacres were conducted. Lyon, Rouen,
Dieppe, Havre, and other places were drenched in blood.
Estimates vary from 50,000 upward; huge numbers in that
time. Arguments persist, as always, over the precision of the
statistics, but none is possible over the significance of the
event.

That made the Calvinists a minority in France for genera-
tions to come, and set the stage for Louis XIV. The
Renaissance had won a victory, though it was the victory of a
stage, a period.

The Calvinists fled France in great numbers: some to
England, some to Switzerland, some to Germany, some to
other parts. In England, Elizabeth and the Cecils shook with
anger; St. Bartholomew’s ended Catholic chances in England.
Yet the Vatican rejoiced, and the Machiavellian school ap-
proved of a final stroke that ended, it seemed, disorders
among the French.

The Calvinists of the Low Countries, however, took the
lesson to heart. The realization that there would be no mercy
and no reconciliation led to eighty years of struggle against
Spain, and the loss to that empire of Holland. The Calvinists,
in other words, did not lose heart. The movement proceeded.

Renaissance or Reformation?

One of the reasons for a resurgence of interest in this
period and these events has been the steady espansion of
despotic anti-Christian power on the part of governments in
our own century and in these times. Scholars have begun to
look, once again, into the rise of Calvinism and the manner in
which it spread across Northen Europe to alter our civilization.

In the view of contemporary historian Robert M.
Kingdon, Calvin and Protestantism achieved a genuine
revolution by overthrowing a ruling class: the Roman Catholic
clergy. That class represented a widespread international sys-
tem using a special elite language, with its own courts and
