APOLOGETICS AND STRATEGY M7

in the minds of fundamental Christians, neither of us would
have brought any of this up. We both kept our opinions out of
print until now. But the circumstances of the day no longer
permit silence.

We all want to find allies wherever we can, but we must
recognize deep-seated flaws in strategies and tacties recom-
mended in the name of Christ by people who, in this particu-
lar area, do not adhere to a consistent theology of Christian
resistance. If we refuse to face the theological and tactical
differences that divide us, “before the shooting starts,” we may
not have time to think through what needs to be thought
through when the crises escalate. That is what issues 2 and 3
of Christianity and Civilization are all about.

When lives are literally at stake--indeed, when the sur-
vival of Western civilization is literally at stake—we dare not
pussyfoot around the hot issues. If generals who are responsi-
ble for devising a military strategy prior to the declaration of
war are afraid of discussing openly the nature of each strategy
and the reasons for each, then as military leaders, they are not
fit to exercise command. If concern for “damaged egos in the
general staff” is considered more important than the strength-
ening of the army’s strategy, then the survival of those in the
army is called into question. If we are at war with the philoso-
phy and institutions of humanism—and it is our contention
that we are—then we have to get our offensive and defensive
strategies discussed and agreed to before we commit our
capital and even our lives. The stakes are just too high.

Christians tend to rip each other up. Sometimes this is
necessary, if someone is selling out the faith in a key area.
Paul, after all, publicly confronted Peter when Peter
capitulated to the Judaisers (Gal. 2:11-14). Usually, attacks by
Christians on other Christians are ego-inspired or “turf-
protecting” exercises. What is needed in an era in which
Christians do not agree on the fundamentals of the faith, let
alone strategy and tactics—and we are living in such an
era—is open disagreement, in order to facilitate cooperation in those
areas where there is agreement. We should not cooperaie with
those who are undercutting the defense of the faith in those
areas where they are undercutting it. Thus, we have to get our
disagreements into the open, not to destroy our cooperative
efforts, but in order to increase cooperation in those areas
where we are agreed. We need to define and thereby limit our
