EDITOR’S INTRODUCTION xv

under God rather than under some civil government?

The Question of Sovereignty (Legal Jurisdiction)

The question of sovereignty, civil government vs. church
government, has confronted Christians in every major period
of Western history, but especially during periods of rapid
political change. It was Christ vs. Caesar in the early church,
the Pope vs, the Emperor in the middie ages, the Roman
Church vs. the princes in Luther’s day, and the British Parlia-
ment and the Anglican Church vs. both the independent and
established state churches in the American colonies in 1775.

Few Americans are aware that it was not even legal for
American colonists to print Bibles in the colonies; that was a
monopoly granted to British printers, most notably Oxford
and Cambridge Universities. The restriction applied only to
unannotated versions of the King James Bible, but that was
sufficient. The annotated ones were too elaborate and expen-
sive to print in the colonies. Prior to the American Revolu-
tion, the only Bibles ever legally printed in the colonies—one
may have been produced illegally in Boston in 1750—were
John Eliot’s 1663 translation into the Algonquin Indian lan-
guage and a mid-eighteenth century German language Bible.
During the Revolutionary War, Bibles became extremely
scarce. One of the “acts of rebellion” by the Continental Con-
gress in 1781 was to authorize the printing of 10,000 copies of
the Bible, which Robert Aitken printed at his own expense the
next year.®

There can be no escape from the question of sovereignty. Few evan-
gelical church leaders in the West’s industrial nations
recognize the nature of the confrontation, but these in Marx-
ist and Islamic nations do. The intellectual and religious bat-
tles between humanists and Christians have heated up only in
recent years. High-risk confrontations have been limited in
number. The publicized battles generally have been confined
to a barrage of words. It is not surprising to Jearn that

6. Rosalie J. Slater, Teaching and Learning America’s Christian History (San
Francisco: Foundation for American Christian Education, 1975), pp. 339ff,
C£ Robert R. Deardon, Jr. and Douglas 8. Watson, The Bible of the Revolu-
tion (San Francisco, 1930). A facsimile copy of this Bible was printed by the
Amo Press, New York, in 1968. The introductory essay includes a one-page
bibliography about the Aitken Bible.
