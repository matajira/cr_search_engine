EDITOR’S INTRODUCTION xili

Assume that you are a member of the board of a small
church. Your state has passed a law which allows local coun-
ties to impose property taxes on all church properties that are
not specifically set apart for exclusively religious activities.
Officials of the county government have written to your
church to inform you that they intend to enter the premises of
your church and make an assessment of just which property in
your church is devoted exclusively to worship, and what prop-
erty is used also for “non-worship” activities and is therefore
subject to the tax. You know that most of the mainline
denominations are going to cooperate with the local officials,
and they have decided to pay any taxes levied by the county,
just to avoid trouble and expensive lawsuits. Will you recom-
mend to other members of the church’s board to allow the tax
assessors in to make their assessment, and then pay the tax
bill when it is sent?

These are not hypothetical cases. I know a Dutchman
whose family did hide a Jewish gentlemen from local authori-
ties for several years. They were nearly driven crazy by the
man, who kept getting bored and would insist on going shop-
ping, or even getting involved in municipal work projects. As
for the Negro lady, her name was Rosa Parks, and she refused
to move from her seat on December 1, 1956. She was arrested,
The Negro community in Montgomery then organized a
boycott of the municipal bus system, which was an illegal act.
The boycott was successful economically; the bus company
lost a fortune. Negroes returned only eleven months later,
when the United States Supreme Court declared the
segregated seating law unconstitutional. The man who led the
protest was Rev. Martin Luther King, Jr. The protest launched
his career.

Finally, the problem of property taxes. A model law sub-
jecting church property taxation is on the books in most states
in the United States. It has been quietly passed by
legislatures, and it has received little or no attention from the
press or the churches. It is now being enforced in California.
Its success or failure there will probably be used as a prece-
dent. Other counties around the nation are waiting for the
political go-ahead signal. Churches are now declared to be
under the jurisdiction of the civil government, and the State
now asserts its power to destroy the churches, for “the power
to tax involves the power to destroy,” as Supreme Court Chief
