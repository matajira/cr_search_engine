The Catechism of the Naw Age 9

characters, magical flight, fireballs, divination, and even restoring the
dead.

Many of the spells, incantations, symbols, and protective measures
are genuine occultic techniques. Several spells, for example, instruct
the player to draw a protective circle when communicating with de-
mons, a practice used by real witches. Spells often require human
blood or flesh. The spell for a Cacodemon (Conjuration) suggests that
“By tribute of fresh human blood and the promise of one or more
human sacrifices, the summoner can bargain with the demon for
willing service.” Another spell, smacking of Biblical references, gives
instructions for changing sticks into snakes and back again.

The Dungeon Master also has a wide variety of monsters and
demons to populate his dungeon. Many of these are drawn from
Satanism. One monster, the Mane, is described as a sub-demon who
has gone to the “666 layers of the demonic abyss.” The most evil of
them “are confined in tiers of flames of Gehenna.” Baalzebub is listed
as an archdemon. There are hell hounds, able to “breathe out a
scorching fire on an opponent up to a one inch distance.” The Rakshasa
is described as an “evil spirit encased in flesh” who enjoys a dict of
human flesh and has the powers of ESP and illusion. At the very least,
anyone familiar with FRP rule books is learning the terminology of
witchcraft and Satanism. More likely, a seed is sown and he is almost
imperceptibly drawn into the occult.

Of course, the creators of FRPs deny that they are promoting
witchcraft. Most claim that they don’t believe in the stuff anyway, And
besides, they insist, the characters in the game are fighting against these
demons. Even some Christians have defended the game because of its
realistic depiction of evil. But, remember that it’s not just the monsters who
have Satanic powers. The heroes, the “good guys” also use magic and witchcraft
and learn genuine spells and occult techniques, Regardless of the intentions
of the creators of FRPs, Dr. Gary North’s summary is still accurate:

Without any doubt in my mind, after years of study in the
history of occultism, after having researched a book on the topic,
and after having consulted with scholars in the field of historical
research, ¥ can say with confidence: these games are the most
effective, most magnificently packaged, most profitably marketed,
most thoroughly researched introduction to the occult in man’s
recorded history. Period.

According to the dictionary, a catechism is “a program of instruction
containing a summary of the principles or an introduction to the
