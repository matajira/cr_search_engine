16 A Christian Response to Dungeons and Dragons

footsteps. That is the essence of discipleship. And that is the essence
of parenting: teaching by doing.

Third, as parents we must explain our rules to our children. It is not
enough to say “no.” Our children need to know why we’ve said “no.”
If we don’t want to let them watch some of the Saturday morning
cartoons that “aif the other kids” watch, then we ought to offer a
reasonable and Scriptural explanation. Perhaps we can even turn the
programs on for a few minutes every once in a while, pointing out the
problems to our children and providing them with the basic tools
they'll need to exercise discernment themselves. Don’t just say “no”
to Dungeons and Dragons, or anything else, We must equip our children
so that ¢hey can say “no” without us having to say a thing.

Fourth, as parents we need to provide the right kind of entertain-
ment for our families. Good books should always be available. And
they should be read. Why not read a chapter of C. S, Lewis’ Narnia
series before bed every night? Or maybe G. K. Chesterton’s Father
Brown series? Or even C. H. Spurgeon’s John Plowman series? What
about J. R. R. Tolkein, or George McDonald, or Dorothy Sayers, or
Jobn Bunyan, or William Shakespeare, or John Milton, or Geoffry
Chaucer? But don’t stop with books. Fill your home with good music,
beautiful art, challenging crafts, and lovely plants. Every one of us has
limitations — differing gifts, skills, talents, and financial resources — but
all of us can use what we do have creatively and wisely. Paperbacks,
cassettes and posters are very inexpensive, and anyone can transform a
pile of construction paper, glue, scissors, glitter, and crayons into a
rollicking good time.

Conclusion

Dungeons and Dragons is a dangerous game. It serves as an introduc-
tion to evil, a catechism of occultism, a primer for the ABCs of the New
Age. It is a recruiting tool of Satan. It can alter the daily behavior of
regular players. It stimulates the seamier side of our imaginations, It
is an enormously attractive and effective escape for people frustrated
with life. Yor many it becomes pure, obsessive fantasy, in its most
destructive form. It is no longer a game, an imaginative diversion, but
a substitute universe in which the player pretends to be his own God
and to make his own rules. D&D is not the only problem among young
people. The statistics on teenage suicide, however misinterpreted in
the major media, are indicative of widespread dissatisfaction and
disillusionment among our youth. This is the larger problem that must
be dealt with. And this problem is often exacerbated by D&D.
