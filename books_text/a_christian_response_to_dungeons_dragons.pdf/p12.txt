8 A Christian Response to Dungeons and Dragons

flaws, A lot of things are unfair. When I am in my world, T control
my own world order, I can picture it all. Phe groves and trees.
The beauty. I can hear the wind. The world isn’t like that... .
It’s hazardous... . The more time you spend in your fantasy
world, the more you want to walk away from the burdensome
decisions in life... . The more play D@D, the more T want to
get away from this world.

 

No doubt, D&D did not create John’s disillusionment, bat it
provided an outlet for the full expression of his autonomy and rebel-
lion. In a very dramatic way, D&D reinforced John’s hatred for life
as ordered and given by God.

Obsessive escapism of this sort is virtually equivalent to schizo-
phrenia. One psychologist warned that adolescents may become emo-
tionally and mentally disturbed by obsessive participation in FRPs.
“The greatest danger I sec is the escape from reality. These kids take
their own unacceptable impulses and put them into fantasy. Schizo-
phrenia is a thought disorder and D&D confuses the way people think.
Thoughts not based on reality are dangerous.” One enthusiastic player
admitted that D@D had made him a virtual schizophrenic. He said
he would probably cry if the character he has been directing for three
years were to die.

To this point, D&D might appear to be only a more graphic form
of earlier war games. But the method of warfare in D@D is entirely
different. Magic, witchcraft, casting spells — these are the most desir-
able and powerful weapons in FRPs. In fact, it is the occultic element
that makes FRPs what they are; without them, they would be gro-
tesque but essentially run-of-the-mill war games, It is the occultic
element that gives players of D@D and its imitators the sense of
awesome power, It is the occultic element that leads to a manipulative
attitude. And it is the occultic element that makes the game so very
dangerous. Magic and witchcraft are found on nearly every page of
an FRP rule book. The list of characters from which a player chooses
includes magic users, druids, illusionists, and clerics. There are two
major types of spells described: magical and clerical. The characters
with magical powers are the most powerful players in the game; the
other characters rely on physical strength and savagery. The Dungeon
Master’s Guide includes several pages of instruction on acquiring and
casting spells. There are directions for chanting, the usc of familiar
spirits, speaking with the dead, uses of occultic symbols to protect the
spell caster, and definitions of special spells used by shamans and
witchdoctors. Spells are given for healing, exorcism, charming other
