The Catechism of the New Age 15

So, what do we say to children who play with toy guns and plastic
soldiers? Is not this a role-playing activity that can. lead to violent
behavior? It certainly can, if the child takes his play role so seriously
that he begins to buily children in, say, Sunday school class. But war
is, Biblically speaking, a legitimate, though undesirable, use of vio-
lence. By learning to play war, a child is not necessarily learning to
do anything that God forbids. Indeed, such play can be helpful to
instill in a child the reality of the Christian’s life-long warfare against
Satan and sin.

Or what about playing evil characters in drama? May a Christian
legitimately play Iago? Mephistopheles? Ted Bundy? Charles Man-
son? Clearly, the character’s place in the entire context of the work is
the decisive factor. If a play celebrates the perversion of its character,
there can be little justification for a Christian playing the role, And,
there is the factor of identification with the role. Lee J. Cobb was so
intensely involved in his portrayal of Willy Loman that he began to
contemplate suicide. This is clearly a sinful level of identification.

In summary, Scripture encourages leisure, play, and even role-
playing, though always within the limits of the moral Law. In the
context of these standards, however, our imaginations find true free-
dom. Like the sheep to which Scripture so often compares us, our freest
play is within the fold. Outside, there is only the bondage of fear that
allows for no real leisure.

Practical Matters

Okay. So now, what’s a parent to do? How can we both protect
our children from the evil effects of Dungeons and Dragons and teach
them a healthy Biblical perspective of play?

First, as parents we must make certain that our families begin to
build a spiritual life together. Simply going to Church on Sundays is
not enough. A family needs to pray together, study the Bible together,
and celebrate the seasons together. Our children need to see us modeling
a tully orbed comprehensive Christian walk that touches every area of
life, And they must be invited to walk that walk with us.

Second, as parents we must make certain that our families have
plenty of fun together. By watching us have fun, our children learn how
to have good, wholesome fun. By watching our friendships in action,
they learn how to make godly friendships. By watching us play, they
see what righteous recreation is like. And by including our children in
our fun, our friendships, and our play, we beckon them to walk in our
