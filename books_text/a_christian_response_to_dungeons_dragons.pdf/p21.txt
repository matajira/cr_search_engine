The Catechism of the New Age 7

Games reflect, as well as reinforce, the faith-consensus of a culture.
As such, D@D is the perfect game for the New Age 80s, providing the
self-indulgent escapism of drugs without the harmful physical effects.
You can get a kind of hallucinogenic high and still make it to the health
club for your workout. Taking a wider view, we might say that FRPs
take modernity to its logical conclusion. Since Descartes, modern man
has retreated from the bright light of God’s creation into the dark
world of his own mind and imagination. Though his world is a dun-
geon populated by dragons, demons, and monsters, he says with
Milton’s Satan, “Better to rule in hell than to serve in heaven”!

Thankfully, as parents we can provide our children with whole-
some alternatives. We can protect them from the strangely magnetic
allure of the dungeon. We can win the war for their hearts and minds
and souls.

Ifonly we would.
