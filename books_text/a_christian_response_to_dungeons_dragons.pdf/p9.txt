The Catechism of the Nav Age 5

Since 1980, D&D and its imitators have entered the mainstream
of American life. Public schools use D&D to teach reading and math
skills. Highly intelligent students are particularly attracted by the
complexity and excitement, and D&D is often used in classes of “gifted
children.” In Herber City, Utah, parental objections to D@D in the
public school pressured the school board to discontinue its use. A Boy
Scout post in Chattanooga uses D&D, and the game has been adver-
tised in Boys Life, the official Scout magazine. Some Catholic schools
are using D&D. Public libraries open their doors for training and
game sessions.

The Moral Dilemma

So, why all the fuss? D@D sounds like a challenging and exciting
new way to spend an evening. It must be educational; otherwise they
wouldn’t use it in public schools, right? Even the Boy Scouts use it. In
the face of all this support, it seems almost un-American to suggest
there are serious deficiencies with the game.

But there are serious defects. Very serious. In spite of the favorable
financial results of the Egbert case, the cerie course of events cast a
shadow over the entire industry. Remember the “Freeway Killer”
Vernon Butts, who committed suicide in his jail cell in 198] while
being held as a suspect in a string of murders? Butts was an avid D&D
player, who often communicated with visitors using a code developed
as a part of his D@D involvement. Another isolated incident? Not
hardly. Police reports around the country have connected FRP activity
to more than a hundred suicide and murder cases.

Of course, not everyone who plays the game becomes suicidal or
homicidal. Still, there are an awful lot of unsettling things going on
here. One of the chief defenses of FRPs is that they stimulate the,
imagination. This is undeniably true. The question is whether we
want our imagination (or that of our children) stimulated in this
particular way. Consider, for example, the level of violence and crime
in a typical D@?D game. One psychologist wrote, “There is hardly a
game in which the players do not indulge in murder, arson, torture,
rape, or highway robbery.” And he tikes the game! The Dungeon Mas-
ter’s Guide lists Hitler among those historical characters who exhibited
true D&D charisma.

It gets worse, because the violence in D&D is generally less

graphic than in some of its imitators. Consider these characters from
The Monster Manual:
