xxii HE SHALL HAVE DOMINION

position. In the late nineteenth century, the Baptist Calvinist
Charles Haddon Spurgeon was a well-known historic premillen-
nialist, although his language was often very optimistic with
respect to the spread of the gospel, and he believed in the
familiar postmillennial doctrine of the future conversion of the
Jews. He did not have much use for millennial theories. “I am
not now going into millennial theories, or into any speculation
as to dates. I do not know anything about such things, and I
am not sure that I am called to spend my time in such re-
searches. I am rather called to minister the gospel than to open
prophecy.”* In our day, the most famous American historic
premillennialist has been the Calvinist Presbyterian author,
Francis Schaeffer, although he rarely wrote about his Calvinism,
his Presbyterianism, or his premillennialism. (It does present a
problem for historic premillennialists when their most famous
representatives prefer not to write about eschatology.)

Historic premillennialists can appeal to recent books by
George Eldon Ladd. But I am aware of no book that discusses
the premillennial view of the era of the Church prior to Christ’s
return to earth to set up His kingdom, ie., no book on the
premillennial philosophy of history. The focus of all historic
premillennial works is on the Second Coming: the great future
discontinuity that supposedly will inaugurate the judicially
visible phase of Christ’s kingdom in history, when Jesus will
reign in person to rule on earth. Only then does the idea of
Christian civilization become significant in historic premillen-
nialism. Christendom is ignored until after the Second Coming.

Even with respect to this future era, there is never any de-
tailed discussion of ethical cause and effect in history, ie., a
biblical philosophy of history. There is no detailed discussion of
how Jesus Christ will rule on earth through His people. Will
there still be politics? Will government be entirely bureaucratic?

8. Charles Haddon Spurgeon, “The Restoration and Conversion of the Jews”
(June 16, 1864}, Sermon No. 582, Metropolitan Tabernacle Pulpit, vol. 10 (1864) p. 429.
