338 HE SHALL HAVE DOMINION

Great Tribulation is denied by all premillennialists and by many
amillennialists.* In the next few pages, I will sketch the general
argument for a preterist understanding of the Great Tribula-
tion. My focus will be on its presentation in Matthew 24 (which
was briefly mentioned in Chapter 8).

Matthew 24: Interpretations

Matthew 24 is held by some to be contra-indicative to post-
millennialism: “Postmillenarians have a different problem in
that they want to support their view that the world is going to
get better and better as the Gospel gradually triumphs; but this
passage of Scripture does not support this and, in fact, predicts
increasing evil with the climax at the Second Coming.”* Post-
millennialism “stands in sharp contrast with that whole body of
biblical data which describes the days prior to the coming of
Christ as days in which lawlessness abounds (Matthew 24:12)”
and “Matthew 24 itself is strong proof of all this.”®

Due to the prominence afforded the Great ‘Tribulation in
popular study, I have allotted more space to it than to the
other features to be considered. It is the key to the preterist
interpretation. If the Great Tribulation refers to the fall of Jerusalem
in A.D. 70, all “futurist” interpretations collapse. So does the pessi-
mism created by an eschatology of predestined defeat.

3. Amillennialists Anthony Hoekema (The Bible and the Radure [Grand Rapids:
Eerdmans, 1979], pp. 112-117, 178) and Herman Ridderbos (The Coming of the
Kingdom (Philadelphia: Presbyterian & Reformed, 1962], pp. 498ff) deny the preterist
view, Amillennialists Jay Adams (The Time Is af Hand [n.p.: Presbyterian & Reformed,
1966], Appendix B) and George Murray (Millennial Studies: A Search for Trath [Grand
Rapids: Baker, 1948], pp. 110ff) affirm it.

4. John F Walvoord, Prophecy Knowledge Handbook (Wheaton, IL: Victor, 1990),
p. 381.

5. Herman Hanko, “The Illusory Hope of Postmillennialism,” Standard Bearer
66:7 (Jan. 1, 1990) 158. Hanko, “An Exegetical Refutation of Postmillennialism”
(unpublished conference paper: South Holland, IL: South Holland Protestant
Reformed Church, 1978), p. 27.
