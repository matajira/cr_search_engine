Concluding Remarks 503

the salvation of sinners (Ezek. 18:23; Luke 15:10). The gospel
is nothing less than “the power of God unto salvation” (Rom.
1:16; 1 Gor. 1:18, 24).

The binding of Satan was effected in principle (i.e., defiini-
tively) through the ministry of Christ in history (Matt. 12:28-
29), thus casting him down from his dominance (John 12:31;
Luke 17:10) on the basis of Christ’s redemptive labor (Col.
3:15). Christians may resist the devil, causing him to flee (Jms.
4:7); they may even crush him beneath their feet (Rom. 16:20)
because “greater is he that is in you, than he that is in the
world” (1 John 4:4).

Conclusion

In accordance with the plan of God and under His almighty
hand, Christianity is destined to overwhelm the world so that
“the earth shall be full of the knowledge of the Lorn as the
waters cover the sea” (Isa. 11:9). There is coming a day when
virtually all men and nations will bow before the Lord in hum-
ble worship, offering up the labor of their hands and the glory
of their kingdoms to Him Who is “the King of kings and Lord
of lords” (Rev. 17:14; 19:16).

Postmillennialism expects the expansion of Christ’s right-
eousness throughout the earth. According to the clear teaching
of Christ (Matt. 5:17-20) and the New Testament (e.g., Rom.
3:31; 7:12; I John 2:3-4), His righteousness is defined by God's
law. Hence, consistent, biblically based postmillennialism is
necessarily theonomic. Theonomic ethics and postmillennial eschatol-
ogy are two sides of the same coin.

The glorious message of Scripture - in both Old and New
Testaments — is that “every knee shall bow to Me, and every
tongue shall confess to God” (Rom. 14:11). Paul confidently
asserts that

Christ has indeed been raised from the dead, the firstfruits of
those who have fallen asleep. For since death came through a
