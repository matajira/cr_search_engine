Time Frames 321

gression (v. 24) with the cutting off of Messiah (v. 26a) — results
in God’s act of reserving Israel’s sin until later. Israel’s judgment
will not be postponed forever; it will come after the expiration
of the seventy weeks. This explains the “very indefinite”®
phrase “till the end of the war”: the “end” will not occur during the
seventy weeks. That prophesied end occurred in A.D. 70, exactly
as Christ had made abundantly clear in Matthew 24:15.

The Dispensational Interpretation
The Gap in the Seventy Weeks

Dispensationalism incorporates a gap or parenthesis between
the sixty-ninth and seventieth weeks. This gap spans the entire-
ty of the Church Age from the Triumphal Entry to the Rap-
ture.** The dispensational arguments for a gap of undeter-
mined length between the sixty-ninth and seventieth weeks are
not convincing. Let us consider a few of their leading argu-
ments for a gap.

First, the peculiar phraseology in Daniel: Daniel places the cut-
ting off of the Messiah “after the 62 ‘sevens,’ not in the 70th
‘seven.’ “8” This is so stated to allow for a gap between the
sixty-ninth and seventieth-weeks. If the cutting off did not
occur during the sixty-ninth week or during the seventieth
week, there must be a gap in between wherein it does occur.

In response, it is obvious that seventy occurs after sixty-nine,
and thus fits the requirements of the prophecy. Consequently,

35. Allis, Prophecy and the Church, p. 115.

36. Walvoord, Prophecy Knowledge Handbook, pp. 256-257. Ryrie, Basic Theology,
p. 465. Pentecost, “Daniel,” BKC, 1:161. Walvoord, Daniel, pp. 230-231. It is interest-
ing to note that the early Fathers held to a non-eschatological interpretation of the
Seventieth Week, applying it either to the ministry of Christ or to A.D. 70. See:
Barnabas, Epistles 16:6; Clement of Alexandria, Miscellanies 1:125-26; Tertullian, An
Answer to the Jews 8; Julius Africanus, Chronology 50. See: L, E, Knowles, “The Inter-
pretation of the Seventy Weeks of Daniel in the Early Fathers,” Westminster Theological
Journal 7 (1945) 136-160.

37, Pentecost, “Daniel,” BKC, p. 1364. See: Walvoord, Rapture Question, p. 25.
