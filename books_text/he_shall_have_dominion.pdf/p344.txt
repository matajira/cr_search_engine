298 HE SHALL HAVE DOMINION

belled against God’s eternal Law, both humans and demons. As
such, hell is a place of conscious torment, as the Scripture dem-
onstrates (Luke 16:23; Rev. 14:11). This torment is of endless
duration,” though the degree of this torment is proportioned
according to the extent of one’s rebellion.” Its horrible nature
is directly due to the withdrawal of the presence of God (Matt.
25:41, contra. v. 34).

Interestingly, despite the revulsion of some to the doctrine,
“the strongest support of the doctrine of Endless Punishment is
the teaching of Christ, the Redeemer of man.”* Due to the
same language of eternal duration being applied to hell as to
heaven (e.g., Matt. 25:46), “We must cither admit the endless
misery of Hell, or give up the endless happiness of Heaven.”
The classic study on hell is W. G. T. Shedd’s, The Doctrine of
Endless Punishment (1886). A helpful recent defense of the doc-
trine is John H. Gerstner’s Repent or Perish (1990).

New Earth

Though not agreed on by all postmillennialists, there is
ample evidence suggestive of a refashioning of the earth for the
eternal abode of the saints. The teaching of Scripture is often

93. Matt. 10:28; 13:41-42, 49-50; 18:8; 23:16, 23; 25:32-33, 41, 46; Mark 9:43-
48; Luke 3:17; 16:22-23; Jude 7; Heb. 6:2; Rev. 14:10-11; 20:10). See also: John
5:29; Jude 13, Although many orthodox scholars, while holding to the grievous,
conscious misery involved, doubt whether literal flames of fire are used in the
torment of the wicked. See: A. A. Hodge, Outlines of Theology (Fdinburgh: Banner of
Truth, [1878] n.d.), p. 580. Dabney, Systematic Theology, pp. 853-854.

94. Mart. 10:15; Luke 12:48. This is also implied in there being books of judg-
ment, Rev. 20,

95. Shedd, Dogmatic Theology, 2:675. See: C. 8. Lewis, The Problem of Pain (New
York: Macmillan, 1944), Cf Buis, “Hell,” ZPEB, 3:114.

96, Moses Stuart, Several Words Relating to Eternal Punishment (Philadelphia:
Presbyterian Publishing Committee, n.d.), p. 89.

97. See also: Robert Morey’s Death and the Afterlife (Minneapolis, MN: Bethany
House, 1984), which is a refutation of Fudge, Fire That Consumes. Roger Nicole, “The
Punishment of the Wicked,” Christianity Today (June 9, 1958). J. I. Packer, “The
Problem of Eternal Punishment,” Crux 26 (Sept. 1990) 18-25.
