536 HE SHALL HAVE DOMINION

glory?” (Luke 24:26). “He predicted the sufferings of Christ
and the glories that would follow” (1 Pet. 1:11b). Christ no
longer suffered on earth after the resurrection. His exaltation
glory began with His bodily resurrection, in time and on earth.
This is the divine pattern for His Church, as well.

Paul speaks of his own patient endurance of persecutional
suffering in a context that expects earthly victory. He presents it
as an example for Timothy to follow: “You, however, know all
about my teaching, my way of life, my purpose, faith, patience,
love, endurance, persecutions, sufferings — what kinds of things
happened to me in Antioch, Iconium and Lystra, the persecu-
tions I endured. Yet the Lord rescued me from all of them. In
fact, everyone who wants to live a godly life in Christ Jesus will
be persecuted” (2 Tim. 3:10-12). Preceding this statement, Tim-
othy was assured by Paul of the failure of the evil men and
impostors of history, who often are the instruments of suffering:
“But they will not get very far because, as in the case of those
men, their folly will be clear to everyone” (2 Tim. 3:9).

Second Timothy 3:12 is more of an ethical statement than a
prophetic one: “In fact, everyone who wants to live a godly life
in Christ Jesus will be persecuted.” That is, given the historical
setting, the godly may expect their godliness to stir up persecu-
tion. It is not the case, however, that this must always and
everywhere be the case. Paul is not establishing a universal
principle. The premillennialist and dispensationalist surely do
not believe that in the millennium this will be true. Neither
does the amillennialist believe such will be the case for those
who live godly lives in the New Heavens and the New Earth.
Paul is instructing Timothy what he is to expect, while living
among an ungodly majority. He is also instructed that the ungodly
will be exposed eventually (2 Tim. 3:9).

Back to the Old Testament

The ultimate outcome of the long period of suffering which
the Church endures is destined to be historically glorious. This
