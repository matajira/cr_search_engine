AUTHOR INDEX

Aalders, G. Charles, 194

Adams, Jay E., 53, 58, 285,
365, 427, 430, 444

Alexander, J. A., 202, 276

Alford, Henry, 64

Allis, Oswald Thompson,
26, 34-35, 55, 66-69,
234, 310, 351

Aristotle, 8

Arminianism, 447

Augustine, 9

Aurelius, Marcus, 9

Bahnsen, Greg L., 44, 71,
103, 331, 464

Barbieri, Louis A., 343, 474

Barclay, William, 296

Barnes, Albert, 326

Barnhouse, Donald Grey,
61

Beasley-Murray, G. R., 353

Berkhof, Hendrikus, 22

Berkhof, Louis, 3, 5, 53, 58,
74, 492

Berkouwer, Gerrit C., 51,
294

Blackstone, William E.
(WEB), 61, 474, 487

Boettner, Loraine, 35, 44,
203, 253, 334

Boyd, Alan Patrick, 58, 74-
77

Boyer, James, 285

Brightman, Thomas, 78, 90

Brown, David, 72, 233, 234,
252, 332, 451, 484

Bruce, A. B., 261

Calvin, John, 88-89, 225,
244
Campbell, Roderick, 26, 35,
253
Chafer, Lewis Sperry
Holy Spirit restrains, 384
non-dispensationalism,
52
postmillennialism, 32,
42, 426
rapture, 278
Warfield, 66
Chilton, David, 44, 104, 451
Clark, David S., 34
Clarke, Adam, 253
Clouse, Robert G., 54, 78,
333-34
Clowney, Edmund, 359ff
