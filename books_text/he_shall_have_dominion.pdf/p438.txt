392 HE SHALL HAVE DOMINION

will be revealed, whom the Lord will consume with the breath
of His mouth and destroy with the brightness of His coming.
The coming of the lawless one is according to the working of
Satan, with all power, signs, and lying wonders.”™ As indicat-
ed, the lawless one was eventually openly revealed. The mystery
form of his character gave way to a revelation of his lawlessness
in Nero’s wicked acts. This occurred after the restrainer [Claud-
ius] was “taken out of the way,” allowing Nero the public stage
upon which he could act out his horrendous lawlessness.

In that judgment-coming against Jerusalem, there is also
judgment for the Man of Lawlessness, Nero. There is hope and
comfort in the promised relief from the opposition of the Jews
and Nero (2 Thess. 2:15-17). Not only was Jerusalem destroyed
within twenty years, but Nero himself died a violent death in
the midst of the Jewish War (June 8, A.D. 68). His death, then,
would occur in the Day of the Lord in conjunction with the
judgment-coming of Christ. He would be destroyed by the
breath of Christ, much like Assyria was destroyed with the
coming and breath of the Lorp in the Old Testament (Isa.
30:27-31) and like Israel was crushed by Babylon (Mic. 1:3-5).

Conclusion

There are a number of prominent characters who dot the
prophetic Scriptures. Often it seems that these are better
known than the general flow of eschatology itself. What con-
temporary evangelical Christian has not heard and spoken
about the Beast and the Antichrist? So many of these characters
are evil minions of Satan and are thought by adherents to
pessimistic eschatologies to be inimical to the postmillennial
hope. We have seen that this pessimistic concern regarding the
persecutors prophesied in the New Testament is no longer

60. Such imperial arrogance would produce alleged miracles as confirmation.
Vespasian is called “the miracle worker, because by him “many miracles occurred.”
Tacitus, Ifistories 4:81; Suetonius, Vespasian 7.
