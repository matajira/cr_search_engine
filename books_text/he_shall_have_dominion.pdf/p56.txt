10 ITE SHALL HAVE DOMINION

drama, history has direction.” He saw that history is moving
to a glorious conclusion; hence, he vicwed history as lincar
rather than cyclical.** The three basic elements of a Christian
view of history are these: (1) linear movement, (2) divine intru-
sion, and (3) teleological orientation.

The Christian, Augustinian view of universal history reigned
with great influence throughout the Medieval period. It was
largely displaced by a secular philosophy of history influenced
by the Renaissance concern with classical antiquity. For a brief
time in 1792, the leaders of the French Revolution attempted to
impose a new calendar on France. In fact, the very designations
“Middle Ages,” “Mcdieval Period,” “Dark Ages,” and so forth,
evidence a bias against the Christian influence on history. The
period of the dominant influence of Christianity in the Middle
Ages is considered to be a dark period separating the golden
days of pagan Greece and Rome from its glorious modern heirs
in secular humanism. Notice the dim view that the Marquis de
Condorcet had of the Middle Ages: “Man’s only achievements
were theological day-dreaming and superstitious imposture, his
only morality religious intolerance.” But the ancient pagan
and modern secular views of history are not glorious at all.**

Christian historian G. Gregg Singer relates an experience he
had at an annual meeting of the American Historical Associa-
tion in the early 1970s. He was at an informal small group

33. W. T. Jones, The Medieval Mind, vol. 2 in A History of Western Philosophy (Qnd
ed.; New York: Harcourt, Brace, & World, 1969), p. 135.

34. Although secularist intellectuals are still debating the origin of the linear
conception of history, most accept that it derives from the Bible. Victor Ferkiss,
Technological Man (New York: Mentor, 1969), pp. 22, 43-44. See also: Cullmann,
Christ and Time, chaps. 1-2.

35. Marquis de Condorcet, Sketch for a Historical Picture of the Progvess of the
Human Mind, trans. by June Barraclough (London, [1795] 1955), p. 77.

36. It is more than a little interesting that this century, which has been praised
as the age of the triumph of humanism, is also noted for being the bloodiest century
known to man, See: Gil Elliot, The Twentieth Century Book of the Dead (New York:
Scribner's, 1972). See also: Kenneth L. Gentry, Jr, “The Greatness of the Great
Commission,” journal of Christian Reconstruction 7:2 (Winter 1981) 19-24.
