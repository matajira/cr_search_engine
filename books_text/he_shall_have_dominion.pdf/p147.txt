The Revelation of Truth 101

and all his works are done in truth. Ie loveth righteousness
and judgment: the earth is full of the goodness of the Lorn. By
the [creative] word of the Lorn were the heavens made; and all
the host of them by the breath of his mouth.”

The Providential Word

By God’s Word the universe is providentially upheld
through the continued application of its inherent power. The
God of Scripture is no deistic Creator; He is intimately and
personally involved in every aspect of His creation to maintain
and preserve it by His active Word. The divine Christ is said to
be “upholding al] things (ta panta) by the word of his power”
(Heb. 1:3; cf. 2 Pet. 3:7). His Word will never pass away (Matt.
24:35).

The sovereignty of God must be brought to bear upon this
matter. The Scripture teaches that the Triune God is in total
and absolute control of every eventuation in every corner of the
universe. God’s total, absolute, unchangeable control of all
things is rooted in His predetermined plan from eternity past.
God is not a finite creature, limited to the confines of time, the
succession of moments, and the competition of other forces. He
is the Eternal Now, existing always in the present (Exo. 3:14).
“God, from all eternity, did, by the most wise and holy counsel
of his own free will, freely, and unchangeably ordain whatsoev-
er comes to pass: yet so, as thereby neither is God the author of
sin, nor is violence offered to the will of the creatures; nor is
the liberty or contingency of second causes taken away, but
rather established” (Westminster Confession of Faith 3:1).

God controls the universe as a system and rules the “natu-
rai” phenomena on earth.” “And He is before all things, and
in Jim all things consist” (Col. 1:17; cf. Isa. 45:7a; Heb. 1:3).
For the Christian “natural law” is but a convenient phrase to

12. Nah. 1:3-6; Isa. 45:7b; Psa, 29; 104:21; Job 36:32; 37:3; 28:23-27; 38:12-
39:30; Amos 4:7; Matt. 5:45; 6:28-30; Acts 14:17.
