Characters 387

ate and distinct “assembly” (episunagoge; the Church is called a
sunagoge in James 2:2). After the Temple’s destruction, God
would no longer tolerate going up to the Temple to worship (it
would be impossible!), as Christians frequently did prior to A.D.
70.8

Paul consoles them by denying the false report that “the day
of Christ had come” (2 Thess. 2:2). Apparently, the very reason
for this epistle, written so soon after the first one, is that some
unscrupulous deceivers had forged letters from Paul and had
claimed erroneous charismatic insights relevant to eschatologi-
cal concerns. In his earlier letter, he had to correct their grief
over loved ones who had died in the Lord, as if this precluded
their sharing in the resurrection (1 Thess. 4:13-17). Now new
eschatological deceptions were troubling the young church (2
‘Thess. 2:1-3a): Some thought that the Day of the Lord had
come“ and, consequently, they quit working (2 Thess. 3:6-12).

The word “trouble” (threes; 2:2) is in the present infinitive
form, which signifies a continued state of agitation. It is the
same word used elsewhere only in the Olivet Discourse (Mark
13:7; Matt, 24:6). There it is even found in the same sort of
theological context: one warning of deception and trouble regard-
ing the coming of the Day of Christ (Mark 13:5-7).

Verses 3-7. Paul is quite concerned about the deception being
promoted (v. 3a). To avoid the deception and to clarify the true

48. Acts 1:4; 1:8; 18:21; 20:16; 24:11. Even in this early post-commission Chris-
tianity, believers continued to gravitate toward the Jews: engaging in Jewish worship
observances (Acts 2:1ff.; 21:26; 24:11), focusing on and radiating their ministry from
Jerusalem (Acts 2-5), frequenting the Temple (Acts 2:46; 3:1ff.; 4:1; 5:21; 21:26;
26:21), and attending the synagogues (13:5, 14; 14:1; 15:21; 17:1ff.; 18:4, 7, 19, 26;
19:8; 22:19; 24:12; 26:11).

49. Greek: enesteken. A. M. G. Stephenson, “On the meaning of enesteken he hemera
tou kuriaw in 2 Thessalonians 2:2,” Texte und Untersuchungen zur Geshichte der altchristli-
chen Literatur 102 (1968) 442-451. W. FE. Arndt and EF. W. Gingrich, A Greek-English
Lexicon of the New Testament (Chicago: University of Chicago Press, 1957), p. 266. See:
Morris, 2 and 2 Thessalonians, p. 215. Note the agreement among the following
translations: NASB, NKJV, NEB, TEV, Moffatt's New Translation, Weymouth,
Williams, Beck.
