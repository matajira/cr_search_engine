124 HE SHALL HAVE DOMINION

are seen to be but stages in evolving culture, phases in the
religious self-awareness of man. Liberation theology and main
line denominations are contemporary representatives of this
view. This approach is sometimes called situation ethics.

The Separationist Model is representative of the right wing
of Christianity. It sees the Church’s calling as keeping itself
wholly separated from contemporary culture. The focus of this
view is on heavenly citizenship, seeing the Church as but a
pilgrim community passing through this world to a greater
world above. It is essentially retreatist, recognizing the power of
sin at work in the world and seeking to avoid staining itself with
such tendencies. It concentrates on what it calls a New Testa-
ment ethic. Fundamentalism is a notable contemporary repre-
sentative of this view.

When contrasted to the two views above, the Transforma-
tionist Model may be seen to be represented in the truly cen-
trist wing of historic, orthodox Christianity. It sees the Church’s
calling as that of leading human culture to the unfolding of
God’s creation according to the directives of the Word of God.
Such is done with a view to the ethical and spiritual transforma-
tion of every area of life. The Transformationist Model sees the
significance of this world in light of the world above and seeks
to promote God’s will being done on earth as it is in heaven. It
promotes godly culture in the stead of an ungodly culture. It
concentrates on a whole Bible ethic, including God’s Law, as
opposed to a truncated, separationist, “New Testament only”
ethic. Confessional Presbyterianism has been representative of
this view. Machen was typical (note 4, above).

Realizing these varying approaches, let us turn to a biblico-
theological consideration of the question of the continuing
validity of the Law of God in the New Covenant era. Has God
changed His covenantal demands in the New Covenant era so
as to abolish the Law as the normative standard of Christian
ethics? Approaching the question of ethical righteousness from
a covenantal perspective, we can discern a transformationist
