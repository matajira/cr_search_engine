Features 339

The Key Text

As I noted in Chapter 8, the key to understanding the Great
Tribulation in Matthew 24 is the time statement in verse 34:
“Assuredly, I say to you, this generation will by no means pass
away till all these things are fulfilled.” This is the statement that
must be reckoned with by the futurist or historicist viewpoints.
Some point to Matthew 24:34 and such verses as “difficult
texts” requiring that we “look at them carefully.”* The diffi-
culty is generally held to be due to two problems. (1) The ne-
cessity of reconciling the nearness statement of verse 34 with
verse 36, which reads: “But of that day and hour no one
knows, no, not even the angels of heaven, but My Father only.”
(2) How to understand the reference to “all these things,” when
many of these seem to be of worldwide effect and/or are con-
summational and incapable of application to the first century
(e.g., Matt. 24:14, 21, 27, 29-30).

Amillennialist theologian Anthony Hoekema holds that “this
generation” is used in a qualitative sense, as of an “evil” (Matt.
12:45), “adulterous” (Mark 8:38), or “perverse” (Matt. 17:17)
generation. “By ‘this generation,’ then, Jesus means the rebel-
lious, apostate, unbelieving Jewish people, as they have
revealed themselves in the past, are revealing themselves in the
present, and will continue to reveal themselves in the future.””

Ridderbos’ amillennial view is similar to Hoekema’s, but is
somewhat broader. With Hoekema, he sees in “all these things”
@ compaction of two evenis: the A.D. 70 destruction of the Temple
and the consummative Return of Christ. Consequently, “all
these things” are to occur upon “this generation,” which in his
understanding refers not just to the Jewish race, but to all

6. Hoekema, Bible and the Puture, p. 113.
7. bid. F. Biichsel, “genea,” Theological Dictionary of the New Testament, Gerhard
Kittel, ed., 10 vols. (Grand Rapids: Ferdmans, 1964), 1:663.

8. Ridderbos, Coming of the Kingdom, p. 502. Hoekema, Bible and the Future, p.
178.
