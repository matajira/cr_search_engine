142 HE SHALL HAVE DOMINION

Law very extensive, specific, and all-encompassing commands. The
Law is explicit in regard to moral directives and correctives.
Man is given concrete standards possessing ultimate authority
over man’s ethical guidance in personal and social ethics. Non-
Christian ethics has long since divided between facts and values.
But such cannot be the case in Christian ethics. The Creator
God of all facts is also the Righteous God of all values. There is
no divorce between metaphysics and ethics in Christianity.

It is the well-known Law of God that the prophets saw as
established in the future Messianic Kingdom (a consequence of
the work of Christ and the spread of the gospel). In Isaiah 2:2-
4, we read of the glory of the Messianic future:

Now it shall come to pass in the latter days that the mountain of
the Lord’s house shall be established on the top of the moun-
tains, and shall be exalted above the hills; and all nations shall
flow to it. Many people shall come and say, “Come, and let us go
up to the mountain of the Lorp, to the house of the God of
Jacob; he will teach us His ways, and we shall walk in His paths.”
For out of Zion shall go forth the law, and the word of the Lorp
from Jerusalem. He shall judge between the nations, and shall
rebuke many people; they shall beat their swords into plow-
shares, and their spears into pruning hooks; Nation shall not lift
up sword against nation, neither shall they learn war anymore.

In Jeremiah 31:33-34, we discover the spiritual application
of that righteous Law to the very heart of man, as a vital aspect
of the saving work of God:

“But this is the covenant that I will make with the house of
Israel: After those days,” says the Lorn, “I will put My law in
their minds, and write it on their hearts; and I will be their God,
and they shall be My people. No more shall every man teach his
neighbor, and every man his brother, saying, ‘Know the Lorp,’
for they all shall know Me, from the least of them to the greatest
of them,” says the Lorp. For I will forgive their iniquity, and
their sin I will remember no more.”
