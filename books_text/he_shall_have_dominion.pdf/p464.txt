418 HE SHALL HAVE DOMINION
Judgment Day

At the end of the kingdom era and just preceding Judgment
Day, Satan is loosed very briefly (@ “little while,” Rev. 20:3)
from his bondage. During this short period of time, he is al-
lowed to gather a sizeable force of rebels, who will attempt to
supplant the prevailing Christian majoritarian inflence in the
world (Rev. 20:7-9). Under His providential rule, Christ’s
spiritual kingdom will have spread over the face of the earth
and have dominated human life and culture for ages. But all
men are never converted during any period of history. Conse-
quently, upon Satan’s brief loosing, he quickly incites to war the
repressed children of wickedness.

No sooner does he prepares his forces than fire comes down
from God out of heaven and devours them (Rev. 20:9). This
figuratively portrays the Goming of Christ for what it represents
to the wicked. Christ returns “in flaming fire taking vengeance
on those who do not know God, and on those who do not obey
the gospel of our Lord Jesus Christ” (2 Thess. 1:8). Before he
can actually harm the Christian order (he merely surrounds
“the camp of the saints and the beloved city,” Rev. 20:9),
Christ’s Second Advent ends history and sweeps all evil into
eternal judgment. At this event, men enter their final, eternal
abode: either the New Heavens and New Earth or the Lake of
Fire (see Chapter 13, above). Here I show that not only will the
Lord vindicate His people by historical sanctions on earth, but
there will be a final and conclusive judgment of the wicked and
a blessed confirmation of the righteous.

The Spiritual Beauty of the Bride

The New Creation/Jerusalem of Revelation 21-22 began in
the first century, although it stretches out into eternity in its

60. See: Gary North, Dominion and Common Grace: The Biblical Basis of Progress
(Tyler, TX: Institute for Christian Economics, 1987).
