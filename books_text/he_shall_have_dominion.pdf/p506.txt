460 HE SHALL HAVE DOMINION

postmillennialism? Is Christ’s resurrection-power limited to the
upholding of believers in times of persecutional suffering?

Again, his statements must be understood in terms of his
Present condition: Paul is writing from prison, where he is
being held because of his enemies (Phil. 1:7, 13°*). As with the
case in 2 Corinthians 4, and as Davidson notes regarding Phil-
ippians 3, “verses 4-11 are a biographical passage.”**

Romans 8:17

Romans 8:17 reads: “[I]f so be that we suffer with him, that
we may be also glorified together.” Of this verse Gaffin com-
ments: “This correlation of future glory and present suffering
is a prominent concern in the section that follows. At least two
points are worth noting about ‘our sufferings’ (v. 18): (1) their
nature/breadth and (2) their terminus” (i.e., the resurrec-
tion).** It is important to note that this passage is something of
a conclusion to Romans 6-7. Romans 6 and 7 deal with the
internal struggle of the Christian against indwelling sin, not the
external buffeting of persecution! The postmillennialist does not
teach that there is coming a day in which Christians will no
longer have a sin nature. As John Murray notes on this verse:
“Christian suffering ought not to be conceived of too narrowly.
In the passages so far considered, and elsewhere in the New
Testament (e.g., 2 Co 1:5-10; 1 Pe 4:12-19), suffering includes
but is more than persecution and martyrdom.”*”

34. Paul was imprisoned many times (2 Cor, 11:23) and suffered much affliction
(1 Cor. 15:32; 2 Cor. 1:8-11; 6:5).

35. Francis Davidson, The New Bible Commentary (2nd cd.; Grand Rapids: Eerd-
mans, 1968), p. 1030, Hendriksen suggests that this letter may even be in response
to the Philippians’ specifically expressed concern about Paul’s condition. William
Hendriksen, Exposition of Philippians (New Testament Commentary) (Grand Rapids:
Baker, 1962), p. 19.

36. Gaffin, “Theonomy and Eschatology,” p. 213.

37. John Murray, The Epistle to the Romans (The New International Commentary on
the New Testament), 2 vols. (Grand Rapids: Eerdmans, 1959), 1:213.
