The Hermeneutic of Scripture 147

The Philosophy of Language Argument

The immediately striking point about Ryrie’s first proof is
that it is a preconceived hermeneutic. This is quite evident in
Ryrie’s statement that “principles of interpretation are basic and
ought to be established before attempting to interpret the
Word. .. .""' Does not his approach to language function dis-
allow the possibility of a spiritual interpretation at the very out-
set? Why must we begin with the assumption of literalism? May
not so rich a work as the Bible, dedicated to such a loity and
spiritual theme (the infinite God’s redemption of sinful man),
written by many authors over 1,500 years employ a variety of
literary genres? No symbols? No metaphors? No analogies?

Even dispensationalists admit that biblical revelation often
does employ figures of speech. But this brings up the very
controversy before us: when is prophecy to be interpreted liter-
ally, and when figuratively? Poythress rightly suspects that
dispensationalists “may have conveniently arranged their deci-
sion about what is figurative after their basic system is in place
telling them what can and what cannot be fitted into the sys-
tem. The decisions as to what is figurative and what is not
figurative may be a product of the system as a whole rather
than the inductive basis of it.”!* This fact is evidenced in Ry-
rie’s statement that “The understanding of God’s differing
economies is essential to a proper interpretation of His revela-
tion within those various economies.”” In other words, you
must have a dispensational framework (“understanding God’s

differing economies”) in order to do “proper interpretation”!'*

11. Ryrie, Dispensationalism Today, p. 86.

12. Poythress, Understanding Dispensationalists, p. 53. For a discussion between
Poythress and two leading dispensationalists over Poythress’ arguments, see: Grace
Theological Journal 10:2 (Fall 1989) 123-160,

13. Ryrie, Dispensationalism Today, p. 31.

14. This is despite Ryrie’s complaint: “Thus the nondispensationalist is not a
consistent literalist by his own admission, but has to introduce another hermeneutical
principle (the ‘theological’ method) in order to have a hermeneutical basis for the
