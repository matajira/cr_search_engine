Pragmatic Objections 439

pect a delay of His return - in the very same passages! (See
Matthew 25:5, 14, 19 in conjunction with Matthew 24:42;
25:13.) It is interesting that in Christ’s parable, it is the foolish
virgins who expected His imminent return (Matt. 25:1-13). It is
significant that it is the wicked scoffer who was not prepared for
the long delay of Christ’s Return (2 Pet. 3:3-9).

As Davis observes, we should understand that the call to
watchfulness in regard to the Second Coming is based on its
unexpectedness, not its calculability (Luke 12:35-40).*

“Postmillennialism is Rooted in Evolutionary Thought”

In this and the next two sections, we encounter inter-related
objections to postmillennial eschatology. These problems point
to the alleged basis (evolution), method (liberalism), and result
(social gospel) of postmillennialism. Let us consider the evolu-
tionary argument first.

Not infrequently, a complaint against postmillennialism is
that it is closely associated with evolutionary thought. Often
both the optimism and the developmental progress inhcrent in
evangelical postmillennialism are alleged to have been derived
from evolutionary science. Because of this misperception, it is
not uncommon to hear the criticism that “postmillennialists
have had some difficulty maintaining a genuine supernatural-
ism.”*” In his study of millennial views, Walvoord parallels a
theologically liberal, evolution-based postmillennialism with
theologically orthodox, Scripture-based postmillennialism (al-
though he allows there is some distinction between them).*
Berkhof sharply distinguishes between evangelical, supernatur-
alistic postmillennialism and its naturalistic, evolutionary imita-
tion. Yet, after citing several liberal postmillennialists, he writes:

36. Davis, Christ’s Victorious Kingdom, p. 105.
$7, Erickson, Contemporary Options in Eschatology, p. 72.

38. John F. Walvoord, “The Millennial Issue in Modern Thought,” Bidliatheca
Sacra 106 (Jan. 1948) 154.
