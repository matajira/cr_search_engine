354 WWE SHALL IIAVE DOMINION

Furthermore, John’s vision of the New Jerusalem obviously
reflects back in some ways upon Ezekiel’s vision. John seems to
have adapted Ezekiel’s vision as a portrayal of the kingdom of
God in history.” But John’s is manifestly a symbolic portrayal,
for the city’s size is a 1,342-mile cube. This would cause the top
of the city to extend 1000 miles beyond the orbit of today’s
Space Shuttle! Like John’s vision, Ezekiel's is an ideal symbol,
not a prophecy of a literal city.*°

Second, the dispensational view is redemptively retrogressive.
As David Brown complained over a century ago: Such a posi-
tion is guilty of “Judaizing our Christianity, instead of Chris-
tianizing the adherents of Judaism.”*?

Ezekiel’s Temple vision, if interpreted literally, would reim-
pose circumcision and displace baptism (at least for males): “No
foreigner, uncircumcised in heart or uncircumcised in flesh,
shall enter My sanctuary, including any foreigner who is among
the children of Israel” (Ezek. 44:9). This re-establishes that
which has forever been done away with, according to the clear
teaching of the New Testament.” The circumcisional separat-
ing “partition” between Jew and Gentile has been permanently
broken down, according to the New Testament (Eph. 2:11-21).

A literalistic approach to Ezekiel’s vision would re-institute
redemptive sacrifices, despite their fulfillment and removal in the
New Testament (Heb. 7:27; 9:26; 10:1-14). It re-institutes “the
burnt offering, the sin offering, and the trespass offering”
(Ezek. 40:39; cf. 43:21), though these were taken away in Christ
(Heb. 10:5, 9, 18). Why would the Lord return again to the

49. G. R. Beasley-Murray, “Ezekiel,” The Eerdmans Bible Commeniary, Donald
Guthrie and J. A. Motyer, eds. (3rd ed.; Grand Rapids: Eerdmans, 1970), p. 684.

50. I have seen no example of a dispensational defender of literalism apply his
literalist hermeneutic to this passage. This is not to say that some dedicated but ob-
scure author has not done so.

51. David Brown, Christ's Second Coming: Will it Be Premillennial? (Edmonton,
Alberta; Still Waters Revival, [1882] 1990), p. 352.

52. Acts 15; Rom. 2:26-29; 4:9-12; 1 Cor. 7:18-19; Gal. 5:2-6; 6:12-15; Phil. 3:3;
Col, 2:11; 3:11.
