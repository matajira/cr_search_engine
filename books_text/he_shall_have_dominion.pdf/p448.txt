402 HE SHALL HAVE DOMINION

It is equally certain that marriage was based on a covenant con-
tract.’* That the scroll in Revelation 6 would be a bill of div-
orcement is suggested on the following considerations.

First, in Revelation we have prominent emphases on two
particular women, two women who obviously correspond as
opposites to one another. The two women are the wicked harlot
of the beast (Rev. 17-18) and the pure bride of Christ (Rev. 21).
They correspond with the earthly Jerusalem, which was the scene
of Christ’s crucifixion (Rev. 11:8), and the heavenly Jerusalem,
which is holy (Rev. 21:10), as I show below. The flow and drift
of the book is the revelation and execution of the legal (Rev.
15:3; 16:5-7) judgment on the fornicating harlot and the com-
ing of a virginal bride, obviously to take the harlot’s place after
a marriage supper (Rev. 19).

Second, the apparent Old Testament background for this
imagery is found in Ezekiel. In Ezckiel 2:9-10, Israel’s judg-
ment is portrayed as written on a scroll on the front and back
and given to Ezekiel. ‘his corresponds perfectly with Revela-
tion 5:1, In Ezekiel, chapters 2 and following, the devastation of
Israel is outlined, which corresponds with Revelation 6ff. In
Ezekiel 16, Israel is viewed as God’s covenant wife who became
a harlot (see also Jer. 3:1-8; Isa. 50:1) that trusted in her beauty
and committed fornication, just as Jerusalem-Babylon of Revel-
ation (Rev. 18). She is cast out and judged for this evil conduct.

Third, following the “divorce” and judgments associated with
them, John turns to see the coming of a new “bride” out of
heaven (Rev. 21-22). It would seem that the new bride could not
be taken until the harlotrous wife should first be dealt with legally.
John imports the imagery of the harlot, bride, and marriage
feast; this is not being read into the text from outside. Thus,
the imagery of divorce fits the dramatic flow of the work.

The judgment of the fornicating harlot is begun when Christ
begins opening the seven seals on the scroll: God the Father

15. Prov. 2:17; Ezek. 16:8; Mal. 2:14.
