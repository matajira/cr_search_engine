334 HE SHALL HAVE DOMINION

postmillennial complaint is well-justified. If a literal earthly
millennium is so prominent in Scripture and such an important
era in redemptive history (as premillennialists and dispensation-
alists argue), then why should we not expect that a reference to
the thousand years should appear in more than one passage?

This is even more difficult to conceive of in light of our
second obscrvation: the mention of the thousand-year reign occurs in
the most figurative and difficult book in all of Scripture. If it is a
literal time frame, why is it that it is only mentioned in this
highly symbolic book? It is a bit odd, too, that this time frame
is so perfectly rounded and exact, which seems more compati-
ble with a figurative view. Warfield is surely correct, when he
comments “we must not permit ourselves to forget that there is
a sense in which it is proper to permit our understanding of so
obscure a portion of Scripture to be affected by the clearer
teaching of its more didactic parts. .. . [T]he order of investiga-
tion should be from the clearer to the more obscure.” But this
hermeneutical principle has not often been honored. “Nothing,
indeed, seems to have been more common in all ages of the
Church than to frame an eschatological scheme from this pas-
sage, imperfectly understood, and then to impose this scheme
on the rest of Scripture vi et armis.””

Clouse is correct about the place of the millennium in the
discussion: “These categories [amillennial, premillennial, post-
millennial], although helpful and widely accepted, are in certain
respects unfortunate as the distinctions involve a great deal
more than the time of Christ’s return.””? Nevertheless, post-
millennialist Boettner is scolded by amillennialist Hoekema for
not giving exposition to Revelation 20:1-6 in his presentation of
the postmillennial conception of the kingdom!*°

78, B. B. Warfield, “The Millennium and the Apocalypse,” Princeton Theological
Review, 2 (Oct. 1904) 599.

79. Clouse, “Introduction,” Meaning of the Millennium, p. 7.
80. Anthony Hoekema, “An Amillennial Response,” idid., p. 150.
