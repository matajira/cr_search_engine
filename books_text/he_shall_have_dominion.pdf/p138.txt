92 HE SHALL HAVE DOMINION

Conclusion

Systematization of the various theological loci naturally dev-
eloped over time, engaging the gifts and minds of spiritually
sensitive Christian leaders. Most biblical theologians would
agree that eschatology has certainly been one of the loci that
has undergone the most development in history. As I indicated
earlier, eschatology is extremely deep and involved, intertwin-
ing itself with the very essence of Christanity itself. Because of
this, the antiquity of an eschatological system, as such, is not
absolutely essential to its orthodoxy. Nevertheless, the eschatol-
ogical factors in Scripture cannot have been without some ap-
parent impact upon the nascent development of early Christ-
endom’'s perception of the flow of history. An eschatology lack-
ing any historical rooting in antiquity is rightly suspect.

Much popular literature leaves the impression that postmil-
lennial thought is a recent novelty. I have shown that postmil-
lennialism is not without historical precedent in the early centu-
ries of the Christian Church. Indeed, it has been the framework
of some of the noted minds of the Church. The crucial ele-
ments of postmillennialism — the presence of a biblically inform-
ed, historically relevant, and ultimately optimistic temporal
hope - is clearly present in antiquity.

Furthermore, the postmillennial position has been held in
more recent centuries by noted and devout defenders of the

Hulse, The Restoration of Israel (1968). Francis Nigel Lee, Will Christ or Satan Rule the
World? (1977). Marcellus Kik, An Eschatology of Victory (1971). J. Gresham Machen, in
Ned Stonehouse, J. Gresham Machen: A Biographical Memoir (1954), pp. 187, 245, 261.
George CG. Miladin, Is This Really the End? (1972). Iain Murray, The Puritan Hope
(1971). John Murray, Romans (1965). Gary North, Millennialism and Social Theory
(1990). John Owen, The Works of John Owen, vol. 8 (1850-1853). R. J. Rushdoony,
God’s Plan for Victory (1977). Steve Schlissel, Hal Lindsey and the Restoration of the Jews
(1990). W. G. T. Shedd, Dogmatic Theology (1888). Norman Shepherd, in Zondervan
Pictorial Bible Dictionary, 4:822-823 (1975). Augustus H. Strong, Systematic Theology
(1907). J. H. Thornwell, Collected Writings, vol. 4 (1871), Richard C. Trench, Notes on
the Miracles and Parables of Our Lord (1875). B. B. Warfield, Selected Shorter Writings
(1970). Note: On the Puritans, see: Peter Toon, Puritans, the Millennium and the Future
of Israel (1970).
