302 HE SHALL HAVE DOMINION

Scripture solely to the final consummate order, using this pas-
sage as determinative of the others.'°

A part of the problem with 2 Peter 3 lies in the fact that the
passage employs terminology that is sémetimes used to desig-
nate the spiritual new creation and at other times is used in
reference to the destruction of Jerusalem in A.D. 70. This pas-
sage, however, does not speak either of the spiritual new cre-
ation (Isa. 65:17) or the conflagration of Jerusalem (Heb. 12:25-
29). It points instead to the consummate order to follow the
resurrection and final judgment, as is evident from the follow-
ing considerations.

First, the thrust of the book seems to promote a spiritual
perseverance in anticipation of the historical long run — a long
run that ends up in the eternal new creation. Peter urges the
perseverance of his readers (1 Pet. 1:6) and warns against short-
sightedness (1:9). It is only through long-term perseverance
that we may expect access to the eternal kingdom of Jesus
Christ (1:11). Peter himself expects to die soon (1:13-14; as did
Paul, 2 Tim. 4:6-8). Consequently, he urges his readers to recall
these things after he is gone (1:15), apparently not expecting a
rapture of the Church in A.D. 70 (as per radical preterists’”).

Peter gives Noah and Lot as examples of those who perse-
vered through hard times, like those facing the looming de-
struction of Jerusalem. They came out on the other end still
upon the earth (2:5-9). The rescue of believers from the oncom-
ing temptation (2:9a) associated with A.D. 70 (by preserving
them in trial, Luke 21:18-22) is set in contrast to the reserving
of the fallen angels and the ungodly until the (later) Judgment
Day (2:4, 9b). While contemplating the judgment cleansing of

106. See for example: Hoekema, Bible and the Future, ch. 20. Adams, Time Is at
Hand, pp. 13ff. Philip Edgcumbe Hughes, Intexpreting Prophecy (Grand Rapids:
Eerdmans, 19°76), pp. 131-135. G. C. Berkouwer, The Return of Christ (Grand Rapids:
Eerdmans, 1972), ch. 7. Herman Ridderbos, The Coming of the Kingdom (Grand
Rapids: Eerdmans, 1962), p. 274.

107. Russell, Parousia, Preface, and pp. 126, 137, 165, 168, 199, 445, 525.
