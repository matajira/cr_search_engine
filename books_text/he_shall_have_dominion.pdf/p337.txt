Consummation 291

human race: “He has given Him authority to execute judgment
also, because He is the Son of Man” (John 5:27). It also serves
as the necessary outcome of His incarnation: “It was right that
when the Lord of all condescended, in His unspeakable mercy,
to assume the form of a servant, and endure the extremist
indignities of His enemies, He should enjoy this highest tri-
umph over them, in the very form and nature of His humilia-
tion” (Phil 2:9-11). Christ’s prominence in the final judg-
ment is a frequent theme in the New Testament.”

The doctrine of the general judgment (i.e., of all men in one
scene) may be discerned from various angles. First, there is
coming a judgment “day.” “He has appointed a day on which
He will judge the world in righteousness by the Man whom He
has ordained” (Acts 17:31a). This singular judgment day is
evident in a number of Scriptures.® As in the case of the day
of resurrection, this day of judgment cannot be stretched out
over a 1,007-year period, as per dispensationalism (see above
discussion).

Second, judgment day involves both the saved and the lost.
It is at the day of resurrection that both the just and the unjust
will enter into their judgment, one to life, the other to condem-
nation: “Do not marvel at this; for the hour is coming in which
all who are in the graves will hear His voice and come forth;
those who have done good, to the resurrection of life, and
those who have done evil, to the resurrection of condemnation”
(John 5:28-29).

Romans 2:5-8 clearly speaks of a day of judgment that will
encompass both classes of men: “In accordance with your hard-
ness and your impenitent heart you are treasuring up for your-
self wrath in the day of wrath and revelation of the righteous

66. Dabney, Systematic Theology, p. 846.
67. Matt. 25:31-32; John 5:22, 27; Acts 10:42; 17:31; Rom. 2:16; 14:9-10; 2 Cor.
5:10; Phil. 2:9-11; 2 Tim. 4:1, 8.

68. Dan. 7:10; Matt. 7:22; 11:22; 12:36; Rom. 2:5; 2 Tim. 1:10-12; 4:8; 2 Pet.
3:7; 1 John 4:17,
