Realization 229

the lowest level there are the saved, living, Gentile nations.””

“The Gentiles wil] be Israel’s servants during that age. . . .
The Gentiles that are in the millennium will have experienced
conversion prior to admission.”*'

“God will keep his original promises to the fathers and will
one day convert and place Israel as the head of the nations.”

“Israel will be a glorious nation, protected from her enemies,
exalted above the Gentiles... .” “In contrast to the present
church age in which Jew and Gentile are on an equal plane of
privilege, the millennium is clearly a period of time in which
Israel is in prominence and blessing. . . . Israel as a nation will
be exalted.”*

Yet in Scripture, Christ’s kingdom is distinctly represented
as being pan-ethnic, rather than Jewish. While on earth, Christ
clearly and forthrightly taught that God would soon set aside na-
tional Israel as a distinctive, favored people in the kingdom. In Mat-
thew 8:11-12, in the context of the Gentile centurion’s faith, He
expressly says that the “sons of the kingdom shall be cast out”
while “many from the east and west” shall enjoy the Abrahamic
blessings. In Matthew 21:43, He parabolically teaches the rejec-
tion of national Israel when He says: “Therefore I say to you,
the kingdom of God will be taken away from you, and be given
to a nation producing the fruit of it.” In Matthew 23-24, He
prophesies the removal of the spiritual heart of Israel, the
temple. He says it will be lefi “desolate” (Matt. 23:38) during
the Great Tribulation (Matt. 24:21) when men should flee
Judea (Matt. 24:16). He emphatically noted that “all these
things shall come upon this generation” (Matt. 23:36; 24:34).

40, Herman Hoyt, “Dispensational Premillennialism,” The Meaning of the Millenni-
um: Four Views, Robert G. Clouse, ed, (Downer’s Grove, IL: Inter-Varsity Press,
1977), p. 81.

41. Pentecost, Things to Come, p. 508.

42. House and Ice, Dominion Theology, p. 175.

43, John F. Walvoord, The Millennial Kingdom (Grand Rapids: Zondervan, 1959),
pp. 136, 302-303.
