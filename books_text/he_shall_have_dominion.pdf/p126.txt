80 HE SHALL HAVE DOMINION

Origen (A.D. 185-254)

Although much in Origen is unacceptable, he is a notewor-
thy church father of considerable influence. As Philip Schaff has
noted regarding Origen’s views, there was in them a place for
a great evidencing of the power of the gospel: “Such a mighty
revolution as the conversion of the heathen emperor was not
dreamed of even as a remote possibility, except perhaps by the
far-sighted Origen.” “Origen seems to have been the only one
in that age of violent persecution who expected that Christiani-
ty, by continual growth, would gain the dominion over the
world."

Origen comments:

[I]t is evident that even the barbarians, when they yield obedi-
ence to the word of God, will become most obedient to the law,
and most humane; and every form of worship will be destroyed
except the religion of Christ, which will alone prevail. And in-
deed it will one day iumph, as its principles take possession of
the minds of men more and more every day.

This sort of statement is of the essence of postmillennial opti-
mism.

Eusebius (A.D. 260-340)

In Eusebius, there is an even fuller expression of hope that
is evident. In Book 10 of his Ecclesiastical History, he is con-
vinced he is witnessing the dawning of the fulfillment of Old
Testament kingdom prophecies. Of Psalms 108:1,2 and 46:8,9,
which he specifically cites, he writes that he is “Rejoicing in
these things which have been clearly fulfilled in our day.”

62. Schaff, History of the Christian Church, 2:591, 122. He cites Neander, General
History of the Christian Religion and Church (12th ed.), 1:129.

63. Origen, Against Celsus 8:68.

64. Eusebius, Ecclesiastical History 10:1:6.
