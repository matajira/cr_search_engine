Features 361

ational blessings above or beyond history, either to heaven or to
the consummational New Earth.” Postmillennialism, however,
expects the redemptive labor of Christ to have a transforma-
tional effect in time and on earth, continuous with present
spiritual realities already sct in motion by Christ.

The major passage setting forth the spiritual conception of
the change wrought by Christ in history is Isaiah 65:17-25. In
that vast scene, we have a sweeping picture of the full extent of
the coming gospel economy, a reality established by Christ at
His first coming. This economy will develop through “a multi-
stage process that culminates at the final judgment.”* This is
a redemptive economy that will gradually so transform the world
ethically and spiritually that it is here portrayed as a “new
heavens and a new earth” of which “the former shall not be
remembered or come to mind” (Isa. 65:17).

This Isaianic vision is in the background of Paul’s statement
in 2 Corinthians 5:17, which refers to contemporary spiritual
realities: “Therefore, if anyone is in Christ, he is a new creation;
old things have passed away; behold, all things have become
new.” According to New Testament theology, the Second
Adam, Christ, stands at the head of a new creation (Rom. 5:14;
1 Cor. 15:22, 45),

Calvin viewed Isaiah 65:17-25 as a New Covenant blessing
that resulted from a change in covenantal administration:

67. See my response to amillennialist Richard B. Gaffin’s article “Theonomy and
Eschatology: Reflections on Postmillennialism”: Gentry, “Whose Victory in History?”
Theonomy: An Informed Response, Gary North, ed. (Tyler, TX: Institute for Christian
Economics, 1991), pp. 207-230. See also: North, Millennialism and Social Theory, ch. 5.

68. North, Millennialism and Social Theory, p. 104.

69. See: John Calvin, The Second Epistle of Paul to the Corinthians, and the Epistles
to Timothy, Titus and Philemon (1577), David W. Torrance and Thomas F. Torrance,
eds., trans. by T. A. Smail (Grand Rapids: Eerdmans, 1964), pp. 75-76. F. F. Bruce,
I & If Corinthians (New Century Bible Commentary) (Grand Rapids: Eerdmans, 1971), p.
209. See also: Geerhardus Vos, The Patdine Eschatology (Phillipsburg, NJ: Presbyterian
& Reformed, [1930] 1991), pp. 48-49.
