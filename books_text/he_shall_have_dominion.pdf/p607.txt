Author Index 561

Cocceius, Johannes, 91
Cotton, John, 89
Cox, William E., 333
Crenshaw, Curtis, 150
Culbertson, W., 32
Cullmann, Oscar, 9, 325
Culver, Robert Duncan, 66,
314, 433, 442

Dabney, Robert L., 282,
284

Darwin, Charles, 440

Davis, John Jefferson, 44,
78, 425

denHartog, Arie, 24, 237

Doezema, Don, 23

Dollar, George H., 19

Dyer, Charles H., 155

Engelsma, David J., 23

English, E. Schuyler, 286,
293

Erickson, Millard J., 43, 433

Faid, Robert W., 370
Fairbairn, Patrick, 300, 353
Feinberg, Charles L., 3n,
66, 148, 158, 261, 350
Feinberg, John S., 60, 116
Finger, Thomas N., 44
Ford, Herschell W., 278

Gaffin, Richard B.
imminence, 435
New Heavens and New
Earth, 360
postmillennialism, 43,
72, 327

Revelation of John, 20,
69

suffering, 25, 457, 528
Gannett, Alden, 329, 430
Gentry, Kenneth L., 261,

374, 389, 398, 458
Gerstner, John H., 282, 298
Goodwin, Thomas, 89
Gouge, William, 89
Graham, Billy, 24
Gunn, Grover, 150
Guthrie, Donald, 22

Hamilton, Floyd E,, 34, 53,
427
Hanko, Herman
Antinomianism, 506-524
Cultural Mandate, 181
pessimism, 22-232, 338,
371, 426, 474, 480
postmillennialism, 203,
249n, 445, 450, 492
suffering, 526-539
Hendriksen, William, 21,
388, 457
Hengstenberg, W. E., 312
Henry, Carl F. H., 295
Heys, John, xl, 371
Hobbs, Herschell, 453
Hodge, Charles 214, 267
Hoekema, Anthony
amillennial, 56-57
Antichrist, 373-374
Great Tribulation, 338f
heaven, 294
immortality, 290
New Heavens and New
earth, 18, 363-364
