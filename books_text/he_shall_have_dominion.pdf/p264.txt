218 HE SHALL HAVE DOMINION

37a; cf. Matt. 27:11; Mark 15:2; Luke 23:3).

Although He defines His kingdom as something other-
worldly, rather than essentially political (as was Caesar’s king-
dom), He nevertheless indicates His kingdom is present: He
speaks of “my kingdom” (v. 36a). He claims to have His own
“servants” (even though they do not fight with sword to defend
Him, v. 36b). He clearly states “I am king” (v. 37a). And, as we
might expect, given our previous study of Mark 1:14-15, He
states that it was for that very purpose He was born into the
world (v. 37b)!

Application
The Coronation of the King

A frequent refrain in the New Testament is that of the glori-
ous and powerful enthronement of Christ, which was anticipat-
ed in His nativity prophecies. In a number of passages, He is
spoken of as having ascended into heaven and having been
royally seated at the right hand of the throne of Almighty God,
Creator of the heavens and the earth.

The anticipation of this enthronement is clearly evident in
His post-resurrection, pre-ascension Great Commission.’* In
Matthew 28:18-20, we read a statement much in contrast to His
earlier reservation and humility. No longer do we hear the
familiar, “I can do nothing of Myself” (John 5:19, 30; 8:28;
12:49; 14:10). Rather, we hear a resoundingly powerful: “All
authority has been given Me in heaven and on earth.” A mighty
transformation has taken place in the ministry of Christ as a
direct result of His resurrection. Satan has been conquered;!”

15. On “The Nature of the Kingdom,” see below: pp. 225-227.

16. Kenneth L. Gentry, Jx., The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, TX: Institute for Christian Economics, 1990).

17. Matt. 12:26-29; Luke 10:18; John 12:31; Col. 2:15; Heb. 2:14; 1 John 3:8;
4:3,4,
