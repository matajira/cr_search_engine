450 HE SHALL HAVE DOMINION

postmillennial kingdom victory. In fact, this is the most com-
mon objection that I encounter when I lecture on the subject.

In Pentecost’s assessment of the deficiencies of postmillen-
nialism, his fourth objection is along these lines. “[T]he new
trend toward realism in theology and philosophy, seen in neo-
orthodoxy, which admits man is a sinner, and can not bring
about the new age anticipated by postmillennialism.”’ As men-
tioned in the last chapter, Lindsey asserts that postmillennialism
believes in “the inherent goodness of man.”? Hanko is con-
vinced that “from the fall on, the world develops the sin of our
first parents. This development continues throughout history.
. .. More and more that kingdom of darkness comes to mani-
festation as time progresses.”* Indeed, postmillennialism “is a
mirage, therefore, a false hope, because it fails to reckon prop-
erly with the fact of sin” and “cannot take sin as seriously as do
the Scriptures.”* The implication is clearly that his view of
man’s innate sinfulness is contradictory to postmillennial expec-
tations for the future.

Regarding the question whether the depravity of man un-
dermines the postmillennial system, we must realize that each
and every convert to Christ was at one time a totally depraved
sinner. And yet there are millions of Christians. Salvation comes
by the gospel] which is the power of God unto salvation. How
can we discount the power of the gospel to save multiple mil-
lions? What God can do for one depraved sinner He can do for
another. This is evident in the apostolic era (Acts 2:41; 4:4), as
well as in biblical prophecy (Rev. 5:9; 7:9).

1, J. Dwight Pentecost, Things to Come: A Study in Biblical Eschatology (Grand
Rapids: Zondervan, 1958), p. 387.

2. Hal Lindsey, The Late Great Planet Earth (Grand Rapids: Zondervan, 1970), p.
176.

3. Herman C. Hanko, “An Exegetical Refutation of Postmillennialism” (anpub-
lished conference paper: South Holland, IL: South Holland Protestant Reformed
Church, 1978), p. 25.

4, Herman C. Hanko, “The Illusory Hope of Postmillennialism,” Standard Bearer
66:7 Jan. 1, 1990), p. 159.
