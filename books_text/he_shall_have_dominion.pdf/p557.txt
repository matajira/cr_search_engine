Cultural Antinomianism §11

to another area of confusion in Hanko’s thought. Besides the
eschatological non sequitur characterizing his concern, there is
lurking behind it a fundamental cultural antinomianism. It is to
this issue that I will now turn.

Antinomian Tendencies

By the very nature of the case, evangelical antinomianism
tends to be retreatist when it comes to confronting secular
culture and challenging it with the positive alternative of biblical
law. Retreatism is contra-postmillennial by definition. Though
the theologians of the Protestant Reformed Church deny that
the denomination is retreatist and antinomian, nevertheless, we
do find those tendencies in their writings on eschatology.

In a rebuttal of the postmillennial hope for cultural transfor-
mation, VanOverloop comments: “Most. importantly, the believ-
er’s hope is based on God’s promise of everlasting life. The
believer, with uplifted head, anticipates the return of his resur-
rected and ascended Lord. He fixes his mind upon the new
heaven and earth, which Jesus will bring upon His return.”!
Hanko argues that the antithesis between the world and the
church “comes especially to manifestation in the life of men in
the world with respect to their view of the future.” This is
because the world longs “to set up a Kingdom where Satan is
king. We walk in hope. They look for heaven here upon earth.
We are strangers in the world. They make this world their
abiding city. We look for the full realization of the purpose of
God in the Kingdom which is to come.””

It is true, of course, that to the Christian the prospect of the
consummative new heaven and new earth one day in the future
is a joyful contemplation. And certainly we must await that day

11. Ronald VanOverloop, “The Hope of Every Believer Regarding His Future
Earthly Life,” Standard Bearer 66:7 (Jan. 1, 1990), p. 163.

12. Hanko, The Christian’s Social Calling and the Second Coming of Christ (South
Holland, TL: South Holland Protestant Reformed Church, 1970), pp. 10-11.
