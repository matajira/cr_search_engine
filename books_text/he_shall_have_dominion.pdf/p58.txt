12 HE SHALL HAVE DOMINION

tions to our study are not properly understood as givens. These
will be brought to bear in our treatment of the biblical eschato-
logical system set forth in the present work. The fundamental
presuppositions of the Christian philosophy of history, which
are discovered in both testaments,”® are: God, creation, provi-
dence, fall, redemption, revelation, and consummation.

God. God exists and is absolutely independent and wholly
self-sufficient. In Exodus 3:14, He defines Himself via His spe-
cial covenantal name “YHWH” (“Jehovah”). Here He identifies
Himself as “I am that I am.” This self-designation is peculiarly
important to our understanding of God. This statement is
found in the imperfect tense in Hebrew, thereby distinguishing
a constantly manifested quality. From this name we may discern
certain of God’s intrinsic qualities: (1) His aseity: God exists of
Himself. He is wholly uncreated and self-existent. There is no
principle or fact back of God accounting for His existence (John
5:26). (2) His eternity: He is of unlimited, eternal duration. The
combination of the verb tense (imperfect) and its repetition (“I
am” / “I am”) emphasize His uninterrupted, continuous exis-
tence (Psa. 90; 93:1-2; Isa. 40:28; 57:15). (3) His sovereignty: He
is absolutely self-determinative. He determines from within His
own being. As the Absolute One, He operates with unfettered
liberty. He is not conditioned by outward circumstance. He is
what He is because He is what He is. He is completely self-
definitional and has no need of anything outside of Himself
(Isa. 40:9-31),

Creation.” There is a personal, moral, sovereign origin of
all of reality. The Christian’s creational viewpoint puts man
under God and over nature (Gen. 1:26-27; Psa. 8). It imparts
transcendent meaning to temporal history and sets before man

39. There are those who (wrongly) argue that the Old Testament operates from
a cyclical view of history. J. B. Curtis, “A Suggested Interpretation of the Biblical
Philosophy of History,” Hebrew Union College Annual 34 (1963),

40. See Chapter 9, below, for more detail.
