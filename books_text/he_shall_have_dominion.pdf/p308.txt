262 HE SHALL HAVE DOMINION

Spirit, teaching them to observe all that I commanded you” (vv.
19-20). The command of the resurrected Christ who possesses
“all authority” is for His followers to bring all nations to conver-
sion and baptism. This is precisely the expectation of so many
of the Old Testament prophecies, which foresaw all nations
flowing to Mount Zion (e.g., Isa. 2:1-4; Mic. 4:1-4), and which
anticipated “no more shall any man teach his neighbor, ‘Know
the Lord, for they shall all know the Lord’ ” (Jer. 31:34; cf. Isa.
11:9).

In addition, the Commission urges our “teaching them to
observe ail things whatsoever I have commanded you.” He has
given His people instruction for all of life (cf. 2 Tim. 3:16-17).
Involved in the proclamation of the gospel is the call for repen-
tance. Should not the “repentance for forgiveness of sins” (Luke
24:47) that we are to preach be particular and detailed rather
than general and vague? That is, should not repentance be a
“change of mind”®™ regarding the particulars of our conduct in
ail of life, so that we strive to live differently (i.c., Christianly)?
According to Luke 3 should not we then be urged to bring
forth particular fruits worthy of repentance (Luke 3:8), ie., a
change of our external behavior by being transformed by God
rather than conformed to the world (Rom. 12:1-2), as displayed
by our caring for the poor (Luke 3:11), being honest govern-
mental officials (Luke 3:12-14), or whatever?

Should not the Christian realize that “the weapons of our
warfare are not carnal, but mighty through God to the pulling
down of strong holds; casting down imaginations, and every
high thing that exalteth itself against the knowledge of God,
and bringing into captivity every thought to the obedience of
Christ” (2 Cor, 10:4-5)? If we cast down “every high thing that
exalteth itself against the knowledge of God” and bring “into
captivity every thought to the obedience of Christ,” will we not
be engaging in culture-transforming change? If we are going to

84. The Greek term metancia means a “change of mind.”
