Time Frames 319

Daniel’s prayer was particularly for Israel (Dan. 9:3ff), and it
was uttered in recognition of God’s promises of mercy upon
those who love Him (v. 4). Therefore, the prophecy holds that
the covenant will be confirmed with many for one week. The
reference to the “many” speaks of the faithful in Israel. “Thus
a contrast is introduced between He and the Many, a contrast
which appears to reflect upon the great Messianic passage,
Isaiah 52:13-53:12 and particularly 53:11. Although the entire
nation will not receive salvation, the many will receive.”**

This confirmation of God's covenant promises to the “many”
of Israel will occur in the middle of the seventieth week (v. 27),
which parallels “after the sixty-two [and seven] weeks” (v, 26),
while providing more detail. We know Christ’s three-and-one-
half-year ministry was decidedly focused on the Jews in the first
half of the seventieth weck (Matt. 10:5b; cf. Matt. 15:24). For a
period of three and one-half years after the crucifixion, the
apostles focused almost exclusively on the Jews, beginning first
“in Judea” (Acts 1:8; 2:14) because “the gospel of Christ” is “for
the Jew first” (Rom. 1:16; cf. 2:10; John 4:22).

Although the event that serves as the terminus of the sixty-
ninth week is clearly specified, such is not the case with the
terminus of the seventieth. Thus, the exact event that ends the
seventicth is not so significant for us to know. Apparenily at the
stoning of Stephen, the first martyr of Christianity, the coven-
antal proclamation began to be turned toward the Gentiles
(Acts 8:1). The apostle to the Gentiles appears on the scene at
Stephen’s death (Acts 7:58-8:1), as the Jewish persecution
against Christianity breaks out. Paul’s mission is clearly stated as
exceeding the narrow Jewish focus (Acts 9:15).

This confirmation of the covenant occurs “in the middle of
the week” (v. 27). I have already shown that the seventieth

33, Young, Daniel, p. 213.

34. Payne, “The Goal of Daniel's Seventy Weeks,” p. 109n; Boutflower, in and
Around the Book of Daniel, pp. 195ff; Hengstenberg, Christology of the Old Testament,
2:898. Young, Daniel, p. 213.
