564

Unger, Merril F., 33, 350

Vanderwaal, Cornelius, 21
Van Impe, Jack, 18
van Krimpen, J. M., 21
VanOverloop, Ronald, 24
van Riessen, Hendrik, 22
Van Til, Cornelius, 105, 127
Vincent, Marvin R., 383
Vos, Geerhardus, 2, 6, 51,
106, 202, 294, 326, 327

Walvoord, John F.
Antichrist, 370-371
date-setting, 331
hermeneutics, 149, 279n-

280n, 369, 483
millennial development,
73, 88
pessimism, 18, 384, 426
postmillennialism, 33,
36, 42, 66-67, 439,
474

HE SHALL HAVE DOMINION

Seventy Weeks, 310fF
signs of times, 41, 327
temple, 305
Zionism, 229

Warfield, Benjamin B., 51,
66-69, 256, 265, 269,
434, 475

Whisenant, Edgar C., 40

Whitcomb, John C., 350,
351, 355

Whitby, Daniel, 77

Wiersbe, Warren W., 480,
487

Wilson, Dwight, 40, 329,

433

Woudenberg, Bernard, 453

Young, Edward J., 310

Zahn, Theodor, 64
