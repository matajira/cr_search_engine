310 HE SHALL HAVE DOMINION

The Seventy Weeks

The chronology provided in Daniel’s prophecy of the Seven-
ty Weeks (Dan. 9:24-27) is a linchpin in the dispensational
system, although it is not crucial to any of the other millennial
systems. Walvoord comments that the “interpretation of Daniel
9:24-27 is of major importance to premillennialism as well as
pretribulationism.” Being such, it is the “key” to prophecy and,
consequently, “one of the most important prophecies of the
Bible.” Surcly Allis is correct when he observes that. “the impor-
tance of the prophecy of the Seventy Weeks in Dispensational
teaching can hardly be exaggerated.”

This dependence upon Daniel 9 is unfortunate for dispensat-
ionalism for two reasons. First, historically: Great difficulties are
associated with the interpretation of this passage. J. A. Mont-
gomery calls the prophecy “the Dismal Swamp of Old Testa-
ment criticism.”? Young comments: “This passage . . . is one of
the most difficult in all the OT, and the interpretations which
have been offered are almost legion.”*

Second, theologically: This “extremely important prophecy”
is the most difficult for dispensationalists to make credible to
those outside of their system. Even dispensationalist Robert
Culver admits: “The difficulty of the verses that now lie before
us is evident.”* “Premillcnnial writers of two or threc genera-

1, John F. Walvoord, The Rapture Question (Grand Rapids: Zondervan, 1957), p.
24, John F Walvoord, Daniel: The Key to Prophetic Revelation (Chicago: Moody Press,
1971), pp. 201, 216. Q. T. Allis, Prophecy and the Church (Philadelphia: Presbyterian
& Reformed, 1945), p. 111. See also: Alva J. McClain, Daniel’s Prophecy of the 70 Weeks
(Grand Rapids: Zondervan, 1940), p. 9. J. Dwight Pentecost, Things to Come (Grand
Rapids: Zondervan, 1958), p. 240. E. Schuyler English, “The Gentiles in Revelation,”
Prophecy and the Seventies, Charles Lee Feinberg, ed. (Chicago: Moody Press, 1971), p.
242,

2. J. A. Montgomery, 4 Critical and Exegetical Commentary on the Book of Daniel
(International Critical Commentary) (New York: Scribner’s, 1927), p. 400.

3. E. J. Young, The Prophecy of Daniel (Grand Rapids: Eerdmans, [1949] 1977),
p. 191.

4. Robert Duncan Culver, Daniel and the Latter Days (2nd ed.; Chicago: Moody
Press, 1977), p. 144.
