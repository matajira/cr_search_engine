534 HE SHALL HAVE DOMINION

“You know that the testing of your faith develops persever-
ance” (fms, 1:3).

“Brothers, as an example of patience in the face of suffering,
take the prophets who spoke in the name of the Lord” (Jms.
5:10).

“I, John, your brother and companion in the suffering and
kingdom and patient endurance that are ours in Jesus” (Rev.
1:9),

“If anyone is to go into captivity, into captivity he will go. If
anyone is to be killed with the sword, with the sword he will be
killed. This calls for patient endurance and faithfulness on the
part of the saints” (Rev. 13:10).

Christianity is an historical faith designed for the long run.
The faithful are to be diligently laboring now amidst trials and
tribulations with an eye to the future. It is the tendency of
sinful man to seek short-cuts to attaining his goals, but the
Christian is to labor against difficult circumstances with the
expectation of the gradualistic development of God’s kingdom
good in history. We as Christians are to learn this through our
trials and tribulations, through our affliction and suffering.

The Progressive Reduction of Suffering of in History

Because suffering is designed to teach humble patience
before God it becomes a strategic means for the training of God’s
people. It is not an historical end for them. Suffering is a character-
istic of the Church in evil times; it is not the definition of the Church
for all times. It is an instrument to a greater goal: the ultimate
blessing of godly man. Suffering is not a goal; it is a means.

Job Revisited

In Job’s case, we have a wonderful illustration of this. He
genuinely suffered. And though he wavered somewhat, he
learned obedience through his trials. Because of this we read of
his final temporal estate (not his heavenly estate): “After Job
