Expansion 237

that there will be a worldwide conversion of all or even of the
majority of peoples on the earth. The Lord gathers unto Him-
self a remnant according to the election of His grace.” The
Great Commission strongly supports the postmillennial escha-
tology, commanding God's people to seek the discipling of all
the nations.

Dominion Assured

Not only is dominion commanded, but is given the authority
of the one commanding it. The New Testament assures this.
The Great Commission command from Christ is given on the
basis of His prior eschatological teachings. We must consider briefly
a few of the important passages in this regard.

Matthew 13

In Matthew 13, we have the famous Kingdom Parables that
sketch some of the basic aspects of the spiritual kingdom that
Christ was establishing. The Parable of the Sower (Matt. 13:3-23)
identifies those who are the righteous subjects of the kingdom:
those who rightly receive the word of God (by the sovereign
grace of God, of course). Their numbers will greatly increase,
thirty-fold, sixty-fold, and a hundred-fold.

The Parable of the Tares (Matt. 13:24-30, 36:43) and the Para-
ble of the Net (Matt. 13:47-50) point out that despite the growth
of the righteous, the kingdom will include a mixture of the
righteous and the unrighteous. These will not be separated
absolutely until the resurrection.

The Parable of the Hidden Treasure (Matt. 13:44) and the
Parable of the Pearl of Great Price (Matt. 13:45-46) speak of the
priceless value and blessings of the kingdom. The Parable of the
Mustard Seed (Matt. 13:31-32) and the Parable of the Leaven

10, A, denHartog, “Hope and the Protestant Reformed Churches’ Mission
Calling,” 166.
