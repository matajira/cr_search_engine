Pragmatic Objections 441

1935: “There is no law of cosmic progress, but only an oscilla-
tion upward and downward, with a slow trend downward on a
balance owing to the diffusion of energy. This, at least, is what
science al present regards as most probable, and in our disillu-
sioned generation it is easy to believe. From evolution, so far as
our present knowledge shows, no ultimately optimistic philoso-
phy can be validly inferred.”* It is difficult to see how such
statements could be deemed close to postmillennialism. Indeed,
North has noted similar applications of the Law of Entropy in
the Scientific Creation movement and also in certain New Age
philosophies and modern evolutionism.*

The ultimate problem with the “postmillennialism = evolu-
tionism” argument is that it identifies things that differ in funda-
mental respects. The goals, motives, and standards of evangelical
postmillennialism are clearly supernaturalistic, whereas those of
secular postmillennialism are thoroughly naturalistic. Postmil-
lennialism’s goal is the glorfication of Jesus Christ in all areas of
life; humanism’s goal is the glorification of man in‘all areas of
life. Postmillennialism’s motive is faithfulness to the resurrected
Christ; humanism’s motive is faithfulness to self-sufficient man.
Postmillennialism’s standard is the written revelation of Almighty
God in Scripture; humanism’s standard is autonomous human
reason or mystical illumination. Evangelical postmillennialism
and some forms of evolutionism expect historical progress. Yet
the nature and results of this progress are radically different.

This leads to the next objection.

“Liberal Tendencies Govern Postmillennialism”

A more popular and more simplistic exception to postmillen-
nialism is that it contains the seeds of liberalism within it. Dallas
Theological Seminary professor Robert Lightner writes:

42, Bertrand Russell, “Evolution,” in Russell, Religion and Science (New York:
Oxford University Press, [1935] 1972), p. 81.

43. North, is the World Running Down?, passim.
