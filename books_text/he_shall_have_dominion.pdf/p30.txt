XXX HE SHALL HAVE DOMINION

God to defend His covenant law through the imposition of
historical sanctions. Consistent men ask: “If God will not apply
sanctions, then how can Christians dare to apply them?” But if
God’s judicial sanctions are not applied, then Satan’s judicial
sanctions will be. There is no judicial neutrality in history.

By refusing to acknowledge either God’s revealed law or
God’s predictable corporate sanctions in history, defenders of
postmillennialism have generally abandoned a philosophy of
history. They have proclaimed a pietistic postmillennialism
rather than covenantal postmillennialism.’* They have pro-
claimed Christianity’s victory in history, but without specifying
the legal foundations of the kingdom (civilization) of God.

The Key Issue: Ethics With Historical Sanctions

Ethics cannot successfully be divorced from eschatology, but
neither can the question of God’s sanctions in history. The unified
question of ethics and corporate sanctions cannot be evaded.
The eschatological issue is this: Do Christians have legitimate
hope for the positive historical effects of their efforts, both
personal and corporate, in history? Do their sacrifices really
make a difference in history? Of course they make a difference
in eternity; this is not the question. Do Christians’ individual
and corporate efforts make a positive difference in history?

If all that Christians can accomplish in history is to present
God’s covenant lawsuits against individuals, allowing the Holy
Spirit to pull a few people out of the eternal fire, then why
should they go to college, except to serve as witnesses to college
students? Why should they become lawyers, except to witness to
lawyers? Is everything we do or build doomed to destruction,
either in some future great tribulation or in the final rebellion

14, Ray R. Sutton, “Covenantal Posumillennialism,” Covenant Renewal (Feb. 1989);
Sutton, “A Letter from Loraine; or a Covenantal View of the Millennium” Covenant
Renewal (May 1989). Copies of these two newsletters are available on request from the
Institute for Christian Economics, RO. Box 8000, Tyler, TX 75711.
