Realization 231

people for a time. Rather, it is a newly organized fulfillment of
the old body for all time. This Church is one with the Jewish
forefathers, being grafted into the Abrahamic root and partak-
ing of its sap (Rom. 11:17-18). Because of the redemptive work
of Christ “there is neither Jew nor Greek .. . for ye are all one
in Christ Jesus” (Gal. 3:28).

In Ephesians, Paul is quite emphatic on this matter. Though
in the past the Gentiles (Eph. 2:11) were “strangers to the
covenants of promise” (2:12), Christ has brought them “near”
(2:18) by breaking down the wall of separation between Jew
and Gentile “through” redemption (2:14-15), This makes one
people of two (2:16-17), who worship one God (2:18), making
the Gentiles “fellowcitizens with the saints, and of the house-
hold of God” (2:19), being built upon one foundation (2:20-22).

Conclusion

The New Testament portrait of Christ is that of a King who
has come sovereignly to establish His kingdom. At His birth
there was an outburst of hymnic joy at the coming of the long
prophesied King. In the winding down of John Baptist’s minis-
try, we are presented the Christ who has come into Messianic
fulfillment. Early in Christ’s ministry, He declared His king-
dom’s approach, and then set out to establish it through
preaching and teaching.

Upon His coronation, Christ began ruling judicially over the
nations of the earth through spiritual means rather than by the
sword. He rules representatively through His covenant people,
just as Satan rules representatively through his people. Those
who are redeemed are members of His kingdom. As they labor
for Him, they rule by spiritual and ethical power. Their goal?
To see all nations baptized in Christ. The essence of Christ’s king-
dom is spiritual and ethical, not political and racial. (This does not
deny that the kingdom has objective ethical and judicial impli-
cations; it does, in the same way that the conversion of a per-
son’s soul has objective ethical and judicial implications.)
