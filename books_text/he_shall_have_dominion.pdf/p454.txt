408 HE SHALL HAVE DOMINION

continues in Christianity. Christians are called “temples” by use of
this very Greek term, naos.”® As in the Epistle to the Hebrews,
the Temple/tabernacle here receives a heavenly replacement
(Rev. 11:19). The outer court speaks of the physical Temple, which is
to be destroyed (Matt. 24:1-2). History records that Jerusalem’s
wall “was so thoroughly laid even with the ground by those that
dug it up to the foundation, that there was left nothing to make
those that came thither believe it had ever been inhabited. This
was the end which Jerusalem came to.”**

The “forty-two months” (v. 2) or “1260 days” (v. 3) indicates
the period of the Jewish War with Rome from its formal engagement
until the Temple was destroyed. “When Vespasian arrived the
following Spring [A.D. 67] to take charge of operations, he
steadily reduced Galilee, Peraea. . . . Titus [Vespasian’s son]
began the siege of Jerusalem in April, 70... . By the end of
August the Temple area was occupied and the holy house
burned down... .”*

The “two prophets” probably represent a small body of
Christians who remained in Jerusalem to testify against it. They
are portrayed as two, in that they are legal witnesses to the
covenant curses.**

The Jerusalem Church Protected

In Revelation 12, John backs up chronologically in order to
show that the “mother” church in Jerusalem, which was being
protected from Satan, inspired resistance. This would cover the
time frame from Christ’s ministry through the Book of Acts up
until the destruction of Jerusalem.

33, 1 Cor, 3:16-17; 2 Gor, 6:16; Eph. 2:19ff; 1 Pet. 2:5.

34, Josephus, Wars 7:1:1.

35. Bruce, New Testament History, pp. 381-382

36. Deut. 17:6; 19:5; Matt, 18:16; 2 Cor. 13:1; 1 Tim. 5:19; Heb. 10:28.
