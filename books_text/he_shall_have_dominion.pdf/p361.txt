Time Frames 315

having Him crucified (Matt. 21:37-38; cf. 21:33-45; Acts 7:51-
52)."8

The second part of the couplet is directly related to the first:
Having finished the transgression against God in the rejection
of the Messiah, now the sins are sealed up (NASV marg.; chat-
ham). The idea here is, as Payne observes, to seal or to “reserve
sins for punishment.”!* Because of Israel’s rejection of Messi-
ah, God reserves punishment for her: the final, conclusive
destruction of the temple, which was reserved from the time of
Jesus’ ministry until A.D. 70 (Matt. 24:2, 34). The sealing or
reserving of the sins indicates that within the “Seventy Weeks”
Israel will complete her transgression, and with the completing
of her sin, by crucifying Christ, God will act to reserve (beyond
the seventy weeks) her sins for judgment.

The third result (beginning the second couplet) has to do
with the provision of “reconciliation for iniquity.”*" The Hebrew
word kaphar is the word for “atonement,” i-e., a covering of sin.
It clearly speaks of Christ’s atoning death, which is the ultimate
atonement to which all temple rituals looked (Heb. 9:26").
This also occurred during His earthly ministry ~ a1 His death.
The dispensationalist here prefers to interpret this result as
application rather than effecting. He sees it as subjective appro-
priation instead of objective accomplishment: “[T]he actual
application of it is again associated with the second advent as
far as Israel is concerned.”” But on the basis of the Hebrew
verb, the passage clearly speaks of the actual making reconcilia-
tion (or atonement).

18. Matt. 20:18-19; 23:37-38; 27:11-25; Mark 10:33; 15:1; Luke 18:32; 23:1-2;
John 18:28-31; 19:12, 15; Acts 2:22-23; 3:13-15a; 4:26-27; 5:30; 7:52.

19. Payne, “Goal of Daniel’s Seventy Weeks,” p. 111.

20. The definite article, which occurred before “transgression” and “sins,” is
lacking here. There it referred to the particular situation of Israel. Here it considers
the more general predicament of mankind.

21. Heb, 1:3; 7:27; 9:7-12, 26, 28; 10:9-10. See also: John 1:29; Rom. 3:25; 2
Cor, 5:19; 1 Pet. 2:24; | John 2:2.

22. Walvoord, Daniel, p. 222.
