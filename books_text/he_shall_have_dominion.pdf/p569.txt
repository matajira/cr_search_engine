Cultural Antinomianism 523

Norman Shepherd has rightly complained:

One of the most insidious weapons which Satan has been able
to wield against the advancement of the Kingdom of God is the
inculcation of the belief that though the Kingdom must be pro-
claimed throughout the world, the church really cannot expect
that such proclamation will meet with any significant degree of
success. One prominent writer in the field of inter-national
missions has given expression to the commonly held expectation
in this way: “The New Testament clearly predicts that in spite of
great victories of the Gospel amongst all nations the resistance of
Satan will continue. Towards the end it will even increase so
much that Satan, incarnated in the human person of Antichrist,
will assume once more an almost total control over disobedient
mankind (IT Thess. 2:3-12; Rev. 13).”

These words really constitute a confession of faith. More
accurately they are a confession of anti-faith — anti-faith in anti-
Christ. If we were to find them in the anti-confession of a mod-
ern Satanist cult, they would not surprise us. At least we cannot
conceive of a Satanist solemnly confessing that toward the end of
human history, the Son of God, incarnate in the person of Jesus
Christ, will assume an almost total control over obedient man-
kind,

Why are we as Christians so much more confident with re-
spect to the victory of anti-Christ than we are with respect to the
triumph of Jesus Christ? Is the worldwide dominion of Satan
toward the end ofhistory so much more obviously and unambig-
uously a revealed truth of Scripture than is the worldwide do-
minion of Jesus Christ?”

Shepherd makes an important point. The Church of the Lord
Jesus Christ needs to be armed with the Word of God in order
to fully engage her hope-filled calling. She needs theonomic
postmillennialism rather than antinomian pessimism.

27, Norman Shepherd, “Justice to Victory,” Journal of Christian Reconstruction 3:2
(Winter 1976-77) 6.
