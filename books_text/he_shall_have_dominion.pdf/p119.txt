Introduction to Postmillennialism 73

blood-bought people into the consummative and eternal form
of the kingdom. And so shall we ever be with the Lord.

Confusion Regarding Millennial Development

Unfortunately, serious errors have brought distortion into
the understanding of the historical rise of millennial views. A
recent work comments: “The early church was solidly chiliastic
until the time of Augustine.” Another boldly asserts that “the
church from the beginning was premillennial in belief.”® Still
another states that “a premillennial belief was the universal
belief in the church for two hundred and fifty years after the
death of Christ.” This is commonly heard today.

Frequently the false historical data is traceable to the serious-
ly flawed, long-discredited claims of George N. H. Peters.”
Peters commented on premillennialism in history: “Now let the
student reflect: here are éwo centuries . . . in which positively no
direct opposition whatever arises against our doctrine.”*° His
claims, though still persisting and highly regarded by some,
have been shown to be quite erroneous.*' Because my primary
concern is to provide data for tracing the rise of postmillen-
nialism, I will only briefly comment on the general historical

26. H. Wayne House and Thomas D. Ice, Dominion Theology: Blessing ar Curse?
(Portland, OR: Multnomah, 1988), p. 200.

27. Paul Enns, The Moody Handbook of Theology (Chicago: Moody Press, 1989), p.
389.

28. Pentecost, Things to Come, p. 374 (italics his). But then he quotes Schaff as
saying it was not creedally endorsed by the church, but was “widely current” among
distinguished teachers. How he leaps from “widely current” to “universal” we proba-
bly will never know.

29. George N. H. Peters, The Theacratic Kingdom, 3 vols. (New York: Funk and
Wagnalls, 1884).

30. Pentecost, Things to Come, p. 375, citing Peters, Theocratic Kingdom, 1:494-496,

$1. Walvoord calls it “a classic work.” John E Walvoord, The Millennial Kingdom
(Findley, OH: Dunham, 1959), p. 119. Other dispensationalists employ his findings.
See: Chafer, Systematic Theology, 4:270-274; J. Dwight Pentecost, Things ta Come, pp.
373-384; and Leon J. Wood, The Bible and Future Events (Grand Rapids: Zondervan,
1973), pp. 35ff.
