524 HE SHALL HAVE DOMINION

I opened this book with the words of the great Christian
psalter hymn: “Christ Shall Have Dominion.” This hymn is
based on Psalm 72. Perhaps it would be fitting at this point to
cite Psalm 72, as a reminder of the hope for the progress of the
gospel of Jesus Christ in all of human culture.

Psalm 72

“A Psalm of Solomon”

Give the king Your judgments, O God,
And Your righteousness to the king’s Son.

He will judge Your people with righteousness,
And Your poor with justice.

The mountains will bring peace to the people,
And the little hills, by righteousness.

He will bring justice to the poor of the people;
He will save the children of the ncedy,
And will break in pieces the oppressor.

They shall fear You
As long as the sun and moon endure,
Throughout all generations.

He shall come down like rain upon the mown grass,
Like showers that water the earth.

In His days the righteous shall flourish,
And abundance of peace,
Until the moon is no more.

He shall have dominion also from sea to sea,
And from the River to the ends of the earth.

Those who dwell in the wilderness will bow before Him,
And His enemies will lick the dust.

The kings of Tarshish and of the isles
Will bring presents;

The kings of Sheba and Seba
