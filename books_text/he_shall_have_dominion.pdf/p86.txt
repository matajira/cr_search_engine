40 HE SHALL HAVE DOMINION

“The premillenarians’ credibility is at a low ebb because they
succumbed to the temptation to exploit every conceivably possi-
ble prophetic fulfillment. . . . It is not likely that the situation
will change greatly.”®' In the late 1980s, there was an agree-
ment signed by a number of dispensationalists, urging against
such a lamentable situation, but the addiction continues.

Even those dispensationalists less prone to date-setting admit
the problem. Lightner comments: “Sometimes individuals who
embrace a particular view of end-time events embarrass others
who hold the same view and they even put the view in poor
light by their radical and extreme viewpoints. ... I refer partic-
ularly to date setting for Christ’s return.”"? He specifically
mentions Edgar C. Whisenant’s Why the Rapture Could Be in
1988 and Hal Lindsey’s 1980’s: Countdown to Armageddon. Tho-
mas D. Ice laments: “[J]ust this week (the week before Christ-
mas) I received in the mail from an anonymous sender, a book
entitled Blessed Hope, 1996 .. . by someone from the Houston
area named Salty Doc. You guessed it, the Rapture is slated for
1996. ... Unfortunately, both advocates and antagonists of dis-
pensationalism are woefully ignorant that the very Biblical
assumptions underlying dispensationalism are themselves hos-
tile to the date-setting of the Rapture. Much harm has been
done by the supposed friends, not to mention the critics of
dispensationalism by these distortions.”®*

51. Dwight Wilson, Armageddon Now!, p. 218. As I write these words, just a
couple of weeks after the conclusion of the Allied victory in the Gulf War of 1991, I
am aware of a large number of books pointing to Saddam Hussein and Babylon as
harbingers of the end. Christianity Today, Newsweek, and other magazines have run
articles on the flood of evangelical doom sayers. Joe Maxwell, “Prophecy Books
Become Big Sellers,” Christianity Today 34:3 (March 11, 1991) 60. Some of the titles
mentioned in Christianity Today are: John Walvoord, Armageddon, Oil and the Middle
East, an update of his 1974 book; Charles H. Dyer, The Rise of Babylon: Sign of the End
Times, Edgar C. James, Arabs, Oil and Armageddon; Charles C. Ryrie, Crisis in the
Middle East.

52. Lightner, The Last Days Handbook, p. 171. A 1991 offender is John Walvoord.

53. Thomas D. Ice, “Dispensationalism, Date-Setting and Distortion,” Biblical
Perspectives 1:5 (Sept/Oct. 1988) 1.
