538 HE SHALL HAVE DOMINION

their inheritance will endure forever. In times of disaster they
will not wither; in days of famine thcy will enjoy plenty. But the
wicked will perish: The Lorp’s enemies will be like the beauty of
the fields, they will vanish - vanish like smoke. The wicked bor-
row and do not repay, but the righteous give generously; those
the Lorn blesses will inherit the land, but those he curses will be
cut off. If the Lorp delights in a man’s way, he makes his steps
firm; though he stumble, he will not fall, for the Lorp upholds
him with his hand. I was young and now I am old, yet I have
never seen the righteous forsaken or their children begging
bread. They are always generous and lend freely; their children
will be blessed. Turn from evil and do good; then you will dwell
in the land forever. For the Lorp loves the just and will not
forsake his faithful ones. They will be protectcd forever, but the
offspring of the wicked will be cut off; the righteous will inherit
the land and dwell in it forever. The mouth of the rightcous
man utters wisdom, and his tongue speaks what is just. The law
of his God is in his heart; his feet do not slip. The wicked lie in
wait for the righteous, seeking their very lives, but the Lorp will
not leave them in their power or let them be condemned when
brought to trial. Wait for the Lorp and keep his way. He will
exalt you to inherit the land; when the wicked are cut off, you
will see it. I have seen a wicked and ruthless man flourishing like
a green tree in its native soil, but he soon passed away and was
no more; though 1 looked for him, he could not be found. Con-
sider the blameless, observe the upright, there is a future for the
man of peace. But all sinners will be destroyed; the future of the
wicked will be cut off. The salvation of the righteous comes from
the Lorn; he is their stronghold in time of trouble. The Lorp
helps them and delivers them; he delivers them from the wicked
and saves them, because they take refuge in him.

Conclusion

This same David was confident that the world’s rage against
Christ in an attempt to displace Him would bc totally unsuc-
cessful. The determination to destroy the work of the Lord
would result in its ultimate establishment and victory:
