Realization 213

Zacharias’ prophecy continues the glad tidings. He sees
Christ’s birth as bringing tidings of victory for God's people
over their enemies (Luke 1:68-71). This, again, is in fulfillment
of the Abrahamic Covenant (v. 73; cf. Rom. 15:8-12). Christ is the
sunrise that will “shine upon those who sit in darkness and the
shadow of death” (vv. 78-79). Elsewhere this refers to the Gen-
tiles (Isa. 9:1,2; Matt. 4:16). This light is later seen as a positive
force, dispelling darkness in the present age (Rom. 13:11-13; 1
John 2:8). Because Christ has come, He will bring “peace on
earth” (Luke 2:14a). It is His birth at His first coming that in-
sures the peace on earth, not His second coming (although in
the consummative New Earth this peace will come to perfect,
eternal realization‘).

The Approach of the Kingdom

In sure expectation of the fulfillment of the Old Covenant
expectations and nativity prophecies, Christ’s ministerial ap-
pearance on the scene of history is introduced with a pro-
nouncement of the nearness of the kingdom.

John Baptist, Christ’s divinely commissioned forerunner,
preached “Repent, for the kingdom of heaven is at hand”
(Matt. $:2). In Mark 1:14-15, Jesus took up the same theme:
“And after John had been taken into custody, Jesus came into
Galilee, preaching the gospel of God, and saying, ‘The time is
fulfilled, and the kingdom of God is at hand; repent and be-
lieve the gospel.’ ” This is a very important statement. Let us
note three crucial aspects of this declaration.

First, Christ asserts “the time” is fulfilled. What is “the time”
to which He refers here? The Greek term employed is kairos,
which indicates “the ‘fateful and decisive point,’ with strong,
though not always explicit, emphasis . . . on the fact that it is
ordained by God.” This “time” surely refers to the prophetical-

4. See below, pp. 299-304.
5. Gerhard Delling, “kairos,” Theological Dictionary of the New Testament, Gerhard
