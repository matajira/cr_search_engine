Anticipation 191

argument is not persuasive, however. In the first place, there is
a common use of olam (“forever/everlasting”) where it is em-
ployed of long-term temporal situations.” Secondly, it is evi-
dent that God’s covenants and promises are conditioned upon ethical
obedience, even when this is not specifically stated: no conditions—
no covenant. “It is the conditional nature of all prophecy that
makes the outcome contingent on the ethical decisions of
men.” For instance, Jonah was clearly told that Nineveh
would be overthrown in forty days (Jon. 3:4), yet God “repent-
ed” of His determination (Jon. 3:10).”

The Abrahamic Covenant was conditioned on the ethical
obligation to “keep the way of the Lord” (Gen. 18:17-19).*
This is why it was accompanied by circumcision. Israel's forfeiture of
the Land promised in the Abrahamic Covenant was clearly
possible, as God’s Word makes abundantly clear.”

Consequently, we must understand the biblical view of the
land. The land of Israel is “His holy Land” (Lev. 25:23; Psa.
78:54), It depended on His favor upon Israel (Hos. 9:3; Jer.
2:7) and His dwelling therein (Num. 35:34; Lev. 26), which
continued as long as Israel was obedient to Him (Deut. 4:40;
Isa. 1:19; Jer. 15:13-14; 17:1-4). When Israel is rejected by
God, the promise of the Land is rejected by God: sanctions.

11. Exo. 12:14; 40:15; Num. 25:13; 2 Chr. 7:16. “Figuratively also the term is
applied to objects of impressive stability and long duration, as mountains, hills (e.g.
Gen 49:26; Ilab 3:6).” James Orr, “Everlasting,” The International Standard Bible
Encyclopedia. James Orr, ed., 5 vols. (Grand Rapids: Eerdmans, [1929] 1956), 2:1041.

12. Gary North, Millennialism and Social Theory (Tyler, TX: Institute for Christian
Economics, 1990), p. 120. B. B. Warfield, “The Prophcies of St. Paul” (1886), Biblical
and Theological Studies (Philadelphia: Presbyterian & Reformed, 1952), pp. 470ff.
Sidney Greidanus, The Modern Preacher and the Ancient Text: Interpreting and Preaching
Biblical Literature (Grand Rapids: Ecrdmans, 1988), pp. 232ff.

13. Cf. also 1 Sam. 2:30; Isa. 38:1-6; Jer. 26:13-19; Joel 2:13-14. See: Kenneth
Jones, “An Amill Reply to Peters,” Journal of the Evangelical Theological Sociely 24:4
(Dec. 1981) 333-341.

14. CE Gen. 17:9-14; 29:18; 26:5; Heb. 11:8.

15. Exo. 19:5; Deut. 28:15ff; 30:5-19; Lev. 26:14f; Josh. 8:34; 24:20 1 Kgs.
2:3,4; 9:2-9; 11:11; 2 Kgs, 21:8; 1 Chr. 28:76; 2 Chr. 7:19-22; Jer. 18.
