384 HE SHALL HAVE DOMINION

tionalism! Note the following comments by dispensationalists.
Constable observes that “this section of verses contain truths
found nowhere else in the Bible. It is key to understanding
future events and it is central to this epistle.” According to
Walvoord, the Man of Lawlessness revealed here is “the key to
the whole program of the Day of the Lord.” Of 2 Thessalonians
2 Chafer notes: “though but one passage is found bearing upon
the restraining work of the Holy Spirit, the scope of the issues
involved is such as to command the utmost consideration.”
Ryrie and Feinberg employ 2 Thessalonians 2:4 as one of the
few passages used “to clinch the argument” for the rebuilding
of the Temple.“ There is no doubt that anti-postmillennial
commentators place considerable weight on this passage.

Because of its enormous difficulties, 2 Thessalonians 2? has
generated lively debate in eschatological studies. In the pessi-
mistic eschatologies of amillennialism, premillennialism, and
dispensationalism, there is frequent employment of this passage
as evidence of worsening world conditions until the final apos-
tasy. When setting forth objections against postmillennialism,
amillennialist Hoekema makes but a cursory reference to this
passage in a mere two sentences, confident that it offers a self-
evident refutation of postmillennialism.* Though a perplex-
ing passage requiring caution, there are sufficient data in it to
remove it at least as an objection to postmillennialism.

The Historical Setting

During Paul’s visit to Thessalonica he preached to the Jews

44, Thomas L. Constable, “2 Thessalonians,” Bible Knowledge Commentary: New
Testament, p. 717. Walvoord, Prophecy Knowledge Handbook, p. 493. Lewis Sperry
Chafer, Systematic Theology, 8 vols. (Dallas: Dallas Theological Seminary, 1948), 6:85.
Charles CG. Ryrie, The Basis of the Premillennial Faith (Neptune, NJ: Loizeaux Bros.,
1953), p. 151. See also: Charles Lee Feinberg, “The Jew After the Rapture,” Prophecy
and the Seventies, Feinberg, ed. (Chicago: Moody Press, 1971), p. 181.

45. Hoekema, Bible and the Puture, p. 178. See also: Hanko, “An Exegetical
Refutation of Postmillennialism,” p. 26.
