Bibliography 547

Gerstner, John H. A Primer on Dispensationalism. Phillipsburg,
NJ: Presbyterian and Reformed, 1982. A brief critique of dis-
pensationalism’s “division” of the Bible.

Jordan, James B. The Sociology of the Church. Tyler, TX: Ge-
neva Ministries, 1986. Chapter entitled, “Christian Zionism and
Messianic Judaism,” contrasts the dispensational Zionism of
Jerry Falwell, et. al. with classic early dispensationalism.

MacPherson, Dave. The Incredible Cover-Up. Medford, OR:
Omega Publications, 1975. A revisionist study of the origins of
the pre-trib rapture doctrine. He traces it to private revelations
in 1830 by a 20-year-old mystic and follower of Edward Irving,
Margaret Macdonald.

Mauro, Philip. The Seventy Weeks and the Great Tribulation.
Swengel, PA: Reiner Publishers, n.d. A former dispensationalist
re-examines prophecies in Daniel and the Olivet Discourse.

Miladin, George C. Is This Really the End?: A Reformed Analysis
of The Late Great Planet Earth. Cherry Hill, NJ: Mack Publish-
ing, 1972. A brief postmillennial response to Hal Lindsey’s
prophetic works; concludes with a defense of postmillennial
optimism.

Provan, Charles D. The Church Is Israel Now: The Transfer of
Conditional Privilege. Vallecito, CA: Ross House Books, 1987. A
collection of Scripture texts with brief comments.

Vanderwaal, C. Hal Lindsey and Biblical Prophecy. Ontario,
Canada: Paideia Press, 1978. A lively critique of dispensation-
alism and Hal Lindsey by a preterist amillennialist.

Weber, Timothy P. Living in the Shadow of the Second Coming:
American Premillennialism 1875-1982. Grand Rapids, MI: Zon-
dervan/Academie, 1983. This touches on American dispensa-
tionalism in a larger historical and social context.

Wilson, Dwight. Armageddon Now!: The Premillenarian Response
