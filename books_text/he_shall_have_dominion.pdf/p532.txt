486 HE SHALL HAVE DOMINION

Luke 22 links the Lord’s Supper with Christ’s judicial rule in
history. It is a means of exercising spiritual and covenantal
judgment among men (cf. | Cor. 11:22-34).°” The Lord’s Sup-
per draws a covenantal distinction between men, between the
saved and the lost. It appears that the express application to
His apostles is especially in His mind. The particular concern is
that their authority from Him was to be demonstrated in the
destruction of Jerusalem. By their preaching the apostles would
be “passing sentence on the twelve tribes of Israel, who would
reject their ministry as they had done his”** (1 Thess. 2:15-16;
cf. Matt. 23:32-37; Acts 2:19-20, 37-40). North observes: “Their
sitting in judgment over Israel was fulfilled representatively, yet
no less definitively, for Old Covenant Israel is no more.”?°

Acts 3:19-21

Repent therefore and be converted, that your sins may be blot-
ted out, so that times of refreshing may come from the presence
of the Lord, that He may send Jesus Christ, who was preached
to you before, whom heaven must receive until the times of
restoration of all things, which God has spoken by the mouth of
all His holy prophets since the world began.

This is a favorite passage of dispensationalists, which is
thought to establish the premillennial expectation against all
others. “But Heaven has only received Him until the time of
restitution of all things which God hath spoken by the mouth of
all holy prophets (Acts 3:21), when He shall come again, to sit
in the throne of His Father David. This again proves His com-

87. See discussion in Gary North, Millennialism and Social Theory (Tyler, TX:
Institute for Christian Economics, 1990), pp. 215ff.

$8. Thomas Scott, The Holy Bible Containing the Old and New Testaments According
to the Authorised Version: with Explanatory Notes, Practical Observations, and Copious
Marginal References (Philadelphia: J. B. Lippincott, 1868), 3:265.

39. North, Millenniatism and Social Theory, p.217. See: John Lightfoot, Commentary
on the New Testament from the Talmud and Hebraica, 4 vols. (Peabody, MA: Hendrikson,
[1658] 1989), 2:265-266.
