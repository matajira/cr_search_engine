Subject Index 573

spread of, 84, 232, 258,
349, 491
victory of, 16, 21, 26, 71,
177, 247
Gospel of the Kingdom (See:
Gospel — kingdom)
Government (See also:
Politics; Civil
Disobedience)
God ordained, 181
Grace, Covenant of (See:
Covenant — Grace)
Gradualism (See also:
Catastrophism), 71, 116,
249-253, 268, 432, 510
Great Commission
given to the Church, 218,
257
incorrect views, 235-236
postmillennialism and,
934, 501
world embracing, 27
Great Tribulation (See:
Tribulation)
Greece, 8, 10
Gulf War (See: War)

Heaven, 2, 120, 294-295, 483
Hell

absurdities, 295

denial of, 295n

gates of, 242, 259

place of torment, 2, 57,

120, 295-298

significance, 298ff
Heresy (heretical), 29
Hermas, 3n, 58, 74
Hermeneutics

apocalyptic, 158ff, 347, 404

Christian, 156, 484

cultic, 153, 444

destruction language, 155i

figurative, 209, 287, 347,
351, 418

general study, 144-174

Jewish, 157ff 484

language philosophy, 147-
149

liberal, 46, 152, 442-444

literalism, 59, 145-158,
350, 3696, 470

metaphor, 361

neutrality and, 149

Old Testament and, 43

preteristic, 159-164, 270-
306, 338-339, 348, 394

spiritualizing, 42, 147

tradition and, 43

typology, 150, 199, 464,
517

History

battleground of Satan, 13

Christian elements of
philosophy of, 10-16

complicated, 7

cyclical, 7-10

cyclical view not dead, 11n

direction, 6-7, 102, 429

dispensational view, 463

dividing point is Christ, 9,
325

end, 294ff

eschatology and (See:
Eschatology — history
and)

evolutionary view, 7, 10-11
