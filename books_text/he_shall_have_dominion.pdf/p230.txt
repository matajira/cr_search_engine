184 HE SHALL HAVE DOMINION

other words, what we have portrayed here is the constant con-
flict between the children of the devil and the children of the
kingdom.”” “This first gospel promise, therefore, despite the
terse and figurative language in which it is expressed, provides
a true perspective of the whole sweep of human history.”**
This, then, explains the struggle in history: God’s creational
purpose is being resisted. (It also helps to explain the Bible’s
concern with genealogies leading up to and culminating in
Christ, Luke 3:23-38.)

This, then, is the establishment of the covenant of grace, for
this is the first promise of a redeemer.”

Despite this great struggle in history, the fundamental point
of this poetic datum is that of the victorious issue by the woman's
seed, Christ. Later revelation in the New Testament shows that
this prophecy began to find fulfillment at the death and resur-
rection of Christ; it is not awaiting some distant beginning of
its fulfillment. Christ has already ascended to God’s throne.

Yet, citing Genesis 3:15, Hockema asserts that “the expecta-
tion of a future golden age before Christ’s return does not do
justice to the continuing tension in the history of the world
between the kingdom of God and the forces of evil.”? He
draws too much out of this terse statement. Why may we not
refer it to Christ’s first coming in the establishment of His king-
dom and Church (cf. Col. 2:15; Rom. 16:20)??8 Later revela-
tion developed the nature of the struggle and its outcome in
history, as Hoekema himself admits: “We may say that in this

23. Gerhard Charles Aalders, Genesis (Bible Student's Commentary) trans. by William
Heynen (Grand Rapids: Zondervan, 1981), 1:107, See also: Robertson, Christ of the
Covenants, pp. 97 ff.

24. Philip Edgcumbe Hughes, Interpreting Prophecy: An Essay in Biblical Perspectives
(Grand Rapids: Eerdmans, 1976), p. 11.

25. Anthony Hoekema, The Bible and the Future (Grand Rapids; Eerdmans, 1979),
p- 180.

26. 1 John 3:8; Heb. 2:14; Col. 2:14,15.

27. Hoekema, Bible and the Future, p. 180.

28. On the binding of Satan, see below: pp. 258-259, 413-415.
