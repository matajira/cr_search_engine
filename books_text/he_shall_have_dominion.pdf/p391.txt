Features 345

11:28; 24:15). The phrase “all the nations,” is epexegetical,
referring to those nations that were subsumed under the imperial
authority of Rome. The world to which the “gospel of the king-
dom was preached” was provided a witness: “the gospel which
has come to you, as it has also in all the world. . . . [T]he gospel
which you heard, which was preached to every creature under
heaven” (Col 1:6, 23; cf Acts 2:5; Rom. 1:8; 10:18).

The Abomination of Desolation. Jesus warns: “Therefore when
you see the ‘abomination of desolation,’ spoken of by Daniel the
prophet, standing in the holy place (whoever reads, let him
understand)” (Matt. 24:15). This refers to the A.D. 70 event, as
we may discern from several angles: (1) The Temple was then
standing in the “holy city” (Jerusalem, Matt. 4:5; 27:53). (2)
That Temple had just been pointed to by the disciples (Matt.
24:1), giving rise to this very discourse (Matt. 23:38-24:3). (3)
Christ pointed to that particular Temple to speak of its destruc-
tion (Matt. 24:2). (4) The specific time frame demands an A.D.
70 reference for the “abomination” (Matt. 24:34).

The “abomination of desolation,” so dreaded as to give risc
to desperate flight from the area (Matt. 24:16-20), was to occur
“in the holy place.” Surely the Temple is involved here, but the
reference is broader, speaking of both the city and the Temple. Two
problems present themselves to the Temple-only view: (1) Luke
21:20 interprets the phrase as the surrounding of the city,
which did indeed happen (Josephus, Wars 5:12:1). Jerusalem
was considered a holy place, being the capital of the “holy land”
(Zech. 2:12). (2) The original Old Testament context men-
tions both “the city and the sanctuary” (Dan. 9:26).

The events leading up to the destruction of Jerusalem and
the Temple by the Roman armies are summarily designated by
the Lord by citing Daniel’s phrase, the “abomination of desola-

23. Jerusalem is a holy place: Neh. I1:1, 18; Isa. 48:2; 52:1; 66:20; Dan. 9:16,
24; Joel 3:17. For Jewish references to Israel as the “holy land,” see: 2 Baruch 63:10;
4 Ezra 13:48; 2 Maccabees 1:7.
