Foreword XXXV

keep asking it will either go away soon or else Jesus will come
again, thereby shutting the mouths of the postmillennialists.
But neither event takes place: the postmillennialists keep asking
the question, and Jesus remains on His heavenly throne.

The amillennialists and the premillennialists agree: Chris-
tians can leave nothing of significance behind that will survive
the horrors of the satanic oppression that inevitably lies ahead.
Only the institutional Church will survive, and a besieged and
shrinking institution it will be until Jesus comes again.

Gentry says that they are wrong. Gentry says that the Bible
says they are wrong.” It is now incumbent on premillennial
and amillennial theologians to refute Gentry: point by point,
verse by verse. Silence is no longer golden.

The Link Is No Longer Missing

Dr. Gentry has already defended exegetically the compre-
hensive implications and applications of Jesus’ Great Commis-
sion.” In doing so, he has offered the culturally retreatist and
defeatist theology of pietism its most detailed exegetical chal-
lenge in the twentieth century. He has also documented in
exhaustive detail the dating of the Book of Revelation: before
A.D. 70.2" This has removed the most significant criticism of

19, Gentry is a pastor in the Presbyterian Church in America (PCA), which left
the Presbyterian Church of the U.S. (Southern Presbyterians) in the early 1970's
when the PCUS became far more liberal theologically. Gentry is an heir of the
postmillennial tradition of Southern Presbyterian theologians James Thornwell and
Robert Dabney, Both of these theologians prior to 1861 had been members of the
Presbyterian Church in the U.S.A, (Northern Presbyterians), sometimes known as the
Old School Presbyterians, whose chief theologians taught at Princeton Seminary:
Charles Hodge, A. A. Hodge, J. A. Alexander, and B. B. Warfield. They were also
postmillennial. On the postmillennialism of nineteenth-century Southern Presbyteri-
anism, see James B. Jordan, “A Survey of Southern Presbyterian Millennial Views
Before 1930,” Journal of Christian Reconstruction, III (Winter 1976-77).

20. Kenneth L. Gentry, Jr., The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, TX: Institute for Christian Economics, 1990).

21. Kenneth L. Gentry, Jr., Before Jerusalem Fell: Dating the Book of Revelation
(Tyler, TX: Institute for Christian Economics, 1989).
