Subject Index 579

Peace
false, 508i
Pax Romana, 344, 403
postmillennial, 72, 120,
268
Prince of, 205, 209, 487
prophesied, 85, 201-202,
213
Pentecost, 168
People of God, 57
Persecution of Christians
by Jews, 319, 379-380, 414
by Romans, 344n, 379,
389, 409-410, 493
throughout history, 459-
462, 526-539
Persia, 8, 138, 312
Pessimism (-istic) (See:
Eschatology — pessimistic)
Pharisees
Christ and, 161, 217, 343,
485
distortions by, 133, 517
Philosophy (See also:
History — philosophy of),
8, 9, 264
Philosophy of history (See:
History — philosophy of)
Physics (-ists), 110
Pietism, 143, 444
Pilate, 217, 487
Politics (See also:
Civil Disobedience;
Government)
cycles, 8
Messianism and, 157 .
reference to, 462, 513
retreat from, 19, 445, 513

Polycarp, 58
Population growth, 199
Postmillennialism (See:
Eschatological Systems)
Practical theology (See:
Theology — practical)
Prayer, 90, 258
Premillennialism (See:
Eschatological Systems)
Presbyterianism, 33, 78, 123-
124
Preterism (See:
Hermeneutics — preteristic)
Priest
Christian as, 166, 247
dress, 380-381, 473
Ezekiel as, 353
king distinguished from,
139, 200
Prince of Peace (See:
Peace — prince of)
Promised Land (See:
Israel - Promised Land)
Prophecy (See also:
eschatology)
Bible and, 200-209
books, xlii, 28, 29
causative power of, 14, 105
false speculation, 3, 44
fulfillment, 146-152, 367-
369
nature of, 42
new views, 4, 28
percentage of Bible, 5
popular views, 15
time clock, 41
time frames, 309-336
Prophets, xli, 60, 156, 491
