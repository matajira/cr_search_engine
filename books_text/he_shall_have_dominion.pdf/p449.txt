Revelation 403

turns over the judgment to Christ, who will open the scroll,
thus having judgment authority committed to Him (John 5:22;
cf. Rev. 5:4-7). Jesus tells Caiaphas and those later associated
with him in the crucifixion, that they shall see the “Son of Man
coming with the clouds of heaven” (Matt. 26:64). This fits well
with the Pauline imagery of the casting out of the one wife
(Hagar who is representative of the Jerusalem below) and the
taking of the other wife (Sara who is representative of the Jeru-
salem above) in Galatians 4:24ff.

As the seals are opened, the judgments begin. At the open-
ing of the first seal (the white horse) we have a picture of the
Roman army victoriously entering Israel toward Jerusalem
(Rev. 6:1-2). This cannot be Christ, because: (1) The white
horse is the only similarity with Revelation 19:11. (2) Christ is
opening the seals in heaven. (3) The Living Creatures would
not command Christ to “come!” This one is God’s “avenger”
upon Israel. The white horse indicates victory, not holiness.
God often uses the unjust to bring His judgments in history.’®

The second seal (the red horse) speaks of the eruption of
Jewish civil war (Rev. 6:3-4). In Greek, “the peace” is empha-
sized. It refers to the famous Pax Romana covering the Roman
Empire.” Hence, the significance of “rumors of wars” (Matt.
24:6) in such a peaceful cra. Josephus notes that the civil war in
the Land was worse than the carnage wrought by the Romans
themselves.’ The ¢hird seal (black horse) portrays famine
plaguing Israel (Rev. 6:5-6). Black symbolizes famine (Lam 4:8;
5:10). One of the most horrible aspects of Jerusalem’s woes was

16. Deut. 28:15,49; Isa. 10:5-6; 44:18-45:4.

17, “Building on the foundations laid by his uncle, Julius Caesar, [Augustus]
brought peace. , . . The internal peace and order which Augustus achieved endured,
with occasional interruptions, for about two centuries. Never before had all the
shores of the Mediterranean been under one rule and never had they enjoyed such
prosperity. The pax Romana made for the spread of ideas and religions over the area
where it prevailed.” Kenneth Scott Latourette, A History of Christianity, 2 vols. (2nd
ed.; San Francisco: Harper & Row, 1975), 1:21,

18. Josephus, Wars 4:3:2; 5:1:1, 5.
