370 HE SHALL HAVE DOMINION

The Antichrist

Perhaps more than any other evil figure in Scripture, the
Antichrist is one who is the most feared. Many dispensational-
ists are convinced that he is alive today. In an interview in
Eternity magazine in 1977, Hal Lindsey responded to a question
regarding the Antichrist: “[IJn my personal opinion, he’s alive
somewhere now.”? One poorly timed 1988 book was Gorbachev:
Has the Real Antichrist Come?® Of course, this sort of belief has
for generations been the tendency among dispensationalists,
who have pointed out a number of possible Antichrist candi-
dates.? One best-selling dispensationalist writes that there “is
strong evidence indeed that the Antichrist could appear very
soon — which means that the rapture may be imminent.” He
is convinced that “somewhere, at this very moment, on planet
Earth, the Antichrist is almost certainly alive.”"

Tronically, the least helpful verses for developing the dispen-
sational, premillennial, and amillennial view of the Antichrist
are the only ones that expressly mention him! “Antichrist” ap-
pears only four times in all of Scripture: 1 John 2:18, 22; 4:3;
and 2 John 7. (Walvoord in his comprehensive Prophecy Knowl-
edge Handbook does not even mention these verses in his treat-
ment of “Prophecy in 1, 2, and 3 John and the Epistle of Jude”
- or anywhere else in his 800-page work.)"*

7. Hal Lindsey interview, “The Great Cosmic Countdown: Hal Lindsey on the
Future,” Eternity (Jan. 1977) 80.

8. Robert W. Faid, Gorbachev: Has the Real Antichrist Come? (Tulsa, OK: Victory
House, 1988).

9. Dwight Wilson, Armageddon Now!: The Premillennial Response to Russia and Israel
Since 1917 (Tyler, TX: Inscicute for Christian Economics, [1977] 1991). Gary DeMar,
Last Days Madness: The Folly of Trying to Predict When Christ Will Return (Brentwood,
TN: Wolgemuth & Hyatt, 1991),

10. Dave Hunt, Peace, Prosperity, and the Coming Holocaust (Eugene, OR: Harvest
House, 1983), p. 256. Commas were not in the original dle.

11. Dave Hunt, Global Peace and the Rise of Antichrist (Eugene, OR: Harvest
House, 1990), p. 5.

12. Walvoord, Prophecy Knowledge Handbook, pp. 513ff. On the cover and beneath
the title of this massive work we read: “All the prophecies of Scripture explained in
