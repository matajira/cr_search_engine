The Pessimistic Millennial Views 57

to be a single event, not one that involves two phases. At the time
of Christ’s return there will be a general resurrection, both of
believers and unbelievers. After the resurrection, believers who
are then still alive shall be transformed and glorified. These two
groups, raised believers and transformed believers, are then
caught up in the clouds to meet the Lord in the air. After this
‘rapture’ of all belicvers, Christ will complete his descent to
earth, and conduct the final judgment. After the judgment unbe-
lievers will be consigned to eternal punishment, whereas believ-
ers will enjoy forever the blessings of the new heaven and the
new carth.'®

Engelsma adds:

As the theology of hope, the Reformed faith [amillennialism]
directs the saints’ expectation to the great good in the future that
is the genuine object of hope. This is not some event within time
and history, but the event that is the end of time and history: the
coming of Jesus Christ. . . . Faithful to its calling as the theology
of hope, the Reformed truth {amillennialism] vigorously uproots
all false hopes that spring up among Christians: earthly success;
establishing the kingdom of Christ on the earth in a carnal form,
before the Day of Christ (utopia). . . .!”

Descriptive Features. 1. The Church Age is the kingdom era
prophesied by the Old Testament prophets.'® The people of
God are expanded from Israel of the Old Testament to the
universal Church of the New Testament, becoming the Israel of
God.

2. Satan is bound during Christ's earthly ministry at His
First Coming. His binding prevents him from totally hindering

16. Hoekema, Bible and the Puture, p. 174.

17. David J. Engelsma, “The Reformed Faith - Theology of Hope,” Standard
Bearer 66:7 (Jan. 1, 1990) 149.

18. Unlike earlier amillennialists, Hoekema sees the fulfillment of the kingdom
prophecies in the New Heavens and New Earth, rather than in the Church: Bible and
the Future, ch. 20.
