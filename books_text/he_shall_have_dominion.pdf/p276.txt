230 HE SHALL HAVE DOMINION

It is true that racial Jews in great mass will be saved later in
the development of the kingdom in history (Rom. 11:11-25),
per postmillennialism.** The hermeneutical rub comes with
Jews’ being exalted over and distinguished from saved Gentiles,
and the turning back of the redemptive progress to “the weak
and beggarly elements” of the sacrificial system. As mentioned
above, Isaiah 19:19-25 expressly alludes to pagan nations that will
be brought into the kingdom on a basis of equality with righteous Jews:
“In that day Israel will be the third party with Egypt and Assyr-
ia, a blessing in the midst of the earth” (v. 23). Here the former
enemies are seen receiving an equal share of God’s favor. In
Zechariah 9:7, God speaks of His future favor upon other ene-
mies of Israel. He refers to Ekron, one of the five chief cities of
Philistia: “I will remove their blood from their mouth, and their
detestable things from between their teeth. Then they also will
be a remnant for our God, and be like a clan in Judah, and
Ekron like a Jebusite.” This Philistine enemy is to become like
“a clan in Judah.”

Israel’s demise from dominance is directly related to her
ethical conduct. Israel crucified the Messiah. Jesus makes this the
point of His Parable of the Householder mentioned above
(Matt. 21:33ff). The constant apostolic indictment against the
Jews pertained to this gross, conclusive act of covenantal rebellion.
Although it is true that the Romans were responsible for physi-
cally nailing Christ to the cross (John 18:30-31), nevertheless,
when covenantally considered, the onus of the divine curse fell
squarely upon those who instigated and demanded it: the Jews
of that generation. The Biblical record is quite clear and em-
phatic: the Jews were the ones who sought His death (Matt. 26;
27; John 11:53; 18; 19). This most heinous sin of all time, committed
by the Jewish nation, is a constant refrain in the New Testament
(Acts 2:22-28, 36; 3:13-15a; 5:30; 7:52; 1 Thess. 2:14-15).

The New ‘Testament-era Church is not a distinct body of

44. See Chapter 12, below.
