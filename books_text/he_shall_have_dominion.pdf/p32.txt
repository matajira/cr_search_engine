XXxii HE SHALL HAVE DOMINION

opinions on what the Church can expect in the future. And the
more pessimistic these expectations, the more ready those who
hold them are to imagine that the Church has very little time
remaining. Facing (they beljeve) the threat of persecution in the
future, and facing also (they believe) the inevitable (predestined)
historical trrelevance of their efforts to turn back the satanic tide,
Christians who hold to either premillennialism or amillennial-
ism place their hope in a future, discontinuous, supernatural
escape from the cares of this world, meaning an escape from
personal and institutional responsibility in this world. | do not mean
that they place their hope in death; I mean they place hope in
“getting out of life alive”: the dispensational rapture or the
amillennial Second Advent. “It’s just around the corner!”

The eschatological concern of evangelical, Protestant Chris-
tianity in the twentieth century has not been on ethics and
Christians’ responsibility — ethical cause and effect in history —
but rather on the transcending of Christians’ responsibility
through a future divine intervention into history, either to set
up Jesus’ One World State bureaucracy (premillennialism) or to
remove sinners from history by ending history (amillennialism).
The eschatological focus has been on our legitimate (because
eschatologically inevitable) escape from corporate responsibility
as Christians. The psychological motivation has been the quest
for theological justification for the Christians’ escape from any
obligation to work to extend the kingdom (civilization) of God
in history: bystander Christianity. Eschatology has been employed
to justify retroactively the fact that the Protestant Church since
1660*° has not accomplished much in the way of presenting
an explicitly biblical alternative to the competing worldviews of
the many forms of covenant-breaking. There is a reason for this
lack of an alternative: a missing link. This missing link is a
theory of cause and effect in history.

16. The restoration of Charles II to the throne of England and the rejection of
the Puritans’ holy commonwealth ideal.
