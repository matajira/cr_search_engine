The Significance of Eschatology 21

Historic premillennialism. Historic premillennialists would join
in the denial of the gospel victory theme. J. Barton Payne be-
lieves that “evil is present in our world as predicted in the Holy
Books” (of the Bible). This evil must occur because it is a fore-
cast of Christ’s imminent return.’”® Robert H. Mounce laments
that “it is difficult to see from history alone any cause for opti-
mism.” He is certain that it will be a “persecuted church [that]
will witness the victorious return of Christ”’® rather than a
world-conquering Church. George Eldon Ladd concurs: “In
spite of the fact that God had invaded history in Christ, and in
spite of the fact that it was to be the mission of Jesus’ disciples
to evangelize the entire world (Matt. 24:14), the world would
remain an evil place. False christs would arise who would lead
many astray. Wars, strife, and persecution would continue.
Wickedness would abound so as to chill the love of many.”*°

Amillennialism. Among amillennialists we discover the same
sort of despair. William Hendriksen comments that “the majori-
ty will ever be on the side of the evil one.”*! Cornelius Vander-
waal writes that “I do not believe in inevitable progress toward
a much better world in this dispensation” and God's “church
has no right to take an optimistic, triumphalistic attitude.”®?
H. de Jongste and J. M. van Krimpen are forthright in their
declaration that “there is no room for optimism: towards the
end, in the camps of the satanic and the anti-Christ, culture will
sicken, and the Church will yearn to be delivered from its

78. J. Barton Payne, Biblicat Prophecy for Today (Grand Rapids: Baker, 1978), p.
10.

79. Robert H. Mounce, The Book of Revelation (New International Commentary on the
New Testament) (Grand Rapids: Eerdmans, 1977), p. 47; cf. p. 44.

80. George Eldon Ladd, The Last Things: An Eschatology for Laymen (Grand
Rapids: Eerdmans, 1978), p. 58.

81. William Hendriksen, More Than Conquerors (Grand Rapids: Baker, [1939]
1967), p. 228.

82. Cornelius Vanderwaal, Hal Lindsey and Biblical Prophecy (St. Catherine's,
Ontario: Paideia, 1978), pp. 44, 45.
