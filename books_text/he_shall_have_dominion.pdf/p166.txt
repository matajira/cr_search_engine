120 HE SHALL HAVE DOMINION

these four observations, Christians have a genuine concern with
their objective environment.

At death, all men enter the spiritual world, the eternal realm
(either heaven or hell). But prior to our arrival in the eter-
nal state, all men live before God in the material world,
which He has created for His own glory, as the place of man’s
habitation.®' His covenant sanctions (blessings for the right-
eous; curses for the unrighteous) may, therefore, be expected in
history. That is to say, these sanctions ave predictable.

The objectivity of covenantal blessing, which undergirds the
postmillennial eschatology, is clearly set forth in Deuteronomy
28 and Leviticus 26. When God’s covenant people are faithful
to His Law-word, He will bless them in all areas of life.®
When they fail Him, His curses will pursue them to overtake
them (Deut. 28:15-68; Lev. 26:21-39).

Such blessings are alluded to in a number of places and
under a variety of images. Among these blessings are the reduc-
tion of disease, abundant food production,“ temporal ion-
gevity,” blessings upon offspring, economic prosperity,”
national stability and peace.* In fact, such passages provide
the biblical basis of progress in history, not just linear move-

59. 2 Cor. 5:8; Phil. 1:23; Luke 16:22-23. On the doctrine of hell, see: Gary
North, Sanctions and Social Theory (forthcoming). See Chapter 13, below,

60. 2 Chr. 16:9; Psa. 33:13-15; Prov. 15:3; Acts 17:28; Heb. 4:13. No U. S.
Supreme Court “right-to-privacy” decision can alter this truth.

61. Psa. 24:1; 115:16; Prov. 15:3; Dan, 5:23; Acts 25:24-31; Rev. 4:15.

62. Deut. 28:1-14; Ley. 26:3-20, 40-46. Cf Psa. 37:25; 112:1-3; Prov. 13:22.

63. Exo. 15:26; 23:25; Deut. 7:15; Psa. 103:3.

64. Sce Exo. 23:24-25; Deut. 8:7-9; Psa. 67:6; Isa. 30:23-24; 65:21-23; Jer. 31:12;
Ezek. 34:26-27; 36:29-38; Amos 9:13; Zech. 8:1 2ff.

65. Deut. 4:40; 5:33; 32:46,47; Isa. 65:20; Zech. 8:4.

66. Deut. 5:29; 7:13.

67. Deut. 7:12-16; 8:18; 28:1-15; Psa, 112:3; Prov. 13:22. See Gary North, “An
Outline of Biblical Economic Thought,” in North, An Introduction to Christian Econom-
ics (Nutley, NJ: Craig, 1973), ch. 18.

68. Josh. 1:5; Isa, 2:4; Mic, 4:3; Isa. 11:6-9.
