Time Frames 313

than a sparsely populated, unwalled village. Daniel speaks of
the command to “restore” (shub, return) Jerusalem (Dan. 9:25).
This requires that it be returned to its original integrity and
grandeur “as at the first” (Jer. 33:7). It was not until the middle
of the fifth century B.C. that this was undertaken seriously.”’

The first period of seven weeks must indicate something, for
it is set off from the two other periods. Were it not significant,
Daniel could have spoken of the sixty-nine weeks, rather than
the “seven weeks and sixty-two weeks” (Dan. 9:25). This seven
weeks (or forty-nine years) apparently witnesses the successful
conclusion of the rebuilding of Jerusalem.”

The second period of sixty-two weeks, extends from the
conclusion of the rebuilding of Jerusalem to the introduction of
the Messiah to Israel at His baptism at the beginning of His
public ministry (Dan. 9:25), sometime around A.D. 26. This
interpretation is quite widely agreed upon by conservative
scholars, being virtually “universal among Christian exe-
getes”? — excluding dispensationalists. The third period of
one week is the subject of intense controversy between dispen-
sationalism and other conservative scholarship. I will turn to
this shortly.

Interpretation of Daniel 9:24

In Daniel 9:24, the overriding, glorious expectation of the
prophecy is stated: “Seventy weeks are determined for your
people and for your holy city, to finish the transgression, to
make an end of sins, to make reconciliation for iniquity, to
bring in everlasting righteousness, to seal up vision and proph-
ecy, and to anoint the Most Holy.”

11. {bid., 2:384-911. J. Barton Payne, Encyclopedia of Biblical Prophecy (New York:
Harper & Row, 1973), pp. 388ff. C. Boutflower, Jn and Around the Book of Daniel
(London: SPCK, 1923), pp. 195ff.

12. Hengstenberg, Christology of the Old Testament, 2:894ff.
13. Montgomery, Daniel, p. 332.
