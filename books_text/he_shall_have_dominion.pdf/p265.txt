Realization 219

Christ has overcome the world’® to be “declared the Son of
God with power” (Rom. 1:3, 4). The Great Commission is truly
a postmillennial commission.

The “all” in “all authority” is here used in the distributive
sense: it speaks of every form of authority as being at His com-
mand. He does not just have the authority of moral persuasion
among individuals and in the inter-personal realm. He also has
authority in the ecelesiastical and familial, as well as in the
societal, political, economical, etc., realms. As Revelation 1:5
says of Him, in the days when John wrote, He is “the ruler of
the kings of the earth.” As Philippians 2:10 and Romans 14:11
teach, He has a Name above every name, unto which all will
bow.

Following upon this claim of universal! authority, He delivers
to His few followers the obligation and plan for universal con-
quest: “Go therefore and make disciples of all the nations, bap-
tizing them in the name of the Father and the Son and the
Holy Spirit, teaching them to observe all that I commanded
you” (vv. 19-20). The command of the resurrected Christ who
possesses “all authority” is for His followers to bring all nations’
as nations to conversion and baptism. The word ethne is em-
ployed rather than basileia to show that His concern is not for
earthly political power. The word anthropos is passed by in
demonstration of His concern for the transformation of all of
culture, rather than just individuals. This is precisely the expec-
tation of so many of the Old Testament prophecies, which
foresaw all nations flowing to Mount Zion (e.g., Isa. 2:1-4; Mic.
4:1-4), and which anticipated “no more shall any man teach his
neighbor, ‘Know the Lord, for they shall all know the Lord’ ”
(Jer. 31:34; cf. Isa. 11:9).

Not only does He command them on the basis of universal
authority, but He closes with the promise that He will be with

18. John 16:33; Eph. 1:21-22; Rew 1:5,6.
19. See Chapter 10, above.
