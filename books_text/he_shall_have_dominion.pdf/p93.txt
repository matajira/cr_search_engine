The Purpose of This Treatise 47

Christ-promoting, optimistic, culture transforming postmillenni-
al eschatology. I do not desire merely to produce an academic
work which merely presents the case for postmillennialism, but
one that will in fact promote its adoption. If there is a strong
biblical case for postmillennialism (and I believe there is), and
if that case can be convincingly presented (which I pray I will
do), then the Christian reader must let the biblical case have its
ultimate influence in his thinking. He must allow this; he must
not merely maintain his former position because of ecclesiasti-
cal, social, or familial pressures.

It is difficult to cast off one’s eschatology in order to adopt a
new one. I know; I have done it.”4 It is difficult intellectually,
as well as ecclesiastically and socially. Intellectually, an eschatol-
ogical system affects every realm of one’s theological under-
standing and philosophical worldview. A correction in eschatol-
ogy necessarily produces far-reaching effects throughout one’s
system of thought and conduct. Ecclesiastically, a systemic cor-
rection in one’s eschatological position can have disruptive
effects in some church circles (particularly those requiring
dispensational adherence among its officers). Socially, such a
change can cost one his fellowship with some Christians (again,
this is particularly true among dispensationalists who convert
from the system). Yet the Scripture urges: “Let God be true,
but every man a liar” (Rom. 3:4a). If the case for postmillen-
nialism can be effectively presented, the challenge is issued.

Practically, with the presentation of the postmillennial sys-
tem, there is set forth a challenge to Christian social activism.”
With the adoption of a full-orbed, biblical worldview based on
postmillennialism, the Christian is urged to confront secular

74, See my testimony in the Preface (entided “Why 1 Could Not Remain a
Dispensationalist”) to Bahnsen and Gentry, House Divided, pp. xlvii-hi.

75. See Gary North, Millennialism and Social Theory (Tyler, TX: Institute for
Christian Economics, 1990), pp. 254ff. This 400-page work presents a challenge to
pessimistic eschatologies regarding the call 10 practical Christian activism in the
world.
