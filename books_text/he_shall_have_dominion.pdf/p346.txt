300 HE SHALL HAVE DOMINION

pangs together until now. And not only they, but we also who
have the firstfruits of the Spirit, even we ourselves groan within
ourselves, eagerly waiting for the adoption, the redemption
[resurrection] of our body” (Rom. 8:19-23). The comprehensive
nature of sin and redemption (involving both soul and body),
demands a comprehensive new earth (spiritual and material).
For what other purpose would we be returned to our bodies via
resurrection, if we were to remain solely and forever in the
spiritual, heavenly realm?!”

Continuity and Discontinuity

There is both a continuity and a discontinuity between the
present world and the new world. Things we do righteously are
patterned after God’s Law and will. Of the righteous who enter
the eternal realm, we read: “Then I heard a voice from heaven
saying to me, ‘Blessed are the dead who die in the Lord from
now on. Yes,’ says the Spirit, ‘that they may rest from their
labors, and their works follow them’ ” (Rev. 14:13). Our tempo-
ral labor (which is commanded of God, Gen, 1:26-30) is a train-
ing ground for eternity. The biblical concept of rewards seems
to come to play in this arena: “His lord said to him, ‘Well done,
good and faithful servant; you were faithful over a few things
[in earth history], I will make you ruler over many things [in
eternity in the new earth?]. Enter into the joy of your lord’ ”
(Matt, 25:91).

As we strive to subdue the earth in a holy and spiritual fash-
ion, we approach God’s design for man. Thus, we should expect
our present cultural labors to have eternal significance, for we are
commanded, in light of Christ’s bodily resurrection, to “be
steadfast, immovable, always abounding in the work of the
Lord, knowing that your labor is not in vain in the Lord” (1
Cor. 15:58). Amillennialist Hoekema goes too far in assigning

100. See: Patrick Fairbairn, The Typology of Scripture, 2 vols. in one (Grand
Rapids: Zondervan, [n.d.] 1963), 1:350.
