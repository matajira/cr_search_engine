Features 351

The doctrine of a rebuilt Temple is so patently erroneous,
both theologically and exegetically, that it is called by some the
“Achilles’ heel of the Dispensational system of interpreta-
tion.”#! Even dispensationalists recognize that “the future func-
tion of the millennial temple (Ezekiel 40-48) has long been
problematic for dispensationalists.”*

The Dispensational View

Walvoord presents the dispensational position on Ezekiel’s
millennial Temple: “In the Millennium, apparently, sacrifices
will also be offered, though somewhat different than those
required under the Mosaic Law, but this time the sacrifices will
be memorial, much as the Lord’s Supper is a memorial in the
Church Age for the death of Christ.”” This raises an obvious
but never-answered question: A memorial to what? And why? Is
he saying that the Lord's Supper is merely a temporary sacra-
ment suitable only for the “Church Age”? It seems so.

The argument for such a Temple is ultimately due to the
literalistic hermeneutic employed by dispensationalists. It is
maintained that a symbolic interpretation of Ezekiel’s revelation
is hermeneutically flawed in that it leaves “unanswered why
such specific details were revealed” to Ezekiel. Furthermore,
Walvoord admits, “those who adopt the figurative interpreta-

1918), p. 312; Pentecost, Things to Come, p. 514; Walvoord, Prophecy Knowledge Hand-
book, pp. 198ff; Stewart and Missler, Coming Temple, p. 225.

41. O.T. Allis, Prophecy and the Church (Philadelphia: Presbyterian & Reformed,
1945), p. 248.

42. John C. Whitcomb, “Christ’s Atonement and Animal Sacrifices in Israel,”
201.

43. Walvoord, Prophecy Knowledge Handbook, p. 202. Fellow dispensationalist
Whitcomb disagrees that the sacrifices will be memorial: “[F]uture animal sacrifices
will be ‘efficacious’ and ‘expiatory' only in terms of the strict provision for ceremonial
(and thus temporal) forgiveness within the theocracy of Israel.” Whitcomb, “Christ’s
Atonement and Animal Sacrifices,” p. 210. Rut Walvoord’s view is the predominant
view in dispensationalism, as is demonstrated hy John L. Mitchell, “The Question of
Millennial Sacrifices,” Bibliotheca Sacra 110 (1953) 248ff.
