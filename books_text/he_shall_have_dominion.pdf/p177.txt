The Righteousness of God 131

role of the Mosaic law in discussions of social righteousness. In
fact, there is widespread antipathy to the Mosaic law. Yet a
strong and compelling case may be made for the use of the
Mosaic law today.

In Matthew 5:13-16, Christ calls His Church to exercise
cultural significance.” He sovereignly declares that His follow-
crs are to be “the salt of the earth.” Sait is both a preservative
and a flavor-enhancer. The imagery here is of the Church’s
calling to preserve the good of human culture and to enhance
life. We are not called to be wholly separate from the world in
the sense of avoiding involvement in it. Rather, we are to be a
vital and distinctive aspect of it, just as salt is distinctively pres-
ent in the flavoring of food. Indeed, He says that if we do not
do so we are “good for nothing” (v. 13). In short, Christ has
denied the moral legtimacy of the Separationist Model.

In verses 14-16, we are called to be “the light of the world.”
Light, the first aspect of the original Creation, is a positive and
penetrating energy that dispels darkness and brings things into
clear focus. The Christian light exhibits the glory of God (v.
16). Light is essential for life itself and for direction. Paul re-
flects this idea in Ephesians, chapters 4-5. In Ephesians 5:11, he
calls us to “expose the works of darkness.”

But these are general exhortations to holy living before God
and to the glory of God. The pair of specific normative ques-
tions remain: (1) How may we properly be the salt of the earth?
(2) How may we properly be the light of the world? Jesus gave
answers. Immediately following upon these general directives,
the Lord provides the specifics needed, when He directly
affirms the Law’s validity in Matthew 5:17-19.

Think not that T am come to destroy the law, or the prophets: [
am not come to destroy, but to fulfil. For verily I say unto you,

27. For a detailed exposition of the passage, see: Bahnsen, Theonomy in Christian
Ethies, ch. 2, and Bahnsen, No Other Standard: Theonomy and Its Critics (Tyler, TX:
Institute for Christian Economics, 1991), Appendix A: “The Exegesis of Matthew 5.”
