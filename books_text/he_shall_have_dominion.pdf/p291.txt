Expansion 245

the prince of this world will be deposed from his present ascen-
dancy.”®

The means of the restoration is immediately appended: He
will cast out the great hinderer of men, Satan,”* and will begin
redemptively drawing all men to Himself. The same word for
“draw” (elkuo) here is used in John 6:44. It speaks of the spiritu-
al drawing power of the Holy Spirit. It implies a certain
amount of resistance that is ultimately overcome. This is evident
in its usage in John 21:11, where Peter “draws” a heavy-laden
net full of large fishes to shore by himself.

The massive influence of Christ’s death is to be experienced
in history through the drawing of all men so that the world as
a system”’ might be moved back to God. This is not to be accom-
plished by political imposition, but spiritual transformation. The final
result, however, is not an each-and-every universalism of salva-
tion. Rather, it is a massive, systemic conversion of the vast
majority of men, who then progressively transform the world.

1 Corinthians 15:20-28

Here we come to one of the strongest New Testament pas-
sages supportive of postmillennialism. Paul here teaches not
only that Christ is presently enthroned, but also that He is
enthroned and ruling with a confident view to the subduing of
His enemies. (I here employ the New International Version as
our basic English translation.)

In 1 Corinthians 15:20-22, Paul speaks of the resurrection
order: Christ has been resurrected as a first-fruits promise of
our resurrection. In verses 23-24, we read further of the order
of and events associated with the resurrection: “But each in his
own turn:” Christ the first fruits; then, when he comes, those

25. R. V. G. Tasker, The Gospel According to St. fohn (Tyndate New Testament
Commentaries) (Grand Rapids: Eerdmans, 1960), p. 150,

26, For the demise of Satan, see below: pp. 258-259.
27. See the discussion of kosmos below, pp. 263-267,
