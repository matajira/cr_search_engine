494 HE SHALL HAVE DOMINION

Conclusion

For the devout Christian, the ultimate issue determining the
validity of a particular eschatological system is none other than
the Word of the Living God. If there are biblical problems with
a person’s concept of the millennium, he is in serious trouble.
Such is not the case with postmillennialists. In this chapter, I
have examined several of the leading alleged biblical objections
to postmillennialism. These, along with others that are touched
on in the main text of this book, can be adequately answered
from within the framework of postmillennialism. It seems clear
to me that the Bible itself sets before the Christian a grand
historical hope that is postmillennial in orientation. It may not
be equally clear to others. The question is: Who is clearly con-
fused? Who is clearly unclear about the details and the ethical,
judicial, and social implications of his eschatological system?

I have argued in this book that eschatology is a part of an
integrated system of biblical truth. It is not something that sits
on the sidelines of theology. Biblical eschatology reveals itself in
three ways. First, it is consistent theologically (exegetically) with
the whole of Scripture. The Bible does not contradict itself.
Second, it is internally consistent. What it teaches about the
Second Advent of Christ fits what it teaches about the kingdom
of God in history. Third, its view of the kingdom of God in
history is consistent with ethical cause and effect as described in
the Bible. I ask my critics: Does God progressively reward cov-
enant-breakers in history, while bringing covenant-keepers into
bondage to them, long term? Any eschatological system that
teaches that the unrighteous will triumph over the righteous in
Church history should also explain how this view of the future
fits God’s covenantal promises — God’s blessings and cursings in

"history — in such passages as Leviticus 26 and Deuteronomy 28.

If this book successfully challenges those who hold rival
eschatologies to go into print on these three issues, it will have
been a successful effort in my view. Public debate on fundamen-
tal theological issues is healthy. Silence is evidence of confusion.
