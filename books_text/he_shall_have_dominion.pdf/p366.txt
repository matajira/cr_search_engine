320 HE SHALL HAVE DOMINION

week begins with the baptismal anointing of Christ. Then, after
three and one-half years of ministry - the middle of the seven-
tieth week -— Christ was crucified (Luke 13:6-9; Eccl. Hist.
1:10:3). Thus, the prophecy states that by His conclusive confir-
mation of the covenant, Messiah will “bring an end to sacrifice
and offering” (v. 27) by offering up Himself as a sacrifice for sin
(Heb. 9:25-26; cf. Heb. 7:11-12, 18-22), Consequently, at His
death the Temple’s veil was torn from top to bottom (Matt.
27:51) as evidence that the sacrificial system was legally disestab-
lished in the eyes of God (cf. Matt. 23:38), for Christ is the Lamb
of God (John 1:29).

The Destruction of Jerusalem

But how are we to understand the latter portions of both
verses 26 and 27? What are we to make of the destruction of
the city and sanctuary (v. 26) and the abomination that causes
desolation (v. 27), which most non-dispensational evangelical
commentators agree occurred in A.D. 70?

In verse 26, we learn there are two events to occur after the
sixty-ninth week: (1) The Messiah is to be “cut off,” and (2) the
city and sanctuary are to be destroyed. Verse 27a informs us
that the Messiah’s cutting off (v. 26a) is a confirmation of the
covenant and is to occur at the half-way mark of the seventieth
week. So, the Messiah’s death is clearly within the time frame of
the Seventy Weeks (as we expect because of His being the ma-
jor figure of the fulfillment of the prophecy).

The events involving the destruction of the city and the
sanctuary with war and desolation (vv. 26b, 27b) are the conse-
quences of the cutting off of the Messiah and do not necessarily
occur in the seventy weeks time frame. They are an addendum to
the fulfillment of the focus of the prophecy, which is stated in
verse 24. The destructive acts are anticipated, however, in the
divine act of sealing up or reserving the sin of Israel for pun-
ishment. Israel’s climactic sin — their completing of their trans-
