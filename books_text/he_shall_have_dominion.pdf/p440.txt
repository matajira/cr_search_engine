17

REVELATION

Then he said to me, “These words ave faithful and true.” And the Lord
God of the holy prophets sent His angel to show His servants the things
which must shortly take place. (Revelation 22:6)

Introduction

Revelation stands apart from all other New Testament books
as the one pre-eminently concerned with prophetic questions.
A substantially wrong view of this capstone of biblical prophecy
is therefore inimical to any hope for a truly biblical eschatology.
Indeed, non-postmillennial scholars often point to the woes of
Revelation as contra-indicative to postmillennialism.’ Although
we cannot delve deeply into Revelation, it is important that we
at least grasp its fundamental drift and major features.? The
vantage point from which I approach Revelation is that of
preterism,’? which I introduced briefly in Chapter 8. Despite

1, For example, Floyd E. Hamilton, The Basis of the Millennial Faith (Grand
Rapids: Eerdmans, 1942), p. 33; Bruce Milne, What The Bible Teaches About the End of
the World (Wheaton, IL: Tyndale, 1979), pp. 80-81.

2. For more information see my: The Divorce of Israel: A Commentary on Revelation
(forthcoming) and David Chilton, The Days of Vengeance: An Exposition of the Book of
Revelation (Ft. Worth, TX: Dominion Press, 1987).

3. The preterit tense is the definitively past tense: completely over, finished,
done with, as in the Greek aorist tense. It is worth noting that Hal Lindsey’s scurri-
