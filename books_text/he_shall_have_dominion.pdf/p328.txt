282 HE SHALL HAVE DOMINION

be accomplished by the almighty power of God and will be one
resurrection involving all men, often designated the “general
resurrection.” This great resurrection will not be followed by a
millennial reign on earth; it is consummational, bringing to an
end the temporal order.

Such a doctrine is so clear in Scripture and so important to
orthodoxy that it is a constant refrain in the historic creeds of
the Church. This doctrine is evident in such ancient creeds as
the Apostles’ Creed, the Nicene Creed, the Council of Constan-
tinople, and the Athanasian Creed. It appears as well in later
creeds, including the Tetrapolitan Confession, the First and
Second Confessions of Basle, the Second Helvetic Confession,
the Heidelberg Catechism, the Belgic Confession, the Orthodox
Confession of 1642, the Westminster Confession of Faith,” the
Thirty-Nine Articles of the Church of England, and the Augs-
burg Confession. Of course, our primary concern here is
with the Scriptural evidence.

I will not here engage the age-old philosophical questions
relative to the difficulty of the resurrection of long-destroyed
bodies.” My concern is with issucs rclevant to systematic the-
ology regarding the general conception of the eschatological
resurrection.

15. Ancient Greeks and Romans had a conception of the afterlife, but these were
Platonic, denying the resurrection and calling for the immortality of the soul only
(Acts 17:18, 32; 26:8). For example: Socrates’ Phaedrus and Marcus Aurelius’ Medita-
tions 10; Pliny, Natural History 1:7; cf. Tertullian, Apology 48 and Against Marcion 5:9;
Origen, Against Celsus 5:14; Julian Against the Christians (known only through Cyril,
Contra Julian 1:7).

37, It is strange that a church as strongly committed to creedal theology as
Presbyterianism would tolerate any form of premillennialism. The Westminster
Standards are strongly anti-premillennial. Robert L. Dabney, Lectures in Systematic
Theology (Grand Rapids: Zondervan, [1878] 1976), p. 839.

38. Schaff, Creeds of Christendom. See: John H. Gerstner, Wrongly Dividing the Word
of Truth (Brentwood, TN: Wolgemuth and Iyait, 1991), pp. 9, 14.

39. For brief and insightful discussions see: Dabney, Lectures in Systematic Theology,

pp. 834-836; Charles Hodge, Systematic Theology, 3 vols. (Grand Rapids: Eerdmans,
[1871-1873] 1973), 3:774-780.
