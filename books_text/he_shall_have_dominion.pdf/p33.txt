Foreword XXXili

Ethical Cause and Effect: Historical Sanctions

The missing eschatological link has been a theory of New
Covenant history that is forthrightly based on ethical cause and
effect. The Old Covenant saints had such an cthics-based theory
of history, which is outlined in Leviticus 26 and Deuteronomy
28: blessings in history for those who obey God’s Bible-revealed
law, and cursings in history for those who disobey God’s Bible-
revealed law.!” Today, the premillennialists and the amillen-
nialists agree: such a system of ethical cause and effect no lon-
ger operates in New Covenant history. Thus, biblically speak-
ing, ethical cause and effect either leads nowhere in particular
(God’s random sanctions in history) or, more widely believed,
it leads to the cultural defeat of Christianity in history until
Jesus comes again in person to judge His enemies.

This is an odd view of history, theologically speaking. We
know that God backed up His prophets in the Old Covenant
cra. When they brought a covenant lawsuit, God would prose-
cute it. But, we are assured, this is no longer the case in the
New Covenant. The Church can no longer successfully invoke
such divine power in history. Question: If Jesus’ death, resur-
rection, and ascension to the right hand of God has left His
Church even more powerless than the Church was in Mosaic
Israel, then what have been the culturally significant effects (if any)
of Jesus’ death, resurrection, and ascension to the right hand of God?
Both the amillennialists and the premillennialists avoid answer-
ing this question at all costs, for they keep coming up with this
highly embarrassing answer: almost no effects whatsoever. This is
just too embarrassing to admit in public. They must be pushed,
and pushed hard, to get them to admit it. (I do the pushing.)

The postmillennialist insists that Jesus’ ascension to the
throne of God is the transcendent mark of His absolute sover-

17. Gary North, Boundaries and Dominion: The Political Economy of Leviticus (Tyler,
TX: Institute for Christian Economics, forthcoming), ch. $2: “Ethical Cause, Econom-
ic Effects.”
