410 HE SHALL HAVE DOMINION

caused by Nero’s suicide by his own sword, Roman historian
‘Tacitus reported of the Roman Civil Wars: “This was the condi-
tion of the Roman state when Servius Galba . . . entered upon
the year that was to be for Galba his last and for the state al-
most the end.”*? Roman historian Suetonius wrote of the out-
come of the Civil Wars two years later: “[T]he empire, which
for a long time had been unsettled and, as it were, drifting
through the usurpation and violent death of three emperors,
was at last taken in and given stability by the Flavian family.”
Josephus, the Jewish court historian to the Flavians, agrees: “So
upon this confirmation of Vespasian’s entire government, which
was now settled, and upon the unexpected deliverance of the
public affairs of the Romans from ruin. . . .”*

The “second beast” is a minion of the first beast (Rev. 13:11-
12). He arises from “the land” (tes ges), i.e., from within Pales-
tine. This is probably Gessius Florus, the Roman Procurator
over Israel, who caused the Jewish War.”

The Angelic Proclamation

In Revelation 14:6-8, we read the angelic proclamation of
“Babylon’s” destruction: “Then I saw another angel flying in
the midst of heaven, having the everlasting gospel to preach to
those who dwell on the earth; to every nation, tribe, tongue,
and people; saying with a loud voice, ‘Fear God and give glory
to Him, for the hour of His judgment has come; and worship
Him who made heaven and earth, the sea and springs of wa-
ter.’ And another angel followed, saying, ‘Babylon is fallen, is
fallen, that great city, because she has made all nations drink of

the wine of the wrath of her fornication.’ ” As noted in the

39. Histories 1:2, 11.
40. Vespasian i:1.
41. Josephus, Wars 4:11:5.

42. J. Stuart Russell, The Parousia: A Study of the New Testament Doctrine of Our
Lord’s Second Coming (Grand Rapids: Baker, {1887] 1983), pp. 465Ef.
