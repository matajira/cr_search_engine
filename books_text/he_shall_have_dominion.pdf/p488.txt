442 HE SHALL HAVE DOMINION

*[Pjostmillennialism found it almost impossible to stem the tide
toward liberal theology. The nonliteral method of prophetic
interpretation that both postmillennialism and amillennialism
rest on, leaves the door wide open, hermeneutically at least, for
the same kind of interpretation to be applied to other biblical
matters, such as the deity of Christ, and the authority of the
Bible.*** Walvoord argues similarly when he complains that
postmillennialism cannot resist the tendency to liberalism in
that it “lends itself to liberalism with only minor adjust-
ments.”* Pentecost agrees that there is “the trend toward lib-
eralism, which postmillennialism could not meet, because of its
spiritualizing principle of interpretation.”** This argument
equates theological liberalism with optimism, a very questionable as-
sumption. Neo-orthodox theology, existentialist to the core, was
a reaction to the optimism of the older liberalism. It is nonethe-
less equally hostile to an orthodox view of biblical revelation.
Adams notes the temptation of this sort of argument for
premillennialists, while disavowing its helpfulness: “But side-by-
side with [the postmillennialists], libcrals began announcing
similar expectations, while attributing them to very different
causes. . . . Evangelicals found it easier to attack the general
idea of a world getting better and better (held by both) than to
make methodological distinctions between conservative, super-
naturalistic postmillennialism and liberal, naturalistic modern-
ism.””” Dispensationalist Culver’s honesty in this regard is re-
freshing: “During the ‘golden age’ of American Protestant
modernism, which came to an end with World War II, mod-
ernists adopted a kind of postmillennialism to which earlier
advocates would have given no approval... . It was based more

44. Robert P Lightner, The Last Days Handbook (Nashville: Thomas Nelson,
19990), p. 84.

45. John EF Walvoord, The Millennial Kingdom (Grand Rapids: Zondervan, 1959),
p- 35; see also p. 34.

46. Pentecost, Things to Come, p. 386,

47. Adams, Time Is at Hand, p. 1.
