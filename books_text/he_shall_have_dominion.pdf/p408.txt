362 HE SHALL HAVE DOMINION

By these metaphors he promises a remarkable change of affairs;
as if God had said that he has both the inclination and the power
not only to restore his Church, but to restore it in such a manner
that it shall appear to gain new life and to dwell in a new world.
These are exaggerated modes of expression; but the greatness of
such a blessing, which was to be manifested at the coming of
Christ, could not be described in any other way. Nor does he
mean only the first coming, but the whole reign, which must be
extended as far as to the last coming.”

The transformational effect of the gospel kingdom is such
that those who are newly born of its power” are thereby con-
stituted new creatures, so that “in Christ Jesus neither circumci-
sion nor uncircumcision avails anything, but a new creation”
(Gal. 6:15). The transforming power of the gospel creates a
“new man” of two warring factions, Jew and Gentile (Eph. 2:15-
18). Gospel- transformed new creatures are to lay aside the old
self and take on the new (Eph. 4:22-23), which is “created
according to God, in righteousness and true holiness” (Eph.
4:24; cf. Col. 3:9-11). This is because they are “His workman-
ship, created in Christ Jesus for good works, which God pre-
pared beforehand that we should walk in them” (Eph. 2:10).

This glorious conception involves both a re-created “Jerusa-
lem” and “people” (Isa. 65:18-19). Interestingly, in Galatians 6
Paul speaks of the new creation in the context of a transformed
“Israel of God” existing in his day: “For in Christ Jesus neither
circumcision nor uncircumcision avails anything, but a new
creation. And as many as walk according to this rule, peace and
mercy be upon them, even upon the Israel of God” (Gal. 6:15-16;
cf. Rom. 2:28-29).” In that same epistle, he urges a commit-
ment to the “Jerusalem above” (the heavenly Jerusalem, Heb.

70. John Calvin, Commentary on the Book of the Prophet Isaiah (1559), trans. by
William Pringle, 4 vols. (Grand Rapids: Baker, {n.d.] 1979), 4:397-398.

71, John 3:3; Jms. 1:18; 1 Pet, 1:23; 1 John 2:29; 3:9; 5:1, 18.
72. See previous discussion of this passage in Chapter 8, pp. 164-172.
