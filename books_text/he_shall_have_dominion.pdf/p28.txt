xxvili HE SHALL HAVE DOMINION

accursed. And they shall build houses, and inhabit them; and
they shall plant vineyards, and eat the fruit of them. They shall
not build, and another inhabit; they shall not plant, and another
eat: for as the days of a tree are the days of my people, and mine
elect shall long enjoy the work of their hands. They shall not
labour in vain, nor bring forth for trouble; for they are the seed
of the blessed of the Lorn, and their offspring with them. (Isa.
65:17-23, emphasis added)

A postmillennialist can interpret this passage literally: a
coming era of extensive millennial blessings before Jesus returns
in final judgment. So can a premillennialist: the era after Jesus
returns to earth but before final judgment. But the amillennial-
ist cannot admit the possibility of such an era of literal, culture-
wide blessings in history. His eschatology denies any literal,
culture-wide triumph of Christianity in history. Therefore, he
has to “spiritualize” or allegorize this passage.

So, how did the author handle this passage? He didn’t. He
simply ignored it. “It isn’t in my Bible,” he seems to be saying.
In a 233-page book on the new heavens and the new earth,
there is no discussion of Isaiah 65:17-23. The Scripture index
refers the reader to pages 139 and 157. On page 139 there isa
reference to Isaiah 65:17-25, but not one word of commentary.
On page 157, there is neither a reference nor a comment. The
book is filled with thousands of Bible references, but nowhere
does the author comment on the one passage, more than any
other passage in the Bible, that categorically refutes amillennialism.
Yet this book is regarded by amillennial theologians as a schol-
arly presentation of their position. There are very few other
books that present a detailed exegetical case for amillennialism.

Most amillennial discussions of ethical cause and effect in
history are limited to the unpleasant conclusion that evil men
will get ever-more powerful culturally, while the righteous will
become progressively weaker culturally."* In other words, the

12. This was Cornelius Van Til's view, presented in his book, Common Grace
