478 HE SHALL HAVE DOMINION

In objecting to postmillennialism, amillennialist Kuiper agrees
that there will be a “parallel development of the kingdom of
light and that of darkness... . That twofold process is being
exemplified in current events.”"” Hanko concurs.*

Dispensationalists and premillennialists concur. In his treat-
ment of the Parable of the Tares, Walvoord is convinced that
“the parable does not support the postmillennial idea that the
Gospel will be triumphant and bring in a golden age.” Bar-
bieri explains the significance of the parable as indicating “in
this period between Jesus’ rejection and His future return He
the King is absent but His kingdom continues, though in a
newly revealed form. . . . This mystery period does not involve
a universal triumph of the gospel, as _postmillennialists
affirm.”®° Premillennialists Moorehead and Erickson agree.?!

A proper understanding of this parable requires its viewing
in its setting, however. It is true that this particular parable,
which is found collected among the Kingdom Parables-in Mat-
thew 13, does not overtly teach the “universal triumph of the
gospel.” But it does not need to. The Parables of the Mustard
Seed and Leaven (Matt. 13:31-33) teach that concept.”* That
is, the Mustard Seed Parable teaches that the kingdom of Christ
will grow until it dominates its setting (the world). The Leaven
Parable teaches the method of its victory: through total perme-
ation within, until the whole (world) is leavened.

Eerdmans, 1942), p. 33.

17. R. B. Kuiper, God-Centered Evangelism: A Presentation of the Scriptural Theology
of Evangelism (Grand Rapids: Baker, 1961), pp. 208-209,

18. Hanko, “An Exegetical Refutation of Postmillennialism,” p. 15.

19. Walvoord, Prophecy Knowledge Handbook, p. 373.

20. Barbieri, “Matthew,” Bible Knowledge Commentary: New Testament, pp. 50-51.
Cf. J. B. Chapman, “The Second Coming of Jesus: Premillennial,” in A. M. Hills,
Fundamental Christian Theology: A Systematic Theology, 2 vols. (Salem, OH: Schmul,
1980), 2:341.

21. Millard J. Erickson, Christian Theology, 3 vols. (Grand Rapids: Baker, 1985),
3:1216, William G. Moorehead, “Millennium,” Intemational Standard Bible Encyclopedia,
3:2053.

22, See earlier discussion in Chapter 12, pp. 237-244,
