The Hermeneutic of Scripture 169

26:28; Mark 14:24; Luke 22:20; 1 Cor. 11:25), The apostle to
the Gentiles even promotes the New Covenant as an important
aspect of his ministry (2 Cor. 3:6). He does not say he is a
minister of a “second new covenant” or “another new cove-
nant.” In short, “One Church-one New Covenant.”

Hebrews 8, on everyone’s view, cites Jeremiah’s New Cove-
nant in a context in which he is speaking to New Testament
Christians. Yet Ryrie argues that “the writer of the Epistle has
referred to both new covenants”!"* This is literalism?

Though Ryrie dogmatically affirms “Israel means Israel” via
his literalistic hermeneutic, he does so on the basis of an incon-
sistently applied principle. Elsewhere, Ryrie fails to demand
that “David” means “David.” He cites Jeremiah 30:8-9 as proof
of Messiah’s millennial reign: “They shall serve the Lord their
God, and David their king, whom I will raise up unto them.”
Then he says: “[T]he prophet meant what he said — and what
else can we believe. . . ?” He cites also Hosea 3:4-5, where
“David their king” will be sought in the millennium, then com-
ments: “Thus the Old Testament proclaims a kingdom to be
established on the earth by the Messiah, the Son of David, as the
heir of the Davidic covenant.”” This is literalism?

Other passages illustrating how the Church fulfills prophe-
cies regarding Israel are found in the New Testament. Citing
Amos 9:11-12, James says God is rebuilding the tabernacle of
David through the calling of the Gentiles (Acts 15:15ff).% In
Romans 15:8-12, Paul notes that the conversion of the Gentiles
is a “confirming of the promises to the fathers.” And at least
one of the verses brought forth as proof speaks of Christ’s
Messianic kingdom rule (Rom. 15:12). In Acts, the preaching of
the gospel touches on the very hope of the Jews, which was

96. Ryrie, Basis of the Premillennial Faith, p. 121.
97. Ibid., pp. 86-87, 88. (emphasis mine}

98. O. Palmer Robertson, “Hermeneutics of Continuity,” Continuity and Disconti-
nuity, ch. 4.
