Theological Objections 461

Even the next reference to suffering by Paul has reference to
the decaying condition of the natural world (Rom. 8:19) and is
not tied to persecutional suffering by opposition to the Chris-
tian faith. Although postmillennialism teaches the advancement
of longevity (cf. Isa. 65:17-21), nevertheless death remains
throughout the kingdom era (Isa. 65:20; 1 Cor. 15:26). The
sufferings of Romans 8 are not evidences against postmillennial-
ism, which promises the reduction to negligible proportions (at
least) of suffering for the faith. The great advances of the post-
millennial kingdom expansion, even at its glorious height, will
still not compare to the glory of the total liberty of the believer
in the resurrection as he possesses a glorified, eternal body.*

Conclusion

To assess properly the future expectations for the Church,
we must consider the specifically positive prophetic statements of
the New Testament, which I have presented in this book. These
actually set before us our divinely ordained victory-oriented
expectation for the future, rather than describing our hope
amidst present trial when it arises (as it did so universally
among our first century forefathers). Does not 1 Corinthians
15:20-28 hold before us the prospect of the universal triumph
of the gospel of Jesus Christ as He sovereignly reigns from the
right hand of God? Do not the statements of cosmic redemp-
tion set forth the confident expectation of a redecmed world
(John 3:17; 1 John 2:2)? Do we not have the right to hope that
the kingdom of God will dominate and permeate the entirety of
human life and culture (Matt. 13:31-33)? Are we not command-
ed to “make disciples of all the nations” under the absolute
authority of Christ, who is with us in the project until the end
(Matt. 28:18-20)? Does not the prospect of the redemptive New
Heavens and New Earth, which began definitively in the first

38. See discussion in Murray, Romans 1:300-302.
