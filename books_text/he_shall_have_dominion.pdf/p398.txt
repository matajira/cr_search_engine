352 HE SHALL HAVE DOMINION

tion have not agreed as to the meaning of this temple” (as if
differences of opinion were absent in dispensational discussions
of this issue!*). Here is his rationale for a rebuilt Temple:
“Though it is objectionable to some to have animal sacrifices in
the millennial scene, actually, they will be needed there because
the very ideal circumstances in which millennial saints will live
will tend to gloss over the awfulness of sin and the need for
bloody sacrifice. The sacrifices offered will therefore be a re-
minder that only by the shedding of blood and, more specifica-
lly, the blood of Christ, can sin be taken away.”*® Question:
With the death and resurrection of Jesus Christ behind us, why
should anyone in the future need a reminder other than the
Lord's Supper and the gospel message? I ask: “Do what in
remembrance of Christ?” Shed the blood of animals? This would
mean the annulment of Hebrews 9. Yet it is presented in the name
of faithfulness to the biblical text.

Problems with the Dispensational View

First, the dispensational view is hermeneutically flawed. We
have already commented on the error of the literalism of dis-
pensationalism as a basic hermeneutic (Chapter 8). What is
more, in Ezekiel we have a vision. This fact could easily militate
against literalism, because spiritual truths in the Bible are often
conceptualized ideally in visions. This approach matches well
the tendency in earlier visionary chapters in Ezekiel, where
spiritual truths are framed in terms of concrete realities. See particu-
larly Ezekiel 1-3 and 8-11 (cf. the distinction between a vision

44, Walvoord, Prophecy Knowledge Handbook, p. 202. Cf. Stewart and Missler, The
Coming Temple, pp. 2271.

45. Two prominent dispensationalists who deny a future Temple are: H. A.
Ironside, Ezekiel the Prophet (New York: Loizeaux Bros., 1949), pp. 284ff; J. Sidlow
Baxter, Explore the Book (Grand Rapids: Zondervan, 1960), p- 32ff. Some such as
Whitcomb have disputed the common explanation of the sacrifices as “memorials.”
Whitcomb, “Christ’s Atonement and Animal Sacrifices in Israel,” pp. 201-217.

46. Walvoord, Prophecy Knowledge Handbook, p. 202
