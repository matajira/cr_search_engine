Biblical Objections 481

Second, even if it does refer to the Christian faith or the
system of Christian truth, why is a negative prospect expected?
As with the Matthew 7:13-14 passage, could not Christ be seek-
ing to motivate His people, encouraging them to understand
that the answer issue forth in an optimistic prospect. In another
context, was not Peter’s answer to such a query optimistic (John
6:67, 68)? Could it not be that “the question is asked for the
purpose not of speculation but of self-examination.”*°

In point of fact, the question does not “assume” a negative answer
at all. It is not a rhetorical question. The classic Greek grammar
Funk-Blass-Debrunner notes that when an interrogative particle
is used, as in Luke 18:8, “ow is employed to suggest an affirma-
tive answer, me (meti) a negative reply. . . .”?” But neither of
these particular particles occurs here, so the implied answer to
the question is “ambiguous,”™ because the Greek word used
here (ara) implies only “anxiety or impatience.””

Third, the terminus is open to debate. Apparently, Christ
had in mind the era of His imminent coming in judgment
upon Israel, not His distant Second Advent to end history.
Christ seems clearly to speak of a soon (cf. Rev. 1:1) vindication
of [His people, who will cry out to Him (cf. Rev. 6:9-10): “I tell
you that He will avenge them speedily” (Luke 18:8a). He is
urging His disciples to endure in prayer through the trouble-
some times coming upon them, just as He does in Matthew 24,

25. For similar ethical promptings, see Warfield, “Are There Few That Be
Saved?”

26. William Hendriksen, The Gospel of Luke in New Testament Commentary (Grand
Rapids: Baker Book House, 1978), p. 818. See also: Francis Davidson, ed., New Bible
Commentary (2nd ed.; Grand Rapids: Eerdmans, 1954) p. 857.

27, Robert W. Funk, ed., F. Blass and A. Debrunner, A Greek Grammar of the New
Testament and Other Early Christian Literature (Chicago: University of Chicago Press,
1961), p. 226 (section 440).

28. Ibid.

29. William F. Arndt and F, Wilbur Gingrich, 4 Greek-English Lexicon of the New
Testament and Other Early Christian Literature (Chicago: University of Chicago Press,
1957), p. 103.
