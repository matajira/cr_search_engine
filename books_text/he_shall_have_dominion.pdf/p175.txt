The Righteousness of God 129

that all our Lord does is governed by all-wisdom and all-knowl-
edge, thus making His Word practical for all times and applica-
ble for all situations), when properly interpreted (taking into
account the full significance, purpose, and situation of the
original intent of the various laws individually considered) and
properly applied (the unfolding of redemptive history must be
taken into account and the New Testament precepts and princi-
ples must be given their full significance). Thus, the details of
the Law are essential to law-keeping (they form an essential
part of the Law, as parts to the whole), and are meant to be
equitably observed by man on the personal, social, and civil
levels of human existence. In short: “One covenant, one law!”

The focal standard of Christian theistic ethics, then, is the
Law of God. God’s Law is the transcript of His holiness, as is
evident from its own nature:

(1) The Law represents the presence of God. God’s Law is the
revelational expression of His holy character and the moral
representation of His presence to man. The summary statement
of His Law, the Ten Commandments, was written by the very
finger of God, as no other portion of Scripture was.'? Conse-
quently, bearing His own divine imprint, it necessarily shares
His moral perfections. This truth is underscored by the fact
that the Ark of the Covenant, which was housed in the Holy of
Holies in the center of Israel, contained within it the summary
of the Law of God, the Ten Commandments written on
stone.’? At the most holy place of Israel, where the Shekinah
glory of God was resident, God’s Law was housed as an expres-
sion of God’s holy presence among and will for His people.

(2) The Law reflects the character of God. The Law which He
has given to His people is a transcript of His holy character,
possessing the very moral attributes of God Himself. God is

11. Exo. 31:18; 32:16; Deut. 4:13; 9:10; 10:4.
12, Deut. 10:5; 31:25ff.
