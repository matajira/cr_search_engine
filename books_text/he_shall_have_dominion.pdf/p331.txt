Consummation 285

(Rom. 8:11, 23; Phil. 3:20).

In Colossians 1:18 and Revelation 1:5, He is called “the first
begotten from the dead.” This also implies our being begotten
from the dead in like manner to His resurrection. I have al-
ready noted that Christ was resurrected in a tangible body, the
very body in which He died, even though it was controlled by
heightened spiritual powers.*®

But dispensationalism multiplies resurrections — as they do
comings of Christ. Adams has called this phenomenon “premil-
lennial diplopia.”*” The dispensational system gets so bound
up in its conflicting programs and divergent peoples of God
that it necessarily multiplies resurrections. Most prominently
dispensationalism, with its mother premillennialism, emphasizes
two resurrections: one of the just and onc of the unjust. These
resurrections are separated by at least 1000 years, on the sole
basis of the highly wrought symbolism found in Revelation
20:1-6, What is worse, the system requirements of dispensation-
alism end up in several resurrections.

Dispensationalists teach: “The Bible knows nothing of one
future general resurrection.”* “All bodily resurrections fall
into two categories” and the first “resurrection will include
several groups: the dead saints of this Church Age (1 Thes.
4:16), the dead saints of Old Testament times (Dan. 12:2), and
martyrs of the Tribulation period (Rev. 20:4). These resurrec-
tions of the saints of all ages constitute the first resurrection
(Rev. 20:6)... .”"” The first “resurrection is made up of a

46. Hence its designation as a “heavenly” and “spiritual” body (1 Cor. 15:44, 48-
49). The description “spiritual body” (1 Cor. 15:4) does not indicate a non-material
body, but a body more directly controlled by the spirit. The same word preumatikos
is used of a Christian in 1 Cor. 2:14-15. See Geerhardus Vos, Pauline Eschatology, pp.
166-171. Dabney, Systematic Theology, pp. 832-835.

47, Jay E. Adams, The Time Is at Hand (n.p.: Presbyterian & Reformed, 1966), ch.
3.

48. James L. Boyer, For a World Like Ours: Studies in 1 Corinthians (Grand Rapids:
Baker, 1971), p. 141.

49. Ryrie, Basic Theology, p. 518.
