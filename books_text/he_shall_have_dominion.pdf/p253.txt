Anticipation 207
Additional Prophecies

Jeremiah foresees the day when the ark of the covenant will
no longer be remembered, but in which “all the nations will be
gathered before” the “throne of the Lord” (Jer. 3:16-17). The
New Covenant (initiated by Christ, Luke 22:20; 1 Gor. 11:25)
will issue forth in worldwide salvation (Jer. 31:31-34). Natural
enemies of God’s Old Testament people will be brought to
blessing in the era of the last days: Moab (Jer. 48:47), Ammon
(Jer. 49:6); Elam (Jer. 49:39).

With Isaiah, Daniel sees the expansion of the kingdom to the
point of worldwide dominion (Dan. 2:31-35, 44-45; cf. Isa. 9:6-
7). Christ’s kingdom shall crush the world.kingdom, expressed
in the Lord’s day in the Roman Empire. The Messiah’s as-
cension and session will guarantee world dominion: “I was
watching in the night visions, and behold, One like the Son of
Man, coming with the clouds of heaven! He came to the An-
cient of Days, and they brought Him near before Him. Then to
Him was given dominion and glory and a kingdom, that all
peoples, nations, and languages should serve Him. His domin-
ion is an everlasting dominion, which shall not pass away, and
His kingdom the one which shall not be destroyed” (Dan. 7:13-
14). We must notice that Daniel 7:13-14 speaks of the Christ’s
ascension to the Ancient of Days, not His return to the earth. It is
from this ascension to the right hand of God® that there will

65. See also: Egypt and Assyria (Isa. 19:23-25), the Gentiles (Isa, 49:23); Edom
(Amos 9:12); many nations (Zech. 2:11); the Philistines (Zech. 9:7).

66. Although the imagery in Daniel 2 suggests on the surface a rapid destruction
of the image, it is not uncommon for the occurrence to come about gradualistically
in prophecy (see pp. 249-253, below, on the principle of gradualism in prophecy).
“Thus the threatening against Babylon, contained in the thirteenth and fourteenth
chapters of Isaiah, if explained as a specific and exclusive prophecy of the Medo-
Persian conquest, seems to represent the downfall of the city as more sudden and
complete than it appears in history.” However, that prophecy should be “regarded
as a panorama of the fall of Babylon, not in its first inception merely, but through all
its stages all its consummation.” Alexander, {seiah, 1:30.

67. See later discussion of His present Kingship in Chapter L1, below.
