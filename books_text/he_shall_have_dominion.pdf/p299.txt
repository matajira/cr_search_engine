Expansion 253

Vastness, Not Universalism

It is sometimes mistakenly supposed that postmillennialism
implies either the ultimate salvation of all men or at least a
form of temporal universalism. Evangelical postmillennialism
teaches, rather, that “the greater part” of men will have been
saved at the outcome of history.* This is quite contrary to
amillennialism, which leaves the vast majority of men lost, and
the remnant of the saved only a minority. Premillennialism and
dispensationalism, though, may suggest that after the 1,000-
year earthly kingdom, with its enhanced fecundity, the ranks of
the saved may outstrip those of the lost.

But neither is it the case at any given point in history that all
men will be born-again Christians. Brown comments: “Have we
not evidence that during that bright period the world’s subjec-
tion to the scepter of Christ will not be quite absolute?”?
Campbell writes that the phrase “Christianized world” “does
not mean that every living person will then be a Christian, or
that every Christian will be a perfect Christian. It does surely
mean that the righteous rule and authority of Christ the King
will be recognized over all the earth.”** Boettner observes only
that “evil in all its many forms eventually will be reduced to
negligible proportions, that Christian principles will be the rule,
not the exception, and that Christ will return to a truly Chris-
tianized world.”*

The Scriptural evidence, though clearly expecting Christ’s
dominion throughout the world, also allows that there will be a
minority who will not be converted to Him. There seems to be clear
evidence for this in the events associated with Christ’s return,
which include a brief rebellion, as indicated in 2 Thessalonians

42, Loraine Boettner, The Millennium (Philadelphia: Presbyterian & Reformed,
1958), p. 30.

43. Brown, Christ's Second Coming, p. 145.

44. Roderick Gampbell, Israel and the New Covenant (Tyler, TX: Geneva Divinity
School Press, [1954] 1981), p. 298.

45. Boettner, Millennium, p. 14.
