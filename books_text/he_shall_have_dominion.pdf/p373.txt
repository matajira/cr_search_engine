Time Frames 327

The last days of Old Testament prophecy anticipated the
establishment of Mount Zion/Jerusalem as the enduring spiritu-
al and cultural influence through the era.>* This came in the
first century, with the establishment of the New Covenant phase
of the Church, the focal point of the kingdom of Christ (cf. Joel
2 with Acts 2:16ff; Heb. 12:18-27).

Because the last days have been with us since the first century-
coming of Christ, there is no days to follow. There is no mil-
lennium that will introduce another grand redemptive era in
man’s history (see discussion of “Millennium” below). With the
coming of Christ, earth history reached “epochal finality.”
The idea of the appearance of Christ as the “Last Adam” (1
Cor. 15:45) is indicative that there is no different historical age
to follow. The finality has come, though it has undergone continu-
ous development since its arrival in the ministry of Christ.”

It is primarily in the dispensational literature of the millenni-
al discussion that reference to the “last days” generates errone-
ous conclusions. Dispensationalists point to contemporary inter-
national social decline as indicative of the onset of the “last
days”: “The key that would unlock the prophetic book would
be the current events that would begin to fit into the predicted
pattern.” “The conflicts that we see in our world today are
symptoms of the day in which we live. They may be symptoms
of the last days. . . .“°* Such observations overlook the biblical

54, Isa. 2:2; 24:23; 37:32; Joel 2:32; Oba. 1:17, 21; Mic. 4:7.

55, The last day resurrection has yet to occur (Matt. 13:39-40, 49). he Great
Commission is still in effect (Matt. 28:20).

56. Vos, Pauline Eschatology, p. 28.

57, Contrary to Richard B. Gaffin, “Theonomy and Eschatology: Reflections of
Postmillennialism,” Theonomy: A Reformed Critique, William S. Barker and W. Robert
Godfrey, eds. (Grand Rapids: Zondervan, 1990), ch. 9. See my response to Gaffin:
“Whose Victory in History?” Theonomy: An Informed Response, Gary North, ed. (Tyler,
TX: Institute for Christian Economics, 1991), ch. 8.

58. Hal Lindsey, The Late Great Planet Earth (Grand Rapids: Zondervan, 1970),
p- 181. Pentecost, Things to Come, pp. 154ff.

59. John F Walvoord, “Why Are the Nations in Turmoil?” Prophecy and the
Seventies, pp. 211-212.
