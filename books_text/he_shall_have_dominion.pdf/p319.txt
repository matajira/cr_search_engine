Consummation 273,

We know that the disciples (and other believers) are with the
Lord in heaven after their deaths (Phil. 1:21-23; 2 Cor. 5:6-9).
IIence, this statement must mean IIe comes to them at their
deaths. Though Stephen’s death is unique in Scripture, it may
indicate something of Christ’s personal involvement in the
deaths of all His saints (Acts 7:59). Are we left to find our way
to heaven? Or does Christ personally receive His own into the
presence of the Father? After all, Jesus said, “I am the way, the
truth, and the life. No one comes to the Father except through
Me” (John 14:6).

He comes into the presence of the Father af His ascension, in
order to receive His kingdom. “I was watching in the night
visions, and behold, One like the Son of Man, coming with the
clouds of heaven! He came to the Ancient of Days, and they
brought Him near before Him” (Dan. 7:13). He leaves the
world so that He may “come” to the Father: “Now I am no
longer in the world, but these are in the world, and I come to
You. . . . Now I come to You, and these things I speak in the
world” (John 17:11, 13a).’

Beyond these spiritual comings and in addition to the bodily
second advent, there is another sort of coming. This is a provi-
dential coming of Christ in historical judgments upon men. In the
Old Testament, clouds are frequently employed as symbols of
divine wrath and judgment. Often God is seen surrounded with
foreboding clouds which express His unapproachable holiness
and righteousness.* Thus, God is poetically portrayed in cer-
tain judgment scenes as coming in the clouds to wreak historical
vengeance upon His enemies. For example: “The burden
against Egypt. Behold, the LORD rides on a swift cloud, and will
come into Egypt; the idols of Egypt will totter at His presence,
and the heart of Egypt will melt in its midst” (Isa. 19:1).? This

7. See: Luke 9:51; 24:51; John 8:14; 13:1, 3; 14:28; 16:28; Acts 1:10.

8. Gen. 15:17; Exo, 13:21-22; 14:19-20; 19:9, 16-19; Deut. 4:11; Job 22:14; Psa.
18:8fF; 97:2; 104:3; Isa. 19:1; Ezek. 32:7-8.

9. 2Sam. 22:8, 10; Psa. 18:7-15; 68:4, 33; 97:2-39; 104:3; Isa. 19:9; 26:21; 30:27;
