Features 349

The trumpet gathering. Matthew 24:31 portrays the ultimate
Jubilee of salvation, decorated with imagery from Leviticus 25.
Following upon the collapse of the Temple order, Christ's
“messengers” will go forth powerfully trumpeting the gospel
of salvific liberation (Luke 4:16-21; Isa. 61:1-3; cf. Lev. 25:9-10).
Through gospel preaching the elect are gathered into the king-
dom of God from the four corners of the world, from horizon
to horizon.**

The remainder of the Olivet Discourse looks beyond the
signs for “this generation” (near demonstrative) to “that” (far
demonstrative) sign-less day and hour (Matt. 24:34-36). Thus,
the Lord’s attention turns from the imminent events of that
generation to His Second Advent at the end of history.

There is abundant, clear evidence that the Great Tribulation
was an event of the first century. It punctuated the end of the
Jewish era and the Old Covenant: the separation of Christianity
from its Jewish mother, as by “birth pangs” (Matt. 24:8).

The Rebuilding of the Temple

There are a few prophecies in the Old Testament that seem
on first reading to predict a rebuilding of the Temple of Israel
at some time in the future, i.e., the New Covenant era. Among
the passages so understood are: Isaiah 56:7; 66:20-23; Jeremiah
33:18; Zechariah 14:16-21; and Malachi 3:3-4.

The concept of the Jews returning to their Land so that the
returned Messiah can rule over an exalted Jewish kingdom,
complete with a re-established Jewish Temple and the sacrificial
system, has long been attractive to dispensationalists. Some even
hold such teachings to be cardinal Scriptural truths.” John

33. “Angels” (aggeloi) should be understood here as “messengers,” as in Matt.
11:10; Mark 1:2; Luke 7:24, 27; 9:52, Chilton, Paradise Restored, pp. 103-105.

34. For the phrase “one end of heaven to the other,” see: Deut. 30:4; Neh, 1:9.
The proclamation of the gospel is to be worldwide, Isa. 45:22; Psa. 22:27; Luke
13:29; Acts 13:39,

35, Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989); Dave Hunt,
