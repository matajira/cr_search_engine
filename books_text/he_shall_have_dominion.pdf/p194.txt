148 HE SHALL HAVE DOMINION

Feinberg agrees: “Every prophecy is a part of a wonderful
scheme of revelation; for the true significance of any prophecy,
the whole prophetic scheme must be kept in mind and the
interrelationship between the parts in the plan as well.”

The dispensationalist presumption of a consistent literalism is
unreasonable. “To assert, without express authority, that pro-
phecy must always and exclusively be one or the other, is as
foolish as it would be to assert the same thing of the whole
conversation of an individual throughout his lifetime, or of
human speech in general.”"*

In addition, Ryrie’s first argument begs the question. Ryrie
argues that because God created language, “the purpose of lan-
guage itself seems to require literal interpretation” on the basis
that “it must .. . follow that He would use language and expect
man to use it in its literal, normal, and plain sense.”!” This is
not very convincing.’® Is Jesus literally a door (John 10:9)?

Finally, the dispensational practice of hermeneutics tends to
be immune to criticism by its exclusion of countervailing evi-
dence. As Poythress demonstrates, dispensationalists apply
prophecies in a non-literal way by calling them “applica-
tions” or “partial fulfillments,””’ or by classifying them as

system he holds.” Ryrie, Dispensationalism Today, p. 94,

15. Feinberg, Millenniatism, p. 40.

16. J. A. Alexander, Commentary on the Prophecies of Isaiah, 2 vols. in one (Grand
Rapids: Zondervan, [1875] 1977), 1:30.

17. A problem of which dispensationalists seem to be unaware is the question as
to whom a prophecy is “plain.” The dispensational practice is to wy to make it plain
to the 20th-century reader. What about the ancient audience to whom it was written?

18. Pentecost follows suit: “Inasmuch as God gave the Word of God as a revela-
tion to men, it would be expected that His revelation would be given in such exact
and specific terms that His thoughts would be accurately conveyed and understood
when interpreted according to the laws of grammar and speech. Such presumptive
evidence favors the literal interpretation, for an allegorical method of interpretation
would cloud the meaning of the message delivered by God to men.” Pentecost, Things
to Come, p. 10.

19. J. Dwight Pentecost, Thy Kingdom Come (Wheaton, IL: Victor, 1990), p. 80.

20. For example, Psa. 69:25 in Acts 1:20. Feinberg, Millennialism, p. 51.
