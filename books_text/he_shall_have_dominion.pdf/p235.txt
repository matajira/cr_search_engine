Anticipation 189

Thus we see the objective corporate sanction of God against sin
in the Flood, which also serves as a type of Final Judgment (2
Pet. 3:4-6). We also see God’s judicial sanctions in history in His
ordaining of capital punishment (Gen. 9:6). God’s objective
judgment therefore finds civil expression in the affairs of man.
His grant of legitimate authority to the civil government to
enforce capital punishment is based on a religious principle,
namely, the image of God in man (Gen. 9:6), and is given to
the world through the Church (i-e., Noah’s family). God or-
dains civil sanctions as a means for preserving the human race
for His redemptive purposes (cf. Rom. 13:1-4; 1 Pet. 2:13-14).

The Abrahamic Covenant

As the scarlet thread of redemption — a non-literal metaphor
— 18 progressively woven more distinctively into the fabric of
Scriptural revelation and history, the eschatological pattern of
redemptive victory becomes more evident and more specific.
The patriarchal and Mosaic eras demonstrate this fact. Here I
will survey a few of the more significant references in these cras
in order to illustrate this truth.

In Genesis, the Abrahamic Covenant continues the redemp-
tive theme begun in Genesis 3:15 and traced through Genesis
6-9. The active redemptive restoration of the fundamental relation-
ship of man with the God of the covenant is greatly intensified
through God’s establishing of His gracious covenant with Abra-
ham and his seed: “Now the Lorp had said to Abram: ‘Get out
of your country, from your family and from your father’s
house, to a land that I will show you. I will make you a great
nation; I will bless you and make your name great; and you
shall be a blessing. I will bless those who bless you, and I will
curse him who curses you’ ” (Gen. 12:1-3a).

Here we may discern three aspects of the promise: the pro-

5. This does not mean that the institutional Church has the authority to execute
criminals.
