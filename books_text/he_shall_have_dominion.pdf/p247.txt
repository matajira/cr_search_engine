Anticipation 201
Isaiah 2:1-4

In Isaiah 2, we learn that in the “last days,” there will be a
universally attractive influence of the worship of God, an inter-
national dispersion and influence of Christianity, issuing forth
in righteous living on the personal level and peace on the inter-
national level. This is because there will be judgment in history:
“He shall judge between the nations, And shall rebuke many
people; They shall beat their swords into plowshares, and their
spears into pruninghooks” (v. 4a). Isaiah indicates the “last
days” will be the era that witnesses these things — not some era
after these last days. The “last days” begin with the coming of
Christ.*! Isaiah’s younger contemporary, Micah, repeats this
prophecy almost verbatim (Micah 4:1-3).

Here the reference to “Judah and Jerusalem” stand for the
people of God, as “Israel and Judah” do in Jeremiah 31:31,
which is specifically applied to the Church in the New Testa-
ment.?? The reference to the “mountain,” the “house of the
God of Jacob,” and “Zion” refer to the Church, which, accord-
ing to the express revelation of the New Testament, is the
temple and house of God* and the earthly representation of
the city of God (Heb. 12:22; 1 Pet. 2:6") that is set on a hill
(Matt. 5:14; Heb. 12:22). Again, we must remember that it
was in Jerusalem where the historical redemption by Christ was

ment; J. A. Alexander, Commentary on the Prophecies of Isaiah.

41. That is, in the times initiated by Christ at His First Advent, Acts 2:16, 17, 24;
1 Gor. 10:11; Gal. 4:4; Heb, 1:1,2; 9:26; Jms. 5:3; 1 Pet. 1:20; 1 John 2:18; Jude 18.
For a discussion of “the last days,” see pp. 324-328, below.

42. See my previous discussion: pp. 168-169,

43. The Church is the “temple of God,” | Cor. 3:16; 6:19; 2 Cor. 6:16; Eph.
2:19-22; 1 Pet, 2:5. She is specifically designated “the house of God,” 1 Tim. 3:15;
Heb. 2:6; 1 Pet. 4:17. See my discussion on pp. 349-360, below.

44. The heavenly city of God comes to earth in the establishment of Christ's
kingdom and Church, Rev. 3:12; 21:2, 10, 14f. See my brief discussion of Revelation
21-99 on pp. 418-420, below. /

45. See discussion of “The Ifoly Mountain”: David Chilton, Paradise Restored: A
Biblical Theology of Dominion (Ft. Worth, TX: Dominion Press, 1985), ch. 4.
