Iniroduction to Postmillennialism 87

A number of statements in Book 18 of The City of God cer-
tainly give the appearance of a postmillennial optimism. Of
Nahum 1:14 and 2:1, Augustine states: “Moreover, we already
see the graven and molten things, that is, the idols of the false
gods, exterminated through the gospel, and given up to oblivi-
on as of the grave, and we know that this prophecy is fulfilled
in this very thing” (City of God 18:31). “ ‘The tents of Ethiopia
shall be greatly afraid, and the tents of the land of Midian;’ that
is, even those nations which are not under the Roman authori-
ty, being suddenly terrified by the news of Thy wonderful
works, shall become a Christian people. “Wert Thou angry at
the rivers, O Lord? or was Thy fury against the rivers? or was
Thy rage against the sea?’ This is said because He does not now
come to condemn the world, but that the world through Him
might be saved” (18:32).

He comments on Haggai 2:6: “ ‘Thus saith the Lord of
hosts, Yet one little while, and I will shake the heaven, and the
earth, and the sea, and the dry land; and I will move all na-
tions, and the desired of all nations shall come.’ The fulfillment
of this prophecy is in part already seen, and in part hoped for
in the end... . so we see all nations moved to the faith; and the
fulfillment of what follows, ‘And the desired of all nations shall
come,’ is looked for at His last coming. For ere men can desire
and wait for Him, they must believe and love Him” (City of God,
18:35). His comments on Psalm 2 could also be cited.

Medieval Postmiliennialists

Somewhat later in history, but still pre-Whitby, is the case of
the medieval Roman Catholic Joachim of Florus (1145-1202).
Several non-postmillennial scholars cite him as a postmillen-
nialist,"° due to his view of a coming outpouring of the Spirit,

86. See: Kromminga, Millennium, pp. 20; 129ff; he cites Benz, Zeitschrift fir
Kirchengeschichte, 1931. See also: W. Miller, in Philip Schaff, A Religious Encyclopedia
(rev. ed.; New York: Funk & Wagnalls, 1883), 2:1183. Ryrie, Basic Theology, p. 443.
