Consummation 287

rections. It is enlightening to note: (1) This fundamental pas-
sage is in the most highly figurative book in the Bible. Revela-
tion is widely regarded as the most difficult book of Scripture
by eminently qualified biblical scholars.** (2) The material is
taken from a scene that is manifestly figurative. It involves, for
instance, a chain binding a spiritual being (Satan). (3) This
passage is written by the very John who speaks of the resurrec-
tion only on the dast day, and involving both the just and the
unjust simultaneously (John 5:28-29, see next point).

(4) Just after the verses in question we come upon Revela-
tion 20:11-15. “If ever language expressed the doctrine of a
simultaneous and universal resurrection, surely we have it here.”
Jehn clearly is speaking of ali men, as is evident in his lan-
guage:-(a) He says the dead “small and great” will be judged.
This terminology is applied at times to believers in Revelation
(Rev. 11:18; 19:5). (b) The righteous are judged on the basis of
their works, as those here are (Rom. 2:5-6; 14:11-12; 2 Cor.
5:10). (c) He mentions the “book of life” here (Rev. 20:12, 15),
which clearly involves the righteous.

Second, the Lord’s teaching in the Kingdom Parables de-
mands that there be a general resurrection. “But he said, ‘No,
lest while you gather up the tares you also uproot the wheat
with them. ‘Let both grow together until the harvest, and at the
time of harvest I will say to the reapers, “First gather together

54, B. B. Warfield, “The Book of Revelation,” A Religious Encyclopedia, Philip
Schaff, ed., 3 vols. (NY: Funk & Wagnalis, 1883), 2:80. Milton S. Terry, Biblical
Hermeneutics (Grand Rapids: Zondervan, [n.d.] 1983), p. 466. Henry B. Swete,
Commentary on Revelation (Grand Rapids: Kregal, [1906] 1977), p. xii. G. R. Beasley-
Murray, The Book of Revelation, in New Century Bible, R. E. Clements and Matthew
Black, eds. (London: Marshall, Morgan, & Scott, 1974), p. 5. George Eldon Ladd, A
Commentary on the Revelation of John (Grand Rapids: Eerdmans, 1972), p. 10. Eduard
Wilhelm Reuss, History of the Sacred Scriptures of the New Testament (Edinburgh: T & T
Clark, 1884), p. 155. Isbon T. Beckwith, The Apocalypse of John: Studies in Introduction
(Grand Rapids: Baker, [1919] 1967), p. 1.

55. David Brown, Christ’s Second Coming: Will It Be Premillennial? (Edmonton,
Alberta: Still Waters Revival Books, [1882] 1990), p. 195.

56. Rev. 3:5, 8; 17:8; 21:27; 22:19; cf. Phil. 4:3.
