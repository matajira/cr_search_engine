Expansion 239

shadow of the branches thereof shall they dwell. And all the
trees of the field shall know that I the Lord have brought down
the high tree, have exalted the low tree.” The portrayal here is
of a universal magnificence and exaltation of the kingdom of heaven,
which will graciously provide shelter for all, when it comes to
full fruition. This seems to provide the specific backdrop of
Christ's parable, which he adapted to mustard seed imagery.
Both point to the dominance of Christ’s kingdom: the twig is
planted on a high mountain above all the trees; the mustard
seed becomes the largest plant in the garden. The Mustard
Seed Parable speaks of the extension of the kingdom in the
world.

The Parable of the Leaven reads: “The kingdom of heaven is
like leaven, which a woman took and hid in three measures of
meal till it was all leavened” (Matt. 13:33). This parable
doubtless speaks of the kingdom’s intensive progress in the
world. Leaven is a penetrative agent that diffuses itself through-
out its host from within (cf. Luke 17:20-21). The emphatic
statement is that the whole of that into which the leaven is put
(the world, cf. Matt. 13:38) will be thoroughly penetrated. The
leaven parable, then, parallels in sentiment the glorious expec-
tation for the kingdom of heaven in the other parables. The
kingdom will penetrate all (Matt. 13:33). It will produce up to a
hundred-fold return (Matt. 13:8). It will grow to great stature (Matt.
13:31-32). It will dominate the field/world (having sown the wheat
seed in the world, that world to which Christ returns will be a
wheat field, not a tare field, Matt. 13:30).

The kingdom parables, then, comport well with the victori-
ous expectation of the Old Testament. The kingdom of the God
of heaven (Dan. 2:44) will grow to dominance in the world. It
will manifest itself progressively as a true civilization, encom-

13. For an excellent treatment of this parable, see: Allis, “The Parable of the
Leaven,” Evangelical Quarterly, op. cit. 254-273.

14. Cf pp. 477-479, below, for a response to amillennialism’s view of the Parable
of the Tares.
