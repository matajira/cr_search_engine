Postmillennialism and Suffering 529

“The Bible can be interpreted as a string of God’s triumphs
disguised as disasters.”* Dispensationalist C. Sumner Wemp:
Paul “assures that persecutions await all who will live godly.”®
Historic (non-dispensational) premillennialist Edith Schaeffer:
“Job’s cry here shows an understanding that there is no bal-
anced solution until the resurrection.”! Historic premillenni-
alist George Eldon Ladd: “[I]t is clear that Jesus taught that all
his disciples could expect in the world was tribulation and
persecution. . . . The only difference between the normal role
of the Christian in the world and the time of the Great Tribula-
tion is the intensity of the persecution.” Amillennialist G. C.
Berkouwer: “There are ominous dangers and hazards connect-
ed with the service one gives in the Kingdom of God. Persecu-
tion and imprisonment, bodily harm and even death are never
far removed from man in this life.”

I could multiply examples. ‘The theme of relentless suffering
for the Church throughout history is pervasive in contemporary
Christian literature. The point is clear: the pessimistic eschatol-
ogies interpret the suffering theme in Scripture as prophetically
ordained for all times. It is not, however, predestined for all time.

The postmillennialist does not deny the expectation of per-
sonal sufffering in history. Suffering is an important feature of
God’s governance of His people. But we must consider two
important questions relative to suffering:

What role does suffering play in the divine scheme of things?

8. Herbert Schlossberg, Idals for Destruction: Christian Haith and Its Confrontation
with American Society (Washington, D.C.; Regnery, [1983] 1990), p. 304.

9. C, Sumner Wemp, “II Timothy,” Liberty Commentary on the New Testament,
Edward E. Hindson and Woodrow Michael Kroll, eds. (Lynchburg, VA: Liberty
Press, 1978), p. 626.

10. Edith Schaeffer, Afftiction (Old Tappan, NJ: Revell, 1978), p. 55.

11. George Eldon Ladd, The Last Things (Grand Rapids: Eerdmans, 19), p. 62.
See also pages 58-64.

12. G. C. Berkouwer, The Return of Christ (Grand Rapids: Eerdmans, 1972), p.
55, See also pages 115-122.
