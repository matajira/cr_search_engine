The Purpose of This Treatise 4]

For instance, dispensational theologian John F. Walvoord is
dogmatic that the rapture of the Church must be always immi-
nent, when he writes: “There is no teaching of any intervening
event. The prospect of being taken to heaven at the coming of
Christ is not qualified by description of any signs or prerequi-
site events.”** Yet he cannot resist the excitement generated
by the recent Gulf War (the “100-day war”). In an interview in
U.S.A. Today, we read his words: “Bible prophecy is being fulfil-
led every day... . Q. So the prophetic clock is ticking? A:
Yes.”°5 In one of his recent books, Walvoord includes a table
recording “Predicted Events Relating to the Nations.” Among
those “predicted events” he lists: “1. United Nations organized
as first step toward world government in 1946... . 6. Red
China becomes a military power. . .. 8. The Arab oil embargo
in 1973,” and other such “predicted events.’°°

It is likely that a continuing flood of failed expectations will
eventually sink premillennial views. It would seem that the
current decline (halted only temporarily by the Gulf War) of
premillennialism might be attributable to failed expectations.

Exposition

I have in mind several major reasons for the publication of
the present book. My first desire is to set forth in the contem-
porary debate a careful, exegetically rigorous foundation for
postmillennialism. Care will be taken to treat the major eschat-
ological passages of Scripture in establishing the case for post-
millennialism. There are some Christians, including Christian
scholars, who seem remarkably unaware of the existence of an
exegetical case for postmillennialism. Others doubt that postmil-

54. John F Walvoord, The Rapture Question (Qnd ed.; Grand Rapids: Zondervan,
1979), p. 73. See Chapter 14, below, for a critique of the imminence doctrine,

55, Barbara Reynolds, “Prophecy clock is ticking in Mideast,” U. S. A. Taday (Jan.
19, 1991), Inquiry section.

56. Walvoord, Prophecy Knowledge Handbook, p. 400.
