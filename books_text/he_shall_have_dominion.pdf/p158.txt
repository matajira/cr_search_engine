112 HE SHALL HAVE DOMINION

people.” This idea occurs a great number of times in Scrip-
ture.) The redemptive covenants are established in order to
secure a favorable relationship between God and His people.”
By means of the covenant, the covenant people become inti-
mately related to the Lord of heaven and earth.”

Covenantal development is onion-like, layer upon layer:
“[EJach successive covenant supplements its predecessors.”?*
We may easily see this in comparing the structural and thematic
continuity between the covenants.” For instance, in preparing
for the establishment of the Mosaic covenant, we learn that
“God remembered his covenant with Abraham” (Exo, 2:24),
Under the Davidic Covenant, we find reference to and deliver-

21. Gen. 17:7; Exo. 5:2; 6:7; 29:45; Lev. 11:45; 26:12,45; Deut. 4:20; 7:9; 29:13-
15; 2 Sam. 7:24; Psa. 105:9; Isa. 43:6; Jer. 24:7; 31:33; 32:38; Ezek. 11:20; 34:24;
36:28; 37:23; Hos. 1:10; Zech. 8:8; 13:9; 2 Cor. 6:18; Rev. 21:3, 7. God’s people are
his “special treasure,” His “own possession,” “his people,” and the like, Exo. 19:4,5;
Deut. 4:20; 9:26, 29; 32:9; 1 Kgs. 8:51, 53; 2 Kgs. 11:17; 2 Chr 23:16; Psa. 28:9;
33:12; 78:71; 94:14; Isa. 19:25,

22, This, of course, would not include the pre-Fall Creation Covenant.

 

 

23. The covenantal structure of redemption is reflected in the forensic terminol-
ogy associated with redemption, such as “judgment/condemnation” (krinein), “justifi-
cation” (dikaio), “imputation” (ogizomai), “judgment seat” (bema), God as “judge”
(dikaois), judgment based on “law” (nomios), etc. In Acts 16:4 the dogmata kekrimena
(“decrees having been decided upon”) is “court-terminology.” Vos, Pauline Eschatol-
ogy, p. 268.

24. Robertson, Christ of the Covenants, p, 28. An earlier dispensationalist position
was that “the dispensation of promise was ended when Israel rashly accepted the
law,” The Scofield Reference Bible (New York: Oxford University Press, [1909] 1917),
p- 20n. Rashly accepted?!? Though recanted by later dispensationalists, this bold
statement (“Israel rashly accepted the law”) well illustrates what is still a continuing
tendency in dispensationalism to a strong discontinuity between the covenants.

25. In passing, I will note only briefly that the three initial covenants could be
included in the survey to follow, as well. They arc all foundational to the outworking
of God's redemptive purpose: The Creation Covenant establishes man as the image
of God, whom God will redeem (Gen. 1:26-28). The Adamic Covenant accounts for
the sinfulness of man and the actual initiation of the redemption that will overcome
that sin (Gen. 3:15). The Noahic Covenant is a preservative for the world, so that
God’s redemptive purpose might be realized (Gen. 8:22).

26. A number of Scriptures speak of the conquest of the Promised Land under
the Mosaic covenant as a development of the Abrahamic: Exo. 3:16, 7; 6:4-8; Psa,
105:8-12, 42-45; 106:45,
