Foreword XXV

reprint but a strategically revised edition. It is nowhere identified
as such. Dr. Pentecost had the typesetter carefully superimpose
a crucial revised section. The switch is almost undetectable, yet
it is a devastating admission for dispensationalism. Here is his
revised exposition of Christ’s kingdom during the “Church
Age.” Mustard Seed: “This part of the parable stresses the great
growth of the kingdom when once it is introduced. The king-
dom will grow from an insignificant beginning to great propor-
tions” (p. 147). There is not a word about its ethical corruption.
Leaven: “When leaven is used in Scripture it frequently con-
notes evil... . Its use in the sacrifices that represent the perfec-
tion of the person of Christ (Lev. 2:1-3) shows that it is not
always so used. Here the emphasis is not on leaven as though
to emphasize its character, but rather that the leaven has been
hidden in the meal, thus stressing the way leaven works when
once introduced into the meal” (p. 148). In short, there is now no
focus on ethics: not one word about any evil effects of either the
mustard seed or the leaven. Today his focus is on the growth of
the kingdom of Christ in history — the postmillennial focus:
“The parable of the mustard and the leaven in meal, then,
stress the growth of the new form of the kingdom” (p. 148).
If Christ’s kingdom is not being corrupted in our dispen-
sation, then it is either ethically neutral (the kingdom of Christ
as ethically neutral?!?) or positive. Pentecost’s theological prob-
lem is obvious: there can be no ethical neutrality. If the necessarily
expanding kingdom of Christ is not being steadily undermined
by theological and moral perversion, then it must be growing in
righteousness. This interpretation is the postmillennial view of
the kingdom of God: expansion over time. Matthew 13 is not
discussing Satan’s kingdom; it is discussing Christ’s. Dr. Pente-
cost has very quietly overthrown the heart and soul of the
traditional dispensational system’s account of the inevitable
progress of evil in this, the “Church Age.”? Yet no one inside

9. Gary DeMar spotted this shift in early 1992. He looked up Fentecost’s section
