Characters 377

numerals. Textual scholars agree: it must be intentional.”

Although we cannot be absolutely certain, a strong and most
reasonable case may be made for the following conjecture.
John, a Jew, used a Hebrew spelling of Nero’s name in order to
arrive at the figure 666. But when Revelation began circulating
among those less acquainted with Hebrew, a well-meaning
copyist who knew the meaning of 666 might have intended to
make its deciphering easier by altering it to 616. It surely is no
mere coincidence that 616 is the numerical value of “Nero
Caesar,” when spelled in Hebrew by transliterating it from its
more common Latin spelling.

Third, the beastly image. In Revelation 13, the one behind the
666 riddle is both called and portrayed as a “beast.” Because of
its natural association, the term “beast” is often quite aptly used
figuratively of persons with a bestial nature. It is almost univer-
sally agreed that Nero was one who was possessed of a bestial
nature. Nero was even feared and hated by his own country-
men, as ancient Roman historians agree.”” The pagan writer
Apollinius of Tyana, a contemporary of Nero, specifically men-
tions that Nero was called a “beast.”

Fourth, the war with the saints. The Beast is said to “make war
with the saints and to overcome them” (Rev. 13:7). In fact, he
is said to conduct such blasphemous warfare for a specific peri-
od of time: 42 months (Rev. 13:5). The Neronic persecution,
which was initiated by Nero in A.D. 64,” was the first ever
Roman assault on Christianity, as noted by Church fathers Euse-
bius, Tertullian, Paulus Orosius, and Sulpicius Severus, as well

26. Bruce M. Metzger, A Textual Commentary on the Greek New Testament (London:
United Bible Societies, 1971), pp. 751-752.

27, Suetonius, Nero 7:1; 27:1; 12:1; 28-29; 33-35; Tacitus, Histories 4:7; 4:8; Pliny,
Natural History 7:45; 22:92; Juvenal, Satire 7:225; 10:306ff; See also: Dio, Roman
History 61:1:2; Ascension of Isaiah 4:1; Sibylline Oracles 5:30; 12:82.

28. Philostratus, Life of Apollonius 4:38.

29. Herbert B. Workman, Persecution in the Early Church (Oxford: Oxford Univer-
sity Press, [1906] 1980), p. 22; Philip Schaff, History of the Christian Church, 8 vols.
(3rd. ed.; Grand Rapids; Eerdmans, [1910] 1950), 1:379.
