Realization 227

Jesus answered, “You say rightly that I am a king. For this cause
I was born, and for this cause I have come into the world, that
T should bear witness to the truth. Everyone who is of the truth
hears My voice.” (John 18:33-37)

He presented Elis kingship in terms of meekness and lowliness
and not as a conquering, political entity. “All this was done, that
it might be fulfilled which was spoken by the prophet, saying,
Tell ye the daughter of Sion, Behold, thy King cometh unto
thee, meek, and sitting upon an ass, and a colt the foal of an
ass” (Matt. 21:4, 5). In illustration of the Emmaus Road confu-
sion, John adds regarding this triumphal entry in fulfillment of
prophecy that “these things understood not his disciples at the
first: but when Jesus was glorified, then remembered they that
these things were written of him, and that they had done these
things unto him” (John 12:15-16).

Paul picks up on and promotes the spiritual nature of the
kingdom, when he writes that “the kingdom of God is not meat
and drink; but righteousness, and peace, and joy in the Holy
Ghost” (Rom. 14:17). He disavows any carnal conception of the
kingdom. Likewise, he speaks of attaining an inheritance in the
Spiritual kingdom (the heavenly aspect of the kingdom) for those
who are righteous (1 Cor. 6:9-10; 15:50; Gal. 5:21). He even
says very plainly of the heavenly aspect of the kingdom: “Now
this I say, brethren, that flesh and blood cannot inherit the
kingdom of God; neither doth corruption inherit incorruption”
(1 Cor. 15:50).

How could it be that an earthly, political kingdom would
hold forth no inheritance for flesh-and-blood people? It is in
salvation that we are “delivered from the power of darkness,
and translated into the kingdom of his dear Son: In whom we
have redemption through his blood, even the forgiveness of
sins” (Col. 1:12, 13). But Christ’s kingdom is not exclusively
spiritual. It has cosmic implications.
