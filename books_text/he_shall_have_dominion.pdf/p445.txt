Revelation 399

comings of God in judgment upon ancient historical people
and nations. God “comes” upon Israel's enemies in general
(Psa. 18:7-15; 104:3), upon Egypt (Isa. 19:1), upon disobedient
Israel in the Old Testament (Joel 2:1, 2), and so forth. It is not
necessary that it refer to His final, Second Advental coming to
end history. This is so for the following reasons.

(1) The coming will be witnessed by “those who pierced him.”
The clear testimony of the New Testament record is that of the
guilt of the Jews of the first century.’* (2) The reference to
those who pierced him is reinforced by the designation of the
mourners. They are called “all the tribes of the earth.” Here the .
“earth” (ge) should most probably be translated “land,” i. e. the
Promised Land (see discussion below). The idea of the “tribes”
in Revelation is of Israel’s Twelve Tribes (Rev. 7:1{}. Whenever
“tribes” is applied beyond Israel, the application adds the no-
tion of “every tongue and kindred.” Furthermore, the focus of
this “tribulation” (Rev. 1:9; 7:14) is said by Christ to be Judea
(Matt. 24:16, 21). (3) This coming is expected by an inspired
writer as occurring soon. The Second Advent has not occurred
yet, while over 1,900 years have transpired since the time in
which this coming was expected “quickly” (Rev. 22:7, 12, 20).

In regard to the Jews, the Jewish War with Rome from A.D.
67 to 70 brought about the deaths of tens of thousands of the
Jews in Judea, and the enslavement of thousands upon thou-
sands more. The Jewish historian Josephus, who was an eye-
witness, records that 1,100,000 Jews perished in the siege of
Jerusalem, though this figure is disputed. J. L. von Mosheim,
the great ecclesiastical historian, wrote that “throughout the
whole history of the human race, we meet with but few, if any,
instances of slaughter and devastation at all to be compared
with this.”"!

10, See: Acts 2:22, 23, 36; 3:14, 15; 4:8-10; 5:30; Matthew 21:33-35; 23:29-34:2;
Luke 23:27-31; John 19:5-15; 1 Thess. 2:14-16.

11, John Laurence von Mosheim, History of Christianity in the First Three Centuries,
3 vols. (New York: Converse, 1854) 1:125.
