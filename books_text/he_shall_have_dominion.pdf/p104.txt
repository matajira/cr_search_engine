58 HE SHALL HAVE DOMINION

the proclamation of the gospel. This allows for the conversion
of great numbers of sinners to Christ and insures some restraint
upon evil.

3. Christ now rules spiritually in the hearts of believers.
There will be but occasional, short-lived influences of Christian-
ity on culture, where Christians live out the implications of
their faith.

4, History will gradually worsen as the growth of evil acceler-
ates toward the end. This will culminate in the Great Tribula-
tion, with the arising of a personal Antichrist.

5. Christ will return to end history, resurrect and judge all
men, and establish the cternal order. The eternal destiny of the
redeemed may be either in heaven or in a totally renovated
new earth.

Representative Adherents. In the ancient church, the following
are non-millennialists, who seem best to fit in with the amillen-
nial viewpoint: Hermas (first century), Polycarp (A.D. 69-105),
Clement of Rome (A.D. 30-100), and Ignatius (ca. A.D. 107)."°
In the modern church, we may note the following: Jay E. Ad-
ams, Louis Berkhof, G. C. Berkouwer, William E. Cox, Richard
B. Gaffin, W. J. Grier, Floyd E. Hamilton, Herman Hanko,
William Hendriksen, Jesse William Hodges, Anthony A. Hoek-
ema, Philip E. Hughes, Abraham Kuyper, R. C. H. Lenski,
George L. Murray, Albertus Pieters, Vern S. Poythress, Herman
Ridderbos, Ray Summers, E. J. Young, and Bruce K. Waltke.?°

19. This is according to the research of dispensationalist Alan Patrick Boyd, “A
Dispensational Premillennial Analysis of the Eschatology of the Post-Apostolic Fathers
(Until the Death of Justin Martyr)” (Dallas: Dallas Theological Seminary Master’s
Thesis, 1977), p. 50 (m. 1), 91-92. Premillennialist D. H. Kromminga provides
evidence in this direction, as well, in his book, The Millennium in the Church: Studies in
the History of Christian. Chiliasm (Grand Rapids: Eerdmans, 1945), pp. 267ff. See also:
Louis Berkhof, The History of Christian Doctrine (Edinburgh: Banner of Truth, [1937]
1969).

20. Adams, The Time Is at Hand (1966). Louis Berkhof, The Second Coming of Christ
(1953). G. C. Berkouwer, The Return of Christ (1972). William E. Cox, Amillennialism
Today (1966). Richard B. Gaffin, “Theonomy and Eschatology: Reflections on Postmil-
lennialism” in William S. Barker and W. Robert Godfrey, eds., Theonemy: A Reformed
