26 HE SHALL HAVE DOMINION

role it plays in Scripture, which holds priority in the developing
of a truly Christian worldview. It is also crucial to the develop-
ment of a distinctively Christian philosophy of history, which is
fundamental to the Christian understanding of the here and
now. In addition, eschatology significantly impacts the Chris-
tian’s cultural endeavors because it sets before the Christian the
foreordained pattern of the future. If that pattern is one of
pessimism, it will tend to discourage and thwart the Christian
social enterprise.’

In this work, I will set forth a biblical eschatology that gives
prominence to the gospel victory theme. ‘The optimistic eschat-
ological perspective from which I write is that of postmillennial-
ism — a postmillennialism generated neither by a contemporary
Reagan-era optimism nor by a Kierkegaardian leap of faith, but
by a careful exegetical and theological study of the eschatolog-
ical data of Scripture.

I believe, with Roderick Campbell, that “the church today
needs this kind of vision — the vision of her reigning Lord with
all the resources of heaven and earth under His command for
the help and protection of His church and the ingathering of
His elect.”' In the Foreword to that book, O. T. Allis wrote:

[M]y own studies in this and related fields have convinced me
that the most serious error in much of the current ‘prophetic’
teaching of today is the claim that the future of Christendom is
to be read not in terms of Revival and Victory, but of growing
impotence and apostasy, and that the only hope of the world is
that the Lord will by His visible coming and reign complete the
task which He has so plainly entrusted to the church. This claim.
... is pessimistic and defeatist. I hold it to be unscriptural. The
language of the Great Commission is world-embracing; and it
has back of it the authority and power of One who said: “All

101. See: North, Js the World Running Down? and James B. Jordan, ed., Christiani-
ty and Civilization 1 (Spring 1982): “The Failure of American Baptist Culture.”
102. Campbell, Israet and the New Covenant, p. 79.
