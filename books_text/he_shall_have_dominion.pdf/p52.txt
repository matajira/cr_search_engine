6 HE SHALL HAVE DOMINION

McDonald boldly asserts of Jesus’ teaching: “It is much more
than a mere paradox to say that the first things in the Gospels
is their presentation of the last things. Their theology, like any
sound theology which is true to its biblical perspective, involves
an eschatology, a doctrine of end events.””

For the evangelical Christian, the Scripture holds a domi-
nant sway over his worldview.'* With such a heavy biblical em-
phasis on matters of eschatological significance, we should not
overlook this field of study. In fact, this matter leads us to our
next, related concern: the philosophy of history.

The Christian Philosophy of History

Does history have any meaning, purpose, or significance? Is
there a unified movement in history? Is history going any-
where? These are important questions for us as we begin a
study of biblical eschatology; the first two prepare for and the
last one speaks of cosmic eschatology. After all, the issue of
eschatology is “not just one of how to interpret Rev. 20, but one
that bears on the entire philosophy of history.”

Carl Henry observes that “Judeo-Christian revelation has
nurtured a universal conviction that no theology or philosophy
can be comprehensive unless it deals with the direction of histo-
ry and the goal of the universe, with the matter of man’s ulti-
mate destiny and the problem of death.””° Vos notes:

It is no wonder that such energetic eschatological thinking tend-
ced towards consolidation in an orb of compact theological struc-
ture. For in it the world-process is viewed as a unit. The end is

17, H. D. McDonald, Living Doctrines of the New Testament (Grand Rapids: Zonder-
van, [1971] 1972), p. 116.

18. See Chapter 5, below.

19. G. C, Berkouwer, The Return of Christ (Grand Rapids: Eerdmans, 1972), p.
234n,

20. Carl K H. Henry, God Wha Stands and Stays: Part Two, in Henry, God, Revela-
tion, and Authority, 6 vols. (Waco, TX: Word, 1983), 6:492.
