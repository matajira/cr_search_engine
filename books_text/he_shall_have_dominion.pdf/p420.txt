374 HE SHALL HAVE DOMINION

Antichrist is not an individual, malevolent ruler looming in our
future. Rather, Antichrist was a contemporary heretical tendency
regarding the person of Christ that was current among many in
John’s day. Hoekema is mistaken when he writes: “Yet it would
not be correct to say that John had no room in his thinking for
a personal antichrist, since he still looks for an antichrist who is
coming.””° As we shall see below, the Beast of Revelation and
the Man of Lawlessness were also contemporary realities in the
first century - though distinct from Antichrist.

The Beast of Revelation

Next to Antichrist, the Beast of Revelation is probably one of
the best-known eschatological images in Scripture." Much has
been written about him — much of it is worthless because a
fundamental element necessary to properly identifying the
Beast is often glossed over. That element is the exegetical deter-
mination of John’s own expectations regarding the timing of
the events of Revelation.

The Beast’s Time

As I showed in Chapter 8, John clearly expected the events
to occur in his day. Revelation opens and closes with anticipa-
tion of the imminent occurrence of the events specified within.
Revelation 1:la reads: “The Revelation of Jesus Christ, which
God gave Him to show His servants; things which must shortly
take place.” Revelation 22:10 warns: “Do not seal the words of
the prophecy of this book, for the time is at hand.”

In light of the original significance of Revelation to its first-
century audience (Rev. 1:4, 11; 2-3), the Beast must be some-
one of relevance to that audience. Revelation 13 portrays him
as a horrible and powerful foe of God’s people and of all righ-

20. Ihid., p. 158.

21, For a more detailed argument see my book, The Beast of Revelation (Tyler,
TX: Institute for Christian Economics, 1989).
