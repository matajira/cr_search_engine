196 HE SHALL HAVE DOMINION

Psalm 2

Particularly significant in this regard are the Messianic
Psalms. In Psalm 2, Jehovah God laughs at the opposition of
man to Him and to His Messiah.” Psalm 2:2 and Daniel 9:26
show that the term “messiah” (i.e., anointed one) was commonly
understood to designate the great Deliverer and King.” Kings
were “anointed” in the Old Testament.” “King” and “Messi-
ah” are used interchangeably in certain places in Scripture
(John 1:41, 49; Mark 15:32; Luke 23:2).

According to Peter, the opposition of the “nations” to “the
Lord and His Messiah” includes the Jews (Acts 4:25-287!) and
occurred in the ministry of Christ, at His crucifixion. In Heb-
rews 1:5 and 5:5, Christ is seen as already having fulfilled
Psalm 2:7, which occurred at the resurrection (Acts 13:33).
Consequently, Psalm 2 can be neither an amillennial nor a
premillennial proof-text. The scene of this resistance is on the
earth (contra amillennialism) and in the past (contra premillen-
nialism). The scene of God’s victory over rebels is also in history.

Rather than at the Second Advent,” this Psalm’s fulfill-

27. For the theonomic implications of Psalm 2, see: Greg L. Bahnsen, “The
Theonomic Position,” God and Politics: Four Views on the Reformation of Civil Govem-
ment, Gary Scott Smith, ed. (Phillipsburg, NJ: Presbyterian & Reformed, 1989), pp.
28-30.

28. J. A. Alexander, The Psalms Translaied and Explained (Grand Rapids: Baker,
[1873] 1977), p. 13.

29. 1 Sam. 10:1; 16:13,14; 2 Sam. 2:4, 7; 3:39; 5:3, 17; 12:7; 19:10; 1 Kgs. 1:39,
45; 5:1; 2 Kgs. 9:3, 6, 12; 11:12; 23:30; 1 Chr. 11:3; 14:8; 29:22; 2 Chr 23:11; Psa.
18:50.

30. See also the close assaciation of kingdom and Christ in Acts 8:12; Eph, 5:5;
2 Tim. 4:1; Rev, 11:15.

31. “According to Acts 4:25-28, vv. | and 2 have been fulfilled in the confederate
hostility of Israel and the Gentiles against Jesus the holy servant of God and against
His con fessors.” Franz Delitszch, The Psalms, 3 vols. (Grand Rapids: Eerdmans, [1867]
1973), 1:90. Israel’s priority in resisting Christ is indisputable. See: Matt. 22:33-46;
23:29-38; Luke 19:14; John 11:47; 19:14-15; Acts 5:17, 33; 1 Thess, 2:16.

32, Anthony Hoekema, The Bible and the Future (Grand Rapids; Eerdmans, 1979),
p. 178. Allen P. Ross, “Psalms,” The Biéle Knowledge Commentary: Old Testament, John
F. Walvoord and Roy B. Zuck, eds. (Wheaton, IL: Victor, 1985), p. 792.
