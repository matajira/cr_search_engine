Revelation 405

numerical figure itself is obviously stylized symbolism. But of
what?

The 144,000 saints seem to be representative of Jewish con-
verts to Christianity who dwelt in Israel, for the following reasons:
(1) The particular reference to the Twelve Tribes and the fact
that they are the “first fruits” (Rev. 14:4). Christianity’s first
converts were from Jerusalem (Acts 2). (2) Their distinction
from the great multitude from every nation (Rev. 7:9). (3) The
Old Testament background in Ezekiel 9:4 clearly specifies their
habitation at Jerusalem. (4) They are protected by God in “the
Land” (ge), which is being judged (Rev. 7:1-3). This fits well
with the forgoing action as being in Israel. (5) Such a designa-
tion is compatible with Christ’s warning His followers to flee
Jerusalem before its final overthrow (Matt. 24:15-16; Luke
21:20-24). He promised that His followers who heeded His
prophecy would be protected (Luke 21:18-19), (6) The events
of Revelation are spoken in anticipation of their soon occurring.
This fits perfectly the historical outcome of the flight of the
Christians from Jerusalem prior to her fall.

The Seven Trumpets

With the opening of the seventh seal, we hear the sounding of
the seven trumpets (Rev. 8:1-6). The first four trumpets show
judgments upon things, the last three upon men. They review
and intensify the chaos of the seals: destruction increases from
one-fourth (Rev. 6:8) to one-third (Rev. 8:7-12). Regarding
earthquakes and eruptions, James Moffatt writes: “Portents of
this abnormal nature are recorded for the seventh decade of
the first century by Roman historians. . . . Volcanic phenomena
... in the Egean archipelago . . . are in the background of this
description, and of others throughout the book; features such
as the disturbance of islands and the mainland, showers of
stones, earthquakes, the sun obscured by a black mist of ashes,
and the moon reddened by volcanic dust, were the natural
consequences of eruption in some marine volcano, and there —
