Theological Objections 467

Because eschatology is fundamental to biblical revelation, no es-
chatological system with major theological problems ought to be
held. Postmillennialism is a coherent system that flows naturally
from the Scriptural record.
