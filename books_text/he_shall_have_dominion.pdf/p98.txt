52 HE SHALL HAVE DOMINION

at least four major views about the nature of the millennium or
the millennial reign here described.”?

It is often the case that premillennial theologians and dis-
pensational theologians are more enamored with Revelation 20
than are others.® Writing of some of the great non-premillen-
nial Christian theologians of this century, dispensationalist L. S.
Chafer derides such exegetes because of their view of Revela-
tion 20: “Their abandonment of reason and sound interpreta-
tion has but one objective in mind, namely, to place chiloi
(‘thousand’) years — six times repeated in Revelation, chapter 20
— back into the past and therefore something no longer to be
anticipated in the future. The violence which this interpretation
imposes upon the whole prophetic revelation is such that none
would propose it except those who, for lack of attention, seem
not to realize what they do, . . . In sheer fantastical imagination
this method surpasses Russellism, Eddyism, and Seventh Day
Adventism. .. .” He speaks of “antimillennialism” as a “strange
theory, the origin of which is traced to the Romish notion that
the church is the kingdom.”

In a calmer tone, historic premillennialist Ladd admits: “We
must recognize frankly that in all the verses cited thus far it
would seem that the eschatological Kingdom will be inaugurat-
ed by a single complex event, consisting of the Day of the Lord,
the coming of the Son of Man, the resurrection of the dead,
and the final judgment. However, in the one book which is
entirely devoted to this subject, the Revelation of John, this
time scheme is modified. . . . The theology that is built on this
passage is millennialism or chiliasm. . . . This is the most natu-

5. Hoekema, The Bible and the Future (Grand Rapids: Eerdmans, 1979), p. 173.

6. “There are some who connect with the advent of Christ the idea of a millenni-
um, either immediately before or immediately following the second coming. While
this idea is not an integral part of Reformed theology, it nevertheless deserves
consideration here, since it has become rather popular in many circles.” Louis
Berkhof, Systematic Theology (Grand Rapids: Eerdmans, 1941), p. 708.

7, Lewis Sperry Chafer, Systematic Theology, 8 vols. (Dallas: Dallas Theological
Seminary Press, 1948), 4:281-282.
