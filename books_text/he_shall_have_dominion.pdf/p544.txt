498 HE SHALL HAVE DOMINION

A Summary of the Old Testament Evidence

The sovereign plan of God for the world can be adequately
understood only when viewed in light of its historical inception:
in the Bible’s account of the beginning of the universe, we discover
the very purpose of history. God created man in His own image
(Gen. 1:26) as a materio-spiritual being (Gen. 2:7). Man’s pur-
pose and destiny are to bring honor and glory to God by exer-
cising godly dominion in the earth (Gen. 1:26-30).

Because God possesses almighty power (Job 40; Isa. 40), and
governs by inscrutable wisdom (Isa. 55:8-9; Rom. 11:52-35), the
Christian actually should be predisposed to the sort of historical
victory envisioned by postmillennialism. The postmillennial
system best balances the material and spiritual aspects of Scrip-
ture, giving full significance to the temporal and eternal fea-
tures of God’s plan and man’s obligation to Him. The Lord
created man and history for His glory, therefore, man and
history will bring glory to Him. “You are worthy, O Lord, to
receive glory and honor and power; for You created all things,
and by Your will they exist and were created” (Rev, 4:11). “For
of ITim and through Him and to Tim are all things, to whom
be glory forever. Amen” (Rom. 11:36).

Postmillennialism teaches that there is coming a time in
earth history, continuous with the present and resultant from
currently operating, God-ordained spiritual forces, in which the
overwhelming majority of men and nations will voluntarily bow
in salvation to the Lordship of Jesus Christ. This righteous
submission to His gracious scepter will issue forth in wide-
spread righteousness, peace, and prosperity. The eschatological
theme in Scripture is a clearly discernible victory theme. It
begins with the protoevangelium of Genesis 3:15 (which harmo-
nizes with the creational purpose of God) and weaves its golden
cord throughout Scripture all the way to Revelation 22. There
is certainly the expectation of struggle in history. But it is a
struggle that will triumphantly issue forth in victory rather than
stalemate, defeat, or despair. The Seed of the Woman (Christ)
