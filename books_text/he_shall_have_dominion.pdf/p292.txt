246 HE SHALL HAVE DOMINION

who belong his to him. Then the end will come.” With Paul we
are now in the era awaiting the end-time coming of Christ,
when all believers will be resurrected in glory. When Christ comes
this will be “the end”! There will be no millennial age on the
present earth to follow.”

But notice the expectation preceding the end: Verse 24 says,
“the end will come, when he hands over the kingdom to God
the Father.” The end of earth history is brought about “when-
ever”! Christ “hands over” the kingdom to the Father. In the
syntactical construction before us, the “handing over” (NIV) or
“delivering up” (KJV) of the kingdom must occur in conjunc-
tion with “the end.””? Here the contingency is the date: “when-
ever” it may be that He delivers up the kingdom, then the end
will come. Associated with the predestined end here is the
prophecy that the kingdom of Christ will be delivered up to the
Father only “after he has destroyed all dominion, authority and
power.”

dispensationalists — see B. B. Warfield, “The Prophecies of St. Paul” (1886), Biblicat
and Theological Studies (Philadelphia: Presbyterian & Reformed, 1952), p. 484.

29, The Scripture is clear that the resurrection is a “general resurrection” of both
the righteous and unrighteous (Dan. 12:2; John 5:28-29; Acts 24:15), which will
occur on the “last day” (John 6:39-40, 44, 54; 11:24; 12:48). See Chapter 13, below.

30. For helpful discussions of this prohibition of an intervening kingdom (Zwisch-
enveich) era prior to the end, see: C. K. Barrett, From First Adam to Last (London:
Black, 1962), p. 101; Geerhardus Vos, The Pauline Eschatology (Phillipsburg, NJ:
Presbyterian & Reformed, [1930] 1991), pp. 238-258; Herman Ridderbos, Paw: An
Outline of His Theology (Grand Rapids: Eerdmans, 1975), pp. 556-559; W. D. Davies,
Paut and Rabbinic Judaism (New York: Harper, 1967), pp. 291-298. See also: A. T.
Robertson, Word Pictures in the New Testament, 6 vols. (Nashville; Broadman, 1930),
4:191,

31. A better translation of hotan is “whenever.” We know not “when” this will be,
Matt. 24:36,

32. The Greek for “hands over” here is paradidot, which is in the present tense
and subjunctive mode. When hoian is followed by the present subjunctive it indicates
a present conlingency that occurs in conjunction with the main clause: here the
coming of the end, Arndt-Gingrich, Lexicon, p. 592.

33. In the Greek text the hotan is here followed by the aorist subjunctive, katar-
gese. Such a construction indicates that the action of the subordinate clause precedes
the action of the main clause. Arndt-Gingrich, Lexicon, p. 592.
