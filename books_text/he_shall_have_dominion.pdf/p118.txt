72 HE SHALL HAVE DOMINION

writes: “It will be marked by the wniversal reception of the true
religion, and unlimited subjection to the sceptre of Christ.” “It shall be
a time of universal peace.” “It will be characterised by great
temporal prosperity.”**

It should be noted at this juncture that there are some im-
portant differences between two types of postmillennialism
today: pietistic and theonomic postmillennialism. “Among cur-
rent postmils, to be sure, there are some who are not recon-
structionists. ... Nonreconstructionist postmils would naturally
deny any such connection” between theonomic ethics and post-
millennialism.” Pietistic postmillennialism (as found in Banner
of Truth circles)** denies that the postmillennial advance of the
kingdom involves the total transformation of culture through
the application of biblical law. Theonomic postmillennialism
affirms this.

Seventh, possibly “we can look forward to a great ‘golden
age’ of spiritual prosperity continuing for centuries, or even for
millenniums, during which time Christianity shall be trium-
phant over all the earth.” After this extended period of gos-
pel prosperity, earth history will be drawn to a close by the
personal, visible, bodily return of Jesus Christ (accompanied by
a literal resurrection and a general judgment) to introduce His

22. David Brown, Christ’s Second Coming: Will it be Premillennial? (Edmonton, AB:
Still Waters Revival, [1882] 1990), pp. 399, 401.

23, Gaftin, “Theonomy and Eschatology,” Theonomy: A Reformed Critique, p, 197.
For more detail see: Rousas John Rushdoony, God's Plan for Victory: The Meaning of
Postmillennialism (Fairfax, VA: Thoburn, 1977). Greg L. Bahnsen and Kenneth L.
Gentry, Jr, House Divided; The Break-up of Dispensational Theology (Tyler, TX: Institute
for Christian Economics, 1989). Gary North, Millennialism and Social Theory (Tyler,
TX: Institute for Christian Economics, 1990), especially Chapter 10,

24. The Calvinists who are associated with this group are self-consciously identi-
fied with the revivalistic postmillennialism of Jonathan Edwards rather than with the
theonomic postmillennialism of the colonial American Puritans, See: lain Murray,
The Puritan Hope: A Study in Revival and the Interpretation of Prophecy (Edinburgh:
Banner of Truth, 1971). The reprints of Puritan works issued by the Banner of
‘Truth are pictistic rather than Cromwellian, introspective rather than cultural,

25. Loraine Boettner, The Millennium (Philadelphia: Presbyterian & Reformed,
1958), p. 29.
