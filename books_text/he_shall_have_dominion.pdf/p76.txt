30 HE SHALL HAVE DOMINION

intense study and careful reflection. The need of care in this
area is evidenced by the proliferation of “last day” cults over
the last 150 years, such as Seventh-Day Adventists, Church of
Jesus Christ of Latter-Day Saints (i.c., Mormons), Jehovah’s
Witnesses, Herbert W. Armstrong’s Worldwide Church of God,
the Children of God, the Unification Church, and others.

A divine lament in Scripture is quite apropos today: “My
people are destroyed for lack of knowledge” (Hos. 4:6). Chris-
tians are urged to “be diligent to show themselves approved,
workmen of God that need not to be ashamed, handling accu-
rately the word of truth” (2 Tim. 2:15). Light is an emphasized
metaphor of the Christian faith;® consequently, obscurantism
and ignorance are not virtues for the people of God.* We need
to get things bright and clear, theologically and ethically.

Because of both our sin and our finitude, we cannot know
anything exhaustively (though we can know truly what we do
know).” Consequently, no one knows all there is to know re-
garding Scripture, so we always need to study it more in order
to gain a better understanding of it.? The Scripture teaches
that “a wise man will hear, and will increase learning” (Prov.
1:5). And the better we apprehend and apply Scripture, the
closer will be our walk with God, for sanctification comes

5. The word “light” occurs seventy-six times in the New Testament. It is used
metaphorically most often.

6, The call to “know” is a frequent refrain in the New Testament, particularly in
Paul's writings, where it occurs no less than sixty-one times. The rebuke “know ye
not” occurs fifteen times; see: Rom. 6:3, 16; 7:1; 1 Cor 3:16; 5:6; 6:2, 3, 9, 15, 16,
10; 9:13, 24; 2 Cor. 13:5; Jms. “I would not have you ignorant [i.e., unknowing]”
occurs seven times; see: Rom. 1:13; 11:25; 1 Cor. 10:1; 12:1; 2 Cor. 1:8; 2:11; 1
Thess. 4:13, “We/ye know” occurs thirty times; see; Rom, 3:19; 7:14, 18; 8:22, 26, 28;
1 Cor, 2:12, 14; 8:1, 2, 4; 12:2; 15:58; 16:15; 2 Cor. 5:1; 8:9; 13:6; Gal. 3:7; 4:13;
3:19; Eph, 5:5; Phil. 2:22; 4:15; 1 Thess. 3:3; 4:2; 5:2; 2 Thess. 2:6; 3:7; 1 Tim. 1:8;
3:5. “I would have you know/that ye may know” occurs nine times; see: 1] Cor. 11:3;
2 Gor. 2:4; Eph. 1:18; 6:21, 22; Col. 4:6; 1 Thess. 4:4; 1 Tim. 3:15; 2 Tim. 3:1.

7. Cornelius Van Til, The Defense of the Faith (3rd ed.; Philadelphia: Presbyterian
& Reformed, 1967}, ch, 3.

8. Psa. 1:2-3; 119:97; Matt. 13:23; Acts 17:11.

 

  

   
