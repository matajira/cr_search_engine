Expansion 233

Recently there have been some who have recoiled at the
mention of the word “dominion,” when applied to the prog-
ress of the gospel. Yet the concept of dominion is a revealed
expectation. The word “dominion” is used in significant ways in
Scripture.? ‘God’s providential rule over the universe is His
“dominion” over His kingdom (Psa. 145:13; Dan. 4:3). The Old
Testament anticipates Christ’s “dominion” in history (Psa. 72:8;
Dan. 7:14; Zech. 9:10). Of course, those who lament the em-
ployment of “dominion” are not concerned about its reference
to God's rule, but to the rule of His people in contemporary
history. Yet, as we saw in Chapter 9, “dominion” is a general
calling given to man as God’s image (Gen. 1:26-28; Psa. 8:6).
The expectation of “dominion” specifically for God’s redeemed
is also legitimate in that we currently have a kingship based on
Christ’s “dominion” (Rev. 1:6). Ours is a derivative, subordinate
dominion under God and over His creation: representative.

Unfortunately, due to imprecise thinking by some, dominion
is wrongly thought to imply a carnal militarism (such as in
Islamic fundamentalism) or an ecclesiocracy (such as in medi-
eval Romanism). Nevertheless, dominion is both commanded
and assured in the New Testament record.

Dominion Commanded

It is important to note that the postmillennial view is the
only one of the three major evangelical eschatologies that builds
its case on the very charter for Christianity, the Great Commis-
sion (Matt. 28:18-20). David Brown wrote over a century ago:

The disciples were commissioned to evangelize the world
before Christ's second coming; not merely to preach the Gospel,

1, Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989), ch. 2; H.
Wayne House and Thomas D. Ice, Dominion Theology: Blessing or Curse? (Portland,
OR: Multnomah, 1988), chaps. 1, 15; Dave Hunt, Whatever Happened to Heaven? (Eu-
gene, OR: Harvest House, 1988), chaps. 10-11.

2. The English word “dominion” is derived from the Latin dominus, “lord.”
