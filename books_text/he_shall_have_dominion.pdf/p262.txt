216 HE SHALL HAVE DOMINION

ready come, the only sense that can be put upon the perfect
participle here employed.”” Thus, His disciples were to ex-
pect its exhibition in power. It was not powerfully to evidence
itself immediately, for many of His disciples would die before it
acted in power. Yet it was to be within the lifetimes of others,
for “some” standing there would witness it. This seems clearly
to refer to the A. D. 70 destruction of the temple and removal
of the Old Testament means of worship (cf. Heb. 12:25-28;
Rev. 1:1, 3, 9). This occurred as a direct result of Jesus’ proph-
ecies (John 4:21-23; Matt. 21:33ff; 23:31-34:34),

Such data as these set the stage for a clear elucidation of the
victory theme. The long-awaited kingdom prophesied in the Old
Testament era was about to break forth in history. Would its effect be
wholly internal, limited to small pockets of the faithful? Or
would it exhibit itself in powerful victory, transforming the
mass of men in salvation, whole cultures by righteousness, and
national governments for justice? I take the latter view.

The Establishment of the Kingdom

Because “the time” was “fulfilled” and the “kingdom of God”
was “at hand,” we should expect its appearance in the gospel
record. God determines the “times” (Dan. 2:21; Acts 1:7); the
time had come. Clear and compelling evidence exists that the
kingdom did im fact come in Christ’s ministry. Perhaps one of the
“clearest proofs” in the gospel for the presence of the kingdom
of heaven is Matthew 12:28: “But if I cast out devils by the
Spirit of God, then the kingdom of God is come unto you.”
The truth is, Jesus did cast out demons by the Spirit of God.
The protasis of this “ifthen” statement being true, then the
apodosis follows: “the kingdom of God is come.” The very fact
that Satan's kingdom was being invaded and his possessions

12. J. A. Alexander, The Gospel According to Mark (Grand Rapids: Baker, [1858]
1980), p. 230.

13. Ridderbos, Coming of the Kingdom, p. 107.
