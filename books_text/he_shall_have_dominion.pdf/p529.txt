Biblical Objections 483

The approach taken to these verses marks a clear distinction
between all premillennial views as opposed to amillennial and
postmillennial views. All premillennialists vigorously dispute the
non-premillennial positions by interpreting these verses and
their parallels in a literalistic fashion. The dispensationalist is
especially vigorous in this assertion, although other premillen-
nialists hold that these passages demand a premillennial under-
standing.”

Walvoord employs these and related passages in his argu-
ment against amillennialists, and by implication postmillennial-
ists. Of Luke 1:30-33, he writes: “If it is true, as advocates of
amillennialism contend, that the Old Testament has been mis-
understood and that a literal fulfillment of the Davidic Cove-
nant should not be expected, why would God instruct His angel
to use such terminology for Mary?” He continues this line of
argumentation: “Later Christ confirmed [the disciples’] expecta-
tion in promising them that they would sit on thrones judging
the twelve tribes of Israel in the promised period of restoration
(Matt. 19:28). This promise was confirmed later in Luke 22:30
when Christ met with His disciples for the Passover the night
before His crucifixion. Again, they were assured that they
would sit on thrones and judge the twelve tribes of Israel.””
The expectation, as argued by dispensationalists, then, is clearly
a literalistic political governance by the apostles.

The amillennial view also differs with a leading postmillenni-
al interpretation. Amillennialists generally apply them exclusive-
ly to the heavenly realm: “So the disciples are not to expect
earthly glory and worldly power as a reward, but heavenly joy
and a holy vocation in His eternal kingdom.”* These verses

31. For example, see George Eldon Ladd’s use of Matthew 19:28 in A Theology
of the New Testament (Grand Rapids: Eerdmans, 1974), pp. 48, 109, 205, 628, 631.

32. Walvoord, Prophecy Knowledge Handbook, p. 65. See also: Barbieri, “Matthew,”
Bible Knowledge Commentary: New Testament, p. 65; Pentecost, Thy Kingdom Come, p.
154-155,

33. Norval Geldenhys, The Gospel of Luke (NICNT) (Grand Rapids: Eerdmans,
