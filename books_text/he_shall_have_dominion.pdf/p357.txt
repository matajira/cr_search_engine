Time Frames 311

tions ago were very far apart on the details. Much of the same
diversity appears in premillennial contemporary writers.”® In
fact, Daniel’s Seventy Weeks prophecy leads dispensationalism
into one of its most strained peculiarities: the doctrine of the
gap theory of the Church Age.® I will consider this later.

Covenantal Structure

As we get started, it is crucial to grasp the structure of the
prophecy. Meredith Kline provides a thorough presentation of
the strongly covenantal cast of Daniel 9 which leads up to the
prophecy, noting that it is “saturated with formulaic expressions
drawn from the Mosaic treaties, particularly from the Deuter-
onomic treaty” (cf. Dan. 9:4-6, 10-15).’ This prayer regarding
covenant loyalty (hesed, 9:4) is answered in terms of the coven-
antal sabbath pattern of the seventy weeks (9:24-27), which
results in the confirmation of the covenant (9:27). Daniel 9 is
the only chapter in Daniel to use God’s special covenant name,
YHWH (vv. 2, 4, 10, 13, 14, 20; cf. Exo. 6:2-4).

Recognizing the covenantal framework of the Seventy Weeks
is crucial to its proper interpretation. It virtually demands that
the focus be on the fulfillment of redemption in the ministry of
Christ. Let us see why this is so.

The prophecy of the Seventy Weeks is clearly framed in
terms of sabbatic chronology. The first phase of the Seventy
Weeks is “seven weeks,” or (literally) “seven sevens” (Dan. 9:25),
which results in a value of forty-nine. This reflects the time

5. Ibid., p. 144.

6. Allis mentions this teaching flowing out of the dispensational approach to
Daniel 9:24-27 as “one of the clearest proofs of the novelty of that doctrine as well as
of its revolutionary nature.” Allis, Prophecy and the Church, p. 109. Kline's analysis of
Daniel 9 leads him to call dispensationalism an “evangelical heresy.” Meredith Kline,
“Covenant of the Seventieth Week,” The Law and the Prophets: Old Testament Studies in
Honor of Oswald T. Allis, John H. Skilton, ed. (n-p.: Presbyterian & Reformed, 1974),
p. 452,

7. Kline, “The Covenant of the Seventieth Week,” p. 456.
