TABLE OF CONTENTS

Foreword, Gary North... 0. ccc eee eae nee ix
Preface 2... cee ete eee eee ee eens xl

PART I: INTRODUCTION

1. The Significance of Eschatology ...............-- 1
2. The Purpose of This Treatise .................4. 28
3. The Pessimistic Millennial Views ................- 50
4. Introduction to Postmillennialism ................ 65

PART II: INTERPRETATION

5. The Revelation of Truth ................2..-... 97
6, The Covenants of Redemption .............+.4; 106
7. The Righteousness of God 2.2.2.0... cee eee eee 122
8. The Hermeneutic of Scripture ............--50. 144

PART III: EXPOSITION

9. Creation 22... . ee eee eee 177
10, Anticipation... 6... ce eee eee ene 187
ll. Realization 2.0... eee eee eee 210
12. Expansion... 0... eee eee 232
13. Consummation .........0....... 2.20.20. eee eee 269

PART IV: SPECIFICATION

14. Time Frames... 00.000 ce eee 309
15. Features 00. eee ene ee eens 337
16. Character oo. 0... ccc cee ee ee eee eee 366
17. Revelation... 0.0.0... 0... eee 394

PART V: OBJECTIONS

18. Pragmatic Objections ................ 00.0000. 425
19. Theological Objections .... 1.6.0... cece eee 449
20. Biblical Objections ©... 0.00. cece eee 468
