Biblical Objections 485

references to the presence of the kingdom studied heretofore
(e.g., Luke 11:20; 17:20-21; Rom. 14:17; Col. 1:13). He is not
speaking of the future eternal and heavenly aspects of the
kingdom. Here Christ the King indicates that he is presently
bestowing formal authority on His apostles; they are His ambas-
sadors (2 Cor. 5:20) who reign with Him (Rom. 5:17, 21).

The kingdom He is here bestowing upon them is not an
earthly, political kingdom, for He expressly forbids such carnal
kingly trappings: “And He said to them, ‘The kings of the
Gentiles exercise lordship over them, and those who exercise
authority over them are called “benefactors.” But not so among
you; on the contrary, he who is greatest among you, let him be
as the younger, and he who governs as he who serves” (Luke
22:26), His kingdom is a spiritual kingdom of humble spiritual service
rather than regal political glory.

As a consequence of His bestowal of the kingdom, the Lord
holds out the promise to them “that you may eat and drink at
My table in My kingdom, and sit on thrones judging the twelve
tribes of Israel” (Luke 22:30). The reference to the eating and
drinking at His table must speak of the Lord’s Supper, which
He had just instituted a few moments before (Luke 22:13-20).
Though He is about to die (Luke 22:21-23), they should not
despair, for He will be with them spiritually. This will be partic-
ularly evident as they gather for “communion” (1 Cor. 10:16;
Rev. 3:20) with Him at “the Lord’s Table” (1 Cor. 10:21).

Since the kingdom is a present, spiritual reality, we may not
take the sitting on thrones in a literal sense, for the apostles
never really sat on thrones. This sitting on thrones has spiritual
implications, of the order of the Pharisees sitting in “Moses’
seat” (Matt. 23:2) - which certainly was not a literal chair. Al-
though here the express reference is to the Apostles themselves,
elsewhere there is a sense in which all Christians sit on thrones.
He “raised us up together, and made us sit together in the
heavenly places in Christ Jesus” (Eph. 2:6; cf. Rev, 20:4-6).
