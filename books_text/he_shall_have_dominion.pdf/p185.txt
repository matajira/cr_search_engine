The Righteousness of God 139

“just” (Rom. 7:12; Heb. 2:2) and “good” (Rom. 7:12; 1 Tim.
1:8).

The Civil Magistrate and God’s Law

Church and State were separate under the Mosaic Law.
There was a distinction between the civil ruler, Moses, and the
priestly head, Aaron; between the offices of priest and king (not
with Melchizedek: unique); between the temple and palace: 1
Samuel 13:11; 2 Chronicles 19:5-11; 26:16-21. Yet the Law was
the standard of civil justice. The same is true in the New Testa-
ment era, as an analysis of Romans 12 and 13 shows.

In Romans, Paul speaks to the problem of evil in society:
“Repay no one evil (kakon] for evil [kakou]” (Rom. 12:17). He
urges them: “Beloved, do not avenge [ekdikountes] yourselves,
but rather give place to wrath [orge]” (Rom. 12:19a). Why? “For
it is written, ‘Vengeance [ekdikesis] is Mine, I will repay,’ says the
Lord” (Rom. 12:19). Thus, he urges the Christian not to take
the law into his own hands: “Be not overcome of evil [kakon]”
(Rom. 12:21). He then engages a discussion of the God-or-
dained role of the civil magistrate as God’s avenger.*

In Romans 13, the matter of the civil magistrate is ap-
proached prescriptively, rather than descriptively.*” As such,
he has been “ordained of God” (Rom. 13:2) so that “he does
not bear the sword in vain. Ile is, in fact, God’s minister, an
avenger [ekdikes] to execute wrath [orgen] on him who practices
evil [kakon]” (Rom. 13:4). Clearly, then, the magistrate is to
avenge the wrath of God against those who practice evil (Rom.
13:4, 6).

As he continues, Paul makes express reference to the Law of
God, citing four of the Ten Commandments (Rom. 13:9a) and

56. The very contextual flow (Rom. 12:17ff leads directly to Rom. 13:1ff) is
validated by lexical similarity between the two chapters.

57. Gentry, “Civil Sanctions in the New Testament,” Theonomy: An Informed
Response, Gary North, ed. (Tyler, TX: Institute for Christian Economics, 1991), ch. 6.

~
