The Hermeneutic of Scripture 167

when he designates Christians as “stones” being built into a
“spiritual house” (1 Pet. 2:5-9). But he does more; he draws
upon several Old Testament designations of Israel and applies
them to the Church: “. . . a chosen generation, a royal priest-
hood, an holy nation.”®* He, with Paul, also calls Christians “a
peculiar people” (1 Pet. 2:10; Titus 2:14), which is a familiar
Old Testament designation for Israel.®

If Abraham can have Gentiles as his “spiritual seed,” why
cannot there be a spiritual Israel? In fact, Christians are called by
the name “Israel”: “And as many as walk according to this rule,
peace and mercy be upon them, and upon the Israel of God”
(Gal. 6:16), Although dispensationalists attempt to understand
Galatians 6:16 as speaking of Jewish converts to Christianity
“who would not oppose the apostle’s glorious message of salva-
tion,”®! such is surely not the case, as we shall see.

The entire context of Galatians is set against claims to a special
Jewish status or distinction, as urged by dispensationalists. “For
you are all sons of God through faith in Christ Jesus. For as
many of you as were baptized into Christ have put on Christ.
There is neither Jew nor Greek, there is neither slave nor free,
there is neither male nor female; for you are all one in Christ
Jesus” (Gal. 3:26-28). In Christ, ali racial distinction has been done
away with. Why would Paul hold out a special word for Jewish
Christians (“the Israel of God”), when he had just stated that
there is no boasting at all, save in the cross of Christ (Gal.
6:14)? After all, “in Christ Jesus neither circumcision nor uncir-
cumcision avails anything, but a new creation” (Gal. 6:15), That
new creation is spoken of in detail in Ephesians 2:10-22, where
Jew and Gentile are united in one body. This is the Church.

It is important to note, as does Poythress, that the Church is

90

88, 1 Pet. 2:9-10; Exo. 19:5-6; Deut. 7:6.

89. Exo. 19:5; Deut. 14:2; 26:18; Psa. 135:4.

90. New Scofield Reference Bible, p. 1223 (at Rom, 9:6).

91. Ibid. See also: Ryrie, Dispensationalism Teday, p. 139; Pentecost, Things to Come,
p. 89; Donald K. Campbell, “Galatians,” Bible Knowledge Commentary, 1:611.
