Realization 225

burning zeal.””!

The Kingdom's Spiritual Nature

Despite dispensationalism’s confusion, the Scripture is quite
clear regarding the spiritual nature of the kingdom. It is a
distinctive of dispensationalism that asserts that Christ offered
to Israel a literal, political, earthly kingdom, but that the Jews
rejected it, thus causing its postponement.” This view of the
kingdom is totally erroneous. As a matter of fact, it was just this
sort of kingdom that the first-century Jews wanted and which
Christ refused: “When Jesus therefore perceived that they
would come and take him by force, to make him a king, he
departed again into a mountain himself alone” (John 6:15).

The disciples themselves missed His point, for the most part,
while He was on earth. This is evidenced in the Emmaus Road
encounter after the crucifixion, where these disciples lament:
“But we trusted that it had been he which should have re-
deemed Israel: and beside all this, today is the third day since
these things were done” (Luke 24:21). We should note that
Jesus rebuked them for such foolishness: “Then he said unto
them, O fools, and slow of heart to believe all that the prophets
have spoken: Ought not Christ to have suffered these things,
and to enter into his glory? And beginning at Moses and all the
prophets, he expounded unto them in all the scriptures the
things concerning himself” (Luke 24:25-27). They expected
politica] deliverance and glory to come to Israel through this
Messiah.” But Jesus spoke to them of the true meaning of the
prophecies of the Old Testament, showing them that He must

31. John Calvin, Matthew, Mark, and Luke (1553), 3 vols. (Grand Rapids: Eerd-
mans, 1972), 2:7, See also: Ridderbos, Coming of the Kingdom, p. 54.

32. Charles L. Feinberg, Millennialism: The Two Major Views (3rd ed.; Chicago:
Moody Press, 1980), ch. 8.

33. Cf. their hope that He would “redeem Israel” with the Old Testament
declaration thal God “redeemed” Israel by delivering them from Egypt to become an
independent nation, Deut. 7:8; 9:26; 13:5; 15:15; 24:18; 1 Chr. 17:21; Mic, 6:4.
