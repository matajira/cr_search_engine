74 HE SHALL HAVE DOMINION

confusion regarding postmillennialism. But it does deserve at
least passing comment.

The errors of Peters’ analysis and others like it have been
exposed by a number of scholars. The three leading, most
detailed, and helpful are: Alan Patrick Boyd (a dispensational-
ist), D. H. Kromminga (a premillennialist), and Ned Stonehouse
(an amillennialist).** Also noteworthy are studies by Louis Berk-
hof, Philip Schaff, Albertus Pieters, and W. J. Grier? Krom-
minga carefully examines the sub-apostolic writings, including:
Clement of Rome’s J Clement, the pseudo-Clementine 2 Clement,
The Didache, the Ignatian epistles, Polycarp’s Epistle, The Letter of
the Church at Smyrna on the Martyrdom of Polycarp, Barnabas,
Hermas, Diognetus, Fragments of Papias, and Reliques of the
£ilders. He convincingly shows that only Papias among the sub-
apostolic fathers is premillennial. He concludes that “an inquiry
into the extent of ancient chiliasm will serve to show the unten-
ableness of the claim that this doctrine was held with practical
unanimity by the Church of the first few centuries.”

Put in the best light, the most that Peters could say is: “[I]t
would seem that very early in the post-apostolic era millenar-
ianism was regarded as a mark neither of orthodoxy nor of
heresy, but as one permissible opinion among others within the
range of permissible opinions.”* Dispensationalist Lightner
has admitted that “None of the major creeds of the church

32. Alan Patrick Boyd, “A Dispensational Premillennial Analysis of the Eschatolo-
gy of the Post-Apostolic Fathers (Until the Death of Justin Martyr)” (Dallas: Dallas
Theological Seminary master’s thesis, 1977); D. H. Kromminga, The Millennium in the
Church (Grand Rapids: Eerdmans, 1945), pp. 29-112; Ned Stonehouse, The Apocalypse
in the Ancient Church (Goes, Holland: Oosterbaan and LeCointre, 1929), pp. 1311.

33. Louis Berkhof, The History of Christian Doctrines (Grand Rapids: Baker, [1937]
1975), p. 262; Philip Schaff, History of the Christian Church, 8 vols. (5th ed.; Grand
Rapids: Eerdmans, [1910] n.d.), 2:615; Albertus Pieters, two articles: “Chiliasm in the
Writings of the Apostolic Fathers” (1938), cited by Kromminga, Millennium, p. 41; W.
J. Grier, The Momentous Event (London: Banner of Truth, [1945] 1970), pp. 19ff.

34. Kromminga, Millennium, pp. 30, 41, 42.

35. Jaroslav Pelikan, The Christian Traditions, vol. 1 (Chicago: University of
Chicago Press, 1971), p. 125.
