The Significance of Eschatology

power is given unto Me in heaven and in earth. Go ye therefore
and make disciples of all nations.” The duty of the church is to
address herself to the achieving of this task in anticipation of the
Lord’s coming, and not to expect Him to call her away to glory
before her task is accomplished.'”

103. Allis, “Foreword,” in ibid., p. ix.

27
