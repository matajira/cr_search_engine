Consummation 301

steadfast, immovable, always abounding in the work of the
Lord, knowing that your labor is not in vain in the Lord” (1
Cor. 15:58). Amillennialist Hoekema goes too far in assigning
obviously spiritual references (e.g., Isa. 65:17) and many king-
dom victory passages (Psa. 72) to the consummate new heavens
and new earth.’*! Nevertheless, he well states the significance
of our present cultural labors in the light of eternity: “[W]e may
firmly believe that products of science and culture produced by
unbelievers may yet be found on the new earth. . . . [OJur
mission work, our attempt to further a distinctively Christian
culture, will have value not only for this world but even the
world to come.” This comports well with the postmillennial
drive to cultural and spiritual activity. The difference is, Hock-
ema did not believe that Christians will achieve this in history.

2 Peter 3

The key passage for the consummate new heavens and new
earth is found in 2 Peter 3, Unfortunately, this passage has
been the source of a good deal of confusion. Some
dispensationalists hold that it refers to the carthly millennium,
while others argue that it speaks of the consummate new cre-
ation,’” Some postmillennialists hold that it refers to the pre-
sent era introduced by the destruction of Jerusalem,’™ others
apply it to the consummate new heavens and new earth.’
Many amillennialists refer the new creation concept in all of

101. Hoekema, Bible and the Puture, pp. 177-178, 201-203.

102, Ibid., pp. 39-40.

103. See thebrief discussion in Walvoord, Prophecy Knowledge Handbook, pp. 510-
513, 633,

104, Roderick Campbell, Lsrael and the New Covenant (Tyler, TX: Geneva Divinity
School Press, [1954] 1981), ch. 13. Sec also the amillennialist and preterist Cornelius
Vanderwaal, Search the Scriptures (St. Catherines, Ontario: Paideia, 1979), pp. 52-53.

105. See: John Calvin, Hebrews and I and I Peter (1549), Calvin’s New Testament
Commentaries, David W. Torrance and Thomas F. Torrance, eds. (Grand Rapids:
Eerdmans, 1963), pp. 363-366. Dabney, Systematic Theology, pp. 850-852. Shedd,
Dogmatic Theology, 2:665.
