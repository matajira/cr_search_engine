562

pessimism, 184, 477-478,
492

postmillennialism, 2534,
334, 384

Revelation of John, 51-
52

rewards, 295

salvation, 185

House, H. Wayne, 18, 39,
26, 228, 479

Hoyt, Herman, 20, 229,
329, 368, 426

Hughes, Philip Edgcumbe,
53, 297

Hunt, Dave, 20, 39, 45, 278,
329, 435, 456

Hyles, Jack, 453

Ice, Thomas D., 18, 42, 45,
228, 479

Irenaeus, 3n

Ironside, Harry, 153, 351

Jeffrey, Grant R., 329
Johnson, Alan F., 66

Kaiser, Walter C., Jr, 60
Keil, C. F., 325
King, Max, 271
Kik, J. M., 386
Kistemaker, Simon, 304
Kline, Meredith, 311
Knowles, L. E., 321
Kromminga, D. H., 34, 58,
7416, 88, 91, 433
Kuiper, Dale H., 22, 42
Kuiper, R. B., 480

HE SHALL HAVE DOMINION

Ladd, George Eldon, 21,
52, 54, 62-63, 333, 445
LaHaye, Tim, 24, 325
LaRondelle, Hans K., 144,
151, 158, 321
Lightfoot, John, 366
Lightner, Robert P, 40, 66,
441, 446
Lindsey, F. Duane, 469ff
Lindsey, Hal
Antichrist, 370
last days, 327
pessimism, 19, 480
postmillennialism, 33,
35-36, 428, 446, 450

MacArthur, John, 261, 474
Machen, J. Gresham, 29, 34
Machiavelli, Niccolo, 11n
Martin, John A., 472
Masters, Peter, 447

Mauro, Philip, 64
McClain, Alva J., 426
McDonald, H. D., 6

Mede, Joseph, 50

Milne, Bruce, 333
Montgomery, J. A., 310
Moody, Dwight L., 453
Moorhead, James H., 428
Morris, Henry, 440
Morris, Leon, 383, 492
Mosheim, J. L. von, 399
Mounce, Robert H., 21
Murray, George, 42, 53
Murray, Iain, 36, 48
Murray, John, 36, 37, 460

Niebuhr, Richard, 447
