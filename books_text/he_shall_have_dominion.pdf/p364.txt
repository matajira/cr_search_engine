318 HE SHALL HAVE DOMINION

expansion of verse 26. Negatively, Messiah’s cutting off in verse
26 is the result of Israel’s completing her transgression and
bringing it to a culmination (v. 24) by crucifying the Messiah.”
Positively, verse 27 states this same event: “He shall confirm a
covenant with many for one week; but in the middle of the week
He shall bring an end to sacrifice and offering.” Considered
from its positive effect, this confirming of the covenant with
many makes reconciliation and brings in everlasting righteous-
ness (v. 24). The confirming of a covenant (v. 27) refers to the
prophesied covenantal actions of verse 24, which come about as
the result of the Perfect Covenantal Jubilee (Seventy Weeks)
and are mentioned as a result of Daniel’s covenantal prayer (cf.
v. 4). The covenant mentioned, then, is the divine covenant of
God’s redemptive grace.” Messiah came to confirm the covenan-
tal promises (Luke 1:72; Eph. 2:12). He confirmed the covenant
by His death on the cross (Heb. 7:22b).*°

The word translated “confirm” (higdir) is related to the angel
Gabriel’s name, who brought Daniel the revelation of the Sev-
enty Weeks (and who later brings the revelation of Christ’s
birth [Luke 1:19, 26]). “Gabriel” is based on the Hebrew gibbor,
“strong one,” a concept frequently associated with the covenant
God.” The related word found in Daniel 9:27 means to “make
strong, confirm.” This “firm covenant” brings about “everlast-
ing righteousness” (Dan. 9:24) — hence its firmness,

28. Matt. 20:18-19; 27:11-25; Mark 10:33; 15:1; Luke 18:32; 23:1-2; John 18:28-
31; 19:12, 15; Acts 2:29-23; 3:13-15a; 4:26-27; 5:30; 7:52.

29, When “covenant” is mentioned in Daniel, it is always God’s covenant, see:
Daniel 9:4; 11:22, 28, 30, 32. This includes even Daniel 11:22. See: J. Dwight
Pentecost, “Daniel,” Bible Knowledge Commentary, John F. Walvoord and Roy B, Zuck,
eds., 2 vols, (Wheaton, IL: Victor, 1985), 1:1369. Hereafter referred to as BKC.

30. Mart. 26:28; Mark 14:24; Luke 22:20; | Cor. 11:25; 2 Gor. 3:6; Heb. 8:8, 13;
9:15; 12:24.

31. Deut. 7:9, 21; 10:17; Neh, 1:5; 9:39; Isa. 9:6; Dan. 9:4. Hengstenberg argues
convincingly that the source of Daniel 9 seems to be Isaiah 10:21-23, where Gad is
the “Mighty God” who blesses the faithful remnant.

52. Young, Daniel, p. 209; Allis, Prophecy and the Church, p. 122; Hengstenberg,
Christology of the Old Testament, 2:856.
