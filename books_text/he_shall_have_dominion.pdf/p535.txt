Biblical Objections 489

salvation that God mercifully offers them along with the favor
of God that would issue forth from it. This refreshing is especially
glorious, for it stands in contrast to the horrible wrath under which they
now live and which will soon crash down upon them.

But perhaps they would lament their having destroyed the
only One who could bring them such consolation — a fear much
like he had encountered before (Acts 2:37). In order to circum-
vent such, Peter sets a promise before them. The promise is that
Christ will yet come to them in salvation: “, . . and that He may
send Jesus, the Christ appointed for you” (Acts 3:20 NASV). It
is true that He is in heaven physically away from them; in fact,
“heaven must receive [Him] until the times of restoration of all
things” (3:21). Still, there is the promise that God will send
Him to them in salvation.* Although He is in heaven, He is
not beyond their reach, for He comes to dwell in those who
have faith in Him (John 14:23). As the gospel is preached, the
hearers discern the voice of the living Christ (Eph. 2:17).

This understanding of the “sending” (apostello) of Jesus in
salvation is no more awkward than is the Second Advent view.
Neither the wording for the sending of the Son in salvation nor
for the sending of the Son in the Second Advent expressly
occurs in Scripture. Though in the economy of redemption it
is more precise to speak of the Father sending the Spirit in the
gospel (John 14:26), we must understand that the sending of
the Spirit results in the coming (sending) of the Son into the
believer (Rom. 8:9). In this context, the focus is on what they
have done to Christ, who was perfectly subject to God. God
fore-announced His incarnation (3:18); Christ was God’s “Ser-
vant” (3:13, 26), “His Christ” (3:18), whom God sent (3:22).
Hence the unusual manner of speaking: Christ is being empha-
sized as One Who is subject to the Father.

Danicl 9:24.

48. John Lightfoot, Commentary on the New Testament from the Talmud and Hebraica,
4:40-41. Cf. G, C. Berkouwer, The Return of Christ (Grand Rapids: Ecrdmans, 1972),
p- 151.
