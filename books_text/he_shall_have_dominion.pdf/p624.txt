578

HE SHALL HAVE DOMINION

rare in Scripure, 51, 52,
332
sacrificial system and, 115,
153ff, 228, 230, 350,
352, 3544, 470
second advent and, 42,
52n, 53-63, 228
systems of (See.
Eschatological Systems)
Montanism, 79ff
Mormonism (See:
Cults ~ Mormons)
Moses (See: Covenant
— Mosaic)
Mount of Olives, 344, 469,
472
Mountain(s), 201, 256
Mystery (See:
Church — mystery)

Name (significance of), 180
Nations (Christianity and),
202, 219, 232, 2626
Nature, 8, 101
Nero, 344, 376-378, 389, 392,
404, 409-410
Neutrality, 127
New Age, 441
New birth, 243, 272
New Creation (See: New
Heavens and Earth)
New Earth (See: New
Heavens and New Earth)
New Heavens and Earth
amillennialism and, 57n,
198, 208-209, 360, 483-
484, 511
Christians as New

Creatures, 362ff
eschatology and, 18, 23,
213, 299-305, 360-364,
418-420, 461
premillennialism and, 62,
360
sin and, 363-364
Newspaper exegesis, xli, 431,
433-434
Nicene Creed, 3n, 282
Ninevah, 137, 452, 488
Noah, 188-189, 302, 347
Now/Not yet, 299

Oath (See: Covenant
— oath)

Obscurantism, 30

Olivet Discourse, 160-163,
360, 385, 387

Ontology, 13

Optimism (See: Eschatology
~ optimism)

Origen, 80

Papias, 3n, 63, 74, 77
Parables
destruction of Jerusalem
in, 274-275
Leaven, 239-243, 252, 487
Mustard Seed, 238-239,
241, 252, 478
Tares, 477-479
Parenthesis (See: Church
— parenthesis view)
Passover, 483
Paul, 2
Pax Romana (See: Peace — Pax
Romana)
