xvi HE SHALL HAVE DOMINION

I know of no book that presents the case for any view of eschat-
ology that is equally painstaking. He covers every base,

Notice, too: his book has a positive aspect and a negative
aspect. As with the gospel, this book has a two-fold goal: recon-
ciliation and condemnation. There is no escape from these
goals. When we share the gospel, we are bringing God's coven-
ant lawsuit, just as Jonah brought it before the people of Nin-
eveh. This lawsuit offers blessings and cursings. Therefore, He
Shall Have Dominion is designed to achieve the following results:
(1) to give confidence and greater information to those who
already believe its general position; (2) to persuade those who
have not yet made up their minds; (3) to persuade those who
are still open to new evidence; (4) to silence the critics.

An honest critic, if he goes into print against He Shall Have
Dominion, should do the following: (1) show how Gentry has
generally misinterpreted biblical eschatology, i.c., demonstrate
a pattern of misinterpretation; (2) provide several examples of
this pattern; (3) refer the reader to equally detailed and equally
comprehensive studies in eschatology that offer biblical solu-
tions to the problems that Gentry raises; (4) show how Gentry
either ignored this missing book or completely misrepresented
it. While a short book review cannot match Gentry’s massive
documentation, the reviewer had better be able to point the
reader to a book or books of equal or greater exactness as He
Shall Have Dominion. If he fails to do the third task — suggest an
exegetically superior book ~ he is implicitly admitting that
Gentry has offered the most exegetically impressive case that
anyone has made so far. My belief is that no reviewer will pub-
licly identify the definitive book on eschatology; this would
involve too much commitment on his part. No reviewer today
trusts any book on eschatology unless it is his own, but review-
ers rarely have the chwzpah to say this in print. So, Gentry wins.

This leads me to a discussion of the state of eschatological
writing in this, the final decade of the second millennium after
the life, death, resurrection, and ascension of Jesus Christ, the
