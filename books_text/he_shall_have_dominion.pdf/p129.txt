Introdustion to Postmillennialism 83

over to His faith, and all to obey His teaching. . . .”” “For
where Christ is named, and His faith, there all idolatry is de-
posed and all imposture of evil spirits is exposed, and any spirit
is unable to endure even the name, nay even on barely hearing
it flies and disappears. But this work is not that of one dead,
but of one that lives — and especially of God.””* In fact, re-
garding idols, Christ “chases them away, and by His power
prevents their even appearing, yea, and is being confessed by
them all to be the Son of God.””* Athanasius goes on to exult
in Christ’s continuing victory:

The Saviour does daily so many works, drawing men to religion,
persuading to virtue, teaching of immortality, leading on to a
desire for heavenly things, revealing the knowledge of the Fa-
ther, inspiring strength to meet death, shewing Himself to each
one, and displacing the godlessness of idolatry, and the gods and
spirits of the unbelievers can do none of these things, but rather
shew themselves dead at the presence of Christ, their pomp
being reduced to impotence and vanity; whereas by the sign of
the Cross all magic is stopped, and all witchcraft brought to
nought, all the idols are being deserted and left, and every un-
ruly pleasure is checked, and every one is looking up from earth
to heaven. . .. For the Son of God is ‘living and active,’ and
works day by day, and brings about the salvation of all. But
death is daily proved to have lost all his power, and idols and
spirits are proved to be dead rather than Christ.”

Athanasius applies prophecies of the triumph of Christ to the
Church age and even rhetorically asks: “But what king that
ever was, before he had strength to call father or mother,

73. Athanasius, Incarnation 30:4,

74. Ibid. 30:6.

75, Ibid. 30:7.

76. Ibid. 31:2-3. This is particularly significant in that idolatry was a world-wide
phenomenon (2 Kgs. 17:29; | Chron. 16:26; Psa. 96:5) in which Satan exercised
control of men through demonic power (Lev. 17:7; Deut. 32:17; Psa. 106:37; 1 Cor.
10:19-20). Satan's binding (Rev. 20:2-3; Matt. ]2:28-29) is increasing “day by day.”
