128 HE SHALL HAVE DOMINION

The Proximate Standard of Righteousness

A fundamental theological assertion of orthodoxy is this: the
unity of God. Consequently, there is no reason flowing from this
unified God that either compels us or predisposes us to expect
that His one creation has two plans operative in its historical
progress. We should reject all ethical systems that propose two
systems of law or two decrees of God. I have in mind here the
dualistic theory of a universally logical natural law for non-
Christians (Gentiles) and Bible-revealed law for Christians.

Man’s sanctification (moral restoration) is definitive, progres-
sive, and final. One God, one covenant law: through time and
across borders. The successive covenants of Scripture really
record for us a gradual historical unfolding of one overarching
covenant, rather than the successive, compartmental establishing
of distinctively different capsule covenants. This is clearly ex-
pected in the initial covenant directive of God for history that
flows out of the Genesis 3:15 curse, which mentions only one
basic struggle between two seeds, the Satanic and the Messianic.

This also is clearly asserted in Paul’s argument in Ephesians,
chapter 2. In this passage, Paul speaks not of the establishing of
a new and distinct community separate from Israel, but of God’s
annexing of additional people — the Gentiles — into His one people.
He speaks in verse 12 of “the covenants of the promise”
(Greek), which defined His singular purpose. In verses 14-16,
he speaks of the removal of the dividing wall between Jew and
Gentile, so that the Gentiles might be included in God’s one
redemptive purpose. In verses 19-22, he speaks of the merging
of these two peoples into one, indivisible temple.

Thus, the very unity of God’s covenantal dealings with man
flows out of the unitary being of God, as well as the explicitly
revealed plan of God. These truths should predispose us to
assume continuity, as opposed to discontinuity, in the ethical
dictates of God.

It may be summarily stated that God’s Law is binding (in
that we are obliged to obey it for our sanctification), relevant (in
