238 HE SHALL HAVE DOMINION

(Matt. 13:33) instruct us as to the gradual development and
ultimate outcome of the kingdom. Let us consider a little more
closely the outcome of the kingdom, as spoken of in the last two
parables mentioned.

The Parable of the Mustard Seed reads: “The kingdom of
heaven is like a mustard seed, which a man took and sowed in
his field, which indeed is the least of all the seeds; but when it
is grown it is greater than the herbs and becomes a tree, so that
the birds of the air come and nest in its branches” (Matt. 15:31-
$2).!' The imagery is unquestionably of something magnificent
beyond expectation: a minuscule mustard seed gives rise to a tree.
To this tree flock the birds of the air in order to build their
nests for their young. The Old Testament imagery involved
here is interesting. Birds singing among the branches is a pic-
ture of peaceful serenity and divine provision (Psa. 104:12, 17). In
Daniel 4:12 and Ezekiel 31:3, 6, Babylon and Assyria (which
God providentially prospered, Jer. 27:5-8; Ezek. 31:3, 91") are
portrayed as massive kingdoms to which birds flocked to nest in
their branches. Daniel 4:12 indicates that this speaks of a lovely
provision of food for all; Ezekiel 31 shows that this symbolizes
the kingdom's fairness, greatness, and provision for all great
nations. That is, they were great kingdoms which for a time
secured provisions and shelter for men.

But God has a kingdom that also will be a great tree pro-
viding a nesting place for the birds and their young. Ezekiel
17:22-24 reads: “I will also take of the highest branch of the
high cedar, and will set it; I will crop off from the top of his
young twigs a tender one, and will plant it upon an high moun-
tain. In the mountain of the height of Israel will I plant it: and
it shall bring forth boughs, and bear fruit, and be a goodly
cedar: and under it shall dwell all fowl of every wing; in the

11, An excellent exposition of this parable may be found in Richard C, Trench,
Notes on the Miracles and the Parables of Our Lord, 2 vols. in 1 (Old Tappan, NJ: Revell,
{n.d.] 1953), 2:109-115.

12. Cf. Psa. 75:6-7; Dan. 2:21; 4:17, 32; Job 12:23.
