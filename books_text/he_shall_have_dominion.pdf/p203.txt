The Hermeneutic of Scripture 157

until the fulfillment came.” The conclusive New Testament
revelation was needed (Heb. 1:1-2).

The Emmaus disciples, holding to current literalistic Jewish
conceptions, nceded to have Christ open the Scriptures to them
(Luke 24:32, 45). Christ rejected the political Messianism of the
literalistic Jews. The Jews had a dullness of understanding™
that seems to be accounted for (at least partially) in that “the
prevailing method of interpretation among the Jews at the time
of Christ was certainly the literal method of interpretation.”*®
After all, when Christ confronted Nicodemus, He pointed to
this very problem: “Jesus answered and said to him, ‘Are you
the teacher of Israel, and do not know these things? .. . If 1
have told you earthly things and you do not believe, how will
you believe if I tell you heavenly things?’ * (John 3:10, 12).
Literalism plagued the Jews throughout Jesus’ ministry.’ Few
would dispute the fact that the Jews of Christ’s day looked for
a political Messiah (John 6:14-15; 18:33-36). The Emmaus
disciples were rebuked for just such a conception (Luke 24:17-
21, 25-26). Christ suffered, then entered immediately into His
glory. The cause of Israel's rejection of Christ is due (at least
partially) to their not knowing He fulfilled prophecy (Luke

53. Poythress, Understanding Dispensationalists, p. 107.

54. Matt. 23:37-38; Luke 19:41-42; 24:21-27; John 6:15; 18:36.

55. 2 Cor. 3:14; cf. Matt. 13:15; John 8:12; 12:46; Acts 28:26-27; Rom. 11:7-8.
The dullness led eventually to their ascribing Satanic influence to Christ (Matt. 12:22-
28).

56. Pentecost, Things to Come, p. 17. See also: Richard Longenecker, Biblical
Exegesis in the Apostolic Period (Grand Rapids: Eerdmans, 1977), ch. 1. Bernard Ramm,
Protestant Biblical Interpretation (Boston: W. A. Wilde, 1950), pp. 48£ In fact, the
fundamental idea of a premillennial kingdom seems to be traceable back to the
literalistic Jewish conception, and thus it may be said that “premillennialism is a
descendent of ancient Judaism.” William Masselink, Why Thousand Years? (Grand
Rapids: Eerdmans, 1930), p. 20. See also: Leon Morris, The Revelation of St. fohn
(Grand Rapids: Eerdmans, 1969), p. 234; Henry B. Swete, Commentary on Revelation
(Grand Rapids: Kregal, [1906] 1977), p. cxxxiii; Feinberg, Millennialism, pp. 34-35.

57. See: John 2:19-21; 3:5-7; 4:10-15, 31-38; 6:31-35, 51-58; 8:21-22, 32-36;
8:51-53; 9:39-40; L1:11-14; 13:33-37; 18:33-37,

58. Luke 24:26; 1 Pet. 1:11. Gf. John 12:23-24; Phil. 2:8-9.
