Pragmatic Objections 431

tion by nuclear warfare.”" A book brimming with newspaper
clippings of the portentous danger of Communist Russia warns:
“We are on an irreversible course for world disaster.””

Note well: No knowledgeable postmillennialist would point
to the current apparent collapsing of Communism as definitive
proof of postmillennialism (although Communism everywhere
will have to collapse before the final stages of the postmillennial
advance of Christ’s kingdom, since it is inherently anti-Chris-
tian).” Yet, it is encouraging to watch the freeing up of Chris-
tianity in the totalitarian lands once dominated by monolithic,
atheistic Communism - despite claims by dispensationalists of irre-
versibly worsening world conditions. It is a heartening cause for
rejoicing and continuing prayer (and surely the result of the
effectual fervent prayers of persecuted Christians) to read such
headlines as: “Religion Gains Momentum in Soviet Union,”'*
“Prayers and Bible Welcomed in the Kremlin,” “Albania
Awakes from Atheism,”!® “Churches Gain Favor with Castro,
See Spiritual Awakening,””” “New Law Extends Religious Frec-
dom,”* and “Evangelism Finds a Place on New Soviet Agen-
da.” Who knows where all of this will lead in the near fu-
ture? If “short run” arguments were valid, such headlines could
just as easily be used as newspaper exegesis for the evidence of
postmillennialism!

11, Adams, Time Is at Hand, p. 2.

12. Salem Kirban, Countdown fo Rapture (Eugene, OR: Harvest House, 1977), p.
11. Gf. pp. 148-160.

18. Gary North, Marz’s Religion of Revolution: The Doctrine of Creative Destruction
(rev, ed.; Tyler, TX: Institute for Christian Economics, [1968] 1989). Francis Nigel
Lee, Communist Eschatology: A Christian Philosophical Analysis of the Views of Marx, Engels,
and Lenin (Nutley, NJ: Craig, 1974). David Chilton, Productive Christians in an Age of
Guilt Manipulators (3rd ed.; Tyler, TX: Institute for Christian Economics, 1985).

14. New York Times release, Greenville Piedmont (Oct. 7, 1991) Al.
15. Article in Christianity Today 35:11 (Oct. 7, 1991) 42-43.

16. Art Moore, in Christianity Today 35:6 (May 27, 1991) 52-54.

17. Christ Woehr, in Christianity Today 35:1 (Jan. 14, 1991) 46ff.

18. Ken Sidey, in Christianity Today 34:16 (Nov. 5, 1991) 76ff.

19. Russell Chandler, in Christianity Today 34:18 (Dec. 17, 1990) 39ff.
