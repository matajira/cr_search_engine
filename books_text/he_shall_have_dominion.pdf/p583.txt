Postmillennialism and Suffering 537

is the purpose of God-inflicted suffering. He promises to curse
covenantal unfaithfulness (Deut. 28:1-14), but to bless covenan-
tal obedience (Deut. 28:15ff). This blessing is cultural, involving
all aspects of life: population growth (vv. 4, 11), economic pro-
ductivity (vv. 11-12), political stability (vv. 7, 13), agricultural
abundance (vv. 4-5, 8, 11), increased health (contra wv. 27-29),
favorable weather (v. 12), and so forth.

Of Israel’s trial in the wilderness, we learn: “He gave you
manna to eat in the desert, something your fathers had never
known, to humble and to test you so that in the end tt might go
well with you” (Deut. 8:16).

David is confident in the eventual blessing of the Lord,
despite tribulation. This is the message of Psalm 37:

Do not fret because of evil men or be envious of those who do
wrong; for like the grass they will soon wither, like green plants
they will soon die away. Trust in the Lorp and do good; dwell in
the land and enjoy safe pasture. Delight yourself in the Lorp
and he will give you the desires of your heart. Commit your way
to the Lorn; trust in him and he will do this: He will make your
righteousness shine like the dawn, the justice of your cause like
the noonday sun. Be still before the Lorn and wait patiently for
him; do not fret when men succeed in their ways, when they
carry out their wicked schemes. Refrain from anger and turn
from wrath; do not fret — it leads only to evil. For evil men will
be cut off, but those who hope in the Lorp will inherit the land.
A little while, and the wicked will be no more; though you look
for them, they will not be found. But the meek will inherit the
land and enjoy great peace. The wicked plot against the righ-
teous and gnash their teeth at them; but the Lord laughs at the
wicked, for he knows their day is coming. The wicked draw the
sword and bend the bow to bring down the poor and needy, to
slay those whose ways are upright. But their swords will pierce
their own hearts, and their bows will be broken. Better the little
that the righteous have than the wealth of many wicked; for the
power of the wicked will be broken, but the Lorp upholds the
righteous. The days of the blameless are known to the Lorp, and
