Foreword XXXVii

as theonomic postmillennialism. All future expositions in the
name of this position will have to build self-consciously on He
Shall Have Dominion. As the old advertisement used to put it,
“Accept no substitutes!”

Gentry got to the finish line first. To the victor belong the
spoils. This will not win him cheers from the also-rans.

The Task of the Critics

Consider the wealth of documentation in this book. It will
not be sufficient for a critic to conclude in some two-page re-
view that “Gentry’s book just does not prove his case.” If any-
one tries this stunt, the careful reader should ask: “Then what
theologian has produced an equally comprehensive book that
defends a rival position?” At this stage of history —- approaching
the year 2000 — to refute Gentry’s book will require a compre-
hensive positive case presenting a rival eschatology with equal
or greater diligence. The critics should not expect to be able to
refute something this comprehensive with anything less com-
prehensive and detailed. I must remind the critics of an old
political slogan: “You can’t beat something with nothing.”

First, let me remind the reader of the disastrous attempts so
far by a few theologians to refute both theonomy and postmil-
lennialism. Westminster Seminary’s attack, Theonomy: A Reformed
Critique (1990), called forth my book, Westminster's Confession
(1991), Bahnsen’s No Other Standard (1991), and the collection
of essays, Theonomy: An Informed Response (1991). In it, Gentry
refuted amillennialist Richard Gaffin’s feeble essay, point by
point. Gentry had already refuted in great detail the embar-
rassingly weak criticisms of postmillennialism that were set forth
by Rev. Thomas D. Ice in Ice’s section of the co-authored and
ill-fated book, Dominion Theology. Blessing or Curse?** There was

24. H. Wayne House and Thomas D. Ice, Dominion Theology: Blessing or Curse?
(Portland, OR: Multnomah, 1988). House left Dallas Seminary the next year to join
the faculty of an obscure Baptist college in Oregon. In 1992, he departed from that
