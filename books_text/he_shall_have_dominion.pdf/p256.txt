11

REALIZATION

After John was put in prison, Jesus came to Galilee, preaching the gospel
of the kingdom of God, and saying, “The time is fuifilled, and the king-
dom of God is at hand. Repent, and believe in the gospel.” (Mark 1:14-
15)

The Scriptures, being an infallible and unified revelation of
God, continue the prophetic victory theme into the New Testa-
ment. This is despite the charges by some amillennialists that
the postmillennial hope cannot be sustained in the New Testa-
ment: “Whatever support postmillennialism may draw from its
own interpretation of the Old Testament, we question seriously
whether the New Testament gives any valid encouragement to
this theory.”' As I shall show, such charges are wholly without
merit. There is ample witness to the postmillennial hope in the
New Testament revelation. While dispensationalism’s Zionistic
approach to the kingdom promises of the Old Testament runs
into serious problems in the New Testament, such is not the
case with postmillennialism.

1. George L. Murray, Millennial Studies: A Search for Truth (Grand Rapids:
Eerdmans, 1948}, p. 86. See also: Richard B. Gaffin, “Theonomy and Eschatology:
Reflections on Postmillennialism,” Theonomy: A Reformed Critique, William S. Barker
and W. Robert Godfrey, eds. (Grand Rapids: Zondervan, 1990}, p. 217.
