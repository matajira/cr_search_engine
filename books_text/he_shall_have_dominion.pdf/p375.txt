Time Frames 329

imminent before the 1900s, if the 1900s are foreshadowed in
Revelation 2-3?)

Due to this doctrine of perpetual imminence, dispensationalists
should be the last people to seek signs of the approaching end. Such a
quest undermines their most distinctive doctrine: the ever-
imminent, sign-less, secret rapture. Yet, date-setting has long
been a problem associated with premillennialism, especially
dispensationalism.™ The last twenty years have been particu-
larly rife with cries of the approaching end. Alden Gannet
(1971): “While many of God’s people through the centuries
have looked for Christ’s imminent return, it is only in our
generation that the events of Ezekiel 37 are beginning to come
to pass.” Charles Ryrie (1976): “[T]ake a good look again at
current cvents. . . . How do you account for these unusual
events converging in our present day? Jesus said: ‘Even so,
when you see all these things, you know that it is near, right at
the door’ (Matthew 24:33 NIV).” Herman Hoyt (1977): “The
movement of events in our day suggests that the establishment
of the kingdom is not far away.”® Hal Lindsey (1980): “The
decade of the 1980’s could very well be the last decade of histo-
ty as we know it.” Dave Hunt (1983): “This is strong evidence
indeed that the Antichrist could appear very soon — which
means that the rapture may be imminent.” In 1988, Edgar C.
Whisenant created an uproar among expectant dispensational-
ists, when he published his 88 Reasons Why the Rapture Could Be

Prophetic?” Grace Theological Jounal 6:2 (Fall 1985) 267-273; Hal Lindsey, There's a
New World Coming (Santa Ana, CA: Vision House, 1973), pp. 38ff; The Scofield Refer-
ence Bible (New York: Oxford University Press, [1909] 1945), pp. 1331-2; The New
Scofield Reference Bible (New York: Oxford University Press, 1967), p. 1353.

64, See the classic historical study on the problem: Dwight L. Wilson, Armageddon
Now! The Premillenarian Response to Russia and Israel Since 1917 (Tyler, TX: Institute
for Christian Economics, {1977] 1991). For an exegetical study of the error, see: Gary
DeMar, Last Days Madness: The Folly of Trying to Predict When Christ Will Return (Brent-
wood, TN: Wolgemuth & Hyatt, 1991).

65. It is ironical that his niece, a postmillennialist, was one of the proofreaders
of my manuscript.
