206 Second Chance

This explains why we don’t have to keep the cleansing laws of the
Old Testament, Even though some of them have good medical
consequences, the New Covenant man is not morally impure if he
eats pork or shrimp (and I am thankful!).

Applying this principle to unlawful second marriages, I be-
lieve that they should be prosecuted against, but not broken up,
especially if there are children involved. The Church goes
through the steps of Church discipline. If the couple repents, then
they make public confession of sin and pay restitution. If they do
not repent, then they would have to be excommunicated.

5. Protection of Inheritance by Remarriage

The Church protects the inheritance of the family by en-
couraging remarriage. As Adams noted above, “The New Testa-
ment always speaks favorably about remarriage,” and the reason
is that the Church maintains a special role in the life of the family
when it encourages lawful remarriage.

First, Paul indicates that the Church and not the State is the
guardian of the family, when he entrusts the Church with the re-
sponsibility of taking care of widows and orphans (1 Timothy
5:3-16). (Since those who are lawfully divorced are widows, there
may be a Biblical basis here for the Church’s taking care of the
lawfully divorced.)

Second, Paul encourages the young widow to remarry. He
says, “I desire that the younger widows marry, bear children,
manage the house, give no opportunity to the adversary to speak
reproachfully” (1 Timothy 5:14). By encouraging remarriage, he
moves the Church into a unique guardian role of the family
estate. He implies that the Church protects the estate through a
second marriage. If remarriage occurs, the Church will not get
the inheritance and certainly not the State.

Thus, as we saw with the Church’s involvement in the divorce
lawsuit, its relationship to the remarriage process is vitally im-
portant!
