Living Happily Ever After 101

Summary

1. I began with the story of Roger and Norma to consider
what a person should do if he’s the innocent party and discovers
that his spouse has committed a divorceable offense, thereby kill-
ing the marriage covenant.

2. I presented the fifth principle of the Biblical covenant
model, transfer, and applyied it to the marital covenant.

3. The Biblical covenant teaches that the inheritance of the
people of God was passed to the faithful when a death occurred.
This covenantal process transferred the inheritance to the faithful
so that it would be preserved for them,

4. The marital covenant does the same. At the beginning of
the chapter (Deuteronomy 24:1-3), I pointed out that the guilty
party was given a bill of divorce, a statement of death like a death
certificate, and he was driven out of the house, which meant being
disinherited.

5. The principle of transfer applies if Roger restores his rela-
tionship to Norma. He will want to confront her and in the words
of Dr. James Dobson, “precipitate a crisis,” to cause Norma to see
just how much she is losing through her affair. He will want chal-
lenge her with the reality that she will be cut off and out of the in-
heritance, including seeing the children, if she does not repent.

6. The principle of transfer applies if Roger decides to divorce
Norma. He must understand the principle so that he can shame-
lessly transfer as much of his estate as possible to his side of the
family, and start all over,

7. The principle of transfer applies if Roger decides to stay
with an unrepentant Norma who decides to return to him. It will
help him to be able to enforce some kind of restitution to protect
his estate from Norma.
