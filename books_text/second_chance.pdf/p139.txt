The Period of Covenantal Transition it?

that he was not a Christian. Then one day, he went to church
with Greta because she said a very good preacher was going to be
there, and she thought that he might be interested in what the
man had to say. Reluctantly, he went . . . and apparently he was
converted, or so everyone thought.

Richard and Greta were married, and he quickly rose among
the ranks of leadership in his evangelical church. He was in-
telligent, more able than most to grasp many of the new Christian
concepts coming his way. He was rich. He was outgoing. And
with this combination, he scon became an elder in his church.

Richard was a fine leader in his church, his family, and his
community.

He assumed church responsibilities as though he had been
around the church all his life. Because he was so perceptive, he
even became the head-elder, and some were beginning to suggest
that he consider going into the ministry, maybe even become a
medical missionary.

In his family, he had two wonderful daughters. He loved them
very much, He wanted them to grow up strong in the Christian
faith, and so, he sent them to the best Christian school in the area.
He spent time with them, and he gave himself to their interests.

Over the years, he became known as Dr. Christian in his com-
munity, because he was such a zealous witness. He placed tracts
everywhere. He witnessed on the airplane, and he witnessed at
the hospital. He witnessed as a lifestyle. But he did more than just
talk the faith. He got involved in a service organization for the
blind, having been quite impressed by a blind evangelist who had
come through his church on a preaching tour.

Greta was thrilled. After considerable time praying and hav-
ing her friends pray, she was happy to see Richard become a
Christian and so thoroughly demonstrate a love for the Lord. She
thought to herself, “I’m finally going to be married to a Christian!”

All went pretty well for about twenty-five years, until late one
evening, Richard said he had to go back to the office. He was
gone for a long time, and he ended up being gone ail night. He
was a doctor though, and Greta was used to sudden all night calls;
