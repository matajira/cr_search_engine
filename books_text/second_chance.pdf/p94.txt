72 Second Chance

Covenanting With a Professional Fornicator

The “one flesh” phrase also indicates a condition of death. For
one, it pictures a human body being torn in half should the cove-
nant bond be broken. For another, it is used by the Apostle Paul
to describe a rival covenant that results in death. He says,

Do you not know that your bodies are members of Christ?
Shall I then take the members of Christ and make them members
of a harlot? Certainly not! Or do you not know that he who is
joined to a harlot is one body with her? For the “The two,” He says,
“shall become one flesh” (1 Corinthians 6:15-16).

Thus, Paul’s warning is that a relationship with a harlot brings
a rival sanction on the original marriage. What marriage? Even
though the passage does not explicitly say that the man in ques-
tion was married, the chapter following discusses the marriage
covenant and indicates that he was married. (Even if he wasn’t,
the same principle of rival covenant would be meant to be pulled
over into the marriage covenant.) In this case, however, his wife
would be the innocent party. She would be the one being brought
under the rival sanctions and rival covenant of the harlot, which
meant that the harlot was “lord” of her relationship to her hus-
band, and more importantly, she could become “lord” of her rela-
tionship to the Lerd. She would be in danger because her husband
would begin to mediate the death of the harlot to her. She needs
protection from these serious covenantal ramifications, which
Paul indicates from the context are guarded against by the same
principle of protecting the innocent.

Covenanting With an Amateur Fornicator
He says in the broader context that “fornicators” are dead in the
covenantal relationship and are therefore to be excommunicated.

It is actually reported that there is sexual immorality [‘Porneo’ is
used here, which is normally translated ‘fornication,’ and which is
the same Greek word in Matthew 5:32 that Jesus says is the only
reason for divorce.] among you, and such sexual immoratity as is not
