xviii Second Chance

tive Christian handbook on divorce and remarriage ever pub-
lished. Because it meets a perceived need as no other book in its
field has done or can do, its thesis will be accepted almost over-
night by hard-pressed marriage counselors who desperately need
a Bible-based approach that works, not traditional slogans that
don’t, This book unlocks the shackles of the “no divorce/no remar-
riage” tradition, but without delivering the newly freed counselor
into the bondage of humanism’s no-fault divorce system.

But then the counselor will face the beginning of a personal
theological crisis. He will have to ask himself; Why does the five-
point model work? Next, the brighter ones will ask themselves: In
how many other areas of life does this five-point made! also apply?
This question will lead them step by step to other books in the
Biblical Blueprints Series. They will be dragged, perhaps kicking
and screaming in the early stages, into several other areas of
Christian responsibility. If they pay attention to the covenant
structure, they will find that it opens not only formerly blurred
Bible passages but also a whole host of practical problems that the
Bible addresses authoritatively— problems that Christians have
been self-consciously ignoring throughout this century.*

Nevertheless, with greater knowledge always comes greater
personal responsibility. Some Christians resist strongly the idea
that they have cultural responsibilities as Christians, for this reali-
zation raises the painful corollary: if God has transferred to His
covenant people such widespread cultural responsibilities, then
they also have been given sufficient time in history to assume
these responsibilities and to deliver the cultural goods to a lost
generation. Thus, questions of Biblical ethics inevitably raise
questions of Biblical eschatology.®

The covenant’s answers can very often be even more painful
than even the theological questions, especially for pastors of large,
established congregations. Large, established congregations are

4, Douglas W. Frank, Less Than Conquerors: How Evangelicals Entered the Tiwen-
tisth Century (Grand Rapida, Michigan: Eerdmans, 1986).

5. Gary DeMar and Peier J, Leithart, The Reduction of Christianity (Ft. Worth,
Texas: Dominion Press, 1988).
