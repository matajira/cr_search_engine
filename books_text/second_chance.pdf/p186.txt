164 Second Chance

treated like royalty, meaning that he had become an adopted son
by being awarded a crown, a robe and a place beside the king in
his chariot, a place where only a son could stand (Esther 8:15-17).

Finally, at another level of understanding, and perhaps the
main message of the Book of Esther, Israel was brought back to
the Promised Land through Esther’s and Mordecai’s adoptions.
As a result of the good favor shown to the Jews, they were later
granted permission to return under Ezra and Nehemiah. In-
terestingly, the return was facilitated through a gentile, pointing
to the New Covenant times where the Jews are said to return to
the kingdom of God through the gentiles, and not apart from them
{Romans tt:11-32),

So, all of these adoptions point to the same five areas I men-
tioned above:

New Name
New Position
New Law

New Sign

New Inheritance

What happened in the covenant is also found in the process of
adoption in the family, When a spouse dies covenantally, he loses
his family covenantal relationship to his children, allowing for
adoption. I believe that this is the key principle to solving the
problems related to stepchildren. So, let’s apply it to see how it
works,

How to Deal with Stepchildren

Using the story of George and Judy from the beginning of the
chapter, and applying the principle of adoption, here are some
suggested steps for any two people entering a second marriage
with children from previous marriage(s).

1. At least discuss how any children from a previous marriage
will be viewed by each partner in the new marriage. George and
Judy should have decided ahead of time whether or not her son
would be ¢heir son or Judy’s, and whether or not George’s kids
