124 Second Chance

and initiating accordingly. At a practical level, however, a mar-
ried person gets used to another kind of hierarchy: the jurisdiction of
learning io cope with how a mate’s habits and ways structure his life. He
learns to orient his whole life around his spouse. Even if he’s try-
ing to avoid that spouse, he’s still under the hierarchy or jurisdic-
tion placed on him, When covenantal death occurs, the innocent
spouse is forced to adjust to cope with the situation, and most of
the time the adjustment blurs the innocent party’s judgment.

For example, if a woman has had to live with an adulterous
spouse who has become totally thoughtless, she starts to crave any
attention. By the time the divorce finally occurs, she is emo-
tionally malnourished. She will tend to elevate the characteristics
that her previous spouse lacked, almost to the exclusion of very
basic and necessary characteristics, maybe even characteristics
that if they are not present will be much more destructive to her
than her previous spouse’s problems. At any rate, the woman who
has been emotionally starved will be tempted to marry the first
man who comes along and does a few things for her. She has not
adjusted so that she thinks clearly and so that she looks at many
other areas of her new suitor’s life, often glaring weaknesses that
far outweigh the fact that he brings her flowers at the office where
she works. But she was affected by the covenantal death of her
husband who committed adultery, and has become infatuated
with someone else and completely neglected her mental and emo-
tional needs. She is vulnerable.

The Guilty Party

As for the guilty party, he too falls under the same jurisdic-
tional influence of his spouse and his mistress. Remember that he
enters a pseudo-covenant, according to the Apostle Paul, and so
he has two hierarchies that force him into certain habits and ways
of thinking that in turn influence him. If he repents, assuming
that he has lost his innocent spouse to another marriage, he may
want to remarry to another. But he will need to be even more
careful in allowing a statute of limitation to pass on the effects of
his own death, and in allowing his head to clear from the moral
