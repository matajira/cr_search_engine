Author's Introduction ii

renewing a covenant between God and man is analogous to form-
ing a marriage covenant or entering a second marriage, and dis-
solving a covenant is analogous to divorce.

For example, when the God-to-man covenant is violated, God
begins a process called a covenanial lawsuit, He sends His messen-
gers or witnesses of the covenant to prosecute the offending party.
If the guilty party does not repent, then He divorces the offender.
As in the case of the Laodicean church, Jesus says to them that He
is the witness bringing a lawsuit and He will dissolve His cove-
nant with them if they don’t repent:

And to the angel of the Church of the Laodiceans write, “These
things says the Amen, the Faithful and True Witness, the Begin-
ning of the creation of God: ‘I know your works, that you are
neither cold nor hot . . . I will spew you out of My mouth’” (Rev-
elation 3:14-16).

Notice the degal language such as “witness,” which means Jesus is
entering a covenantal lawsuit against this church, Since it disap-
peared in history, it was obviously divorced. Thus, covenant lawsuit is
the Biblical guide to answering all of those tough divorce questions.

Let’s consider the other side of the coin: remarriage. Using the
concept that marriage is a picture of God’s covenant with man,
since man can enter a second covenant with God, a New Cove-
nant, it is quite possible for a man and woman, who have been
properly divorced on Biblical grounds, to enter a second or new
covenant; in other words, they can remarry. Just as the New Cov-
enant is a process of adoption, so we will find that marriage and re-
marriage are adoptions, because according to the Biblical tradi-
tion, the woman receives the husband’s name in the same way
that a Christian receives Christ’s name at baptism. Again, the
covenant is the key to divorce, and now we will be able to see in
greater detail that the covenant will enable us to unravel those
rough questions on remarriage.

The structure of the book is simple. I give five principles of
divorce in the first five chapters, what I have already called a cov-
enantal lawsuit. And I present five principles of remarriage, cove-
