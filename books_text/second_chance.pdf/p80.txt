58 Second Chance

Bible, there is no such distinction as “good and bad” magic. All
magic is bad, and it is an attempt to undermine the Word of God.
Again, any attack on the Lord is an attack on marriage.

3. Sabbath-breaking

We must be careful to understand this law. In the Old Testa-
ment, it was not the mere picking up of wood on the Sabbath Day,
as a superficial reading of the Bible text might indicate. Rather,
Sabbath-breaking was a deliberate attempt to foil a special day of
rest by overtly working when work was not necessary, and a self-
conscious effort to detract from worship by setting up the fire in
the home in such a way that it became a rival place of sacrifice, ex-
plaining the reference to picking up wood.®

In view of this understanding, strictly speaking, the death
penalty was not for working on the sabbath, although work was
not allowed unless “your ox was in a ditch,” something which is
called an acé of necessity. Rather, the sanction was for something
much more basic, For example, Pharaoh was a sabbath-breaker
because he would not let God’s people worship (Exodus 5-14). In-
stead, he set up his own place of rival worship to keep the nation
of Israel from worshipping their God. Thus, he received the death
penalty.

Applied to marriage, a person is not allowed to stop his spouse
from worshipping God, that is, keep his mate from going to
church for Sunday worship. If he (she) does, he commits a very
serious offense because he violates the essence of what the sabbath
is all about. He threatens the innocent party’s spiritual life, for
every believer is commanded to “assemble himself together with
other believers to worship God” (Hebrews 10:25-31). And, he also
threatens his own spiritual life. He does something for which God
destroyed Pharaoh and his whole army, and he commits an act
that kills the marriage covenant just as much as it covenantally
kills the offender.

8. James B. Jordan, Sabbath Breaking and the Death Penalty: A Theological Investi-
gation (Tyler, Texas: Geneva Ministries, 1986).
