Everyone Needs to Be Adopted 161

become children of Gad, even to those who believe in His name,
who were born not of blood, nor of the will of the flesh, nor of the
will of man, but of Ged” (John 1:12-13).

Second, Abraham was given a new position as a son. A son in
the Bible is set in contrast to a slave. He rules, whereas a slave is
ruled, Perhaps the most graphic story in the Bible of this contrast
is the account of the prodigal son. He shows what happens when
sonship is Jost and then regained.

A certain man had two sons, And the younger of them said to
his father, “Father, give me the portion of goods that falls to me.”
So he divided to them his livelihood. And not many days after, the
younger son gathered all together, journeyed to a far country, and
there wasted his possessions with prodigal living, But when he had
spent all, there arose a severe famine in that land, and he began to
be in want. Then he went and joined himself to a citizen of that
country, and he sent him into his fields to feed swine. And he
would gladly have filled his stomach with the pods that the swine
ate, and no one gave him anything. But when he came to himself,
he said, “How many of my father’s hired servants have bread
enough to spare, and I perish with hunger! I will arise and go to
my father, and say to him, ‘Father, 1 have sinned against heaven
and before you, and I am no longer worthy to be called your son.
Make me like one of your hired servants.” And he arose and came
to his father. But when he was still a great way off, his father saw
him and had compassion, and ran and fell on his neck and kissed
him, And the son said to him, “Father, I have sinned against
heaven and in your sight, and am no longer worthy to be called
your son.” But the father said to his servants, “Bring our best robe
and put it on him, and put a ring on his hand and sandals on his
feet. And bring the fatted calf here and kill it, and let us eat and be
merry; for this my son was dead and is alive again; he was lost and
is found” (Luke 15:11-24).

The story of the prodigal son is about adoption. It is a case
where a man’s son gave up his sonship (his inheritance), became a
slave, and had to be adopted back into the house. And when he
was adopted, he became a king. He was given a ring, a robe, and
