54 Second Chance

cut off from among their people. .. . I am the Lord (Leviticus
18:1-30, emphasis mine).

First, God begins this chapter on marriage with an introductory
summary statement of the Ten Commandments. The statement,
“I am the Lord,” begins, ends, and periodically appears all
through the passage. It is the introductory statement of the terms
of the covenant that says there is one Lord and therefore one cove-
nant with the Lord. Its repeated use emphasizes that other rela-
tionships are viewed as rival covenants, It falls in the context of,
“Nor shall you take a woman as a rival” (Leviticus 18:18). It com-
municates that all of the illicit sexual relationships condemned in
this passage are part of the lifestyle of Egypt, making them aspects
of the previous covenant relationship they had to the false gods of
Egypt. It says in essence, “You have left the previous covenant,
and you have a new covenant with me; any false relationship is a
rival covenant to the new covenant into which I have brought
you.”

Second, by the use of the introductory statement to the Bibli-
cal covenant in the context of a chapter on marriage, God con-
nects His covenant with His people’s marriage covenant. Since
He created marriage and marriage is always in some sense in
Him, any challenge to His covenant is a challenge to the marital
covenant, which goes all the way back to the garden. When Adam
and Eve died, their marriage died.

Third, the marriage covenant is protected by the death penalty,
just as the Biblical covenant is protected by the death penalty.
Notice how many of the capital offenses are mentioned in this pas-
sage: incest (v. 6), adultery (v. 20), child sacrifice (v. 21), homo-
sexuality (v. 22), and bestiality (v. 23). A capital offense was also a
marital offense, meaning the death of the Biblical covenant would kill the
marriage covenant.

Moses and Jesus

Jesus confirms this observation about the relationship between
marital offenses and the capital offenses. He says,
