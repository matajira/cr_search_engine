132 Second Chance

to them. Nor shall you make marriages with them. You shall not give
your daughter to their son, nor take their daughter for your son.
For they will turn your sons away from following Me, to serve
other gods; so the anger of the Lord will be aroused against you
and destroy you suddenly (Deuteronomy 7:1-4),

Paul taught this Mosaic principle with the analogy of a yoke. It
was a harness with two openings for oxen or some other domestic
animals. It kept the two animals moving in the same direction, so
that they would not pull against each other. But it was assumed
that the two animals being yoked would be the same kind of animals.
It was powerless to hold a donkey and an ox together, for exam-
ple, and in this regard it illustrated one of the fundamental princi-
ples of the covenant. It taught that the people of God are only to
enter covenant with another who is covenanted to the Lord.

Applied to marriage, the equal-yoke principle means that two
people should only marry if they are bound by the same yoke. Or
to put it another way, the principle means that anyone consider-
ing marriage, whether for the first, second, or third time, should
marry someone who is covenantally compatible. What is this? I be-
lieve that Moses placed the equal yoke command at the beginning
of the ethics segment of the covenant to imply that the stipulations
of the covenant itself would lay down the ethical boundaries for en-
tering any kind of new covenant. So, I think that we can go to the
terms of the marriage covenant to find the proper guidelines for
being equally yoked in a new marriage.

Terms of Equal Yoke

The terms of the marital covenant parallel the terms of the
Biblical covenant, as expressed in the Ten Commandments. They
are found in the first marriage.

And Adam said: “This is now bone of my bones and flesh of my
flesh; she shall be called woman, because she was taken out of man.”
‘Therefore a man shall leave his father and mother and be joined to his
wife, and they shall become one flesh. And they were both naked, the
man and his wife, and were not ashamed (Genesis 2:23-25).
