190 Second Chance

Fifth, he should consider the consequences. He may have legiti-
mate grounds for a divorce, but it may still be unwise to pursue it,

I pointed out that remarriage is an adoption process, and I
listed five areas involved: New name, new position, new law, new
sign, and new inheritance. First, change of name is essentially re-
ligious. Second, I spoke about the family structure and related
problems involved in remarriage. Third, I talked about the need to
know the family customs of the person you’re marrying. Fourth, I
discussed the importance of the ceremony. Fifth, I noted the need
to work out inheritance issues,
