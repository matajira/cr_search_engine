Everyone Needs to Be Adopted 165

would be their children or simply George’s.

2, Preferably, previous children will be adopted into the new
marriage. I know that State no-fault divorce laws make it nearly
impossible to adopt children from previous marriages in the eyes
of the State, but it would not prevent a Biblical adoption such as I
have described above. Here is how the process would take place:

(1) The children who are old enough would be given the
choice of whether or not they would be adopted into the new home,

(2) The ones who decide to be adopted would be given some
symbol of adoption, maybe a ring or some other piece of jewelry, a
large party would be thrown, and they would be included in the
new family’s inheritance. And they would also share in the respon-
sibility of taking care of the new parents when they are old.

I should add that adoption into the new family does not neces-
sarily nullify the relationship with the previous parent(s). For ex-
ample, David was adopted into Saul’s family, so that he could be
heir to the throne (1 Samuel i8:1-5}, but there is no indication that
he was disinherited by his original father. Obviously, dual mem-
bership would complicate matters, but it would also mean double
inheritance.

(3) The parent of children too young would make the decision
for them on the same basis that Abraham had his infant male chil-
dren circumcised before they understood the adoption process.

(4) Finally, the children who decide not to be adopted would
be considered “working guests” in the house, who will be assigned
definite responsibilities in exchange for room and board until they
are legal age, But they would not be given first preference in the
new family covenant. And they would not have the same status as
the children who had decided to be adopted.

This process of dealing with the stepchildren problem gives
the new parents a system by which they can deal with so many of
the problems that arise, As I have seen time and again in counsel-
ing, it will work! It will help them around the naive idea that every-
one can be treated equally. It will guide them away from Cinderella’s
nightmare!
