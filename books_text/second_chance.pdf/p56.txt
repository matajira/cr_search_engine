34 Second Chance

curs to me that Bob is a zombie!”

Counselor: “Come on, you're not serious are you? I know Bob
is a little over-weight, looks kinda spaced out at times, and occa-
sionally acts a little weird, but I wouldn't classify him as a zombie.
I think that’s going a bit far Anne. Do you actually believe he's a
zombie?”

Anne: “Well... yes... I mean, no...

Counselor: “Go on, what do you mean? Either he is or he
isn’t.”

Anne: “No wait, I don’t mean he’s a real zombie. I mean it
seems to me that he’s dead, and our marriage is dead, but Bob’s
alive and our marriage is technically alive, I guess. It’s like we
have a living dead marriage.”

Counselor: “You'll still have to explain that one for me. You're
not making much sense to me. How could your husband be dead,
when he's alive? And how could your marriage be dead, when the
two of you are still married?”

Anne: “Let me back up for a minute, and please allow me to
come at it from another angle.”

Counselor: “Sure, take your time, because I really want to
understand you. And I especially want to see how a person could
be living and dead at the same time.”

Anne: “Q.K. I should begin with Bob’s spiritual condition, I
think you know that Bob has professed to be a Christian for a long
time.”

Counselor; “How long?”

Anne: “About twenty years. Anyway, our first ten years of
marriage seemed to go just fine. Then something happened; it’s
like we declared war on each other.”

Counselor: “What happened?”

Anne: “Pm not sure, but I think he started having an affair
with one of the teachers at the college where he is a professor. I be-
lieve that he’s been involved with this particular teacher for quite
some time. But I don’t think there was any involvement during
the first decade of our relationship. Then it started.”

Counselor: “What started?”

»
