148 Second Chance

a person what he wants to hear. He will tell him what needs to be
done, and he will usually tell him things to do that will take more
time. An older person usually has a more stretched-out view of
time, enabling him to see things more in the long term.

All of this is especially important when considering remar-
riage. A person needs objective guidance. He needs someone who
is not afraid to tell him what he doesn’t want to hear. And he
needs someone with a different view of time, someone who can
slow him down. He needs seasoned counseling.

2. Structured Counsel

And so it was, on the next day, that Moses sat to judge the peo-
ple; and the people stood before Moses from morning until eve-
ning. So when Moses’ father-in-law saw all that he did for the peo-
ple, he said, “What is this thing that you are doing for the people?
Why do you alone sit, and all the people stand before you from
morning until evening?” And Moses said to his father-in-law,
“Because the people come to me to inquire of God. When they
have a difficulty, they come to me, and I judge between one and
another; and I make known the statutes of God and His laws.” So
Moses’ father-in-law said to him, “The thing that you do is not
good. Both you and these people who are with you will surely wear
yourselves out. For this thing is too much for you; you are not able
to perform it by yourself. Listen now to my voice; I will give you
counsel, and God will be with you: Stand before God for the peo-
ple, so that you may bring the difficulties to God. And you shall
teach them the statutes and the laws, and show them the way in
which they must walk and the work they must do. Moreover you
shall select from all the people able men, such as fear God, men of
truth, hating covetousness; and place such over them to be rulers
of thousands, rulers of hundreds, rulers of fifties, and rulers of
tens. And let them judge the people at all times. Then it will be
that every great matter they shall bring to you, but every small
matter they themselves shall judge. So it will be easier for you, for
they will bear the burden with you” (Exodus 18:13-22).

The father-in-law of Moses, Jethro, noticed that Moses’ lead-
ership responsibilities were too large for one man to handle. He
