Fools Rush in Without Counsel 147

1. Seasoned Counsel

‘Then King Rehoboam consulted the elders who stood before
his father Solomon while he still lived, and he said, “How do you
advise me to answer these people?” And they spoke to him saying,
“If you will be a servant to these people today, and serve them, and
answer them, and speak good words to them, then they will be
your servants forever.” But he rejected the counsel which the elders
gave him, and consulted the young men who had grown up with
him, who stood before him. And he said to them, “What counsel
do you give? How should we answer this people who have spoken
to me, saying, “Lighten the yoke which your father put on us?”
‘Then the young men who had grown up with him spoke to him,
saying, “Thus you should speak to this people who have spoken to
you, saying, ‘Your father made our yoke heavy, but you make it
lighter on us’—thus you shall say to them: ‘My little finger shall be
thicker than my father’s waist! And now, whereas my father laid a
heavy yoke on you, I will add to your yoke; my father chastised you
with whips, but I will chastise you with scourges!’” (I Kings 12:6-11).

Rehoboam was the son of the great king, Solomon. He had
been asked by the people to lighten up on the demands that had
been placed by his father. So, he went to the elders to seek coun-
sel, which was a good thing. But they didn’t tell him what he
wanted to hear. They were older and wiser, and they told him to
think about serving the people, to endear them to him so that they
wouldn’t mind the demands placed on them. Rehoboam did not
like their counsel, so he did what so many people do: he found
counselors who would tell him what he wanted to hear! In this
case, he chose young counselors who would give him a quick and
easy solution. They told him to use force instead of service to gain
the compliance of the people.

The principle of confirmation is seasoned counsel. Anyone seek-
ing counsel should go to an older, and/or wiser counselor who has
gained lots of experience dealing with his particular problem. He
does not have to go to a counselor older than he is, but he should
go to someone who has matured in his abilities and insights. An
older counselor will have firmer and wiser counsel. He will not tell
