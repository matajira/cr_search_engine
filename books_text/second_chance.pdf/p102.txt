80 Second Chance

But the family in the Old Testament or Covenant could not
escape being under the disasterous effects of the Fall of man, and
so it failed as an institution to administer successfully the blessings
of the covenant. If anything, it meets with constant failure
throughout the history of the Old Testament, one generation after
another collapsing into immorality. It needed another to usher in
the blessings of God.

‘The other came in the Person of Jesus Christ. He was the new
agent through whom the blessings of the covenant were distrib-
uted. He replaced the first Adam, and because of this role, He
even encountered opposition by His own family, forcing Him to
make His most exacting description of this shift from the head of
the house to Himself, when He said, “‘Where is My mother, or
My brothers? And He looked around in a circle at those who sat
about Him, and said, ‘Here are My mother and My brothers!
Whoever does the will of God is My brother and My sister and
mother” (Mark 3:35).

Jesus was not opposed to the family, but He clearly became
the new agent through whom blessing, and therefore cursing,
would come. One day, He picked up little children and dlessed
them (Luke 18:15-17). Judging by the disciples’ response, who
tried to stop Jesus from blessing the children, He had hit a nerve.
By this act and by the previous statement about His true family,
He made Himself the primary agent of distributing blessing. He
had shifted the agency of blessing from the family to Himself. And
with that, He delegated this responsibility to the Church, when
He exercised His role as the agent of blessing saying to Simon
Peter, “Blessed are you . . . And I will give you the keys of the
kingdom of heaven, and whatever you bind on earth will be
bound in heaven, and whatever you loose on earth will be loosed
in heaven” (Matthew 16:17-19). Christ was giving to the Church
the responsibility of blessing and loosing that had heretofore been
given to the father in the family. He gave the Church the power to
marry and He gave the Church the power to grant divorce.

Thus, the shift from the family as the agent of divorce to the
Church would make securing a divorce much more difficult. As I
