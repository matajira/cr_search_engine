82 Second Chance

My purpose is not to expound this whole passage,’ but first,
the woman who falsely claims to be a virgin was to be put to death,
thereby protecting the man who had become the innocent victim.
Why? She had victimized her husband defore marriage, having
previously fornicated with another man. She entered the covenant
under deception, and according to Old Testament law, she was a
harlot. Why? According to the the law, a seduced virgin was sup-
posed to tell her father, so that fe could decide whether she would
go ahead and marry her seducer, and thereby protect the family
name (Exodus 22:16-17), If she failed to report the seduction to
her father, however, she was considered a harlot, and she was
liable to the death penalty for deceiving a man into marrying her.
So, she received the penalty of death for her unfaithfulness prior
to her marriage covenant, from which the innocent husband was
protected, according to the Bible.

Second, if the husband falsely accused his new wife of pre-
marital unfaithfulness, he was heavily fined, and he was not allowed
to divorce her. Evidently, he was given the equivalent of the death
penalty if she had not been found guilty. In this case, he was sen-
tenced to live with her until she died physically, which was a
severe form of protection for the innocent. A man would think
very seriously about whether he could prove his case before hav-
ing to receive a no divorce penalty, let alone the large sum of money
that the one hundred shekels represented.

In the case of the guilty woman and in the case of the guilty
man, however, a principle of the protection of the innocent was at
work, even though it was a rather unusual application. But now
that I have raised the whole issue of divorce, a myriad of other
questions surface: “When can I get a divorce? What happens to
the children and so forth?” In the next chapter, I will deal with
more of the specifics. Let’s quickly summarize, however, what we
have covered in this chapter.

7, For an excellent explanation of this passage in full, see Gary North, Tools of
Dominion: The Case Laws of Exodus (Tyler, Texas: Institute for Christian Eco-
nomics), to be published in 1988, chapter 16.
