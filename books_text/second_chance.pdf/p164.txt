IV. _Sanctions/Judgment

9
FOOLS RUSH IN WITHOUT COUNSEL

Without counsel, plans go awry, but in the multitude of coun-
selors they are established (Proverbs 15:22).

There’s an old saying, “Fools rush in where angels fear to
tread.” I’m changing that slogan a bit, as you can tell from the title
of this principle on remarriage. I'm calling it, “Fools Rush in with-
out Counsel,” because it captures the essence of the verse: “With-
out counsel, plans go awry, but in the multitude of counselors they
are established” (Proverbs 15:22). The whole Book of Proverbs is
designed to prevent a man from becoming a “fool,” meaning the
kind of moral foolishness that results in a covenant-breaking life-
style.

In a sense, all of the principles on remarriage have been de-
signed to prevent a covenant-breaking remarriage lifestyle. They have
focused on principles to help you understand not only if a person
can remarry after a legitimate divorce, but to enable you to see
the key problems associated with remarriage. They have ail
begun with an actual counseling experience of some sort that
describes the particular issue I’ve tried to address.

As I consider these situations, they have all had their unique
differences, but there is one common denominator that virtually
all of them have had: almost all failed io get counsel. That’s right, they
are like most people who get into serious trouble. They rushed in
without seeking the insights and advice of Biblical counselors:
pastors, teachers, Christian friends and counselors.

142
