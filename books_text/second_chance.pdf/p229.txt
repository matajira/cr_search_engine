What the Church Should Do 207

Summary

I began with the account of the ordeal of jealousy to introduce
the Church's role in a divorce lawsuit (Numbers 5:12-31). Any
Church that attempts to handle the divorce lawsuit should do five
things. First, I said that a legal dispute between believers should be
brought before the Lord by being processed through the Ghurch
and not the State. Second, a Church should be part of an appeals
system because some of the problems become bigger than the local
congregation can handle. Third, a Church should make sure the
case is actionable, meaning there is a Biblical ground for the
divorce, and also meaning there is Biblical evidence. Fourth, a
hearing should be conducted under oath. Fifth, the Church should
make a decision, either granting the divorce, and/or excommuni-
cating the unrepentant guilty party.

Next, I discussed the issues related in the Church’s in-
volvement in the remarriage process. First, the Church should re-
alize the Lord’s name is at stake more than the name of the parties
wanting to remarry. Second, the Church should allow its officers to
divorce and remarry for Biblical reasons, even though they may
have to allow for a statute of limitations to pass.

Third, conversion introduces 2 new cause/effect relationship
that would allow an unlawfully divorced person who converts to
remarry. Fourth, the Church should bring a judgment against
unlawfully married believers, but not force them to dissolve their
marriage if they are repentant and they are willing to pay restitu-
tion. Fifth, the Church protects the family inheritance by en-
couraging lawful remarriage.
