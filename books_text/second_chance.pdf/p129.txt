New Covenant, New Spouse 107

nant is to deny the gospel of Jesus Christ who was God incarnate,
and is presently God ascended on high, who offers a true second
chance to all men through the second, or new, covenant in His
blood. To deny this new covenant is to deny the Bible’s account of
the death of Jesus Christ, and most important, to deny it is to
deny the Resurrection of Jesus Christ, the greatest transcendent act
since the creation of the world. The Resurrection is the message of
hope, the true offer of a second chance to the world.

It is the event that makes a New Covenant possible. It offers a
second chance to man, but it is also the theological basis for the
concept of remarriage. Without it, there can be no thought of a sec-
ond covenant of any kind, and certainly not a marriage covenant.
With it, however, there is hope! There is a second chance!

The Marital Covenant

As we have seen throughout this book, the marital covenant
includes the same basic principles as the Biblical covenant,
because marriage is a picture of the relationship between God and
man. Since there are two covenants, or two marriages in the
Bible, the Old Covenant relationship between God and man is
pictured by dead marriages and divorce, and the New Covenant
is represented by living marriages and remarriage. It is rep-
resented by living marriages because they could not be living
without the Death and Resurrection of Jesus Christ. How so?
Marriage died at the Fall of man. Afterwards, if God had not
been anticipating the great redemptive act of Jesus Christ, there
would have never been another marriage. Any living marriage is
living because of the effect of the New Covenant; they are, we
could say, “New Covenant marriages.”

But the New Covenant also means that second covenants are
possible. Remarriage can occur, and it can take place because of
the way that God has set up marriage as an analogy of His cove-
nant with man. After the Cross, Jesus received a new bride, the
gentiles, because there was a New Covenant.

We can see the new spouse/new covenant idea illustrated in
the passage at the beginning of this chapter. If you look closely,
