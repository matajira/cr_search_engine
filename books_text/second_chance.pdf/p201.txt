Wheat the Family Should Do 179

measure you use, it will be measured back to you” (Matthew 7:2),
we should conclude that we receive the same judgment rendered
to our spouse if we are guilty of the same offense. For example, if
we are involved in adultery and pursue a lawsuit against our
spouse for the same, then we will be simultaneously ruled cove-
nantaily dead by God when the judgment is rendered to our
spouse. I believe that this would have a canceling out effect, just
as it did with Tamar and the woman caught in adultery. I don’t
believe that a person could legitimately pursue a divorce if he is
guilty of the same offense.*

2. Proceed with Accountability

When Judah found out that Tamar had committed what he
thought was an act of immorality, he sentenced her to death. How
could he do such a thing? As I pointed out in Chapter Three, he
could divorce his wife according to Old Testament law (Deuteron-
omy 24:1-3). But he would have still needed to have acted with
some kind of check and balance on his behavior. If he falsely ac-
cused his wife, he could receive a rigorous penalty, and he would
not ever be allowed to divorce her (Deuteronomy 22:13-21). In his
case, he was checked by the very pledge that he had made to
Tamar, when she produced it in front of everyone.

Also, in the case of perceived immorality of any kind, an Old
Testament man could take a suspected woman to the priest, and
he could have her submitted to the ordeal of jealousy where the priest
made her drink a special mixture of purified water from the taber-
nacle. If she was guilty, she died instantaneously (Numbers
5:11-31).2 So the ecclesiastical authorities could in some sense
serve as a check and balance on an unlawful divorce proceeding.

1, The historic court guidelines for the Presbyterian Church indicate prece-
dent for the principle that a person cannot charge another when he is committing
the same offense. The Book of Church Order for the Presbyterian Church in Amer-
ica says, “Great caution ought to be exercised in receiving accusations from any
person who is known to indulge a malignant spirit towards the accused; who is
not of good character; who is himself under censure or process; or who is known
to be litigious, rash or imprudent” (32-8).

2. See Chapter 12.
