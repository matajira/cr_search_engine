xii Second Chance

They must begin their search with these questions:

What is the character of God?

How does He relate to the creation?

How does He deal with man?

How is man supposed deal with other men?

‘We must start with God and God’s legal relationships with man-
kind. Only then can we safely begin to look for Biblical solutions
to the social problems of any era. Man is made in God’s image.
God’s covenantal relationships with men, both redeemed and
fallen, provide us with models of how we are to deal with each
other, both redeemed and fallen.

Throughout the Bible, God’s people are described collectively
as God’s bride. The marriage covenant becomes the model of the
God-Church covenant, Ezekiel 16 is a classic passage in this re~
gard; so is Ephesians 5:22-33. If marriage is the model, then what
about divorce?

God divorced Israel when Israel revolted by crucifying Christ.
This was the last straw. Israel had committed spiritual adultery
repeatedly, from the golden calf forward. God soon remarried; He
gained a new bride, the Church. Jesus Christ is the bridegroom of
the Church, not of Israel. The legal basis of this marriage was a
prior divorce, If God had not lawfully cast off Israel, the Church
could not legitimately be called God’s bride. God is not a
bigamist. Divorce and remarriage: without both of these cove-
nantal actions on God’s part, there could be neither Church nor
salvation in New Testament times.

Tf this is how God has dealt with mankind, then how are we to
deal with each other? If God established the Church on the basis
of covenantal divorce and remarriage, are we to use this as our ex~-
ample? If not, why not?

The Covenant

To understand the basis of divorce and remarriage in both the
Old and New Testaments, we must first understand the covenant.
Pastor Ray Sutton, after many years of marriage counseling,
