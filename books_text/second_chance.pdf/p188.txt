166 Second Chance

Summary

1. I began the chapter with story of George, a doctor who was
married to a woman, and who was deserted by her. I pointed out
the stepchildren problems that emerged in his second marriage to
Judy.

2. E presented the principle of adoption as the solution. Adop-
tion is directly tied to the continuity/inheritance point of the cove-
nant. It has five aspects to it, which I illustrated from the ratifica-
tion of Abraham's covenant.

New Name
New Position
New Law

New Sign

New Inheritance

3. I moved from the Biblical covenant to the family covenant
by referring to the story of Esther. The whole book is about the
principle of adoption, but it is clear that Esther was adopted by her
uncle Mordecai when her parents died. Just as the physical death
of a spouse enables his child to be adopted, so his covenantal death
will permit the same.

4, The Book of Esther teaches adoption at several levels since
not only Esther, but Mordecai and the nation of Israel were
adopted.

5. The steps for applying the principle of adoption to the step-
child problem are the following:

(1) Two people with children from a previous marriage who
are considering a second marriage should at least discuss how the
kids will be viewed and treated.

(2) The children who want to should be adopted in the five-
fold manner above.

(3) The children who do not want to be adopted should be
treated as working guests in the new home and they should be
given responsibilities to pay for the cost of room and board,
