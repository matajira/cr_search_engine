Burying the Living Dead 43

Application

Now we can understand why Anne and so many other people
have said in essence, “I feel as though f am married to a dead per-
son.” It may very well be, if the person has broken the marriage
covenant. It is possible to be physically married, even though the
marriage covenant has long since died! Anne was in this situa-
tion. Her husband had broken the marriage covenant by entering
covenant relationship with another; in his case it was a man, mak-
ing it a homosexual relationship. When he did, he died covenant-
ally in his relationship with Anne. But he still wanted the security
of marriage, even if it was a dead marriage. He didn’t want to
leave Anne, nor did he want to stop his homosexual relationship.
This situation left Anne with a “science fiction” kind of life, as she
described it in her own “night of the living dead” imagery. Only it
was not fiction; it was real. She was married to a living dead person.

According to the Apostle Paul, Anne could divorce her hus-
band, and she could remarry. She should not be classified as an
“adulteress” if she remarries. She was free from Bob’s jurisdiction,
because Bob had covenantally died. She was no longer “bound” by
her original marriage covenant to him, because that covenant had
been broken by his committing of a sin specified as covenantal
death-producing. (It is both significant and appropriate that
AIDS is now producing the physical death in homosexuals that
God long ago announced and required: Leviticus 20:13.)

T should add a warning at this point: keep in mind that when a
person dies physically, a coroner has to declare him dead. Other-
wise, you might end up with a lot of supposedly dead people being
buried alive. The point is that even though Anne feels that Doug
is dead and has reasonable proof that her husband has died cove-
nantally, she would need to prove this before a church court if she
is a Christian. She would need to ask her pastor to convene what-
ever court process is established in her church and make sure that
she secures a egal decision, a “coroner’s report” on her marriage.
Why? Because people sometimes mistakenly accuse people of do-
ing things they haven’t done just as they might mistakenly bury a
