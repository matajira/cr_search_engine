12 Second Chance

nantal adoption, in the next five chapters. Then, in the last part of
the book I apply all of these principles to the Family, Church, and
State by asking: “What do the Family, Church, and State do
about divorce and remarriage?” When you finish, I think that you
will have a clear understanding of the Biblical blueprint for
divorce and remarriage.

Not a New View on Divorce and Remarriage

I want to be quite emphatic that I am not presenting some
new, far out view. The message of this book is Biblical, as I have
already introduced it. But it also has tremendous historical sup-
port. It is not as though history is everything; it’s not; it comes sec-
ond to the Bible, Nevertheless, even though it comes second, it is
important. It proves that others among God’s people have seen it
the same way.

Although this is a Biblical presentation, and not a historical
study, I should mention that the view in this book is found in the
early Church, and it was re-popularized at the time of the Refor-
mation, In fact, the two are tied together, because the Reformers
saw themselves as going back to the Bible and the early Church.

As for the early Church, it is certain that divorce and remar-
riage were allowed for several reasons. Jerome (a.p. 345-419) de-
fended a woman named Fabiola who divorced her husband for
adultery and married another. Origen (a.p. 185-254) allowed
divorce and remarriage to avoid worse sin,'! and Leo, Bishop of
Rome (440-446), tolerated divorce and remarriage among the
priesthood. 2

10. Jerome, Epistola, LXXVII found in J.P. Minge Patrologiae Cursus Com-
pletus, Series Latina (Paris, 1844-65), XXII, 691-92. Some have tried to deny that
‘Jerome was arguing in defense of divorce and remarriage in this instance,
because he was actuaily trying to give a reason for penance. But it should also be
understood that Fabiola was not required to do penance by the church; she did it
voluntarily. And the occasion for her repentance was not until after her second hus-
band’s death.

1, Origen, Commentaria in Evangelium Secundum Metthaeum, XIV, 23 found in
J.P. Minge, Patrologiae Cursus Completus, Series Graeca (Paris, 1857-66), XIII, 1245.

32. Leo Magnus, Epistola XI, 3 (Migne, Latina, LIV, 648-59).
