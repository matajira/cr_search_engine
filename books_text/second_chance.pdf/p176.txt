154 Second Chance

point with the verse at the beginning: “In the multitude of counse-
- lors they are established” (Proverbs 15:22), which means confirmed.

3. But I pointed out that most people don’t know how to use a
counselor. They don't know what to ask him to obtain the right an-
swers. I gave five guidelines for seeking judgment from a counselor,

4. First, a couple should be judged with vintage counseling,
meaning advice given by someone who is mature and experienced
enough in their particular problems to give them wise counsel.

5. Second, a couple should be judged with structured counsel,
meaning they should be advised to join a church where they would
have an appeals court system, a hierarchy of counselors around
them.

6. Third, a couple should be judged with lawful counsel, mean-
ing the counsel should be according to the laws of the Bible and not
the laws of man.

7. Fourth, a couple should be judged with protective counsel,
meaning advice given to them should protect them from the judg-
ment of God.

8. Fifth, a couple should be judged with lasting counsel, mean-
ing they should be counseled how to have a lasting relationship.
