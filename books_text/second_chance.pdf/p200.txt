178 Second Chance

first stone,” He was really saying, “If the accusers were not in-
volved in adultery with this woman or another woman, then let
them throw the first stone.” He had to have been saying this
because they backed down. They knew that He had hard evi-
dence, or at least they thought He had enough to convict them,
and so they did not want to risk it.

Jesus was not saying that a person has to be “without sin” in
the sense of sindess. If He was, then no one would ever be able to
try to restore a fallen brother by taking him before a Church court
(Matthew 18:15-18; Acts 15:1-21), because no one is, or ever can
be, sinless: “If we say that we have no sin, we deceive ourselves,
and the truth is not in us” (i John 1:8). Rather, Christ was simply
affirming the law that you can’t bring a charge against someone
for the very thing of which you yourself are guilty. I believe that
this is the true meaning of His often misquoted statement,

Judge not, that you be not judged. For with what judgment
you judge, you will be judged; and with the same measure you
use, it will be measured back to you. And why do you look at the
speck in your brother’s eye, but do not consider the plank in your
own eye? Or how can you say to your brother, “Let me remove the
speck out of your eye,” and look, a plank is in your own eye?
Hypocrite! First remove the plank from your own eye, and then
you will see clearly to remove the speck out of your brother's eye.
Do not give what is holy to dogs; nor cast your pearls before swine,
lest they trample them under their feet, and turn and tear you to
pieces (Matthew 7:1-6).

Clearly, Jesus was not saying by this passage that we can
never judge anyone, else how would we determine whether or not
a person was a “swine” before whom we should not cast our
pearls? No, He was saying that we ought not to bring a judgment,
probably a lawsuit, against a person for the same offense of which
we ourselves are guilty.

I believe that this simple principle, if applied, would force
many persons considering a divorce to stop the proceedings. Fur-
thermore, if we take seriously Jesus’ comment, “With the same
