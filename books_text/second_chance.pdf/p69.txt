Playing with Fire Burns Out a Marriage 47

hardly ever was sick, and he never got hurt or laid off because of
any kind of injury.

As if all of this work was not enough, Tom also bought a place
out in the country, where he, his wife, and her daughter (his step-
daughter) began to clear the land to farm and to raise cattle, He
wanted to grow his own food, and he wanted to provide his own
beef. He wanted to become totally self-sufficient.

‘Tom was also very dedicated at the evangelical church where
he attended. He and his family were quite active in everything, as
you would expect. They were the kind who were there whenever
the doors opened. As on his job, he had a reputation for doing a
great deal of work at the church. He was always more than willing
to help with the lawn or any other odd job that needed tending.
When he was not on the job, or when he was not working on his
land, he was at the church doing one thing or another, He was a
real Mr. Fix-it.

One day, however, things started to go badly for Tom. It was
as though the plagues of Egypt were upon him. His crop was in-
fested with swarms of insects so that he eventually lost the whole
crop. His cattle started to die from some kind of strange disease,
and eventually they too died off. His wife became very ill, and his
small farm was repossessed by the owner, whom Tom learned was
one of the cruelest and meanest men in the county.

At about the same time, he and his family got into some kind
of trouble at the church where they had been so dedicated. There
was a dispute related to the Lord’s Supper, and they were formally
disciplined out of the church, Not only did they leave this local
church, but Tom left the Church altogether.

After that they kind of dropped out of sight. They had lost
everything, and they just mysteriously disappeared. I had heard
that they had left the area, but every now and then I would see
them around. Then one day another man told me that the wife,
Vicky, had come into his office looking for a job. As they talked,
she broke down. She told him a sad tale of how she and Tom had
divorced because she had discovered that he had been sexually
abusing her daughter since she was ten. Vicky was penniless,
