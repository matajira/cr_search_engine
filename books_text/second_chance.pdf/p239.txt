What the State Should Do 217

heritance, and in their case it was an unlawful effort on the part of
the State to combine the power of the State and the inheritance of
the family. How? If someone kept marrying within his own blood-
line, he could guarantee that the inheritance would stay in his
family. Eventually, if incest were allowed to go unchecked, a huge
family clan could be built, leading up to an empire. This is pre-
cisely what happened in the powerful Egyptian, Assyrian,
Babylonian, Greek and Roman empires. But God crushed them
all, declaring that the final kingdom on earth would be His own,
which by the way was immediately to follow the Roman Empire
(Daniel 2:24-45). God has not allowed incest to go unchecked,
allowing inheritance to be unlawfully stored up against Him.

He wants the family inheritance protected and He wants the
State to protect the family’s inheritance. But He never permits the
family inheritance to be mixed with the power of the State. So in
cases where the State is prosecuting the divorce lawsuit, it should
never be allowed to seize the family inheritance, nor should it be
allowed to give the inheritance into the hands of the wicked. It
should protect the inheritance by allowing it to pass to the right-
eous,

The State and Remarriage

Many of the remarriage issues have already been hinted at in
the divorce section. Nevertheless, the State’s involvement in
remarriage is worth considering, just as we have found the
family’s and church’s involvement in this area worth con-
templating.

1. State Can Transfer the Name

The State has a legitimate Biblical role in participating in the
transfer of a name. If it can participate in the divorce and remar-
riage process, then it has the power to allow the woman to be
adopted by her husband, expressed by taking his name. The John
the Baptist passage seems to be an obvious play on the name of
Herod, when the text says that his wife’s name was Herodias.
Perhaps Herod had given her this name, when he married her.
