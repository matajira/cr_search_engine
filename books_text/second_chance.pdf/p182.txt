160 Second Chance

Feast of Tabernacles . . . gather the people together, men and
women and little ones, and the stranger who is within your gates,
that they may hear and that they may learn to fear the Lord your
God and carefully observe all the words of this law, and that their
children, who have not known it, may hear and learn to fear the
Lord your God as long as you live in the land which you cross the
Jordan to possess” (Deuteronomy 31:10-13),

Why were these instructions given about their children? As the
Psalmist says, “Children are a heritage from the Lord” (Psalm
127:3). And these children were receiving their inheritance, just as
the parents had received them as an inheritance. So anything hav-
ing to do with children touches on the issue of inheritance, and
here is the central issue on the stepchildren question.

But to understand fully what adoption involved, we should ex-
amine an actual rite of adoption, circumcision,

When Abram was ninety-nine years old, the Lord appeared to
Abram and said to him, “. . . I will make My covenant between
Me and you, and will multiply you exceedingly. . . . No longer
shall your name be called Abram, but your name shall be Abra-
ham; for I have made you a father of many nations. . . . [And]
this is My covenant which you shall keep, between Me and you
and your descendants after you: Every male child among you shall
be circumcised; and you shail be circumcised in the flesh of your
foreskins, and it shall be a sign of the covenant between Me and.
you. . . . As for Sarai your wife, you shall not call her name Sarai,
but Sarah shall be her name” (Genesis 17:1, 5, 10-11, 15).

When Abraham ratified his covenant with God, he was given
five things. First, he was given a new name. He was told by God
that he and his wife’s names had been changed from Abram to
Abraham and from Sarai to Sarah through this ratification proc-
ess; he and his family were being adopied. The covenantal princi-
ple here is quite important for understanding salvation. It teaches
that man is not saved by race, rather he is saved by a legal act
called adoption, something Jesus had to remind the Jews of when
He said, “But as many received Him, to them He gave the right to
