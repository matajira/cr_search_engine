60 Second Chance

6. Contumacy and Malicious Perjury

Contumacy has to do with rebellion to Biblical authority,
which is tantamount to attacking God (Romans 13:1ff.).!° Mali-
cious perjury is a violation of an oath, which destroys the name of
God.

These offenses, when highhanded or prolonged, can form the
grounds for Biblical legitimate divorces. They have been estab-
lished on the solid Biblical reasoning of the cause/effect principle of
the covenant, But let’s be even more specific in our application of
this principle.

Application

Believers

First, believers who commit adultery, or any of the capital
offenses are playing for high stakes, eternal stakes. They are com-
mitting covenantal suicide. Sure, they can be resurrected, as I will
prove in the fifth chapter, but will they? Tom didn’t. Why? Sin
hardens the heart, and although there is always a way back, some
who turn away from the Lord cannot because they will not. Paul
says,

Beware, brethren, lest there be in any of you an evil heart of
unbelief in departing from the living God; but exhort one another
daily, while it is called “Today,” lest any of you be hardened
through the deceitfulness of sin (Hebrews 3:12-13).

Second, the innocent party is free to divorce and remarry
when his spouse commits one of the capital offenses, since the
guilty party dies covenantally to his covenant with God, and he
simultaneously kills the marriage covenant at that moment. As we

10, Note that this would not pertain to “lawful rebellion” to authority. For a
full discussion of this distinction, see Samuel Rutherford, Lex, Rex (Harrison-
burg, Virginia: Sprinkle, [1644] 1982). See also Christianity and Civilization, 2
(1983): Theology of Christian Resistance, published by the Geneva Divinity
School, Tyler, Texas.
