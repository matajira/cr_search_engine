Playing with Fire Burns Out a Marriage 59

4. Sexual Sins

These include homosexuality, bestiality, rape, adultery, and
incest. These offenses obviously violate the “one flesh” term of the
covenant, and they have the same effect as we saw above with
adultery.®

5. Murder

This general classification includes infant sacrifice (abortion),
physical abuse, desertion (physical and sexual), and the stubborn
failure of the father to provide economically for his family. Any
destruction of the image of God is an attack on marriage. Deser-
tion is by default a murdering of the covenant. I have included
sexual desertion here because cutting one’s partner off sexually
caused “fornication” (1 Corinthians 7:2-5), and it was analogous
to excommunication (“Cutting off from the table”) in the Biblical
covenant.

I also believe that if the man deliberately fails to provide for
his family, he is in essence starving them to death. He is murder-
ing them. Evidently, the Apostle Paul thought the same, because
he prescribed starvation as a form of execution for the man who
would not work: “If anyone will not work, neither shall he eat”
(2 Thessalonians 3:10). Probably, Paul’s reference of “not letting
him eat” is to excommunication, cutting a person off from the
communion table, But this would be a form of a covenantal death
penalty.

9. Sexually transmitted diseases, or any disease for that matter, are not
Biblical grounds for divorce. It is the immoral act that contracts the disease that
kills the covenant bond, not the disease itself. Leprosy in the Old Testament
perhaps brought covenantal death, but it was symbolic of sin and death, and it
was the only disease that fell in this category. The Reformers were divided over
whether or not to grant divorce for disease, especially leprosy: Zurich and Basel
allowing divorce for leprosy and other illnesses, while the Lutherans rejected
leprosy as legitimate grounds for divorce. For certain they all were generally
agreed that separation was allowable. See Steven Ozment, When Fathers Ruled:
Family Life in Reformation Europe (Cambridge, Massachusetts: Harvard University
Press, 1983), p. 97. I do not believe that sickness of any kind is a divorceable
offense, but I think that separation, or quarantining is necessary in the case of
lethal contagious diseases.
