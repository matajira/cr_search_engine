Living Happily Ever After 99

$45,000, But after paying her half of the back debts, she was left
with a mere $39,000. And with that, she was to start a new life.

Kathi immediately moved into a small, one bedroom apart-
ment and went to work as a waitress in a Brooklyn Kosher deli. She
made about $900 a month, including tips. Jacob meanwhile, had
quit drinking, gone back to the electrical supply business, and had
remarried. His annual income returned to his pre-divorce level —
nearly $65,000 a year—and he and his young new wife purchased
a home in the Long Island suburbs.?

Grant goes on to give some rather frightening statistics, quot-
ing Lenore J. Weitzman’s book, The Divorce Revolution: The Unex-
pected Social and Economic Consequences for Women and Children in
America: “On the average, divorced women and the minor chil-
dren in their households experience a 73 percent decline in their
standard of living in the first year after divorce. Their former hus-
bands, in contrast, experience a 42 percent rise in their standard of
living.”!?

So don’t feel guilty about gaining possession of as much of the
estate as you can, when you're the innocent party who is married
to an unrepentant guilty party. The loss of a spouse is costly, espe-
cially if you’re a female. When you're in the right, go ahead and
fight for what God has given you, because that means you are
fighting for God's inheritance. Who knows, the innocent person
may end up with everything, as he shoudd under the Biblical sys-
tem. If Roger is going to find a true second chance through
divorce, assuming that Norma doesn’t repent, he must begin to
think and to act according to the first five principles, and espe-
cially, he should put into practice the principle of transfer.

Living with the Living Dead
Finally, it is possible that Norma might want to return to
Roger in an unrepentant state. And it is also possible for Roger to

9. George Grant, The Dispossessed: Homelessness in America (Ft, Worth, Texas:
Dominion Press, 1986), pp. 71-72.
10. Zid., p. 79.
