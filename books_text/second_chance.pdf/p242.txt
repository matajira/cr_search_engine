220 Second Chance

copy of the law for his own personal guidance. He was told by
Moses,

Also it shall be, when he sits on the throne of his kingdom, that
he shall write for himself a copy of this law in a book, from the one
before the priests, the Levites. And it shall be with him, and he
shall read it all the days of his life, that he may learn to fear the
Lord his God and be careful to observe all the words of this law
and these statutes, that his heart may not be lifted above his
brethren, that he may not turn aside from the commandmenis to the
right hand or to the left, and that he may prolong his days in his king-
dom, he and his children in the midst of Israel (1 Kings 17:18-20).

What an assignment for a king or civil magistrate! Copying
the Word of God would have taken months. Reading it would
have taken years. Applying it would have taken a lifetime. But the
bottom line was that this intimate encounter with the Word of
God made him humble, a servant. It kept him from “lifting his
head above the people.” It kept him from collecting horses, gold
and wives. It kept him from being a power-religionist.

Remarriage was to be on an ¢eéhical and not on a political or
power basis. It was allowed only under the conditions that I have
explained in earlier chapters, Even when it was allowed, it was a
potentially dangerous situation. Consider what happened to Solo-
mon, the wisest man in the world. He got carried away with
remarriage and became an idolater, as well as the world’s worst
power-broker, because he certainly was remarrying for political
reasons (1 Kings 1i:1-13), Any State allowing for non-discretionary
remarriage is a power state, condoning its own right to be a
power-broker. To limit it in the area of divorce and remarriage,
therefore, will limit its own power. To make the State remarry
people on the basis of ethics will limit its ability to marry on some
other basis, What do I mean?

Inter-racial marriage has been somewhat of an issue in the his-
tory of the U.S.° Nevertheless, it is not a significant area of con-

5. Lawrence M. Friedman, History of American Law (New York: Simon and
Schuster, 1973), pp. 435-36.
