What Are Biblical Blueprints? 245

of God would require the death penalty? What kind of God would
send a plague (or other physical judgment) on people, the way He
sent one on the Israelites, killing 70,000 of them, even though
they had done nothing wrong, just because David had conducted a
military census in peacetime (2 Samuel 24:10-16)? What kind of God
sends AIDS” The proper answer: “The God of the Bible, my God?

Compared to the doctrine of eternal punishment, what is some
two-bit judgment like a plague? Compared to eternal screaming
agony in the lake of fire, without hope of escape, what is the death
penalty? The liberals try to embarrass us about these earthly
“down payments” on God’s final judgment because they want to
rid the world of the idea of final judgment. So they insult the char-
acter of God, and also the character of Christians, by sneering at
the Bible’s account of who God is, what He has done in history,
and what He requires from men.

Are you tired of their sneering? I know I am.

Nothing in the Bible should be an embarrassment to any Christian, We
may not know for certain precisely how some Biblical truth or his-
toric event should be properly applied in our day, but every historic
record, law, announcement, prophecy, judgment, and warning in
the Bible is the very Word of God, and is not to be flinched at by
anyone who calls himself by Christ’s name.

We must never doubt that whatever God did in the Old Testa-
ment era, the Second Person of the Trinity also did. Ged’s counsel
and judgments are not divided. We must be careful not to regard
Jesus Christ as a sort of “unindicted co-conspirator” when we read
the Old Testament. “For whoever is ashamed of Me and My
words in this adulterous and sinful generation, of him the Son of
Man also will be ashamed when He comes in the glory of His
Father with the holy angels” (Mark 8:38).

My point here is simple. If we as Christians can accept what is
avery hard principle of the Bible, that Christ was a blood sacrifice
for our individual sins, then we shouldn’t flinch at accepting any
of the rest of God’s principles. As we joyfully accepted His salva-
tion, so we must joyfully embrace all of His principles that affect
any and every area of our lives.
