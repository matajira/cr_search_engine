What the State Should Do 213

court of the land. How? As the king, he was supposed to be a
model of Biblical behavior and living. He was supposed to assure
the people that if all the other courts broke down, his court would
be run by Biblical law. On the other hand, if he blatantly broke
the law, he inescapably sent a message to the courts below him.
He told them that they could not count on good judgment. He
preempted them by his own sinful behavior.

In Herod’s case, he had demonstrated to them how he would
tule in divorce and remarriage cases. If he would tolerate incest, then
he would tolerate anything. He was saying by the judgment in his
own case that “anything goes” in society. In essence, Herod had
reversed the bottom-up process by imposing his own unlawful
Biblical behavior on everybody else, because he had ruled immor-
ally at the highest court in the land.

How does this apply to society today? As I explained in Chap-
ter Four, the Old Covenant judicial authority to pronounce mar-
riage and divorce was given to the father, as the agent of blessing
and. cursing. In the New Covenant, this is given to the church.
But this raises an interesting question: “What about unbelievers’
marriages and divorces?” As I will show later, the State has a legit-
imate role in marriage and divorce as a witness, and because the
witness in the Bible could bring a lawsuit, the State can lawfully
handle divorces. But it should not judicially initiate the divorces
of believers, meaning that it should be the court of appeal only for
the unbeliever in matters pertaining to divorce and remarriage.?

As the court of appeal for unbelievers’ divorces, it is just as im-
portant for the State to operate by the Word of God as it is for the
Church. Why? Because it is bound by Biblical law, just as much as
the Church. It is on the “shoulders of Jesus” no less than the
Church is carried by Christ, according to the prophet Isaiah (Isa-
iah 9:6,7), to whom Gary DeMar referred above. When the State
functions according to the Word of God, it says to the unbeliever

2. Of course the State has a legitimate role to play in all civil and criminal
matters, any situation where the “sword” could be applied (Romans 13:1-4).
