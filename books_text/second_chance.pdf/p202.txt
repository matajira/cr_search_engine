180 Second Chance

As I mentioned in Chapter Four, they were not a mandatory court
of appeal, because the father was the agent of blessing and cursing
in the Old Testament; he had the power by himself to divorce,
unless he falsely accused his bride. The priests only helped the
father to keep from this false presumption, By the ordeal of
jealousy, they protected the father, and therefore, the integrity of
the family.

The New Testament system of accountability protects the in-
tegrity of the family even more so. It shifts the agent of blessing
and cursing to Christ, as well as the Body of Christ, the Church,
as I demonstrated in Chapter Four, which requires a Christian
family to go to the church to secure a divorce; the father (or head
of the house if there is no father) can no longer act on his own; he
must be checked by the church court if he is a believer, and the
civil court is he is an unbeliever.

There ig another important reason why the ecclesiastical
authorities of the New Covenant provide accountability to the
family. The act of baptism is the point where the family gives the
children and other members of the family to the church to be bap-
tized, While baptism does not remove the family members from
their household, it does place them in an additional court besides
the family’s. It puts them in a heavenly court, the highest possible
court of appeal. It appoints them to the church covenant that lasts
forever, whereas the family covenant is temporary. I said it this
way in That You May Prosper:

The Church is the agent that baptizes “into the faith.” As such,
the church is by definition a hierarchy, meaning authority. There
can be no law without some kind of institutional authority to apply
it. Holiness cannot take place in a vacuum. It can only come about
in the context of a lawfully constituted authority. To say one is
committed to Christ, therefore, and not be a member of a local
church, is a contradiction to his baptism. It is a contradiction to
the Christian faith.

Why? Everyone should be accountable. If one does not have
real accountability to the Body of Christ, then he is symbolically
and covenantally autonomous. Not only is this extremely dangerous
