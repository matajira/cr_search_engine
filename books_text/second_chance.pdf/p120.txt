98 Second Chance

children being placed under the covenantal authority of an adul-
terous pair. 've seen many cases where Christians have taken a
passive approach because they've been counseled that divorce is
never a Biblical option. I’ve found that in more cases than I care
to admit that the innocent have been literally “taken to the clean-
ers.” They are the ones who end up being victimized. They end up
forfeiting their inheritance to the wicked. And most of all, they
end up losing their covenant children to be raised by an apostate,
immoral person.

Let George Grant, expert on dealing with the problem of pov-
erty from a Christian point of view, illustrate the losses incurred
by the innocent party with a story that he tells in his brilliant
book, The Dispossessed: Homelessness in America (1986):

Up until eight years ago, Kathi Tannenbaum was a traditional
homemaker. She had dedicated herself to building a comfortable
life with her husband Jacob and her son Aaron. For twenty-two
years, she was the epitome of the committed and caring wife,
mother, and housekeeper. She had a good life,

But then one day Kathi’s whole life caved in. Aaron was killed
in a tragic automobile accident and Jacob took to drink for con-
solation, “We were both devastated, of course. But Jake just never
seemed to recover. He went deeper and deeper into his own dark
little world and just shut me out. . . . We became strangers.”

Three months after the accident, Jacob sold the family’s small
electrical supply business and two weeks after that he filed for
divorce. . . . But that wasn’t the half of it.

The judge awarded Kathi an equal property settlement, but
she was unable to demonstrate that Jacob had any other assets
than the three-flat Brooklyn brownstone that had been their home
for ten years.

“He had a fantastic lawyer and they were able to shelter the
business assets. I didn’t get a dime,” she lamented, “and since New
York has a no-fault divorce law, I wasn’t entitled to any alimony.”

Suddenly, at age 43, Kathi Tannenbaum was alone. She had
no job. No job history. No job skills. No job leads. No job refer-
ences. Nothing.

Her share from the sale of the brownstone came to just under
