182 Second Chance

to Judah’s attempt to have Tamar put to death, which would have
amounted to divorcing her because he had “pledged” himself to
her (Genesis 38:18), He had married her, even though he didn’t
know it. Yet, when he found out that he was married to her, he re-
alized that he could not put her to death, even though Biblical law
did not allow a person to marry an in-law, within degrees of con-
sanguinity (Leviticus 18:15), a violation of which received the
death penalty.

Why didn’t he have her put to death? Why did he say that she
had acted “more righteously” (Genesis 38:26)? He realized that
she had acted to protect the seed-line of the Bible, which is the line
that leads to Christ. He understood that she had utilized a special
Old Covenant law, called the levirate marriage law, which says
that a widow without an heir could marry her deceased husband’s
brother in order to maintain his inheritance in the land (Deuter-
onomy 25:5-10). Judah came to an awareness that he had not
given his third son to Tamar for fear that he would also be killed.
But he also knew that he would now be killed if he had Tamar exe-
cuted, and he did not fulfill his responsibilities to the child she car-
ried in her womb. Why? Because as it turns out, she was carrying
twins, and one of those was in the line of Christ!

So, anyone interested in pursuing a divorce should make sure
that he has Biblical grounds. Just because he feels like it, or he
“thinks” God has told him to get a divorce, as I have heard so
many say to me, does not count. What counts is what we know
God has said for certain. Besides, God is not going to tell some-
thing that is contradictory to what He as already told the people of
God in the Bible!

4. Make Sure There Is Evidence

Judah was prepared to have Tamar executed on the basis of what
his friends told him. In fact, he didn’t have any evidence, and
when Tamar revealed her evidence, it incriminated Judah. He
had unknowingly given the pledge of his ring, cords and staff to
Tamar. He never dreamed that Tamar, his daughter-in-law, was
the one with whom he had a sexual affair. He never considered
