What the Church Should Do 193

Jocal church, What is the greatest restraining force in a society? It
is the Church of Jesus Christ. Do you believe me? Listen to what
Jesus said, “You are the salt of the earth; but if the salt loses its
flavor, how shall it be seasoned? It is then good for nothing but to
be thrown out and trampled under foot by men” (Matthew 5:13).
The “you” to whom Jesus is addressing is the people of God, the
Church of Jesus Christ, and the “you” is the salt, which means the
Church functions like salt on a society. So what does salt have to
do with restraining sin and corruption?

Salt and Judgment

Salt in the Bible is a symbol of the judgment of God, because it
Pictured a cleansing process. For example, the sacrifices were rubb-
ed and offered with salt because they had to be perfectly clean to
be pure sacrifices; in a sense the salt judged the sacrifice (Leviticus
2:13). This same salt is said to be present eternally in the fiery
judgment of covenant-breakers: “For everyone will be seasoned
with fire, and every sacrifice will be seasoned with salt” (Mark
9:49).

Perhaps the greatest example of salt as a symbol of judgment
and purification is the story of Sodom and Gomorrah, where God
turned Lot's wife into a pillar of sa/t (Genesis 19:26). Sodom and
Gomorrah had become the home of Lot and his family. When
they fell into gross sin, particularly the sin of homosexuality, they
were faced with being judged by God, as He says is the last phase
of any civilization before it meets with total annihilation (Romans
1:18-32). In the midst of this corruption God delivered Lot and his
family through Abraham. But the Lord placed one condition: that
they not look back as they left the city. In other words, He wanted them
to leave what they had experienced, indicating that a look back
meant a return in their heart to the life of Sodom and Gomorrah.
When Lot’s wife did not obey, the Lord judged her by making her
into a human symbol of judgment to remind everyone that God
does not tolerate sin.

The Church is to provide this salting function in society. It is
to be a community of salt to remind the world of the judgment to
