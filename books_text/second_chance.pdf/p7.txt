TABLE OF CONTENTS

Editor’s Introduction 6.0.4.0 ..c0eccceeneeeeeee ee eees ix

Part I: BLUEPRINTS
Author's Introduction .......0.c sce ce reese ete eee eeee 3

Principles of Divorce; Covenant Lawsuit

 

 
 
  

 
   

1. Marriages Are Made in Heaven... .

2. Burying the Living Dead......... 100033

3. Playing with Fire Burns Out a Marriage . 46

4. Covenantal Execution Protects the Innocent - 64

5. Living Happily Ever After ........+-... beeen 85

Principles of Remarriage: Covenant Adoption

6. New Covenant, New Spouse ........-....-008 102

7. The Period of Covenantal Transition........... 115

8. Binding Two or Strangling One... +128

9. Fools Rush in without Counsel. . 142
10. Everyone Needs to Be Adopted ... 155

  

Gonclusion........ 0.00 ee sence es teeeeee

Part Il: RECONSTRUCTION
11. What the Family Should Do ...........
12. What the Church Should Do .......
13, What the State Should Do.
Bibliography ...... 0.06. c epee eer eens
Scripture Index ...........
Subject Index. .
What Are Biblical Blueprints? .

sane 173

   
 
 
    

vii
