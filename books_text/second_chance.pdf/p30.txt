8 Second Chance

wife-selling in Britain, as late as the 1930s. You heard me right. It
took an act of Parliament to get a divorce as late as 1857,’ and so
the way that men got around the problem was that they literally
sold their wives. No muss, no fuss, no divorce. You just auction off
your wife as you would an old car. Sad to say, the Church was
generally silent about this system.

No, the no divorce/no remarriage view hasn't been all that
successful. In fact, where it has been most faithfully applied, I am
confident that a strong case could be built to prove that promiscuity
has gone up. Furthermore, I’m pretty sure it can even be said that
the no divorce/no remarriage view has actually promoted promis-
cuity. Think about it. If a person knows that he can never be
divorced by his wife, then he can aduiterate all he wants. Why?
There are no real legal and economic consequences, because there
is no real consequence of losing the spouse.

No, the no divorce/no remarriage group is living in never-
never land, while divorce and remarriage continue ever and ever
to increase,

Anything Goes

The other extreme at the opposite end of the spectrum is the
anything goes group. It usually turns its head away from the Bible
by couching its motives in glowing terms such as, “a desire to deal
with the real needs confronting the Church.” It is quick to remind
the more conservative types that it sees a society full of divorce
and remarriage, and moreover, a congregation full of divorce and
remarriage. It unabashedly defends its anti-Biblical view with,
“The Bible was for the first century, so we'll just operate on the
principle of ‘redeeming the situation,’ because the basic measage
of the Bible after all is redemption. How can you redeem people’s
situations if you are too narrow?”

The probiem here is that all integrity is thrown out the win-

6. Samuel Pyeatt Menefee, Wives For Sale (Oxford: Basil Blackwell, 1981), pp.
258-259. .

7. M.D. A. Freeman, “Jews and the Law of Divorce in England,” The Jewish
Law Annual, ed. Bernard 8, Jackson, Vol. 4 (Leiden: E. J. Brill, 1981), p. 276.
