What the Family Should Do 189

The ring was actually a signet-ring or seal. The same word is
translated in Exodus in connection with the engraving on the
stones the names of the sons of Israel (Exodus 28:11, 21, 36; 39:6,
14, 30), These stones were to be worn on the shoulder of Aaron.
The same type of engraving was also to be used on the plate worn
on Aaron’s turban.

The cord was probably a rope that was attached to the seal so
that it could be hung around the neck, Later in the Judah and
Tamar passage, Tamar ties a piece of scarlet around the first-born
child’s hand. Perhaps this was the cord that had held the family
seal of inheritance.

And finally, the staff in this context was more than likely the
special tribal staff. Judah literally gave away his inheritance with
these symbols, which indicates how far he had fallen from the
Lord. If Tamar had not been faithful to the covenant, he would
have lost everything. By her faithfulness, he actually got the fam-
ily inheritance back in his family.

This points out the need to resolve all inheritance problems
before the marriage takes place. A couple should know what each
other’s estate involves. They should sort out all inheritance prob-
lems relating to the children. They should be sensitive to the fact
that children from a previous marriage may view a new mother as
a potential threat to the estate. At any rate, don’t be naive about
money,

Summary

I opened the chapter with the story of Judah and Tamar
because it is clearly a case of disinheritance and adoption. I allowed
this account to be a guide to related problems. Divorce is actually a
covenantal lawsuit, and I noted five areas to which a person should
be sensitive before and during his pursuit of a divorce.

First, he should examine himself to make certain that he is not
guilty of the same offense of which he is accusing his spouse. Sec-
ond, he should proceed with accountability. He should take the
matter to his church officials to seek guidance and counsel. Third,
he should make sure that there is a Biblical justification for his law-
suit. Fourth, he should make sure that he has proper evidence.
