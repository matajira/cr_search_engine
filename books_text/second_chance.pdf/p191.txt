Conclusion 169

Remarriage

When I approached remarriage, I used the same covenantal
guide, I started with the covenant, specifically the New Covenant,
to see how a new marriage covenant could be formed. I suggested
five covenantal principles of remarriage.

First, I laid out the principle of new covenant. L_said that if God
can create a new covenant between man and Himself, then there
can be such a thing as remarriage. I clarified that a new covenant
is a way of transcending the older covenant, tying this principle
into the first point of the covenant.

Second, I established the principle of the statute of limitations.
Returning to the second point of the covenant, hierarchy, I said
that the death of a spouse has a moral effect on a person that may
take a period of time, a statute of limitations, to wear off, before
remarriage should be entertained.

Third, I developed the principle of equal yoke. A person consid-
ering a second marriage should be careful whom he marries. The
Bible says that there should be an equal yoke, so I used the terms
of the marriage covenant to clarify what kind of person a divorcee
should remarry.

Fourth, the principle of judgment was examined. Before remar-
riage takes place, two people should go to Biblical counselors to
receive judgment on their decision. In other words, they should re-
ceive some competent and Biblical person’s blessing or cursing.
In this chapter, I also gave some guidelines for using counsel.

Fifth, I presented the principle of adaption. I said that the way
to deal with the stepchildren problem is to adopt children from pre-
vious marriages into the new marriage, to transfer inheritance to
them. This way, a whole new life and a whole new inheritance is
created for them, removing the level of hostility toward the second
marriage.

So, the arguments of this book stand on the idea that marriage
is a covenant, as expressly stated in the Bible (Malachi 2:14). If it is
a covenant, and I believe it is because I believe the Bible, then it
will have all of the marks of a Biblical covenant. Not only will it
