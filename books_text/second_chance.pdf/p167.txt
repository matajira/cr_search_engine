Fools Rush in Without Counsel 145

Reverend had been a lot of help throughout Lois’s illness. So,
when he got back from his trip, he went to see the pastor, and he
told him the whole story.

To his surprise, the pastor did not share John’s enthusiasm. He
was not even impressed when John told him that her name was
Dorothy and that the name means, “Gift of God,” implying that
God had given him this woman as a replacement for Lois. He just
started pointing out all of the negatives. He pointed out that John
had only known this woman for a week. Furthermore, he was quite
troubled about her story. He flat out told John that he didn’t know
anything about the woman. And then he almost insulted John
when he said, “How do you know this woman isn’t lying to you
about her past? How do you know she hasn’t been married before?”

John was really upset, but he concealed it out of respect for his
pastor. He left quietly, bothered about the whole meeting. He
walked and walked and walked. He went home and found himself
full of energetic anxiety. He could not get the pastor’s conversa-
tion out of his mind. Then he was struck with the thought that he
would do a little checking on Dorothy to prove that she was telling
the truth.

Well, after John hired a detective to do some work, he was sur-
prised. He Jearned that Dorothy had been married five times, the
last two times to fairly wealthy business executives, and her last
husband had died with some serious questions around his death.
John had been forced to find out information that may have liter-
ally saved his life. He had been driven to decide not to marry this
woman all because a good Biblical counselor had smeiled a prob-
lem and provoked him into doing the right thing: “Without coun-
sel plans go awry, but in the multitude of counselors they are es-
tablished” (Proverbs 15:22).

John had sought to improve his judgment by allowing a coun-
selor to pass judgment on his feelings and decisions. He had
spared his life a great deal of headache and heartache through this
invaluable principle. Let us understand this principle and then
consider how to apply it, so that you will know properly how to
seek this confirmation,
