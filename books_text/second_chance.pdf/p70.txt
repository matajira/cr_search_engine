48 Second Chance

without work, without a church, without friends, almost without
a daughter because she had been so scarred for life, and without
Tom; he was being indicted on criminal charges because of his re-
lationship with his stepdaughter; and Vicky had been told that the
District Attorney was confident that Tom would receive a prison
sentence.

My friend, on whom Vicky unloaded all of this information,
was kind of shocked, and so was I, but I suppose that I was less
taken aback because incest (even though this situation could not
strictly speaking be classified as incest) has become so prevalent in
the church and in society. But I suppose that I was not too sur-
prised, because for a long time I had had many unanswerable
questions about Tom’s situation: “Why the sudden blow-up in the
church? Why had the colossal trail of catastrophes befallen Tom
and his family? Why had Tom completely left the church? Why
had his family disappeared?” So when I heard what had been
going on in Tom’s house, all the strange pieces of the puzzle began
to fall into place. Although I didn't know where Tom was,
whether he was in prison or what, I could now begin to under-
stand the situation. I realized that his sudden hostility to the
church had been the tip of a huge immoral iceberg. I also knew
that Tom had lost everything, and as far as I could discern, he had
totally left the faith.

Tom is a special case because he had been part of a vibrant
evangelical church that taught the faith and that certainly taught
against illicit sexual relationships like fornication and sexual
abuse of children. Tom heard but he did not hear. He listened to
sermon after sermon, but he was still willing to risk everything for
an illicit relationship with his stepdaughter.

Consider what Tom risked and lost. He ost everything. He lost
his family, job, crops, cattle, home, farm, church, his freedom,
his vote, and most important, Ae lost his faith. In Solomon’s words
that are quoted at the beginning of this chapter, “Whoever com-
mits adultery with a woman lacks understanding, he who does so
destroys his own soul” (Proverbs 6:32).

Now I don’t want to get into a debate over whether or not Tom
