104 Second Chance

only two reasons for divorce (adultery and desertion). But they
would more than likely say that she must remain unmarried for
the rest of her life. But what about her children? Remember, she
has four hungry mouths to feed. She only has a high school educa-
tion, no marketable skill, and she has neither the money nor the
time (two of her children are pre-school) to get the training neces-
sary to make enough to feed, clothe and educate the kids.

She has gone to her local church, but it does not have enough
money to help her.

She could go to the State (welfare), but she would lose full au-
thority over her children. She would have to put them in a public
school, Anyway, she couldn’t make enough with welfare and food
stamps to pay for a Christian education.

What is she to do?

Ah, some well-intentioned soul will be quick to say,

“Look Rev. Sutton, you're approaching the problem from a
pragmatic point of view. You've raised a practical dilemma, but
we're supposed to work off of principle and not pragmatism, She
could stay married, and the odds are that she won't get AIDS if her
husband uses a condom. If you don’t believe me, just listen to the
Surgeon-General of the United States. He’s a Christian, and he
believes that a condom is a near perfect solution to prevent the
spread of AIDS, And even if she contracts AIDS, she could con-
sider herself a martyr, suffering and dying for her religious convic-
tions. If she dies, I’m sure that Christians and family will take care
of her children. If her children contract AIDS—which they proba-
bly won't, since we all know for sure that AIDS is only sexually
transmitted—they can learn how to suffer for their convictions.
Better to die than violate Biblical principle.”

T agree, we should not change our theology for pragmatic con-
siderations. But I would also say in response that when men misin-
terpret the Bible, all sorts of practical problems result. Men once
thought that the world was flat, for example, and this misinter-
pretation of Scripture inhibited the exploration of the world,
God’s creation. Hitler surrounded himself with liberal theologians
