150 Second Chance

of the Lord your God: to walk in His ways, to keep His statutes,
His commandments, His judgments, and His testimonies, as it is
written in the Law of Moses, that you may prosper in all that you
do and wherever you turn (I Kings 2:1-3).

David, the great giant-killing king, was about to die when he
called his son to offer some final counsel, much the same as Moses
had done with Israel, even using the same summary purpose of
his counsel; “That you may prosper in all that you do” (Deuteron-
omy 29:9; I Kings 2:3). And not only did David give the identical
summary purpose, he also offered the same basic counsel as
Moses who had attached to the front of “that you may prosper,”
“Keep the words of this covenant, and do them” (Deuteronomy
29:9a). So David echoes the same ethical thrust in his counsel by
telling Solomon to prove himself a man by keeping God’s law. He
did what a true Biblical counselor was supposed to do. He moved
Solomon toward God’s law.

Two people considering marriage for a second time should go
to a counselor to have him point out any of God’s laws that might
affect their situation, which presumes that he should be a Biblical
counselor who knows the Old and New Testament. They should
be leery of a counselor who knows more psychology than the
Bible, and that’s not to say that certain aspects of research done in
the field of psychology can’t help a Biblical counselor; but he
should know more about the Word of God and how it applies to
the practical problems that man faces.

They should go with the counsel of Jethro in mind, who said
to Moses, “You shall teach then the statutes and the laws, and
show them the way in which they must walk and work they must
do” (Exodus 18:20). And with these words in mind, they should
expect their counselor to do no less. He should be able to tell them
if they had Biblical grounds for divorce, and if they have Biblical
grounds for remarriage. He should know the principles of this
book, because I have tried to approach the question of divorce and
remarriage with the Old and New Testament in mind. And he
should warn them of any part of God’s law that the couple might be
violating or coming near to violating if they should marry.
