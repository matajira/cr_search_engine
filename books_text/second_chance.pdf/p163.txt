Binding Tivo or Strangling One 141

positive (blessing) and negative (cursing) side to it. On the positive
side, it is communication that consists of five levels: cliché, fact,
opinion, emotional, transparent. On the negative side, it is mutual
accountability, the willingness to assume responsibility not to let
your spouse destroy the covenant.

8, The fifth term of the marriage covenant is inheritance. Two
divorced people headed for remarriage should beware of all the
problems associated with money, because they bring possessions
from previous marriages.
