212 Second Chance

serve as the highest court of appeal in the land. He was at the top
of the appellate system described by Moses, which was explained
in the previous chapter on the Church as a bottom-up, represen-
tative and layered system. He was like Moses at the top, who rep-
resented God's true, righteous justice. He was to perform the
function that was so marvelously exemplified in Solomon’s rule:

Then two women who were harlots came to the king, and stood
before him. And one woman said, “O my lord, this woman and I
dwell in the same house; and I gave birth while she was in the
house, Then it happened, the third day after I had given birth,
that this woman also gave birth. And we were together; there was
no one with us in the house, except the two of us in the house. And
this woman’s son died in the night, because she lay on him. So she
arose in the middle of the night and took my son from my side,
while your maidservant slept, and laid him in her bosom, and laid
her dead child in my bosom. And when I rose in the morning to
nurse my son, there he was, dead. But when I had examined him
in the morning, indeed, he was not my son whom I had borne.”
Then the other woman said, “No! But the living one is my son, and
the dead one is your son.” And the first woman said, “No! But the
dead one is your son, and the living one is my son.” Thus they
spoke before the king.

And the king said, “The one says, ‘This is my son, who lives,
and your son is the dead one’; and the other says, ‘No! But your
son is the dead one, and my son is the living one.’” Then the king
said, “Bring me a sword.” So they brought a sword before the king.
And the king said, “Divide the living child in two, and give half to
one, and half to the other.” Then the woman whose son was living
spoke to the king, for she yearned with compassion for her son;
and she said, “O my lord, give her the living child, and by no
means kill him!” But the other said, “Let him be neither mine nor
yours, but divide him.” So the king answered and said, “Give the
first woman the living child, and by no means kill him; she is his
mother” (1 Kings 3:16-27).

Herod was an adulterer whose adultery broke down the whole
system of justice, which also was true of Solomon. Herod jammed
up the court system with his own bad judgment in the highest
