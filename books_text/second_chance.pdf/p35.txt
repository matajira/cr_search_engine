Author's Introduction 13

Another place to find out what the early Church thought
about divorce and remarriage is in the law codes of the Christian
Emperors of the Roman Empire, after Rome was converted. For
example, Theodosius I, the Emperor of the East (a.p. 378-395),
and Valentinian II, Emperor of the West (a.p. 372-392), granted
divorce and remarriage for such offenses as adultery, witchcraft,
wife-beating, and several other violations.‘? I believe that it is quite
clear that their reasoning was the same as the covenantal thinking
of this book. But a detailed historical study will have to be re-
served for another book.

Jumping to the Reformation, a number of the Reformers
argued that divorce and remarriage were acceptable on Biblical
and covenantal grounds. Martin Bucer [pronounced BOOTzer]
(a.p. 1491-1551), a man who influenced all of the major Reformers
and who taught at Cambridge University, used the covenant to
guide him in determining permissible grounds for divorce and
remarriage. The following statement points to the same conclu-
sions drawn in Second Chance.

‘To the first institution [of marriage in Genesis 2:18] did Christ
recall his own; when answering the Pharisees [Matthew 19;3-12],
he condemned the license of unlawful divorce. He taught therefore
by his example, that we, according to this first institution, and
what God has spoken thereof, ought to determine what kind of
covenant marriage is, how to be kept, and how fare; and lastly, for
what causes to be dissolved . . . By these things the nature of the
holy wedlock is certainly known; whereof if only one be wanting in
both or either party, and that either by obstinate malevolence [for-
nication of any kind, including such offenses as witchcraft, adul-
tery, homosexuality, and bestiality], or too deep inbred weakness
of mind [insanity], or lastly, through incurable impotence of the
body [diseases such as leprosy and other contagious incurable dis-
eases], it cannot then be said that the covenant of matrimony holds
good between such; if we mean that covenant which God instituted

13. Juris Givilis, Code, Book V, title xvii, paragraph 8 (Geneva, 1594-1595, col.
406; Scott, V, 203-5).
