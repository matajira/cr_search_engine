188 Second Chance

There are sure to be more customs, but one thing you might do
before considering remarriage is to write down and discuss all of
the family customs that will come to bear on the second marriage.

4. The Importance of the Ceremony

‘Tamar realized that she needed a pledge from Judah to validate
her marriage to him. She knew that in order to keep the inherit-
ance, she needed to stay married to one of the sons according to the
levirate marriage law. When she discovered that Judah would not
give his third son to her because he was afraid that God would kill
him, she deceived him into pledging himself to her. She knew that
she needed his pledge, or else the marriage would not be valid.

Judah did not know what he was getting himself into, but he
knew that he needed to honor the pledge, which was the same as
marriage. From all indication, however, he went ahead and gave
her to his son because the Bible text says, “He never knew her
again” (Genesis 38:26). He literally transferred the marriage obli-
gation to his son which was precisely the essence of the levirate
law: the transference of marital responsibilities to the male next of
kin. He had been brought to do what he was supposed to do,

The ceremony of a remarriage is important, because it in-
volves an official exchange of pledges. In fact, the traditional cere-
mony is worded, “I pledge my troth (covenant).” It may not seem
important to two who have already been through one before,
however, because they want to get on with the actual marriage.
But official ceremonies also force people to slow down and evalu-
ate carefully whether they really want to go through with mar-
riage. Is this good? Yes, even though it is not an infallible test of
two people’s commitment.

5. Work out the Inheritance

Tamar demanded a pledge from Judah, forcing him to ask,
“What pledge do you want?” (Genesis 38:18). She then told him
that she wanted his ring, his cords, and his staff. Why did she ask
him for these things? Because they all represented some aspect of
his inheritance.
