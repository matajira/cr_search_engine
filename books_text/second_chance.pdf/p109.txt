Living Happily Ever After 87

feel. It is an awful experience. Norma has destroyed me. I will
never recover. [ am lonely and depressed. I wake up in the night
thinking about what might have been . . . and what is. Only God
can help me now!!

“Can He?” That’s what I've heard a lot of divorced people say,
who aren’t quite so sure as Roger. Can God help a divorced per-
son? Can he (she) live happily ever after? Can Roger live happily
ever after? How about Norma? Can she live happily ever after?
And last but certainly not least, the children, Gan they live hap-
pily ever after, or are they destined to live broken lives haunted by
relentless guilt?

In a way, the present chapter comes to what this book is all
about: a second chance! The previous chapters have carefully laid
out a Biblical rationale for divorce, moving from the principles of
creation, jurisdiction, and cause/effect, to the principle of the last chap-
ter, protection, where we discussed the sanctions for breaking the
Marriage covenant,

But the question we want to consider now is “What then?”
Let’s say your spouse has committed a capital offense and he (she)
has killed your marriage covenant. Let's say that you agree, that
you want to protect yourself from further spiritual damage, and
that you realize that you can legitimately get a divorce. What
then?

Your marriage, as I see it, could go three possible directions.
One, you could divorce your spouse, provided you have legiti-
mate Biblical grounds (see the previous chapters). Two, you could
choose to live with a “dead” person. Three, your spouse could re-
pent, and he (she) and your marriage could be resurrected. In
each case, there is one final principle of divorce that must be
understood, if you are to live happily ever after, the principle of
transfer. Let’s first fix the principle securely in our minds, for this
principle is found in the Biblical and marital covenants, and then
let’s apply it to each of the three different scenarios.

1. Dr, James C. Dobson, Love Must Be Tough (Waco, Texas: Word, Inc., 1983),
pp. 1-12.
