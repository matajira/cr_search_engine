BACK TO THE BRIDGE 217

nity support amounted to just about nil.

But we didn’t let that stop us. Taking seriously Scripture’s
charity mandate, we determined to forge ahead.

“I'd been run through every wringer welfare’s ever devised,”
Elsie later said, ‘and I’d never made a lick of improvement in all the
years I’d been on the dole. In fact, | was worse off than ever before.
Cheap wine and a life of wandering from one handout to the next were
all I’d come to know. So, I was pretty skeptical, as you can well
imagine.”

Elsie wasn’t the only one who was skeptical. After all, how
could we possibly expect to succeed when the governmental system
had failed so miserably, despite its vast resources, unparalleled exper
tise, and unnumbered years of trial and error experience? But, then,
we had to try. So, we did.

Most of our work was conducted over the phone at first. We
called churches, social service agencies, libruries, government
offices, YMCAs, and anyone or anything else we could think of
calling. We combed the city to see what services were currently
available elsewhere, and, perhaps, to coordinate our efforts with
theirs. We also called store owners, landlords, doctors, dentists,
Jaycees, lawyers, bankers, journalists, and deejays in an attempt to
find jobs, solicit help, and/or spread the word. We contacted realtors,
city council members, and Sunday schools. We conducted radio
interviews, wrote newspaper articles and editorials, and made cameo
appearances on television.

It wasn’t long before jobs came pouring in: babysitting, carpen-
try, retail sales, warehouse stocking, fast food services, plumbing,
inventory control, odd jobs, and small appliance repair. Networking
with other churches and agencies was also accomplished naturally,
through word of mouth. Sheltering homes opened up. Manufactured
commodities and seconds were made available. Services were
offered, professional and personal.
