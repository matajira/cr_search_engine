POOLING RESOURCES 159

since the Reformation.

First, the community coalition enables individual churches to
share the charity burden. If one outreach is overloaded, the others can
step in and shoulder some of the load for awhile. If one outreach has
access to free dental care and another to cheap housing and still
another to large stores of food commodities, then sharing back and
forth only makes sense. Obviously, the members of the coalition will
need to be in close communication with one another so that new
developments, surpluses, or short-falls can be closely monitored.
The essential element here is selfless sharing. If each of the task
forces and each of the churches involved are genuinely concerned for
the poor, and seriously committed to the ethical mandate to exercise
compassion, then petty rivalries and denominational differences will
be happily dispatched. The coalition will work harmoniously, sharing
responsibility, splitting expenses, and pooling resources. Different
ventures will require different levels of commitment and coordina-
tion, but a community coalition that is organized along loose, infor-
mal lines, and nothing more, is certainly good enough to start with. A
more complex organization, with committees, officers, budgets,
etc., may be necessary later, but in the beginning, small is beautiful;
informal is best.

Second, the community coalition makes possible joint projects
that would be beyond the realm of feasibility for any of the churches
acting alone. For instance, the coalition could rent warehouse space
and begin a food bank. Canned goods, staples, and other non-
perishables could be collected from all over the community, and then
stored at the one central location. Distribution would be simpler,
faster, and more efficient. Gaps could be closed, care made more
uniform, and costs reduced.

Another joint project, made possible by the coordinated efforts
of a coalition, would be an emergency shelter. Instead of each church
or task force stretching itself to the limits, staffing, funding, and
supervising separate sheltering ministries, why not channel all that
