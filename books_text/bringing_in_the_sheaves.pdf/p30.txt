30 BRINGING IN THE SHEAVES

there is enough rice to fill them up.”

Another woman told him that her children are used to going
without food a few days each month. ‘“They know we have to wait
until we get more food . . . there are some things you have to do even
if it means eating a little less.”

Mindy Lester lives with her two children in a small back room in
a Philadelphia boarding house. She, too, has known hard times. She,
too, has had to make some hard choices. “The price of oil is so high
that it is a choice between a little heat, not even enough to keep us
warm, and food. Not good food, but just enough to keep us going. It’s
cold. It’s very cold. Some nights, it’s so cold I don’t know if we can
last til! morning. But one thing I do know, we can’t stay alive without
food. So, usually we choose food. I tell the kids we have to try it this
way and not complain and blame each other and make it worse. We do
the best we can . . . we pray.”

Standing in line at a Catholic relief center in Baltimore, Will
Salinski marvels at the choices he’s had to make in recent weeks.
“Used to be that most of the folks standing in this line were
bums . . transients. But now . . . look! Senior citizens and whole
families. And now, me. Who’d have ever guessed it? But it’s either
this or starve. You gotta make hard choices in hard times.”

The poor are not the only ones having to make hard choices. The
relief center in Baltimore at St. Ignatius Catholic Church opens each
morning at six. But lately, people have been arriving even earlier.
They wait outside, rain or shine, heat or cold, because they have no
other place to go and they know they won't be turned away. But the
center’s policy of helping all people who seek it is becoming harder to
maintain. Since 1981, requests have increased over 300%,

And St. Ignatius Church is not alone. Similar stories continually
flow in from relief centers, rescue missions, and social service
centers across the country. Hunger center managers in Cleveland say
they are serving 75% more families this year than last. One Episcopal
church in Washington, D.C., closed its feeding program because it
