THE WAR ON THE Poor 51

The Government’s Role

What then, can the government do to reverse its dismal record in the
war on poverty? What can the government do to read/y help the poor?
According to economist Murray Rothbard, the only correct answer is,

Get out of the way! Let the government get out of the way
of the productive energies of all groups in the population
—rich, middle-class, and poor alike — and the result will
be an enormous increase in the welfare and the standard of
living of everyone, and most particularly of the poor who
are the ones supposedly helped by the miscalled welfare
state.46

In his book, Welfare Without the Welfare State, Yale Brozen points
out,

With Jess attempt to use state power to compress the
inequality in the distribution of income, inequality would
diminish more rapidly. Low wage rates would rise more
rapidly with a higher rate of saving and capital formation,
and inequality would diminish with the rise in income of
wage-carners.*7

If the government were to reduce the level of taxation, remove
industrial restraints, climinate wage controls, and abolish subsidies,
tariffs, and other constraints on free enterprise, the poor would be
helped in a way that AFDC, Social Security, and Unemployment
could never match. Jobs would be created, investment would be
stimulated, productivity would soar, and technology would advance.
If that were to happen, says Rothbard, “the lower income groups
would benefit more than anyone else.” 4*

The war on the poor can be turned around. It can be as it was
intended to be from the start: a war on poverty. But only if the
