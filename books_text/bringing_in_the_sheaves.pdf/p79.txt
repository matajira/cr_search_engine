BIBLICAL CHARITY 79

than a prod to full restoration of the poor to their God-ordained
calling. Paul makes it plain: “If a man will not work, he shall! not eat”
(2 Thessalonians 3:10).

A handout does not charity make!

Every effort must be made to ensure that our helping really does
help. A handout may meet an immediate need, but how does it
contribute to the ultimate goal of setting the recipient aright? How
does it prépare him for the job market? How does it equip him for the
future? How well does it communicate the Law of God and the
precepts of Biblical morality? The kind of evangelical myopia that
envisions the Scriptural duty to the poor as a simple transfer of funds
simply misses the boat. When the church mimics the government by
promiscuously dispensing groceries and other goods and services, it
hurts the poor more than it helps. Adherents of such short-sighted
thinking only perpetuate the war against the poor.

The Good Samaritan faith goes to work putting the able poor to
work. That’s Biblical charity.

Sheaves for the Provident
But how? How are the able poor to be put to work?

As David Chilton has shown, in Scripture, “the primary source
of regular charity to the poor is the practice of gleaning.”'®

Perhaps the best illustration of how gleaning works is the story of
Ruth. It is a story of compelling beauty and romance, of faithfulness
and intrigue, of tragedy and hope. Set during the time of the judges, it
provides for us an intimate glimpse of covenant life in ancient Israel.

The main characters in the story, Ruth and Naomi, are widows
living on the edge of destitution (Ruth 1:6-13). Determined to take
responsibility for her elderly mother-in-law (Ruth 1:14), and to accept
the terms of God’s covenant for herself (Ruth 1:16-17), Ruth does the
only thing she could do. She goes out to find work (Ruth 2:2). In
many ways, though, this was a good news-bad news situation for her.
‘The bad news was that Ruth was a stranger to the ways and customs of
