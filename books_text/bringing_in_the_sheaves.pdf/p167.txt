CHAPTER 10

The Bootstrap Ethic

 

he Charity Organisation Society was England’s leading private
T chacty agency in the late nineteenth century. It operated on the
Biblical principle of aid to foster self-help. According to Charles
Loch Mowat, the historian of the Society, it

embodied an idea of charity which claimed to reconcile
the divisions in society, to remove poverty, and to produce
a happy, self-reliant community. It believed that the most
serious aspect of poverty was the degradation of the char-
acter of the poor man or woman. Indiscriminate charity
only made things worse; it demoralised. True charity
demanded friendship, thought, the sort of help that would
restore a man’s self-respect and his ability to support
himself and his family. True charity demanded gainful
employ. !

167
