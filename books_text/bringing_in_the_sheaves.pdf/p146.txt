146 BRINGING IN THE SHEAVES

mistakes are minimized. Demographics are, thus, of prime impor-
tance.

It is clear that Christians in our day need to pioneer Biblical
charity outreaches. We are to jump into the struggle for genuine
justice and mercy with both feet. But we are to look before we leap.

Before you commit to a plan of action, why not do a bit of
preliminary footwork? Why not gather some information?

First, contact all the churches in your community. Virtually
every church in America has some kind of benevolence program. But
even if they don’t, they'll at least be able to tell you what kind of
people come around looking for help and what kind of help they’re
looking for. So, ask lots of questions and take notes. Do they have a
poverty outreach? Have they ever thought about starting one? Why or
why not? How much money do they spend annually on benevolence?
What kind of records do they keep, if any? Do they participate in
seasonal charity, dispensing Christmas presents or holiday baskets?
What kinds of successes can they share? What about failures? Do they
refer to other agencies? Which ones?

A treasure trove of information can be mined from just a few
calls. Not only are you better able to gauge the climate of poverty in
your area and to assess what services are already available, but you’ve
made a good contact for future cooperative efforts as well.

Second, contact all the social service agencies, both private and
public. You may find yourself being transferred from phone to phone,
but the information you can glean is invaluable and, thus, worth the
hassle. See if they can send you literature detailing their activities and
services. Even general information brochures can tell you a great
deal. Ask them to send you everything they’ve got, from year-end
reports to PR pieces, from financial schematics to appeal letters.
Then, sort through all the material to find out what you need to know.
Again, ask lots of questions. Take notes. And be sure to obtain their
referral list. That way you’ve not only acquired information about a
single agency, you’ve discovered how that agency networks with
