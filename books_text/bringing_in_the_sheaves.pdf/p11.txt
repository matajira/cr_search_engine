Foreword

n the 1970s there grew to great intensity a debate within the
Tovanzetica world conceming the relative importance of evange-
lism and social action, the latter of which was understood principally
as the helping of the poor. For most of those in the fray on both sides,
evangelism meant the preaching of the gospel to bring people to a
saving faith in Christ. The social action side carried a meaning that
was somewhat more vague; for some it meant personal charitable
activity, for others it meant primarily supporting humanitarian
activity by the state.

This debate was evidence of a terrible weakness in the church, in
both its theology and its practice. Evangelicals were united in their
insistence on the Bible as the rule of faith and practice, and yet were
unable to realize that the debate was being conducted on grounds that
were foreign to biblical thinking. The law, the prophets, the gospels
and the epistles are devoid of any idea that there is a contradiction
between the communication of God’s grace on the one hand and the
