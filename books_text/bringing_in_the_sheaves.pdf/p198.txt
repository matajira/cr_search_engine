198 BRINGING IN THE SHEAVES

disasters. As Oscar Wilde said, “Experience is the name we give to
our mistakes,”* and there is a certain health in leaving them judi-
ciously scattered around the landscape as continued lessons.

Perhaps with Pruitt-Igoe thus left as a spur in our memories, we
can turn from the federal government to the Christian community for
viable solutions to the sheltering crisis. Perhaps with lessons of the
project’s disastrous demise still fresh in our minds, we can begin to
see Biblical charity as the only practical answer to the plight of the
temporarily displaced.

The Biblical Solution
Our obligation to the homeless and displaced poor is indisputable, We
are “to love those who are aliens” (Deuteronomy 10:19). According
to the Old Testament definition, the alien was anyone passing through
another’s land. The stranger, or sojourner, was a resident alien living
in the foreign land, as opposed to traveling through. Both found
themselves at a real disadvantage. Being homeless, they were often
unable to provide for themselves in a landed agricultural society.
They were in danger of being abused, manipulated, and exploited by
the natives of a region who knew the language, customs, and civil
structure better than they did. For all intents and purposes, the
displaced alien was weak and powerless, dependent on others for
refuge and provision.

God’s people are especially qualified to provide refuge and to
express love to the alien, because they have historically found them-

selves in similar circumstances of vulnerability: “ . . . for you your-
selves were aliens’’ (Deuteronomy 10:19). Even in the New
Testament, the Apostle Paul reminds us that we are“ . . . aliens and

strangers in the world” (Hebrews 11:13).

it was in this context then that God gave his people, aliens
themselves, specific commands regarding everyday treatment of the
displaced (Exodus 22:21, 23:9; Leviticus 19:9-10). Since God Himself
loves the alien, giving him refuge (Deuteronomy 10:18), God’s people
