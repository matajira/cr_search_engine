FOR FURTHER READING AND STUDY 233

William B. Eerdmans, 1966). In no less than 53 chapters, Kuiper
outlines the essential function of the church according to Scripture,
and how we must reform if we are to fulfill that function in our own
day.

MacGregor, Geddes. Corpus Christi: The Nature of the Church
According to the Reformed Tradition (Philadelphia, PA: Westmin-
ster Press, 1958). This book is unparalleled in its scope and vision
for the church.

North, Gary. Backward, Christian Seldiers? (Tyler, TX: Institute for
Christian Economics, 1984). North outlines a plan of ministry
action that must be considered as we approach the end of this
tumultuous century.

Pride, Mary. The Way Home (Westchester, IL: Crossway Books,
1985). In this powerful handbook to the Christian home, Pride
outlines an agenda for family ministry that simply cannot, must
not, be ignored.

Rupprecht, David and Ruth. Radical Hospitality (Phillipsburg, NJ:
Presbyterian and Reformed Pub. Co., 1983). This all but indis-
pensable book lays the Biblical foundations for the “Good
Samaritan faith” in home and church.

Schaeffer, Francis A., ed. by Richard B. Sherman. Reclaiming the
World (Los Gatos, CA: Schaeffer V Productions, 1982). In this
comprehensive leaders’ guide to the film of the same title, Dr.
Schaeffer discusses the “Great Evangelical Disaster” and gocs on
to propose a series of solutions involving the ministry of the
church.

Tillappaugh, Frank. The Church Unleashed (Ventura, CA: Regal
Books, 1982). This unique book of church renewal has its flaws,
but the thesis is irrefutable: the church must get Gad’s people out
where the needs are.

Developing A Theology Of Victory
Alexander, J. A. The Prophesies of Isaiah (Grand Rapids, MI: Zon-
