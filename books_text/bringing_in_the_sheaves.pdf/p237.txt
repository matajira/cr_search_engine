Notes 237

CHAPTER 2 / The War on the Poor

1 Milton and Rose Friedman, Free te Chouse (New York, NY: Harcourt, Brace,
Jovanovich, 1979), p. 87.

2 Tbid.

4 Ibid.

4 Toid

> Ibid.

6 Ibid.

7 I Peter Grace, Burning Money (New York, NY: Macmillan, 1984), p. 108.

® Friedman, p. 87.

° Charles Murray, Losing Ground (New York, NY: Basic Books, 1984), p. 48.

‘© Thid.

" Ibid.

'2 Thid.

13 Thid.

14 Ibid... p. 16.

‘5 The Poor Law Commission for the Poor Law Reform of 1934, quoted by Mark
Blaug, “The Myth of the Old Poor Law and the Making of the New,” Journal of
Economic History, 23 (June, 1963), p. 152.

16 William Wilkins, The Unknown FDR: Zen Politics at the Outset (New York,
NY: York Press, 1967), p. 85.

1? Albert Donaldson, American History in Review (Tulsa, OK: Christian Truth
Pubtishers, 1971), p. 426.

18 Murray, p. 22.

19 Btaug, p. 153.

20 Toid.

21 Thid., p. 154.

22 Thomas Algier, The American Legacy (Chicago, IL: Heritage Publishing
Co,, 1959), p. 13.

24 Thomas MacKay, Methods of Social Reform (London, UK: John Murray,
1296), p. 3.

24 Hardy L. Kalwaski, Poor Richard and the American Ethic (Boston, MA:
Commons Press, 1874), p. 317.

25 New York Times, April 20, 1967. Quoted in Donaldson.

26 Murray, p. 8.

27 Ibid, by extrapotation from Murray’s figures.

28 Grace, p. 108.

29 Newsweek, December 27, 1982.

30 Charles Murray, Budget Priorities and Trends in the Federal Effort to Combat
Juvenile Delinquency (Washington,D.C.: Government Printing Office, 1976).

31 George Gilder, Visible Man: A True Story of Post-Ravist America (New York,
NY: Basic Books. 1978).

32 Warren T. Brookes, The Economy In Mind (New York, NY: Universe Books,
1982).

33 Thomas Sowell, The Economics and Politics of Race (New York, NY:
William Morrow and Co., 1983).

34 Murray Rothbard, For @ New Liberty (New York, NY: Macmillan, 1973).

35 George Gilder, Wealth and Poverry (New York, NY: Basic Books, 1981).

36 Grace, p. 105.
