CHAPTER tI

 

Loaves and Fishes

uring one episode in John Bunyan’s classic, Pilgrim's Progress,

Christian seems to be hopelessly mired in “the slough of Des-
pond.” No amount of effort seems sufficient to redeem him from
such a perilous plight, so Christian resigns himself to a sad, sedentary
demise. Suddenly though, out of nowhere, a fellow pilgrim named.
Help comes to the rescue. With a single reach of the hand and a hearty
tug, Help pulls Christian out of the slough and onto safe ground once
again. In short order, both pilgrims are off and on their way, their
destination now one trial closer than before.

If for no other reason than for clarity’s sake, it is a good thing
Bunyan set this scene in the early seventeenth century. Had he written
it in contemporary America, the scenario would have to have been
substantially more complex. Help would not have been able to just
walk right up to the edge of the slough and yank Christian out. Oh my,
no!

Instead, Help would probably have been required to submit an

183
