BIBLICAL CHARITY 67

Nathaniel Samuelson, a Puritan divine of some renown, was
another great spokesman for Christ who devoted his life and ministry
to the poor. He established a network of clinics, hospitals, and rescue
missions that in later years served as the primary inspiration for
William Booth in founding the Salvation Army. In a sermon that he
reportedly preached over three hundred times throughout England, he.
said,

Sodom was crushed in divine judgement. And why, asks
me? Was it due to abomination heaped upon abomination
such as those perpetuated against the guests of Lot? Nay,
saith Scripture. Was it due to wickedness in commerce,
graft in governance, and sloth in manufacture? Nay, saith
Scripture. In Ezekiel 16:49, thus saith Scripture: “Behold
this, the sin-guilt of thine sister Sodom: she and her
daughters wrought arrogance, fatness, and ill-concern,
but neglected the help of the poor and need-stricken.
Thus, they were haughty, committing blasphemy before
me. Therefore, I removed them in judgement as all see.”
Be ye warned by Sodom’s ensample. She was crushed in
divine judgement simply and solely due to her selfish
neglect of the deprived and depressed.>

Wherever committed Christians have gone, throughout Europe,
into the darkest depths of Africa, to the outer reaches of China, along
the edges of the American frontier, and beyond to the Australian
outback, the Good Samaritan faith has been in evidence. In fact, most
of the church’s greatest heroes are those who willingly gave the best of
their lives to the less fortunate: Tertullian, Bernard of Clairveaux,
John Cennick, Howell Harris, David Brainerd, Dwight L. Moody,
Florence Nightingale, Oswald Chambers, Amy Carmichael, Bob
Pierce, and Mother Theresa. Each made the message of their lips
manifest by the message of their hands. “And so the Word of God
