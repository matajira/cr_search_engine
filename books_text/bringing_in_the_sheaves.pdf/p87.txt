BIBLICAL CHARITY 87

Lord your God is giving you to possess as your inheritance,
He will richly bless you, if only you fully obey the Lord
your God and are careful to follow all these commands I
am giving you today. For the Lord your God will bless you
as He has promised and you will lend to many nations but
will borrow from none. You will rule over many nations
but none will rule over you (Deuteronomy 15:4-6).

Summary

The Good Samaritan faith has had its exemplars since the first
century, in the likes of George Whitefield, Charles Haddon
Spurgeon, Nathaniel Samuelson, and a host of others. Their
orphanages, hospitals, almshouses, rescue missions, schools, and
soup kitchens ail spoke eloquently to the world of the exacting
compassion of Christ, and bore stark testimony to the obedience of
His servants. These holy men and women clothed their faith with
works of compassion that not only changed their world, but continue
to affect lives and cultures to this day.

They understood that Christ’s Great Commission commands the
church to much more than mere soul-saving: we are to disciple the
nations by teaching them to obey everything that He has taught us.
This command is but an extension of the Dominion Mandate of
Genesis 1:28. Our call is to win aif things for the Lord Jesus.
Whitefield, Spurgeon, Samuelson, and the rest, lived their lives to
that end. Unfortunately, the modem tendency to ignore ali our Scrip-
tural responsibilities except soul-saving has resulted in the gradual
takeover of Western culture by an insipid humanism.

Since the Bible is a blueprint for living, providing models,
structures, and systems for the Godly ordering of every aspect of life,
we can fully expect to find in it answers to even the most monolithic of
problems, like an encroaching humanism and an endemic poverty.
And, we can fully expect that when we adhere to those models,
