148 BRINGING IN THE SHEAVES

Straight from the Horse’s Mouth

But even after you've conducted a thorough demographic survey of
your community, you still haven’t successfully discovered and identi-
fied the needs of the poor until you actually talk to the poor. Find out
what they think. Find out how they feel.

Robert Thompson is the pastor of First Baptist Church, Evan-
ston, Illinois. Several years ago, he began an outreach to the unem-
ployed. But instead of structuring a program based on what he
thought the poor needed, he gathered them together, on a weekly
basis, to let them explore, share, plan, and formulate strategies of
compassion. “In spite of the fact that each person’s experience of
unemployment is unique,” he said, “all unemployed people do share
some common feelings . . . a sense of alienation, a loss of self-
esteem, and a feeling of powerlessness . . . These need to be identi-
fied and understood if one hopes to make the most of a troublesome
situation.”

Thompson’s group meetings not only enabled him to put
together informed and effective tactics of Biblical charity, they also
provided support, encouragement, and evangelistic opportunity for
the poor themselves. In fact, the group meetings continued long after
Thompson’s fact-finding mission had been completed, just for that
reason.

Max Harfield is the director of the Calvary Mission Center in
New York City. For eight-and-a-half months in 1972, he lived on the
streets with the homeless. Sleeping in phone booths, subway stations,
and public shelters, eating in soup kitchens, abandoned warchouses,
and city parks, and wandering almost aimlessly throughout lower
Manhattan, he came to know the poor in a way most of us never will.
He probed their deepest fears, their greatest ambitions, and their
clearest needs. Then he established the center. Starting with the
Scriptural strategies for charity, and taking into accountall that he had
learned from the poor themselves from his life on the street, he began
to build a dynamic model of Biblical charity that changes lives for
