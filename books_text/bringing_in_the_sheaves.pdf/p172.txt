172 BRINGING IN THE SHEAVES

each posting remains current. Also, for administrative ease, each
notice needs to be standardized. To accomplish both of thase objec-
tives easily, simply mock-up a blank form and photocopy several
dozen copies. The form should have spaces for the job description,
pay scale, skills required, personnel contact, job site, address, phone
number, and date. Make certain that each job notice has all the
information filled in before posting. That will not only eliminate
clerical hassle, but will also save a great deal of time and effort on the
part of the applicants.

Keep the job board in an open area that has free and easy access,
and then utilize it continually in your counseling and helping minis-
tries.

If the job board remains a bit sparse, peruse the want-ads in the
local paper and check in with the employment commission, the
Chamber of Commerce, or the Junior Chamber of Commerce. It’s
amazing what just a little initiative can do.

In the Houston suburb of Humble, a job board was begun by the
Humbic Evangelicals to Limit Poverty (HELP) during the worst part
of 1982’s economic constriction. Not only was there never a time
when the board was lacking for jobs, but as word spread in the
community, extra space had to be added twice. The local hospital, the
city’s maintenance department, and several large businesses all com-
mitted to notify HELP immediately any time job openings occurred.
A local Christian radio station set up a temporary labor pool so that
homeowners in need of minor repairs or maintenance could contact
the HELP office, thus facilitating market growth and entrepreneurial
activity. In less than five months, at a time when all leading economic
indicators pointed to unrelenting “stagflation” and decline, HELP
was able to place 432 poor applicants in permanent jobs, and another
367 in temporary situations. Now, those figures pale in significance
when compared to the massive unemployment rates of the same
period. But, on the other hand, if every Bible-believing church in
America had a charity outreach and each of those charity outreaches
