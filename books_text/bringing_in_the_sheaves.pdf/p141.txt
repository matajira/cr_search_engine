CHAPTER 8

 

Discovering and Identifying Needs

reg Davies could barely believe his own eyes. “Honey, have you
Geen the front page of the paper yet?”

“Are you kidding?” replied his wife, Evelyn. ‘With three kids
to clothe, feed, and get off to school?”

“Look. It’s Freddie Johnson.”

“So who, pray tell, is Freddie Johnson?”

“Don’t you remember? Freddie was one of the fellas who
worked the line on the sonic project. Good worker. Real industrious.
One of the best | ever had. Well, when the government canned the
sonic...”

“Boeing canned him.”

“Right. And now look at him. Here in the paper. On the front
page, no less. Standing in linc for free government cheese and
butter.”

Alternate waves of guilt and pity washed over Greg as he looked
up from the paper. The breakfast room’s bay window afforded him an

141
