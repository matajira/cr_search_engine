EQUIPPING FAMILIES 119

single outsider, the federal dole could be eliminated completely.® Just
like that. Families simply fulfilling their Christian responsibility to
their own (1 Timothy 5:8), to their brethren in Christ (Galatians 6:10),
and to the stranger and alien (Exodus 23:9), can so effectively do the
work of charity that no back-up system, no federal bureaucracy, no
matching funds, and no professional humanitarians are necessary.
Families can do the job.

When churches mobilize and motivate, equip and enlist families
to practice the Good Samaritan faith, they have not only gotten God's
people out where the needs are and seen those needs met in accord
with the Great Commission mandate, they have also set into motion
the power of body life. They have catalyzed the practice of the
priesthood of believers.

The Priesthood of Believers

Ever since the Reformation, virtually all Christians have held, at least
in theory, to the practice of the priesthood of believers. When Martin
Luther, John Calvin, Ulrich Zwingli, and other sixteenth-century
reformers challenged the authority of Rome, it was primarily over this
issue. They asserted that the people of God were not merely recipients
or spectators, but were also vital participants in the affairs of the
church. Thus, the heretofore inactive and immobilized believers were
unleashed to do the work of the ministry (Ephesians 4:12) and to
exercise their gifts (Romans 12:6).

Wherever the Reformation message reached, the priesthood of
believers was emphasized. New Christians were taught early on that
they were “being built into a spiritual house to be a holy priesthood,
offering spiritual sacrifices acceptable to God through Jesus Christ”
(1 Peter 2:5). They learned that they were ‘“‘a chosen people, a royal
priesthood, a holy nation, a people belonging to God,” called to
“declare the praises of Him who called them out of darkness into His
wonderful light” (1 Peter 2:9).

Church history affirms that whenever this Reformation truth has
