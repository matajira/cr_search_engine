PART IV:

 

THE TACTICS

I humbly beg and implore . . that you will not decline to read,
and diligently ponder, what I have to lay before you. The
magnitude and weightiness of the cause may well excite in you an
eagerness to hear, and I will set the matter so plainly in your view
that you can have no difficulty in determining what course to
adopt. Whoever I am, I here profess to plead in defense both of
sound doctrine and of the church .. There are two circumstances
by which men are wont to recommend, or at least to justify their
conduct. If a thing is done honestly and from pious zeal, we deem
it worthy of praise; if it is done under the pressure of public
necessity, we at least deem it not unworthy of excuse. Since both of
these apply here, I am confident, from your equity, that I shall
easily obtain your approval of my design.

John Calvin (1544)

 
