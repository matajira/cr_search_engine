134 BRINGING IN THE SHEAVES

governor (Nehemiah 5:14). He broke with tradition by refusing to
accumulate for himself land (Nehemiah 5:16) or ill-gotten gain
(Nehemiah 5:15). He did not even request reimbursement for his
living expenses (Nehemiah 5:18). Instead, he and his household
devoted themselves to the labor at hand. Side-by-side with the others,
they rebuilt the wall. Nehemiah was a servant. He sacrificed for the
sake of the Kingdom, and this example of selflessness encouraged
and motivated all the rest of the people to sacrifice as well. The
greatness of Nehemiah’s achievement lies in this: he was able to lead
the people in a staggeringly difficult endeavor without guilt-manip-
ulation, without despotism, and without bureaucratic finagling. He
did it with righteous sacrifice. In order to motivate families and
mobilize private initiative to hammer out practical models of Biblical
charity, we need servant leaders like Nehemiah. We need men and
women who can inspire sacrifice.

Vishal Mangalwadi and Sabutu Mariam are such men.

In the mid-’70s, Vishal Mangalwadi and his wife, Ruth,
founded the Association for Comprehensive Rural Assistance near
Chhatarpus in their native India. Besides operating farms, schools,
textile mills, and evangelistic camps, the Association assists lower-
caste Hindus to rise above the bitter lot of destitution through indus-
try, cooperation, and creativity.

After graduation from Wheaton College, the Mangalwadis were
led by God to begin erecting models of Biblical charity sufficient to
supplant the structures of injustice, corruption, sloth, and despair that
were so much a part of India’s economic ecology. The Gospel imper-
ative to care for the afflicted compelled them to look for solutions
deeper and more abiding than mere relief. They sought out long-term
answers.

Since Scripture teaches that right thinking precedes right action,
they began a comprehensive program of Christian education. Since
Scripture teaches that gleaning is the primary model of Biblical
charity, they began to organize work crews, separating the deserving
