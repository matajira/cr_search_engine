Like a mighty army, moves the Church of God,
Brothers, we are treading where the saints have trod.
We are not divided, all one body we,

One in hepe and doctrine, one in charity.
Onward Christian soldiers, marching as to war,
With the cross of Jesus going on before.
Sabine Baring-Gould

 

Blest be the tie that binds
Our hearts in Christian love!
The charity of kindred souls
Is like to that above.
John Fawcett

 

Forward through the ages, in unbroken lines
Move the faithful spirits, at the call divine;
Gifts of differing measure, hearts of one accord,
Manifold the service, one the sure reward.

Not alone we conquer, not alone we fail;
in each loss or triumph, lose or triumph all.
Bound by God's far purpose, in one living whole,
Move we on together, to the shining goal.
Frederick Hosmer

 
