12 ForEworD

doing of good works on the other. Indeed, it was in the midst of his
missionary journeys that Paul organized the collection for the Chris-
tians in Jerusalem who were living in privation. That ministry was the
prime example of the unity between believing rightly and doing good.

Throughout the New Testament fove is described as the identify-
ing mark of the Christian community to its pagan neighbors, its
authenticating feature, that which proves that God’s life is in its midst.
James's statement that faith without works is dead is of one piece with
the entire biblical witness that the separation of the inner life from the.
exterior one makes no sense. Similarly, works without faith is of no
religious significance except as a continuing testimony of the futility
of trying to save ourselves. The task remains for cach generation of
Christians to ascertain how it can live an integrated life, fully
exemplitying the inner and outer dimensions in the wholeness that
only biblical faith makes possible.

Once this is agreed upon, we're ready to address the thornicst
issue of those debates: whether our responsibilities to the poor are to
be discharged primarily by personal charitable action or through
supporting the humanitarian policies of the state. There may have
been some excuse to debate that issue ten years ago, but there is none
today. Now that the War on Poverty is entering its third decade, its
record of abysmal failure is becoming increasingly clear. The sub-
stitution by officers of the state of humanitarian “good works” for
Christian charity has been a disaster almost without precedent.

We now have presented for us in bone-chilling detail by such
writers as P. ‘I. Bauer, George Gilder, and Charles Murray how poor
people, in our own country and abroad, have been transformed by
humanitarian policy into helpless wards of the state, completely
dchumanized by the programs that were supposed to be motivated by
compassion. The bitterest denunciations of the state welfare system
come from the pens of black economists Thomas Sowell and Walter
Williams, fod up with seeing their people destroyed by the policies of
“compassion.”
