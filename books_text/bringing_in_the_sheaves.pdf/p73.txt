BIBLICAL CHARITY = 73

am wel] aware will content themselves with scolding: they
will not venture a rational reply . . . The only office that
remains .. . is to leave testimony for . . . righteous
fame, feeble it may be now, amidst the din of passion and
material, yet inextinguishable as Truth’s own torch. His-
tory will some day bring present events before her impar-
tial bar.>

For the record, the federal government’s war on poverty is 4
dismal failure. It has become a war on the poor. By asserting its
universal responsibility to care for the poor, by centralizing the
criteria of poverty, by bureaucratically administering relief, by
reducing the importance of local conditions and accountability,
and by institutionalizing the apparatus for care, the State has,
according to Dr. Gary North, created ‘‘a permanent welfare class
which owes its survival (it thinks) to the continued generosity of
the State.’’6 The war on poverty will never be met with anything
except devastation and defeat simply because it does not (and
cannot) help people get on their feet. It is but a salve to succor
momentarily mortal wounds. It is but a drop in the bucket. But it is
the program which “now rules the hour.” And the liberal church,
in its zeal to procure mercy for the broken and justice for the
downtrodden, goes awhoring after more statist intervention and,
thus, is busily and triumphantly engaging in “perfecting a plain
injustice.”

Clearly, the only hope for the poor lies in the Scriptural
alternative, in the blueprint from which the Good Samaritan faith
is constructed. The only hope for the poor lies in the church
returning to the Great Commission compulsion that drove White-
field, Spurgeon, Samuelson, and others throughout church his-
tory. The only hope for the poor lies in the establishment of
functioning models of Biblical charity.
