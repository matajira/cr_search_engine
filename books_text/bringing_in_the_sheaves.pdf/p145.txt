DISCOVERING AND IDENTIFYING NEEDS = 145

fore, your charity outreach must be precisely tailored to your unique
situation. Obviously, a church in a posh suburb of San Jose is not
going to be able (or willing) to duplicate the charity programs of an
inner-cily church in Chicago. A small rural church in Southern Ohio
will want a substantially different approach to implementing Biblical
charity than a large urban church on the edge of Houston’s industrial
complex. Although the Scriptural prescriptions for the exercise of
charity are immutable, their applications are extremely flexible.

Demographics

Demographics is the science of vital statistics. It is the gathering,
sorting, and evaluating of data so that informed projections can be
made about the future, and wise decisions can be madc in the present.
Demographics have, in recent years, become an essential tool of any
and all who wish to have a successful impact on the American cultural
apparatus.

But, except for an occasional evangelistic survey,” our churches.
have left demographics to the domain of the pollsters, the
sociologists, the advertisers, and last, but not least, the liberal bot-
tom-rung bureaucrats. Why? Demographic acumen can provide the
raw materials for an informed, precise, effective, focused, efficient,
and productive charity outreach. No need to administrate by guess
and by golly. No need to flail about in uncertainty. No need to mimic
mindlessly the “proven successes” of others. No need to duplicate
services and ministries ably provided elsewhere. Demographics can
take the foundation of good theology and the framework of committed
believers, and channel them to appropriate effect. Demographics can
mean the difference between a powerful! societal and spiritual impact
and a “gospel blimp.”

Anytime the church ventures into unknown and unexplored
realms, there will be elements of risk. We will, of necessity, have to
leam from our multitudinous mistakes. But, by informing ourselves
about the community about us, those risks are reduced and those
