208 BRINGING IN THE SHEAVES

rate of New Park Street Chapel in 1854, the work with the poor
continued unabated. When the church moved to larger facilities in
1861, it was apparent to Spurgeon that the alsmhouses, too, would
need to be moved into larger and more up-to-date facilities. There-
fore, he launched the construction of a new building for them.
According to press reports at the time, “no greater effort has ever
been expended on behalf of the city’s destitute.”’2

The new structure consisted of seventeen small homes which, in
the manner of the times, were joined together in an unbroken row,
There in homestyle fashion, the poor were not only sheltered, but also
provided with food, clothing, and other necessitics. In succeeding
years, a school, an orphanage, and a hospital were added, each an
expression of that holy and zealous compulsion . . . Christian com-
passion.

Both Rippon and Spurgeon looked upon their work of sheltering
the homeless as part and parcel with the rest of their ministry. It was
inseparable from their other labors: preaching, writing, praying, and
evangelizing. It was inseparable, in fact, from their faith in Christ.

Once a doubter accosted Spurgeon on a London thoroughfare
and challenged the authenticity of his faith. Spurgeon answered the
man by pointing out the failure of the secularists in mounting a
practical and consistent program to help the needy thousands of the
city. In contrast, he pointed to the multitudinous works of compassion
that had sprung from faith in Christ: Whitefield’s mission, Mueller’s
orphanage, Bernardo’s shelter. He then closed the conversation by
paraphrasing the victorious cry of Elijah, boisterously asserting,
“The God who answereth by orphanages, LET HIM BE GOD!”

Authentic Faith

In Matthew 25, Jesus describes a scene that may help us to understand
the compulsion of Rippon and Spurgeon to wed faith and deeds so
sacrificially. The scene is not a comfortable one for us to imagine. It
seems to cut across the grain of all that we have come to hold near and
