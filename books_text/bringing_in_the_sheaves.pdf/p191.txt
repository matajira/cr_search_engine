LOAVES AND FISHES 191]

the time, and the resources to put together such a staff? Not many!

So, now we’re back at square one. Our only options are either to
allow the food pantry ministry to stumble along by guess and by
golly, or to come up with a creative alternative to the large, efficient
staff that accomplishes essentially the same thing.

The Brown Bag Project is just such an alternative.

The Brown Bag Project solves the problems of stocking, storing,
and inventorying. It solves the problem of nutritional balancing and
equitable distribution. It even eliminates the need for a large, weil-
trained staff.

Here’s how it works:

Print up several hundred small labels with a list of items neces-
sary for a balanced diet for a family of four over a four- to five-day
span. Such a list would need to include at least:

4 16-ounce cans of vegetables
4 16-ounce cans of meat (tuna/chili)
4 16-ounce cans of fruit
4 10-ounce cans of soup
1 package of noodles (or macaroni & cheese)
1 package of dry beans
1 package of cereal (dry or cookable)
1 package of crackers
1 package of powdered milk
1 jar of peanut butter
1 family-size bar of bath soap
In short, one good hefty sack of groceries.

Once these labels have been printed, then the church youth group
can spend a Saturday afternoon pasting them onto large brown bags
obtained through the cooperative generosity of a local grocer, Having
that done, they can distribute the sacks to each home in the congrega-
tion or neighborhood to be subsequently filled with the specified
items.

Ina way, the Brown Bag Project is nothing more than a carefully
