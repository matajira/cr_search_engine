INTRODUCTION

The Bridge

 

y the time I arrived on the scene, a crowd had already gathered.

Some stood about uncomfortably talking in hushed and guarded
tones. Others shouted up at the solitary figure perched between
trusses of the bridge. Others had sauntered out into the knee-deep
waters of the San Jacinto River hoping for an unhindered view of the
action.

Making my way through the oglers and the curiosity seekers, I
got within ten yards of the bridge when a voice split the air,

“Preacher, don’t you come no closer. I’m gonna jump. I’m
gonna jump, you hear?”

Squinting in the half light, I recognized the huddled and desper-
ate form clinging to the rusty girder. It was Johnny Porston. Instantly
my mind began racing as | offered words of comfort, assurance, and
Scripture. Shivering in the damp coolness of twilight, I talked, 1
pleaded, I exhorted, and I prayed.

Just as the police arrived, I decided I had best make a move

19
