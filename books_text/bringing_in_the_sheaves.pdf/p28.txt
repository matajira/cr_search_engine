28 BRINGING IN THE SHEAVES.

“Hey, relax, Hoskins,” solaced the suddenly sage Will. “The
lay-off in “74 was a vacation. Soon as a few orders came in, we were
all back on line. They even ran three shifts for a while there. Over
time, too. It'll all pan out.”

The impromptu partcy interrupted itself to scrutinize the somber
parade of union stewards now emerging from the offices overlooking,
the linc. The crackly Muzak hushed as an obviously dejected
spokesman stepped up to the PA. ‘*Looks like bad news,” intoned
Will. “Get ready for the pink slips.”

After the announcement, a suffocating silence gripped the plant.
No one spoke. No one groaned. No one even breathed. In shock,
1.500 men stared blankly ahead.

Eight months later, Will was standing in line at the unemploy-
ment office. By this time, of course, it was a standard routine: eight
months of forms and lines and unfeeling clerks and shabby bottom-
rung bureaucrats,

“Twenty-seven years,” he grumbled to no one in particular.
“Twenty-seven ycars I put in my eight hours every day at that plant.
Who'd ever figure on them shuttin’ the whole shebang down? I mean,
lay-offs, that’s to be expected, but to closc us down... ?”

The recession that flexed its stranglehold on the United States in
the "70s and ‘80s dramatically altered the fabric of American culture.
Stories tike Will Salinski’s have been repeated thousands of times
across America’s industrial heartland. From the steel country of
western Pennsylvania to Michigan’s automobile towns; from the
machine tool factories of Illinois to the red iron ore pits of Minnesota's
Mcsabi Range; many hard-working Americans, for the first time,
faced the brute realities of silent machinery and boarded-up store-
fronts, with once proud men waiting in welfare lines and leaden-eyed
women staring from their windows. And though the ensuing recovery
put thousands of the unemployed back to work and shifted the focus of
the media to new vistas of fascination, the misery of privation remains
the daily lot of still thousands morc.!
