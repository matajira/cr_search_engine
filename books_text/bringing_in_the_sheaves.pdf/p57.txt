GOOD SAMARITAN FAITH 57

place a high priority on the care of the poor. Even a cursory glance
through the New Testament hall of fame reveals a startling level of
commitment to ministries of compassion.

Tabitha was a godly woman whose chief occupation was “‘help-
ing the peor” (Acts 9:36-4}).

Barnabas was a man of some means who made an indelible mark

 

on the early Christian communities, first by supplying the needs of
the poor out of his own coffers (Acts 4:36-37), and later by spearhead-
ing relief efforts and taking up collections for famine-stricken Jude-
ans (Acts 11:27-30).

Titus was the young emissary of the Apostle Paul (2 Corinthians
8:23) who organized a collection for the poor Christians in Jerusalem
(2 Corinthians 8:3-6). Later, he superintended further relief efforts in
Corinth, and delivered Paul’s second letter to the church there, all on
his own initiative (2 Corinthians 8:16-17). When last we see Titus, he
has taken over the monumental task of mobilizing the Cretan church
for similar “‘good works” (Titus 2:3,7,12; 3:8).

The Apostle Paul himsclf was a man decply committed to
“remembering the poor” (Galatians 2:7-10). His widespread ministry
began with a poverty outreach (Acts 11:27-30) and ultimately cen-
tered around networking the churches of Greece and Macedonia for
relief purposes (2 Corinthians 8-9). In the end, he willingly risked his
life for this mission of compassion (Acts 20:17-35).

The Good Samaritan is the unnamed lead character in one of
Christ’s best-loved parables (Luke 10:25-37). When all others,
including supposed men of righteousness, had skirted the responsibil-
ity of charity, the Samaritan took up its mantle. Christ concluded the
narrative saying, “Go and do likewise” (Luke 10:37).

These early Christian heroes fully comprehended that “the
feligion our God and Father accepts as pure and faultless is this: to
Sook after orphans and widows in their distress, and to keep oneself
from being polluted by the world” (James 1:27). They knew that true
repentance evidenced itself in sharing food and sustenance with the
