No Room aT THE INN 197

happened, we had things pretty well under control. We'd saved over
the ycars, so we had a nest egg. I started a small repair business.
Everything was fine . . . until this.” Of course, the Smythes col-
lected on insurance and federal disaster benefits. “Even with all that,
we barely had enough to pay off all our bills. We set aside some for a
couple of month’s rent at this apartment complex, but when that runs
out, I don’t know what we’ll do. Pray a lot, T guess.”

The Federal Solution

The federal government has responded to the plight of the displaced
poor like Walt Koselgrave, Jeanie Wilson, and the Smythes. But its
Tesponse has been an unqualified disaster. Its housing projects, urban
renewal initiatives, and public shelters have wreaked more havoc in
the lives of the poor than homelessness would have of its own accord.

The dream of a federal solution to the sheltering crisis died in St.
Louis, on July 15, 1972. at 3:32 p.m. (or thereabouts), when the
infamous Pruitt-Igoe housing project was given the final coup de
grace by dynamite. Previously, it had been vandalized, mutilated,
and defaced by its inhabitants and, although millions of tax dollars
were pumped back into the project, trying to keep it alive by fixing the
broken elevators, repairing smashed windows, repainting, etc. , it was
finally put out of its misery.

Pruitt-[goe was constructed according to the most progressive
ideals of the federal social planners. Its elegant fourteen stories
represented the best that they had to offer. But their best was just not
good enough.

Architect and social critic Charles Jencks has said, ‘‘Without a
doubt, the ruins should be kept, the remains should have a preserva-
tion order slapped on them so that we keep alive memory of this
failure in planning and architecture.”"3 Like the artificial ruin con-
structed on the estate of eighteenth-century eccentric Elton
Waidswelth to provide him with “instructive reminders of former
vanities and glories,”’ we should learn to value and protect our former
