We, in the church, are going to have to take over charity
and welfare, as well as preaching the Gospel. A new standard
is set before us as the day of the easy Christian grinds to
a halt.

Marshall Foster

 

The contemporary explosion of human need may present the
church in North America with its greatest opportunity to make
@ lasting impact on this continent. But this will only happen
if individual Christians and local churches act.
Bernard Thompson

 

When the church of Jesus shuts its outer door,
Lest the roar of traffic drown the voice of prayer,
May our prayers, Lord, make us ten times more aware
That the world we banish is our Christian care.

Uf our hearts are lifted where devotion soars,
High above this hungry suffering world of ours;
Lest our hymns should drug us to forget its needs,
Forge our Christian worship into Christian deeds.
F. Pratt Green

 

The real issue that divides Liberals and Conservatives
is not whether to help the poor, but how to help them.
John Eidsmoe

 

O Master, let me walk with Thee
In lowly paths of service free.
Stir me, Lord, that I might bear
The strain of toil, the fret of care.
Washington Gladden

 
