THE BoorstRap ETHIC |= 175

Christian, hence, the new outlook that brought her to HELP.

Immediately, the HELP staff went to work on the systemic
problems in the Jones family. They recommended that the three-year-
old begin attending a new Christian daycare center that had recently
been established in the Fourth Ward. There, she would receive
educational enrichment in preparation for entering formal school in
two years. The seven-, ten-, and fourteen-year-old children were
found to be far below normal in math and reading skilis. Thus, HELP
workers enrolled all three in extensive tutorial programs that the local
Baptist Association of Churches provided. The sixteen-year-old was,
essentially, a drop-out. Although officially in the ninth grade, he
tested at the fourth grade reading level. Since Mrs. Jones herself had
received little formal education, and was also virtually illiterate, both
mother and son were enrolled in special adult educational programs
which had been started in the community. Both were also placed in
part-time job situations developed by the HELP placement service.

HELP workers then began to teach the family basic financial
management skills, from budgeting to tithing, from thriftiness to
resourcefulness. Before long, the stranglehold that poverty and
dependence had exercised for so long, began to loosen, and the
family’s lot improved for the first time ever.

Oscar Jackson, the man who had been living with the family, was
placed in a training program reflecting his vocational interests, and,
within six months, had gotten an excellent job with an independent
building contractor in town. Weeks later, he and Mrs. Jones were
married and the whole family went off welfare.

Without the comprehensive care and consistent follow-up pro-
vided by HELP’s re-education program, it is highly unlikely that the
family would have ever been abic to escape from the welfare trap.
Even the offer of jobs would have been futile. But with training and
personal attention, they were afforded the tools necessary for the
development of self-reliance and responsibility.

Thus, to do the work of Biblical charity effectively, our out-
