Go WITH WHAT You’vE Gor 211

powerful or weak, every Christian is called to exercise compassion.
‘We are not to allow constraints of time, money or influence to deter us
from our high calling. We are to go with what we’ve got, even if what
we've got is minimal to the extreme. Like our Macedonian forebears,
we are to “‘give according to our ability and even beyond our ability”
(2 Corinthians 8:3).

Notice, too, that the ethical duty to offer Biblical charity to the
broken and afflicted is not conditioned on the talents, skills, or gifts
which we may or may not have. According to Scripture, every
Christian has been accorded one or more supernatural spiritual gifts (1
Corinthians 12:7; | Peter 4:10). And each of the gifts, be it teaching,
helps, wisdom, or evangelism, or any of the others, can be marshalled
to the support of a ministry of compassion. Again, we are to go with
what we’ve got. We must let nothing stand in our way.

Fulfilling our Commission
Those of us willing to walk in accord with the ethics of authentic faith
have been given a mandate; win the world for Jesus Christ. Leave no
stone unturned. Leave no institution untouched. Leave no endeavor
undone,

The strategy for accomplishing this monumental task of world-
wide conquest is clearly stated in Scripture:

For though we live in the world, we do not wage war as the
world does. The weapons we fight with are not the weap-
ons of the world. On the contrary, they have divine power
to demolish strongholds. We demolish arguments and
every pretention that sets itself up against the knowledge
of God, and we take captive every thought to make it
obedient to Christ (2 Corinthians 10:3-5)

As David Chilton has observed, according to this Scripture,
