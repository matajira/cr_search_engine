14 FOREWORD

in doing God’s work in the late twentieth century? Above all, how can
we comprehend our responsibility to help the poor in such a way that
itis integrated with a biblical understanding of the lordship of Christ
over the whole cosmos, so that we don’t isolate this work from the rest
of life, thus idolizing it and turning it into something evil?

We're indebted to George Grant for helping us see our way
through this complex of issues. Rather than continuing to beat the
dead carcass of the welfare system, he leaves the putrefying mess and
heads for fresh air. He shows us our real responsibilities, quoting the
same biblical passages as the defenders of public welfare. But he does
it without the sense of helplessness and guilt that are the identifying
features of humanist preachments, including those erroneously
advanced by Christians.

Moreover, he presents the problem to us in its proper historical
context. We don’t face unprecedented problems; the poor have been
with us from the beginning, and the Christian church has always been
doing something about it. C. H. Spurgeon’s orphanages in nine-
teenth-century London were not as famous as his pulpit, but they were
as fully a part of his ministry. We're not isolated in either time or
space, Grant shows us, but are part of a community of vigorous
service to the poor as far back as the ancients and as near as our
families and neighbors. The body of Christ is the ministering agent
that accomplishes God's commandments, and that includes the minis-
try to the poor.

But Grant intends this book to be a manual for service as well as
a too] for understanding our true role in helping poor people. We learn
in it how to make visible the hidden poor, how to gather and distribute
food; how to find lodging for the homeless; how to minister spiritually
as we help physically; how to anticipate and protect against legal
challenges; how to work together as families and communities, and
thus avoid being defeated by the insidious atomization that is wreak-
ing so much havoc on the larger society.

Thave not read anything so useful in helping us move away from
