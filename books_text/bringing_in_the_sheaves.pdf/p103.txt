DEVELOPING A MISSION 103

worship means that we will be able to participate dynamically in
history. Worship then must be marshalled to the task of defeating the
scourge of poverty.

Third, the missionary implications of the sacraments, especially
the Lord’s Supper, must be recovered. James B. Jordan reminds us,

Historically, the church has particularly remembered the
poor in connection with the Lord’s Supper. That’s because
this is God’s gift to the starving. It is not the gift of
philosophy or of theology, of ideas or inward feelings.
First and foremost, it is the gift of food! Thus, for instance:
the Christian Reformed churches traditionally have a spe-
cial collection for the poor right after the quarterly com-
munion meal. And the historic churches take up food and
clothing for special gifts at Christmas and Easter, that all
may feast.®

Thus comprehended, the Lord’s table, where we reap His boun-
tious grace provisions, becomes a continual provocation to missions.
Thus comprehended, the Lord’s table becomes an ever-present
reminder of our earthly task.

Fourth, the Sunday School also must be utilized as a dynamic
prod for missions once again. Instead of being a dilapidated vehicle
for watered-down moralisms, the Sunday School could serve as an
intensive training camp for dedicated Kingdom activists. Rescued
from banality, Sunday Schoot could be the platform from which
strategies are plotted, tactics are launched, and reclamation is begun.

Why not start a weekly elective Sunday School class or training
union series to explore what Scripture has to say about poverty and the
appropriate Christian response to it? If we start small and take our
learners along in smooth, carefully plotted stages, it won’t be too
terribly long before we have a whole slew of Christians chomping at
the bit, raring to jump headlong into the battle against hunger,
