GOOD SAMARITAN FAITH 63

out blindly in search of “truth, justice, and the American Way.” At
the same time, Biblical Law is not a passionless system of do’s,
don'ts, hows, and whys. Biblical Law is not a prison of rules and
regulations. Biblical Law is simply the encoded mercy, grace, and
peace of God. It is love’s standard. Thus, Biblical Law does not lock
in to heartless, soul-less exercises in social control.

Law and love are inseparable, working in tandem to the glory of
Christ and His Kingdom.

Summary
Welfare is not the government’s job. Welfare is our job. It is the job of
Christians.

The hallmark of authentic Christianity, from the first century
forward, has been its compassionate care of the poor and afflicted.
Even a cursory examination of the ministry of Christ, and that of His
disciples, reveals this as a dominant theme: the role of the Good
Samaritan is to be assumed, not by the State, but by the believer.

In fact, so central was welfare to the task of the disciples, that
even the structure of the church was custom-designed to facilitate its
efficient execution. Thus, the office of deacon was established.

Of course, the work of caring for the needy is not simply and
neatly relegated to the agency of the diaconate. Under its leadership,
all believers are to live out the full implications of the Good Samaritan
faith. All believers are to walk in love.

Love is not just a sweet and soppy sentiment. Love is something
you do. Christ’s life was a crystal clear translation of this fact into
flesh and blood. As His disciples, we, too, must love, not “with
words or tongue, but with actions and in truth.”

Not surprisingly, then, Scripture provides specific patterns for
implementing love in the hard reality of daily life. Law and love are,
thus, coordinated, insuring the care of the poor, and the authenticity of
the Gospel. The Law-love patterns are the Good Samaritan faith.
