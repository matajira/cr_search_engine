204 BRINGING IN THE SHEAVES

stipulation at the outset. Biblical charity is never to be a gravy train for
sluggards and ‘‘professional bums.” Develop each program in such a
way that it naturally “weans” applicants from relief. The purpose of
our efforts is not to transfer the poor from one dole to another, it is to
translate poverty into productivity. We must offer efficient, inexpen-
sive, decentralized, and genuinely compassionate care to the disad-
vantaged, all the while guarding against ingratitude, sloth,
negligence, and irresponsiblity.
That’s Biblical charity.

Summary

Homelessness is a serious problem in America. Between three and
four million people bed down each night in some back alley, aban-
doned warehouse, or quiet public park.

Again, the federal response is sorely inadequate at best,
ominously destructive at worst.

But unless, and until, Christians provide Biblical alternatives,
the poor will have to continue to contend with cither the alleys and
warehouses, or the Pruitt-Igoes. There are no other options.

By meeting the needs of the ‘aliens’ and “sojourners” in our
midst, by opening our homes to the homeless, or by operating short-
term community church shelters, the vicious cycle of indolence and
irresponsibilty can be effectively broken. Or, even better, by working
out long-term, creative lease arrangements with local landlords, the
homeless can be given a fresh start and a new hope.

And that really and truly is Biblical charity.
