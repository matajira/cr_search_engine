DEVELOPING A MISSION 97

Tehran. Humanism’s hope of medical and genetic perfectability has
been shattered in the ovens of Auschwitz, the abortuaries of New
York, and the nurseries of Bloomington. Humanism’s hope of win-
ning the war on poverty has becn shattered in the ghettos of New
York, Chicago, and Detroit. But the Biblical hope has never yet been
found wanting. In fact, as history marches ever forward, that hope
becomes ever more secure.

Like our Good Samaritan forefathers, we must develop an
optimistic view of the future if we are to, in any form or fashion, be
faithful to the call of God and thereby motivate others to action. We
must expect great things and, thus, attempt great things, so that we
may accomplish great things.

War-Zone Mentality

The Biblical charity pioneers were a peace-loving people. Above all
else, they desired to see their families and congregations live long and
harmonious lives. But, at the same time, they nurtured a war-zone
mentality. It is not that they harbored latent paranoia. Far from it. It is
simply that they viewed all of life through the lens of Scripture. And
Scripture teaches that Christians are in a war. It is a war that crosses all
boundaries and invades all ages. It is a life-and-death struggle of
cosmic proportions. Those early faithful Christians believed that and
lived accordingly.

They took seriously the Apostle Paul's admonition to “put on the
full armor of God” in order to prepare for conflict with the dastardly
forces of darkness (Ephesians 6:10-18). They understood it to be their
calling to “wage war,” to “demolish strongholds,” and to “tear down
fortresses” (2 Corinthians 10:4-5). They were more than willing to
“suffer hardship as good soldiers of Christ Jesus” (2 Timothy 2:3). Of
course, they comprehended that their primary enemies were not
“flesh and blood” (Ephesians 6:12) and, thus, they were “not to wage
war as the world does” {2 Corinthians 10:3).

Those stalwart men and women who willingly gave their all for
