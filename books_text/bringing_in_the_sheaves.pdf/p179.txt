THE BOOTSTRAP ETHIC 179

provide for their families, and to prepare for the future.

Precautions

Some of the poor that you will run across in your work of compassion
are not going to be interested in improving their lot. They are not
going to be interested in jobs, or re-education, or anything of the
kind. They will refuse to work and demand a handout. These are the
kinds of people you can expect to attract every now and then if you
undertake a Biblical charity outreach. These are the kinds of people
that Scripture labels “sluggards.””

The teaching on sluggards is clear and precise. The Bible says
that sluggards waste opportunities (Proverbs 6:9-10), bring poverty
upon themselves (Proverbs 10:4), are victims of self-inflicted bond-
age (Proverbs 12:24), and are unable to accomplish anything in life
(Proverbs 15:19). A sluggard is boastful (Proverbs 10:26), lustful
(Proverbs 13:4), wasteful (Proverbs 12:27), improvident (Proverbs
20:4), and lazy (Proverbs 24:30-34). He is self-deceived (Proverbs
26:16), neglectful (Ecclesiastes 10:18), unproductive (Matthew
25:26), and impatient (Hebrews 6:12).

So what are you to do with such a person, should he show up at
your doorstep? According to Scripture, you are to turn him out. He is
not to receive the benefits of compassion and mercy. “If a man will
not work, neither let him eat” (2 Thessalonians 3:10).

According to economist Robert Kuttner,

The first premise of the free market is that people get more

or less what they deserve. If the state or private charities
tamper with that principle of desert, the most efficient
producers may well stop working so hard, while those who
happen to be inefficient, or even slothful, will be over-
compensated. This is why the poor presumably need the

spur of their own poverty.!>

Biblical charity must never mimic the folly of welfare by tamper-
