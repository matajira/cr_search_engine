112 BRINGING IN THE SHEAVES

rags stained dingy yellow by life in the streets. The pockets of their
tattered overcoats and their shopping bags bulge with all the little bits
of rubbish they collect and live on. They are filthy and suffering . . .
bent and twisted by the downward curve of hunger, desperation,
feeble-mindedness, and want. These are our nation’s untoucha-
bles . . . America’s pariah: invisible, disposable, and surplus. They
are the destitute waste of our flailing welfare society.

What can be done to solve the problem of our handicapped
exiles? In Geel, Belgium, there is an interesting model of care for the
unwanted, discarded mentally handicapped. It is a system that has
been in effect since the middle of the fifteenth century.?

Over the course of 500-plus years, thousands of pilgrims have
visited the Shrine of St. Dimpna in Geel. Mentally impaired or
handicapped supplicants often travelled long distances to the site in
hope of a cure. Although records of the church attest to the many
miracles performed by the Lord, many of the pilgrims were not cured.
In such cases, all too often the natural family, in despair and frustra-
tion, returned home, leaving the supplicant behind. Invariably, local
families would open their homes to those abandoned. Again and
again, the same sad scenerio was replayed. As time passed, word
spread throughout Europe that the people of Geel had hearts of
compassion and mercy, and would open their homes to the distressed,
unwanted, and feeble-minded.

To this very day, over 1,000 families within this town of 30,000
exercise hospitality and provide medical care for one or more
impaired boarders. There has never been a recruitment program,
never acentral bureaucracy, and never a central administration. There
have been only Christian families, generation after generation,
demonstrating Christ’s love, compelled by the Good Samaritan faith.
There are no mentally retarded, no autistic, no handicapped outcasts
on the streets of Geel, because there, the people of God take seriously
the Biblical mandate to care for the helpless and equip the poor. In the
United States, there are thousands of psychologically broken victims
