62 BRINGING IN THE SHEAVES

tions love-evidenced faith in contexts that focus on service to the poor,
the hungry, the dispossessed, and the lonely. “He who oppresses the
poor reproaches his Maker, but he who is gracious to the needy honors
Him” (Proverbs 14:31). “He who is generous will be blessed, for he
gives of his food te the poor” (Proverbs 22:9). “The righteous is
concemed for the rights of the poor, the wicked does not understand
such concern” (Proverbs 29:7). “We know love by this, that He laid
down His life for us; and we ought to lay down our lives for the
brethren. But whoever has the world’s goods and beholds his brother
in need and closes his heart against him, how does the love of God
abide in him? Little children, let us not love with word or with tongue,
but in deed and truth” (1 John 3:16-18).

This is the faith, the love-evidenced faith, the Good Samaritan
faith to which God has called us.

Doing Things God’s Way

To facilitate such compassion, God not only gives us commands to
love objectively, but He gives us structures within which to love
objectively. We find those structures in the Law (Exodus 22:21-24;
Leviticus 19:9-10), in the Prophets (Isaiah 32:6-8; Jeremiah 21:11-12),
in the Gospels (Luke 14-12-14; Matthew 25:31-46), and in the Epistles
(2 Corinthians 8:1-9, Galatians 6:2-5). Living illustrations of those
structures in action are woven into the narrative sections of Scripture
(Ruth 2:2-18, 1 Kings 17:7-16), into the historical sections (Acts
4:32-35), into the poetic sections (Psalms 15:1-5; 72:12-14), into the
liturgical sections (Isaiah 1:11-18), and into the didactical sections
(Matthew 6:1-14; 2 Corinthians 9:1-15).

The reason Scripture is so specific about the implementation of
charity is precisely because of the unique interrelationship of Law and
love, Biblical love is not a naive, guilt-provocated sentiment. Biblical
love is not a feeling. Biblical love is simply the compulsion to do
things God’s way, living in obedience to His unchanging, unerring
purposes. It is Law's motivation. Thus, Biblical love does not strike
