The only thing necessary for the triumph of evil is for
good men to do nothing.
Edmund Burke

 

No lasting solution to the problem of poverty can be bought
with a welfare check.
John F. Kennedy

 

Our goal must be to help the helpless and the elderly so
that they can lead full lives and empower the poor to control
their lives and rise from poverty.
Congressman Newt Gingrich

 

But we can never prove the delights of His love
Until all on the altar we lay;
For the favor He shows and the joy He bestows
Are for them who will trust and obey.
John Sammis

 
