234 BRINGING IN THE SHEAVES

dervan Publishing House, 1978). In the midst of total judgment,
how could Isaiah maintain an absolute confidence in the future? In
this classic commentary, first published in 1846, Alexander tells
us: Isaiah trusted the promise of God, the promise of ultimate
victory.

St. Athanasius. On the Incarnation. Translated and edited by Sister
Penelope Lawson, C.S.M.V. (New York, NY: St. Vladimir's
Seminary Press, 1982). This edition of Athanasius’ most beloved
work, with its tantalizing introduction by C. S. Lewis, is an
absolute must book. It abounds with the optimism that marked the
advancing, victorious, early church.

Chilton, David. Paradise Restored (Tyler, TX: Reconstruction Press,
1985). In this book, subtitled “An Eschatology of Dominion,”
Chilton brilliantly outlines the Bible’s explicit emphasis of opti-
mism and victory. But the book's value goes beyond a mere
exposition of hope; it is a practical manual on Biblical her-
meneutics and prophetic speculation as well.

Murray, Ian. The Puritan Hope (Carlisle, PA: Banner of Truth Trust,
1971). This survey of Puritan thought, both English and American,
provides an insightful look at how and why our spiritual and
cultural forefathers exuded optimism in their views of revival and
in their interpretation of prophecy.

North, Gary. Unconditional Surrender (Tyler, TX: Geneva Press,
1981). God’s program for victory is outlined in a clear and concise
fashion in this excellent introductory book.

Rushdoony, R. J. The Institutes of Biblical Law (Phillipsburg, NJ:
Presbyterian and Reformed Publishing Co., 1973). This master
work lays the foundation for comprehending the pattern of God’s
Law as applied to personal and cultural situations. Rushdoony’s
comprehensive exposition of the Ten Commandments here has
caused a veritable revolution in theological circles.
