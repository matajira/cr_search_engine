CHAPTER 4

Biblical Charity

 

he practice of the Good Samaritan faith did not die with the last of
Tire first-century disciples of Christ. Bible-believing Christians
have long been committed to the care of the poor and afflicted.
Throughout history, they have taken the lead in the establishment of
orphanages and hospitals, almshouses and rescue missions, youth
hostels and emergency shelters, soup kitchens, and community
schools, From Polycarp to Penn, from Athanasius to Abelard, from
Cranmer to Clarke, from Francis of Assisi to Francis Schaeffer, the
church has been led by godly men and women who lived out the full
implications of the Good Samaritan faith.

Gcorge Whitefield is best known for his rote in sparking the
great Mcthodist revival in England and the Great Awakening in the
American colonies. But the chief concern of his life, and the labor to
which allt else was subverted, was the erection and maintenance of an
orphanage in Georgia. As early as 1737, Whitefield’s unflagging
energies focused on the relief “of the deplorably destitute children,

65
