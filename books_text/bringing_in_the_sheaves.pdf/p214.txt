Ours is the call to be broken bread and poured out wine. Only
then can we confidently sing of sheaves brought into the barns

of plenty.
Oswald Chambers

 

As the westering sun stoops to drink from the sea
So the Finger of God descends, destroying my complacency.
The whispering tides bear their tales to the sand;

The swell of Your Sovereignty contains my spirit's command.
The winter wind patterns pavanes as it harps through the trees,
The Breath of God blows, compelling my minstreisy.
Kemper Crabb

 

1 see the poor, I see the lame.

1 hear the sobs, the cries of pain;
Yet with this hurt I seldom share,
Teach me, O Lord, teach me to care.
Broadman Ware

 

The typical American Christian is an economic conservative with
@ guilty conscience. This is because he hears so much conflicting
information about his Christian responsibilities in this area,
he doesn’t know what to do.

John Eidsmoe

 
