Retreat is not Biblical.
Gary North

 

At two o'clock in the morning, if you open your
window and listen

You will hear the feet of the Wind that is going

to call the Sun.
And the trees in the shadow rustle, and the trees
in the moonlight glisten.

And though it is deep, dark night, you feel that
the night is done.
Rudyard Kipling

 

Far and near the fields are teeming
With the waves of ripened grain;
Far and near their gold is gleaming
Over the sunny slopes and plain.

Lord of the Harvest, send forth reapers!
Hear us, Lord, to Thee we cry;
Send them now the sheaves to gather
Ere the poverished swoon and die.

Send them forth with morn’s first beaming.
Send them in the noontides glare;
When the sun’s lost rays are gleaming,
Bid them gather everywhere.

John O. Thompson

 
