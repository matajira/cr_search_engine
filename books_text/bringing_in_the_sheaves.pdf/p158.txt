158 BRINGING IN THE SHEAVES

of compassion has been implemented, other churches will begin to
exercise interest in the program or, at the very least, the concept.
When that starts to happen, it’s time to take the next big step in
organizing for Biblical charity: establishing a community association
or coalition. The purpose of this association is to pool resources and
to network efficiently, so that the scope of Biblical charity can expand
beyond the bounds of any single church. It is to coordinate activities,
share ideas, minimize overlap, and maximize compassion. Com-
posed of members of the task force of several churches, the associa-
tion benefits from the shared strengths of a number of unique charity
outreaches. Even so, bigger doesn’t always mean better. As Harv
Oostdyk has said, “Don’t try to organize more than four to seven
churches. If you try to organize a lot of churches, you are often limited
to your lowest common objection. Let a smail handful of churches
demonstrate success, and you will find many others following the
initial attempt.” So, start small.

But don’t start unambitiously. The community coalition can be a
powerful tool to feed the hungry, shelter the homeless, and clothe the
naked. By bringing the weight of several different churches to bear on
poverty, a number of important, but otherwise impossible, tasks are
accomplished.

A community coalition of sorts was brought together in Jerusa-
lem to sort through some sticky theological questions and to give
directions to the ever-expanding mission of the first-century church
(Acts 15:1-6). The Jerusalem Council convened with the most
anointed minds in Christendom in attendance: Paul, Barnabas, Jude,
Silas, Simeon, and James. Together they gave impetus to evangelism,
suasion to ethics, and impulse to charity (Acts 15:15-21). Together,
they launched a program of world-wide Christian dominion.

By pooling our resources, coordinating our thinking, and focus-
ing our aid through community coalitions, we cannot only facilitate
dynamic structures of justice and mercy, but we can once again
unleash a Great Commission force such as has not been witnessed
