80 BRINGING IN THE SHEAVES

Israel, being a Moabitess (Ruth 1:4) and, furthermore, she did not
appear to have any readily marketable skills. The good news was that
God’s Law made abundant and gracious provision for strangers
(Exodus 23:9; Leviticus 19:33-34; Deuteronomy 24:17-18) as well as
for unskilled, destitute workers (Leviticus 19:9-10; 23:22; Deu-
teronomy 23:24-25; 24:19-22). These “gleaner laws” stipulated that
farmers and landowners Jeave the edges of their fields unharvested
and that overlooked sheaves remain uncollected. Any among the poor
or the alien who were willing to follow bchind the harvesters and
gather that grain were welcome to it, thereby “earning” their own
keep. Ruth took advantage of this just provision and was thus able to
uphold her responsibility to Naomi.

Several basic principles concerning charity, in general, and
gleaning, in particular, emerge from Ruth’s story.

First, it is apparent that recipients of Biblical charity must be
diligent workers, unless entirely disabled (Ruth 2:2-7). According to
R, J. Rushdoony, “This was indeed charity, but charity in which the
recipient had to work, in that gleaning the fields was hard, back-
breaking work.’!9

Biblical charity does not attempt to smooth over economic crisis
by making privation somewhat more acceptable. It attempts to solve
economic crisis. Biblical charity does not attempt to help families
adjust to their situation. It attempts to change their situation. Biblical
charity does not strive to make poverty and dependence more com-
fortable. It strives to make productivity and independence more
attainable.

The framers of the Elizabethan Poor Laws understood this when
they sought to “set the poor to work.” Similarly, Whitefield,
Spurgeon, Samuelson, and the others throughout church history
understood this and implemented it in their poverty relief programs.

Tf we are to have any success in our own day in fighting the
scourge of poverty, then we must follow this Scriptural mandate. We
must, as George Gilder has said, “‘allow the poor to overcome
