120 BRINGING IN THE SHEAVES

been actively taught and practiced, there has been great revival and
renewal. However, when the church has narrowed its view of ministry
to the point where only certain people at certain times with certain
training can perform God’s work, there has been only atrophy and
decline.

In recent years, despite the countcring effects of the body life
movement, the home Bible study movement, the charismatic, the
para-church, and the reconstruction movements, the church has
drifted away from its Reformation moorings and the practice of the
priesthood of believers. Christianity has become, by and Sarge, a
spectator sport. It has become an institution, an organization, a vast
complex of properties. Richard Halverson has estimated that even
after taking into account Sunday School teachers, choir members,
youth sponsors, ushers, and committee members, less than one out of
every twenty church members ever actually participates in the work of
the ministry. All the rest come to church just to watch.!° Since the
church can only “grow and build itself up in love, as each part does its
work” (Ephesians 4:15-16), this institutional drift has impoverished
the work of the Gospel. It has resulted in a church that is, according to
Frank Tillapaugh, “ministry-helpless.””

Anew emphasis on the priesthood of believers in our day would
bring about at least three major changes in our churches.

First, Christians would begin to understand and accept their
Scriptural responsibilities. They would spring into action. Liberated
from institutional limits, innovative and powerful ministries would
blossom from newly unfettered gifts. They would function as priests,
accountable directly and individually to the Lord and Giver of Life.
They would move out beyond institutions and traditions and become
salt and light, sharing the Gospel, reaching the lost, and mobilizing
their families to care for the hungry, the naked, and the infirm.

Second, Christians would begin to think and act in terms of the
covenant. Knowing that they were not merely individual priests, but a
priesthood, they would begin to work together. They would cooper-
