180 BRINGING IN THE SHEAVES

ing with “the principle of desert.” Biblical charity is enjoined by
Scripture to help the poor help themselves. It is designed to provide
them with a hand, not a handout.

Summary
The goal of Biblical charity is to transform poverty into productivity.
Thus, finding jobs for the jobless is an ultimate priority.

But finding jobs just isn’t as easy as it once was. Structural
changes in the economy due to international competition, inefficient
manufacturing techniques, protectionist trade policies, and inade-
quate re-training, have produced grave disparities between the types
of jobs available and the number of qualified workers seeking
employment.

Finding jobs is not, however, an impossible task, for there are
jobs to be had. It is up to our charity outreaches to find those jobs and
then match them to the deserving poor in our midst. Through a job
referral service and a career counseling program, the poor can be re-
absorbed into the economy, thus breaking the cycle of deprivation.

Sadly though, because of the debilitating effects of the federal
welfare system, which indisputably encourages indolence and sloth,
many of the chronically unemployed poor will need more than just a
referral and a bit of counseling. They'll need comprehensive re-
education. So, in order to free them from the trap of dependency, our
charity outreaches will have to teach them basic principles of
regeneration, finances, providence, health and hygiene, and industry.
They will not only have to find fit employment for the poor, they will
have to fit the poor for employment.

Thus, to help the poor help themselves, Biblical charity will
need to find them jobs, train them for jobs, and even create jobs for
them. The bootstrap ethic sets Biblical charity apart from mere
philanthropy. And that is simply due to the fact that it is designed to
provide the poor with a hand, not a handout.
