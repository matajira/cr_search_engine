CHAPTER 13

Go With What You’ve Got

 

n 1821, Dr. John Rippon, pastor of the New Park Street Chapel in

Southwark, London, began a ministry to the homeless poor. A
complex of almshouses was erected on a property adjacent to the
church and the monumental task of rehabilitation was begun. Rippon
wrote,

Christian compassion is driven by a holy and zealous
compulsion when sight be caught of deprived distress.
Talk not of mild and gentle acts, of soft provisions and
hesitant walk. Christian compassion knows only boldness
and sacrifice. Lest we strike the Judas bargain and go the
way of the goats, let us invite the strangers in. Let us
shelter the aliens beneath a covering of charity and
Christlikeness.!

When Charles Haddon Spurgeon succeeded Rippon to the pasto-

207
