GOOD SAMARITAN FalTH = 6!

liberty for men, women, and children every where (Zechariah 7:8-10)}.

In Matthew 22, when Jesus was asked to summarize briefly the
Law of God, the standard against which all spirituality is to be
measured, He responded, “You shal! love the Lord your God with ail
your heart, and with ail your soul, and with all your mind. This is the
great and foremost commandment. And the second is like it; you shall
love your neighbor as yourself. On these two commandments depend
the Law and the Prophets.”

Jesus has reduced the whole of the Law, and thus, the whole of
faith, to love. Love toward God, and then, love toward man. But, at
the same time, Jesus has defined love in terms of Law. In one bold,
deft stroke, He freed the Christian faith from subjectivity. By so
linking love and Law, Christ has unclouded our purblind vision of
both. Love suddenly takes on responsible objectivity while Law takes
on passionate applicability.

This sheds a whole new light on what it means for us to “walk in
love.” If our love is real, then it must be expressed; it will be
expressed. If our love is real. then action will result because “love is
something you do,”
“royal law” (James 2:8).

Faith, according to Jesus, is verifiable, testable, and objective
because it is manifested in a verifiable, testable, and objective love.
Thus, Jesus could confidently assert that love is “the final apolo-
getic” (John 13:34-35). And Paul could argue that all effort is vain for
the Kingdom if not marked by love (t Corinthians 13:1-3). And James
could disavow as genuine any and all loveless, lawless, workless faith
Games 2:14-26).

True faith gets its hands dirty in the work of compassion because
that is the way of love. Faith cannot be personalized, privatized, and
esoteric because love catinot be personalized, privatized, and eso-
teric. True faith moves out into the push and shove of daily living and
shows forth its authenticity via love.

It is not surprising then to find that Scripture repeatedly men-

not merely something you feel. Love is the
