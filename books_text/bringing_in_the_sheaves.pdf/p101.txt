DEVELOPING A MISSION 101

of church life will do.

First, the preaching program of the church must be mobilized to
motivate, equip, and educate the saints so that they can then undertake
the work of the ministry (Ephesians 4:12). But all too often our
preaching, the primary means of reproducing Scriptural convictions
in others, has been entirely inadequate. Our focus in homiletics has
either been doctrinal and exegetical to the near exclusion of specific,
practical application, or is awash in an existential piffle, drivel, and
swill, As a result, our sermons have lost their life. They are either dry
or soppy. They arc cither intangible or incorrigible. But either way,
they lack both heart and art. The chief end of preaching is to proclaim
God’s Truth and to thus give Him glory. To be sure, there is no glory in
either dead orthodoxy or flash-in-the-pan contentlessness.

In order for Biblical charity to sec resurgence in our day, in order
to mobilize our congregations for Good Samaritan effectiveness, our
preaching must emphasize both content and passion. Our homiletical
art must match the level of excellence in our homiletical exposition.
Men’s minds must be informed, and their hearts must be stirred. A
sermon’s intent is not simply to transfer information or to provoke.
metaphysical fireworks. It is to motivate. It is to change. It is to ignite
zeal. It is to reproduce convictions. Ht is to set into action the army of
God. It is to lay the foundations for a Biblical worldview and an
optimistic militancy among God’s own.

Why not encourage your pastor to undertake a series of sermons
on the subject of Biblical charity? Perhaps an exposition of Ruth? Or,
ifhe is hesitant, maybe you could begin to give him a few books on the
subject. Keep him informed about the works of compassion that
faithful followers of Christ the world over are undertaking. Encour-
age him. Support him.

When the pulpits of America begin to sound the strains of the
Good Samaritan faith, a vast army of motivated, dedicated warriors
for Truth will emerge. When missions-oriented sermons ring forth
once again, then we will have committed congregations.
