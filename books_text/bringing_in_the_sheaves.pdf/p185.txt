LOAVES AND FISHES = 185

need. But, for the short-term, the hungry need food.

Food for the Hungry

The federal response to hunger has, for years, focused on food
giveaway policies. The Food Stamp Program, the School Lunch and
Breakfast Programs, the Special Supplemental Food Program for
Women, Infants, and Children (WIC), the Summer Food Program,
the Child Care Feeding Program, the Elderly Nutrition Programs, the
Meals-on-Wheels Program, and the FDIC Cheese and Butter Dis-
tribution Program were designed to eradicate the awful! menace of
hunger in our land, But the paternalistic state’s loaves-and-fishes
mentality is bankrupting the entire system. And still a hungry hoard
of federal dependents cry out for more. It is apparent that the federal
food programs, as monolithic as they are, are inadequate. The state
has failed. People are hungry.

In response to the hunger crisis, many Christians have called on
the government to add still more food relief programs. They have
urged legislators to honor the so-called “right to feod.”” They demand
that radical wealth redistribution programs be enacted. Or, for lack of
any other tactic, they supplement the federal giveaway debacle by
imitating its extravagance. Loaves and fishes for everyone. Come
one, come all.

Biblical charity also focuses on food relief, but from an entirely
different perspective. It takes as its model the Old Testament provi-
sion for gleaners. Thus, an effective distinction has been made
between the sluggard and those willing to labor for their sustenance.
Instead of trading his dignity for a five-pound block of cheese, the
gleaner earns his keep. He works. He provides for his family by the
sweat of his own brow. To be certain, he is aided and abetted by his
charitable brethren, but it is by his labor that fruit is brought forth.
The gleaner model is easily transposable to our contemporary
situation.
