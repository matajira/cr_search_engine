220 BRINGING IN THE SHEAVES

American Vision
P.O Box 720515
Atlanta, GA 30328

American Vision is a Christian educational and communications
ministry dedicated to producing a comprehensive Biblical charity
curriculum. Besides this book, a series of workbooks, audio and
video tapes, youth materials, and newsletters have been slated so that
churches can begin the task of Biblical charity all across America.
American Vision has also embarked on an ambitious Worldview
Library publishing project that should prove to be invaluable to the
vital work of reconstruction.

Food for the Hungry
7729 East Greenway Rd.
Scottsdale, AZ 85260

Food for the Hungry was started in 1971 by Dr. Larry Ward as a
non-profit, non-denominational organization committed to disaster
relief and long-range self-help assistance to the poor of the world.
Their work in Africa, Asia, and Central America has been
unparalleled in its effectiveness and Scripturalness.

Geneva Ministries
708 Hamvassy Ln.
Tyler, TX 75701

Geneva Ministries is an education and information outreach of
Westminster Presbyterian Church in Tyler, Texas. Through their
many newsletters, books, and journals, they have propagated the
principles of Biblical charity perhaps more cogently than any other
