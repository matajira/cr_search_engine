In a cave, a lowly stable, Christ our Lord was born;
No room was to be had, no inn to be found
As angelic splendor lavished on homelessness that morn.
Charles McQuade

 

A shade by day, defense by night
A shelter in the time of storm.
No fears alarm, no foes afright,

A shelter in the time of storm.
The raging storms may round us beat
A Shelter in the time of storm.
We'll never leave our safe retreat
A Shelter in the time of storm.
Vernon J, Charlesworth

 

Lord, may Thy church, with mother’s care
Selflessly, her refuge share,
And grant as mornings grow to eves
Passioned haven to scattered sheaves.
Howell Hopkins

 

We blithely sing of “Bringing in the Sheaves,” yet shirk
the duty of bringing in. How can this be? If we are to have
the sheaves, then we must have the gumption to actually bring
them in.

Andrew Murray

 
