DISCOVERING AND IDENTIFYING NEEDS 143

discouraged, including Greg. “I practically guilt-and-pitied myself
to death over the plight of the poor. And for what?” he asked Evelyn
one evening, not really expecting an answer. “Last night, for
instance, we had three winos and a German shepherd show up at the
gym for shelter. I ended up letting the dog stay, but the winos were so
unruly, filthy, and ungrateful, I had to kick them out. It seems as if we
haven’t even touched the needs of the people who really need help,
because we spend all our time dealing with the riff-raff. I don’t even
know if we’ve even seen the people who really need help. I mean,
where are the Freddies? How can we help them? How can we reach
them?”

Countless attempts to help the poor have ended in a similar
fluster of resignation. They were programs that began with bright-
eyed enthusiasm and altruistic idealism, that began with all the
promise of a spring monarch’s premier flight. But, they failed,
nonetheless.

Why? Why was Greg Davies unable to help Freddie and the
others like him, the people he had so intently set out to help?

Part of the reason is that guilt and pity, the primary impulses
behind Greg’s outreach, and many, many others like it, are insuffi-
cient for the task of Biblical charity. Guilt and pity are, in fact,
crippling emotions because they are measured by and against man.'
Ethical obedience to God’s love and Law, on the other hand, liberates
and empowers, because it is measured by and against God. The
compulsion to exercise compassion is invariably exhausted if it has no
deeper root than a metaphysical or existential burden.

But, beyond that strategic philosophical defect, Greg’s outreach
and the others like it that floundered and died shortly after inception,
made a serious tactical error as well. In order to be successful, a
program of Biblical charity must have focus. It cannot simply set out
to “help the poor.” It must pinpoint a precise target group. It must
identify needs. It must develop relationships, set goals, and establish
priorities. It must carefully weigh the circumstances of local privation
