132 BRINGING IN THE SHEAVES

(Nehemiah 1:3), and her magnificent architecture had been reduced to
little more than dusty rubble (Nehemiah 4:2), But Nehemiah had a
mandate (Nehemiah 2:12). lt was a mandate to rebuild the walls,
restore the gates, and remove the reproach (Nehemiah 2:17).

In a miraculous display of cooperation, industry, diligence, and
faithfulness, the people of Jerusalem under Nehemiah’s leadership
were able to succeed (Nehemiah 6:15). Overcoming discouragement
(Nehemiah 4:10), outside opposition (Nehemiah 4:1-8), apathy
(Nehemiah 3:5), and shortages of manpower (Nehemiah 4:16-21),
time (Nehemiah 4:22-23), and resources (Nehemiah 5:1-5), they
demonstrated that obedience to God’s precepts is invariably
undefeatable (Joshua 1:8).

The enterprise of rebuilding the wall was organized around a
private initiative coordination of families, guilds, tradesmen, clergy,
aristocracy, and merchants (Nehemiah 3:1-32). Each group provided
its own unique gifts, skills, resources, and expertise. Individually,
they would have had very little effect on the prevailing climate of
despondency and defeat. But, together, they were able to reclaim
from the rubble their city and their mission. They fulfilled their
mandate.

Several aspects of “the Nehemiah Mandate” and its fulfillment
are especially significant in light of America’s current crisis of priva-
tion and need.

First, Nehemiah’s effort was marked by a constant dependence
on God. Friend and foe alike acknowledged that Nehemiah was a man
of unwavering devotion and that his successes could only be attributed
to the blessing of the Lord (Nehemiah 6:16). He yielded at every turn
to the will of God (Nehemiah 2:12; 5:15; 7:5}. He was constant in
prayer (Nehemiah 1:4; 2:4, 8; 4:4,9; 5:19; 6:14). He was forever
publicly acknowledging God’s sovereignty and providence
(Nehemiah 2:8, 18; 4:14-15, 20; 7:5). And he served selflessly, with
no eye toward personal gain (Nehemiah 5:14-19; 13:15-22). He was a
man of God. In our own day, if we are to meet the formidable
