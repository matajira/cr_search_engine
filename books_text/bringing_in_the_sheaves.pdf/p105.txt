DEVELOPING A MISSION — 105

deacons’ support is critical. Don’t push. Don’t shove. But, by all
means, don’t bypass the deacons.

Seventh, the youth of the church must be enlisted in the work of
missions. Many of the great revivals the church has experienced
throughout history, and many of the great missions movements,
began with the young. But, aside from that very obvious lesson,
church history also teaches us that any effort that ignores the youth is a
short-lived effort, lasting only one generation. That simply won't do
in the case of Biblical charity. [ts complexity and magnitude requires
us to think in multi-generational terms.

The punch-and-cookies approach to youth ministry is a tragic
waste of time, money, and lives. Why not involve the youth of the
church in Biblical charity projects instead? Why not orient the youth
ministry to the service of others’? Why not channel the standard youth
ministry fare of fund-raising, missions trips, fellowships, etc., into
the fulfillment of the Good Samaritan mandate? Why not unleash the
creative and productive labors of Christian kids on problems that
really matter?

After all, if we win the hearts and minds of the next generation,
we've won the future.

Fighth, even counseling can be enhanced by giving it a missions
orientation. It is a common understanding among pastoral counselors
that service to others is the best therapy that a person can engage in.
Many difficulties that Christians bring into counseling sessions have,
as their best solution, discipline, activity, selfless giving, and dedica-
tion. In the work of Biblical charity, people can exercise their spiritual
gifts. They know they are accomplishing something important. Body
life begins to flower spontaneously. Involvement intensifies. What
better way to infect a congregation with the Good Samaritan faith’?
What better way to begin to motivate Christians to action?

Ninth, other media also must be marshalled to the cause if we are
to have committed, convicted congregations forming the framework
for Biblical charity. Though preaching, teaching, worship, the diaco-
