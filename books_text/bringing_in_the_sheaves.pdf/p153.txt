CHAPTER 9

 

Pooling Resources

a Harlem, Harv Oostdyk mobilized dozens of churches from a
Loarety of theological and denominational backgrounds not only to
help the poor and bring the Gospel to the ghetto, but to begin to map
out Scriptural strategies and tactics for the elimination of systemic
poverty altogether. Oostdyk’s STEP Foundation was able to translate
vision into cooperative action. And the impact on Harlem has been
nothing short of phenomenal,!

How did he do it? How was he able to coordinate so many
divergent forces into a cohesive and comprehensive team? How was
he able to enlist ministry teams, task forces, think-tanks, and friend-
ship committees from churches already overloaded and understaffed?

Tn Los Angeles, Bob Hartman spearheaded a campaign to gather
food for the needy during the winter months. Garnering support from
businesses, community service groups, churches, and even local
government leaders, Hartman's Bread Basket was able to raise over
three-and-a-half tons of groceries in less than two months, He was

153
