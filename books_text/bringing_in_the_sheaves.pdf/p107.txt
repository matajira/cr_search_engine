DEVELOPING A MISSION 107

Bits and Pieces

Jeff Wharton was preparing for his next Sunday School lesson. The
opening words of Francis Schaeffer’s classic, A Christian Manifesto,
leapt off the page and gripped his attention: “The basic problem of the
Christians in this country in the last eighty years orso . . . is thal they
have seen things in bits and pieces instead of totals.’’8

“That’s it!” he thought to himself, “that’s the problem. Chris-
tians aren’t going to get motivated, they’re not going to make a
difference in our world if all they ever get is a confused smattering of
facts and issues. We need an overall perspective, a driving sense of
mission that pervades all of life and spirituality.”

Throughout the rest of the week, Jeff went to work mapping out a
strategy to inform and motivate the people in his church. He contacted
the pastor and several of the deacons. He set up a few preliminary
meetings. He made arrangements to rent a series of films froma local
Christian distributor, And he began some serious work on bringing
focus to his Sunday School teaching.

Before long, Jeff's church had begun to implement a compre-
hensive missions strategy. An outreach to the poor had been initiated
and a study group was investigating ways to address the problem of
abortion. “Things are finally beginning to happen,” Jeff said, “and 1]
couldn't be happier. The families in this church will never go back to
the old piecemeal approach to issues. We're ready to take on the
world!”

Summary

In order to facilitate Biblical charity, we must mobilize entire con-
gregations, educating and motivating them to get involved. We must
develop a sense of mission in our churches.

The first step in this process, of course, is to nurture a Biblical
worldview, because right thinking precedes tight action. We must
study the Bible with an absolute certainty that it provides a plan for
every area of life, including a viable model for charity. Only then can
