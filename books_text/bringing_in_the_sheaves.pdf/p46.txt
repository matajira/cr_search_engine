46 BRINGING IN THE SHEAVES

impoverished. Illegitimate pregnancies are generously gratiated
while moral purity is snubbed. Something went wrong.

Third, the war on poverty actually provided incentives to avoid
work. Each increase in welfare bencfits over the last twenty years has
resulted in a huge shift from the payrolls to the welfare rolls. When
entitlement programs become competitive with the salaries of lower
or even middle-income families, it is only sensible to expect that
many, especially the poorly trained and poorly educated, will choose
the path of least resistance. In New York State, for example, in 1981,
an hourly wage of $4.87 would have to be earned in order to equal the
welfare benefits available: hourly earnings one-and-a-half times the
minimum wage.*° Who’s going to work in McDonald’s at $3.35 an
hour when they can “carn” $4.87 an hour on welfare? Something
went wrong.

Fousth, the war on poverty actually contributed to the already
enormous problem of govermmental waste. Instead of helping to
reduce waste by returning more and more citizens to productivity, the
welfare programs have proven to be the most inefficient slice of the
budgetary pie. Only thirty cents of each anti-poverty dollar actually
goes to help the poor alleviate their plight.>7 Shocking, but true. The
other seventy cents is gobbled up by overhead and administration. So,
in 1982, for example, $124 billion was spent to reduce poverty, yet
those expenditures reduced poverty by only $37 billion:38 not a
terribly impressive return. In theory, the $124 billion should have
been enough, not only to bring poor households up to the sustenance
level, but also to bring these and all other houscholds up to 25% above
the sustenance level and still have $48 billion left over for other
purposes, such as reducing the deficit.*° Something went wrong.

Fifth, the war on poverty actually reduced the opportunities of
the poor in the open marketplace. Walter Williams, in his brilliant
book The State Against Blacks,“ and Thomas Sowell, in his equally
insightful book Civil Rights: Rhetoric or Reality?,4! have shown
beyond any reason of doubt that many anti-poverty measures decrease
