248 BRINGING IN ‘THE SHEAVES

Green, F. Pratt, 90
guilt, 143

Hall, Virginia, 199
Halverson, Richard, 120
Hamilton, Alexander, 43
handicapped, 111-113
Harficld, Max, 148-149
Harringdon, J. L. G., 161
Harrington, Michacl, 36, 44
Harris, Howell, 67

Harrold, James W., 161
Hartman, Bob, 153-154, 162
Hill, Dr. E. V.. 174

Holdren, John, 127
homelessness, 21, 31-34, 111-113
Hopkins, Howell, 194
Hosmer, Frederick, 152
hospitality, 199-201
Hubbard, Elbert, 166
hymnology, 102

hygiene, 177

India, 134
industry, 47, 168-171
Intensive Infant Care Project, 40

Jackson, Oscar, 175

Jencks, Charles, 197

Jesus People USA (JPUSA), 173
Job Board, 171-173

job opportunities, 46, 171-174
Jones, Edna, 174-175

Johnson, Lyndon B., 39-40, 43
Jordan, James B., 77, 103
Tulian the Apostate, 56

a’Kempis, Thomas, 126
Kennedy, John F., 42, 64
Kipling, Rudyard, 140, 206
Kissinger, Henry, 26
Koselgrave, Wait, 196
Kuttner, Robert, 179

Landess, Tom, 106
Latimer, Hugh, 75

law, 61-62, 63, 86

lease agreements, 201-203
Lester, Mindy, 30, 31
Levitical tithes, 85

Libertarians, 173

loans, 85

Lord’s Supper, 103

love, 60-62

Lowe, Langdon, 77, 89, 129-131,
136

Luther, Martin, 75-76, 119

Lutz, Donald, 35

MacKay, Thomas, 43, 173, 187
Mains, Karen, 199
Mangalwadi, Vishal, 134-135, 136
Mariam, Sabutu, 134, 135, 136
Marshall, Marlyn, 50

Mather, Cotton, 76

McQuade, Charles, 194

media, 105-106, 157
Medicaid, 40, 48

Medicare, 40, 44

Merril, William P., 54

Miller, Calvin, 110

minimum wage, 46, 51

missions strategy, 100-106
Moody, Dwight L., 67
Mormons, 173, 176-177
Mosqueda, John, 182

Mother Goose, 38, 54

Mother Theresa, 67

Mowat, Charles Lock, 167, 168
Mulligan, Shetly, 200

Murray, Andrew, 194

Murray, Charles, 38, 45

Naisbitt, John, 81, 170-171

Nash, Ronald, 23

Nehemiah Mandate, 131-136

Nightingale, Florence, 67

Nixon, Richard, 40

North, Gary, 54, 73, 81, 115, 162,
177, 206

Oberholtzer, Oscar, 131, 136
occupational licensing, 47

Olney, William, 59

Omarta, Darrel, 201

One and the Many, 121-122
Oostdyk, Harv, 153, 154, 158, 162

Parks, Sharon, 202
Passmore, Joseph, 59-60
