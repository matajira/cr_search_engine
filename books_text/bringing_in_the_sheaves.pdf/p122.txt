122 BRINGING IN THE SHEAVES

They are to synchronize their efforts and maximize effect through
cooperation (1 Corinthians 12:7).

The Christian school movement had its genesis when parents
began to take seriously their responsibility to educate their children,
thus raising them “in the nurture and admonition of the Lord”
(Ephesians 6:4). And yet, usually those families did not just strike out
alone. They worked in tandem with other families and with their
churches, pooling resources, sharing expenses, and delegating func-
tions.

Very similarly, families can be coordinated by churches to take
up their responsibilities in caring for the poor. The churches can
maintain the records, initiate the programs, administrate the
resources, and make the referrals, freeing the families to concentrate
on personal ministry, succor, and relief. The churches can provide
security, expertise, and supervision, while the families begin the
arduous task of restoring the needy to self-reliance and productivity.

The church is charged with the responsibility of equipping the
saints (Ephesians 4:12) and motivating believers with a sense of
mission (Titus 3:8). The individual believers and their families are
then to do the work of the ministry (Ephesians 4:12) and to bear the
burdens of others, “thus fulfilling the Law of Christ” (Galatians 6:2).
Any modern model of Biblical charity will see this balance between
the one and the many deliberately implemented from the start: indi-
vidual faithful families and networking convenantal churches serving
in unison to provide hope for the hopeless.

Putting it all Together
Car] Welch was convinced. For some time, he had watched, listened,
and learned. Now he was ready to act. “At first, when the elders and
deacons began to encourage the families of the church to care for the
poor, I was quite skeptical,” he admitted. “But the more I saw and
heard, the more convinced I became.”

He and his wife, Betsy, began by volunteering two evenings a
