22 INTRODUCTION

not just in theory, but in the tough realm of practice.

Much of what follows is the fruit of work toward just such an end
in Houston, Texas, We never had the luxury of sitting back and
formulating our policies and programs at ease. We were in the midst
of a crisis. At one point in 1982, we had between 30,000 and 60,000
homeless, dispossessed poor camped about town in tent cities, living
out of their cars.” At the same time, nearly 15% of our metropolitan
tegion was facing the trauma of unemployment.® Social service
agencies were buried beneath an avalanche of need. We had to do
something. So we did.

We made mistakes. Lots of them. Sometimes we leamed fram
them; sometimes we, for quite some time, didn’t. Eventually, how-
ever, a pragmatic model was constructed that fit both the Biblical
precepts we'd discovered through diligent study and the obvious need
we'd confronted through diligent labor.

What we’ ve achieved in Houston is not the panacea for all social
ills from now till evermore; but it is a start. What we’ve learned in
Houston is that functioning models of Biblical charity are not only
necessary, they are possible. What we’ve learned in Houston is that
small churches, starting with little or no money, little or no resources,
little or no staff, and little or no experience, can put together a
formidable challenge to the modern notion that poverty is a problem
too big for anyone but the government to handle. What we’ ve learned
in Houston is that we can really make a difference in our world, if we
only take seriously our high calling as believers in the Lord Jesus
Christ.

Bringing in the Sheaves is a practical primer for families,
churches, and private enterprises who wish to begin erecting effective
models of Biblical charity all over the country. As such, it is not
primarily theory. It is primarily practice.

What can be done to help the homeless?

What can be done to ease the unemployment crunch?

How can we effectively get food to the hungry, clothes to the
