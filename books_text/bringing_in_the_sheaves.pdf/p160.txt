160 BRINGING iN THE SHEAVES

time, energy, and money into a mutual project? An already existing
shelter can be updated, renovated, adapted, and then run cooper
atively by the coalition. Or, if the need demands it, a new shelter
could be built. But, either way, no one church is forced to bear the full
brunt of such a work alone.

Computer networking is still another joint project that the com-
munity coalition could facilitate. By tapping into a common data
base, each charity outreach can have instant access to a tremendous
amount of invaluable information. Cross-referencing client informa-
tion can help minimize fraud. Common mailing lists can help mobi-
lize work crews during emergencies. Referral lists, commodity
sources, and information pools can help expedite case loads. With
hardware and software prices dropping dramatically and modern
technologies improving at an equally dramatic rate, computer net-
working makes sense, even for the smallest and most informal charity
associations.

Referrals

Even after a local church has organized a task force to oversee the
work of charity, and then, several like-minded churches have united
their various task forces into a community coalition, there are still
some situations toe complex and too monolithic to deal with ade-
quately on a local level. Outside referrals stil} will have to be made.
For this reason, each charity outreach needs to compile a notebook
that lists community agencies, social service programs, United Way
groups, doctors, lawyers, public health clinics, and rehabilitative
services. There are a whole lot of agencies that perform special
services: care for battered women, drug and alcohol recovery clinics,
veterans retraining programs, care for the elderly, homes for abused
children, crisis pregnancy centers, etc. Each charity outreach ought
not only to know what help is available to the needy, but how to
procure it. Are there waiting lists for any of the programs? What about
costs? Are there prerequisites, conditions, and qualifications? Find
