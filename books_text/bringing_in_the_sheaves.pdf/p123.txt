EQuIPPING FAMILIES = 123

week handling calls at the church office, posting jobs on the bulletin
board, and distributing food to the pre-screened applicants. “The way
the program has been set up by the church, families can begin right
away making a difference in the lives of the needy. I, for one, finally
feel as if I’m doing some good. Never in all my days have I had so
many opportunities to share the Lord and to exercise my gifts. Betsy
and J are closer than ever. This whole program is just fantastic. It’s as
if I’m really putting it all together: evangelism, discipleship, compas-
sion, spiritual gifts, cooperation, and family togetherness.”

Summary

Scripture teaches that the family is the primary agent of stability in
society, providing as it does the moral and institutional foundation
upon which all human relations are built. Thus, when the family fails,
society itself fails. Similarly, when the family is supplanted or sup-
pressed in the work of charity, true charity is distorted and ultimately
destroyed.

The family is the best agent for the dissemination of charity
because it is personal, flexible, accountable, reinforcing, and effec-
tive. As a result, the family can perform the task of charity efficiently
and discriminately without welfare bureaucracies, matching federal
funds, or professional humanitarians.

But, in order for the family to accomplish this feat of daring, it
will need some help. In order for the place of the family to be restored
to its proper function in society, the church must once again empha-
size the reformation doctrine of the priesthood of believers. Only
when the church enables the family to comprehend its centrality to the
work of the ministry will any real progress be made toward solving
the great problems of our day. Only then will a balance be forged
between individual liberty and corporate responsibility. Only then
will deprivation and lack meet their match.

Thus, individual faithful families and networking covenantal
churches, serving hand in hand, form the infrastructure around which
