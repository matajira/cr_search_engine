DEVELOPING A MISSION = 93.

tation of external reality.”"! This mental model is, he says, like a giant
filing cabinet. [t contains a slot for every item of information coming
to us, It organizes our knowledge and gives a grid from which to
think. You see, our mind is not blank and our perspective is not open
and objective. When we think, we can only do so because our mind is
already filled with all sorts of ideas with which to think. These more
or less fixed ideas we think with make up our mental model of the
world, our frame of reference. In other words, our worldview.

James Sire tells us, “A worldview is a map of reality; and, like
any map, it may fit what is really there, or it may be grossly mislead-
ing. The map is not the world itself, of course, only an image of it,
more or less accurate in some places, distorted in others. Still, all of
us carry around such a map in our mental make-up and we act upon it.
All of our thinking presupposes it. Most of our experience fits into
it...

One of the basic demands of Christian discipteship, of following
Jesus Christ, is to change our way of thinking. We are to “take captive
every thought to make it obedierit to Christ” (2 Corinthians 10:5). We
are ‘“‘not to be conformed to this world but be transformed by the
renewing of our minds” (Romans 12:2). In other words, we are
commanded to have a Biblical worldview. All our thinking, our
perspective on life, and our understanding of the world around us, is
to be comprehensively informed by Scripture

God's condemnation of Isracl came because “their ways were
not His ways and their thoughts were not His thoughts” (Isaiah 55:8).
They did not have a Biblical worldview. When we begin to think about
the law, or bio-medical ethics, or art, or business, or love, or history,
or welfare, or anything else apart from God’s revelation, we too have
made ourselves vulnerable to condemnation. A Biblical worldview is
not optional. It is mandatory.

So, how do we develop a Biblical worldview? How do we go
about replacing our old ways of thinking with God’s way of thinking?
How do we go about helping others develop such a Scriptural outlook
