Questionable Answer

“God predestines the church as a whole to believe in
Christ and to carry out its holy calling. But God never
predestined individual members. At the most, He pre-
destined a specific number of believers over time, but
He selected them in terms of His original knowledge of.
whether they would exercise faith in Christ. Men can
reject the offer of salvation. God does not force them
to believe.”

My Reply: The purpose of God is to separate out
from fallen humanity a remnant, His people. They are
to have a holy calling specified by God. But to achieve
His purposes, God must specify each individual's call-
ing. He has planned to achieve certain goals, many of
which were announced by God to His people in the
form of prophecies. Won't God see to it that His proph-
ecies come to pass? Doesn't God make His prophe-
cies come to pass?

The Bible doesn't say that God predestined a group
in general apart from predestinating members in par-
ticular. The Bible speaks only of God's granting His
grace to us in Christ, irrespective of any works we
perform. How can we talk about a work of faith or a
work of unbelief as the foundation of salvation or dam-
nation? Isn't the issue God's sovereign grace? We are
saved by grace through faith (a gift of God), not by
works,

If man is saved by his own autonomous work of
faith, then the lie of Satan is true: man is partially au-
tonomous; he has become a little god.

For further study: | Ki. 8:58; Jer. 31:3; John 8:36, 42; Eph. 2:1-10; It
Cor. 4:6; Titus 3:54.

60
