Questionable Answer

“Everyone knows thai such an act is immoral. The
New Testament writers assumed that Christians would
know this. The light of redeemed wisdom shows this to
all Christians. The Spirit would not lead Christians to
do such things. We are morally bound, not legally
bound, to obey such a prohibition.”

My Reply: Are we bound to obey this law because it
is a universally recognized evil practice? ti certainly
was not recognized as evil by the Canaanites. And if
Christians should gain political influence, shouldn't
they make such practices illegal, so as not to have the
land vomit out an entire population?

Are we bound by this law? {f so, why? Because it is
“logical’? Logical for whom? 8y what standard? Or
because “all righteous men know not to practice such
acts”? But how can we measure righteousness apart
from the prohibition?

Hf we are bound by this law, then why are we sup-
Posedly not bound by alt Old Testament laws? What
about marrying your sister or brother, or an aunt or
uncle? The Old Testament prohibits such marriages
{Leviticus 18:6-18); the New Testament doesn't. If
God’s word isn’t authoritative, both Old and New Tes-
taments, then what is? The temporary morality of
some temporary nation of church? The temporary
morality of mankind? How can we understand right
and wrong if we exclude.the Old Testament's legal
Precepts? How can we avoid humanism's “situation
ethics”? How can.our nation avoid God’s judgment?

 

   

For further study: Lev. 22:31-33; Matt. 5:17-20; 23:23; Rom. 8:34.

90
