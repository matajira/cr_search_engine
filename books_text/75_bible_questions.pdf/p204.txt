200 75 BIBLE GUESTIONS

have two responsibilities: keep on reading in the field
you agree with, and continue studying the field you
don't agree with. You have to come up with solid
biblical reasons why | have completely missed the
point in one area while hitting the target in another
one. Unquestionably, you need to become an expert
in the field you do agree with, because you're going to
be “under the gun” from your friends and institutionat
superiors from now on.

If you agree with some questions within a section,
but not others, you had better find out which of us is
being inconsistent: you or I. It’s difficult for me to be-
lieve that each of the three sections isn’t internally
self-consistent, but maybe I’m wrong. Keep studying.

For anyone who is just confused, you had better
keep thinking about these questions until you get “un-
confused,” one way or another.

If You Agree With the 75 Questions

Now, for those of you who are convinced that my
case in all three areas is essentially correct, you are in
trouble. You have now adopted theoiagicat positions
which are decidedly out of favor—hated might be
closer to it—by the vast majority of those who call
themselves Christians. (As for the opinion of human-
ists, you are officially “off the wall.”)

You now have a number of responsibilities. First,
you must recognize that you have been given knowl
edge which few Christians in our day have ever heard
about, let alone considered. Most of your peers will
disagree with you, including those few who have seen
or even read this book. Second, with greater knowl-
edge comes added responsibility: “For unto whomso-
ever much is given, of him shall be much required”
