“Being freed from sin means being freed from the spe-
cific details of Old Testament law. We are adults now;
we don’t have God watching over our shoulders in
order to punish us for avery infraction of His law. We
Gan now obey the Spirit.”

My Reply: Does the Holy Spirit tell us to do things
that were forbidden by Old Testament law? If so, then
was ‘the law imperfect? Rather, shouldn't we argue
that the Holy Spirit tells us to obey the law in its entirety,
unless a specific application of the law has been
altered by specific revelation in the New Testament? It
the New Testament hasn't specifically told us of a dif-
ferent application—for example, faith in Christ's past
death and resurrection (the Book of Hebrews) rather
than faith in slaughtered animals as a testimony of
our faith in the coming sacrifice of the Messiah (Isaiah
53)— how can we trust our hearts to guide us if we do
not have the law as a guidetine? If our hearts are not
faw-disciplined and our actions are not therefore law-
disciplined, how do we know when we are pleasing
God? By “good feelings” concerning our actions?
Then how are we different from the modern pagans,
who say, “If it feels good, do it’?

Mature, responsible, self-disciplined adults don’t
abandon the righteous teachings of their parents
when they leave the home of their parents. Rather,
they apply what they have learned. They keep the laws
their parents gave them as children, but they keep
them as adults, not as. children.

For further study; Deut. 10:12-13; Ps. 119:1-16; Ezek. 18:1-32; Mic.
6:8.

112
