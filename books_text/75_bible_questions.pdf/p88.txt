 

64 75 BIBLE QUESTIONS

“Murray, John. The Epistle to the Romans. Grand Rapids:
Eerdmans, 1965.
. The imputation of Adam’s Sin. Nutley, Nu:
Presbyterian and Reformed, 1977.
. Redemption—Accomplished and Applied.
Grand Rapids: Eerdmans, 1955.

North, Gary. Unconditional Surrender: God's Program for Vic-
tory. 2nd edition; Tyler, Texas: Geneva Divinity’ Schoot
Press, 1983, Part I.

Owen, John. The Death of Death in the Death of Christ. Reprint
edition; London: Banner of Truth, 1959. (mid-17th century)

Packer, .J.l. Evangelism and the Sovereignty of God.
Chicago: InterVarsity Press, 1961. (Anglican theologian)

Pink, Arthur. The Sovereignty of God. London: Banner of
Truth, 1961. (Baptist:theologian)

Robertson, Norvelte. Church Members’ Handbook of
Theology. Box.1094, Harrisonburg, VA: Sprinkle Pubs.,
(1874). (Southern Baptist theologian)

Shedd, W. G. T. Dogmatic Theology. Reprint by Thomas
Nelson Sons. (late 19th century)

* Steele, David N., and Curtis C. Thomas. The Five Points of
Calvinism: Defined, Defended, Documented. Phillipsburg,
NJ: Presbyterian and Reformed, 1963.

Van Til, Cornelius. Christianity and Barthianism, Philadel-

phia: Presbyterian and Reformed, 1962.
____. A Christian Theory of Knowledge. Philadelphia:
Presbyterian and Reformed, 1989.
. The Defense of the Faith. Philadelphia: Pres-
byterian and Reformed, 1967.
~—___ - Ant Introduction to Systematic Theology. Phil-
lipsburg, NJ: Presbyterian and Reformed, 1978.
. Psychology of Religion. Phillipsburg, Nu: Pres-
byterian and Reformed, 1971.
Wartield, Benjamin B. Ca/vin and Augustine. Reprint edition;
Philadelphia: Presbyterian and Reformed, (1932) 1956.
. The Plan of Salvation. Grand Rapids: Eerd-
mans, 1955.

 
