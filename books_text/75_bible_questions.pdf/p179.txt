Why Shouidn’t Christians
Become Civil Rulers and
Enforce Goa’s Law?

For rulers are not a terror to good works, but to the
evil. Wilt thou then not be atraid of the power? Do
that which is good, and thou shalt have praise of the
same. For he is the minister of God to thee for good.
But if thou do that which is evil, be afraid; for he
beareth not the sword in vain: for he is the minister
of Gad, a revenger to execute wrath upon him that.
doeth evil (Romans 13:3-4).

God has ordained the institution of civil government
as a monopolistic agency which imposes physical
sanctions against evil-doers. The civil magistrate is
His minister of vengeance. Civil government is an in-
escapable concept, it is never a question of civil gov-
ernment vs. no civil government. It is always a ques-
tion of which kind of civil government. Pure anarchy is
not a possibility. Someone or some agency will always
bear the sword of physical judgment.

Civil government is not supposed to make men
good. It is not an agency of regeneration. It is minister-
jal. Thus, its task is to restrain certain kinds of public
evil acts by all people, regenerate or unregenerate. lis
task is to be a terror to those who would commit evil
acts.

How should a civil government detine publicly pun-
ishable evil acts? By what standard? Clearly, by Goa’s
revealed iaw. Which citizens are best equipped to
make this determination? Humanists? Or Christians?

175
