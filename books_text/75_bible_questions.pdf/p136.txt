 

Questionable Answer

“We martify the deeds of the body by avoiding evil
acts. The Spirit tells us what is wrong, and then He
gives us the power to avoid sin (I Corinthians 10:13).
By taking heed of the details of Old Testament law, we
become carnal again, submitting ourselves to the law
that kills”

 

My Reply: What has Paul argued? Not that the law
of God kills the regenerate, but that it allows sin to kill
the unrighteous. The law of God he calls holy, just,
and good (7:12). How can something written by God,
declared to be perfect (Psalm 19:7), and.described by
Paul as holy, just, and good,.be the pathway back to
domination by the carnal mind? On the contrary, Paul
describes it as a means of subduing the evil deeds of
the flesh, but a means that can be used successfully
only by regenerate people. Gad’s law is our guideline
that tells us the step-by-step program of Christian
self-government: God's program of progressive per-
sonal sanctification.

Paul calls the Holy Spirit “the Spirit of adoption’ |
(Romans 8:15). This is the biblical doctrine of ethical
adoption. Alt men are sons of God in terms of God’s
creation (Acts 17:26)—disinherited sons as a result of
Adam's sin. But Christians are adopted sons. “The
Spirit itself beareth witness with our spirit, that we
are the children of God” (Romans 8:16). How do we
test this Spirit? God’s law.

Yet there are Bible teachers who tell us that God's
law no longer is a valid test of the spirits. Amazing!

For further study: Ps, 119:153-176; | Cor, 9:24-27; Col. 3:1-10; Titus
241-12.

 

132
