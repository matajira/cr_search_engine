Questionable Answer

“Satan and his followers are already in hell. While
humans live, they can escape this judgment. Satan
can no more escape judgment today than a sinner will
be able to after he dies.”

My Reply: All this is true, but totally irrelevant. The
question is: Could Satan have repented after his rebel-
lion? Man is given an opportunity to repent after his
rebellion, but what about Satan? The Bibie never
even hints at such a possibility. Is God unjust, there-
fore, in refusing to give Satan a “chance” after his re-
bellion? Have you ever heard a Christian say so?

God offers the gospel to fallen humans, but apart
from their prior regeneration, they cannot respond in
faith (Question 19). So what is the difference in effect?
An offer of salvation which cannot be received by any
rebellious person whom God chooses not to regener-
ate is not any different in its eternal effects from eter-
nal punishment apart from an offer of salvation.

The issue here is the justice of God. Is God unjust in
His treatment of Satan and his angelic followers? if
not, then how can anyone legitimately conclude that
God is unjust in damning any ethical rebel whom He
has chosen not to regenerate? The issue here is the
justice of condemning any rebel inescapably to etemal
punishment without a prior possibility of his repenting. if
such inescapable condemnation is fully justified in
Satan’s case, why not in the case of human rebels?

And if it is unjust in the case of Satan’s human fol-
towers, don’t we have to argue that it is also unjust of
God in Satan’s case? Satan wouid /ove that argument!

 

 

For further study: Acts 5: I Tim. 2:25-26.

 

48
