Introduction to Part i

The question of the law of God has been controver-
sial in Protestant circles for over a century. Most
churches today believe that the laws found in the Old
Testament are no longer binding. This has not always
been the belief of Christians. The Puritans of New
England established their colonies specifically in
terms of their belief in the continuing validity of the
laws of God. Certainly, Christians have affirmed their
faith in the so-called mora! laws of God. But in the late
nineteenth century, in certain fundamentalist circles,
people began to abandon any faith in the continuing
validity of Old Testament faw. In fact,. many prominent
pastors and theologians have actually said that it is i
legitimate to preach the Ten Commandments in New
Testament times, One such pastor was the Presbyter-
ian leader of the 1850's, Donald J. Barnhouse.

Does the New Testament teach such a doctrine of
law? Does the New Testament abandon Old Testament
law? That's what this section of the book deals with.

Let me ask you a question: When you hear the
words, “the principles of the Bible,” or “the moral
teachings of the Bible,” don’t you instinctively think to
yourself, “the /aws of the Bible”? If the principles are
binding, aren't they taws? A iot of Christian teachers
are “fudging” by switching words on you. They claim
that “we're not under the law,” yet they try to take a
stand against immorality by proclaiming the validity of
a group of biblical principles. But what specifically are
these principles? Where do we find them, if not in the
Bible? How can we even define “immorality” if we re-

8?
