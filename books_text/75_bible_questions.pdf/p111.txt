Question 30

How Can We Know if
We are “Dead to Sin” if
We Ignore God’s Law’?

What shalf we say then? Shall we continue In sin,
that grace may abound? God forbid. How shall we,
that are dead to sin, Ilve any longer therein?
(Romans 6:1-2).

Paul argues in Romans § that by the sin of one
man, Adam, sin entered the world, and death by this
sin (v. 12). Without law, sin is not imputed te man (13),
Paul says, but since death reigned from Adam until
Moses (14), we know that the law existed and was
binding. God gave men the law, that man’s offense
“might abound,” and therefore that the grace of God
might abound even more (20). In other words, the law
points out the deadly nature of our sin. Our sin is in
every area of life, in the whole fabric of our being.
God's law is therefore comprehensive, in order to re-
veal to us just exactly how evil we are in aiff areas of
life.

Should we sin therefore that grace might abound?
No! Christians are dead to sin. The knowledge of sin
killed Paul, he says, and it was the law that brought
him. this necessary knowledge (Romans 7:9-11).
Christ then raised him from the dead.

We dare not live in sin, he says (6:2). But how do we
know when we are jiving in sin? The same way we
knew, as unregenerate people, that we were dying in
sin before: by God's law. We are dead to sin because
we can now escape its deadly effects.

107
