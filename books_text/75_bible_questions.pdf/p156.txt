Questionalla Answer

“We are not to understand this parable as a complete
description of history. It only points out that there is
good and evil until the day of judgment.”

My Reply: What is noticeable in the parable is how
forcefully the owner of the field resists the idea of remov-
ing the tares from the wheat. The idea is to get the tares
out of the field, said his servants. No, the husbandman
corrected them, we must protect the wheat. If you try to
uproot the tares prematurely, you will uproot some
wheat. Let both tares and wheat develop to full maturity,
and then the reapers (angels) can harvest the tares
first and burn them, leaving the wheat intact.

What is not even considered in the parable is the
idea that the wheat should be removed from the field,
leaving the tares to grow without competition! The
servants were not so foolish as to suggest that the
tares should receive the benefits of being cared for in
the field, while the opportunity to mature is removed
from the now-uprooted wheat. Yet it is this latter vision
which has been preached by many Bible teachers.
They teach that Christians will be removed miracu-
lously from the “field” (the world), while the tares go
on growing. In shart, the salt will be removed—the
city on a hill, the light that shines before men. The
ethical testimony of the church will be gone. Jesus’
parable absolutely denies such teachings!

Since Jesus taught this, why do Christians refuse
to believe it?

For further stu
6:39-40, 44, 54; 11:

   

Dan, 12:2; Matt. 25:31-32; John 5:27-29;
15; Rev, 20:13.

152
