Aren’t Christians Supposed
to Crush Satan?

 

And the God of peace shall bruise Satan under your
feet shortly. The grace of our Lord Jesus Christ be
with you. Amen (Romans 16:20).

Paul wrote to the church at Rome that in a short
period of time, they would see Satan bruised under
their feet. Obviously, Paul was speaking figuratively.
They did not as a group collectively step on the head
of a physical serpent. What they did was to begin to
participate in a collective, church-wide battering of
Satan’s kingdom. This was yet another stage of the
fulfilment of that crucial prophecy to Satan: “And | will
put enmity between thee and the woman, and be-
tween thy seed and her seed; it shall bruise thy
head, and thou shalt bruise his heel” (Geriesis 3:15).

The warfare between Christ and Satan is ethical.
As the two armies meet each other on the battlefield,
the Christians have access to the critically important
training manual for ethica! war: the Bible. This gives
Christ's forces an overwhelming potential advantage.
The probiem arises when His troops refuse to honor
the specific commands in God's ethical battle plan. His
people are driven from the field repeatedly, for they
lose skirmishes. Why do they lose? Because they re-
ject the explicit teachings of God concerning Himself,
man, law, the church, the family, the civil government,
and all other areas of responsibility that the Bible deals
with. But the church at Rome could rest confidently.
They knew that they would experience a victory.

165
