Question 69

Won’t Men Live Longer
as God’s Kingdom
Progresses?

Fer he must reign, till he hath put all enemies under
his feet. The last enemy that shall be destroyed is
death (1 Corinthians 15:25-26).

The parable of the wheat and the tares stresses con-
tinuity. So do the other parables of the kingdom in Mat-
thew 13. But can we believe in continuity—progressive
sanctification—in the area of biology? Yes, death will
be put down, once and for all, at the end of time. But
this is a radically discontinuous event. If we expect pro-
gressive sanctification culturally, economically, and so
forth, as God's blessing for the enforcement of His jaw,
shouldn't we also expect to see a steady lengthening of
human fife’ spans, as a testimony (“earnest”) of the
coming conquest of death? Do biblical ethics and long
life go together?

“Honor thy father and thy mother: that thy days
may be fong upon the land which the LORD thy God
giveth thee” (Exodus 20:12). Paul cited this passage
and remarked that this was the first of the command-
ments with a promise attached to it (Ephesians 6:2).
This law and its promise stil! hold.

Will tong life be universal, in response to the expan-
sion of God’s kingdom? This is what isaiah prophe-
sied: “There shail be no more thence an infant of
days, nor an old man that hath not filled his days: for
the child shall die an hundred years old; but the sin-
ner being an hundred years old shail be accursed”
(65:20). Biblical ethics and long life do go together.

181
