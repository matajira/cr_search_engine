“ORDER BLANKS

1am including a number of order blanks that
you can use to get on mailing lists, find out about
important programs, and buy materials. These
organizations are hard working, geared to Chris-
tian reconstruction, and aimed at intelligent lay-
men. They all deserve your consideration. |
strongly recommend that you cut out or tear out
these pages and mail them to the various organi-
zations.

If you prefer not to tear out these sheets,
photocopy them and mail them in, or just.mail in
a card,

Warning: if you fail to use the aids this book
provides, you haven't understood this book.
Knowledge should lead to action. Be ye doers of
the word, and not hearers only (James 1:22).

Gary North
