Annentix B
The Bible
Study

I's not enough to ask questions in a classroom. To
develop your perspective and your knowledge of
Bible texts, you need continuing study.

The trouble is, very few people ever develop sys-
tematic Bible study programs on their own, and then
stick with them. People say they will, but they don't.
So we all need outside support. Where self-discipline
is lacking, we need to find other ways to discipline
ourselves. This is especially true of mental and spirit-
ual affairs.

You also need to consider the very real possibility
that the questions raised in this book cannot be an-
swered successtully by your instructors. in fact, the
operating premise of this book is that they can’t an-
swer them. Their theology is not in line with what the
Bible says. You may think I'm exaggerating. | don't
know your school and your instructors. Fine; fest my
hypothesis. Start seeking answers to these questions
in class. lf you get stonewalled, or if the answers are
evasive, or if the answers just don’t come to grips with
the text, then you know: you need another source of
help. The obvious place to begin looking for assis-
tance is among your fellow students. All of you are
there (supposedly) because you're interested in learn-
ing. Anyway, the brighter students are there to learn.
So why not search out other students to meet with on
a regular basis? Select a problem area and study it.

This is what students in the better law schools do.
221
