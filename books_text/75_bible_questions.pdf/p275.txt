APPENDIX C: HELP FROM THE ACCREDITATION ASSOCIATION 271

Conclusion

What you want is a continual stream of students
who are protesting injustice and appealing through
the local college’s court of appeal. The Trustees are
going to get tired of hearing about it. Eventually, the
accreditation association will start an investigation.
They also get tired of letters. Remember the words of
Jesus:

And he spake a parable unto them to this end,
that men ought always to pray, and not to faint,
saying, There was in a city a judge, which
feared not God, neither regarded man. And
there was a widow in that city; and she came
unto him, saying, Avenge me of mine adver-
sary. And he would not for a while; but after-
ward he said within himself, Though | fear not
God, nor regard man; yet because this widow
troubleth me, | will avenge her, lest by her con-
tinual coming she weary me. And the Lord
said, Hear what the unjust judge said (Luke
18:1-6).

There are unjust judges in this world. There are
also people with a righteous cause. It is the réesponsi-
bility of the righteous people to keep hammering
away at the system until even unjust judges render
righteous judgment. So if your judges claim to be
righteous men, they ought to render righteous judg-
ment early. If they don't, it's your responsibility to fight
the good fight.
