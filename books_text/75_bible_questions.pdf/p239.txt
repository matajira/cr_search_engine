APPENDIX &: THE BIBLE STUDY 235.

spend three weeks or a month on each chapter. That
would just about fill an academic year. Order for $9.95
each (or three for $25) from

Geneva Divinity School Press
P.O. Box 8376
Tyler, Texas 75711

| list other materials at the end of 75 Bible Ques-
tions. Consult this list. There is also an extensive
book list at the end of Unconditional Surrender.

Here is my recommended program for each Bible
study. First, get a cormmitment from each participant
to spend a half hour in preparation before each Bible.
study. Agree in advance what questions or topics will
be covered the next week. This requires discipline.
What doesn’t?

Second, open each meeting with prayer. Always
use the Loro’s prayer: Matthew 6:9-13 or Luke 11:2-4.
No exceptions. If you don’t open with this prayer,
please don’t use this book or Unconditional Sur-
render in your Bible study. | think ail group members
should repeat it verbally. Read from both of the
selected texts of the Lord’s prayer, if necessary (the
two versions - differ slightly). | also recommend
reading from at Jeast one psalm.

Third, if you selected a leader of the discussion dur-
ing the previous meeting, have him ask questions.
Questions are good ways to begin study. Read a Bibie
verse, and then ask questions. You can cover no
more than five questions, in ail likelihood, during a
one-hour session. With prayer and opening remarks,
that’s only about ten minutes per.question.

Fourth, bring different translations of the Bible, and
Greek and Hebrew texts if one member or more can
translate. Don’t make the mistake of drawing some
