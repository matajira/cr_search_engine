Does God’s Absolute
Predestination Make
Him Unfair’?

‘Thou wilt say then unto me, Why doth he yet find
fault? For who hath resisted his will? (Romans 9:19).

Paul was a logician. He knew exactly what confused
{or rebellious) men.would ask themselves, once he had
described the hardening of Pharaoh's heart by God.
The person who was unwilling to accept the truth of
Pauls words would reply: “You are saying that God
didn't give Pharaoh a fair shake. Pharaoh might have
repented. But God didn’t allow him to repent. What
kind of God is that? You say He is omnipotent. Then
He isn't fair. Who could resist the will of an omnipotent
God? But such a Gad is not a God of ethics, for He re-
stricts men’s ethical behavior. | don’t befieve in your
God. In other words, Paul, you have misrepresented
God. You have borne false witness against Him. God is
either righteous or else He is sovereign—a predestina-
ting God. But He cannot be both. | prefer to believe in a
righteous God who grants men autonomous free will.
Better a world of chance than a predestined world.”

Paul's response to such logic is offensive to autono-
mous men: “Nay but, O man, who art thou that re-
pliest against God? Shall the thing formed say to
him that formed it, Why hast thou made me thus?
Hath not the potter power over the clay, of the same
lump to make one vessel unto honour, and another
unto dishonour?” (vv. 20-21). In short, be silent, com-
plainer! God is sovereign. Do not raise a logical para-
dox: “righteousness vs. omnipotence.”

21
