 

Questionable Answer

“The meaning of ‘elect’ is not what you think. To be
elect means that you have only a chance for eternal
life after Christ regenerates you. Nothing is certain,
however. When God says ‘Who is he that condemn-
eth?’ He doesn't mean that a regenerate man can't
decide to become unregenerate and thereby con-
demn himself. Regenerate man always has the option
of condemning himself by sinning,” °

 

My Reply: In other words, / am powerful enough to
condemn myself, even though Christ makes interces-
sion for me to God the Father. My power is so great
that | can overcome the election of God and the in-
tercession of Christ. God grants me this power, of
course, but | do possess it. And therefore Satan can
tempt me the same way he tempted Adam and Eve.
He can appeal to my autonomy—my God-granted
limited autonomy—in order to get me to condemn
myself. He can thereby snatch me away from the
electing power of God. Christ's intercession fails
again.

Why does God not honor the prayers of His Son?
Are Christ's prayers half-way prayers? Aren't they
meaningful, honest prayers to preserve His elect fram
disgrace? If Satan has the power to reverse God's
very election of His people, in the face of Christ's in-
tercessory prayer, then what confidence can Chris-
tians have in their own prayers? Are God's answers to
prayers random? If autonomous man can overcome
Christ’s prayers, then hasn't Satan defeated Christ?
Hasn't Satan destroyed’ the power of prayer?

 

For further study: John 10:28-30; Rom. 11:2, 5-7; il Thess. 2:13.

 

30
