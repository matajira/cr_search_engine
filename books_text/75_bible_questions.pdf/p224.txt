220 75 BIBLE QUESTIONS.

have a perfect right to come back to him in private
with a written list of a dozen more questions that were
raised in your mind by the recommended book. What
about this? What about that? If the author believes
this, then he has to believe thus and so, But thus and
so isn’t taught in the Bible. In fact, its opposite is expli-
citly taught. is there anything else you can read?
Does he have any answers of his own? If you have
written up good questions, with Bible citations, you're
reasonably well covered. And now stonewalling be-
comes almost impossible. As Nixon's senior staff man
Haldeman so aptly put it, “When the toothpaste is out
of the tube, it’s almost impossible to get it back in.”
Your instructor has already hit you with his big guns.
He is now totally on the defensive.

Conclusion

Its easier to get these questions answered in Bible
Classes than in electrical engineering. The social
sciences and history jend themselves better to a dis-
cussion of basic theological issues than the hard
sciences do. A class in philosophy should get these
questions answered; if it doesn't, then there is some-
thing wrong with the class.

To repeat: the best way to get a question answered
is when a related issue comes up in class during a
lecture. This is why you should master the questions
and think about their implications and applications.
They are worth mastering, after all. And they do have
enormous implications and applications.
