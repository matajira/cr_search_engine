230 75 BIBLE QUESTIONS

If you don’t seek public power, there is almost no
way for an outside bureaucracy to police your group.
Se avoid seeking public power. Just go to your Bible
studies. Learn. Don’t make organizational waves. But
make ail the theological waves you can get away with.

What can the administration do, cancel all Bible
studies on campus? When word of this gets out to the
donors, it will look foolish. If it gets to the secular
press, it will look preposterous. (But | don’t recom-
mend that you go to the press io tell the story first.)

But if they try to cancel three-person Bible studies
only, that would appear to be an infringement on your
Constitutional liberties. They are singling out a cer-
tain kind of Bible study, but allowing other students to
meet to study their Bibles. That administration has
just delivered the school into the hands of students
who can choose to pursue the matter in public.

3. How Long to Meet

If you're bored with Bible studies, the answer is
easy: no hours per week. But if you get really in-
terested, then you could short-circuit your academic
career by using attendance at a Bible study as an ex-
cuse for neglecting your formal academic studies.

You have begun an academic career. You must
honor Jesus’ warning concerning those who begin
projects: “For which of you, intending to build a
tower, sitteth not down first, and counteth the cost,
whether he have sufficient to finish it? Lest haply [it
happen], after he hath laid the foundation, and is
not able to finish it, all that behold it begin to mock
him, saying, This man began to build, and was not
able to finish” (Luke 14:28-30). If you did not count the
cost beforehand, you had better count it now.
