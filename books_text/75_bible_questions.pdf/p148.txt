144° 75 BIBLE QUESTIONS

in the millennium, in other words.) The kingdom of hu-
manist man will triumph.

Which position is correct? Neither.

What the Bibie teaches is that God’s kingdom, how-
ever imperfect, will be established on earth pricr to
the return of Christ. This kingdom will be visible in-
Stitutionally. It will be empowered by grace, for large
numbers of people will be converted to faith in Christ,
though not everyone need be converted in order to
have this visible kingdom established. it will be
marked by the rule of God’s law. It won't be a central-
ized, top-down authoritarian system over people who
resist Christ's rule, for the bulk of the population will
voluntarily submit to the rule of Christ’s principles in
every area of their lives.

This will happen before Christ returns physically to
render final judgment. Remember, the wheat and the
tares grow together in the field until the day of final
judgment (Matthew 13:3-8, 18-30). So there will be a
millennium of peace. There wil be a rule of Christ's
law on earth before the final judgment. There are ex-
plicitly biblical standards for a Christian society as
well as for Christian individuals. in short, there really
is hope. Our good work today wiif make a difference
tor Christ's Kingdom.

The humanists deny ail this. They believe that the
teachings of the Bible will become ever less impor-
tant. Too many Christians agree with the humanists.
They are therefore theologically and intellectually
compromised. The Bible says that the gospel will be
victorious, that it will eventually conquer all institu-
tions. If you think I'm incorrect about this, read the
next 25 questions.
