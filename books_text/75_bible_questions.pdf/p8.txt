4 75 BIBLE QUESTIONS

ers have taught me since my youth. They couldn't
have been wrong about these matters.” Yes, they
could have been wrong. In fact, they were wrong,
which is why for over a‘century in the United States,
conservative, Bible-believing Christians have allowed
the secular humanists to take over every area of life:
education, psychology, entertainment, business, poli-
tics, the arts, communications, the media, and on and
on. Because Christians have held to ‘humanist pre-
suppositions in certain key areas, they allowed the
secular humanists to take over by default.

| want Christians to reconquer this ‘ost territory. 1
think it's possible, and | think God expects it. | have
shown how we can. begin to do it in my little paper-
back book, Backward, Christian Soldiers? (Institute
for Christian Economics, P.O. Box 8000, Tyler, TX
75711; $4.95). But Christians can’t do it if they share
humanist presuppositions with the humanists. Third-
rate humanists can never defeat first-rate humanists.
Christians must purge out all traces of humanism be-
fore they can successfully battle humanism.

Why Questions?

There is an old slogan that says “one picture is
worth ten thousand words.” | have a variation: “One
geod question is worth fifty assertions.” (And 75 good
questions are worth... ? Let's see...)

A well thought-out question opens many possibili-
ties. People who react negatively against an assertion
may be willing to think through a good question. If you _
ask a question properly, it-can lead another person to
consider a whole new set of ideas. That's why Socra-
tes developed the so-called Socratic method. That's
also why Jesus used questions to disarm his oppon-
ents, as well as to lead His followers inta the kingdom.
