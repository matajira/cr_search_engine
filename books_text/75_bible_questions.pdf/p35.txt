Question 8
Are We LESS
Than Conquerors?

Who shali separate us from the love of Christ? Shall
tribulation, or distress, or persecution, or famine, or
nakedness, or peril, or sword? As it is written, For
thy sake we are killed all the day long; we are ac-
counted as sheep for the staughter. Nay, in all these
things we are more than conquerors through him
that loved us (Romans 8:35-37).

Can we fall from grace? We will no doubt be tempted:
by tribulation, distress, persecution, famine, nakedness,
peril, or sword. You can understand why Paul wrote
this: passage to the church in Rome. But what is his
answer to the question? No! In all these things we are
more than conquerors.

Here it is again: af these things. Again, do all things
work together for good to them who are called accord-
ing to God’s purpose? Yes, they certainly do. But how
can God guarantee this? How can He guarantee this
victory, if He has reserved to man and Satan areas of
potential rebellion by which His elect can become
ethical rebels, cursed of God? After all, the potential
can become actual. This is what “potential” means.

Is going to hell what Gad means when He promises
His peopie good? A most peculiar meaning of “good”!
But if they cannot go te heil, then they are truly elect,
truly guaranteed the victory. They can be persecuted,
but not separated from Gad’s love. If God’s elect can
perish, then where is God's victory?

a
