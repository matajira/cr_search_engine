Questionable Answer

“God does receive glory from His punishment of all
sin, and even sinners. Nevertheless, God is not the
author of sin. To say that God makes evil men for His
own purposes is the same as saying that God is the
author of sin. The text means only that God is glorified
despite evil men.”

My Reply: Is this what the text says? Doesn't it say
that God is the Creator who makes aif things for Him-
self? Does it imply. that sin is the result of a “cosmic
accident,” over which God had no control, ar chose
not to exercise any control—an event that thwarted
His plan for the ages? Was Satan successtul in thwar-
ting God’s plan? Or shall we argue that God has no
comprehensive plan for the ages? And if we argue
this way, what explicit Bible references will we appeal
to in order to prove our point?

Are we using human logic —“God is not the author
of sin, and therefore sin was never a part of God's
plan”—in order to reject what the text says? Are we
using human fogic—“sin could not be part of the plan
of God unless God is the author of sin"—to demon-
strate that Satan and his followers autonomously
overcame God's hopes and plan for the ages? Have
we elevated Satan to a position of “almost God” in
power—a being who.can, has, and may continue to
thwart God’s plan? Are we hiding in logic from the
teaching of the Bible? Can we trust anti-revelational
logic?

 

For further study: Gen. 45:
Amos 3:6; Acts 3:18; Rom. 8:28; 11:36.

 

; Ps. 76:10; Isa, 45:7;

 

"46
