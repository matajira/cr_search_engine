APPENDIX B: THE BIBLE STUDY 231

You can afford an hour a week, plus a half hour of
preparation for each meeting. Look at all the hours
you waste. Get up ten minutes earlier, six days a
week, and you have gained your “lost” hour. Get up 20
minutes earlier and gain two. That's all it takes. Two
hours a week. Then take an extra hour from some
other activity to get your two half-hour pre-meeting
Preparations.

The first hour is spent with the group which
recruited you. The second hour is spent with the
group you recruited. That's it. If this flunks you out,
you don't belong in college.

Always adjourn a meeting after one hour. It doesn't
matter how interesting the discussion is. If you must
continue, schedule an extra meeting the next even-
ing, or whenever. But people must learn to plan their
study schedules. This is Bible study, not a gab-fest.
This program is part of a systematic restructuring of
your study habits. You need discipline. The time ailot-
ted to the weekly meeting should reflect this disci-
pline. Besides, after an hour's discussion, you simply
will not remember much of what you have discussed.

When people covenant with each other to show up,
to be prepared to discuss intelligently, or lead a dis-
cussion, then everyone should stick to the agreed-
upon schedule. The best way to kill off a full-year Bible
study is to allow meetings to drag on into the night.
People may enjoy the talk during the time that its
going on, but next week they think to themselves: “i
really can't risk losing two or three hours. | think Ill
skip tonight's meeting.” At that point, they have
broken their agreement. The idea is to get a continu-
ing Bible study over the whole school term, not one or
two three-hour sessions.
