256 7S BIBLE QUESTIONS

The word of God is not on your opponents’ side in
those areas addressed by this book, and if you start
calling this to their attention, most of them will not ap-
preciate it. More to the point, you may find yourself
ostracized, informed on, and called into the Dean’s
office for “troubiemaking.” You may be asked to sign
again some sort of a doctrinat statement in order to
remain enrolled (or supported financially by your
home church), and that statament may be in flagrant
opposition to everything in this book, especially Part
HL. In short, they may hit you with the old “re-sign or
resign” coercion that has protected desperate and
theologically defenseless seminary administrations
for many decades.

These questions will drive some of your seminary's
faculty members crazy. (Others will patiently ignore
all questions, hoping that all this will blow over soon,
but it won't—not so long as our sponsoring organiza-
tions have money in the till to keep sending it out. For
as long as seminaries make available the names of
their students, this book will be around to plague
seminary professors. And if the schools stop making
the names available, they will be admitting just how
badly this “unsophisticated” book is hurting them. At
that point, they will also have to cancel publication of
ihe seminary year book. In short, there are many
ways to skin a cat and assemble mailing lists, and |
know most of them.) Outraged faculty members are
liabla to lash out at you for having begun a campus
Bible study using this book. They have seldom been
challenged by anyone in the classroom, let alone by a
whole group of students with. as much theological am-
munition as this book contains (even disregarding the
bibliography, which will really fluster them). They
have had a free ride for too long.

This book may look simple, but it’s a hand grenade.
