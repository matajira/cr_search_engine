APPENDIX A: HOW TO GET YOUR ANSWERS 209

matters as much as it applies anywhere else. Younger
people must give the benefit of the doubt to those
whose age indicates that they may possess ex-
perience. (And for people who should have gained it,
but haven't, there are techniques to offset their initial
advantage, as | shalt discuss later.)

3. Your Lack of Self-Confidence

This may not apply to you. Perhaps you're su-
premely confident. If you are, please reread the
previous two sections. But for those of you who have
doubis about yourself, understand how your doubts
can affect your ability to ask questions and pursue
answers.

You may not really understand your question. You
may. not understand its full implications. You may be
“in over your head,” or think you are. A question asked
aggressively, when you're not really confident about
your grasp of the issue, could tead te your capitula-
tion and retreat in the face of a determined answer.
Your teacher may recognize your hesitation. He may
make you appear foolish, or overly aggressive, or ill-
prepared. It never pays to appear to have lost an ex-
change of ideas. Better to structure your questions so
that you cannot visibly lose.

In a hard-fought exchange, an experienced teacher
will smell blood early. He will go for your weak points.
Your fellow students will seldom come to your
defense, unless you're known to be very shy or not too
bright. Better to ask your questions in such a manner
that your initial defeat will produce sympathy for you,
and not hostility.
