224 (75 BIBLE QUESTIONS

9. Where to obtain study materials.
10. .What to do if you're called in for discipline.
11. The decision to transfer out.
12. What to do if you get expelled.

1. Size of the Group

What is your goal? Presumably, to gain a better
understanding of the Bible. Do ! have guidelines that
would help you to achieve this goal? Jesus said, “For
where two or three are gathered together in my
name, there am | in the midst of them’ (Matthew
18:20). This is my suggested size: two or three peo-
ple, preferably three, and never more than four.

Where two people gather to discuss controversial
topics, there can be too much heated, unpleasant,
and ultimately unentightening yelling. 1 think three
students together tends to reduce the likelihood of
contention. One student can serve as a kind of ref-
eree. As ta who serves as a referee, this can change,
according to the topic under discussion. But with one
person there to monitor the level of intensity, it will
help to maintain the integrity of the group. The idea is
to meet to study, not to create a regularly scheduled
shouting match.

You must understand that the only winners in a de-
bate are those who come away with a better under-
standing of the issues. Anyone who comes away trying
to figure out a way-to “get even” with the other person
probably has not learned much. Only if this resentment
is channeled into personal study and growth (the
product of intellectual competition, just like your
classroom exercises) will anything positive come out
of ‘disputation. So | recommend three people to a
group.

In the early phases of a group, there may be only
