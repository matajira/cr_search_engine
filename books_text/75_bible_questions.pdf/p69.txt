Question 25

Did Christ Die
for All Men?

 

For God so loved the world, that he gave his only be-

gotten Son, that whosoever believeth in him should
not perish, but have everlasting life (John 3:16).

We have already asked the question about “whaso-
ever will,” which is a variant of “whosoever believeth”
(Question 20). The question raises the issue of the
ability of unregenerate men to come to saving faith in
Christ, if the natural man receives not the things of
the Spirit (1 Corinthians 2:14).

To understand John 3:16, you need to have come ta
grips with the previous questions in this book. What is
“the world” that God loves? Is it “all men in general”?
Then why did He despise Esau and promise to curse
him, before Esau had done good or evil? Why do
Christ's prayers of intercession convert some but not
all? If He prays for all men to be saved, and some
resist and perish, can we say that Christ's prayers are
efficacious? Do they really work? Or are the results
random?

God loves the whole creation. He sustains it moment
by moment. This is the doctrine of providence. He sus-
tained it after Adam rebelled. Why? He promised to
bring death to Adam, yet He allowed the world to con-
tinue. He preserved it for the sake of the as-yet unborn
elect. He preserved it for the sake of Jesus Christ, His.
incarnate Son, who came to die for His chosen elect.
God loves His creation; He does not love all men, ir-
respective of their relation to Jesus Christ.

65
