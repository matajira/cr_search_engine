Questionable Answer

“When God says ‘predestinates, He really means
‘foreknawiedge.’ God knows in advance who will ac-
cept or reject him (or accept His grace, and then tater
fail from grace), and He guarantees the potential for alt
things working together for good. But He does not
guarantee actual good. He only foreknows the poten-
tial for good.”

My Reply: The text is clear. All things work together
for good. Not some—ai/. Unless usage says other-
wise, we have to take the word seriously.

Then Paul raises the issue of predestination. This
follows his discussion of “ali things.” How much
plainer could he be? God guarantees ail things be-
cause He predestinates all things to benefit His fol-
lowers. Yes, Paul says that God “foreknew” His follow-
ers, but then he says that God predestinates. What
does predestinate mean? Doesn't. Paul say that it
means the guaranteeing of-all things (events) to fit
into the overall plan of God? His guarantee makes
sense only within the framework of His sovereign
power to bring all things to pass in a way that pro-
duces good for those called according to His purpose.

Foreknowledge and predestination go together,
Paul says. You can’t have one without the other. Can
anyone show how the biblical definition of foreknow-
ledge negates predestination?

If “predestination” means “foreknowledge,” why
does Paul use both words in the same sentence?

For further study: Dan. 2:20-21; 5:25-31; Matt. 11:27; 20:15; 22:14;
Luke 10:22.

26
