{an asterisk marks outstanding works)

* Alexander, J. A. The Prophecies of isaiah, Reprint edition;

Grand Rapids: Zondervan, 1953. (mid-19th century)
———-_-- The Psalms Translated and Explained. Grand
Rapids: Baker, 1975.

*Allis, Oswald T. Prophecy and the Church, Philadelphia:
Presbyterian and Reformed, 1945. This is the most com-
prehensive critique of dispensationalism available, writ-
ten by a postmiilenniatist who is often assumed to be an
amillennialist.

*Boettner, Loraine. The Millennium. Philadetphia: Presbyter-
ian and Reformed, 1957. Better in its critique of dispensa-
tionatism than its description of postmillennialism. Inade-
quate treatment of amillennialism.

Brown, David. Christ's Second Coming: Will It Be Premilien-
nial? Grand Rapids: Baker, 1983. Reprint edition. (Same
Brown as in Jamison, Fawcett & Brown; late 19th century.)

*Campbell, Roderick. /sraef and the New Covenant. Reprint
edition; Tyler, TX: Geneva Divinity School Press, (1954)
1981. Probably the best one-voilume exegetical treatment
of postmiltennialism.

*Chilton, David. Paracise Restored. Tyler, TX: Reconstruction
Press, 1984,

Custance, Arthur. The Seed of the Woman. P.O. Box 291,
Brockville, Ontario, Canada K6V 5V5. Excellent on the
question of life expectancy.

Hodge, A. A. Evangelical Theology. Edinburgh: Banner of
Truth, 1976, chapters 12-14.

de Jong, J. A. As the Waters Cover the Sea: Millennial expec-
tations in the rise of Anglo-American missions, 1640-1810.
Kampen, Natherlands: J. H. Kok, 1970.

195
