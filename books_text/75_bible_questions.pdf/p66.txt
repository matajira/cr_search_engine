Questionable Auswor

“God prophesies concerning the future, and the
events do take place. But in between the prophesied
events, God leaves men free to make their own deci-
sions, These decisions do not lead inevitably to the
prophesied event, or else they would themselves be
inevitable. Instead, God intervenes occasionally to
bring men’s events into.line with His specific pur-
poses.”

My Reply: Can't events be two-fold: 1) God's pur-
pose and 2) man's purpose? Can't the same event be
the product of God’s sovereign plan, yet involve man’s.
responsible decision? Can’t men plan evil and God
bring good from it? Can't an event be predestined by
God and still be a matter.of human responsibility? If
not, then why was Christ's crucifixion a blessing from
God, and at the same time, an act of evil on the part of
rebellious men?

If God's providence undergirds all creation, shouldn't
Christians be confident that all things work for the
good of those who are called according to His pur-
pose? Shouldn't they rest confidently in the absolute
sovereignty of God? Why argue that most events are
determined solely by men, and not by God? Look at
Egypt: “For it was of the Lord to harden their hearts,
that they should come against Israel in baitle, that he
might destroy them utterly, and that they might have
no favor, but that he might destroy them, as the Lord
commanded Moses” (Joshua 11:20).

  

on

For further study: Gen. 41
103:19; Prov. 16:3: Isa.

  

I Kings 22:28, 34; Ps. 24:1; 47:7-8;
4610-11; Lam. 3:37-38.

  

62
