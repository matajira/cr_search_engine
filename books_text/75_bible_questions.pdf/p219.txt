APPENDIX A: HOW TO GET YOUR ANSWERS 215

answer them, or so ignorant of their implications that
he should probably drop out of schoo! and get a job,
or perhaps he is simply not a Christian.

The Satest Approach

Perhaps very few of your fellow students have seen
this book. Perhaps hundreds of them have. It makes a
difference. !f everyone is talking about them, it may
be perfectly safe to ask a question in class. You're just
another face in the crowd. But if hardly anyone is dis-
cussing them, your question will identify you as some-
one who reads “subversive literature.”

A frontal attack on power seldom produces victor-
ies. it's far wiser to join the crowd of the “disturbed
Other students will be bothered by many of the 75 ini-
tial questions and their corollaries. Some of them will
want answers, too. They just don’t know what to think,
So you can ask for them.

There are lots of questions to ask. You may have
several years to get answers. Don't be in a hurry. Just
take your time. See which instructors have some in-
sights, and which ones start to dance. It's always a
sight to see instructors dance the free-wil! two-step,
dancing away from the hard questions, seeking to.
evade the obvious implications. (Usually those cam-
puses that prohibit social dancing are staffed by in-
structors who are experienced theological dancers.)

Are other students asking different questions? So
much the better. Are they asking them in some
classes, but not in others? Then you can probably
identify yourself as one of the “mentally disturbed.” if
there are enough “disturbed” students on campus, no
one gets in trouble. But if no one is asking questions,
you must proceed cautiously. You could be identified

 

 
