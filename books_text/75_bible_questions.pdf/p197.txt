Question 75

Aren't There Two Kinds
of Salvation?

For therefore we both labour and suffer reproach,

because we trust in the living God, who is the
Saviour of all men, specially of those that belleve
(| Timothy 4:10).

Christ is the Savior of all man. The words are in-
escapable. Does this mean universal salvation? Will
no man go to hell? This is what the so-called “univer-
salists” erroneously believe.

So what do the words mean? They mean that there
are two kinds of salvation: common and special. There
are therefore two kinds of grace: common and special.
Common grace relates to earthly life itself and the
benefits associated with earthly life. Special grace re-
lates to a man’s exercise of saving faith, which is possi-
ble only because of the sovereign grace of God.

The word “save” is related to “salvation,” which in
turn is related to “salve.” A salve is a healing ointment.
This is a very good description.of Christ's work at Cal-
vary. Christ healed the whole universe by His death.
Because of this, God did not destroy the world on the
day Adam rebelled. This salve heals some. men’s
souls and alt men’s environment.

This is why Christ's salvation is comprehensive. It
affects everything, for it was designed to heal every-
thing progressively, and finally on the day of judg-
ment. This is why obedience to God’s law brings long-
term growth and progress. We live in an orderly
universe—ordered by the ethical character of God.

193
