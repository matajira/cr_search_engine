Question 21

Could the Authorities
Have Acted Righteously
and Released Jesus?

For of a truth against thy holy child Jesus, whom
thou hast anointed, both Herod, and Pontius Pilate,
with the Gentiles, and the peopie of israel, were gath-
ered together, for to do whatsoever thy hand and thy
counsel determined before to be done (Acts 4:27-28).

This prayer of the early apostles before the chief
priests and elders was made publicly to God. They
affirmed by this prayer the sovereignty of the God of
the Bible over the affairs of that day. It was a reminder
to the rulers that they were under God’s authority, and
they could do nothing to thwart His designs, even
when they thought that they were overcoming His de-
signs and His peopie.

The rulers released Barabas, not Christ. The crowd
yelled “Give us Barabas,” and Pilate did. Did they
overturn God's plan for the ages? Hardly; they made
a decision to crucify Christ, the turning point of the
ages. This turning point had been foreordained be-
fore the foundation of the world.

Something similar happened to Joseph when his
brothers sold him into slavery. He saved their lives
from famine years jater. He said to them: “As for you,
ye thought evil against me; but God meant it unto
good” (Genesis 50:20). God ordains the good; men
pian evil; but the historical event is the same. The
greatest good and the greatest evil—the cross— was
ordained.

57
