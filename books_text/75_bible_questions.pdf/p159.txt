Doesn't “Leaven”
Mean Victory?

Another parable spake he unto them; The kingdom
of heaven is like unto leaven, which a woman took,
and hid in three measures of meal, till the whole was
leavened” (Matthew 13:33).

First of all, let's get one. thing perfectly clear:
“Leaven” doesn't mean “siri.” Leavened bread was re-
quired by God for an-important Old Testament sacri-
fice, the peace offering of thanksgiving (Leviticus
7:13), Unleavened bread was required at the Pass-
over, as a symbol of the separation of Israel from the
corrupt ethical leaven of Egypt. But once in the prom-
ised land, they were required to bring leavened bread
before God. At the feast of Pentecost, they were re-
quired to bake leaven loaves as the firstfruits offering
(Leviticus 23:17). This. was the teaven of ethical matur-
ity; the best bread of the land. If we were to equate
“leaven’ with “evil,” we would call into question the de-
cision of God in requiring a symbol of evil in a thanks-
giving sacrifice to Him. The /eavened offering was
characteristic of Pentecost, a day of rejoicing.

What is leaven? It is yeast. A little is put into dough
before it is baked. It causes the dough to rise. It pro-
duces a soft, full loaf. Leaven is a symbol of growth, of
maturity or development. it can refer either to evil or
righteousness. We are to put away the old teaven, the
corrupt feaven (| Corinthians 5:6-8). But the kingdom
is like leaven: growth is basic.

155
