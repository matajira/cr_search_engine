140 75 BieLE QUESTIONS

 

_—..; 6d. The Journal of Christian Reconstruction,
Vol. V, No. 1: Symposium on Politics.

Rushdoony, Rousas John. By What Standard? Tyler, TX:
Thoburn Press, (1959) 1983.

—____. The Institutes of Biblical Law. Nutley, NJ:
Craig Press, 1973. This book will show exactly how
crucial biblical law is, and has been, in Western civiliza-
tion.

. Politics of Guilt and Pity. Fairfax, VA: Thoburn
Press, 1978.

Vos, J. G. The Separated Life. Philadelphia: Great Commis-
sion Publications, n.d. ;
Williamson, G. |. Wine in the Bible and the Church. Phillips-

burg, NJ: Pilgrim Publishing Co., 1976.
