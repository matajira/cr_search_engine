Isn't the Will of God
Absolutely Sovereign?

He-doeth according to His will in the army of heaven,
and among the inhabitants of the earth: and none can
stay his hand, or say unto him, What doest thou?
(Daniel 4:35).

The sovereignty of God is asserted throughout the
Bibie. The question is: Does God voluntarily withdraw
Himself from millions of events, especially the event
of salvation, but also all evil acts of men, and thereby
turn control over these historical events to men or
angels? Does He abdicate fram His position of total
potential power, in order to open zones of pure free-
dom for men? Does He bring His will to pass only by
intervening into history sporadically?

In Hebrews 1:3, we read of Christ that He upholds
all things by the word of his power. All things? He is
“above all, and through all, and in you all” (Ephesians
4:6). God spoke through Isaiah: “Remember the
former things of old: for | am God, and there is none
else; | am God, and there is none like me, Declaring
the end from the beginning, and from ancient times
the things that are not yet done, saying, My counsel
shall stand, and { will do all my pleasure” (46:9-10).

How powerful is God’s word? “So shali my word be
that goeth forth out of my mouth: it shall not return
unto me void, but it shall accomplish that which {
please, and it shall prosper in the thing whereto |
sent it” (isaiah 55:11). Comprehensive!

61
