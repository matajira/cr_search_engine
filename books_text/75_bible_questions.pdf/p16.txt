12 75 BIBLE QUESTIONS

Before You Begin

{| want you'to ask yourself a question: “Do | really
believe that the Bible is God’s absolutely authoritative
and inspired word?” if you can’t honestly answer
“yes,” then you already accept the number-one pre-
supposition of the humanists. You probably will be
repelled and outraged by all 75 questions. But if you
do answer in the affirmative, you should be willing to
subordinate your thinking to whatever the Bible plainly
teaches. tf you're really willing to do this, then these
75 questions will open up a whole new world to you.
Now, are you ready to ask yourself 75 very serious
Bible questions? And are you ready to seek out an-
swers if you find that you don't agree with them?
