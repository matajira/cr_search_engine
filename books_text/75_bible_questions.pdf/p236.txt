232 75 BIBLE QUESTIONS

One person should be responsible for monitoring
time. When there are ten minutes to go, he should an-
nounce it. Then everyone can make a few more com-
ments: But in that last ten minutes, everyone should
be preparing mentally to wrap it up. The only excep-
tion would be if an unbeliever finally recognizes that
he has been an unbeliever, and he wants to repent in
the group and then discuss what he has just done
with his life. This may happen, and you can make an
exception for what is clearly a kind of emergency. But
nothing any less important than a major breakthrough
in the life of one of the participants should break
through the one-hour time limit. Redeem the time
well.

Now, to prevent parents or administration members
from trying to shut down these Bible studies “for the
sake of the students,” | lay down this one rule: set up
and attend only one extra weekly Bible study. | don't
want the administration going to the Board of Trust-
ees with the complaint that “students are spending so
many hours in these secret Bible studies that they are
gaing to flunk out, so we have to abolish them.’ Never
give your self-deciared opponent ammunition if you
can help it.

This brings up another point. You should study
much more ditigently for your academic courses if you
have become a member of a Bible study recruiting pro-
gram. First, your instructors may take vengeance on
you and start grading your papers especially hard. You
want to give this kind. of morally corrupt, vindictive in-
structor no valid cause to do this. Second, you want to
be able to show parents and other critics that your
grades have improved because of your new-found
diligence—something you have been taught in your
Bible studies. Learn-from the early church’s confronta-
