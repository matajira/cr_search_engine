Question 67

Doesn't the Bible Require
_an Appeals Court?

Casting down imaginations, and every high thing
that exalteth itself against the knowledge af God,
and bringing into captivity every thought to the obe-
dience of Christ; and having in a readiness to
revenge all disobedience, when your obedience is
fulfilled (II Corinthians 10:5-6).

There is a progressive sanctifying of the creation,
from the heart to the society at large. First, there is re-
generation, when we receive the mind of Christ (1 Cor-
inthians 2:16}. We cast down evil and vain imaginations
that are in opposition to the knowledge God provides
of Himseif. This establishes our presuppositions on the
revelation found in the Bible. Next, we bring our
thoughts captive to Christ. This is intelectual self
discipline, and the standard of success is the Bible.

Having achieved this in our personal fives, we then
apply our wisdom to other areas: home, church,
school, business, and civil government. The New
American Standard Version translates verse 6 as: “we
are ready to punish all disobedience, whenever
your obedience is complete.” When the elders of the
church at Corinth have. at last decided to enforce
biblical law, then they can caii in Paul and other
elders to serve as outside judges in cases where
disobedience still prevails among unruly members.
Church sanctification is progressive; as men disci-
pline themselves, they can call in godly judges for
more serious problems.

W77
