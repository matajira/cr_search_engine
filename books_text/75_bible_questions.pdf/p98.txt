Questionable Answer

“Jesus was speaking of spiritual gifts of the Spirit, not
the details of Old Testament law. Without love and
charity, and a willingness to forgive men, all men’s
works mean nothing (I Corinthians 13).”

 

» My Reply: But what are “spiritual gifts"? They are
not exclusively invisible gifts, because Christ spoke of
visible fruits. They are acts that can be evaluated by a
standard. The question is: By what standard?

in contrast to gifts of the spirit are evi fruit. Evil acts
are called “the works of the flesh’ by Paul. What are
they? “Now the works of the flesh are manifest,
which are these: adultery, fornication, uncleanness,
lasciviousness, idolatry, witchcraft, hatred, variance
[quarrels], emulations [jealousy], wrath, strife, sedi-
tions [intrigues], heresies, envyings, murders,
drunkenness, revellings, and such Ilke” (Galatians
5:19-21a}. These are acts of lawlessness.

“But the fruit of the Spirit is love, Joy, peace, long-
suttering, gentleness, goodness, faith, meekness,
temperance: against such there is no law” (Galatians
§:22-23). This implies that there /s a law against the
other acts—not necessarily civil law, but God's law.
The specific nature of evil acts makes .Pau!'s point
clear: do these, and you do not enter heaven (6:21b).
Thus, our standard of ethical evaluation is God's law.
As Christ warned: “Beware of false prophets.” They
wilt teach you to pay no attention to God's law.

 

For further study: Ps. 19:7-11; 119:105, 129-30; Prov. 6:23; Isa.
8:20.

 

94
