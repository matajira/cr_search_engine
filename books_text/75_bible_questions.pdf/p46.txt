Questionable Answer

“Jesus knew that these people were not ready for a
complete version of His message. They had to be
spoon-fed. He did intend eventually to present a clear
version of His spiritual truths, but His ministry was cut
short by the crucifixion. His intention was to introduce
all people to the mysteries of the faith.”

My Reply: What Jesus said was not that He intended
to give them a better picture of the gospel later on.
What He said was that the multitudes had not been
given the opportunity to know the mysteries of the
kingdom. He said nothing about speaking to them
more clearly later on. On the contrary, He specifically
said that He intended that they should perish:

For this people's heart is waxed gross, and
their ears are dull of hearing, and their eyes
they have closed; lest at any time they should
see with their eyes, and hear with their ears,
and should understand with their heart, and
should be converted, and | should heal them
(. 15).

Does this mean that Jesus did His best to keep
these people from repenting, to hide the mysteries of
the gospel from them?. If it doesn’t mean this, what
else could it mean?

And more to the point, why did Jesus speak in
parables that even the disciples couldn't understand?

   

For further study: Matt. 11:25-27; Mark 4:11-12; Luke 8:10;

10:21-22; John 12:39-40,

42
