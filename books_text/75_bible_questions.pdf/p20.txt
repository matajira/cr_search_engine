46 75 BIBLE QUESTIONS

questions. They are all concerned with the question:
Is God absolutely sovereign over His creation, or is
man in any way sovereign? The humanists want to
argue that on/y man is sovereign, to the extent that
any conscious force is sovereign. As you will see
when you read the next 25 questions, the Bible
teaches that God, and God alone, is absolutely and
comprehensively sovereign. But there are many,
many Christians who can’t make up their minds be-
tween what the Bible teaches and what the humanists
teach. They don’t want to accept either position.

The Bible doesn’t teach a middle position. (That's
my assertion.) Are you willing to see what the Bible
teaches in this regard? Are you witling to test my
assertion by asking yourself 25 questions, and then
seeking biblical answers?
