Questionable Answer

“The parable of the talents relates only to spiritual
gifts. It has nothing to do with earthly capital. All Jesus
meant was that a man is to multiply his spiritual capi-
tal, primarily by leading people to faith in Christ.”

My Reply: Most of Jesus’ parables were spoken in
agricultural or monetary terms. They were “pocket-
book parables.” He did not speak to the masses in the
janguage of theological disputation. He spoke to
them in the language they understood.

The parable of the talents teaches unmistakably
that no matter what skill or gift you have received
from God, you are supposed to put it to good use. It
should earn a positive rate of return (25:27). You will
be held responsible on judgment day. The period of re-
sponsible action before the Lord returns is to be re-
deemed (bought back). It is to be productive, in every
sense of the word. Men are to gain skills of exercising
good judgment. After all, who received the talent of
the man who was judged? The person who had been
given the greatest amount of responsibility from the
first, and who had earned 100% on his “investment.”
From them to whom much is given, much is expected
(Luke 12:47-48). As millions of Christians pursue
various lines of service, the capital base of the church
multiplies. This brings wealth and influence over time.
But many Christians reject Christ's teaching about
culture-iransforming Christian labor because they
have no confidence in their own talents or God's law.

  
 
 

For further study: Lev. 26:3;
Prov, 3:33; 10:22; tsa, 47:17-19;
