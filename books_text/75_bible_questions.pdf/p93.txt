— Question 26

Isn't it Immoral for People
to Have Sex with Animals?

Neither shait thou lie with any beast to defile thyself
therewith: neither shall any woman stand before a
beast to lie down thereto: it is confusion (Leviticus
18:23).

What Christian wouldn't affirm the immorality of
such an act? Why even include it in this book? Sim-
ple: the New Testament doesn’t mention bestiality. It’s
an Old Testament prohibition. So the question now
arises: Is Old Testament faw stilt morally and legally
binding on church members?

There are many Christians who say that Old Testa-
ment laws no longer apply to Christians unless the
New Testament reaffirmed a particular Old Testament
jaw. But the New Testament doesn't mention this Old
Testament prohibition. Are. we to conclude that. this
issue is morally an “open question”? But God says that
bestiality requires the death penalty: “Whosoever lieth
with a beast shall surely be put to death” (Ex. 22:19).

On the other hand, if we accept the principle of bibii-
cal interpretation that all Old Testament laws are still
completely binding on Christians unless a New Testa-
ment passage has released us from the obligation, then
we-can say in confidence that such an act is an “abomi-
nation.” “Defite not ye yourselves in’ any of these
things: for in all these the nations are defiled which |
cast out before you: And the land is defiled: therefore
Ido visit the iniquity thereof upon it, and the land it-
self vomiteth out her inhabitants” (Leviticus 18:24-25).

39
