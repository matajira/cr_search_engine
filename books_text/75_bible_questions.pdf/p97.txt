Is Profession of
Faith Enough, or Do
Our Acts Also Count?

Not every one that saith unto me, Lord, Lord, shail en-
ter into the kingdom of heaven; but he that doeth the
wifl of my Father which is in heaven (Matthew 7:21).

“Taking the Lord’s name in vain’ is not just cursing. It
is also the unbeliever's proclaiming of God's name while
performing works that impress men but not God. The
Pharisees gave charity in public so that ail men could
see them. “They have their reward,” Christ said sarcas-
tically—their prestige. among men, but also. final judg-
ment. Seven sons of Sceva tried to cast out demons in
Jesus’ name, and a demon leaped upon them and drove
them out of the house, naked and bruised (Acts 1913-17).

Jesus knew what spiritual impostors would do. “Many
will say to me in that day, Lord, Lord, have we not
prophesied in thy name and in thy name have cast
out devils? And in thy name done many wonderful
works? And then will | profess unto them, 1 never
knew you: depart from me, ye that work iniquity’
(Matthew 7:22-23).

How can a person test himself, ar anyone else, to de-
termine whether he is a spiritual counterfeit? “Beware
of false prophets, which come to you in sheep's
clothing, but inwardly they are ravening wolves. Ye
shall know them by their fruits. Do men gather
grapes of thorns, or figs of thistles? Even so every
good tree bringeth forth good fruit; but a corrupt
tree bringeth forth evil fruit” (Matthew. 715-17),

93
