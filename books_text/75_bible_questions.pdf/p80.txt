76 75 BIBLE QUESTIONS

Episcopatian (Anglican)
X. Of Free Will

The condition of Man after the fall of Adam is such, that he
cannot turn and prepare himself, by his own natural strength
and goed works, to faith, and cailing upon God. Wherefore
we have no power to do good works pleasant and acceptable
to Ged, without the grace of God by Christ preventing us,
that we may have a good will, and working with us, when we
have that good will.

XVII. Of Predestination and Election

Predestination to Life is the everlasting purpose of God,
whereby (before the foundations of the world were laid) he
hath constantly decreed by his counsel! secret to us, to de-
liver from curse and damnation those whom he hath-chasen
in Christ out of mankind, and to bring them by Christ to
everlasting salvation, as vessels made to honour. Where-
fore, they which be endued with so excellent a benefit of
God, be cailed according to God's purpose by his Spirit
working in due season: they through Grace obey the calling:
they be justified freely: they be made sons of God by adop-
tion: they be made ike the image of his only-begotten Son
Jesus Christ: they walk religiously in good works, and at
length, by God’s mercy, they aitain to everlasting felicity .. .
the godly consideration of Predestination, and.our Elaction
in Christ, is full of sweet, pleasant, and unspeakable comfort
to godly persons, and such as feel in themselves the work.
ing of the Spirit of Christ, mortifying the works of the flesh,
and their earthly members, and drawing up their mind to
high and heavenly things, as well because it doth greatly
establish and confirm their faith of eternal Salvation to be
enjoyed through Christ as because it doth fervently kindle
their love towards God.

  

The Thirty-Nine Articles, 1563
