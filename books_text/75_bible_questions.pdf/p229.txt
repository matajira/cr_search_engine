APPENDIX B: THE BIBLE STUDY 225

two people. One of the goals of a two-person group
must be the recruiting of a third. When four people
show up, it’s already getting too large. | don't recom-
mend battles in which there are “two to two ties” each
time. If two people agree, and one doesn't, the third
will tend to stop coming or set up his own group (pref-
erably the latter). But when four are split, the fight can
goon.

Never have five people. Have one member set upa
second group. Then you have two groups of three
each, with the leader of the new group meeting twice
a week, once with the originai group, and once with
the second group.

This, in fact, is what your goal should be: the estab-
lishment of study groups of three people each, with
two meetings per person, per agreed-upon time
period, probably weekly.

You want these groups to multipty. You want to cre-
ate new sources of better information, for yourself
and for other students. This means you must do your
part to honeycomb the whole campus with Bible stu-
dies. If one group discovers something interesting,
word can spread from group to group quite rapidly.
The group can type up its discovery, put in its Bible ci-
tations or logic, and pass it “upward” and “downward.”

Here is how this works. There is an original study
group. Three members meet, say, once a week for an
hour to discuss the Bible. Preferably, they agree in
advance to study a particular question or Bible
verse(s). Then they bring in their findings.

Each member should strive to recruit two new
members for his own group. He becomes the organi-
zer. He develops his leadership as a builder. The goal
is to build up others, not to tear them down. The goal
