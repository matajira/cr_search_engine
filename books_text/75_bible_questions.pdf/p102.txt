 

“The Bible is clear: once you make a profession of
faith, God honors that profession (Romans 10:9-10). If
aman makes that profession of faith in Christ, he is
forever sealed by the Holy Spirit.”

My Reply: Paul wrote in Romans 10 that we must
confess Christ and believe in our hearts that God has
raised Him from the dead. Problem: What if we cease
to confess Him or to believe that God has raised Him
from the dead? What if by our public acts of sin or our
public confession of faith, we renounce Christ and His
gospel? Then the promise no longer applies. We
never possessed true saving faith.

“Once saved, always saved” is quite correct. But is
“saved” the same as “I made a confession of faith 23
years ago, when | was young and foolish, and which |
no longer believe"? Some expositors argue erron-
eously that “Once confessed, always possessed.”
There is nothing in the Bible that supports this inter-
pretation. Demas once confessed. So did Hyman-
aeus. It did them no good. In fact, it did them worse
harm, for they had been given much, and “unto
whomsoever much is given, of him shall be much
required” (Luke 12:48b). Men sometimes change
their confessions.

How do we know if we still believe in Christ? Jesus
said: “He that hath my commandments, and keep-
eth them, he it is that loveth me” (John 14:21a). We
test our faith by His commandments.

 

For further study: John 15:10; Ft
312-414; 6:4-12; 10:26-39;.1 John 2:

2:12.43; Gol. 1:22.23; Heb.

 

98
