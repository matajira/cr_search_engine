Question 17

Aren’t Men Ordained in
Advance to Eternal Life?

For so hath the Lord commanded us, saying, | have
Set thee to be a tight of the Gentlles, that thou should-
est be for salvation unto the ends of the earth. And
when the Gentiles heard this, they were glad, and
glorified the word of the Lord: and as many as were
ordained to eternal life believed (Acts 13:47, 48).

The text is clear. There were Gentiles who heard
the preaching of Paul and Barnabas. Some were or-
dained to eternal tife, and some were not. “As many
as were ordained to eternal lite believed.” It could
not be any clearer. Not one fewer believed the gospel
of salvation than had been ordained to eternal life,
and not one more. Just as many as: a “one-to-one”
relationship.

ts there a random aspect to salvation? Are some
men chosen by God to attain the status of adopted
sons, and then they thwart God’s choice, either by re-
fusing to believe the gospel initially, or by believing,
becoming converts, and then departing fram the
faith? Are others not ordained to eternal tife, but
somehow, by the grace of God (yet not by His plan),
believe the message and receive salvation? In other
words, is there biblical evidence to indicate that the
relationship between those ordained for salvation and
those who attain it is not one to one? Is what is des-
cribed in Acts 13:48 itself random—a unique event
which is not representative of God's plan of salvation?
In short, is there a discrepancy between those ordained
and those who are actually converted?

49
