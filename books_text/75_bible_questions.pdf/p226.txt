(222 75 BIBLE QUESTIONS

They set up study groups, and each student special-
izes in a particular field. One person becomes re-
sponsible for teaching the others in a particular area
of study. The others learn, .and the others also in-
struct. The result is more information per student.
Why couldn't a Bible study be conducted this way?

What | recommend is a very specific sort of Bible
study. Thera are certain features of a successful cam-
Pus Bible study that are ignored by students unless
they have been shown what to do. | am now going to
show you exactly what you need ta do, and how you
can change your life and your campus as a direct
result.

Understand from the beginning that there are no
free lunches in life, except the grace of God. You will
have to work. You could also get into a difficult situa-
tion with the administration. Some administrations
treat students as intellectual adults. Some don't. A
good test is roll-taking in class. If you're docked points
for cutting classes, you’re probably dealing with an
administration that thinks students are children. They
think that the pressure of examinations and papers is
insufficient. They also suspect that the school's own
faculty is filled with boring lecturers who need help to
get students into their classes. | know of no major
American university which requires classroom atten-
dance, although individual professors may (generally
the least competent ones). This.is especially true of
European universities, Attendance is strictly voluntary.
But third-rate colleges do require daily attendance.

| regard it as amusing (though pathetic) that there
are theological seminaries in the United States that
require faculty members to take roll each day and
drop students’ grades for missing a class three times
—even seniors who in less than a year will be pastor-
