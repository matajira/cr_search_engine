Questionable Answer

“There were two churches in the transition period be-
tween the Old Testament and the New Testament.
One was a Jewish church, which Peter ministered un-
to, and the other was the church, the body of Christ,
which Paul ministered unto. Peter wrote this epistie to
the Jewish church, not to the Great Parenthesis Gen-
tile church.”

My Reply: This is the answer of the ultradispensa-
tionalists who date the church, meaning the Gentile
church which was never foreseen in the Old Testa-
ment, with the advent of Paui’s ministry. They pro-
claim two churches, one Jewish and one Gentile, to
match two different kingdoms, the Jewish kingdom of
heaven and the Gentile kingdom of God.

And is Christ married to two brides? Both the
church and Israei are called the bride of God in the
Bible. Is Christ a bigamist?

The traditional dispensationalist would have to an-
swer differently. He would probably point to that old
familiar theme, the “transitional era.” This was a fulfilled
prophecy in the church age, but it was not yet the
church age for the Jews. But who was Peter writing to?
He refers to a people who had never been the people of
God. Israel had been the people of Ged. This prophecy
must refer to Gentiles. Se the church age was known in
the Oid Testament! Therefore, the key argument of dis-
pensationalism—the Church as a “great parenthesis" —
isn't. true. Have you been equally misled concerning
other doctrines?

 
 

For further study: Acts 7:
10:4; Gal. 3:8-9, 14, 29; Phil

-21; | Cor.
