Questionable Answer

“Actually, Paul's question is not what it looks like. It
looks rhetorical, but it isn't. He only sounds as though
he Is saying that nothing can separate us from the love
of God. In fact, we can separate ourselves fram God's
love. Besides, the “us” he refers to are indeterminate
people, though perhaps a determinate number of peo-
pie. After we die, then nobody can separate us—those
of us who never actually get separated before we die
—from the love of God.”

 

My Reply: How can anyone be threatened after
death by tribulation, distress, persecution, and so
forth? Of course dead Christians are safe then from
being separated from God's love by these threats.
They aren't threats any longer. But Paut is talking
about not being separated from God's love during life
on earth, when these threats are real. So who are the
us” on earth he refers to?

Doesn't “us” refer to all regenerate people, all of
whom are protected from separation from God’s love
before they die physically? Who among “us” can fall?
lf this isn’t what “us” means, why does Paui mention
all those trials and tribulations?

Also, why does he use what is obviously a rhetorical
argument—“Who can separate us from the love of
God?"—if he really believes that someone can? How
could a master of biblical logic misuse words this
way? How can an elect person fall from grace?

 

For further study: Jer. 32:40; John
5:9, 23-24; H Thess. 3:3; | Pet. 1:4-5, 9.

 

 

32
