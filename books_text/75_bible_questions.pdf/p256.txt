252° 7s BIBLE QUESTIONS

copy of this book to every incoming freshman next
year.

(Warning: we can't do much to help you if you hap-
pen to attend an unaccredited college which isn't tax
exempt. If it also has a disastrous academic reputa-
tion when it comes to the question of free and open
discussion, the school is probably not very
vulnerable. Students in such a school have very little
“leverage” because the school has so little to lose.
This is the administration's “happy by-product” of be-
ing third-rate. if you attend such a school, | suggest
that you get your transcripts sent to another school,
immediately, for self-protection.)

In short, any attempt by an administration to harass
students who are prayerfully using our materials wil!
be met with countervailing power. You're not at the
marcy of college deans, so long as you conduct your- -
self graciously. If they take so much as a single hostile
step against you, they are then at your mercy.

{To students attending a respectable’ campus, this
language may sound overly aggressive.. But you
aren't facing expulsion for your beliefs. Some stu-
dents are. They need to know how to defend them-
selves. It's my responsibility to show them how, since
this book could get them into a predicament.)

Christian teachers and administrators have experi-
enced no systematic and effective challenges from
students for several generations. The students
haven't known how to defend themselves. With the
publication of this little book, they now have a much
better idea of just how vulnerable petty tyrants are. It
will do tyrants on the campus a lot of good. They may
learn what happens to people who are unwilling to
show good faith or fair play. Shed no tears for them.
