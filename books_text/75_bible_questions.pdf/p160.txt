 

“Yes, the kingdom grows. it grows spiritually. Those
who are part of the church do mature spiritually over
time. But this says nothing about the influence of the
kingdom in history. It does not replace Satan’s corrupt
leaven loaf.”

 

My Reply: Unquestionably, the premise of the
growth of God’s kingdom is the ethical maturing of His
people over time. But this is not simply growth within
one person’s lifetime. It is growth throughout history
This parable is like the parables that preceded it. /ts
setting is world history.

What is growing is a specifically Christian civiliza-
tion. The parables do not describe just one individual's
spiritual development. The focus is on a collective entity,
the kingdom. This kingdom is made up of ail Chris-
tians. It isn't limited to the institutional church; it en-
compasses the church. !t includes families, schools,
nations, and ail institutions — wherever Christ's gospel
and biblical law apply, namely, across the face of the
earth. All people need the saving power of Christ.
Therefore, all their relationships and institutions need
the saving power of Christ's gospel. As Christians
work out the implications of their faith over time, the
kingdom continues to develop. God's pre-resurrection
kingdom never fully replaces Satan’s earthly. king-
dom, but it will avercome most of it.

The whole loaf is going to be leavened; the final
maturity comes at the day of judgment. What does
“whole loaf” mean, if not the whole world?

 

For further study: Ps. 89; 102:13-22; 138:

 

5 Isa.

 

156
