68 75 BIBLE QUESTIONS

the great Baptist preacher of the late nineteenth cen-
tury. He preached a famous sermon on “Election,”
based onl! Thessalonians. 2:13-14, which is reprinted
in his New Park Street Pulpit, Vol. |. His position is
summarized in the book.by lain Murray, The Forgotten
Spurgeon (Carlisie, Pennsylvania: Banner of Truth
Trust). But be sure to get the original versions or
photographic reprints of the originals. In the 1950's,
“expurgated” editions of Spurgeon’s writings were
reprinted by an unscrupulous fundamentalist leader
who deliberately removed all references to predestin-
ation from the revised sermons by Spurgeon (and ad-
mitted that he did it in a letter | have personally seen).
As I’ve said, there has been a deliberate effort on the
part of many instructors to mislead students with
respect to certain doctrines of the faith.

| have selected relevant statements from a number of
ecclesiastical traditions to prove my case: Episcopalian
(Anglican), Baptist, Congregationalist, Lutheran, Re-
formed, and Presbyterian. Anyone who tells you that the
sovereignty of God hasn't been the primary belief of or
thodox Christians through the ages is either lying, or he
is ignorant about what he is saying, or he detines “ortho-
dox’ differently from what the historic creeds have
taught. But in any case, the history of the church stands
against him. Will it stand against you, too?

Baptist
Chapter It—Of God's Decrees

1. God hath (Isa. 56:10; Eph. 1:11; Heb. 6:17; Rom. 9:15,
18) decreed in himself, from all eternity, by the most wise
and holy counsel of his own will, freely and unchangeably,
all things whatsoever come to pass; yet so as thereby is God
neither the author of sin (James 1:13; | Jotin 1:5) nor hath
fellowship with any therein; nor is violence offered to the will
