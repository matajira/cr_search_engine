Question 70

Doesn’t Christ's Kingdom
Expand Over Time?

Thy kingdom come, Thy.wili be done in earth, as it is
in heaven (Matthew 6:10).

We are to pray for God's kingdom to come. But
Christ said that when the disciples saw Him casting
out demons that they would know that the kingdom
had already come (Luke 11:20). So why are we to pray,
“Thy kingdom come’?

We must understand the biblical triad: definitive, pro-
gressive, and final..God gives us definitive sanctifica-
tion when we are regenerated—Christ’s righteousness
imputed to us. We work out the implications of this
definitive sanctification throughout our. lives (Philip-
pians 2:12b). Then on the day of judgment we receive
our final, perfect sanctification which had been granted -
to us in principle at the moment of our conversion.

The kingdom came, and the final kingdom will
come. In the meantime, we as Christians are to work
out historically the implications of what God gave to
us definitively with Christ's ministry—definitive, pro-
gressive (historical), and final.

How do we judge our success in our efforts to develop
Christ's. kingdom principles, before the final judgment?
By comparing God's law to what we have built by using
God's law. This is why Christ also required that we pray,
“Thy will be done in earth, as it is in heaven.” God's
kingdom is not marked by Christ's physical presence
on earth for 1,000 years; His kingdom is marked by
our progressive application of Gad’s law on earth.

183
