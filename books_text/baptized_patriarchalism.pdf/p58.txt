50 BAPTIZED PATRIARCHALISM

The question is: How are the two oaths confirmed? ‘The marriage
oath is confirmed one time only: by physical consummation of
the partners. The church’s oath is also confirmed one time
only: by baptism. This is the heart of Calvin’s judicial theology
of baptism. But Rushdoony has denied the authority of the
institutional church to seal the ecclesiastical oath, yet only by
baptism can the church’s oath be sealed. In Rushdoony‘s patri-
archalism, the institutional church is stripped of its judicial
authority to bind its members by oath. This authority belongs
to the head of the marital family’s household. But if there is no
way to bind an oath, on what basis can any covenantal institu-
tion legally impose its sanctions? Here is the dilemma: ne sanc-
tions — no oath. The institutional church becomes impotent.
Another point is crucial: there is no judicial means of renew-
ing a marriage covenant. This is what distinguishes the family
judicially from both church and state. In the state covenant,
there is covenant renewal by confirming a new ruler, e.g.,
voting. In the church covenant, there is renewal through for-
mal worship — specifically, by taking the Lord’s Supper. But
there is no biblical method of renewal for the marital covenant. This is
because families are temporary and temporal. A family cove-
nant is broken through death of one spouse, either physical
death or covenantal death. There is no permanent covenant
bond between parents and children, which is why children are
instructed to leave their families in order to establish new fami-
lies (Gen. 2:24). The biblical family is a nuclear family.
Rushdoony’s system denies that there is no means of cove-
nant renewal for families, for in his system, the Lord’s Supper
is a family rite. By stripping the institutional church of its rite
of covenant renewal, Rushdoony transfers it to the family. This
is the judicial basis of his patriarchalism-familism. This is why
his doctrine of the church is in fact a doctrine of the patriarchal
family, a revival of the very worldview that Jesus warned
against (Matt. 10:35-37). But this monumental theological error
is not readily apparent to those who do not think covenantally.
