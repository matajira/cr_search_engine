Baptized Patriarchalism 31

a requirement that a man must be able to read in order to vote
in a civil election makes literacy the nucleus of all government.
The family is a training ground in learning how to govern.
There is nothing revolutionary in this observation. The church
is to use the family as a surrogate. If a man cannot rule well in
his family, Paul said, do net make him a leader in the church.
The odds are against his success. That this requirement governs
ordination to the pastorate is clear to everyone except seminary
professors and churches that ordain unmarried seminary grad-
uates. They have substituted term papers for family rule as the
screening criteria. This has been disastrous for the church.

First Timothy 3 does not make the family the nucleus of all
government. Self-government is the nucleus of all government. This
is why there will be a day of final judgment in which each
person will be judged by God. God will not ask where your
parents are, or your children, or your ministers, or your rulers.
God will ask only what you thought of Ilis son, Jesus. The
reason why Paul specified the family as the screening institution
is that family government makes visible a man’s skilis of self-
government in the context of a nearly universal hierarchy.
There are more heads of families than heads of civil govern-
ment. If the family were the nucleus of all government, some-
where in the Bible there would be a law making marriage a
requirement for civil office. Nowhere does such a law appear.
But Rushdoony’s commitment to patriarchalism is greater than
his commitment to biblical law. Hence, he wrote in 1984: “The
biblical form of government requires that men and the families
be trained to govern. The basic government is on the family
level, and all other forms of government rest thereon.”*!

In Politics of Guilt and Pity (1970), he wrote: “The basic gov-
ernment is man’s self-government. Other governments of man
include the family, the church, the school, his business, and

51. Tbid., p. 681.
