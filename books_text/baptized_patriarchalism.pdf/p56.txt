48 BAPTIZED PATRIARCHALISM

We return to the question of excommunication. No one who
had been excommunicated could lawfully attend Passover. The
physical mark of circumcision was judicially irrelevant; the
officially declared judicial status of the excommunicate was the
only relevant legal issue. Only the Levitical priesthood had the
authority to excommunicate. Furthermore, the father or other
household head did not have the authority to invite an excom-
municated son or daughter to celebrate the Passover. The
excommunicate was considered covenantally dead. (Orthodox
Jewish sects continue to this day to have public burials of those
sons who have converted to a rival religion.)

Blood Covenants and Sacramental Marriage

In his chapter on the covenant, Rushdoony affirms: “Be-
cause of God’s covenant law, blood is central to the doctrine of
the covenant.” This is an accurate statement. The question
is: Whose blood and whose covenant?

At long last, we come to the heart of Rushdoony’s new theol-
ogy, which is a variation of a very old theology. It may be the
second oldest theology in history. It is a theology of blood, as
all of Christianity’s meaningful rival religions must be. Histori-
cally, there have been two forms of self-conscious, explicit blood
covenants: biblical religion and patriarchalism-familism. Biblical
religion affirms the necessity of shedding the blood of a judi-
cially clean representative sacrifice: the sovereign mediator.
Patriarchalism-familism also affirms the judicial authority of a
sovereign mediator: the head of the household. It places family
blood ties over all other bonds. You are about to read the most
dangerous and misleading sentence in Rushdoony’s career.

The family is a blood tie; communion celebrates the body and
blood which makes us one family.”

89. id., I, p. 386.
90. Ibid., 11, p. 737.
