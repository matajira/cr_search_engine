2 BAPTIZED PATRIARCHALISM

society. Instead, He identified it as the central institutional
threat to the kingdom of God. Loyalty to the family rather than
to Him, He said, is the great temptation. We must resist it.

Sometime in the early 1960's, R. J. Rushdoony wrote the
following critique of the Roman family. It was published in
1971, but he had written it years earlier when he was a full-
time employee of the William Volker Fund in California.

In early Greek and Roman cultures, paternal power was
religious power, a power continuous with all being and essential-
ly divine, requiring duties of the father and conferring him with
authority. The father, as Fustel de Coulanges has shown, in The
Ancient City, was under law; but, it must be added, he was not
only under law but a part of that law and continuous with it in
the chain of being. He was thus to a degree the law incarnate, in
that he possessed a measure of the ultimate law in his person.
This manifestation of law moved steadily from the father to the
state, so that the state, originally the creature of the family and
of the fathers, made itself the father, and the source of law, with
the family turned into its creature.?

‘The Roman state stcadily absorbed the Roman family under
the Empire. This is the perpetual threat to ail patriarchalism.
The patriarchal system begins with almost total loyalty to the
father, but eventually this loyalty is transferred to the state
because the statc takes over the family’s welfare functions and
its sacramental office. Bread and circuses are provided by the
state. Copulating priestesses replace the father’s lustral rites.

Any attempt to strengthen the family without also strength-
ening the institutional church is self-defeating for Christians.
‘The autonomous family is not an alternative to the state; rather,
it becomes the state’s most important agent. The father repre-
sents the state to his children. The willingness of fathers to send

2. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy (Fairfax, Virginia: Thoburn Press, [1971] 1978), p. 180.
