Baptized Patriarchalism AT

Lord’s Supper honors this judicial fact. The special priesthood
of the institutional church still possesses authority over the rite;
the general priesthood is still subordinate. This was always the
case judicially in Mosaic Israel; the Lord's Supper makes this
judicial reality visible to all. But some people refuse to see.

Rushdoony has remained silent about the implications of this
transformation of outward celebration. Had he ever discussed
the change in celebration, he could not readily have come to
this conclusion: “As we examine the Lord’s Table or eucharist
from the perspective of Scripture, we must recognize that it is
the Christian Passover. The Passover of Exodus is a family rite;
it was oriented to admitting the smallest child able to speak and
understand into the joy of salvation and the meaning of salva-
tion (Ex. 12:21-27). It is no less a family celebration in the New
‘Testament; the family is now Christ’s family.”®*

Judicially, this statement is correct, but it proves the oppo-
site. The Lord’s Supper is no less a family celebration than
Passover was under Mosaic law because, judicially speaking,
Passover never was a rite under the authority of a marital family. It
was always a rite of God’s adopted family: the institutional
church. This is why all the families of Israel had to journey to
a central location to celebrate Passover (Deut. 16:6-7). Passover
in Israel was never celebrated at home. It was celebrated out-
side the geographical jurisdiction of a family’s tribe because it
was celebrated under another tribe’s authority. This authority
was national because it was Levitical: the tribe of Levi. /¢ was
therefore under the authority of the special priesthood. The eleven
non-priestly tribes could not claim any originating authority
over Passover. This means that the general priesthood of Israel,
ie., members of the eleven non-Levitical tribes, could not law-
fully administer Passover apart from the presence of the special
priesthood: the Levites. Like King Jeroboam (I Ki. 12:25-33),
Rushdoony ignores this. Jeroboam, however, was not a familist.

88. Systematic Theology, p. 76. In Institutes, he called it a family service (p. 752).
