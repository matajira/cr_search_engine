46 BAPTIZED PATRIARCHALISM

baptism is in to [sic; he means into] the covenant of our
God.”* This was never a matter of dispute. What is a matter
of dispute is which covenantal agency possesses the right to
baptize. This is a dispute between Rushdoony and (in round
numbers) all the theologians in the history of the church.

He writes: “Like baptism, the Lord’s Table or communion is
rooted in the Old Testament, in the Passover.”** He appcals
to Jesus: “Our Lord's institution of this rite came with the Pass-
over celebration and with His interpretation of the meaning of
Passover as fulfilled in Himself.”

Let us pursue this assertion for a moment. The move from
Passover to the Lord's Supper came in the upper room on the
night before Jesus’ crucifixion, as Rushdoony affirms in Institutes
of Biblical Law.®’ Let me ask an obvious question: Where were
the wives and children of the apostles? Peter had a mother-in-
law (Matt. 8:14); presumably, he also had a wife. His wife was
not in the upper room, nor was his mother-in-law, who dwelt
in his household. Unless Rushdoony is ready to affirm the cel-
ibacy of the apostles, he faces a monumental problem: Passover
was in no way a family rite in the sense of a marital family. The
Head of a new household of faith administered the rite that
night. This household was confessional. Something radical had
taken place in the exterior form of Passover that night, but not
judicially. Jeses did not violate the Mosaic Passover.

Unless the Lord’s Table was a judicially radical break with
Passover — which Rushdoony denies - then this change in out-
ward form points to an inescapable conclusion: the judicial-
covenantal agency of final authority over the Passover was
never the marital family. To the extent that the family adminis-
tered certain aspects of this ritc, it did so, as in the case of
baptism, under authority delegated from the priesthood, The

85. Tbid., p. 735.
86. Ibid.
87, Institutes, p. 46.
