Baptized Patriarchalism 45

As a household priest, the father may have circumciscd his
sons. We are not told this specifically regarding the Mosaic era.
Surely, without specific revelation, we should not draw revolu-
tionary ecclesiastical conclusions from the mere possibility that
the father circumcised his son. But if he did, he did so a dele-
gated agent of the Levitical priesthood. He did not retain the
authority to excommunicate, i.e., judicially revoke the covenant.
This points to the two-fold judicial reality of circumcision. It
was priestly in two senses: general and special. First, the father
representatively invoked the covenant oath in the name of his
son through the rite of circumcision. He had a lawful role as a
father: a general Israelite priest (Ex. 19:6). Second, in invoking
the covenant oath, he affirmed the law of the covenant. As a
general priest, perhaps he could lawfully do this. But a special
priest of the uibe of Levi, not the head of the household, would
determine whether the circumcised son met the stipulations of
the covenant: confession of faith and outward obedience to
God’s law. This identifies both sacraments as ecclesiastical.

Having defied the entire history of the church by proclaim-
ing baptism as a family rite, Rushdoony then condescendingly
announces: “Having said all this, let me add that much of the
church’s teachings on baptism are [sic] very important. The
error has been to limit its implications to the society of the
church, and membership therein.”™ This is as persuasive as a
statement from some dedicated socialist: “Having said all this,
let me add that much of the Austrian School economists’ teach-
ings on the free market is very important. Their error has been
to ground their system on the idea of private property.”

Communion

Having announced the transfer of the authority to baptize
from the church to the marital family, he immediately moves to
a discussion of the Lord’s Supper. He begins: “As we have seen,

84. Ibid., p. 784.
