viii BAPTIZED PATRIARCHALISM

There is another possible mental response: “I don't care
what you say. I don’t care what Bible verses you cite. I don’t
care what evidence you offer. I have made up my mind. I will
not listen to you. I don’t have to listen to you. You can’t make
me listen to you Go away.” This is the mentality of someone
who has joined a cult. If your reaction as you read my book is
anything like this, I warn you: you have already been snared. You
must escape from the snare while there is still time.

Your snare was set in 1956. In that year, a woman divorced
her husband, a pastor. I do not believe she had biblically lawful
grounds to do so. Half of their children — the older ones —
voluntarily accompanied her when she departed. His pastorate
was undermined. He left the pastorate in 1962 to become a
full-time writer. He has long refused to mention his divorce in
public and rarely in private, Instead, he has invented a new
ecclesiology. You may have accepted his ecclesiology. Don’t.

In 1990, another woman sued her husband for divorce. She,
too, took their children and departed. (One went back to his
father.) She placed herself under the authority of the man who
had been the victim of a similar attack in 1956. In 1991, he
created a local church and welcomed her into it. This forced
him to modify his ecclesiology once again. Although she had
never attended college and had never written anything for
publication, he made her the managing editor of his monthly
theological magazine, beginning in the month after her divorce
became final. One year later, in 1994, she quit his church and
left town. This was his risk when he restructured his theology
(again), thereby sanctioning her divorce. He put his trust in the
wrong person. And you may have put your trust in him.

Divorce is a fearful thing. So is creating a new ecclesiology
and a home church to justify this new ecclesiology.

Follow the footnotes. Examine the original sources. See if I
am quoting out of context. Make up your own mind. But be
sure that you do make up your mind. Do not try to defer a
decision. The stakes are too high.
