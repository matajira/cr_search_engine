Baptized Patriarchalism 7

reversed? How can the family be kept from drifting back into
statism should it ever be delivered from today’s bondage?

A Question of Funding

There are three — and only three - covenantal institutions:
church, state, and family. Each is legitimate. Each has a God-
assigned role to play. But when the church is weak, the state
increases its power. The family moves alongside cither church
or state, strengthening the position of whichever institution
seems to be dominant. No better example of this process exists
than tax-funded education. The vast majority of families have
accepted the offer of “free” education. They have demanded it.
The church was the dominant force in education, especially
higher education, for seven centuries. In 1850, every college in
the United States was Christian; a century latcr, almost all were
secular. This got much worse after 1950.? The family has not
been able to resist the lure of tax subsidies. What happened in
ancient Rome is happening in the West.

The lesson should be obvious: the family is not the central
institution of society in the sense of giving direction and vision
io society. It is central only in the sense of having what might
be called the crucial swing vote. It allies itself with cither
church or state. Whichever institution seems to offer healing -—
earthly salvation - will gain the support of the family. The
family cannot independently offer social healing. It looks to
external authorities to do this. The family is called upon to
fund those agencies that promise social healing, cither through
the tithe or taxes. Healing must be funded, and the family is
the source of this funding. But the state’s offer is a false one.

There is an unbreakable rule in institutional theory: the
source of the funding determines the structure of the system, The
source of the funding is either the family or the productive

2. George M. Marsden, The Soul of the American University: Prom Protestant Estab-
lishment to Established Nonbelief (New York: Oxford University Press, 1994).
