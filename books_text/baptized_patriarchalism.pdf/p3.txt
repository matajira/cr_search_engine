Because of the importance I place on the question
of the covenantal relationship between the family
and the local church, I hereby place the entire
contents of Baptized Pairiarchalism mio the public
domain. Anyone may reproduce all or any part of
this book without permission from the author or the
original publisher.
Gary North

Institute for Christian Economics
P.O. Box 8000
Tyler, TX 75711
