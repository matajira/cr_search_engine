8 BAPTIZED PATRIARCIIALISM

individual. The family must support both church and state. It
is therefore judicially subordinate to church and state in the
area of money. The judicial subordination of the family is an inescap-
able concept. It is never a question of family subordination vs. no
family subordination. It is always a question of the degree and
the spheres of family subordination to church and state.

In one sense, it is legitimate 10 speak of the family as the
central institution and above church and state. Imagine three
boxes. The box in the middle is labeled “family.” The box on
the right is labeled “church”; the box on the left is labeled
“state.” The family is elevated above both. Out of each side of
the box labeled “family” is a pipe. Through these two pipes
flows money. Like water, the money runs downhill. So, in this
sense, the family is the central institution and above both
church and state. This, however, is not what the familists have
in mind when they speak of the family as central.

To rest social theory on the idea of the centrality of the
family is to rest on a weak reed. The family is the primary
agency of welfare, but it is not the source of law in society.
‘There are too many families to serve as the single source of law
and judgment. The family’s legitimate sanction ~ the rod — is
not valid outside of its own limited sphere of authority. It does
not lawfully wield the sword (state authority) or the keys of the
kingdom (church authority). Also, church and state can bring
sanctions against the family. The family is not in a position to
bring autonomous sanctions against the state. It is required by
God to pay the tithe. It can rebel against the church, but ex-
communication is a far greater threat to family members than
their threat to cut off funding or quit is to the local church.

He who believes that the family exercises primary authority
in society has not cxamined his tax forms lately. The family
pays; the state collects. The church is owed money, too. One
task of Christian economic theory is to search the Scriptures to
see how much is lawful for church and state to collect.
