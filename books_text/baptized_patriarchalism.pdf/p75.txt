Baptized Patriarchatism 67

Sanctions Applied

God defends His church. Rushdoony attacked it after 1973.
There were immediate sanctions. Pick up a copy of one of his
early books. Read a chapter. You will be impressed by its clari-
ty, its comprehensive, wide-ranging documentation, and the
power of his logic. I recommend The Messianic Character of
American Education and Freud as the best examples of his abilitics
as an essayist. Read /nstitutes of Biblical Law. The footnotes are
impressive; so are the number of new insights per page.

Then pick up anything he published after 1973: Law and
Society or Salvation and Godly Rule or Systematic Theology. The
chapters are short. (“Short” hardly does justice to Law and Soci-
ety, which is subtitled Volume IT of the Institutes of Biblical Law.
The book’s text is 726 pages. There are 160 chapters and 1i
appendixes: four pages per essay.) The footnotes in the later
books are sparse, and they invoke non-scholarly sources com-
pared to the sources cited in his early books: a handful of Bible
commentaries and religious encyclopedias. The vast reading he
has done over the years is not reflected in anything he pub-
lished after 1973. Rather than a series of tightly knit scholarly
essays, which his early books were, his chapters aftcr 1973 are
loosely joined paragraphs connected far too often by unsup-
ported assertions, such as his undocumented appeals to what he
claims are fine points of Greek grammar.

He used to write about history: This Independent Republic, The
Nature of the American System, and World History Notes. He no
longer does. Ihe closest thing to a history book after 1973 was
his Australian lecture series, The “Atheism” of the Early Church
(1983), a 100-page pocket book.

He used to write about the philosophy of education. Com-
pare Messianic Character (1963) with The Philosophy of Christian
Curriculum (1981), which features a slide rule on the cover (a
great collector’s item these days: technologically outmoded by
the hand-held calculator after 1972).
