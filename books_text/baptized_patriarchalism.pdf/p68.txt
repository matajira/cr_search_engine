60 BAPTIZED PATRIARCHALISM

church publicly excommunicated in the last, say, two decades?)
Reminder: Rushdoony has spent his entire career attacking
mercilessly {and justly) the modern church’s apostasy. Now he
writes: “By contrast, let us look at the over-governing church,
the church whose gospel! is excommunication.” Got that?
The over-governing Protestant church in the late twentieth century. He
then returns to the non-existent grammatical difference be-
tween discipline and disciple: “Excommunication has its place
in the fife of the church, but not as a_ substitute for
discipling.”'* He announces a law I cannot seem to find in
the Bible: a church that frequently excommunicates people is a
weak church. “Indeed, a heavy use of excommunication indi-
cates commonly the lack of a sound teaching ministry. Coercion
replaces teaching and covers up the failure of the ministry.
Coercion not only replaces teaching but also the Holy Spir-
it." (Covenant-breakers in the church will applaud this!)
“For the church to take the same coercive route is to despise its
teaching ministry and to treat with contempt the power of the
Holy Ghost. Authority is net gained by putting on a garb or by
the fiats of a consistory[,] session, presbytery, or board but by
putting on Christ, by being under his authority.”

If this were true — and there in nothing in the Bible to indi-
cate that it is true, nor does he cite any other author in this
section (“Man's Relationship to Authority”) — it would still not
answer the crucial question: Which agency possesses the exclusive
authority to excommunicate? If there is no exclusivity, then every
time two or three people are gathered together in Christ’s
name, someone’s right to the Lord’s Table could be at risk. But
such sporadic gatherings confer no authority to make judicial
pronouncements. The attempt by any little prayer group to do

107. Systematic Theology, p. 1141.
108. Ibid.

109. #id., p. 1142.

110, Ibid,
