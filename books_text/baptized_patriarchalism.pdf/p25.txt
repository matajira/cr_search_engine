Baptized Patriarchalism 17

from biblical law? Can they lawfully ignore a creation ordinance
{the law against “hybridization”)? Racism spreads.

Rushdoony’s analysis here is not concerned with a Mosaic
case law governing state authority. His analysis rests on an all-
inclusive principle: a creation ordinance. He has announced the
existence of a creation ordinance in order to justify a view of
marriage based on community standards of order and propri-
ety. His assertion of the existence of a creation ordinance man-
dating racial separation based on community standards is an
affirmation of a theory of society that he elsewhere opposes so
eloquently: John Dewey's view of community standards and
community authority.'* Humanism spreads.

In Christ, the only valid standards for judicial exclusion in a
formally covenanted church, state, or family are: (1) denying
the Trinitarian faith (confession); and (2) repeatedly breaking
God’s law as a way of life. Race is not a valid standard for cov-
enantal exclusion. Whenever race becomes a means of exclusion
within any covenantal organization that is bound by a common
confession of faith, this works against the ideal of the biblical
covenant. While there is no earthly court-enforceable biblical
law against voluntary separation from others outside the boun-
daries of the church, there can be no morally valid judicial
exclusion of any race from the rights and obligations of the
ecclesiastical covenant. The same is true of family and state. A
program of court-enforced racial segregation within any covenantal
institution is judicially evil. If a high court annuls such segrega-
tion within its covenantal jurisdiction, this must not be dis-
missed as a program of mandatory racial integration. The court
merely prohibits a judicial evil: mandatory racial segregation.”

16. On Dewey, see Rushdoony, The Messianic Character of American Education:
Studies in the History of the Philosophy of Education (Nutley, New Jersey: Craig Press,
1963), ch. 15.

17. ‘This should not be understood as a legitimation of laws requiring businesses
to serve people or hire employees irrespective of race. Economic discrimination is not
a covenantal act. If a business decides to hire or not to hire people of a certain race,
