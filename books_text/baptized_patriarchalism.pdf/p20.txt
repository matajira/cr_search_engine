12 BAPTIZED PATRIARCHALISM

eternity than the non-Christian family. But when conservative
Christian theologians begin to discuss social theory, most of
them begin to waffle. They tend to affirm the family as society’s
central institution. In a non-Christian society, this may be the
case operationally, The relevant theological question, however,
is this: In God’s design for Ilis holy commonwealth — the visible
social manifestation of the kingdom of God - is the family the
central institution? The correct answer is no. The institutional
church is the central institution, for it alone possesses the au-
thority to excommunicate: the most fearful sanction in history.
God has turned this authority over to His institutional church.
“And fear not them which kill the body, but are not able to kill
the soul: but rather fear him which is able to destroy both soul
and body in hell” (Matt. 10:28). Through excommunication, the
institutional church lawfully declares God’s judgment.

Which father’s wrath is more terrifying: God the Father or
a family patriarch? Which father’s authority is absolute? Which
family is the model: the institutional church or the marital
family? To ask these questions is to answer them, or so you
might think. You would be wrong. That is why this little book
has become mandatory.

The Bible nowhere says that the patriarch has the power of
the keys. The institutional church does. In history’s representa-
tive matters of eternal salvation, fathers have nothing covenant-
ally binding to say; church elders do. You would think that no
Christian theologian would argue otherwise. You would be
wrong. That is why this little book has become mandatory.

Jesus was clear: He was at war with any ideal of the kingdom
of God which would place the marital family at the center of
either formal worship or faith. He was therefore at war with
any definition of His church which would transfer the sacra-
mental monopoly of the institutional church to the marital
family. Men can either get behind Jesus on this issue or they
can take their stand in opposition to Him. I strongly recom-
mend getting behind him. Otherwise, you will be run over.
