14 BAPTIZED PATRIARCHALISM

common culture, not just a common profession of faith, how
can the racism inherent in Rushdoony’s familism not become a
factor in his ecclesiology? The family administers the sacra-
ments in Rushdoony’s ecclesiology, as we shall see. The family
in his system is the most visible manifestation of the church.

Laws Against “Hybridization”

This was not some one-time error on Rushdoony's part. In
his exposition of Leviticus 19:19 as a law prohibiting hybridiza-
tion, which it was not,’ he concludes: “St. Paul referred to the
broader meaning of these laws against hybridization, and
against yoking an ox and an ass to a plow (Deut. 22:10), in IT
Corinthians 6:14." Broader meaning, yes; judicial specifics,
no. Paul wrote: “Be ye not unequally yoked with unbelievers:
for what fellowship hath righteousness with unrighteousness?
and what communion hath light with darkness?” The issue here
is faith. Christian faith overcomes all other divisions.

Rushdoony says that the hybrid comes at great cost — sterility
— “and thereby violates God’s creation ordinance.”!! He identi-
fies the prohibition against genetic mixing within a species as a
ereation ordinance rather than a temporary ordinance governing
tribalism in national Israel. Then he adds that “the command-
ments clearly require a respect for God’s creation.”

Second, Rushdoony writes: “But Deuteronomy 22:10 not
only forbids unequal religious yoking by inference, and as a
case law, but also unequal yoking generally... . The burden of
the law is thus against inter-religious, inter-racial, and inter-

9. The law prohibited mixing of seeds in a man's field. It was a law illustrating
the requirement that the tribes in Israel be kept separate. This aw was annulled by
the New Govenant, when the church replaced Mosaic Isracl. Gary North, “Herme-
neutics and Leviticus 19:19 ~ Passing Dz. Poythress’ Test,” in North, ed., Theonomy:
An Informed Response (Tyler, Texas: Institute for Christian Economics, 1991), ch. 10.

10, Rushdoony, /nstitules, p. 256.

11. Tbid., p. 285.

12. Tid.
