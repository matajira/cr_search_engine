Baptized Patriarchalism 55

earth shall be loosed in heaven. Again I say unto you, That if
two of you shall agree on earth as touching any thing that they
shall ask, it shall be done for them of my Father which is in
heaven. For where two or three are gathered together in my
name, there am I in the midst of them (emphasis added).

He asserts that this passage “is normally wrenched out of its
context.” He offers no citation to anyone who has wrenched
these verses out of context. He continues: “The vy, 15-20 are
usually seen as the classic text on church ‘discipline,’ and, un-
happily, by discipline men usually mean punishment or chas-
tisement. The key concept in the word discipline is disciple, a
very different idea.” Here he does it again: he bases his
theology on a fine point of grammar — a non-existent point.

According to Kittel’s Theological Dictionary of the New ‘Testa-
ment, the Greek word for disciple indicates a master-servant
relationship, either philosophically or religiously. New Testa-
ment usage indicates a highly personal relationship (vol. IV,
pp. 44142). The disciple was a man under authority. Simple.

Because they were under Christ’s absolute authority, the
disciples were given power over the supernatural: “And when
he had called unto him his twelve disciples, he gave them pow-
er against unclean spirits, to cast them out, and to heal all
manner of sickness and all manner of disease” (Matt. 10:1).
Imposing negative sanctions against demons and positive sanc-
tions on the physically afflicted was Jesus’ way to persuade
them and others of the authority that God gave to them. Since
they were all under the threat of eternal judgment — as Judas
discovered (Luke 22:22) - they were given power to impose
sanctions. But Rushdoony does his best to avoid the obvious
conclusion: discipline and disciple are linked biblically in terms of
sanctions. The disciples were under covenantal sanctions, so they

96. Ibid., p. 757.
97. Ibid., pp. 757-58.
