32 BAPTIZED PATRIARCHALISM

many private associations as well as public opinion.” This
was the ideal of government that had attracted his early associ-
ates, In Institutes of Biblical Law, he also began with self-govern-
ment under God. “Government means, first, self-government,
then the family, church, state, school, calling, and private asso-
ciations as well as much clse.”** But much later in the book,
and perhaps three years later in terms of when he wrote this
passage, he began to modify his earlier position. “The basic
government of man is the self-government of Christian
man.”*4 But a hint of a shift in his perspective — a cloud no
larger than a man’s hand ~ immediately followed: “The family
is an important area of government also, and the basic one.
The church is an area of government, and the school still an-
other.” Notice: he used the word basic for both self-government
and family government. This equality could not survive indefin-
itely. In Systematic Theology, he moved the family to first place.
This represented a major shift away from his original theology.
He now places an institution at the center of both his social
theory and his theology; before, his social theory had rested on
the principle of self-government under God's law. This pro-
posed ceniral institution is not the church. It is the church’s
oldest rival, the one Jesus had warned against most strongly.
He attempts to ground his patriarchal theology on grammar,
but he offers no proof for his grammar. “The main office, that
of elder, is the name of the head of a family.”* But there is no
verse in the New Testament that refers to elder as the head of a
family. Luke 15:25 refers to an older son. Presbuteros usually
refers to a church office. Bauer’s definitive lexicon offers no
example of presbuteros as a head of family, either in the New
Testament or Greek literature. The word means what it means

52. Rushdoony Politics of Guilty and Pity (Fairfax, Virginia: Thoburn Press,
11970] 1978), p. 144.

53. Institutes, p. 240.

54, Ibid., p. 72.

55. Systematic Theology, p. 685. In Institutes, he said it could mean this: p. 740.
