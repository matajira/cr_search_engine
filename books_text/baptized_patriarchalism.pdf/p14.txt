6 BAPTIZED PATRIARCHALISM

Messianic Character of American Education in 1963, Rushdoony’s
next major writing project was The One and the Many, although
it was not published until 1971. He was working on the manu-
script of The One and the Many in the summer of 1963 when we
both worked for the William Volker Fund. The book's early
chapters were completed in the mid-1960’s. In his chapter on.
“Christ: The World De-Divinized,” Rushdoony warned against
patriarchalism as a precursor to the divinization of the state. I
have already cited his statement in my Introduction. He said
that the Roman state absorbed the Roman family, making the
family the state’s creature. Rushdoony understood clearly that
the authority of the family is not sufficient to keep it from being
absorbed into the state and used for the state’s purposes.

What should we conclude? ‘This: the family is a legitimate
and necessary institution, but separated from the institutional
church, it has been no match for the state in history. The high
point of the state in Western history, and the low point of the
family, was during the Roman Empire, when Jesus issued his
warning. Not until the twentieth century has the family in the
European West been more oppressed by the state.

The West needs a solution to this question: What are the
biblical limits of State authority? Christian Reconstruction has been
dealing with this problem for over two decades. Its preliminary
answer is this: the tithe sets these limits. Civil government at all
levels combined is not authorized by God to collect taxes equal
to the tithe (I Sam. 8:17). Nothing funded by the state beyond
this limit is biblically legitimate. Taxation in the twentieth cen-
tury has exceeded this limit by at least three to one in every
nation. The modern world stands condemned.

If the state must be shrunk, what should replace it? The
secular conservatives’ most popular practical answer to this
question is this: the family. The problem is, history does not
move backward. The Western family was very strong prior to
the Enlightenment, but it has surrendered to the state, gen-
eration by generation. Ilow can this process of surrender be
