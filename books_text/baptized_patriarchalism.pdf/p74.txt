66 BAPTIZED PATRIARCHALISM

As one example of the inevitable confusion that such a cov-
enantal mixture produces, consider a very real possibility that
Rushdoony never mentions. If the head of the household can
lawfully administer the sacraments, what if she is a widow with
only minor sons? Because primary authority in Rushdoony’s
system is based on biological eldership within a marital family,
ihe husband's death transfers his judicial office to his wife.
Rushdoony’s version of patriarchalism can become matriarchalism. If
the vision of a household priestess administering the sacramen-
tal rites seems suspiciously similar to Canaan and its cults, there
is a valid theological reason for this suspicion.

There are many hidden agendas in Rushdoony’s doctrine of
the church, but the one that has been the most hidden is also
the oldest onc: the ecclesiastical implications of his divorce and
the voluntary departure of his three older children with their
mother in 1956. His pastorate never fully recovered; he quit in
1962 to write full-time. That the Orthodox Presbyterian Church
allowed him to retain his ordination after 1956 is not relevant
to his formal ecclesiology. After he left the OPC, he dismissed
the “Orthodox Pharisees Church” (OPC) for its “endless nit-
picking about trifles.”"© What is relevant for his ecclesiology
is his insistence that I Timothy 3 must govern ordination to the
eldership, which is a family office in his system. Somewhere in
his writings there needs to be a discussion of the consequences
for ordination of a divorce and the open rebellion of half of a
man’s children. This is not too much to ask of someone who
has publicly dismissed the church’s teaching on baptism as
proof of the church’s “hardening of the arteries.”

Rushdoony’s failure to answer this crucial judicial question
reflects his inability to deal with it personally. Decade by de-
cade, his refusal to face this problem undermined both his
ministry and his theology. The result is his Systematic Theology.

116, Rushdoony, God's Program for Victory (Fairfax, Virginia: Thoburn Press,
1977), p.9.
