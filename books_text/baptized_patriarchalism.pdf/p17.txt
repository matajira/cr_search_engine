Baptized Patriarchalism 9

Rushdoony understood this two decades ago. In Institutes of
Biblical Law, he cited a pseudo-Augustinian sermon regarding
tithe vs. taxes. “Our ancestors had more than they needed
because they gave God tithes and paid their taxes to the Em-
peror. However, since we do not wish to share the tithes with
God, everything will soon be taken from us. The tax collector
takes everything which Christ does not receive.” In short, no
tithe to the church — no protection from the tax collector.

But Rushdoony’s theology had a flaw in it. At first, it did not
seem to be too dangerous. In the last two decades, it has com-
pietcly undermined the biblical foundation of his social theory.
Rushdoony believes that the tithe-payer has the authority under God to
allocate his tithe as he sees fit. If the tithe-payer wishes to send his
tithe money to a non-profit foundation chartered by the gov-
ernment rather than to his local church, according to Rush-
doony, he has this authority. This undermines the church.

Rushdoony insists that no church can lawfully compel its
members to pay it their complete tithe or even any portion
thereof. “It is significant, too, that God’s law makes no provi-
sion for the enforcement of the tithe by man. Neither church
nor state have [sic] the power to require the tithe of us, nor to
tell us where it should be allocated, i.e., whether to Christian
Schools or colleges, educational foundations, missions, charities,
or anything else. ‘The tithe is to the Lord.”* With respect to
tithing, Rushdoony teaches the divine right of the head of the house-
hold: no earthly appeal beyond conscience. This is familism.

Familism

Familism has been a common heresy throughout history. In
the modern world, it is far more common among secular con-

3. Rushdoony The Institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1978), p. 512,

4. Rushdoony, “The ‘ax Revolt Against God,” Position Paper 94, Chalcedon
Report (Feb. 1988), p. 16.
