Baptized Patviarchalism 41

Authority

Then what about church discipline? This is the missing piece
of the puzzle in Rushdoony’s ecclesiology. Ile denies that the
Mosaic priests ever possessed such authority. “A careful exami-
nation of the Old Testament gives no evidence of any govern-
ing power over men by the priests of Israel; that such a power
developed later, is an aberration, not an aspect of Biblical law.
Authority did exist among pricsts, some ruling over others,
because authority is common to every realm. It is not a specific
attribute of any one realm.””

This is fishy ~ six days in the pantry in summer fishy. If the
priesthood had no authority, then who was it who cut people
off for their rebellion? The phrase, cut off from their people, ap-
pears over and over in the Mosaic law, This sanction was not
imposed by the civil magistrate. It refers to excommunication.
It was imposed by the ecclesiastical government. The following
were not capital offenses. The state had no authority to impose
sanctions in these cases. Perhaps God might; the state did not.

Seven days shall ye eat unleavened bread; even the first day
ye shall put away leaven out of your houses: for whosoever eat-
eth leavened bread from the first day until the seventh day, that
soul shall be cut off from Israel (Ex. 12:15).

Seven days shall there be no leaven found in your houses: for
whosoever eateth that which is leavened, even that soul shall be
cut off from the congregation of Israel, whether he be a strang-
er, or born in the land (Ex. 12:19).

But the soul that eateth of the flesh of the sacrifice of peace
offerings, that pertain unto the Lorp, having his unclcanness
upon him, even that soul shall be cut off from his people (Lev.
7:20).

79. Systematic Theology, p. 725.
