We Must Render Appropriate Service 109

Rendering to Caesar

Because civil governments are ordained by God and act as ser-
vice institutions, for they are ministers of God (Romans 13:4), they
are in need of tax money to pay for the rendered services. Jesus
states that Caesar is due tax money because he offers them protec-
tion against foreign enemies. Caesar renders a service: “Render to
Caesar the things that are Caesar’s; and to God the things that are
God's” (Matthew 22:21), Jesus certainly was not endorsing the
way Caesar governed in all cases, but He was, at least, upholding
the Biblical institution of civil government and its authority to
limited taxation. Of course, those “things that are Caesar’s” are
not his by edict. Rather, they fall within the parameters of God’s
ordination of Caesar’s jurisdiction. Jesus was not giving carte
blanche authority to Caesar as the civil representative of the State.

But what about those who maintain that the State is taking
more than its God-ordained share? Should we still pay the tax?
We can afford to pay. If we are faithful, God will provide. When
Peter asked about the tax, Jesus provided a fish with a coin in its
mouth (Matthew 17:24-27). Christians are on the winning side. In
effect, we are paying the taxes to ourselves. We are maintaining
an orderly society for the day when Christians will faithfully carry
out the dominion mandate.

Of course, once Christian dominion is a growing reality, the
need for taxes will decrease because the State will once again be a
protector and not a provider. Some nations tax over 100% of in-
come! While we should not be satisfied with our tax rate, we
should thank Ged that it’s not worse. At every opportunity we
should work to cut taxes and reduce expenditures. There are
more significant battles to fight (e.g., abortion, church-State rela~
tions, the building up of the church and family, and Christian
education). For the most part, the courts are stacked against the
tax protester. What happens if the court rules against you? You
will still have to pay the tax as well as penalties and interest. You
might even lose your house. This says nothing about what might
happen to your family if you are put in jail for a year or two. I
want to make it clear that there are certain non-negotiable items
