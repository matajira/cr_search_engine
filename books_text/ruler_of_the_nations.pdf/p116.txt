96 Ruler of the Nations

be simply whatever a ruler autonomously desires or thinks it
ought to be. The standard of good and evil is nothing less than
that which the Creator, Sustainer, Ruler, and Judge of heaven
and earth ordains, decrees, and declares it to be: the revealed
Word and law of God.

The psalmist declares he “will speak of Thy testimonies before
kings, and shall not be ashamed” (Psalm 119:46). These testimon-
ies are the “commandments” that he loves (v. 47). Jesus informs
His disciples that persecution will give them opportunity to speak
“before governors and kings . . . as a testimony to them and to
the Gentiles” (Matthew 10:18).

Civil servants approached John the Baptist regarding their ob-
ligations to the law of God: “Some tax-gatherers also came to be
baptized, and they said to him, ‘Teacher, what shall we do?’ And
he said to them, ‘Collect no more than what you have been
ordered to.’ And some soldiers were questioning him, saying, ‘And
what about us, what shall we do?’ And he said to them, ‘Do not
take money from anyone by force, or accuse anyone falsely, and
be content with your wages’” (Luke 3:13-14). John was not
appealing to them on the basis of some “neutral” law, but referred
to the sixth, ninth, and tenth commandments of the Decalogue,
though he did not name them as such.

An incident in Jesus’ ministry shows that the Biblical laws of
restitution are in force for tax thefts. Zaccheus, an unscrupulous
tax collector, followed the laws of restitution by promising to pay
back those he defrauded: “If I have defrauded anyone of anything,
I will give back four times as much” (Luke 19:8; cf. Exodus 22:1;
Leviticus 6:5). Christians are obligated to inform those who rule
of the demands of the law and the consequences of disobedience.
There is no area of life where man is exempt from the demands of
the law of God.

Blessings and Curses

Because God’s laws are a standard for all nations, conse-
quences of disobedience affect pagan nations as well as godly na-
tions. External blessings accrue to societies that conform to the
