254

government
biblical definition, 6, 14, 15
ecclesiastical, 16
family, 187
Grant, George, xv, 184

Hamilton, Alexander, 23
Hegel, 6
hierarchy
biblical, 31-32, 35-36
bottom-up, 27, 169
Egypt's, 28
Israel’s, 32
necessity of, 28
top-down, 207
Tower of Babel, 29, 30
history, 226
Hitler, Adolf, 6
Hodge, A. A., 178
Holmes, Oliver Wendell, 206-207
Home Education and Constitutional
Liberties, 202
homoeracy, x
homosexuality, 64, 212
Hopper, Grace, xviii
humanism, 97, 147, 148, 151,
214, 233-35

individualism, 58-61
Inherit the Earth, xiii
The Institutes of Biblical Law, 181, 186

The Institutes of the Christian Religion, 105

Israel, example of
bottom-up hierarchy, 27
domino effect, 9
lack of self-government, xvi
slavery, 72

Jay, John, 23
Jefferson, Thomas, 233-35
Jordan, James, 123
judgment

of Adam and Eve, 55

Ruler of the Nations

of Cain, 55
collective, 56
delaying, 57
the flood, 54
of nations, 57-58
saving a remnant, 56
of Sodom, 55-56
of Sodom and Gomorah, 54
“Tower of Babel, 54
jurisdiction
of the church, 34, 91-92, 135-36
of the family, 34, 135-36
limits of, 138
of the State, 34, 76-82, 91-92,
102-105, 135-36, 207
jury, 218-19

Kirk, Paul J., Jr., 148, 149
Knight, Albion, 79, 108

Kramer, Rita, 190
Kuehnelt-Leddihn, Erik von, 175

Ladd, George, 155
law
administrative, 219-221
God's given, 44
natural, 47-51
leadership qualifications, 112-13
leftism, 175

Lenin, 19
Lewis, C. §., x, 205
Lex Rex, 21, 93

Locke, John, 47

MAD, 108

Madison, James, 23

Magna Carta, 218

Marshner, Connaught, 48, 49-50

Mars, Karl, 184

Marxism, 19

Moses and Pharaoh: Dominion Religion
Verses Power Religion, 50

Mussolini, Benito, 6
