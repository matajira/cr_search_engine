228 Ruler of the Nations

In the name of Gad, Amen. We, whose names are underwrit-
ten, the loyal subjects of our dread sovereign lord King James, by
the grace of God, . . . having undertaken for the glory of God
and advancement of the Christian faith, and the honor of our
king and country, a voyage to plant the first colony in the north-
ern parts of Virginia; do by these presents, solernnly and mutually
in the presence of God and one another, covenant and combine
ourselves together into a civil body politic, for our better ordering
and preservation and furtherance of other ends aforesaid .. . .*

The First Charter of Virginia emphasizes the Christian char-
acter of the infant nation:

We, greatly commending and graciously accepting of their
desires for the furtherance of so noble a work, which may, by the
providence of Almighty God, hereafter tend to the glory of His
Divine Majesty, in propagating of the Christian religion to such
people, as yet live in darkness and miserable ignorance of the
true knowledge and worship of God. . . 5

The Fundamental Orders of Connecticut, drafted in January
14, 1639 at Hartford, was the first written constitution that created
a civil government. It reads in part:

Forasmuch as it has pleased Almighty God by the wise dis
position of His divine providence so to order and dispose
[these] .. . lands... ; and well knowing where a people are
gathered together the Word of God requires that to maintain the
peace and union of such a people there should be an orderly and
decent government established according to God, to order and
dispose of the affairs of all the people at all seasons as occasions
shall require; do therefore associate and conjoin ourselves to be
as one public State or Commonwealth, and do, for ourselves and
our successors and such as shall be adjoined to us at any time
hereafter, enter into combination and confederation together, to

4. William Bradford, “Mayflower Compact,” History of Plymouth Plantation, pp.
106-107.

5, “First Charter of Virginia,” April 10, 1606, Documents of American History, 6th
edition, ed. Henry Steel Commager (New York: Appleton-Century-Crofts, Inc.,
1958), p. 8.
