140 Ruler of the Nations

Church leaders in the New Testament are to exhibit similar ethi-
cal qualities and real life experience (1 Timothy 3:1-7). Being “above
reproach” can be compared to “men of truth.” “Those who hate dis-
honest gain” is similar to being “free from the love of money.” An
“experienced” man is someone who is “not a new convert.”

Parallel Jurisdictions

Moses became the chief judicial officer in Israel, assisted by
numerous lesser magistrates (Exodus 18:17-26). Aaron, Moses’
brother, became the chief ecclesiastical officer as High Priest, as-
sisted by numerous lesser priests (Leviticus 8).

In the days of the Judges, Othniel, Ehud, Shamgar, Gideon
and Samson served as political officers (Judges 1-13), while the
son of Micah, Phineas, Eli, and the Levites served in an ecclesias-
tical capacity (Judges 17; 20:28; 1 Samuel 1-8).

During the period of the monarchy, King Saul served in a civil
capacity while Ahimelech ministered as the chief ecclesiastical
leader in the nation (1 Samuel 10 and 21). There was King David
and Priest Abiathar (1 Chronicles 15:11), King Solomon and Priest
Zadok (1 Kings 1:45), King Joash and Priest Jehoiada (2 Kings
18), and King Josiah and Priest Hilkiah (2 Kings 22:4).

Even after the return from exile, church and State as parallel
institutions operated with Governor Nehemiah (Nehemiah 7) and
Priest Ezra (Nehemiah 8). This jurisdictional cooperation cul-
minated in the priestly office of Joshua and the civil office of
Zerubbabel (Zechariah 4:14).

All Are Ministers

The New Testament describes leaders in the church and State
as “ministers” (Mark 10:42-45 and Romans 13:4). Even when
describing the role of the civil magistrate, the Greek word for
“deacon” is used.

The word underscores the ruler’s duty to serve, rather than to
“lord it over” those under his authority. Dominion comes through
service. The civil “minister” rules for our “good” and he is “an
avenger who brings wrath upon the one who practices evil”
