6 Ruler of the Nations

The kingdom of God will conquer them all: *. . . the God of
heaven will set up a kingdom which will never be destroyed, and
that kingdom will not be left for another people; it will crush and
put an end to all these kingdoms, but it will endure forever” (Dan-
iel 2:44). God’s kingdom is a kingdom of the heart, not a top-
down centralized power that crushes the lawful desires of men,
thereby creating rebels.

If a house is divided against itself, it cannot stand. Man’s poli-
tical houses are divided against themselves. They shall not stand.

A Different Kind of Domino Effect

There is a governmental domino effect that also seems to be
facing nations. Government too often has been defined solely in poli-
tical terms. The Bible’s definition of government is comprehen-
sive. It includes self-government, family, government, church
government, and civil governments at all jurisdictional levels,
There are those, however, who want to narrow the definition, and
thus create a domino effect: the denial of God's sovereign rule over
all of life, and the denial of all earthly governments established by
God—parents and children in family government, elders and
members in church government, and rulers and citizens in civil
government. In effect, the State,! civil government, is the last
domino to fall. All governmental distinctions are eliminated in
favor of the State, The State is everything. For Hegel, “The State
incarnates the Divine Idea upon earth.” In effect, the State is god.
Benito Mussolini declared, “Everything for the State; nothing
outside the State; nothing against the State.”

Adolf Hitler, one of the greatest tyrants who ever lived, under-
stood the domino effect. It begins with the elimination of the indi-
vidual will for the benefit of the State:

1. State with an uppercase S will refer to the federal or national civil govern-
ment of any nation or a single political entity. For example, since the Soviet
Union cannot be described in constitutional terms, the term State will refer to the
oppressive political system of that regime. The use of state with a lowercase s
refers to one of the fifty states.
