198 Ruler of the Nations

deferred gratification, What does it really cost to play a video
game? Get them started saving for college. Let them see you pay
the bills each month. Teach them the principles of debt-free living
or at least short-term debt living. Take them shopping to teach
them how to look for the best deals, not the cheapest items. You
get what you pay for.

Teach your children that welfare begins in the home. They
must learn to care for themselves. This will mean working for a liv-
ing. Even if you have money to lavish on your children, don’t. If
you must give them something expensive, give them a useful éool.
Make it clear that you are doing everything in your power to care
for yourselves as you approach old age but if anything unforseen
happens, your children are going to be responsible, not the State.

Teach them to tithe ten percent of their money income (in-
cluding gifts). Teach them to save ten percent, too. It doesn’t seem
fair or reasonable to force them to tithe and save the equivalent
money value of Christmas and birthday presents, but they can
tithe and save Christmas and birthday money, Since they have
been given the authority to spend this present as they please, they
have been given a new degree of responsibility.

The primary wage earner should prepare for the possibility of
a premature death. A term life insurance policy of at least $250,000
is necessary. You should be able to decrease the amount as you get
older and your responsibilities decrease. Also, leave an inherit-
ance to your children, Disinherit the unfaithful.

Work for the elimination of the Social Security program. Look
what a family could do on its own: a wife can buy a $250,000 term
life insurance policy for her 25-year-old husband for under $20 a
month. This will protect her family in the event of his death.
While the husband is still alive, and his wife and family are pro-
tected by the policy on him, the family could save nearly $2,500
plus interest that would have gone to the Social Security program.
All in all, the family would be much better off financially. As he
gets older the cost of term insurance increases but the need for
such coverage decreases if an individual has saved a percentage of
his income every year.
