xviii Ruler of the Nations

Summary

Only the innovation and flexibility of self-governed people
under the rule of God’s law can sustain the growth of God’s king-
dom over time. Men will be ruled by God, or else they will be ruled
by men who imitate God. There is no escape from the rule of
other men; the question is: By what standard will rulers rule, and
also be governed? Everyone is under another person’s authority in
most areas of his life. Everyone answers for his actions. The doc-
trine of “divine rights” applies only to God: God alone answers to no
one else. There is no divine right of kings, priests, parents, or vot-
ers. There is no divine right of anyone on earth, Everyone is ac-
countable to other people. But this accountability is judicial: an
appeals court system. Initiative remains with the individual. In the im-
mortal words of Grace Hopper, a developer of the computer lan-
guage Cobol, and who in her late seventies in the early 1980s was
the oldest officer still on active duty in the U.S. Armed Forces
(Navy): “It’s easier to say you're sorry later than to ask permis-
sion.” This outlook is the essence of self-government under God.
Ask God; then say you are sorry to men later on, if necessary.
