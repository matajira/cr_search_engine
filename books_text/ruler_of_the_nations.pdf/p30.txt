10 Ruler of the Nations

God was rejected as their sovereign redeemer. Instead of
“craving” God, they “craved evil things.” Instead of worshipping
God, they became “idolaters.” Their rejection of God started the
dominoes falling. Forty years were wasted in the wilderness as the
people ignored God's sovereign government and endured His
judgment (Numbers 13-14). An entire generation died in the wil-
derness. A new generation entered the land and again were warned
by God: “This book of the law shall not depart from your mouth,
but you shall meditate on it day and night, so that you may be
careful to do according to ail that is written in it; for then you will
make your way prosperous, and then you will have success”
(Joshua 1:8).

After the conquest, when a new generation was about to take
over the reins of dominion, Joshua repeated the earlier admoni-
tion: “Now, therefore, fear the Lorp and serve Him in sincerity
and truth; and put away the gods which your fathers served
beyond the River in Egypt, and serve the Lorp. And if it is dis-
agreeable in your sight to serve the Lorn, choose for yourselves
today whom you will serve: whether the gods which your fathers
served which were beyond the River, or the gods of the Amorites
in whose land you are living; but as for me and my house, we will
serve the Lorv” (Joshua 24:14, 15).

Summary

In our day, we have imitated the Israelites in their sin, not
their faithfulness. We are facing similar problems. At all levels,
governments (plural) are experiencing crises: crises in authority,
standards, financing, and stability. Until men turn to the Biblical
standards for righteous government, beginning with self-govern-
ment under God’s law, there will be no long-term solutions. The
conflicts between man’s empires will continue. The dominoes will
continue to fall, threatening whole civilizations,

But Christians have forgotten their Biblical heritage. They
stand silent, yet they have the Bible in their hands. They must
turn to it again for guidance, and ask themselves: What does it mean
to disciple the nations for Christ? It means that we must announce that
Jesus Christ is ruler of the nations.
