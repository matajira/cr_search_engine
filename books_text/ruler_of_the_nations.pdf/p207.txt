Reconstructing Family Government 187

With the collapse of the family or the denial of the family’s au-
thority, a nation rapidly moves into social anarchy. God, as the
source of the family’s authority, has located immediate authority
in the father or husband (1 Corinthians 11:1-15). When the father
abdicates, or certain “interest groups” deny his authority, social
rebellion, as described in Isaiah 3:12 results. Women become rul-
ers and children are looked upon as authority equals with the
same rights and privileges as their parents. Parents are oppressed
in such a view of authority. A crisis in leadership forms (cf. Judges
4). The result is social collapse and captivity (Isaiah 3:16-26). For
women, it is a time of “reproach” or “disgrace” (Isaiah 4:1).

The first government we recognize as children is the family. If
parents lose the respect and obedience of their children within the
family, they most certainly are going to lose them to the broader
culture. Some of what I'm suggesting might not seem to relate to
“Jesus as Ruler of the Nations,” but I assure you it does. We're
talking about family government. What your children learn in the
home will be taken to the work place and the political arena. Our
job is to produce leaders, not followers.

Family discipline is essential to good government. Children
must learn Biblical principles relating to self-government so that
they eventually will not reject family government for the proposed
security of the State. One primary way we can discipline children
is by teaching them Biblical truth about private property. Too
often we ask the State to confiscate money from the more prod-
uctive members of society to redistribute it to the less productive
members, presumably in the name of fairness and justice. Gary
North writes, “If the concept of private property is worth defend-
ing, and if personal responsibility is the moral basis of private
property, the family must be the scene of the child’s introduction
to the responsibilities of ownership.” Money received in transfer
payments comes from other families. In effect, some families use
State power to plunder other families.

3. Gary North, “Ownership, Responsibility, and the Child,” The Freeman
(September 1971), pp. 515-21.
