Reconstructing Family Government 193

to wives to obey their husbands (Ephesians 5:22-24). Then he
goes on to warn children to be obedient to parents (Ephesians
6:1-3).

The familiar phrase of mothers, “Wait till your father gets
home!” is proper. Fathers must back up the threats of mothers. As
soon as the father walks through the door at night, the wife must
immediately inform him of any major infraction of the rules.
Judgment should not be delayed. This may not please fathers,
who want a little peace and quiet at home, but peace and quiet are
not automatic benefits given by God to those whose work is not
yet done. The child who is to be dealt with must not learn that he
can avoid punishment by becoming an instant nice person when
he hears father’s car in the driveway.

Mothers are not to defer all judgment to fathers. The normal
transgressions of children, especially younger children, must be
dealt with immediately, or they will forget. They have very short
memories, Only as they grow older, can they fully appreciate the
threat of father’s arrival hours later. Furthermore, the judgment of
fathers is traditionally more severe, so children may spend the day
worrying. They should worry this way only when infractions are
major. But if they really are major, then the day of worrying is ap-
propriate. Had Adam and Eve worried more before God’s return,
they might have come to repentance before full judgment was
handed down.

Mothers, if they do not have to work, should try to stay home
during the child’s pre-school years, especially if the children will
have to be put in day-care centers. Fathers should take extra care
in spending time with the children after work. The old adage of
“quality time, not quantity time” does not hold water. Children
like to be around their parents. Make it easy for them.

Do things as a family. Husbands should not exclude wives,
and mothers and fathers should not exclude their children. This
will mean taking your children to a variety 6f activities. Children
eventually get used to being left out. When they get into their teen
years, they will often start leaving you out.
