Call No Man Your Father 7

votes from their constituency, political favors are granted by those
running for office. Of course, it’s usually done under the slogan of
“taxing the rich in order to help the poor.” These political step-
fathers set a trap for the dependent. In time, the politically de-
pendent get “hooked” on the favors from a supposed benevolent
State. Slavery is the result. The dependency habit is hard to
break. More and more promises are made, and fewer freedoms
are secured by civil law. Where individuals, families, and
churches had authority and power, the State has now moved in
and supplanted their God-ordained jurisdictions, all in the name
of freedom, security, and greater efficiency. This is why the Bible
is very specific as to what the civil magistrate ought to do. Before a
nation will have good civil government, a nation must have good
self-government,

Civil Justice

Let’s establish the Biblical guidelines for the operation of the
State in society, First, civil government should operate judicial sys-
tems on the local, state, and national levels. The law of God, as
outlined in Scripture, is to be the standard of justice, If the accused
does not believe justice has been done, he can appeal his case to a
higher court. He can move from a local jurisdiction to, say, the
county, state, or a district court that encompasses a section of the
nation, Finally, he can appeal to the Supreme Court. In each
case, however, Scripture is to be the standard of justice.

A judicial system can operate only so long as the majority of
the citizenry are self-governed. Our nation’s courts would be
swamped and justice perverted if the majority were lawless. We
are now seeing the breakdown in law and order, a backlog of court
cases, and bulging prisons. Self-government is being repudiated
by a growing segment in our nation. The best courts and the most
just judges cannot deal with the abandonment of self-government.

Weights and Measures

Second, the State must ensure the maintenance of “just
weights and measures.” God considers tampering with weights
