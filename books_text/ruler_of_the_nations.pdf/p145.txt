Neutrality Isa Myth 125

shows how rejecting God as Israel's true King leads the people to
substitute Him with an earthly king (an attempt to equate the
State with the kingdom of God); the books of Samuel, Kings, and
Chronicles tell of the rise and fall of kings and kingdoms, with in-
dividual kings singled out for special counsel by God's emissaries
(e.g., Jeremiah 36-38). Daniel serves as one of Darius’ three civil
commissioners (Daniel 6).

The realm of “politics,” or civil government, is given much at-
tention in the Bible, in both the Old and New Testaments. Ken-
neth Gentry writes:

That God is vitally concerned with political affairs is quite
easy to demonstrate: it is God who ordained governments in the
first place (Rom. 13:1; Rom. 2:21). He is the One who establishes
particular kings (Prov. 16:12; Psa. 119:46, 47; 82:1, 2). Therefore,
He commands our obedience to rulers (Rom. 13:1-3). Rulers are
commanded to rule on His terms (Psa. 2:10ff.). Even in the New
Testament activity of political import is discoverable. Jesus urged
payment of taxes to de facto governments (Matt. 22:15-22). In
response to reminders of King Herod’s political threats against
Him, Jesus publicly rebuked the king by calling him a vixen
(Luke 12:32). He taught that a judge is unjust if he does not fear
God (Luke 18:2, 6). John the Baptist openly criticized King
Herod (Luke 3:19, 20). Peter refused to obey authorities who
commanded him to cease preaching (Acts 5:29). The Apostle
John referred to the Roman Empire as “the beast” (Rev. 13).6

Denial of political involvement repudiates most of the Bible.
Paul makes it clear that the “saints will judge the world” (1 Corin-
thians 6:2). The context of this verse has to do with constituting
“the smallest law courts.” Christians at various times in history
have “judged the world.” The foundation of Western legal tradi-
tion is Christian. The demise of the West results from Christians’
non-involvement in every sphere of life, the civil sphere included.

6. Kenneth Gentry, “The Greatness of the Great Commission,” Joumal
of Christian Reconstruction, Symposium on Evangelism, VIL, No. 2 (Winter, 1981),
p. 45.
