A Bottom-Up Hierarchy 33

anarchy would result. Many individuals could claim authority on
their own terms.

Second, no one institution, group, nation, or society is ulti-
mate. This opposes the idea that the one should be ultimate. If the
one becomes ultimate, an institution, group, church, nation, or
society can claim to be the final arbiter of truth and power, putting
all diversity under its rule. Only God is ultimate; the one and the
many are governed by God’s Law in all things. Thus, the one and
the many in society are balanced under the one authority of the
Triune God.

Many delegated governments decentralize the centers of
power. Corruption and tyranny are heightened when authority
structures, from the individual to civil governments at the local,
county, and state levels, break down, and all authority then rests
in one institution, usually the State.

Reclamation of multiple authorities comes about when the in-
dividual assumes his responsibilities under God and thoroughly
transforms his family, and working with other like-minded indi-
viduals, transforms his school, church, vocation, local commu-
nity, state, and national civil government. We cannot expect
diverse authority structures to arise on their own, however.
Regeneration, the basis of all godly authority, begins with God
working in the individual, and it extends to every facet of life.

Looking for Relief

A word of caution is needed here. Too often Christians turn to
the State for relief because of the failure of individuals, families,
businesses, schools, churches, and civil governments at the local,
county, and state levels. The State has limited jurisdiction and
competence, and is not man’s answer for sin, except in temporal
punishment for criminal acts. In fact, as recent history so well
proves, the State frequently compounds the ills of society—the
economy, education, welfare, the courts— when it acts outside of
the area of its proper authority and jurisdiction.

it has been established that all authority belongs to God and
that all earthly authority, whether family, church or State, is dele-
