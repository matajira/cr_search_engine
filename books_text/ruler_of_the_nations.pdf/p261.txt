BIBLIOGRAPHY

Bahnsen, Greg L. By This Standard: The Authority of God’s Law
Today. Tyler, TX: Institute for Christian Economies, 1985

DeMar, Gary. God and Government: A Biblical and Historical Study.
Atlanta, GA: American Vision, 1982.

—__—__. Issues in Biblical Perspective. Atlanta, GA: Ameri-
can Vision, 1984.

. The Restoration of the Republic. Atlanta, GA: Amer-
ican Vision, 1986.

Eidsmoe, John. God and Caesar: Christian Faith and Political Action.
Westchester, IL: Crossway Books, 1984.

Foster, Marshall and Mary-Elaine Swanson. The American Cove-
nant: The Untold Story. Thousand Oaks, CA: Foundation for
Christian Self-Government, 1981.

Grant, George. The Changing of the Guard: Biblical Blueprints for Pol-
itical Action. Ft Worth, TX: Dominion Press, 1987,

North, Gary, ed. The Journal of Christian Reconstruction: 1) Sym-
posium on Politics, 2) Symposium on Christianity and the
American Revolution; 3) Symposium on Puritanism and Soci-
ety; and 4) Symposium on Biblical Law (Chalcedon, P.O. Box
158, Vallecito, CA 95251). These journals are not going to be
reprinted, according to the publisher, so order them immedi-
ately: $7.50 each.

Rushdoony, Rousas John. Christianity and the State. Vallecito, CA:
Ross House Books, 1986.

. The Institutes of Biblical Law. Nutley, NJ: Craig
Press, 1973.

241
