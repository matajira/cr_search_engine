God Is Sovereign, Not Man 22

As would be expected, Lex Rex was outlawed in England and Scot-
land. Rutherford’s view so jeopardized the “divine right of kings”
mandate that he was condemned to death for his views, He died
before he could be executed as a rebel of the autocratic State.

Oligarchy

The word “oligarchy” is derived from a Greek word meaning
“rule (archein) by the few (oligos).” In our day, the Supreme Court
acts as an oligarchy. The justices on the Court are considered the
final court of appeal. The law is what they say it is. The Court is a
closed system. Nothing outside the Court, nothing higher than
the Court, rules. While Congress can overrule the Supreme
Court and even impeach justices who consistently rule contrary to
the Constitution, it rarely, if ever, happens. These justices are
then an oligarchy by default.

What restrains the Court from overstepping its constitutional
authority? In the words of Justice Stone’s famous remark of 1939,
“the only check upon our exercise of power is our own sense of
self-restraint.” But the very purpose of government is to check the
inability of man to monitor his self-restraint. To give absolute
control of government to a small group of men and women flies in
the face of the Biblical doctrine of total depravity.

Power in the hands of men whose only check is their own
“sense” of what they believe is right or wrong puts a nation at risk.
“Power corrupts,” said Lord Acton, “and absolute power corrupts
absolutely.” What happens if the philosophy chosen by the court is
assumed to be just and right and yet results in the tyranny of the
masses? What happens when a small group of men pronounce
that an unborn child is not protected by the Constitution, that he
has no rights, that his “mother” has the constitutional right to kill
him at will? Nine men, an oligarchy, sentenced 20 million unborn
babies to death.

Democracy

Most Americans are under the impression that our nation is a
democracy. To be sure, there are certainly democratic elements in
