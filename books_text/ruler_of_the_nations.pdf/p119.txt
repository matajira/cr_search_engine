Jesus Alone Possesses Divine Rights 99

suppose, suppose: the theory is all supposition and no fact. It is a
lie perpetuated by the devil and his minions.

Tf it were true, then it would mean that men around the world
can break God’s law and get away with it. Or, if His law does not
bind them, then this theory means that God is not sovereign over
the nations. Legal theory of all societies recognizes this principle: no
law-no authority, no law-no sovereignty. Those who argue that God's law
does not apply are in principle denying the sovereign rule of God.

This must be Satan’s favorite lie. It is certainly one of his most
successful lies in Christian circles.

You cannot come away from a reading of the Bible with the
conclusion that the nations are exempt from the commandments
of God. If the nations are exempt as long as they do not submit to
Jesus as their King, then how will they be held accountable on
judgment day? According to this “new theology,” there is no ac-
countability. The atrocities of despots from the beginning of time
are off the hook.

Our country’s foreign policy makes it evident that we have for-
saken the gospel for the nations. When is the last time (let alone
the first time) that you heard that what our foreign policy really
needs is the claims of Jesus Christ? Do our ambassadors call the
Russians, the Czechs, the Chinese, the Japanese, the Iranians,
the Poles, and the Jews to surrender unconditionally to the lordship
of Jesus Christ? My friends, this is the only hope for the world.
God will not honor our supposed religious neutrality for long.

God establishes nations by His eternal decree. All nations are
accountable to God and His law. When God is rejected as the
King, false king-messiahs claim to be saviors of the people. The
State and God cannot claim to be the ultimate sovereign. All con-
spiratorial designs of men and nations are doomed to fail.

Jesus is King of kings because of who and what He is. For na-
tions to submit to Jesus as King, the gospel must be preached and
the Word of God proclaimed as law. Both church and State are
obligated to keep the law of God, The State cannot exempt itself
from keeping the commandments of God except at its own peril.

There is a jurisdictional separation between church and State
but not a religious separation. Jesus is King of afl the nations.
There is no “divine right of kings.” The New Testament repeats
