We Must Render Appropriate Service 113

Jethro’s advice to Moses suggests that “able men” must rule
(Exodus 18:21). Ability is cultivated through time as the Word of
God is applied to life’s situations. Of course, there are rare excep-
tions to this general rule. Timothy is told, “Let no one look down
on your youthfulness . . .” (1 Timothy 4:12). Instead, he is to con-
duct himself in a way that reflects his faith in ethical terms. His
life (ethical behavior) is to be an example (practical behavior) for
others to imitate.

Civil leadership, like ecclesiastical leadership, is designed to
be ministerial. Those in authority must follow the pattern of God
as ministers rather than attempt to define the role of governmental
leadership in terms of how others rule (Luke 22:24-30; cf. 1 Sam-
uel 8:5).

Summary

The seventh basic principle in the Biblical blueprint for civil
government is that those who rule in the civil sphere are God’s ser-
vants. Those under their jurisdiction must serve the civil govern-
ment faithfully, to the extent that the government is serving God
faithfully by enforcing God’s law. Faithful service upward is sup-
posed to insure faithful service from subordinates.

Civil government is not a “necessary evil.” God established the
civil sphere of government like He established the family and
church, for our good. What is missing in each of these govern-
ments is godly leadership. We’re often faced with voting for the
best of two bad choices. It’s hard to find men of principle, men
who “fear God rather than man.”

But where is leadership cultivated? The family and church are
the training grounds for developing true civil servants. The exam-
ple of Christ as the servant par excellence is our model. Most gov-
ernmental leaders are persuaded by their voting constituency. If
the people back home want some law passed that will favor their
district or them personally, their congressman will seek out their
wishes and vote accordingly. Of course, if it’s the majority view.
Service in the Biblical sense means responsibility. Today, leader-
ship so-called is really slavery. Politicians are slaves to the will of
