The Christian Origins of American Civil Government 237

England, published in the decade before the American Revolution,
was read by most lawyers in the colonies. Perhaps more than any-
one else, Blackstone established the “terms of debate” for the Rev-
olution. (Amazingly, his books are seldom read today, even by
historians who specialize in the American Revolution.) In his
Commentaries, Blackstone had argued that the Fall of man has
made his mind untrustworthy, so it is necessary to use the Bible to
rightly understand the universal laws that God has established.

In time, however, the “Law of Nature” degenerated into “Nat-
ural Law,” a law devoid of Biblical content. In 1892, the United
States Supreme Court determined, in the case The Church of the
Holy Trinity vs. United States, that America was a Christian nation
from its earliest days. The court opinion, delivered by Justice
Josiah Brewer, was an exhaustive study of the historical and legal
evidence for America’s Christian heritage. After examining hun-
dreds of court cases, state constitutions, and other historical docu-
ments, the court came to the following conclusion:

Our laws and our institutions must necessarily be based upon
and embody the teachings of the Redeemer of mankind. It is im-
possible that it should be otherwise; and in this sense and to this
extent our civilization and our institutions are emphatically
Christian. .. . This is a religious people. This is historically
true. From the discovery of this continent to the present hour,
there is a single voice making this affirmation... .We find
everywhere a clear recognition of the same truth. . . . These,
and many other matters which might be noticed, add a volume of
unofficial declarations to the mass of organic utterances that this
is a Christian nation.

 

 

Incorporation

In the Everson case of 1948, the court held that the First
Amendment's Establishment Clause had been “incorporated” into
and made a part of the “due process” clause of the Fourteenth
Amendment (adopted in 1868), and was thus a restriction on the
States as well as Congress. Yet, the “due process” clause of the Four-
teenth Amendment makes no reference to religion, but simply
