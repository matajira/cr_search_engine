170 Ruler of the Nations

Transcendence/Presence

First, we begin with the truth that God alone is absolutely sov-
ereign, not man. Thus, He alone is all-seeing (omniscient), all-
powerful (omnipotent), and everywhere present with His creation
(omnipresent). No creature, including Satan, possesses any of
these attributes of God. They are incommunicable attributes.

Thus, we can have faith that “God causes all things to work
together for good to those who love God, to those who are called
according to His purpose” (Romans 8:28). We do not need to im-
pose power over others to achieve good. God does this through
His direct control over history and His direct meeting with each
person. He alone is truly transcendent (above everything, and
distinct in being from His creation) and truly immanent (truly
present next to each creature, yet not part of His creation). We
must avoid deism (a totally distant god) and pantheism (a god totally
immersed in the creation, and not different from it).

The church in many respects has lost its vitality and manifests
corruption, compromise, and retreat. Scripture tells us: “For it is
time for judgment to begin with the household of God?” (1 Peter 4:17).

This means that true worship must be restored. Adam and
Eve rebelled, choosing to serve the creature rather than the Crea-
tor (Genesis 3:1-7; Romans 1:18-32). Jesus rebuked Satan and in
the process taught the true purpose of life: “You shall worship the
Lord your God, and serve Him only” (Matthew 4:10; cf. Deuter-
onomy 6:13). Robert Rayburn writes:

The worship of God is at once the true believer's most impor-
tant activity and at the same time it is one of the most tragically
neglected activities in the average evangelical church today. In
the preaching and teaching of the churches, in conferences and in
seminars, individual Christians have been encouraged to have
their own daily “quiet time,” for a period of personal devotion is
an important part of every Christian life. They have been ad-
monished to pray for their own needs and the needs of others.
They have been taught to study the Bible for their own spiritual
growth and for use in guiding and instructing others. There re-
mains, however, among sincere believers today a woeful ignor-
