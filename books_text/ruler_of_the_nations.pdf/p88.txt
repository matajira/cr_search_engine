68 Ruler of the Nations

11. Sanctification (God’s setting apart morally) applies to indi-
viduals and also to collectives.

12, There is no such thing as a victimless moral crime.

13, A measuring ruler is a tool of dominion; so is a govern-
ment ruler. Both impose éimits.

14. God judges nations in history for disobeying His limits.

15. God has not abdicated His office as ruler in history.
