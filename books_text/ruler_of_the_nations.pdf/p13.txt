Editor's Introduction xiii

electronically. The results of his restructuring can be read in his
marvelous commentary on the Book of Revelation, The Days of
Vengeance (Dominion Press, 1987).

Here, then, is the five-point structure of the Biblical covenant,
as developed by Sutton in his excellent book, That You May Prosper:
Dominion by Covenant (institute for Christian Economics, 1987).

. The transcendence and immanence of God
. Authority/hierarchy of God’s covenant

. Biblical law/ethics/dominion

. Judgment/oath: blessings and cursings

. Continuity/inheritance

CPO Ne

Simple, isn’t it? Yet it has implications beyond your wildest
imagination. Here is the key that unlocks the structure of human
government. Here is the structure that Christians can use to ana-
lyze church, State, family, and numerous other non-covenantal
but contractual institutions.

It can be used to unlock the long-debated structure of the Ten
Commandments: 1-5, with a parallel 6-10. I spotted this almost as
soon as Sutton described his discovery, just as I was finishing my
economic commentary on the Ten Commandments, The Sinai
Strategy (Institute for Christian Economics, 1986), which I outlined
in its Preface. It can also be used to make sense out of some of the
basic concepts of economics, as I show in my book in the Biblical
Blueprints Series, Inherit the Earth (1987). In fact, once you begin
to work with this model, it becomes difficult not to see it every-
where you look. This means that Sutton’s model is either very
powerful or very hypnotizing.

Where the intellectual pay-off really gets high is in the field of
government. Gary DeMar did not deliberately structure the first
draft of this manuscript around Deuteronomy’s five-point cove-
nant model. Nevertheless, as I read it, I recognized the five
points. I simply moved his chapters around. He had already
covered the basic topics of government that the five-point model
reveals: in two sets of five chapters. Once again, we see the power
