Jesus Alone Possesses Divine Rights 97

laws of God, and there are curses for those societies that fail to
conform externally to these laws (Deuteronomy 28:1-68). The
laws of God that relate to blessings and curses are operative for all
peoples. The prophet Amos made this clear when he denounced
the nations surrounding Israel. Damascus, Gaza, Tyre, Edom,
Ammon, and Moab incurred the curses of Deuteronomy 28:15-63
(Amos 1:3; 2:5). Those judges who fail to render verdicts accord-
ing to the absolute standard of the law of God “will die like men,
and fall like any one of the princes” (Psalm 82:7).

The Levites stood before the people to remind them of their
sins and the reason for God’s judgment on their nation: “For our
kings, our leaders, our priests, and our fathers have not kept the
law or paid attention to Thy commandments and Thine admoni-
tions with which Thou hast admonished them. . . . Behold, we
are slaves today, and as to the land which Thou didst give to our
fathers to eat of its fruit and its bounty, behold, we are slaves on it”
(Nehemiah 9:34, 36). Slavery, in which even our bodies are ruled
by despotic leaders (v. 37), is the result of a nation’s failure to keep
the commandments of God.

Breaking God’s commandments means “transgressors and sin-
ners will be crushed together, and those who forsake the Lorp
shall come to an end” (Isaiah 1:28). Harlotry, injustice, murder,
theft, taking bribes, and afflicting the helpless are results of a na-
tion’s repudiating the laws of God for the laws of men
(humanism). Even the greatest kingdomis of the world will be re-
duced to dust if they fail to honor God’s law (Daniel 2:31-35). One
of the most sobering judgments of God is the one that falls on
Herod for his humanistic government: “On an appointed day
Herod, having put on his royal apparel, took his seat on the
rostrum and began delivering an address to them. And the people
kept crying out, ‘The voice of a god and not of a man!’ And imme-
diately an angel of the Lord struck him because he did not give
God the glory, and he was eaten by worms and died” (Acts
12:21-23),

God in His providence appoints and deposes all rulers. He,
therefore, is never surprised about the development of the nations
