44 Ruler of the Nations

selves. Satan’s lie is alive and destroying our nation: “And you will
be like God, knowing good and evil” (Genesis 3:5).

As soon as Adam and Eve sinned, the accusations began. God
cross-examined Adam and Eve, beginning with the responsible
agent in the family, Adam. Adam blamed his wife, and ultimately
God; “The woman whom Thou gavest to be with me, she gave me
from the tree, and I ate” (v. 11). God then cross-examined Eve.
Eve blamed the serpent: “The serpent deceived me, and I ate”
(Genesis 3:13). In other words, both of them blamed their envir-
onment. Both of them implicitly blamed the One who made their
environment, God. But God was not responsible; neither was
their environment. Adam sinned wilfully, and was fully responsi-
ble. Eve sinned because she had been deceived (1 Timothy 2:14),
but she was also fully responsible. God punished both of them for
their sin (Genesis 3:16-19). He also punished the serpent (Genesis
3:14)—a punishment that promised ultimate victory to the seed of
the woman over the forces of evil (3:15).

Murder (Genesis 4:8, 23) and other acts of violence (6:2) then
became part of the cursed created order. God therefore im-
plemented laws to deal with the violence that resulted from man’s
sinful desires: “Whoever sheds man’s blood, by man his blood
shall be shed, for in the image of God He made man” (9:6). Laws
protecting life (Exodus 20:15), property (v. 13), and personal/
neighbor relationships (vv. 12, 14, 16) were made part of the sys-
tem of laws that Israel was bound to follow. Justice, therefore, was
defined in terms of the law, God’s law.

The Power of the Sword

While the church is given the authority of the “keys” (Matthew
16:19), the State has the power of the “sword” (Romans 13:4). The
“sword” means the power to inflict violence. There are certain
crimes that necessitate the use of the sword (e.g., capital crimes)
where the church has no jurisdiction. Church and State are sepa-
rate, God-ordained monopolies in their respective realms.

Israel’s governmental systems were decentralized. Judges and
their officers, and the elders of the people were appointed to a
