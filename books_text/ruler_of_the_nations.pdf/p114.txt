94 Ruler of the Nations

theory regarding kings was tossed into the historical dustbin. The
“Glorious Revolution” of 1688 transferred the mythical divine
right of kings to Parliament. Nine decades later, the American
Revolution broke out in opposition to Parliament’s taking seri-
ously its own stolen theory.

One purpose of the civil law in the Old Testament was to see
to it that the king’s “heart may not be lifted above his countrymen”
(v. 20). Citizens and king are to serve the same law. King Solo-
mon prayed for “an understanding heart to judge [God’s] people
to discern between good and evil” (1 Kings 3:9). Solomon's stand-
ard of right and wrong was the Bible. Only when he ignored
Scripture did judgment came to his kingdom. This is no less true
in the New Testament, where Jesus is one with the Lord who gave
Moses the law (John 10:30), and said that we are to keep His
commandments (John 14:15; 15:14). By keeping Jesus’ command-
ments, we keep the commandments of God, for Jesus is God
(John 1:1).

God’s standard of justice is the same for all His creatures. This
includes nations that consider themselves non-Christian. Some
people believe that because they do not acknowledge God as Lord
and King, they somehow are exempt from following the law of
God. Sodom and Gomorrah enjoyed no such exemption: “Now
the men of Sodom were wicked exceedingly and sinners against
God? (Genesis 13:13). This wicked city was destroyed for breaking
God's law: in particular, the sin of homosexuality! (Genesis
19:4-5; Leviticus 18:22; 20:13). Jonah went to preach to the non-
Israelite city of Nineveh because of their sins. If the Ninevites
were not obligated to keep the law of God, then how could they be
expected to repent (Jonah 3)? The stranger, an individual outside
the covenant community, must obey the law of God: “There shall
be one standard for you; it shall be for the stranger as well as the
native, for I am the Lorp your God” (Leviticus 24:22; cf. Num-
bers 15:16; Deuteronomy 1:16-17).

1, See Gary DeMar, “Homosexuality: An Ilegitimate, Alternative Death-
style,” The Biblical Worldview, Vol. 3, No. 1, January 1987, Adanta, Georgia:
American Vision.
