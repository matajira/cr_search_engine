Reconstructing Family Government 199

Low-Cost Annual Renewable Term Life Insurance
AGE = $100,000 += $250,000 + $500,000 $1,000,000

35 120 200 375 725
40 149 253 480 935
45 186 320 615 1205
50 231 403 780 1535
55 333 593 1160 2295
60 556 998 1970 3915

If the average worker at age 25 saved $2,549 per year and in-
vests it at 6 percent interest after taxes, he will accumulate
$394,483 by the time he is 65. This is for funds compounded an-
nually. If interest were compounded quarterly, as many private
institutions compound in order to be competitive, the effective in-
terest rate would be 6.14 percent, and the savings total at age 65
would be even greater. There are many investment opportunities
that pay more than 6 percent. Each year $2,549 saved and in-
vested at 7 percent would produce $508,857 in 40 years. At 8 per-
cent, the total would be $660,344.

© $2,549 per year for 40 years @ 6% interest = $394,483
® $2,549 per year for 40 years @ 7% interest = $508,857
© $2,549 per year for 40 years @ 8% interest = $660,344

With a sum of $394,483 invested at 6 percent, the retired indi-
vidual could draw $1,972 monthly, or $23,664 per year. Compare
this with the maximum monthly payment of $981 that a couple
could receive under Social Security. Now it is important to take
note of another important difference under this private savings
plan versus Social Security, The $1,972 per month under the pri-
vate plan is an interest payment. They can go on drawing interest
at this rate (six percent) until they die. And they can follow the in-
junction of Proverbs 13:22: “A good man leaves an inheritance to
his children’s children, and the wealth of the sinner is stored up
for the righteous” (Prov. 13:22).®

8. The above material on Social Security is taken from Tom Rose, Economics:
The American Economy from a Christian Perspective (Mercer, Pennsylvania: American
Enterprise Institute, 1985), pp. 100-111.
