Rebuilding Takes Time 155

reminds us that God requires us to work, regardless of external circum-
stances. Faithfulness is evaluated in terms of kingdom work: “Who
then is the faithful and sensible slave whom his master put in
charge of his household to give therm their food at the proper time?
Blessed is that slave whom his master finds so doing when he
comes” (Matthew 24:45, 46). Jesus goes on to hint at the time and
circumstances of His coming: “the master of that slave will come
on a day when he does not expect him and at an hour which he does not
know” (v. 50).

Nowhere does Scripture intimate that we should cease any
aspect of kingdom work, even if we think Jesus’ coming is near.
George Ladd, a premillennial scholar, writes, “The delay of the mas-
ter made no difference to the true servant: he busied himself about his
Lord’s business. . . . But the master’s delay induced the false ser-
vant to a sinful course of action. The Lord’s delay brought out the true
character of his servants.”?

Jesus related a parable to His disciples when “they supposed
that the kingdom of God was going to appear immediately” (Luke
19:11). In Jesus’ day, many of His disciples assumed the kingdom
would arrive through a cataclysmic event with no effort on their
part. Jesus told them through the parable, “do business until I
come back” (v. 13). When the master finally returns he will take an
accounting, Those who made a profit on the money given by the
master will “be in authority over ten and five cities” (vv. 17-19).
The one who put the money “away in a handkerchief” (v. 20), not
being industrious enough to “put the money in the bank” to collect
“interest” (v. 23), loses everything (v. 24).

Charles Haddon Spurgeon (1834-1892), the great Baptist
preacher and evangelist of the nineteenth century, shows how pes-
simism robs the church of its vitality and stunts its growth.

David was not a believer in the theory that the world will
grow worse and worse, and that the dispensations will wind up
with general darkness, and idolatry. Earth’s sun is to go down

2. George Eldon Ladd, The Blessed Hope (Grand Rapids, Michigan: Eerd-
mans, 1956), p. 106.
