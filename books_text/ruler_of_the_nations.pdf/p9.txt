EDITOR'S INTRODUCTION
by Gary North

Thus saith the Lorp God; Remove the diadem and take off
the crown: this shal] not be the same: exalt him that is low, and
abase him that is high. I will overturn, overturn, overturn it: and
it shall be no more, until he come whose right it is, and I shall
give it to him (Ezekiel 21:26-27; King James Version).

God rules the nations. He sets up rulers and removes them
(Daniel 2:21). He alone is the true Sovereign of the world. All
other sovereign rulers are merely delegated sovereigns, whether or
not they admit this fact to themselves or to their followers,

It is each man’s job to restructure his thoughts, words, and
deeds to conform to God’s standards of righteous rule. If each per-
son would do this perfectly (an impossibility, given the effects of
sin), every human institution would be restructured to conform to
God's standards for it. God’s inescapable rule would then be made
manifest by every institution.

We must not be misled: God rules, whether or not men
acknowledge this. God has standards for the proper administra-
tion of every person (and therefore all institutions), whether or
not anyone admits this. The proper historical question regarding
God's rule is this: To what extent does the operation of any given
human institution conform to God’s standards at any point in
history? To answer this, we first need to know what the standards
are that God has established for all of the institutions of gov-
emment.
