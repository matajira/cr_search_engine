Reconstructing Church Government 173

The apostles were imprisoned and some murdered because they
refused to be silenced (Acts 4:1-31; 5:17-42; 12). Those tyrants are
dead but the church of Jesus Christ remains.

Ethics/Law/Dominion

Third, the church must be a “boot camp” for the saints in the
war with humanism, The “whole counsel of God” (Acts 20:27)
must be marshalled against the whole counsel of humanism: “And
He gave some as apostles, and some as prophets, and some as
evangelists, and some as pastors and teachers, for the equipping of
the saints for the work of service, to the building up of the body of Christ”
(Ephesians 4:11, 12). Archie Jones states:

As a consequence of Christians’ abandonment of the study,
preaching, hearing, and doing of the whole counsel of God, we
have neglected the scope of the spiritual warfare between the army of
Ghrist and that of Satan, neglected the subtlety of the Serpent,
the Adversary, and retreated from our created, re-born, and commanded
purpose of dominion for Christ, under His lordship. Consequently,
Christians today find themselves unmistakably embattled. Due
to our neglect of God’s word, however, rather than being on the
offensive and the attack, American Christians find themselves
under attack by humanists in high places.*

Elders, required to meet certain moral qualifications before
being considered for office, also must be “able to teach” (1 Timothy
3:2). Basic doctrine certainly should be taught, from theology
proper (the doctrine of God) to eschatology (the doctrine of the
last things). The church should take care to include doctrines per-
taining to inflation, abortion, foreign affairs, education, welfare
and the poor, philosophy, and economics.

Finance represents an area of neglect within much of the
church. Paul tells us to “owe no man anything” (Romans 13:8).
Too many of us spiritualize this to mean “owe no man a spiritual

4, Archie Jones, “The Imperative of Christian Action: Getting Involved as a
Biblical Duty,” Journal of Christian Reconstruction, Symposium on Social Action,
edited by Gary North, Vol. VIII, No. 1 (Summer, 1981), p. 110-11.
