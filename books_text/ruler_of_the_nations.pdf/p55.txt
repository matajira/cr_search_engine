A Bottom-Up Hierarchy 35

sider an unfair decision. Even the jurisdictional boundaries of ec-
clesiastical bodies must be respected, though this is difficult in a
multi-denominational context. When one church disciplines a
member and excommunicates that member, it is the duty of other
ecclesiastical bodies to grant at least initial respect to the jurisdic-
tion of the disciplining ecclesiastical court.

Jurisdictional authority can only be understood when the Bib-
lical “chain of authority” is considered. This follows under the the-
ology of “headship.” Paul writes, “But I want you to understand
that Christ is the head of every man, and the man is the head of a
woman, and God is the head of Christ” (1 Corinthians 11:3). In
Ephesians 5:23 Jesus is said to be the “head of the church.” In the
book of Revelation Jesus is designated “the ruler of the kings of
the earth” (Revelation 1:5; Psalm 72). In all these cases we find
everyone responsible to someone else, and ultimately all are re-
sponsible to God. Even Jesus in His humanity, as the Son of God,
is under the headship of His Father (Luke 22:42).

The Biblical Answer: A Constitutional Republic

We find in Exodus 18 a description of what a godly civil gov-
ernment should look like. Moses, as God’s unique representative,
appointed righteous judges over the people in a hierarchy. But
this hierarchy was not a top-down pyramid. Rather, it was a bot-
tom-up appeals court. God had given them His law (as He has
given to us), and the people were to take their disputes to God-
fearing men, who would render honest judgment. If a case was
too hard for them, then the judges would refer it to the next level
upward.

This leaves individuals free to work out their own salvation
with fear and trembling (Philippians 2:12). Each person is to be
self-governed under God. In ancient Israel, God required His law
to be read to the whole nation every seventh year (Deuteronomy
31:10-13), which revealed the terms of civil righteousness to every-
one. Each person was therefore made responsible for his actions.
This is God’s system of self-government. Only when people dis-
agree about the legal boundaries between them do they call in the
