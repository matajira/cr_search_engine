238 Ruler of the Nations

provides that no state shall “deprive any person of life, liberty, or
property, without due process of law.”

“Incorporation” had been rejected by the Court for nearly
three-quarters of a century after the Fourteenth Amendment. The
Court in 1948 gave no explanation for its judicial move of incor-
poration. Since the Everson decision of 1948, eminent legal
scholars have rejected “incorporation.”

The Blaine Amendment was introduced in Congress seven
years after adoption of the Fourteenth Amendment. It substituted
the word “state” for “Congress”: “No state shall make any law
respecting an establishment of religion or prohibiting the free ex-
ercise thereof.” The Blaine Amendment was considered twenty
times by Congress between 1876 and 1929 but always failed. Even
Blaine himself never suggested that the First Amendment was
“incorporated” into the Fourteenth.

If the Fourteenth Amendment did incorporate the First
Amendment as the Supreme Court now says, why did its authors
think the Blaine Amendment was necessary to restrict the power
of the states as to religion?

What the Supreme Court Says Isn't Necessarily Law

The U.S. Constitution recognized this from the beginning,
although for the past century, few legal scholars and virtually no
politicians have acknowledged what the Founders wrote into the
Constitution. If Congress is convinced that the Court is usurping
jurisdiction that belongs to Congress, they can remove appelate
jurisdiction from the Supreme Court (Art. HI, Sec. 2, Clause 2).
‘This was done in a case in 1869, Ex parte McCardle.9

It is interesting that law professors in prestigious law schools
teach their students that what the Supreme Court says is not “the
law,” that it is not final, and they encourage their students to try to
get cases overturned that appear to be settled by Supreme Court

9. The Constitution of the United States of America: Analysis and Interpretation, pre-
pared by the Congressional Rescarch Service, Library of Congress (Washington,
D.C.: Government Printing Office, 1972), pp. 751-52.
