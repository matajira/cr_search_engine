Judges Need Jurisdiction 139

Church/State Cooperation

The Bible portrays church and State as cooperating govern-
ments. Most people are aware that the Bible is the standard for
the priests as they carry out their priestly duties. But what of the
king? Was he obligated to follow the Bible as well? The Bible
makes it clear that he was.

Now it shall come about when he [the king] sits on the throne
of his kingdom, he shall write for himself a copy of this law on a
scroll in the presence of the Levitical priests. And it shall be with
him, and he shall read it all the days of his life, that he may learn
to fear the Lorp his God, by carefully observing all the words of
this law and these statutes, that his heart may not be lifted up
above his countrymen and that he may not turn aside from the
commandment, to the right or the left; in order that he and his
sons may continue to live long in his kingdom in the midst of
Israel (Deuteronomy 17:18-20).

While church and State as jurisdictions are separate, religion is
not. Both priests and kings are commanded to follow the same
standard of government, even though not all laws apply to each in
the same way. We can go so far as to say that the presence of the
priests was a reminder to the king that they were to help him in-
terpret the law as it related to civil affairs. This is precisely what
Azariah and the eighty priests were doing when they confronted
King Uzziah; they were reminding him of his limited jurisdiction.

Moral Criteria for Rulership

The criteria for leadership in both church and State is the
same. When Jethro, Moses’ father-in-law, counseled Moses to de-
centralize the judicial arm of the civil government and choose
lesser magistrates, he laid down the qualifications for those who
would rule. They were to be “able men who fear God, men of
truth, those who hate dishonest gain” (Exodus 18:21). In Deuter-
onomy 1:9-15, Moses recounts the circumstances of Jethro’s coun-
sel and adds that these leaders were to be “wise and discerning
and experienced men,” not showing “partiality in judgment”
(Deuteronomy 1:13, 17).
