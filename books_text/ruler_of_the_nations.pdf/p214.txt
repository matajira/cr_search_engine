194 Ruler of the Nations

Ethics/Law/Dominion

Have children. God commands this as part of the dominion
covenant (Genesis 1:26-28). Don’t let the Population Explosion
People (PEP) lead you astray. The populations of Western indus-
trial society are threatened by falling birth rates. The replacement
rate is 2.1 children per family. In virtually no West European na-
tion, the U.S., or Canada is it above 1.8 children per family. In
Italy, it has collapsed to about 1.5. If you have any questions
about the nonsense of the PEP, I suggest you read The Myth of
Overpopulation by R. J. Rushdoony and The Economics and Politics of
Race by Thomas Sowell.

The humanists are killing their future through abortion.
Many are not having children or are limiting the size of their fam-
ilies. This is a great opportunity for Christians to overwhelm
them with numbers. The following anti-child statements will give
some idea of the opportunity we have of winning the future by
default if we only have more children:

® “We could not lead the kind of life we have if we had kids.”

* “[d rather buy a vacation home than funnel a large chunk
of our income into a trust fund [for our children].”

@ “We've made our choice for freedom and spontaneity. Our
cats can take care of themselves, and we're free.”

* “I've seen other couples lose a closeness they had before
deciding to have children.”

® “I guess I’m selfish. I'm just not ready to sacrifice my time
to a kid.”

© “This world is a crazy place to raise a kid.”

© “J find the whole idea of pregnancy repugnant . . . ’m not
wild about children.””

Begin educating your children at an early age. Teach them the
Bible. There are numerous Bible “trivia” games that will make
learning interesting. If you’re not able to teach the Bible to your

2. “Three’s a Crowd,” Newsweek (September 1, 1986), pp. 68-74.
