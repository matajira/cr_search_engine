88 Ruler of the Nations

structure by crushing the colossus: “Then the iron, the clay, the
bronze, the silver and the gold were crushed all at the same time, and
became like chaff from the summer threshing floors; and the wind
carried them away so that not a trace of them was found” (v. 35).

The Conflict Over Kingship

The Roman Empire presents a classic historical example of
the Messianic man-centered State, of the denial of God’s Law,
and of the implementation of humanistic law. The Caesars
declared themselves gods, and their decrees were acknowledged as
the laws of gods. Because of each Caesar's false claim of divinity,
his limited reign was threatened by God’s unlimited and universal
reign. Peter declared confidently “that there is salvation in no one
else; for there is no other name under heaven that has been given
among men, by which we must be saved” (Acts 4:12). The gospel
of Jesus Christ, with its claim of divine prescriptions, threatened
the very nature of the Roman State. Rome had to submit itself to
the position of “minister” under God or be crushed by the power of
God. Rome did not submit.

Early Christians were accused of “acting contrary to the
decrees of Caesar, saying that there is another king, Jesus” (Acts
17:7). There is no evidence that the early church advocated that
people act contrary to the prevailing law-system, except when
those laws prohibited them from worshipping and evangelizing.
However, those who heard the disciples preach understood the
implications of Jesus Christ’s demands. If Jesus is truly the
Messiah, then even the State must submit to His authority and
rule: no middle or neutral ground exists. Jesus’ words make it
clear that only one master can claim absolute authority: “No one
can serve two masters; for either he will hate the one and love the
other, or he will hold to one and despise the other” (Matthew 6:24).

The State and God cannot both be the absolute sovereign. One
must submit to the other. Obviously, the State must submit to the
Lordship of Jesus Christ or perish in its attempt to overthrow His
rule. Any attempt by the nations to oppose the rule of God is an
act of futility. God laughs at and scorns their attempts to over-
