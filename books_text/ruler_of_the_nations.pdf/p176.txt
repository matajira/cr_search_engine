156 Ruler of the Nations

amid tenfold night if some of our prophetic brethren are to be be-
lieved. Not so do we expect, but we look for a day when the
Qwellers in all Jands shall learn righteousness, shall trust in the
Saviour, shall worship thee alone, O God, “and shall glorify thy
name.” The modern notion has greatly damped the zeal of the
church for missions, and the sooner it is shown to be unscriptural
the better for the cause of God. It neither consorts with prophecy,
honours God, nor inspires the church with ardour. Far hence be
it driven.3

“Therefore, my beloved brethren, be steadfast, immovable,
always abounding in the work of the Lord, knowing that your toil
is not in vain in the Lord” (t Corinthians 15:58).

Our Inheritance Grows

Until Christians began to spread the gospel into the Roman
Empire, the ancient world had never heard of linear (straight line)
time. The ancient pagan world, from savages to sophisticated phi-
losophers, believed in circular time. Time is cyclical, they argued.
It is going nowhere. There was no beginning; there will be no end.

Christians challenged this view of time. They preached
Christ, and Him crucified. Christ came in the midst of time, they
said. God created the world; Adam rebelled; and now Christ has
come in the midst of time to die and rise from the dead, overcom-
ing sin. His resurrection points to a future resurrection, Paul
writes in his first letter to the Corinthians, chapter 15. Thus, we
have hope for the future; only Christians had such hope; there-
fore, only Christians had enough confidence in the future to preach a doctrine
of linear time.

God told His people that their earthly efforts have meaning in
time and eternity. What we think, say, and do has consequences
for us, and also for history. And we know that the good that we do
becomes a legacy to other Christians who follow us. Because we have
legitimate faith in the future, we can confidently use other people's gifts to us

3. The Treasury of David (Grand Rapids, Michigan: Guardian Press,
[1870-1885] 1976), 1V:102.
