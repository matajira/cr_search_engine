72 Ruler of the Nations

give us meat to eat? We remember the fish which we used to eat
free in Egypt, the cucumbers and the melons and the leeks and
the onions and the garlic, but now our appetite is gone. There is
nothing at all to look at except this manna’” (Numbers 11:4-6).

They were, in essence, saying, “You can’t beat security . . . three
meals a day... a roof over our heads . . . steady work... .
‘Slavery? That’s such an unpleasant word . . . . We always knew
where our next meal was coming from. . . . That's security.” No,

that’s a dog’s life.

Did you catch what they said? They actually believed that
these things had been “free” in Egypt. Free? They had been
slaves! They had paid for these things with their own lives. A
return to Egypt meant bondage to a system where they had no
voice in the way they lived. There was no future for them in
Egypt. Their children’s children would be slaves, called into ser-
vice to build a civilization to the glory of Pharach. Marching on to
Canaan meant a promised land “flowing with milk and honey”
where they could build a civilization based on the blueprint laid
out in Scripture for the glory of God and as a beacon to the na-
tions (cf. Deuteronomy 4).

Slaves or Servants

In both Hebrew and Greek, the word for “servant” is the same
as the word for “slave.” Yet we know that it is better to be a servant
than a slave. What is the difference, Biblically speaking?

All men are servants by nature. We are creatures made in
God’s image. We are made to serve God, and therefore we are
made to rude in the name of God (Genesis 1:26-28), under the law
of God (Deuteronomy 8), always in light of the fact that we will be
judged by God (Revelation 20:11-15). We are creatures, not the
Creator. God alone is not a servant by nature. He is the Master
by nature— His own self-existent Nature.

What about slavery? Slavery is the demonic imitation of service to
God. It is service to a creature, Satan, the enemy of God. He, too,
seeks servants, but service to him necessarily involves bondage to
sin. Those who reject God as loving Master will eventually seek
