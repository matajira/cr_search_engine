xvi Ruler of the Nations

Judge. It is His desire that everything men build should reftect
His perfection. Therefore, theocracy means that all governments
—self-government, family government, church government, and
civil government—are theocratic governments (plural). God rules
them all. He rules in terms of His revealed law. Biblical theocracy
therefore does not mean tyranny. Humanist theocracy means tyranny:
the rule of self-proclaimed sovereign man.”

The humanists have brainwashed Christians when it comes to
the Biblical meaning of theocracy. Christians should recognize
from the very beginning that humanists have already brainwashed
them. It is time to get “unbrainwashed.” We must begin this intel-
lectual “de-programming” process by reading carefully and by
thinking about what we are reading. It takes real effort to pay at-
tention to the meaning of words. Christians must not allow them-
selves to slip back into using the false definition of theocracy that
humanists have provided for us in order to destroy us.

Training People for Self-Government

Each of the three institutional governments in the Bible is de-
signed to lead people into self-government under God. Spiritual
slaves resent this process of liberation, as do the earthly masters of
spiritual slaves. We see this clearly in the account of the exodus.
The Hebrew slaves were angry at Moses for his having angered
the Pharaoh (Exodus 5:20-21). They were not angry at the
Pharaoh, the tyrant. Slaves need tyrants in much the same way
that tyrants need slaves: a condition of mutual dependence that is
not based on self-government under God,

The need of governments to train subjects to exercise inde-
pendent judgment is obvious in the case of family government.
Parents who keep their children under their thumbs for life will
die in old age in poverty, or at least insecurity, for their children
will be unreliable supporters. Patriarchy in the Bible was always
based on the idea that the father would leave the sons their share
of the family’s property before he died. He would give his sons
their appropriate blessings (assets) when they reached maturity.
Abraham gave Sarah’s tent to Isaac when Isaac married Rebekah
