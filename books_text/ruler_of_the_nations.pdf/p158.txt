138 Ruler of the Nations

The Limits of Jurisdiction

The Bible defines the Jimits of jurisdiction; it must also be used
to prescribe the specifics of operation for each jurisdiction. The Bible
is a blueprint for family government, for church government, and
for civil government. If the Bible is the blueprint for only the fam-
ily and church and not the State, then immediate jurisdictional in-
fringements take place. For example, according to the Bible, par-
ents have educational jurisdiction of their children. If the State
repudiates the Bible as a blueprint for State-craft, children will be
seen as wards of the State and thus under its jurisdiction. Taxes
will be raised by the State, teachers will be certified by the State,
schools will be accredited by the State, and students will be com-
pelled by the State to be educated by the State.

What if the State decides that more taxes are needed to fund
some of its programs (programs that must be defined by the
Bible)? Can the State legitimately tax the church to raise the
needed revenue? The State certainly has the authority and power
to tax (Romans 13:7; Matthew 22:15-22). But can it tax any way it
wishes? Can it tax the church? If the Bible is not a blueprint for
taxation and the limits of civil jurisdiction, then the State is free to
tax as it pleases.

God has established both church and State. They are not, in
principle, hostile toward one another. But because each is a gov-
ernment, with a certain amount of authority and power, we
should expect power struggles. Sometimes the power struggle is
the church attempting to impose its will on the State. The church
grows, then, not by the regenerating work of the Holy Spirit, but
by the power of the sword.

More often than not, the State imposes its will on the church
because it sees the church as a competing government. This oc-
curs because the State has already become secularized. It has
repudiated the Bible as its standard. Of course, the State is
secularized due to the secularization of the nation. By the time the
State rejects the Bible, the nation as a whole has already rejected
the Bible as a blueprint for life. Civil government reflects self,
family, and church governments.
