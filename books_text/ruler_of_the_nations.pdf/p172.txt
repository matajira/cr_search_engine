152 Ruler of the Nations

ing for answers. The Christian, if he truly believes the Bible, has an-
swers. Setting the agenda as we approach the 20th century should be
the priority for all Christians. This will mean involvement.

We are too often occupied with what man has done, and the
whole time we deny what God is doing. The way Christians think
and act, you would suppose that Satan is more powerful than
God. Satan has power, but it is limited by God. Even in the Old
‘Testament era, Satan was restricted in the influence he could exert.
Before Satan could afflict Job he had to seek God’s permission:
“Behold, all that he {Job} has is in your power, only do not put forth
your hand on him’ (Job 1:12; cf. 2:6). Through it all God received
the glory and Job was finally restored ( Job 42:10-17).

During Jesus’ earthly ministry, the disciples had authority and
power over demons because Satan’s power was partially grounded.
When the seventy disciples returned from their mission they re-
marked that “even the demons are subject to us” (Luke 10:17).
How could this be?: “And He said to them, ‘I was watching Satan
fall from heaven like lightning. Behold, I have given you author-
ity to tread upon serpents and scorpions, and over all the power of
the enemy, and nothing shall injure you’” (vv. 17-18).

Jesus tells the Pharisees that His casting out demons is the
sign that the kingdom of God has come, displacing the enemy ter-
ritory of Satan: “But if I cast out demons by the finger of God,
then the kingdom of God has come upon you. When a strong man
[Satan] fully armed guards his own household, his possessions are
undisturbed; but when someone stronger [God] than he attacks
him and overpowers him, he takes away from him all his armor
on that he had relied, and distributes his plunder [Satan’s king-
dom)” (11:10-22).

When Jesus was about to go to the cross He made reference to
the effect that Satan will have on His work: “I will not speak much
more with you, for the ruler of the world is coming, and he has
nothing in Me” (John 14:30). All the powers of Hell would not be
able to deter Jesus in the task that would soon energize the church
to such an extent that the gates of Hell will not be able to stand
against her power: “Upon this rock [the sure testimony that Jesus
