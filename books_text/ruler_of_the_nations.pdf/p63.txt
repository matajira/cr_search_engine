Plural Law Systems, Plural Gods 43

lishes man as judge, lawgiver, king, and savior; it leaves man
under the rule of darkness. The Bible clearly shows that law ori-
ginates in the character of God, and definitions for justice and
righteousness find their meaning in Him and not in the finite, fal-
lible, and fallen nature of the creature: “For the Lorp is our
judge, the Lorp is our lawgiver, the Lorn is our king; He will
save us” (Isaiah 33:22).

The Bible presents law as abiding forever because it reflects
God’s own immutable character:

The works of His hands are truth and justice; all His precepts
are sure, They are upheld forever and ever; they are performed
in truth and righteousness (Psalm 111:7, 8).

Thy testimonies are fully confirmed; holiness befits Thy
house, O Lorn, forevermore (Psalm 93:5),

All commandments are truth. Of old I have known from
Thy testimonies, that Thou has founded them forever (Psalm
119:151b-152).

The sum of Thy word is truth, and every one of Thy right-
eous ordinances is everlasting (v. 160).

The word of our God stands forever (Isaiah 40:8).

The word of man is feeble because his nature is finite and
fallen:

All flesh is grass, and all its loveliness is like the flower of the
field. The grass withers, the flower fades, when the breath of the
Lorp blows upon it; surely the people are grass. The grass with-
ers, the flower fades, but the word of our God stands forever (Isa-
iah 40:6-8).

A Law Unto Themselves

The modern use of law makes it arbitrary, based in one case
on a judge, in another, on the word of a Fihrer like Adolf Hitler.
Thus man becomes the lawmaker, and the word of human author-
ity becomes the new law. The courts, therefore, no longer are
bound by any law. They have in effect become a law unto them-
