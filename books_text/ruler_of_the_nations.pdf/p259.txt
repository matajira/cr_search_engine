The Christian Origins of American Civil Government 239

precedent. But in public, they seldom admit this. “Yes, what the
Court says is the law of the land,” they tell television interviewers.
Then they return to their classrooms and tell their students that
the Supreme Court has only decided one case at one point in
time, but it has not decided the law of the land.

Summary

Jefferson's legacy had its origins in the Enlightenment where
Reason was crowned as god, natural law substituted for Biblical
law, and neutrality became the new legal fiction. The Christian
community was sucked into the vortex of this emerging mythical
world view. But institutions built on myths are collapsing. The
world views based on reason over revelation, natural law over
Biblical law, and neutrality over the religious presuppositions of
Thomas Jefferson in politics, Horace Mann and John Dewey in
education, and Oliver Wendell Holmes in law are disintegrating.

So, the goal is to steadily recover our religious-historical roots,
but without such medieval mistakes as State-financed churches
and State-financed schools. We need to get back to the tradition
that Jefferson fought, but without restoring what the Baptists
fought.

The First Amendment was added to the Constitution to pro-
tect the Church from a national establishment of religion. There
is an abundant amount of evidence supporting the claim that
America’s early history was based on Biblical principles. The
phrase “the separation of church and State” comes from a letter
written by Thomas Jefferson. Jefferson had nothing to do with the
drafting of the First Amendment.

Recent courts and humanist politicians have illegitimately
substituted Jefferson’s anti-Constitutional phrase in place of the
First Amendment. The Communists followed their lead. The
Soviet Constitution maintains that the “Church is . . . separated
from the State.” What the Constitution was clearly designed to
prevent—the intrusion by Congress into state and local ecclesias-
tical affairs—recent interpreters of the Constitution have man-
dated in the name of the Constitution. Christians who have never
