40 Ruler of the Nations

Biblical Laws or Anti-Biblical Laws

The study of any subject must be placed within the limits of a
Biblical worldview. The third principle of the Biblical covenant is
that God lays down the law to man. Man is supposed to exercise
dominion by means of Biblical law. To speak of justice, or any-
thing else for that matter, which is isolated from a Biblical cove-
nantal structure, is to lead us into a sea of ethical subjectivity. A
recent example of such a quest for Bible-less law is the revival in
some Christian and conservative political circles of a so-called
“natural law ethic” as a substitute for Biblical law. (Humanist
legal scholars do not take this revival seriously; they fully under-
stand that Darwinian evolutionary thought destroyed any concept
of humanistic natural law. Man makes his own rules in the post-
Darwin world.)!

We must understand that there is no neutrality in making
laws. A law says one thing is right or wrong. This inevitably in-
fringes on someone’s money, lifestyle, or dreams. Who wins, who
loses? If humanist man has his way, God will lose, and all those
who stand with God. When autonomous (self-law) man’s law pre-
vails, we can expect God’s law to be invalidated (Mark 7:8, 13).

Satan attacked God on the basis of His law: “Indeed, has God
said, ‘You shall not eat from any tree of the garden?’” (Genesis
3:1). He appealed to Adam and Eve to be a law unto themselves
—to “be like God” (v. 5). Even the devil knew there was no neu-
trality. (Of course, the devil is an expert in all of God’s ways. He’s
also an expert in disobeying all of them.) His law and God's law
could not coexist.

But ours is a day of “pluralism.” The God of Scripture has
spoken, but so has the god of convenience, and the god of
tolerance, and the god of expediency, and the god of majority
opinion, and the god of experience, and the god of any other ethi-
cal system that seeks to be a part of America’s legal mainstream.

1. Gary North, The Dominion Covenant; Genesis (Tyler, Texas: Institute for
Christian Economics, 1982), Appendix A: “From Cosmic Purposelessness to
Humanistic Sovereignty.”
