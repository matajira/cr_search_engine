Neutrality Is a Myth 129

near, and Jesus is about to return to remove us from a steadily
decaying world. Such “prophetic pronouncements” have been self-
fulfilling. For generations, Christians have been saying, “This is
it! Now is the time for the end!” Instead of Christians working out
their salvation with fear and trembling, the church of Jesus Christ
has retreated into passivity in fear and trembling.

In the process of retreatism, in the face of an advancing secu-
larism, things have gotten worse. But what do we expect? We've
removed the only preserving factor, the church of Jesus Christ, as
the salt of the earth. Things will be even worse for the next gener-
ation, What will these Christians do? Will they too maintain that
it's the end? Or will they see the errors of the past and work to pre-
serve and reconstruct the world to the glory of God and the ad-
vancement of the Christian faith?

Christians ought to be turning the world “right side up”
through the preaching of the gospel and the application of God's
law to every area of life. This is all possible because Jesus’ king-
dom operates in the world although it does not derive its sover-
eignty, authority, and power from this world.

Jesus was a threat to the religious and political leaders of His
day because He held them accountable to His law. On the other
hand, others looked to Jesus as a political messiah, rejecting His
saving work and the demand for repentance.

The earth belongs to the Lord and to those whom He gives it
as an inheritance. The world has been cleansed by the blood of
Christ; therefore, let us not call unholy what God now calls holy.
God ordained government. Government, even civil government,
is good. The Bible is filled with politics. For example, there are
two books in the Bible titled “Kings.”

Politics is not inherently sinful, Politicians may be, but so are
fathers, mothers, children, teachers, businessmen, ministers, and
doctors. God calls His people to act out the redemption Jesus has
accomplished for the whole universe. Remember, Jesus is the
Savior of the world.

In summary:

1, No man can serve two masters,
