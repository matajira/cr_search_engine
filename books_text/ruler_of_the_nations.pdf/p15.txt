Editor's Introduction xv

church government, or civil government—conforms itself pro-
gressively to God’s standards, it steadily reflects this already existing
theocracy.

Thus, to use the term theocracy to describe a civil government
that is run by the institutional church is a deliberate falsifying of
the Biblical meaning of theocracy. It is the humanists’ favorite and
most successful verbal trap. They have defined theocracy to mean
what they want it to mean, and then they have scared Christians
into political retreat by yelling “Theocracy! Theocracy!” at them
whenever they raise the question of God’s permanent standards
for every area of life. Most humanists define all of life in terms of
politics, or see politics as the primary way to make life better.
Then they redefine theocracy to mean the church’s rule over poli-
tics, for politics is the heart and soul of humanism. (On this point,
see the book by George Grant in the Biblical Blueprints Series,
The Changing of the Guard.)

Humanists usually think of “government” as a top-down bu-
reaucracy that controls almost everything in society. The Bible
teaches that government is actually a general term for self-govern-
ment, family government, church government, and civil govern-
ment. The Bible rejects the whole idea of government as a top-
down bureaucracy. The Bible teaches that government is a bottom-
up appeals court structure. Thus, when humanists paint a picture
of theocracy as tyrannical, they are using a false model— indeed,
an ancient humanist model—to guide their painting.

What the Bible says is that every area of life is to be ruled by God's
permanent principles. This is the Biblical meaning of theocracy: an
earthly reflection of what the Bible says has always existed, namely,
the rule of God in every area of life, not just civil government.

Gary DeMar’s book has made the meaning of theocracy clear.
From this time on, Christians who read it should be able to handle
the misleading (and highly effective) challenge of the humanists
against the rule of God.

Yet despite DeMar’s clarity, it will take an act of will on the
part of each Christian reader to say to himself mentally: “God
rules. God rules everything. He is the only Creator and the final
