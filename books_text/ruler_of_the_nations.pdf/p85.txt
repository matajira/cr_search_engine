God Judges the Nations 65

“victimless” crime. Crime is crime. Crime is an attack on God, a
defiance of His law. Adam and Even sinned in private. They were
“consenting adults.” But they sinned against God. They were pun-
ished by God, and as representatives for all humanity, they
brought sin and judgment into our lives.

God expects lawfully constituted civil governments to restrain
public evil. God’s wrath will come upon any society that allows
public evil to go on without opposition from the civil government.
He will bring horribly painful judgments on those who do not par-
ticipate in such sins, but who are governed by evil rulers. This is
what happened to the seven thousand righteous Jews who went
into captivity to the Babylonians.

Ruler of the Nations

A ruler is a tool that establishes the limits of measurement. We
call it a ruler because it limits us. But without the limits imposed
by units of measurement, we would be blind. We could not exer-
cise dominion. We would be “flying blind.” The limits imposed by
a ruler is the basis of our freedom.

Similarly, a ruling official imposes limits, He enforces the law.
In the case of God, the law is imposed by the Enforcer. God is the
Law-maker and the Judge. He is also the jury. He is the witness.
But aren’t there supposed to be at least two witnesses to convict a
person? Yes, and there are: Father, Son, and Holy Spirit.

We say that God rules the nations, This must mean that He
rules the nations in Aistory. If so, then this means that God judges
the nations in history. Any ruler who refuses to impose his judg-
ment on those under his authority has abdicated. He has resigned
his office. Or else he has died.

“God is dead!” a few silly but consistent pagan theologians
shouted to the world for about two years, 1966-68. For a brief mo-
ment in history, intellectuals played openly with “death of God”
theology. The fad faded rapidly, but in reality, modern man acts
as though he really does believe that God is dead. So did pagan
man. So did Adam in his sin.

Such a challenge really affirms the death of man. A person
