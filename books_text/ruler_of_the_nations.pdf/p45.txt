God Is Sovereign, Not Man 25

Summary

The first principle of Biblical government—church, State,
family, and self-government—is that God is in charge. Govern-
ment must begin with self-government under God. This is as true of
civil government as for all other forms of government.

Humanism’s denial of God’s government means in practice
that the individual’s will is absorbed and denied by the will of the
State. Public policy overrules any contradictory policy held by an
individual, a church, a business, or a civil jurisdiction at the state,
county, or city level. When tyranny finally happens, when the
State’s will becomes the people’s will, the citizenry looks at the
prevailing conditions and asks how it could have happened.
Fingers are pointed all around, when in fact, the finger-pointing
ought to begin with each of us,

A nation that denies God’s government over the individual,
the family, the church, and the State will find itself enslaved to
those who want to be master. The sad thing is that many of us are
willing to let it happen. More than this, we wanted it to happen.
There is always the promise of security, the giving up of a little
more self-determination, the handing over of a bit more jurisdic-
tion to those who want to make us so secure.

The doors of tyranny are closing. Are we motivated to follow
God through the opening, or will we forever find ourselves locked
in the grip of willing power merchants who will use us for their
evil ends? The choice is ours.

The term ‘government’ has many meanings today. Most peo-
ple define it in solely political terms. Older dictionaries, especially
Noah Webster’s American Dictionary of the English Language (1828),
define ‘government’ with a multiplicity of meanings and referring
to numerous jurisdictions. Older textbooks see government as be-
ginning in the home, what we would call “family government.”

Government in the singular belongs to God alone. The church
is a legitimate government with authority and power. God has es-
tablished multiple governing authorities, one of which is civil gov-
ernment. These many authorities were not designed to compete
