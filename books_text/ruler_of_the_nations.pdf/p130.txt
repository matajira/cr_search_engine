no Ruler of the Nations

that the Christian cannot compromise on, These were discussed
earlier in the chapter. Nowhere in Scripture is it a sin to pay a tax
under compulsion,

For the Romans, lordship was personified in the Emperor. For
the Jew, therefore, paying taxes was believed to be an acknowl-
edgment of the Roman gods. This is made clear by the stamp of
the emperor’s face on the coin of the realm: “TI[berius] CAESAR
DIVI AUG|usti] Ffilius] AUGUSTUS,” or, in translation,
“Tiberius Caesar Augustus, son of the deified Augustus.” The
Jews knew whose coin it was. It was used to pay taxes, and it was
the medium of exchange throughout the Roman world. Israel
participated in the Empire; it benefited from roads, a Mediterra-
nean Sea free of pirates, and a common silver currency unit.
Why, then, should the Jews refuse to pay taxes to the State that
provided these benefits? The Jewish leaders were collaborators
with the Roman authorities. Why shouldn't they pay the tax to
their superiors? Why not admit that Caesar was the rightful civil
ruler? It was from him that they derived their own civil authority.
But the Jews, like so many tax protesters of our day, did not want
to admit how economically dependent they had become on
Caesar. They wanted these benefits free of charge.

They also did not want to admit that God had once again
brought them under historical judgment, just as He had in the
days of the judges, with the invasion by Assyria, and their captivi-
ty in Babylon. They had become spiritual rebels, as they proved
when they crucified Jesus. Oppressive taxation always indicates
that the people in general have rejected the God of the Bible. High
taxes are a judgment of God, just like military defeats, pestilence,
and economic crises. The only way to overthrow political oppres-
sion through taxation is to repent before God and acknowledge
that He alone is Savior, Lord, and King.

But what of the faithful? They know that the State is not God.
Two points should be kept in mind. First, it may be that the many
Christians may not delzeve that the State is god, they may, how-
ever, act as if it is. Second, those Christians who do not believe
that the State is god or act as if it is, are not admitting the State is
