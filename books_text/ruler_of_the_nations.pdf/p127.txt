We Must Render Appropriate Service 107

cials of Rome, he was obligated, for he was under compulsion by
God, to preach the gospel. King Agrippa was confronted with the
claims of Jesus Christ and responded by saying, “In a short time
you will persuade me to become a Christian” (Acts 26:28). Paul
responds by saying, “I would to God (lit., Z pray to God), that
whether in a short or long time, not only you, but also all who
hear me this day, might become such as I am, except for these
chains” (v. 29).

It is not enough to have “conservative” rulers who merely fol-
low after the traditions of men. Christians should be working for
Christian leaders whose lives are conformed to the image of Jesus
Christ and who seek to make the Word of God the law of the land.
Moreover, Christians must preach the whole counsel of God to all
men— especially to civil rulers, to whom much has been given and
of whom much will be required.

Civil governments have the responsibility to punish evil doers
and promote the good. The task of civil government at all levels is
to exercise its authority in its jurisdiction and settle disputes be-
tween conflicting jurisdictions. When disputes and/or crimes are
committed, the State must act swiftly and justly, The standard of
judgment is the Word of God, “for it [the God-ordained authority]
is a minister of God to you for good” (Romans 13:4). Notice that
Paul declares that the State is a minister to you for good. Paul has
a Biblical moral order in mind when he speaks about the opera-
tion of the State as minister.

In the Old Testament, the priests who were experts in the law
of God, instructed the king on how he should apply the details of
the law to various civil issues (Deuteronomy 17). Unfortunately,
the church no longer sees its calling as prophetic. Of course, there
are those in the civil sphere who despise the absolutes of God’s
Word and anyone who would hold them accountable.

Pursuing Peace

Peace can only be realized through the life-transforming gos-
pel of Jesus Christ. Genuine and lasting peace will not come
through law, force, political promises or compromises, the
