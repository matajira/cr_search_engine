Il. Law/Dominion

3
PLURAL LAW SYSTEMS, PLURAL GODS

To the law and to the testimony! If they do not speak accord-
ing to this word, it is because there is no light in them (Isaiah
8:20).

Justice! What a long quest it has been for mankind. In every
society, men seek justice, and they never seem to find it. Why not?
Because they do not seek the God of the Bible, in whom alone
there is perfect justice. Why not? Because He dispenses perfect
justice, and imperfect men know what this means: “For all have
sinned and fall short of the glory of God” (Romans 3:23).

What men say they are seeking is honest judgment. But
honest judgment implies a standard: honest law. Honest judg-
ment is not based on the wishes of the few or the will of the
mighty. It is based on the wishes of the Mighty One. Honest judg-
ment can only be secured when we turn to a law that rests outside
the partisan interests of fallen men. Honest judgment must rest on
objective norms of law and justice. But modern man does not want
to admit that such objective standards exist. They tell us to search
elsewhere. Should we appeal to the latest polling statistics? Is the
United States Supreme Court the final court of appeal for
Americans?

What the Bible says is that the final court of appeal is the judgment
seat of God. There is unquestionably a Supreme Court, but it is not
some national Supreme Court; it is a heavenly court. No earthly
court of appeals should be given absolute and final jurisdiction.

39
