80 Ruler of the Nations

war with other men. Under certain circumstances the individual
is given the authority to attack and kill an intruder (Exodus 22:2).
The civil magistrate is God’s “avenger who brings wrath upon the
one who practices evil” (Romans 13:4). The civil magistrate’s sym-
bol of authority is the “sword,” an instrument of death (Romans
13:4). Israel was commanded to have an army of armed men
(Deuteronomy 20). While peace is what we all desire, war often is
a reality we must face and prepare for.

Quarantine

Fifth, civil government has the power to quarantine, to protect
human life. Plagues can race through whole populations because
of the infectious nature of the diseases transmitted through casual
and sexual contact. The individual with the infectious disease
“shall live alone” (Leviticus 13:46). Even his home can be “quaran-
tined” after “an inspection” (Leviticus 14:33-53). If the disease is
not abated, then even his house can be torn down (wv. 39-42). The
State is given legislative power to deal with plagues, epidemics,
venereal diseases, and other contagious and dangerous diseases
like Acquired Immune Deficiency Syndrome (AIDS).

Defining and Defending Private Property

Sixth, civil government has the duty to protect private prop-
erty. When individuals, families, churches, and business estab-
lishments possess property, they have an area of liberty and do-
minion that is beyond the reach of men with greater power and in-
fluence. The Bible is explicit about how property is to be ac-
quired. Confiscation through State power is not legitimate. The
commandments “You shall not steal” (Exodus 20:15} and “You
shall not covet” (Exodus 20:17) are meaningless unless there are
prior owners who are secure in their right to hold the land. When
Naboth refused to sell his land to king Ahab, the king devised a
plot to kill Naboth in order to confiscate his land: “Arise, take pos-
session of the vineyard of Naboth, the Jezreelite, which he refused
to give you for money” (1 Kings 21:15).

The individual in self-government must assist the police pow-
