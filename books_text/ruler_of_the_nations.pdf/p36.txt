16 Ruler of the Nations

Church members are part of a jurisdictional government
called “ecclesiastical government.” The church is given the “keys
of the kingdom of heaven” and with these keys the leadership can
“bind” and “loose” within the church (Matthew 16:19). The
church, that is, those who are in authority, have the authority to
excommunicate unrepentant members (Matthew 18:15-18). The
church is even given power to handle legal matters that many
would see as the exclusive power of the State (1 Corinthians
6:1-11). In the book of Hebrews, we are told to “Obey your leaders,
and submit to them; for they keep watch over your souls, as those
who will give an account” (Hebrews 13:17). Ultimately, God will
demand an accounting from men regarding their obedience to the
authorities, but this includes ai! authorities, not just civil authorities,

The State has the power of the sword: “[I]t does not bear the
sword for nothing” (Romans 13:4). Because the State has legiti-
mate authority, Peter can write: “Submit yourselves for the Lord’s
sake to every human institution, whether to a king as the one in
authority, or to governors as sent by him for the punishment of
evildoers and the praise of those who do right” (1 Peter 2:13, 14).

The One and the Many

God has established numerous authorities for the proper or-
dering of society. Mothers and fathers have authority over their
children (Proverbs 6:20, 21; 15:5; 30:17; Ephesians 6:1-3; Colos-
sians 3:20). Church leaders, elders and deacons, hold authority in
the church (Matthew 16:19; 18:15-20; 1 Thessalonians 5:12, 13;
1 Timothy 5:17, 18; Hebrews 13:17; 1 Peter 5:1-3). Civil rulers exer-
cise political authority by God’s decree (Matthew 22:21; Romans
13:1-7; 1 Peter 2:13, 14).

In other relationships, contracts can bind individuals and
groups subject to the stipulations of a contract. The employer-
employee relationship is contractual and carries with it legitimate
authority (Leviticus 19:13; Deuteronomy 25:4; 1 Timothy 5:18; cf.
Matthew 10:10; Luke 10:7). The courts, the judicial arm of civil
authority, enforce the obligations of contracts by punishing the
contract-breaker and seeing to it that restitution is paid to the con-
