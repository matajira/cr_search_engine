154 Ruler of the Nations

Holy Spirit working through obedient, dominion-oriented Chris-
tians (Matthew 16:18). Victory for the people of God is certain:
not because of their own strength, but because of the strength of
the One whom they serve, even Jesus Christ, unto whom all
power in heaven and earth has been given, and who is with His
people always, even unto the end of the age (Matthew 28:18-20).
The Christian’s hope is found in the resurrected Jesus Christ who
sits at the right hand of His Father. Present-day Christians are to
know the present-day power of that resurrection and act upon it
(cf. Philippians 3:10; Romans 6:5).

If Satan doesn’t have as much power as God does, then Satan’s
human followers also cannot have as much power as God’s human
followers do, unless the latter voluntarily renounce their responsi-
bilities. If God’s people are faithful, power will flow to them (Deu-
teronomy 28;1-14).

It is time that we start to build a Christian civilization.

The Question of Time

One of the most debilitating “doctrines” of the church is the
belief that Jesus is coming back soon— meaning man’s concept of
“soon”—and that the world is headed for inevitable destruction.
There is nothing Christians should do to try to stop the inevitable.

When Jesus’ disciples asked Him at the ascension if at that
time He was “restoring the kingdom to Israel,” Jesus diverted their
attention from final restoration to the work at hand: “It is not for
you to know times and epochs which the Father has fixed by His
own authority; but you shall receive power when the Holy Spirit
has come upon you; and you shall be My witnesses both in Jeru-
salem, and in all Judea and Samaria, and even to the remotest
part of the earth” (Acts 1:6-8). In effect, Jesus was saying, “Do not
worry about God’s timetable; it is already fixed. Busy yourself
with the affairs of the kingdom.”

Some of the Thessalonian Christians were “leading an undisci-
plined life, doing no work at all, but acting like busybodies”
(2 Thessalonians 3:11). While this may have little to do with a
preoccupation with “the day of the Lord” (1 Thessalonians 5:2), it
