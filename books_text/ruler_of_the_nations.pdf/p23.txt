AUTHOR'S INTRODUCTION

Any kingdom divided against itself is laid waste; and any city
or house divided against itself shall not stand. And if Satan casts
out Satan, he is divided against himself; how then shall his king-
dom stand? (Matthew 12:25-26).

Jesus was here contrasting His kingdom with Satan’s. Christ’s
is a unified kingdom, God is sovereign over His kingdom, yet
men are responsible agents. God runs it from the top with perfect
justice and perfect knowledge, a kingdom based on law. But men
choose whom they will serve. They are either covenant-keepers or
covenant-breakers. God’s kingdom is unified, and it serves as
leaven, steadily replacing the divided kingdoms of the devil.

This book is about God's kingdom. God’s kingdom begins in
heaven, but it extends to earth. Christ announced after His death
and resurrection: “All authority has been given to Me in heaven
and on earth. Go therefore and make disciples of all the nations,
baptizing them in the name of the Father and the Son and the
Holy Spirit, teaching them to observe all that I have commanded
you; and lo, I am with you always, even to the end of the age”
(Matthew 28:18-20). With these words, Matthew ends his gospel.

There are five crucial concepts found in these words. These
concepts pattern the ten commandments found in Exodus 20. The
first table of five commandments deals with what man owes to
God while the second table shows what man owes to his fellow
man. Without the first five the second five will not work. The
sanctity of life, respect for the marriage relationship, and the
rights of private property are dependent upon a transcendant God
who gives mandates in an unchanging law for individuals and

3
