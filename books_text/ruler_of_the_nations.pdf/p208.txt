188 Ruler of the Nations

The family provides a training ground for world leadership,
for it is the first institution in which people learn “followership.”
Church leadership is also to be cultivated in the family. The
church leader “must be one who manages his own household well,
keeping his children under control with all dignity (but if a man
does not know how to manage his own household, how will he
take care of the church of God?)” (1 Timothy 3:4-5).

Civil leadership also develops out of family leadership. The
choice of rulers in Israel was based on prior leadership in the fam-
ily and tribe: “Choose wise and discerning and experienced men
from your tribes . . .” (Deuteronomy 1:13; cf. Exodus 18:17-26; 1
Samuel 2:12-17, 22-36). Paul gives us a hint of the extension of
godly leadership into the world: “Do you not know that the saints
will judge the world?” (1 Corinthians 6:2).

Ethics/Law/Dominion

Third, parents must teach their children the law of God. The
Bible is very clear about this:

And these words, which I am commanding you today, shall.
be on your heart; and you shall teach them diligently to your sons
and shall talk of them when you sit down in your house and when
you walk by the way and when you lie down and when you rise
up (Deuteronomy 6:6-7).

Without an understanding that it is God who has “laid down
the law,” children will not clearly recognize the nature of family
authority. They will be tempted to rebel, or to transfer their loyalty
to the gang, the State, or some rival institution.

As they get older, they will no longer be impressed with the
answer, “Because I say so,” to the question, “Why should I do
what you say?” This is why God first lays down the law to parents,
as we have seen: “And these words, which I am commanding you
today, shall be on your heart.” Family government begins with self-
government. God’s government is inside-out government.

As Gary North demonstrates in his economic commentary on
the Bible, The Sinai Strategy (1986), the Eighth Commandment,
