God Judges the Nations 55

decentralized political power, leaving men free to work out their
cultural gifts and skills in their own way.

God went to Sodom and Gomorrah as a Judge. He went to see
whether the sinful outery of these cities was as bad as it sounded,
Like Abel’s blood crying to God from the ground (Genesis 4:10),
sin cries up to God, and God hears. But as a Judge, He first con-
ducts a trial, like the one He conducted in the garden, when He
cross-examined Adam and Eve before bringing judgment, or just
as He cross-examined Cain after Cain had murdered Abel (Gen-
esis 4). He gathers evidence. Then He judges it in the light of His
law, Then He announces His judgment and brings either bless-
ings or cursings.

Collective Responsibility

Abraham bargained with Ged to spare Sodom (and therefore
Lot) as soon as he was told that God was about to judge Sodom.
“Wilt Thou indeed sweep away the righteous with the wicked?”
Abraham asked (Genesis 18:23). Are righteous men who happen
to be dwelling among the unrighteous also to be destroyed, just
because of the sins of the immoral majority?

God’s answer was clear: Ys. Abraham knew this. But he hag-
gled with God over the “price.” Will you spare the city for fifty
righteous people? Yet Abraham’s language is highly significant:
“Far be it from Thee to do such a thing, to slay the righteous with
the wicked, so that the righteous and the wicked are treated alike.
Far be it from Thee! Shall not the Judge of all the earth deal justly?”
(18:25). God is the Judge of “all the earth” and not just of some
people at certain times in history. Ail nations are subject to His
tule. His law is for all the nations.

God, knowing full well that there was but one righteous man
in the city, was willing to promise Abraham that He would indeed
spare the city for the sake of fifty righteous people. So Abraham
tried to get a better deal. What about forty-five righteous people?
God agreed: He would spare it for forty-five (18:28). And so it
went, until Abraham got God to agree to spare the city for as few
as ten righteous people (18:32).
