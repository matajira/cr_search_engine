150 Ruler of the Nations

all our problems, or even the main solution.

The purpose of Christians’ involvement in politics ought to be
more than the mere replacement of non-Christians with Chris-
tians. There are numerous things that civil government should
not be doing that it is doing—and doing with the support of most
voters. A civil government based on Biblical law would mean a
drastic reduction in its size and power and a return of jurisdiction
to individuals, private enterprise, families, churches, and local
civil governments.

The church has awakened (at least some of the church has) to
her responsibilities in the area of world view thinking. Christians
are beginning to realize that the Bible has answers, that the Bible
addresses the world with specific solutions to seemingly perplex-
ing problems. For the Christian, the Bible is a blueprint for build-
ing a Christian civilization.

There are many Christians and non-Christians who do not
like to think of the Bible in these terms. In fact, many are horrified
at any suggestion that the world is in any way redeemable. They
are content to ignore verses like “God so loved the world” (John
3:16); “God was in Christ reconciling the world to Himself” (2
Corinthians 5:19); Jesus is “the light of the world” (John 8:12),
“the Savior of the world” (4:42), and “the Lamb of God who takes
away the sins of the world!” (1:29). And how about this one?: “The
kingdom of the world has become the kingdom of our Lord, and of
His Christ; and He will reign forever and ever” (Revelation 11:15).

Now, don’t get me wrong. To say that the world is redeemed is
not to say that the world can be made perfect. For example, the
dead sinner is redeemed, but he is not perfect. Judicially, the sin-
ner stands before God as perfect because he has the perfect right-
eousness of Christ imputed to him. He is not made righteous; he is
declared righteous. This, however, does not exempt him from
conforming his life more and more to the image of Jesus Christ.
This process is called sanctification.

God expects the redeemed sinner to conform his behavior to
the commandments of God, though the effects of his sin nature
will not be eradicated until he is raised “imperishable” (1 Corinthi-
