V._Inheritance/Continuity

5
CALL NO MAN YOUR FATHER

And do not call anyone on earth your father; for One is your
Father, He who is in heaven. And do not be called leaders; for
One is your Leader, that is Christ (Matthew 23:9, 10).

Before the Bible will be used as the blueprint for civil govern-
ment, Christians will have to repudiate the promise made by poli-
ticians: “We will take care of you.”

God is the source of our protection. He alone can provide true
safety —from invaders, criminals, famine, pestilence, and all the
other judgments that happen to men. Most important, He pro-
tects His people from the final judgment of hell. God alone is our
Father in heaven. We have no father in Washington, London,
Paris, Moscow, or any other national capital.

Most modern advocates of the caretaker State, meaning the
paternal State, believe that the only hope for mankind is the citi-
zen’s relinquishing of personal responsibility and handing more
and more authority over to the “experts” who work in the bureau-
cratic halls of civil government. Some have called this “womb to
tomb” security. This is the false security of the prison cell. Any so-
ciety that seeks to substitute the State for God will eventually find
itself enslaved. There is no freedom apart from Jesus Christ and
His saving grace. There is no freedom apart from God’s covenant.
Our long-term safety is assured by God to us if we are covenant-
ally faithful. This is the fifth point of the Biblical covenant: contin-
uity. Nothing else can give our work continuity. Only God shows
mercy to His people for thousands of generations, if they keep His
commandments (Exodus 20:6).

69
