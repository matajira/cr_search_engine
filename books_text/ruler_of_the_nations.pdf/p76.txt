56 Ruler of the Nations

Then God sent two angels to Sodom in order to warn Lot to
get out. He in turn warned his daughters and their prospective
(betrothed) husbands, but the two men scoffed (Genesis 19:14). So
only Lot, his wife, and his two daughters left the city, and God
then sent fire and brimstone down on Sodom and Gomorrah.

Fire and brimstone: These words have become synonymous in
the English language with preaching that emphasizes hell and
final judgment. This is quite proper; this is exactly what God’s de-
struction of Sodom and Gomorrah is supposed to remind us of.
But what Christians fail to recognize is the context of those fearful
words. God was not raining fire and brimstone down on the heads
of people as individuals; He was raining fire and brimstone down
on a pair of cities. Individuals perished, of course, but they
perished as members of cities whose sins had cried out to God for
collective judgment. Abraham in his negotiating with God, had de-
scribed God well: the Judge of all the earth.

God would have spared the two cities for the sake of as few as
ten righteous people. But this was easy for God to agree to: There
were not that many righteous people in those cities. The principle
is clear, however: God is sometimes willing to spare a Jarge num-
ber of evil people for the sake of a few righteous ones. It was so
bad in Jeremiah’s day, that God challenged him to locate a single
righteous person: “Roam to and fro through the streets of Jeru-
salem, And look now, and take note. And seek in her open
squares, If you can find a man, If there is one who does justice,
who seeks truth, Then I will pardon her” (Jeremiah 5:1). God
knew that Jeremiah would not find that person.

In Elijah’s day, over a century earlier than Jeremiah, God
promised the destruction of Israel, the northern kingdom. They
would be destroyed by the sword, and carried into captivity. God
promised to preserve seven thousand righteous people who would
not bow the knee to Baal (1 Kings 19:18). In this case, there were
not enough people in the land to save Israel from God's historic
judgment. Still, this did not mean that every righteous person
would die. He would keep a handful of them alive—not many,
but a few. In this case, He judged the collective, but kept a rem-
