Rebuilding Takes Time 157

out of the past. And we know that in the future, our good gifts will
be put to good use by our spiritual heirs.

Not so the God-haters. Their gifts will either be cut off, or else
be inherited by God’s people. “The wealth of the sinner is stored
up for the righteous” (Proverbs 13:22b). Our God shows loving-
kindness to thousands of generations, to those who keep His com-
mandments (Exodus 20:6). The text in Exodus 20 says that He
shows lovingkindness to thousands, but most commentators agree
that the comparison is between the three or four generations that
He patiently endures sinners (Exodus 20:5) and the thousands of
generations of kindness that He shows to the righteous.

We can build up spiritual capital, economic capital, educa-
tional capital, and political capital over many generations. This is
our task before God. Slow growth over many generations is the
proper approach. What is God’s way for His people? Hard work
and thrift. Satan, knowing that his time is short (Revelation
12:12), recommends a different program for his human followers:
huge risks, borrowed money to fund “big deals,” and going for
broke. A few generations is all he can expect, unless Christians vol-
untarily give him more time by retreating into cultural irrelevance.

What we need now is not a string of miracles. Miracles are
helpful, but what we need is a revival. We need to preach (and be-
lieve!) the whole counsel of God. We need a full-orbed gospel that
offers comprehensive salvation. We need healing—not just spiritual
and physical healing, but cultural healing. We need the doctrine of
the resurrection—the resurrection, restoration, and reconstruc-
tion of every area of life, to the glory of God. We need to fulfill the
dominion covenant of Genesis 1:26-28: to subdue the earth to the
glory of God. We need the fulfillment of the great commission: the
discipling of the nations.

And how can we get this? By simple covenantal faithfulness,
by doing our normal daily jobs well, and then by doing a lot
more. We have God’s law, the tool of dominion. We have His
Holy Spirit to empower us. We have Calvary behind us, which
dealt a mortal blow to Satan. After all, can we Christians seri-
ously believe that Satan is more powerful after the Cross than be-
fore it? That makes no sense.
