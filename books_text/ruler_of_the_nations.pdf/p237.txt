Reconstructing Civil Government 217

Christians should run for office, in order to get power in the
various government hierarchies. Then they should vote against
every expansion of power and every tax hike and every bond
issue. The State must be cut back.

This is the battle: the belief that the State is the only important
government. As self-governed Christians, we must work to cut
back the unbridled power and authority of the State. Dominion in
the area of civil government does not mean that we desire the
escalating power base available to those who seek and hold office.
Rather, we should run for elected office to pull on the reins of
power, to slow the growth of power run wild.

But Christians must also recognize that we need a peaceful
transfer of power to a new Bible-based system of multiple author-
ities. They must recognize that God will drive out our enemies lit-
tle by little, over many years (Exodus 23:29, 30). We are not to
become revolutionaries. We are not to impose a top-down tyranny
to ram the Bible down people’s throats. The goal is to use every
means available to educaie voters, and only then to transform their
increasingly Biblical outlook into legislation. Mostly, it will be
legislation abolishing past legislation.

Ethics/Law/Dominion

The first step in overturning the messianic State is to place
ourselves under God's law. We must meditate on the law. We must
make the 119th psalm our hymn of obedience.

The second step is to teach our children the law (Deuteron-
omy 6:6, 7). We must demonstrate to them by our actions that we
are self-governed by the law,

Third, we must proclaim the law to others. We must abandon
the false theology that New Testament Christians are in no sense
obligated to obey God’s Old Testament law. We obey the sacrifi-
cial law by baptizing people and eating the Lord’s Supper. We
obey Biblical laws against murder, adultery, and many other capi-
tal crimes in the Bible.

Fourth, we must elect public officials who say they will vote for
Biblical laws. First and foremost, this means voting to prohibit
