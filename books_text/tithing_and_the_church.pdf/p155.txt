Sacraments or Social Gospel? 141

view of the sacraments is only slightly better. Rushdoony’s
theology does defend gospel preaching as a function of the
church, thereby avoiding the liberal version of the social gospel.
But the institutional church has three aspects: the preaching of
the gospel, the administration of the sacraments, and the au-
thority to police access to the sacraments, i.e., church discipline
(the keys of the kingdom). One searches in vain in Rushdoony’s
writings for even one page devoted to a theological exposition
of the discipline of the church. He steadfastly refuses to discuss
the meaning of the keys of the kingdom. This is why he has
never published so much as a chapter on the doctrine of the
church: sacraments, tithe, and discipline.

Rushdoony’s view of the institutional church is not even
remotely Reformed. He uses Calvinist phrases, but he long ago
abandoned Book IV of Calvin's Institutes. His ecclesiology is not
a little bit wrong; it is completely wrong. It is not accidental
that he has refused to write a book on the doctrine of the
church, nor has an issue of The Journal of Christian Reconstruction
been devoted to this topic. This is one reason — I believe the
primary reason — why he has never completed his long-prom-
ised systematic theology. He holds an unorthodox doctrine of
the church, and he cannot publish a section on ecclesiology
without exposing himself to widespread criticism from orthodox
theologians, not to mention Chalcedon’s donors. Dispensation-
alist Lewis Sperry Chafer had a similar problem in his Systematic
Theology (1948), so he muddled his sections on the church;
Rushdoony, in contrast, refuses to write a systematic theology.
To write it, he would have to present a theological defense of
his own refusal to take communion or attend weekly church
services for over two decades. His published views of the sacra-
ments, regular church attendance, and tithing are an integrated
whole. He believes that all three are voluntary actions. His
published bits and pieces of the doctrine of the church reveal
his radical individualism. His published doctrine of the church
is libertarian.
