34 TITHING AND THE CHURCH

late 1840’s who were escaping the potato famine) or they saw
tremendous opportunities in America (e.g., skilled workers).

By the 1880’s, steamships were bringing waves of immigrants
from southern Europe and the Mediterranean. Economist
Thomas Sowell writes: “The changeover from sailing ships to
steamships was sudden and dramatic. As of 1856, 97 percent of
passengers arriving in New York came on steamships. Changes
in origin were almost as dramatic. Whereas more than four-
fifths of all European immigrants to the United States came
from northern and western Europe in 1882, by 1907 more than
four-fifths were from southern and eastern Europe.”®

A similar phenomenon is taking place today in every wealthy
nation. The jet airplane is the primary vehicle, Voters recognize
that they dare not give recent immigrants lawful access to the
voting booth. The newly arrived immigrants could vote their
way into the wallets of those who presently control the civil
order. If mere physical presence entitles a person to the civil
franchise, no one’s wealth is safe in a society that believes that
the State can confiscate other men’s property, All modern soci-
eties believe in a modified eighth commandment: “Thou shalt
not steal, except by majority vote.” This is why immigration
barriers arrived with the massive increase in taxation during
the First World War. Prior to 1914, no Western European
nation issued compulsory passports to its citizens. Today, they
all do. Every nation also requires foreign visitors to present
their passports at the border.

Modern Christians recognize judicial issues in politics long
before they recognize similar issues in the church. Having
considered immigration and civil government, we now turn to
immigration and church government.

5. Thomas Sowell, The Econamics and Politics of Race: An International Perspective
(New York: William Morrow, 1983), pp. 151-52. C£ Sowell, Ethnic America: A History
(New York: Basic Books, 1981), pp. LE
