168 TITHING AND THE CHURCH

that lawfully administers the sacraments rests today, as it did in
the Mosaic law, on the uniquely sacramental character of the
church. The judicial mark of the church’s institutional sover-
eignty is its control over lawful access to the sacraments. This
control necessarily involves the enforcement of a boundary: the
right to include and exclude. The church’s authority to exclude
people from the Lord’s Supper is the ultimate judicial basis of
its discipline. Excommunication means exclusion from Holy
Communion: the Lord's Supper. Because the institutional
church possesses this sacramental monopoly, it alone possesses
the authority to collect the full tithe of every member.

This authority to exclude is imparted to church officers by
means of their possession of the keys of the kingdom. “And I
will give unto thee the keys of the kingdom of heaven: and
whatsoever thou shalt bind on earth shall be bound in heaven:
and whatsoever thou shalt loose on earth shall be loosed in
heaven” (Matt. 16:19). Without access in history to the keys of
heaven, there can be no kingdom of Christ in history: no heav-
enly keys = no earthly kingdom. The keys invoke heavenly sanc-
tions; often, they invoke visible earthly sanctions. A king with-
out sanctions in history is not a king in history. The most im-
portant sanctions in history are in the hands of those who con-
trol the keys to the kingdom: officers of God's visible church.

Rushdoony has, in recent years, poured out his verbal wrath
on the institutional church in his attempt to broaden the defini-
tion of the church to include the family and non-profit educa-
tional institutions, and, in his words, “far, far more.”* This is
why Rushdoony’s view of the visible church has undermined his
theology of the kingdom of God in history. Volume 2 of The
Institutes of Biblical Law undermines Volume 1. What was a flaw
no larger than a man’s hand in Volume 1 became a whirlwind
in Volume 2. It stripped him of his doctrine of the church

2. R. J. Rushdoony Law and Saciety, vol. 2 of The Institutes of Biblical Law (Valle
cito, California: Ross House, 1982), p. 337.
