9

SACRAMENTS OR SOCIAL GOSPEL?

Will a man rob God? Yet ye have robbed me. But ye say, Wherein
have we robbed thee? In tithes and offerings. Ye are cursed with a curse:
for ye have robbed me, even this whole nation. Bring ye all the tithes into
the storehouse, that there may be meat in mine house, and prove me now
herewith, saith the Lorp of hosts, if I will not open you the windows of
heaven, and pour you out a blessing, that there shall not be room enough
to receive it. And I will rebuke the devourer for your sakes, and he shall
not destroy the fruits of your ground; neither shall your vine cast her
fruit before the time in the field, saith the Lorp of hosts. And all nations
shall call you blessed: for ye shall be a delightsome land, saith the Lorp
of hosts (Mal. 3:8-12).

The Bible does not speak of multiple storehouses of the
tithe; it speaks of only one storehouse. If a society violates this
single storehouse principle of the mandatory tithe, it brings
itself under God’s negative corporate sanctions: “cursed with.a
curse.” If it obeys this principle, it gains God’s positive corpor-
ate sanctions: “and all nations shall call you blessed.”

Note carefully that the word is storehouse (singular), not
storehouses (plural). But this is not how Rushdoony has sum-
marized the text: “The tithe was given to the Levites, who
stored the animals and grain in storehouses (Mal. 3:10) until
they could either be used or sold. It is a silly and self-serving
