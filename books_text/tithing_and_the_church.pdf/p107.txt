Primary Sovereignty: Church or Family? 93

In his view, the great war for the minds of men is the war
between family and State. The Bible teaches otherwise.

What Rushdoony fails to recognize is that the New Covenant
priesthood did not originate with Abraham. It originated with
Melchizedek. Abraham paid his tithe to Melchizedek, and he
received bread and wine from him (Gen. 14:18). Jesus Christ’s
high priestly office was grounded in Melchizedek’s primary
priesthood, not Levi’s secondary and judicially subordinate
priesthood (Heb. 7:9-10). Here is the fatal flaw in Rushdoony’s
familiocentric argument: Melchizedek had no parents (Heb. 7:3). 1
take this literally: Melchizedek was therefore a theophany. At
the very least, he had no genealogy, indicating that his authori-
ty was not derived in any way in the family. Melchizedek is the
refutation of Rushdoony’s ecclesiology and therefore of his
entire familiocentric social theory.

The Biblical Position: Ecclesiocentrism

I have long disagreed with Rushdoony on the centrality of
the family in Christian society. The fundamental institution in
history is not the family; it is the church, which extends beyond
the final resurrection as the Bride of Christ (Rev. 21). The
family does not: there is no marriage in the resurrection (Matt.
22:30). Jesus made it plain: the false ideal of the sovereign
family is a far greater threat to Christianity than the false ideal
of the sovereign State. Jesus never spoke this harshly regarding
the State:

Think not that I am come to send peace on earth: I came not
to send peace, but a sword. For I am come to set a man at vari-
ance against his father, and the daughter against her mother,
and the daughter in law against her mother in law. And a man’s
foes shall be they of his own household. He that loveth father or
mother more than me is not worthy of me: and he that loveth,
son.or daughter more than me is not worthy of me. And he that
taketh not his cross, and followeth after me, is not worthy of me.
