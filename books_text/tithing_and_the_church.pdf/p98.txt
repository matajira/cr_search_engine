84 TITHING AND THE CHURCH

ment of the Lord’s Supper. This ended only in late 1991. I
suggest reasons for this remarkable and overnight change of
opinion in Chapter 10,

An Open Division Since 1981

The Christian Reconstruction or theonomist movement re-
ceived its initial theoretical formulation in 1973. In that year,
Craig Press published two books: R. J. Rushdoony’s Institutes of
Biblical Law and my Intreduction to Christian Economics. Rush-
doony’s book contained three appendixes by me; my book
contained one by him. What had been a multi-volume negative
critique of modern culture, beginning with Rushdoony’s Intellec-
tual Schizophrenia (1961), became in 1973 a positive statement of
faith. The Chalcedon Report newsletter had been publishing bits
and pieces of this positive agenda since its founding in 1965,
but only in 1973 did the preliminary general statement appear,
with footnotes. In that same year, I began my monthly column
in the Chalcedon Report: “An Economic Commentary on the
Bible.”

In 1981, the Christian Reconstruction movement openly split
into two rival camps. The co-founders of the movement could
no longer agree with each other on some fundamental issues.'
The official basis of the division was a brief four-page manu-
script written by me and submitted to Rushdoony as a monthly
column in the Chalcedon Report. I had based the essay in part on
an observation in James Jordan’s 1980 Westminster Theological
Seminary master’s thesis.

Rushdoony in a letter accused me and Jordan of blasphemy
— not mere heresy — and wrote to our pastor, Ray Sutton, about
these supposedly blasphemous ideas, demanding that we recant
in writing and agree never to discuss the topic again in our
future writings. In his letter to me, which was written on Chal-

1. See Chapter 6, below, subsection on “The Doctrine of the Church in Christian
Reconstruction.”
