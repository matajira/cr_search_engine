20 TITHING AND THE CHURCH

commission paid to salesmen is the great motivator. Some com-
panies may pay as much as 20 percent of gross revenues to the
sales force.

God, the owner of the whole earth, has established the most
generous commission structure in history: 90 percent after
expenses is retained by the sales force. Any business that would
offer its sales force 90 percent after expenses would attract the
most competent salesmen on earth. The firm would be flooded
with applicants for any sales position that might open up. This
is what God offers to His people. They keep 90 percent; His
church receives ten percent; the State is entitled to no more
than ten percent (I Sam. 8:15, 17). But men rebel. They think
this tithe burden is too onerous. They have been deceived.

The Gon Artist

Satan appears.on the scene and makes a more attractive
offer: “Keep it all!” He can afford to make this offer: he does not
own the company. He is like the con artist who walks into a tem-
porarily empty office and signs up salesmen as if he were the
president of the company. He makes his money on the back
end of the transaction when he sends his goons to collect pay-
ments from the salesmen.

The salesmen have kept all the money from their efforts.
The goons then make the salesmen an offer they cannot refuse.
The Mafia calls these goons “enforcers.” Civil government calls
them “revenue agents.” Their purpose in each case is the same:
to extract far more than ten percent of net earnings from the
naive but now-trapped salesmen. He who refuses to pay faces
unpleasant consequences: broken bones or a bullet in the head
(Mafia); fines, tax liens, or jail sentences (civil government).

‘The victims went into the deal thinking they could get some-
thing for nothing. They firmly believed that someone would
gladly provide them with productive capital and also allow
them to keep everything they earned from their own labor. Any
wise man would have spotted the offer as fraudulent as soon as
