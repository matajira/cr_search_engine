Conclusion to Part 2 177

primary locus of social authority: the family. Geneva, he says,
was “associated with patriarchalism.” By this he means a society
that is “family-governed.”" He calls for the creation of a “Bib-
lical, patriarchal culture.”'*

The problem here is the same old problem of all non-Trini-
tarian social theory: the family is not sufficiently powerful to
resist the encroachment of the modern State except during
times of anarchy, when warlords and other familistic local ty-
rants replace established civil governments. The church, if
stripped of its covenantal authority, would become an adjunct
to either the church or the State. It could not retain its judicial
status as a separate covenantal authority, This.has been the
drift of Rushdoony’s ecclesiology ever since the publication of
Volume 1 of The Institutes of Biblical Law: a non-covenantal
church, ie., a church with no claim on men’s tithes.

Patriarchal political theory moves in two directions: toward
(1) national kingly rule by a monarch who rules as the lawful
representative of all the families of society; (2) local clannism:
the warlord society. In the early twentieth century, the kings
departed. Intellectually, they had departed from Anglo-Ameri-
can political thought after 1690: John Locke’s seldom-read First
Treatise on Civil Government, a detailed refutation of Sir Rob-
ert Filmer’s Patriarcha (1680). This leaves clannism, which in-
cludes the world’s many oath-bound secret societies, criminal
and social, that are satanic substitutes for the family.

History does not move backward. Rushdoony's proposed
society - patriarchalism - cannot be resurrected without a total
social collapse. It cannot possibly resist the acids of modernity
without the support of the church. By stripping the institutional
church of its covenantal status, Rushdoony has abandoned the
family to the tender mercies of the modern State.

13. Iid., p. 408.
14. Bid., p. 409.
