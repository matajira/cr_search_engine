Conclusion to Part 1 79

of sovereignty of the family — in God's eyes ~ is its authority to
retain and allocate at least 80 percent of everything it earns,
net. The modern messianic State has made great inroads on
family sovereignty. It has also made inroads on church sover-
eignty, though not at so great a rate.

The church has failed to defend its legitimate sovereignty
over its members. This failure is visible in the fact that it has
not preached the tithe as a morally mandatory tax on members.

The church in the United States has also failed to defend its
legitimate sovereignty with respect to the State. It has become
fearful of the State because the State threatens to revoke the
tax-exempt status that the State has granted to the churches.
The Western church outside the United States has been in
subjection to many humanist States throughout the twentieth
century. The church is visibly in retreat: theologically, cultural-
ly, and economically. This retreat will eventually end.

The era of extended State sovereignty is drawing to a close.
There is a worldwide tax rebellion going on, and it will escalate.
Economic decentralization will place far greater power into the
hands of individuals and small businesses than ever before. The
question is: What will replace the messianic State? Will it be the
Christian church and the Christian family? Or will it be some
pagan imitation of either or both?

By tying my defense of the tithe to a defense of the sacra-
ments, I have focused on the twin monopolies that God has
granted to the institutional church. Their unity cannot be bro-
ken, despite attempts by theologians, pietists, and pagans to
dismiss the first as annulled and deny the judicial relevance of
the second.

If the institutional church is to regain the pre-eminence it
once had in the West, let alone extend its influence throughout
the world, it must preach the moral obligation of the tithe, the
judicial relevance of the sacraments, and the church’s absolute
monopoly over both. If it refuses to do this, it will remain on
the defensive: culturally, economically, and judicially.
