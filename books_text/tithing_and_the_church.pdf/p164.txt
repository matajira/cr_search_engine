150 TITHING AND THE CHURCH

employees). Holy Communion was never served at these Bible
studies, nor was it served at his Los Angeles Bible studies, also
held at 11 a.m., from the late 1960’s until he canceled them in
the early 1980's. But in late 1991, he began serving communion
in the evening. This chapter suggests the reason.

What is my evidence? First, I was there during part of this
period, 1973-75. Second, I offer the following statement by
David Graves, who audiotaped all of the Chalcedon Sunday
meetings for almost a decade in the 1970's. He writes:

I attended the Sunday morning meetings held by Rev. R. J.
Rushdoony for Chalcedon at Westwood California from January
1972 to August 1981, on a regular basis.

During this time communion was never taken at the meetings
I attended. No communion service for these services was ever
announced at the meetings I attended. I never heard anyone say
that communion was taken at these meetings, during this time
period.

Baptisms, however, were performed from time to time for
members of the group. Some of my own children were baptized
by Rev. Rushdoony at these meetings.”

According to Gary Moes, this was still the practice of
Rushdoony and Chalcedon as late as April, 1991, when he was
still in attendance. Moes served as the editor of the Chalcedon
Report from 1987 until May, 1991.? A few months later, Rush-
doony bought a set of clerical robes and began serving the
Lord’s Supper. Later in this chapter, I survey the highly reveal-
ing chronology of this radical transformation.

People occasionally ask me where Rushdoony got his doc-
trine of the church. Since he has never written an explicit doc-

2. Letter, To Whom it May Concern, Nov. 4, 1993. Address; 7505 Ridgeway
Ave., N. Richland Hills, TX 76180-2933.

3. Moes was fired that April; the dispute over the payment of his salary was
settled out of court.
