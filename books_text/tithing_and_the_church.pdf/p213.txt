Index

Marx, Karl, 39-40

maturity, 31

McCulloch v. Maryland, 63-65

Melchizedek, 1-2, 45-46, 59,
93, 112, 133, 181-82

Miladin, George, 153

Milbank, Norman, 159

Miller, C. John, 151

Moabites, 30

Moes, Gary, 150

money, 23, 130

Mosaic Covenant, 2

nagging, 26

natural law, 96-98
neutrality, 73-75

New Covenant, 1, 2, 24
New World Order, 13, 33
nominalism, 14

oath
central, 8-9
church, 42
covenants, 78
family, 8, 68
sovereignty &, 7-9
State, 8
tithe &, 105
offerings, 13
Orthodox Presbyterian Church,
101-2, 119, 151, 153-4, 155

parachurch
accountability, 50
church &, 50-51
church-financed, 11
deficits, 54-55
offerings, 58

199

Rushdoony on, 119

telegrams, 54-55

tithe &, 134
Pasadena, 152
Passover

circumcision &, 32

Levites &, 31

Lord’s Supper &, 35, 38

Melchizedek &, 2
passport, 33
pastors

debt &, 11

employment, 41

fearful, 72

immigrants &, 38

Levites &, 181

preaching, 26
patriarchalism, 96, 116, 177
Peacocke, Dennis, 85
perfection, 18
persecution, 67
pietism, 72-73
pluralism, 6, 66-69, 76
politics, 66-67, 73, 74, 184
polytheism, 66-69
poverty, 51-52, 59
Powell, Edward, 101
power religion, 61, 96
prayers, 69
preaching, 26, 66-67
Presbyterian Church of
America, 156-60
priesthood

Jesus Christ, 2

Levitical, 2, 31

Melchizedek, 2

New Covenant, 38
productivity, 106-8, 129
