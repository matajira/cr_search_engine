48 TITHING AND THE CHURCH

building. Unlike medieval cathedrals, however, the buildings
that today’s churches build are unlikely to become architectural
classics that inspire men for centuries. They probably will not
survive the next outward wave of urban blight. Or as we could
say of Rev. Schuller’s crystal cathedral, “People who preach in
glass houses shouldn't build on the San Andreas fault.”

Pastors beg. Congregations make down payments on new
buildings. Then they struggle for years to meet mortgage pay-
ments. Mortgage debt transfers power to spiritual blackmailers:
“Preach what we like to hear or we walk!” To tickle their ears,
pastors preach less and less from the law of God. They preach
possibility thinking, or positive confession, or some other vari-
ant of “think and grow rich.” If they are more traditional (post-
1830) in their theology, they preach the doctrine of the immi-
nent Rapture,” which promises to relieve God’s people from
the pressure of paying off heavy mortgages. Lutheran and most
Calvinist pastors preach amillennialism: the eschatology of
Christianity’s guaranteed defeat in time and on earth, but with-
out the hope in an imminent Rapture. So, God’s royal priest-
hood shuffles along, looking over its collective shoulder for
bullies.

If local congregations want more income, here is a sure-fire
way to get it:

1, Require every voting member to tithe: no tithe-no vote.

2. Have deacons police the voting members’ incomes, just as the
IRS polices it. Deacons represent an institution with greater
covenantal authority than the State lawfully possesses.

8. Organize evangelism programs that bring more people into
the congregation.

4. Challenge newcomers and non-voting members with a vision

2. Gary North, Rapture Fever: Why Dispensationalism is Paralyzed (Tyler, Texas:
Institute for Christian Economics, 1998).

8. Gary North, Millennialism and Social Theory (Tyler, Texas: Institute for Chris-
tian Economics, 1990), ch. 9.
