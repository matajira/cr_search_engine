Conclusion to Part 2 173

Thus, what we call church offices are in reality functions of
the body of Christ in this world. This fact is very important.
Offices lead to a bureaucracy and a ruling class, whereas func-
tions keep a bady alive.t

Notice the implied dichotomy: bureaucracy = ruling class vs.
function = life. This is not only a false dichotomy; it is a ridicu-
lous dichotomy. The State has judicial officers. The State is
ordained by God as an agency of government. Is there no
escape from subordination to a permanent ruling class merely
because the State has bureaucracies? Can the State therefore
not advance in history in response to God’s word and men’s
obedience to God’s law? Rushdoony has argued the opposite
throughout his career. Then why suggest a necessary conflict
between a church’s bureaucracy and life? The whole line of rea-
soning is silly. It is beyond silly: it is desperate. He continues:

In the early church also, we-have no evidence of what is
commonplace today, regular, stated bureaucratic meetings of
presbyteries, synods, councils, bishops, etc. Instead, beginning
with the Council of Jerusalem in Acts 15, the meetings were
called to resolve a problem or meet a need, They were functional
Meetings, not organizational; they were aspects of the life of a
body, not of a bureaucratic organization. They exercised no
coercive power, but they did formulate questions and answers
pertaining to faith and morals carefully and precisely.’

The Council of Jerusalem was far more than a functional
meeting. That Council laid down the law - God’s revealed law
- regarding what gentile believers had to refrain from doing
{Acts 15:29). There was no coercion, Rushdoony asserts. This is
an incredible statement coming from a Trinitarian theologian.
Undergirding this representative council was the most fearful

4, Idem,
5. Idem.
