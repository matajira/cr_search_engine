Primary Sovereignty: Church or Family? 95

We must examine both assumptions: one incomplete and the
other incorrect.

Anti-Statism

Conservatives regard the family as the only institution with
sufficient authority and respect to challenge the State success-
fully on a long-term basis."* They view the social function of
the institutional church as an adjunct to the family, just as
liberals see the church as an adjunct to the State. Conservatives
rarely view the institutional church as a covenantally separate
institution possessing superior authority to both family and
State. This is a serious error of analysis.

The authority to excommunicate is the greatest judicial authority
exercised in history. The lawful negative sanctions of the rod
(family) and the sword (State) are minor compared to the sanc-
tion of excommunication (Matt. 16:19). But because formal
excommunication does not impose bodily pain in history, mod-
ern man dismisses the church’s authority in both history and
eternity. This includes modern conservatism. It also includes
most Protestant churches, who refuse to honor each other’s
excommunications. They thereby deny Jesus’ words: “And fear
not them which kill the body, but are not able to kill the soul:
but rather fear him which is able to destroy both soul and body
in hell” (Matt. 10:28). The only agency in history that lawfully
announces a person’s condemnation to hell - short of repen-
tance before physical death — is the institutional church. This
authority is implicitly recognized by the modern Western State.
A condemned criminal on his final walk to the place of execu-

14. One of the strongest statements to this effect was written by G. K. Chester-
ton. The family, he wrate, “is the only check on the state that is bound to renew itself
as eternally as the state, and more naturally than the state.” Chesterton, “The Story
of the Family,” in The Superstition of Divorce (1920); The Collected Works of G. K.
Chesterton, vol. 4 (San Francisco: Ignatius Press, 1987), p. 256. His reference to
eternity betrays his confused social cheology: neither the human family nor the state
is eternal; the church is (Rev. 21, 22).
