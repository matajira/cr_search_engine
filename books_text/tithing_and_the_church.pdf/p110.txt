96 TITHING AND THE CHURCH

tion cannot legally be accompanied by his spouse or his political
representative; he can be accompanied by a minister.

The battle between patriarchalism and statism in the West
has been going on at least since the rise of the Greek city-state,
an outgrowth of clans and family sacrifices.’ The problem is,
the family always loses this battle as a covenant-breaking society
advances over time because the family does not have the power
possessed by the State: the monopoly of life-threatening vio-
lence. Step by step, the State replaces the family in the thinking
of most members of covenant-breaking society. The State pos-
sesses greater power; in the power religion of humanism, this
justifies the expansion of the State.

The family fights a losing defensive battle when it fights
alone. Its authority is steadily eroded by the State. For example,
the divorce rate rises when the State replaces the family’s func-
tions, especially its welfare functions. Therefore, if the familio-
centric view of the church were true - the church as an adjunct
to the family ~ the church would inevitably lose alongside of the
family. Yet this view of the church is widely held today. Result:
those people inside various church hierarchies who seek power
have increasingly allied themselves and their churches with the
State.!6

Natural Law Theory

An implicit natural law theory. undergirds conservatism’s
social analysis: belief in the existence of moral absolutes that are
discoverable by universal logical principles. This faith in moral-
logical universals undermines the judicial authority of the
church. The Trinitarian church is not universal in human

15, Fustel de Coulanges, The Ancient City: A Study on the Religion, Laws, and
Institutions of Greece and Rome (Garden City, New York: Doubleday Anchor, [1864]
1955).

16. C. Gregg Singer, The Unholy Alliance (New Rochelle, New York: Arlington
House, 1978). This book is a detailed history of the Federal Council of Churches and
its successor, the National Council of Churches.
