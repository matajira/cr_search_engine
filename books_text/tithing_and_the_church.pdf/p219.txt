ABOUT THE AUTHOR

Gary,North received his Ph.D. in history from the University
of California, Riverside, in 1972. He specialized in colonial U.S.
history. He wrote his doctoral dissertation on Puritan New
England’s economic history and the history of economic
thought. A simplified version of this dissertation has been pub-
lished as Puritan Economic Experiments (Institute for Christian
Economics, 1988).

He is the author of approximately 35 books in the fields of
economics, history, and theology. His first book, Marx’s Religion
of Revolution, appeared in 1968. His Introduction to Christian Eco-
nomics appeared in 1973, the year he began writing a multi-
volume economic commentary on the Bible, which now covers
Genesis, Exodus (three volumes), and Leviticus, He was the
general editor of the Biblical Blueprints Series (1986-87), a 10-
volume set, for which he wrote four of the books.

Beginning in 1965, his articles and reviews have appeared in
over three dozen newspapers and periodicals, including the
Wall Street Journal, Modern Age, Journal of Political Economy,
National Review, and The Freeman.

He edited the first fifteen issues of The Journal of Christian
Reconstruction, 1974-81. He edited a festschrift for Cornelius Van
Til, Foundations of Christian Scholarship (1976). He edited two
issues of Christianity and Civilization in 1983: The Theology of
Christian Resistance and Tactics of Christian Resistance. He edited
Theonomy: An Informed Response (1991).

He is the editor of the monthly financial newsletter, Remnant
Review. He writes two bi-monthly Christian newsletters, Biblical
Economics ‘Today and. Christian Reconstruction, published by the
Institute for Christian Economics.

He lives in Tyler, Texas, with his wife and four children.
