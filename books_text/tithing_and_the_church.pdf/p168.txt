154 TITHING AND THE CHURCH

OPC, having been formally cleared, before another complaint
could be lodged. This was in 1970. The records of the OPC
show that he was received by the Anglican Churches of America
in 1974,§ a four-year delay. He continued to lecture on Sunday
mornings and evenings at his Chalcedon Bible studies.

Rushdoony had based his defense on the fact that the Sun-
day morning Bible study group was not a church. He could
prove this by pointing out that the meetings did not involve the
sacrament of the Lord’s Supper. For the next two decades, he
continued to maintain that these meetings were not church
meetings despite their 11 a.m. time slot. His judicial problem
was that he ceased to attend any local church on a weekly basis
after he ceased preaching at the local Anglican Orthodox
Church in Orange County. He was under no local congrega-
tion’s authority, which (as of 1993) is the only ecclesiastical
authority he says is legitimate.

The Appearance of Formal Worship

The Chalcedon Sunday meetings were structured as church
worship services: hymns, prayers, responsive readings, and
“sermons.” There were no membership rolls and no Lord's
Supper. That is to say, there were no ecclesiastical sanctions. As
a Chalcedon employee, I spoke once a month at these Bible
studies in 1973, 1974, and 1975. So did Greg Bahnsen, who
was also on the Chalcedon payroll. Functionally (though never
officially), our job was to provide an alternative to church atten-
dance.® Rushdoony once pointed out to me that I did not be-
gin my sessions with formal prayer, and unnamed attendees
had complained that my meetings were not enough like church

8. The Orthodox Presbyterian Church 1936-1956, edited by Charles G. Denison
(Philadelphia: Committee for the Historian of the Orthodox Presbyterian Church,
1986), p. 349.

9. Was this wrong of me? I think so. Neither of the founders of this movement
has clean hands ecclesiastically.
