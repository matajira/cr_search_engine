7

RUSHDOONY’S ECCLESIOLOGY

General William Booth, founder of the Salvation Army, rightly
ridiculed the church as a mremsy factory,

R. J. Rushdoony (1992)!

We come now to Rushdoony’s view of the institutional
church. He subordinates it to the doctrine of the family. In
doing so, he adopts familiocentrism, though not natural law
theory. His abandonment of theonomy in favor of traditional
conservativism has undermined the very foundation of his
theology. His view of church and family was an anomaly in his
original theology — an error no larger than a man’s hand. Like
Elijah’s cloud, however, it has grown into a mud-producing
storm since 1981.

Rushdoony has systematically avoided developing a doctrine
of the institutional church, either in print or on audiotape. I
believe his refusal to present his ecclesiology is deliberate. His
few scattered works on ecclesiology written since 1970 have
broken not only with the Westminster Confession of Faith
(which he officially had to affirm until he resigned from the

LR. J. Rushdoony “Reconstructing the Church,” Calvinism Teday, IL (July
1992), p. 24.
