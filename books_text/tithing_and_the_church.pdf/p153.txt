Sacraments or Social Gospel? 139

the Levites’ separation from the other tribes in terms of their
provision of social services. This cannot be done textually.
Numbers 18 is clear, as we have seen: the separation of the Levites
From the other tribes was based on their unique access to the temple and
its sacrifices. This separation was based on a geographical boundary
— legal access to the tabernacle/temple— and not on their provi-
sion of social services, especially educational services.

Is the education of children lawfully a function of the
church, the State, or the family? Rushdoony has always denied
the legitimacy of education by the State, but he has been ambiv-
alent regarding the educational authority of church and. family.
“The Christian school is a manifestation of the visible church,
and at the same time, an extension of the home.” But which
one possesses institutional sovereignty? Economically, the answer
is clear: the agency that funds education. What about judicially?
On this point, Rushdoony has been ambivalent. But this much
is clear: if education was the function of the Levites, and this
function was separate from the sanctuary (.e., the sacrifices), as
he insists was the case, then the Levites as educators were un-
der the authority of families if families paid for education by
allocating their tithes. This is exactly what Rushdoony’s theol-
ogy of the tithe concludes, This means that pastors as Levite-
educators (i.e., as tithe-receivers) are under the authority of families.
Since he denies the sacramental character of the church, he
strips the church of all covenantal authority. It cannot impose
sanctions for non-payment of tithes. Once again, we are back to
familism-clannism.

Rushdoony’s voluntaristic view of the tithe is shared by most
of the modern church and most of its members, which is why
the modern church is impotent, judicially and economically.
This is why statism has visibly triumphed in our day. Rush-
doony admits this when he writes that “the abolition of the tithe

4, Rushdoony Intellectual Schizophrenia: Culture, Crisis and Education Philadelphia:
Presbyterian and Reformed, 1961), p. 42.
