10

THE CHRONOLOGY OF
RUSHDOONY’S ECCLESIOLOGY

There is a type of leiter I receive which, until now, I have tried to
answer patiently, not always with success. No more. These letters say that
they have heard something bad about me from someone else, i.e., that I
do not believe in communion, or, that I hold false views of hermeneutics,
or, that at this point or that I am theologically, morally, or what have
you, off base, No evidence is offered (for there is none), and I am asked
to answer the charges! Such letters are highly immoral and unChristian.
Instead of writing to me, such persons should challenge the accuser to
provide evidences of the charges, and to denounce them as liars and
slanderers, because they cannot produce evidences. By coming to me they
are morally wrong, since the tale bearer is the one who must be confron-
ted, evidence demanded, and then charged before the church.

R. J. Rushdoony (1993)!

Mr. Rushdoony has issued a challenge. I do indeed regard
his published statements on the institutional church as, in his
words, off base — and not just off base: completely outside the
ball park of orthodoxy. I regard his refusal to join a local
church and take Holy Communion for over two decades as a

1. R. J. Rushdoony “Random Notes, 28,” Chalcedon Report (Oct. 1993), p. 81.
