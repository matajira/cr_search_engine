Table of Contents

Preface 6.6... cece eee eee tenes ix

Part 1: Church Sovereignty and the Tithe
Introduction to Part 1... . eee cece eee eee eee 1

  
  

1, Sovereignty and the Tithe . -. 8
2. Authority and the Tithe .. 2-15
8. Church Membership and the Tithe --27
4. When Royal Priests Beg ........ ~» 45
5. Tithes, Taxes, and the Kingdom .. .. 60
Conclusion to Part 1.1... cece ee eee ee eens 78

Part 2: Rushdoony on Church, Tithe, and Sacrament
Introduction to Part 2.0... -- eee cece eee eee eras 83

 

6. Primary Sovereignty: Church or Family? . «87
7. Rushdoony’s Ecclesiology ..... . - 100
8. The Legal Basis of the Tithe .. . » 123
9. Sacraments or Social Gospel? ...........2.004- 136
10. The Chronology of Rushdoony’s Ecclesiology .... 148
Conclusion to Part 2.2.1... eee eee eee eee 165
Conclusion ©. 0... pee cece ee eee ee eee eee 181
Scripture Index . 0... ccc cece reece cere eens 190
Index vce escent ee ences en eceneneene 193

About the Author .......--- 6s ee ee eee teen eeee 205
