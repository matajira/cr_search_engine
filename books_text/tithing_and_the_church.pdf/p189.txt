Conclusion to Part 2 175

did formulate questions and answers pertaining to faith and
morals carefully and precisely.” This pictures the church’s
councils as if they were little more than occasional meetings in
some seminary’s faculty lounge. But in 1968, he wrote: “The
creedal controversies were not merely theological debates whose
scope was restricted to the intellectuals of the church.”” They
were life-and-death matters, for individuals and societies, and
they still are.' If Christian reconstruction is to become a reality,
he wrote, the church must once again recognize the call to
warfare that the ancient creeds demanded:

Itis not enough, in dealing with a present danger, to avoid it by
citing the fact that someone dealt with the matter in the past. If
an enemy attacks today, the enemy must be fought today, but
without a surrender of past victories. A church cannot say, if
men arise within its ranks denying the infallibility of Scripture,
that it cannot deal with these men today, because the confession
dealt with the matter a few centuries ago. Rather, it must affirm
the old confession by a new condemnation of heretics. This the
Second Council of Constantinople did.?

In 1968, Rushdoony was still orthodox in his view of the
church: he defined it as preaching, sacraments (capitalized!),
and discipline, “The marks of the true church are thus, first, the
true preaching of the Word of God, the infallible Scriptures;
second, the right administration of the Sacraments, i.e., in faith-
fulness to Scripture; and éhird, the faithful exercise of discipline
in terms of Scripture. The means of grace are the word and the
sacraments.” By 1981, he had dismissed the sacrament of the
Lord’s Supper as. judicially empty. “As against an empty rite,
Christian fellowship in Christ’s calling, around a table, is closer

7, Bid, p. 33.
8. fbid., p. 219.
9. Tbid., pp. 111-12.
10. Zid., p. 179.
