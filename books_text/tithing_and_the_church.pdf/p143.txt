The Legal Basis of the Tithe 129

same principle also applies to the tithe. Neither tithes nor taxes
are the basis of dominion: net productivity is. That is, growth is
the basis of dominion. Where there is no doctrine of progressive
dominion in history, there is no doctrine of economic
growth.” This growth of God's kingdom comes primarily
through two processes: (1) the confiscation of Satan’s assets
through God’s adoption of Satan’s human disciples; (2) the
economic growth enjoyed by God’s human disciples, which
enables them to redeem the world through purchase.”

The kingdom of Christ, broadly defined, must be equated
with the total efforts of covenant-keepers: heart, mind, and soul.
What is my conclusion? First, all of the tithe goes to the local
church. Second, gifts and offerings can go to other charities.
Third, the kingdom of Christ is extended by total productivity,
including economic productivity. Fourth, total economic pro-
ductivity, not charity, is the primary economic means of extend-
ing God’s kingdom in history. This is why God promises long-
term economic growth to covenant-keeping societies (Deut.
28:1-14). More wealth per capita should come from covenant-
keeping men than is used up by them.’* Covenant-keepers
should leave a positive economic legacy to their grand-
children.* “A good man leaveth an inheritance to his child-

as atheism. Such anti-Christian men saw the state as man’s savior, and as a result they
favored placing more and more power in the hands of the state. The South was the
last area to accept the property tax, and it was largely forced on the South by
post-Givil War Reconstruction.” When imposed before the Givil War, the property
tax was limited to the county, where “only owners of real property could vote on the
county level.” Rushdoony, Chalcedon Newsletter #24 (Sept. 1967).

1L. Gary North, Js the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1988).

12. There is a third way: military conquest. But this method of dominion is not
primary. It is lawful only when it is the result of successful defensive campaigns that
produce comprehensive victory in wars launched by God's enemies.

13, E. Calvin Beisner, Prospects for Growth: A Biblical View of Population, Resources,
and the Future (Westchester, Illinois: Crossway, 1990).

14, This is one reason why a Christian should instruct his heirs not to put him
on a life-support system once two physicians say that it is unlikely that he will recov-
