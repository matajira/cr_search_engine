INTRODUCTION TO PART 2

And I say also unto thee, That thou art Peter, and upon this rock I will
build my church; and the gates of hell shall not prevail against it, And
I will give unto thee the heys of the kingdom of heaven: and whatsoever
thou shalt bind on earth shall be bound in heaven: and whatsoever thou
shalt loose on earth shall be loosed in heaven (Matt. 16:18-19).

In Part 1, I presented my case for the morally mandatory
nature of the tithe, the church’s authority to collect it from its
members, and the church’s monopoly over the sacraments as
the judicial basis of its right to collect and distribute the tithe.
In the Conclusion to Part 1, 1 wrote:

By tying my defense of the tithe to a defense of the sacra-
ments, I have focused on the twin monopolies that God has
granted to the institutional church. Their unity cannot be bro-
ken, despite attempts by theologians, pietists, and pagans to
dismiss the first as annulled and deny the judicial relevance of
the second.

I now come to the writings of the premier American theolo-
gian who has most forcefully dismissed the church’s authority
to collect the tithe: R. J. Rushdoony. He has tied his rejection
of the institutional church’s authority over the tithe to a rejec-
tion of the church’s authority in general. And, as we shall see,
for over two decades he personally abstained from the sacra-
