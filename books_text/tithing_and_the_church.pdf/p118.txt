104 TITHING AND THE CHURCH

Church and Sanctions

In contrast to the family, both State and church are lawfully
entitled to economic support from those who are under their
respective covenantal authorities. The State’s jurisdiction is
territorial (e.g., over non-covenanted resident aliens) and judi-
cial (e.g.. over its covenanted citizens who live outside the
State’s territory*). The church’s jurisdiction is equally judicial,
though not (in Protestant societies) territorial. Both institutions
have lawful claims before God over a small portion of the net
productivity of all those under their jurisdiction. Their God-
given authority to impose negative sanctions against those who
refuse to pay is the outward mark of their covenantal sover-
eignty. 7d deny the right of either church or State to bring such sanc-
tions is a denial of their covenantal sovereignty.

Rushdoony has understood this with respect to the State; he
has therefore opposed the tax revolt or “patriot” movement?
But he has denied that any payment is automatically owed to
the institutional church. No church can lawfully compel its
members to pay it their complete tithe or even any portion
thereof, he insists. “It is significant, too, that God's law makes
no provision for the enforcement of the tithe by man. Neither
church nor state have [sic] the power to require the tithe of us,
nor to tell us where it should be allocated, i¢., whether to
Christian Schools or colleges, educational foundations, missions,
charities, or anything else. The tithe is to the Lord.” He then
cites Malachi 3:8-12. With respect to the tithe, Rushdoony
believes in the divine right of the individual with respect to the
institutional church: no earthly appeal beyond conscience. This
is not an error of logic on his part; it is a consistent application

8. U.S. citizens living outside the U.S. must pay income taxes on their salaries.
The first $70,000, however, is exempt.

9. R. J. Rushdoony, “The Tax Revolt Against God,” Position Paper 94, Chalcedon
Report (Feb. 1988), pp. 16-17.

10, Zbid., p. 16.
