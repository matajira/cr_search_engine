194 TITHING AND THE CHURCH

Chesterton, G. K., 95n.
Christ for the Nations, 58-59
Christian Reconstruction, 84,

89-93, 122, 166
Christians

autonomy, 23-24

democracy &, 35

dependents, 17

second-class citizens, 26
church

Abrahamic promise, 3

activism, 73

adjunct?, 95, 177

apostate, 126-27

assault on, 3

authority, 46-47, 167

bankers, 11

baptism, 8

beggar?, 47-49

begging, 51

Bible &, 6

biblical Jaw &, 10

body, 172-73

buildings, 11, 47-48

Calvin on, 110

central institution, 8-9, 61,

94
charity, 11, 89
Christian Reconstruction
&, 89-93

claims, 101

cloister, 58

confirmation, 35

contractual, 42

covenant, 6, 53

covenant-ignoring, 10

debt, 11, 43, 72

definition, 120, 175-76, 183

definition (Rushdoony’s),
117-19

democratic, 35

discipline, 123, 141

doubt, 8

eldership, 40-41

eternity, 8, 94

excommunication, 70, 95,
108, 112

exits from, 14

faculty lounge?, 175

family &, 92-98

fellowship, 109

franchise, 11

fund-raising, 11

“gag rule,” 67

Great Commission, 8, 19-
22

guardian, 167

hammer, 12, 14

hierarchy, 111, 113

immigration &, 35-40

impotent, 71

income, 48-49

influence, 28

informant, 69

inheritance, 122

jurisdiction, 104, 120

keys of kingdom, 60, 106,
123

leftovers, 12

liturgy, 7

members, 120

membership, 126-27, 130-
31

membership (see church
membership)

monopoly, 59, 76
