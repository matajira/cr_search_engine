Authority and the Tithe 25

writings and speculations of covenant-breakers.’ This is exactly
what Christians have been doing from the days that Christian
apologists began to appeal to Greek philosophy as the founda-
tion of common-ground truths. It is this quest for common-
ground principles of reasoning that Cornelius Van Til rejected
as a compromise with the devil.’°

Another way to deny the moral necessity of tithing is to
declare, with fundamentalism, “We’re under grace, not law!”
The result of such a universal affirmation is the self-conscious
surrender of history to covenant-breakers. Christians then find
themselves under pagan laws and pagan lawyers."

A third way is to affirm that God’s Holy Spirit will inform
each Christian how much to give. This opens the Christian to
feelings of guilt, either because he thinks he has to give more
than the tithe - but exactly how much? — or because he gives
less and worries about it. Guilt produces doubt. Guilt and
doubt are not conducive to entrepreneurship and economic
growth."®

A fourth approach is to affirm the mandatory tithe, but then
deny that the institutional church has any legal claim on it. This
leaves the tither in control over the allocation of his tithe. This
is an affirmation of man’s autonomy, but in the name of coven-
antal faithfulness."*

All four approaches deny God’s warning through Malachi.
All four seek to evade man’s responsibility to bring one-tenth of
his increase to the single storehouse, the house of God.

9. Gary North, Millennialism and Social Theory (Tyler, Texas: Institute for Chris-
tian Economics, 1990), ch. 7.

10. Cornelius Van Til, A Christian Theory of Knowledge (Nutley, New Jersey:
Presbyterian & Reformed, 1969).

11, Gary North, Political Polytheism: The Myth of Pluralism (lyler, Texas: Institute
for Christian Economics, 1989), Part 8,

12. David Chilton, Productive Christians in an Age of Guilt Manipulators: A Biblical
Response ta Ronald J. Sider (Sth ed; Tyler, Texas: Institute for Christian Economics,
1990).

13, See Part 2, below.
