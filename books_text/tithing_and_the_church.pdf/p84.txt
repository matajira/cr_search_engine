70 TITHING AND THE CHURCH

respect to their enemies that almost no denominational hymnal
today includes all of the psalms, especially the psalms calling
down God’s judgment on His enemies, nor do the prayer books
include all of the psalms. Few Christians have ever heard an
imprecatory psalm directed from the pulpit against an abortion-
ist, let alone a public official.

Excommunication

Protestant Christians (and a lot of married ex-Catholic
priests) have abandoned the idea that excommunication means
very much. It is seen only as a temporary annoyance. If excom-
municated, a person can always walk down the street and join
another church. He is not told that excommunication has eter-
nal consequences. He is not even subjected to official temporal
consequences. Excommunicates laugh in their hearts at the idea
that anything a church’s officers say judicially has any effect in
history or in eternity. They see themselves as immune from
judgment by the church.

Having lost their fear of the efficacy of this rarely applied
church sanction, Christians have also lost respect for church
government generally. This is their first step toward hell and its
earthly manifestation, political tyranny. Societies cannot escape
external government, so the State steps in to replace the vacu-
um created by the church’s defection. This was understood by
Paul from the beginning, which is why he called on the church
of Corinth to judge its own disputes and not seek peace in
pagan civil courts (I Cor. 6).

‘Tyrants increasingly recognize the universally acknowledged
impotence of church sanctions. They understand all too well
that if the church is not seen as God’s authorized representative
agency, it can exercise only minimal authority. To be a repre-
sentative government means that its officers speak judicially in
the Sovereign’s name. This is certainly true of church govern-
ment: Because Christians almost universally ignore a local
church when it speaks judicially against them in God’s name,
