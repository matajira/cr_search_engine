152 TITHING AND THE CHURCH

The Question of Ecclesiastical Jurisdiction

Rushdoony moved to the Los Angeles area from northern
California in the summer of 1965. In the fall of 1964, I had
recommended to Mrs. Grayce Flanagan that she contact him.®
She did. Later, she suggested that he move to Los Angeles to
start a weekly lecture series in Westwood Village, close to
UCLA. He did. He then founded Chalcedon, which operated
for several years under the legal umbrella of Americanism
Education, Inc., a non-profit educational foundation that had
been set up by Walter Knott, the conservative founder of
Knott's Berry Farm. Rushdoony lectured every Sunday after-
noon in Westwood and every Sunday evening in the Pasadena
area.

He was still a Presbyterian pastor. His membership was in
the Northern California Presbytery of the Orthodox Presbyteri-
an Church. Officially, he was laboring outside the bounds of
presbytery, which meant that he remained under the Northern
California Presbytery’s jurisdiction while living in Southern
California. Under Presbyterian law, ministers are members of
their presbyteries, not local congregations. He was therefore not
a local church member. This was a pattern that was never again
to be broken: any church authorities who were officially over
him were always far removed from him geographically.

Bible Study or Church?

Initially, he preached Sunday mornings in Orange County
for a small congregation of a tiny denomination, the Anglican
Orthodox, Church. He had to drive over two hundred miles
each Sunday to meet his speaking responsibilities. In the late
1960's, he abandoned this part-time ecclesiastical employment,
so his mornings were open. He did not start a church nor did

6. I met her while 1 was shopping in the Betsy Ross Book Store in Westwood
Village. She was a part-time employee.
