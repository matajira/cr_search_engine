Rushdoony’s Ecclesiology 101

ministry of the Orthodox Presbyterian Church in 1970) and the
Thirty-Nine Articles of Episcopalianism (which he has officially
affirmed since 1974), but with all of Trinitarian orthodoxy from
the Council of Nicea forward.

Critics of the church’s lawful, God-ordained claim on every
individual's lifetime commitment again and again seek to ele-
vate “Christianity” and dismiss “the church,” as if there could
somehow be Christianity without the church and its mandated
sacraments. One sign of a person’s move away from historic
Christianity’s doctrine of the church to conservative humanism
is his adoption of the pejorative word, Churchianity.2 The per-
son who dismisses “churchianity” is often a defender of his
personal ecclesiastical autonomy: a sovereign individual who
judges the churches of this world and finds them all sadly
lacking. In his own eyes, all the churches fall short of his almost
pure and nearly undefiled standards. No church announces
God’s authoritative word. to him; rather, he announces God’s
authoritative word to the churches. No church officer repre-
sents him before God; instead, he represents himself. Like the
foolish defense lawyer who hires himself as his own advocate in
a court of law, so is the man who is contemptuous of “church-
ianity.” He confidently excommunicates all churches for failing
to meet his standards. All congregations have failed to measure
up, except (should he deign to begin one) his own. He ignores
the obvious: a self-excommunicated person is no less excommunicated.

Rushdoony’s views on the institutional church have become
adjuncts to his theory of the tithe. Prior to his assertion in 1992
of the Chalcedon Foundation’s status as a church as well as a
governmentally chartered educational organization, his views
on the tithe were fully consistent with his views regarding the
visible church. They constituted a single, consistent, and monu-
mental error. This error, if applied retroactively to the conclu-

2. For a good example, see Rushdoony’s editorial, “Copycat Churchianity”
Chalcedon Report (June 1992),
