SOVEREIGNTY AND THE TITHE

Bring ye all the tithes into the storehouse, that there may be meat in
mine house, and prove me now herewith, saith the Lorn of hosts, if I will
not open you the windows of heaven, and pour you out a blessing, that
there shall not be room enough to receive it (Mal. 3:10).

One storehouse, one tithe: this is the heart of the matter. The
day that covenant-keeping men multiply storehouses for God’s
tithe is the day they begin to lose the blessings of God in histo-
ry. Why? Because the existence of many storehouses reveals
that men no longer believe that there is a single, sovereign,
God-authorized collector of the tithe: the institutional church.
Their tithes are broken up into a series of offerings; then these
offerings are perceived as morally voluntary; then this moral
voluntarism transfers visible sovereignty to the donor: he who
pays the piper calls the tune.

The sovereignty of the donor over his tithe is an illusion.
This form of sovereignty cannot remain with the individual.
Individuals possess delegated sovereignty, but they cannot
retain it if they rebel against the ultimate Sovereign, God. They
refuse to tithe; then the State’s tax collector steps in and im-
poses compulsion. The State increasingly calls the tunes.
