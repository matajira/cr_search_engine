Tithes, Taxes, and the Kingdom 75

anything you like — at a given point in its history, you always find
that there was a time before that point when there was more
elbow room and contrasts weren't quite so sharp; and that
there’s going to be a time after that point when there is even less
room for indecision and choices are even more momentous.
Good is always getting better and bad is always getting worse:
the possibilities of even apparent neutrality are always diminish-
ing. The whole thing is sorting itself out all the time, coming to
a point, getting sharper and harder.

So, there is no long-term hope in buying time through si-
lence, unless Jesus comes again very soon to pull His people
out of oppression. This has been the widespread belief among
American fundamentalists for over a century, but now that
hope is fading. Pretribulational dispensationalism is losing its
adherents, either to postribulational dispensationalism or to
postmillennialism.” These Christians are no longer banking
on the so-called Rapture as their cosmic escape from their
earthly troubles, including political oppression. As this faith in
the escape hatch in the sky has faded, Christians have begun to
acknowledge humanism for what it is, namely, an aggressive
religion of empire that will allow no independent authority for
churches unless they worship the State.

We saw ail this during the Roman Empire. It did no good
for the churches to seek to buy time by toning down the com-
prehensive message of Christ’s world-transforming gospel. It.
will do no good this time, either.

Conclusion

The Christian church must defend its authority to collect the
tithe from all of its voting members. It must defend its claim to
be the sole legitimate depository of the tithe. It must defend
itself as a separate covenantal authority, ordained by God and

14, ©. S. Lewis, That Hideous Strength (New York: Macmillan, 1946), p. 283.
18. North, Rapture Fever,
