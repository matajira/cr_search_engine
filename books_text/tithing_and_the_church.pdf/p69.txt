When Royal Priests Beg 55

outfit did not budget properly. It means that its director violat-
ed the warning of Luke 14:

For which of you, intending to build a tower, sitteth not down
first, and counteth the cost, whether he have sufficient to finish
it? Lest haply [it happen], after he hath laid the foundation, and
is not able to finish it, alt that behold it begin to mock him,
Saying, This man began to build, and was not able to finish
(Luke 14:28-30).

But isn’t it possible to make a mistake under pressure? Of
course. But this outfit sends out one of these letters all the time.
My parents were on the list, but they finally stopped giving: too
many appeals for emergency money. I have spoken with other
conservative Christian activists, and they have told me the same
thing about the organization. They are the recipients of an
endless stream of appeals, horror stories, and scare tactics.

Why? The person who heads the organization is neither in-
competent nor immoral. Yet the appeals are mostly hype, and
have been for several years. What went wrong?

There was a transfer of authority: from the visionary who
heads it to the fund-raising technicians who write the letters.
Again and again, this has been the fate of the large parachurch
ministries. The “pushers” take over, and once they do, the
ministry is doomed unless they are all fired. This seldom hap-
pens in time. Instead, the organization's principled, talented
people quit in disgust. The fund-letter writers win by default
until the outfit collapses. Then they take their skills to their
next victim, with its mailing list of about-to-become victims.

The ministry that hires professional fund-raisers has become
an addict. It does not take long to establish the addiction.

The Making of an Addict

T own a home study course by the man who pioneered these
letters in the 1970's. He has lots of samples of his work, and
