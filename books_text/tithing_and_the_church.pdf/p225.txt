Biblical Economics Today (6 times a year)
Christian Reconstruction (6 times a year)
Dispensationalism in Transition (12 times a year)
Biblical Chronology (12 times a year)

Biblical Economics Today is a four-page report that covers
economic theory from a specifically Christian point of view. It also
deals with questions of economic policy. Christian Reconstruction
is more action-oriented, but it also covers various aspects of Chris-
tian social theory. Dispensationalism in Transition deals with the
changes in theology and practice within modern dispensationalism.
Biblical Chronology surveys the discrepancies between contempo-
rary humanism’s dating of events in the biblical past and the Bible’s
account.

The purpose of the ICE is two relate biblical ethics to Christian
activities in the field of economics. To cite the title of Francis
Schaeffer’s book, “How should we then live?” How should we
apply biblical wisdom in the field of economics to our lives, culture,
civil government, and our businesses and callings?

Tf God calls men to responsible decision-making, then He must
have standards of righteousness that guide men in their decision-
making. It is the work of the ICE to discover, illuminate, explain,
and suggest applications of these guidelines in the field of econom-
ics. We publish the results of our findings in the newsletters.

The ICE sends out the newsletters free of charge. Anyone can sign
up for six months to receive them. This gives the reader the
opportunity of seeing “what we're up to.” At the end of six
months, he or she can renew for another six months.

Donors receive a one-year subscription. This reduces the extra
trouble associated with sending out renewal notices, and it also
means less trouble for the subscriber.

There are also donors who pledge to pay $15 a month. They are
members of the ICE’s “Reconstruction Committee,” They help to
provide a predictable stream of income which finances the day-to-
day operations of the ICE. Then the donations from others can
finance special projects, such as the publication of a new book.
