Authority and the Tithe 17

ability to heal rests on political coercion and bureaucratic con-
trol. The State is now reaching the limits of its ability to confis-
cate the wealth of nations, all over the world. If its ability to
exercise dominion by creating dependence by means of contin-
ual grants of money is ever interrupted by economic or other
social disruptions, there will be a temporary void in society.
That void will be filled by something. Authority flows to those who
exercise responsibility. Who will that be?

Who should it be? Christians. But Christians are ill-prepared
today to exercise such responsibility. They are themselves de-
pendents on the State. They, too, send their children to public
schools, collect Social Security checks, and plan their lives on
the assumption that the State will serve as an economic safety
net. The State’s wealth-redistribution system has steadily elimi-
nated competition from private charitable and educational
associations. When the State’s safety net breaks, as it surely will,
most Christians will find themselves as economically unpre-
pared as everyone else. They have been taught to trust that
which is inherently untrustworthy: the modern messianic State.
When this trust is finally betrayed, there will be weeping and
gnashing of teeth in churches, Christian college classrooms, and
other supposedly sanctified places.

In that day, there will be a shift in local and national leader-
ship, as surely as there was during the Great Depression of the
1930's. Regarding this coming shift in leadership, the question
today is: Who will inherit authority? The answer is: those who
bear the greatest economic responsibility in the reconstruction
of the economy.

Will this be the church? If not, why not? If not, then who?

Redemption: Definitive, Yet Progressive

The basis of biblical dominion in history is the redemption of
the world. To redeem something is to buy it back. This process
of long-term repurchase began at Calvary.

At Calvary, Jesus paid God the full redemption price. He did
