When Royal Priests Beg 49

of victory that calls forth great dedication.

5. Provide motivation for people to make more money by get-
ting more education and better jobs.

6. Show people ways to save ten percent of their income each
payday.

7. Preach on the moral obligation to get out of consumer debt.

&. Start paying off the church’s mortgage as fast as possible to
set a good example,

9, Start allocating a tithe from the church's budget to help the
poor.

This program is unacceptable to churches. It is based too
heavily on discipline, personal responsibility, thrift, and long-
term planning. This is not the beggar's way.

Hai in Hand vs. Checkbook in Hand

American evangelical churches have no power and little
influence because they are beggars. No one in a position of
authority pays a great deal of attention to organizations that
have so little discipline over their own members that they must
go outside the local membership to beg for money. The identi-
fring mark of failure in life is beggary (Ps. 97:25). The modern
evangelical pastor is like Oliver Twist, standing in front of Mr.
Bumble, empty bow] in hand: “Please, sir, may I have some
more?” You may remember Mr. Bumble’s reaction: outrage.

Let us compare a local church’s influence with that of the
Rockefeller Foundation. Who pays attention locally to the sug-
gestions of local churches? Hardly anyone. Who pays attention
locally to the suggestions of the Rockefeller Foundation? Lots of
dedicated people do, people who want only to serve the public
{at $75,000 a year plus expenses). They sit up and take notice.
The Rockefeller Foundation — actually, there are several Rock-
efeller Foundations — does not come to beg. It comes to write
large checks. This makes all the difference.
