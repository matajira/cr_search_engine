134 TITHING AND THE CHURCH

above the tithe to non-ecclesiastical organizations.

Prior to 1991, Chalcedon, like the Institute for Christian
Economics, was kingly rather than priestly in its calling.”! Nei-
ther organization is entitled to any portion of the tithe” ex-
cept at the discretion of churches that collect tithes and then
donate the money to either organization. (As the saying goes,
“Don’t hold your breath.”) The donor owes his local church his
tithe; he does not possess the authority to allocate his tithe
money (priestly, sacramental money) to other organizations.
Chalcedon, ICE, and all other parachurch and educational
ministries owe it to their supporters to warn them never to
send in donations unless they first tithe to a local church.”
This limitation would keep most of them quite tiny if they are
presently financed by men’s tithes, a practice which would then
cease. Rushdoony in the late 1970’s invented a theology of the
tithe that justified Chalcedon’s collection of part or all of Chris-
tians’ tithes. This self-interested theological confusion under-
mined his theology of the kingship of Christ and the dominion
covenant.

Conclusion

The legal basis of the church’s monopolistic right to collect
the tithe from its members is the sacramental function of the
church. The tithe and the sacraments are linked judicially. The
issue is not economic service by the church, or any other orga-
nization; the issue is the sacramental basis of the tithe.

Rushdoony’s theology of the tithe rests on an economic

21. The ICE is legally chartered as a charitable trust, not a foundation.

22, Here I speak of non-members, now that Chalcedon has been designated by
Rushdoony as a church. But Chalcedon Church, if it in fact is a lawful church, is not
entitled biblically to the tithe money of non-members. It, too, must rely on non-
members’ gifts above the tithe.

28, On this point, see my response to John R. Muether in Gary North, Westmins-
ter’s Confession: The Abandonment of Van Til’s Legacy (Tyler, Texas: Institute for Chris-
tian Economics, 1991), pp. 289-92.
