118 TITHING AND THE CHURCH

Rushdoony wrote eloquently and to the point that “the marks
of a true church, ie. a body of worshippers, have been defined
for centuries as the faithful preaching of the word of God, the
faithful administration of the sacraments, and the application of
Biblical discipline. Without these things, we are not talking
about the church in any historical or theological sense. Instead,
a purely humanistic ideal of a denatured church is given us.
Such a church is simply a part of the City of Man and an out-
law institution at war with the City of God.”

I agree completely with his excellent summary of the marks
of a true church and the humanistic implications of any denial
of it. The problem is, nineteen years after he wrote it, eight
years after he published it, Rushdoony openly repudiated it,
and more than repudiated it: became contemptuous of it, ridi-
culing it. The transformation of his theology during the 1980’s
was extensive — a fact not widely perceived by his followers or
his critics. He replaced his original commitment to the theology

citation is probably close to the time he wrote the essay. Prior to his move to Valle-
cito, California, in 1975, he threw out his lifetime collection of newspaper clippings.
(What I would have paid for this collection had I known in advance he intended to
trash itl) The chapter cites a local Southern California newspaper The San Gabriel
Fribune: June 26, 1972. He had many disciples in the San Gabriel Valley in this
period. One of the attendees of his evening lectures in Pasadena (in the San Gabriel
Valley), held in the late 1960's, probably sent him the newspaper clipping. There is
no footnote reference in the book to anything published later than 1973. So, I think
it is safe to conclude that the chapter was written no later than the publication date
of Volume 1 of The Institutes: 1973. That he could write these chapters in the early
1970's, several apparently in late 1972 and early 1978, while he was completing the
manuscript of The Institutes, indicates his continuing productivity in 1970-78 period.

Compare the tightly written chapters in Volume 1 with those in Volume 2, Law
and Society (1982), whose newspaper citations cluster noticeably around 1976-77.
These post-1973 chapters are shorter, relying heavily on footnote references to Bible
commentaries and religious encyclopedias, with few references to scholarly journals
and scholarly monographs: a visible contrast with the footnotes in his pre-1974
books. The theological structure and integrating theme of Law and Society are difficult,
to discern, unlike Volume 1, With 160 brief chapters plus appendixes, it could hardly
be otherwise.

46. R. J. Rushdoony, Salvation and Godly Rule (Vallecito, California: Ross House,
1983), p. 160.
