Rushdoony’s Ecclesiology 113

This has not been an oversight on his part. He has not publicly
faced these two crucial issues: a judicially binding ecclesiastical
hierarchy and the uniquely sacramental nature of the church.
This is why he prefers to obfuscate the issue by creating a per-
ipheral dichotomy, as we shall see: church as kingdom vs.
church as a building. The real issue is this: the church as an oath-
bound, covenantal, hierarchical institution with the power to excommu-
nicate those who rebel against church authority. His words show no
trace of any such understanding of the doctrine of the institu-
tional church. “Very clearly, the church in Scripture means the
Kingdom of God, not merely the worshipping institution or
building. . . . It includes godly men and their possessions, and
the earth they subdue in the name of the Lord.”*! He then
launches into a chapter titled, “Church Imperialism.” It is a
long attack on bishops and church hierarchy, which he insists
are pagan in origin: “ecclesiastical totalitarianism.”**
Familism

In Chapter 75, “Kingdom Courts,” he returns to his funda-
mental social theme: familism. He has. already equated the
church with the kingdom of God. “In the Kingdom of God, the
family is in history the basic institution.”® The unique, central
social institution is not the institutional church, he insists; rath-
er, it is the family. The family possesses an authoritative court,
he insists — indeed, the authoritative court in history. In con-
trast, Rushdoony rarely discusses in Law and Society the exis-
tence of authoritative church courts except in the context of
family courts, which possess superior authority, he says, since
the pattern of all government is based on the family. Jethro’s

hierarchical appeals court in Exodus 18 “utilized an already
existing family office, the eldership. The elders are mentioned

81, Jbid,, p. 387.
82. Ibid, p. 341.
88, Iid., p. 343.
