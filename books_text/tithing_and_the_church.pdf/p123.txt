Rushdoony’s Ecclesiology 109

The Sacraments

Rushdoony’s view of the local church affected his doctrine of
the sacraments. He neglects - and his exposition necessarily
denies - the sacramental basis of the local church’s authority to
collect the tithe. “As against an empty rite, Christian fellowship
in Christ's calling, around a table, is closer to the meaning of
the sacrament.” But if the judicial rite of the Lord’s Supper
is not backed up (sanctioned) by the promise of eternal sanc-
tions, both positive and negative, then it is truly an empty rite:
judicially empty - the nominalist-fundamentalist-memorialist
view of the sacraments: Anabaptism.”

Rushdoony’s post-1973 published view of the church is non-
covenantal: the church as a fellowship without judicial sanctions
rather than an institution possessing the judicial keys of the
kingdom. He has even insisted that a church has no lawful
authority to discipline those members who refuse to attend its
worship services: “We are urged not to forsake ‘the assembling
of ourselves together, as the manner of some is’ (Heb. 10:25),
but the church is oi given authority to punish those who
do.”* Then who is? Only God, apparently. There is suppos-
edly no appeal beyond the individual’s conscience: the “divine
right” of a non-attending church member. Then what judicial
authority does the institutional church possess? In Rushdoony’s
view, none. What meaning does church membership have? Less
than membership in a local social club, which at least requires
the payment of dues for membership. In Rushdoony’s theology,
a local flower arrangement society possesses more authority
over its members than a local church possesses over its mem-
bers.

Rushdoony’s view of church discipline represents a funda-
mental break from the history of the church, including the

22. Rushdoony, Law and Society, p, 129.
28. On this question, Zwingli was an Anabaptist.
24. Rushdoony “The Nature of the Church,” Calvinism Tiday, (Oct. 1991), p. 8.
