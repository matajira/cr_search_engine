Rushdoony’s Ecclesiology 115

zation of either State or family. The fact is, secramentalization is
an inescapable concept. It is always a question of which institution
becomes elevated to sacramental status. Unfortunately, Rush-
doony has not understood that sacramentalization is an inescap-
able concept. He seeks to de-sacramentalize the institutional
church. He does not see the Lord’s Supper as an ecclesiastical
matter, but rather fundamentally a family matter: “The central
sacrament of the Christian faith is a family fact, a common
sharing of bread and wine from the Lord’s Table.”**

Which institution becomes the prime candidate for sacra-
mentalization in place of the church? In Rushdoony’s theology,
there is no possibility of the sacramentalization of the State, but
why not the family? Rushdoony has moved dangerously close to
this conclusion. In between his assertion of the family as the
kingdom of God in miniature ‘and his discussion of the office of
elder as “first of all a family office,”® this disconcerting state-
ment appears: “Our regeneration establishes a union with the
Lord. Our every sexual act is an essential step which makes us
a member of the other person.”””

Rushdoony needed to qualify his language covenantally. It is
legitimate to describe Christ’s love for His church as the love of
a husband for his wife, as Paul does in Ephesians 5:23-33, but
not when you begin with a theory of the church as an extension of the
family, Also, not when you personally refuse to take the sacra-
ment of the Lord’s Supper, for this refusal raises the issue of a
substitute sacrament. Biblically, there is no form of covenant
renewal for the family except through membership in the insti-
tutional church and participation in the Lord’s Supper. But if
the uniquely sacramental character of the institutional church
is denied, then what is to prevent the substitution of sexual

38. R. J. Bushdoony “Fhe Life of the Church: I Timothy 5:1-2,” Chalcedon
Report (Jan. 1992), p. 15.

39. Law and Society, p. 389.

40. idem.
