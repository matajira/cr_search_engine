172 TITHING AND THE CHURCH

the history of ecclesiology to deny that these offices are not
endowed by God with binding covenantal authority (Matt.
16:18-19). Speaking of the church as a body (but not as the
bride of Jesus Christ: Ephesians 5), Rushdoony writes:

Now the members of a body (iz., hands, feet, ete.) do not hold
offices; they have functions. The words translated as office in the
New Testament make this clear. For Romans 11:13, 1 Timothy
3:10 and 3:13, the word used is dickonia in Romans and diakoneo
in Timothy. The word, in English as deacon, means a servant,
service; it refers to a function. In Romans 12:4, office in the
Greek is praxis, function. .. .®

So, to be a member means that one cannot be an officer:
function is sharply distinguished from office. Then what is a
minister? Is the civil magistrate in Romans 13:4 not a minister,
as Paul calls him? Yet a civil minister surely has a judicial func-
tion: to suppress public evil. Doesn’t a civil bureaucracy have a
judicial function? This is the very essence of bureaucracy: it is
limited by law to a specific function. Yet civil bureaucrats are
oath-bound agents of the State: ministers of God who possess
judicial authority. What can be said of the office of civil minister
can be equally said of the office of ecclesiastical minister: he is
also a covenantally oath-bound agent — of the church.

The New Testament uses other analogies for the church
besides body: household (Gal. 6:10; Eph. 2:19), temple (Eph.
2:21), bride (Rev. 21:2), etc. Rushdoony has used Paul’s analo-
gy of the church as a body as a convenient smoke screen to
‘disguise the judicial aspect of the New Testament offices of
deacon and elder. He makes a grammatically and theologically
unwarranted distinction between function and office. Then he
continues:

3, Rushdoony, Roots of Reconstruction, p. 405.
