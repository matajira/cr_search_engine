142 TITHING AND THE CHURCH

The Fatal Flaw in Rushdoony’s Theology

Rushdoony began to develop the rudiments of his theology
of the tithe in the late 1960's, after Chalcedon had received its
tax-exempt status from the U.S. Internal Revenue Service. In
The Institutes of Biblical Law (1973), he writes: “Moreover, the
modern church calls for tithing to the church, an erroneous
view which cuts off education, health, welfare, and much else
from the tithe.”® He understands that his view of the tithe
transfers power to the members, who are supposedly under no
judicial requirement to pay their tithes to the church: “If the
church collects the tax, the church rules society; if the state
collects the tax, the state rules society. If, however, the people
of God administer the tithe to godly agencies, then God’s rule
prevails in that social order.”’ The central legal issue is adminis-
tration: Who has the God-given authority to distribute the tithe?
The Bible is clear: the church. Rushdoony is equally clear: the
tithe-payer.

Notice Rushdoony’s implicit assumption: because God says
that He is entitled to a tithe, a godly society is determined
economically by the agent who distributes it. The biblical fact is
very different: the judicial status of a godly society is deter-
mined covenantally in terms of which agency collects and then
distributes the tithe, for this identifies which god rules in society
by which representatives. A Christian society is identified bibli-
cally by the widespread presence of churches that collect the
tithe, ie., churches that possess and exercise their God-given
authority to impose negative sanctions against members who
refuse to pay the tithe. God blesses covenantally faithful societ-
ies, and tithing to God's church is a primary mark of covenan-
tal faithfulness. Cause and effect move from law (boundaries) to
sanctions (blessings and cursings). But the judicial issue is God’s
delegated authority: Who owes what to whom? In short, who

6. Hid., p. 513.
7. Bid., p. 514,
