178 TITHING AND THE CHURCH

Conclusion

Rushdoony’s contempt for the authority of the church is
aimed at ordained officers of the church. He authoritatively
warns them that his verbal wrath is representative of God’s
wrath to come: “Foolish churchmen have often seen themselves
as the truth (and also as the wrath) of God. This is idolatry, and
God will judge such men. Not the church, nor men, but Jesus
Christ is the truth of God, and He alone is our Redeemer.”"
But truth must always be spoken representatively in history.
Who, then, has spoken this truth regarding the idolatry of
foolish churchmen? R. J. Rushdoony. And what was his office at
the time that he issued his warning? What was the legal basis of
this authority? In 1986, he was the president of a government-
chartered foundation who defended his foundation's right to
accept all or a portion of men’s tithes, a man who had not
taken the Lord’s Supper regularly in over two decades.

To conclude, I can do no better than to cite Rushdoony’s
two rhetorical questions:

All over the country, I fmd men. retreating into Phariseeism
rather than advancing into dominion, and their excuse is a false
holiness. No church is good enough for them; granted, the
church scene is a sad picture, but will withdrawal improve it?
Moreover, are we so holy that we cannot afford to associate with
other sinners saved by grace?!®

Ten years later, in 1991, he at long last joined a local church
that he could trust: his very own. He bought a set of clerical
robes and began administering monthly communion in his legal
capacity as... what? A church officer? A functionary? A father?
He has not said. He needs to say. Publicly. Soon (Ps. 90:10).

18. Rushdoony “The Possessor of Truth,” Chaleedon Position Paper No. 81
(December 1986); ibid., p. 351.

16. Rushdoony, “Sovereignty,” Chalcedon Paper No. 19 (Jan. 1981); in Roots of
Reconstruction, p. 87.
