52 TITHING AND THE CHURCH

longer keeps advertisers and administrators employed.

If the charity or ministry actually finances the purchase of
tools and training for poor families, and if it preaches a view of
time and law that instills faith in hard, smart work, meaning
faith in future earthly blessings for those who work hard and
smart, then there will be fewer little M’Gumbos in the future.®
But until the potential donor gets a detailed report on the
content of the preaching and the nature of capital delivered to
the starving waifs and their families, he should keep a tight
hold on his checkbook. He must guard against becoming the
equivalent of the tourist, handing out his alms from a tour bus
window. (The tourist at least enjoys the tour.) The guilt-ridden
donor winds up paying for more fund-raising campaigns and
permanent employment for administrators. The Postal Service
will get a larger percentage of his donation than little M’Gumbo
and his starving friends.

The donor needs to know: Is this fund-raising campaign
designed to heal his conscience temporarily or permanently
heal a demonized social order? If the program is not designed
to elevate families out of poverty through increased produc-
tivity, and ultimately to elevate the whole mission field out of
poverty through increased productivity, then the donor should
seek out a different charity. The closer the program comes to
indiscriminate alms-giving, as distinguished from culture trans-
formation, the more it is like a subsidy program for beggars.
There will always be beggars in pagan societies. There will
always be a sense of futility for most people concerned: donors,
administrators, and recipients. The only long-term beneficiaries
will be those who write the fund-raising letters.

The main victims are the churches that get short-changed
and the donors who think of themselves as soup kitchen operators.

5. David Chilton, Productive Christians in an Age of Guilt-Manipulators: A Biblical
Response to Ronald J. Sider (Sth ed.; Tyler, Texas: Institute for Christian Economics,
1990), ch. 16,
