Sacraments or Social Gospel? 145

taxes, can demand obedience, and therefore it possesses divine
rights. That is, the humanist State claims autonomy from (and
therefore authority over) every rival institution. To challenge
such a view of the State, there has to be an appeal to another
authority with authority that is equal to the State’s in many
areas and superiority to it regarding the collection of funds
from its members. In short, the authority of the church to
collect tithes from its voting members prior to the tax collector's
extraction of money from church members must be affirmed in
civil law. The church must have legal priority over the State’s
authority in the involuntary collection of money.® Only if some
other covenantal institution possesses comparable authority
over its members’ money can we identify an agency with com-
parable covenantal authority.

Rushdoony’s theology of the tithe denies such authority to
the church. This leaves only the family as a rival covenantal
institution. But, biblically speaking, the family possesses neither
the sword nor the keys of the kingdom. This is the fatal flaw of
Rushdoony’s social theory. Rushdoony’s anti-ecclesiastical theology
can offer only two futile alternatives to the divine right of the
State: radical individualism or patriarchalism-clannism. The
State historically has overcome both of these alternatives, from
ancient Greece to the present.

Conclusion.

By rejecting a sacramental defense of both the church and
the tithe, Rushdoony has converted his theology into a conser-
vative version of the social gospel. The legitimacy of the church,
manifested in Rushdoony’s ecclesiology only by its ability to
persuade church members to donate money to it, is grounded

9. This is acknowledged implicitly judicially in the U.S. tax code. The taxpayer
is allowed to deduct tithes aud offerings from his gross income before estimating
what he owes to the State. He pays income taxes only on the money that remains
after charitable giving. This is not true in most European countries, where the State
has primary claim on income. The church may receive a portion of what remains.
