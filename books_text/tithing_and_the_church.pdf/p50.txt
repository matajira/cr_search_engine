36 TITHING AND THE CHURCH

circumcised believer under the Mosaic Covenant to become an
officer of the ecclesiastical assembly. Only Levites could become
ecclesiastical rulers. There was no threat from immigration. But
in the New Covenant, access to ecclesiastical office is not re-
stricted by birth. Access to church offices is by membership in
the congregation followed by some sort of screening process:
voting, formal education, or the laying on of hands by existing
church officers. This means that the immigrant can become a
church officer in a relatively brief period of time. The inherent
democracy of the New Covenant ecclesiastical order has re-
placed the judicial boundaries of the Mosaic Covenant. In most
of the evangelical churches, those who are allowed access to the
Lord’s Table are also allowed to vote. The Protestant concept of
“every redeemed man a priest” has had significant consequenc-
es for church government.

This is one unstated but very important reason why virtually
ali church traditions that defend closed communion draw some
sort of judicial distinction between baptized members and full
communing members. Those churches that do not make such
a distinction among adults do make it with respect to children.
Churches that allow children to vote in church elections are
rare, but they are all marked by an unwillingness to allow in-
fant or young child communion. The idea of a three-year-old
who possesses voting rights is too much to swallow.

Today there are first-class members and second-class mem-
bers in every closed-communion congregation. The judicial
dividing line is access to the Lord’s Table. Not every baptized
member can claim access to Holy Communion. To draw an
analogy, it is as if not every circumcised Jew could claim access
to Passover. But every circumcised person could attend Passover
in Israel, and not only Jews. So, the judicial parallels between
the sacraments of the Mosaic Covenant and the New Testament
are not honored by modern Christianity, Because the priest-
hood is open to all baptized males who meet certain standards
~ standards that are no longer based on tribal membership —
