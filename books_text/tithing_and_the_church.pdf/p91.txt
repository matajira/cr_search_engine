Tithes, Taxes, and the Kingdom viel

and the derivative sovereignty of the institutional church, it will
remain on the defensive. This defensive stance will continue to
be revealed by the local church’s hesitancy to affirm. publicly its
monopolistic authority to collect ten percent of its members’ net
income. Any suggestion that the institutional church has not
been authorized by God to collect the entire tithe from Chris-
tians is an implicit surrender to humanism and the humanist
State.
This leads us to Part 2 of this book.
