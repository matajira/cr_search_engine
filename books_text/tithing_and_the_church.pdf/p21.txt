Sovereignty and the Tithe 7

barely distinguished theologically from a non-profit social club.
It is not perceived as sovereign.

There is very little sense of the judicial presence of God any-
where in modern church liturgy. Men may sing, “All bail the
power of Jesus’ name; let angels prostrate fall,” but neither
angels nor the power of Jesus’ name are taken seriously. In
liberal churches, such realities are seen, at best, as non-histori-
cal (Barthianism); at worst, as mythical (Bultmanism).

The institutional church manifests God’s moral and judicial
standard for the world,? just as Israel manifested His standard
under the Mosaic covenant. This, too, is not believed by the
modern church. We find that there is no sense of the judicial
presence of God in the civil courtroom, the voting booth, and
on inauguration day. The following phrases are mere formali-
ties: “So help me, God” (courtroom oath), “In God we trust”
(slogan on U.S, money), and “God bless you all” (tagged onto
the end of televised speeches by American Presidents). Invoking
God's name has become a mere convention.

The Judicial Marks of Sovereignty: Oath and Sanctions

The presence of a self-maledictory oath is the judicial mark
of covenantal sovereignty. Only three institutions lawfully can
require such an oath: church, State, and family? Such an oath
implicitly or explicitly calls down God’s negative sanctions on
the person who breaks the conditions of the oath. These sanc-
tions are historical, although few Christians believe this, despite
Paul’s warning regarding the misuse of the church covenant’s
oath-renewal ceremony: the Lord’s Supper.

Wherefore whosoever shall eat this bread, and drink this cup

2. Gary North, Healer of the Nations: Biblical Blueprints for International Relations
(Ft. Worth, Texas: Dominion Press, 1987), Introduction,

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed.; Tyler,
‘Texas; Institute for Christian Economics, 1992), ch. 4.
