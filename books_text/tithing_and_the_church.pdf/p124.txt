110 TITHING AND THE CHURCH

theology of the Protestant reformers and especially Calvin.
Rushdoony insists (without any citations from the Bible) that a
Christian has the God-given authority to remove himself indefi-
nitely from a local congregation and cease taking the Lord’s
Supper, but without ecclesiastical judicial consequences. This
necessarily implies that self-excommunication, which is a form
of excommunication, is not an actionable offense within the
church. This is a denial of Holy Communion, for it is a denial
of excommunication.

From Calvinism to Autonomy

Calvin was clear about the keys of the kingdom in history.
He cited Matthew 16:19: “And I will give unto thee the keys of
the kingdom of heaven: and whatsoever thou shalt bind on
earth shall be bound in heaven: and whatsoever thou shalt
loose on earth shall be loosed in heaven.” He then commented
that “the latter applies to the discipline of excommunication
which is entrusted to the church. But the church binds him
whom it excommunicates — not that it casts him into everlasting
ruin and despair, but because it condemns his life and morals,
and already warns him of his condemnation unless he should
repent... . Therefore, that no one may stubbornly despise the
judgment of the church, or think it immaterial that he has been
condemned by the vote of the believers, the Lord testifies that
such judgment by believers is nothing but the proclamation of
his own sentence, and that whatever they have done on earth
is ratified in heaven.”® This is why the sacrament is a monop-
oly, the church is sacramental, and the tithe is owed to the
church. Rushdoony denies all three conclusions.

Rushdoony had ceased being a Calvinist by the late 1970's.
He became a predestinarian Congregationalist without a local
congregation (until he announced his own in 1991), a man who

25. John Calvin, Institutes of the Christian Religion (1559), 1V:X1:2. Edited by Ford
Lewis Battles, 2 vols, (Philadelphia: Westminster Press, 1960), IL, p. 1214,
