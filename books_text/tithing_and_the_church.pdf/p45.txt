Church Membership and the Tithe 31

were not supposed to mistreat strangers in their land.

Why would strangers come to Israel? For many reasons:
trade, better working conditions, greater judicial protection,
greater safety from marauders, and ail the positive benefits
promised by God to Israel in Deuteronomy 28:1-14. Why not
take advantage of better éxternal conditions? So confident was
God in His own covenantal promises of blessing that His law
established guidelines for dealing with the strangers He knew
would come to Israel in search of a better life. His blessings
were not limited to internal feelings experienced only by cove-
nant-keepers. External blessings were available to anyone living
in His covenanted land during those periods in which His
people remained faithful to Him and to His law.

The Mosaic law established two forms of church membership
for circumcised people: communing membership (lawful access
to the Passover) and priestly membership (the Levites). All
circumcised males and their families could come to the Passover
(Ex. 12:48). They would hear the law in the various teaching
services, including the seventh-year service in which the whole
of the law was read to every resident in Israel at the feast of
tabernacles (Deut. 31:9-12).

The administration of the sacrament of Passover and the
other feasts and sacrifices was a monopoly office of one tribe,
the Levites. Other Israelites and even circumcised resident
aliens could be adopted into this tribe at the discretion of a
Levite family and by the payment of a substantial entry fee
(Lev, 27:2-8).> They could become priests in this way. But this
was not an easy thing to accomplish. In most cases, only those
who had been born into the family of Levi ever served as offi-
cers of the assembly. A tribal boundary was the crucial judicial
boundary that protected the Mosaic priesthood.

3. North, Boundaries and Dominion, ch. 36.
