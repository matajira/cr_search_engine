130 TITHING AND THE CHURCH

ren’s children: and the wealth of the sinner is laid up for the
just” (Prov. 13:22).

Sovereignty

If you want to find out where sovereignty lies in any social
system or social theory, you must do two things: (1) identify the
sacraments; (2) follow the money.” In Rushdoony’s theology,
the kingdom of God is based on a compact between God.and
the individual Christian. The institutional church is without
covenantal authority in this God-and-man compact. Church
officers must take whatever they receive from church members
and be thankful to the donors for whatever this is. Rushdoony’s
ecclesiology allows church officers no legitimate institutional
sanctions to impose on those members who send all or a por-
tion of their tithe money elsewhere.

The judicial question surrounding the tithe is this: Who
lawfully retains sovereign control over the allocation of the tithe?
Rushdoony’s answer: the individual Christian, not the officers
of the church. “The Christian who tithes, and sees that his tithe
goes to godly causes, is engaged in true social reconstruction.
By his tithe money and his activity he makes possible the devel-
opment of Christian churches, schools, colleges, welfare agen-
cies, and other necessary. social functions.” (And, he might
have added, non-profit educational foundations, but this would
have appeared self-serving.) He does not mean that Christians
retain ultimate control over the allocation of their tithes by
choosing which local congregation to join; rather, they retain
immediate allocational authority in their capacity as church

ex. The capital of most estates in the U.S. is used up in the last six months of an aged
person's life. It is better to die in bed at home six months early and leave capital
behind. Christians must buy back the world, generation by generation. This requires
a growing supply of capital.

15. North, Political Poltheism, p. BB3.

16. Rushdoony, “Foundation of Christian Reconstruction,” Tithing and Dominion,
pp. 8-9.
