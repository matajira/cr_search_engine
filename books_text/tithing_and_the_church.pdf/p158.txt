144 TITHING AND THE CHURCH

1987: “I’m close to being a libertarian. . . .”® As he sees it, the
success or failure of God’s non-profit kingdom institutions will
be determined by God’s sovereignty by means of the decisions
of individual Christians regarding where to pay their tithes —
decisions made without any legitimate threat of institutional
sanctions from the recipients. Sanctions — positive or negative -—
are imposed by individual Christians on the recipient institu-
tions; the institutions have no legitimate negative sanctions of
their own. The institutional church is described by Rushdoony
as being little more than an income-seeking business that com-
petes for the consumers’ money. This view of church financing
removes the power of the keys from the church. This conclu-
sion is completely consistent with Rushdoony’s pre-1991 view of
the sacrament of the Lord’s Supper: a rite without covenantal
sanctions.

Rushdoony’s libertarianism and individualism are both visi-
ble in his view of the tithe. On this topic, Rushdoony is an
economic determinist. He says, in effect: “He who controls the
allocation of the tithe controls Christian society. The individual
Christian lawfully controls the allocation of the tithe, so he
should control Christian society. The institutional church has
no lawful authority to compel such payment by any threat of
sanctions. Hence, the individual is judicially autonomous in the
allocation of the tithe. Only God can impose negative sanctions
against him.” This is the libertarian theology known as the divine
right of the individual. Divine-right theology always rests on a
presupposition that someone — the king, the legislature, or the
individual — is beyond legitimate institutional sanctions in histo-
ry. Rushdoony’s radical individualism is clearly seen here. He
has rejected covenant theology in favor of Anabaptist theology.

Rushdoony has written repeatedly that individualism always
leads to statism. The humanist State can compel payment of

8. Bill Moyers, “God and Politics: On Earth as It Is in Heaven,” Public Affairs
Television (1987), p. 5.
