Conclusion to Part 2 171

explicitly grounded in familism rather than the church. He has
revived patriarchalism, which has been at war with the church
from the beginning. (Ancient Rome was grounded on patriar-
chal social theory, and it was at war with Christianity.) With
Volume 2 of The Institutes of Biblical Law he has undermined
Volume 1.

That this should be the experience of a man with Rush-
doony’s intellect, insight, and vast bibliographical knowledge
should warn us all: contempt for God’s institutional church is
theologically fatal. It surrenders your legacy to others. God’s
church is not now, nor has it ever been, a mummy factory. The
institutional church, for all her flaws, is God’s bride. God has
no other.

Denying the Church’s Covenantal Status

What if someone were to come to you and argue that the
State is a voluntary contractual association whose magistrates
possess merely functional authority, possessing no authority to
compel its members to obey? You would probably call him an
anarchist, or at least a libertarian. What if he were to argue that
the family is a voluntary contractual association whose founding
pair are not oath-bound officers in a covenantal unit, and who
therefore possess no authority to enforce any standards on their
minor children? You would call him a liberal humanist. Yet this
is what Rushdoony now says of the institutional church. His
ecclesiology would strip the church of all covenantal authority.

It took two decades for Rushdoony to become consistent
with his anti-church theology, but in 1988, he finally made the
break with orthodox Trinitarian ecclesiology. In Chalcedon
Position Paper No. 97, “The Church as Function,” Rushdoony
asserted that the church as an institution is not governed by
oath-bound, .covenantally established, God-ordained officers.
Instead, its leaders are merely functional rulers. While it is
conventional for theologians to discuss the offices of the church
in terms of their varying functions, it is a complete break with
