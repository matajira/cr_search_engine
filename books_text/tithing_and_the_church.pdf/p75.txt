Tithes, Taxes, and the Kingdom 61

church. The church is society's central institution. Primary
economic sovereignty belongs to the individual, but this sover-
eignty is normally manifested through the family.? The State
comes in third in the sovereignty race. This outrages those who
proclaim the power religion: the State as healer.

The debate in Christian circles today seems to be between
those who defend the primary sovereignty of the State and
those who defend the primary sovereignty of the family. The
problem is, we are talking about different kinds of sovereignty.
Primary judicial sovereignty belongs to the church; primary
economic sovereignty belongs to the individual and the family;
the State is to protect the sovereignty of the other two.

What I propose is a restructuring of the debate: a debate
over the rights of inheritance. The State has made major in-
roads in this area. It wants the inheritance of both the family
and the church. Neither the church nor the State creates
wealth; both must be supported by those who do create wealth.
The question is: Which of these two institutions will best protect
the sovereign rights of those who create wealth? Which is the
greater threat?

Church or State

Today, far too many Christian leaders are asking themselves:
Which institution possesses greater sovereignty in history,
church or State? Are both equally sovereign? The Bible is clear
regarding economic sovereignty: a State that taxes at a rate
equal to the tithe is tyrannical (I Sam. 8:15, 17). Thus, the
church possesses greater economic sovereignty.

But what about legal sovereignty? Here, again, economics
comes into the picture. Legal sovereignty is visibly manifested
by an institution's authority to impose taxation on others and
escape taxation by others. Can the State legally tax the church?

2. Gary North, The Sinai Strategy: Econoraics and the Tin. Commandments (Tyler,
‘Texas: Institute for Christian Economics, 1986), ch. 5: “Familistic Capital.”
