The Legal Basis of the Tithe 127

with millions of Christians in rebellion against church authority,
Rushdoony’s doctrine of church and tithe finds supporters.

Church and Kingdom

Rushdoony argues that the individual has the God-given
authority to decide where his tithe money should go. As a
statement of the God-delegated authority of the believer, this is
true, but only in a very specific and limited way: his authority
to transfer his membership to another congregation. But Rush-
doony is not talking about this form of conscience-based au-
thority before God. The decision Rushdoony speaks of is a
decision made not on the basis of where the Christian chooses
to have his local church membership, but rather on the basis of
the Christian's assessment of the broadly defined cultural per-
formance of the church’s officers. “The priests and Levites, to
whom it [the tithe] was originally given, had charge of religion,
education, and various other functions.”* The tithe, he says,
must constitute the financing of every aspect of Christian recon-
struction, not just the preaching of the word and the adminis-
tration of the sacraments: “But the law of the tithe makes clear
it is God’s money and must go to God’s causes, to Christian
worship, education, outreach, and reconstruction. ... And the
tithe must bear the whole burden of Christian reconstruction.”*
(This is clearly incorrect: the tithe is only one-tenth of one’s net
increase. Everything a person has is supposed to be devoted to
Christian reconstruction: heart, mind, soul, and capital.) In
short, “What we must do is, first, to tithe, and, second, to allocate
our tithe to godly agencies. Godly agencies means far more
than the church.”” The Levites provided education, music, and
so forth. “The realm of the godly, of the Christian, is broader
than the church. To limit Christ’s realm to the church is not

5. idem,
6. Bid., p. 8.
7. Rushdoony, “The Foundation of Christian Reconstruction,” ibid., p. 9.
