200 TITHING AND THE CHURCH

progress, 8

property, 39-40
Protestantism, 110, 128
psalms, 69-70

Puritans, 16

Rapture, 48, 75
responsibility, 17
thetoric, 117-21, 168
Rockefeller Foundation, 49
Roman Catholics, 28
Rushdoony, Rousas John
accusations, 84-85
allocation, 142-43
Anabaptism, 138
Anabaptist, 119
apostate churches, 126
Baptist, 111
bureaucracy, 173
Calvin vs., 108, 110, 124
challenge, 147
charity, 128
church & coercion, 173-74
church & family, 92
church defined, 111-13,
117-19, 175-76, 183
church = kingdom, 112-13,
114
church councils, 174
church membership, 149
church membership of, 85
church officers, 171-73
church sanctions, 109
churchless Calvinism, 169-
70
compact theory, 105n, 130
Constitution, 105n
Council of Constantinople,

174-75

covenants, 140

divorce, 146-47

divorce (Thoburn), 156-60

doctrine of church, 100-1

economic interpretation,

183

education, 139

elder defined, 114, 115

excommunication, 105n,
109-10, 140

familism, 92, 94, 113-16

fertility cult religion, 116

flower arrangement society,
109

fundamentalist, 123

heresy, 161

hierarchy=heresy, 111

holiness (social), 131

humanism, 118

individualism, 104, 132

Institutes of Biblical Law, 84,
118n, 168

keys of kingdom, 94

legacy, 122

Leyvites, 125, 127-28, 136-
37

liberalism, 121

liberalism of, 125-26

libertarianism, 106, 110,
143-45, 146

Jocal church, 155

Lord’s Supper, 107-8, 115

Melchizedek &, 93

mummy factory, 117, 119

newspapers, 118n

nominalism, 140

oath, 105n
