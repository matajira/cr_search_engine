Rushdoony’s Ecclesiology 105

of his ecclesiology.

The existence of a mandatory payment to the church is
evidence of a covenantal relationship: a legal bond established by
a. self-maledictory oath"! which each church member takes either
explicitly or representatively (by parents). The church has a
lawful claim on a tithe of every member’s net increase in in-
come.” Unlike the State, which is ruthless in collecting taxes
owed to it, the modern church rarely enforces its lawful claim.
This is not surprising: the modern church rarely enforces any-
thing under its lawful jurisdiction.’ The State has arrogated
power to itself in the face of the churches’ defection. In our
day, most Christians regard this as normal and even normative.

IL. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed. Tyler,
‘Texas: Institute for Christian Economics, 1992), pp. 88-91. Rushdoony refuses to
discuss the self-maledictory oath as the judicial basis of all four biblical covenants:
personal, church, State, and family, He defines the covenant as God-given law rather
than as oath-invoked God-given law. This unique judicial oath formally invokes God's
sanctions. Without this formal invocation, there is no redeeming covenant bond
possible. There is only the general, Adamic covenant bond: a broken covenant.
Rushdoony’s definition does not acknowledge this fact. He writes: “In the Biblical
record, covenants are laws given by God to man as an act of grace.” Rushdoony,
“Covenant vs. Contract,” Chalcedon Report. (June 1993), p. 20. If correct, this defini-
tion would make the covenants universal, since biblical laws govern everything in
history, as he has long argued. But if he were to discuss the sanctions-invoking oath
as basis of the four covenants, he would have to discuss oath-breaking in the church
and its formal sanctions: the doctrine of excommunication. He would also have to
discuss in detail Article VE, Section III of the U.S. Constitution, which prohibits
religious test oaths for Federal (national) office. This is why the U.S. Constitution is
an atheistic, humanistic document a fact which Rushdoony has refused to accept for
over three decades. See Gary North, Political Polytheism: The Myth of Pluralism (Tyler,
‘Texas: Institute for Christian Economics, 1989), Appendix B.

12. This obligation does not apply to gifts from husbands to wives and vice versa;
nor does it apply to intra-family gifts to minors. Parents who feed their children need
not set aside a tithe on the food so consumed. The obligation is covenantal, and the
institutional payment of the tithe by the head of the household serves as a represen-
tative payment for all of its members.

13, At worst, a pastor who is convicted of adultery is suspended for a year or
two. I know of at least one case where an admitted adulterer was asked by his
presbytery only to transfer to another presbytery. The members’ idea of negative
sanctions was limited to “Not with our wives, you don’t!” He voluntarily left the
ministry. I bought part of his library.
