10 Honest Mong

The Function of Money

What has money got to do with all this? Absolutely nothing.
Crusoe doesn’t use money. He simply makes mental estimations
of the value of anything in terms of what he thinks it will produce
for him in the future. If whatever an item will produce isn’t worth
very much to him in the future, it won’t be worth very much today.

He, doesn’t ask himself, “I wonder how much money all this
cost before it was loaded onto the ship?” Unless he expects to be
rescued shortly, thereby enabling him to resell the item, he
wouldn’t bother with such a question. What does he care how
much money any item cost in the past? All that matters is what
actual services (non-money income) it will produce for him in the
future.

Assume that he has really little hope of being rescued. The
ship is sinking. His raft is almost sinking below the water. The
storm is coming. He has to get back to shore fast. As he is about to
climb off the ship and onto the raft, he remembers that the captain
of the ship was rumored to own a chest full of gold coins. Would.
Crusoe run back to the captain’s room to try to find this chest?
Even if he had enough time, and even if he really knew where it
was, would he drag it to the edge of the ship and try to load it onto
the raft? Would he toss the tools into the ocean to make way for a
chest of gold coins? Obviously not.

But money is wealth, isn’t it? Gold is money. Why wouldn’t he
sacrifice some inexpensive knives and barrels in order to increase
his wealth (money)? The answer is simple: in a one-person envi-
ronment, money cannot exist. It serves no purpose. Crusoe knows
that gold is heavy. It displaces tools. It sinks rafts. It’s not only

‘useless; it’s a liability.

The value of money is determined by what those who value it
expect that it will do for them in the future. A lonely man on a
deserted island can’t think of much that money will do for him in
the future. If he remains alone for the rest of his life, there is noth-
ing that money can do for him at all.

So the value of money in this example is zero.
