124 Honest Mong

preached to the king. “So the people of Nineveh believed God,
proclaimed a fast, and put on sackcloth, from the greatest to the
least of them. Then word came to the king of Nineveh; and he
arose from his throne and laid aside his robe, covered himself with
sackcloth and sat in ashes” (Jonah 3:5-6).

The king wisely asked: “ “Who can tell if God will turn and
relent, and turn away from His fierce anger, so that we may not
perish? Then God saw their works, that they turned from their
evil way; and God relented from the disaster that He had said that
He would bring’upon them, and He did not do it” (3:9-10).

Where We Want to End Up

The economic point of view takes time to develop. Most hu-
manist economists have yet to develop’ it, so don’t feel discour-
aged. It takes time.

The fundamental principles of Bible monetary theory are sim-
ple enough:

1, Standard weights and measures, with penalties imposed by ‘
the civil government against those who tamper with the scales.

2. A prohibition on all forms of multiple indebtedness by
banks, meaning fractional reserve banking.

3, Competitive entry into the silversmith, goldsmith, or any
other smith business.

4. No one is ‘to be compelled by law to accept any form of
money, (This is not stated in the Bible, but it follows from the first
three principles that are based on voluntarism.) This means no
legal tender laws (compulsory acceptance),

In short, the Bible declares freedom under God’s moral law. It
declares freedom without fraud or physical coercion, Men may do pretty
much what they please economically, so long as they avoid fraud
and physical violence. There are only a few prohibitions in the
Bible against voluntary exchange; these refer to inherently im-
moral transactions: (My favorite example of a crime which a vast
majority of citizens immediately recognize as immoral and illegal
is a homosexual who gives heroin to an eight-year-old boy in ex-
change for sex. If you think the State has nothing to say about this
