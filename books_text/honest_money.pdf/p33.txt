The Origins of Money 27

Why do people select a particular form of money? Because
they learn from experience that other people usually accept this
monetary unit in exchange. We can make better predictions an’d.
plans about the future when we discover that other people gener-
ally have accepted a certain currency unit in the past. What peo-
ple habitually do they tend to keep on doing. They have a right to
change their minds, but it’s easier not to, at least most of the time.
Thus, money allows us to gain access in the future to the goods
and services we think we will want, or even to new ones that we
haven’t thought about yet.

Thus, historically it was the free market which determined
what was acceptable to people for their economic activities. It
happened to be gold and silver, but other commodities have some-
times been used widely. The point is, people voluniarly selected
what they wanted to use as money. They did not need a commit-
tee to make this decision for them.

The principles of the origins of money are therefore these:

1, The Bible doesn’t say that people should be required to use
gold and silver as money.

2, The Bible does indicate that people in Biblical times came to,
use gold and silver as money.

3, Money will be selected because people expect others to use
it in the future.

4, To establish what money is worth today, we need informa-
tion about what it was worth yesterday.

5, Tracing this principle backward, we conclude that the money
commodity must have been used for something else originally.

6, Gold and salver were used as jewelry and ornaments.

7, The beauty of gold and silver probably had something to do
with their popularity.

8, The symbolic shining of gold may have been connected in
people’s minds with God’s glory.

9, The metallurgical properties of gold make it highly suitable
as money (the five characteristics).

10. Money is the most marketable commodity.

11, The State can influence the continued us¢ of a monetary
unit by taxing and spending m terms of that unit.
