86 Honest Money

of 1969-71. The boom of1972-74turned into the bust of 1975-767
The boom of 1977-79 turned into the bust of 1980-82. It will hap-
pen again.. It always does.

That’s the curse of counterfeit money.

Pure Counterfeit Money

The modern banking system has gone a long way in the last
fifty years. All over the world, nations abandoned the gold stand-
ard. The citizens are no longer given legal access to true ware-
house receipts (gold-backed money). They can’t take their paper
receipts to a bank or the national treasury and demand a fixed,
predictable quantity of gold (or silver) on demand.

Now the bankers don’t have to worry about a “bank run”
against gold. Neither do the politicians. The result has been mass
inflation all over the world. You could buy a three-bedroom home
in 1918 for under $3,000.

Today, the game is played differently. Let’s see how it works.
Say that you take in $100 cash and deposit it in your bank. The
central bank (in the United States, the Federal Reserve System)
requires banks to keep varying percentages of money on reserve
at the Fed itself, in non-interest-paying accounts. A 10% reserve
makes it easy to compute, though for many accounts it’s under 5%.

The bank takes your $100 and issues you a receipt (bank
deposit slip) for $100. It then takes $10 and wires it to the regional
Federal Reserve bank. Then it loans out the remaining $90.

The guy who borrows the $90 deposits it into his account. Pre-
sumably, he then writes a check for the $90. The person who gets
his check deposits it, His banker takes 10%, or $9, and wires it to
the regional Federal Reserve Bank. Then he loans out the re-
maining $81. The borrower writes a check to someone who depos-
its it in his bank. His banker takes 10%, or $8.10, wires it to the
Federal Reserve Bank, and loans out $72.90.

And so it goes, from bank to bank, merrily multiplying. In
theory, that original $100 cash deposit {or check) creates an addi-
tional $800 in loaned money, plus your original $100.

And you wonder why we have inflation? ‘

You think Pm making this up? Then take a look at the official
