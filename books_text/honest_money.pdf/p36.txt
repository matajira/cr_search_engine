30 Honest Money

Will we be disciplined by ourselves, as individuals under God’s
law? Will we be disciplined by God directly (for example, when
He sends a plague on us, as He did several times in the Old Testa-
ment)? Or will we be disciplined by the State? In our day, State
tyranny is the most common alternative to self-discipline.

Without self-discipline under God’s revealed laws, there can
be no freedom. False weights and measures lead to unrighteous-
ness. People who sell items to the public must be sure that they
avoid giving less than what is expected — revealed on the scales —
through tampering with the physical standards. In short, tamper.
ing with the society's physical standards is a sign that men have
already tampered with the society’s moral standards.

Market Scales

When a person in Old Testament times (indeed, up until rel-
atively modern times) went to market in order to buy something,
he brought with him something valuable to exchange. In barter
societies, he would bring some home-grown or home-made item
for sale. He would try to exchange it for someone else’s home-
grown item, or manufactured item.

If a man brought something that would require weighing (for
example, a sheep) and wanted to trade it for some other item that
required weighing (for example, a sack of wheat), the question of
accurate scales was less important. If something was underweighed :
for the “seller,” it was equally underweighed for the “buyer.” (Re-
member, both parties are buyers and sellers simultaneously: one
buys wheat and sells a sheep, while the other buys a sheep and
sells wheat.) Dishonest weights would be those in which the pro-
fessional sellerthe man who could afford the scales— tampered
with the weights in one half of the transaction. Tampering in half
the transaction probably isn’t easy. ©

When people started bringing metals to market in order to
buy consumer goods, it became easier for sellers to use dishonest
scales. The metal bar or item would normally be measured in
small units of weight (“ounces”), or even smaller (“grams”), in the
case of gold. But the item being sold for money would, if sold by
