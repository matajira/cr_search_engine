3
MAINTAINING HONEST MONEY

You shall do no injustice in judgment, in measurement of
length, weight, or volume. You shall have just balances, just
weights, a just ephah, and a just bin: I am the Lord your God, who
brought you out of the land of Egypt (Leviticus 19:35-36).

It’s not necessary to get into a debate over just exactly what
units of measurement an “ephah” and a “bin” were. The point is
clear enough: once defined, they could not be changed by individ-
uals in the marketplace.

Who defined them? ‘That isn’t said. Not the Hebrew civil gov-
ernment, in all likelihood, because it was being set up at the time
the law was announced. Like the widespread use of gold and
silver, certain weights and measures had also come into wide-
spread use on a voluntary basis. The important thing was not that
the civil government make its definitions “scientific”; the impor-
tant thing was for the civil government to enforce a consistent
standard.

It should be noted that God immediately provides the reason
for this commandment: He is the One who brought them out of
Egyptian bondage. He is the Lord, the sovereign master of the
universe. He is the deliverer of Israel. To avoid being placed in -
bondage once again, they had to discipline themselves. First, they
had to discipline themselves by means of honest weights and
measures. -Second, they had to discipline themselves by means of :
God’s comprehensive moral law.

We cannot do without discipline. It is never a question of “dis-
cipline or no discipline.” It is always a question of whose discipline.

29
