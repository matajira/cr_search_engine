The Politics of Money 139

It must be kept simple. It must also work. What we need to un-
derstand is that God’s principles work. They satisfy equally the
desires of the moral man and the pragmatic man, the theorist and
the activist.

The Biblical principles of money are easy to understand —
much easier to understand than the graduate-school textbooks in
monetary theory. After all, the textbooks try to explain a morally
corrupt system in terms of its supposed long-term benefits, a
super-rich elite-controlled system in the name of democracy, and
an engine of inflation in terms of historic monetary stability. They
try to tell us that there is no reason to repay debt, since “we owe it
to ourselves,” and besides, the Federal Reserve can. monetize it.

Compared to the task of the economists, the Christian’s task is
easy. All he has to promote is a simple truth: “Don’t cheat .”

If the Present System Continues

We can look at the inescapable numbers and make some rea-
sonable guesses.

Debt

The world’s mountain of debt is growing relentlessly. There is
only one answer given by Third World leaders, international
lending agencies, economists, and financial columnists: extend more
credit, Roll over the loans. Make the terms of payment easier. In
short, delay.

“Delay” is not a viable answer. It is a non-answer. Half of the
trillion dollar international debt is owed to Western banks by in-
solvent Third World socialist nations that have no intention of re-
paying, and no means of repaying even if their hearts weren’t evil.
They blame us for their debt, because we loaned them the money
to buy our products, which by now they have squandered in
waves of socialist pyramid-building. Too bad for us, they say. The
stupid gringos lose again. But that’s why God made gringos, in
their view, the same way He made sheep.
