A Program of Monetary Reform 125

voluntary transaction, or “victimless crime,” you are a really ser-
tous philosophical libertarian!)

Thus, in monetary affairs, we should seek to avoid a monop-
oly over money, including the State’s monopoly over money. We
also should oppose the State’s transfer of a monopoly over money
to a central bank that is owned and operated by representatives of
private commercial banks.

The Traditional Gold Standard

Thus, our long-term goal should not be with the return to a
traditional gold standard, in which the State issues both gold coins
and paper money, promising to buy and sell gold at a fixed price
for paper money issued by the State.

It has been a major mistake of conservative economists to
think that a traditional gold standard is anything more than a
temporary stop-gap. It is easy for the State to destroy the gold
standard by issuing either of these announcements:

1, “Henceforth, the new price of gold (new definition of the na-
tion’s currency unit) is [such and such more per ounce].” This
means an increase in the price of gold — an overnight depreciation
of the monetary unit.

2. “Henceforth, the government ‘no longer will redeem its
paper notes with gold on demand.?

The second edict ends the so-called gold standard. In short,
the traditional gold standard is in fact a paper standard: the paper ,
of government promises. Historically, these promises have not
been worth the paper they're written on.

The First Target: Central Banking
The goal is to eliminate all central banking, meaning an in-
stitution operated by and for fractional reserve bankers, but
granted a monopoly over money by the State.
Obviously, this is today a utopian dream. But, could we begin
a program of salami-slicing— a steady cutting away at the Federal
Reserve System? I think we can. Step by step, we should pressure
