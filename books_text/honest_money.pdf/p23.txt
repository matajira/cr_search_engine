The Value of Money 17

culated in terms of first, second, third, etc., not by ten units,
seven units, five units, etc. He had no units in his head, so he
couldn’t use them to make comparisons.

In Egypt, the money failed because everyone wanted the same
thing, grain, and nobody was willing to give up any grain except
the Pharaoh. Trade either ceased or slowed down drastically.
Money ceased to serve as a means of trade. The famine made
people poor, and as trade was reduced, they became even poorer.
The division of labor collapsed. This means that the specialization
of production collapsed. -

Money is a social phenomenon. It comes into existence
because individuals begin to recognize that certain common ob-
jects in society are universally sought after. People then sell their
goods and services in order to obtain this sought-after good. They
store up this commodity because they expect others to sell them
what they need in the future. As in the case of Robinson Crusoe
on board the ship, people want to own whatever will provide them
with income (goods and services) in the future. People make deci-
sions concerning the present and the future. The past is gone for-
ever. Money offers people the widest number of options in the
future, so they sell their goods and services in order to buy money
in the present.

The principles governing the value of money are these:

1, Economic action begins with an ordered set of wants (first,
second, third, etc.),

2. A world of scarcity doesn’t permit us to achieve all of our
desires at the same time.

8, To increase output, we need capital (tools).

4, We have to sacrifice present income in order to obtain
capital,

5, The value of the tool to each person is dependent on the ex-
pected value (to him) of the future output of the tool.

6. Value is imputed by a person to goods and services; it is
therefore subjective.

7, Past costs are economically irrelevant; present and future
income are all that matter.
