The Reform of Debt uy

mercial banks are holding. Of course, the Fed will swap them at
the book-vaiue of the Third World bonds, not the market value.

Presto! The commercial banks now own the best assets on the
market, The Fed owns an equal dollar amount of worthless Third
World debt. But this debt is still on the Fed’s books at original pur-
chase price book value. No problem!

Is this legal? Sure. The infamous Monetary Control Act of
1980 legalized the Fed’s purchase of any form of debt certificate in
the world to serve as a “reserve” for the U.S. money supply. The
Fed can buy Third World debt, corporate bonds, or anything.

The banks then own the Treasury debt. All interest payments
for this debt will go to the banks rather than the Fed. The banks,
unlike the Fed, do not return to the Treasury 85% of their T-bill
interest payments each year. Thus, the real debt of the U.S. tax
payer will have gone up by a factor of .85 for every dollar’s worth
of U. S. debt transferred from the Fed to the commercial banks.
Yet the account books of the commercial banks, the Fed, and the
US. Treasury will show zero change.

As I suggested in Chapter Eleven, why not abolish the Fed
and let the Federal government do, the swap, in order to get the
banks to agree to a slow increase in reserve requirements? At least
we should get some long-term reform out of the deal.

Using an Intermediary (the “Bag Man” Approach)

The Fed buys debt certificates from the International Mone-
tary Fund and/or the so-called World Bank (the International
Bank for Reconstruction and Development), the two biggest “in-
ternational” foreign aid boondoggles in the West. These outfits
then lend the money to the dead-bat Third World governments.
They in turn repay this quarter’s interest payments (but never any
principal) to the West’s banks. The two-bit, tin-horn dictators
who run these deadbeat socialist nations will of course pocket a
few millions for themselves, and therefore agree to play the game
a bit longer.

What we've got here is a kind of multinational bank-financed
and U.S. taxpayer-financed retirement program for dictators, It’s a lot
