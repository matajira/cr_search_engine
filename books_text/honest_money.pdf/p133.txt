A Program of Monetary Reform 129

that have already loaned out the money. The length of maturity of

the deposit will match the length of maturity on the loan. There
will no longer be “a substantial penalty for early withdrawal,” as

the bank advertisements say today, because there won’t e any early
withdrawal, There will be only borrowed money with the original
deposit and its interest pledged as collateral.

Buying Of the Bankers ,

Will the banks scream bloody murder? You can imagine just
how loud. One of the most effective political lobbies in the world
will go into action. So we offer them a deal. We buy them off.

We tell the bankers, “All right, boys, we all know the mess~
you're in. You are sitting on top of a mountain of bad debts. You
want out. The U.S. government is here to help you weather the
storm. We will do a swap. You sell us your pile of Mexican and.
Brazilian bonds, and we will give you nice, safe 90-day Treasury
bills in exchange. You get your portfolios liquid again. We will
take all that lousy debt you’re sitting on, which you know will
never be paid off, and you get in its place interest-paying T-bills.
You can even sell them if you want — there’s a market for them,
unlike those Mexican bonds you're sitting on.”

That would bail out the big banks. It would defuse the Third
World debt crisis. It would “re-liquify’ the banking system. And it
would cost the taxpayers only the interest that the Fed now pays
on the T-bills it owns. They would go for it.

What about the small rural banks? What's in it for them? Give
them the same deal with any remaining T-bills. Swap their lousy
farm mortgages for nice, liquid T-bills.

In short, use the reserves of the Fed (.$175 billion) to
strengthen the banks, Then force the banks to start substituting
honest money for counterfeit money.

What would it cost the U.S. taxpayers? Only what they get
back from the Fed each year. If the Fed owns $176 billion in
T-bills, and they are paying the Fed 7% per annum, then the Fed
takes in a little over $12 billion a year. Of this, the Fed will repay
85%, or about $10.4 billion. That’s money that the Treasury will
