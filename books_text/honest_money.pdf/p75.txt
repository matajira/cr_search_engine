When the State Monopolizes Mong 69

2, The State then violates the principle of economic freedom:
allowing people to buy and sell on their own terms: it makes
private coins illegal.

3. The State claims for itself an economic monopoly that it
cannot be trusted to possess.

4, The State in the ancient world used the coinage to pro-
pagandize the public (false religion),

5, The State misuses the trust of the people.

6, The State becomes an official debaser of the metal coinage:
adding cheap metal (“dross”) to the precious metal.

7. These new, “dross” coins add to the number of monetary
units in use,

8. People then bid up the price of goods and services, since
they have more money to spend. .

9, Price inflation begins to erode people’s faith in the money.

10. The modern State uses paper money to hide, and then
speed up, the debasement process.

11, The State has imposed an invisible tax: inflation.

12, Economically and morally, there is no difference between
private counterfeiting and public counterfeiting,

13. Politically, there is a difference: the public loses faith in the
law.
