36 Honest Money

warehouse receipts. Some company is required by law to redeem
the paper receipts by paying out the specified quantity and fine-
ness of gold or silver. To issue phony receipts places the issuer at
risk. Or, if the company should go bankrupt and be unable to
redeem the notes, it places at risk the last person who accepted the
receipt at face value. He goes to get his gold, and the issuing’ com-
pany has gone bankrupt. He is stuck with a worthless warehouse
receipt, whereas the counterfeiter has bought valuable goods and
services. The loser (among others) is the last guy to get stuck with
the bad receipt when the bad news is made public. Unquestion-
ably, counterfeiting is a form of theft.

One way to protect users from counterfeit bills is for the bank
to allow the depositor to. write receipts for the deposited coins
whenever he makes a transaction. This way, the user has to sign
his name at the time of purchase. He writes checks (warehouse
receipts) until he runs ‘ out of coins in reserve. Then he stops,
unless he is a thief, or he makes a mistake (and pays a penalty to
the bank), or he has made prior arrangements with the warehouse
to “cover” his checks with extra gold which is held in reserve— gold that
has no warehouse receipts issued against it — for this purpose by
the warehouse firm.

This is why checks are money, é the money metal backing them up
is money. They are metal substitutes. An honest check is simply
another form of warehouse receipt.

A credit card is also money, #fthe metal backing up the credit card is
money. Tt, too, is a metal substitute. An honest credit card is simply
another form of warehouse receipt.

We could add all sorts of examples to this line of reasoning,
but by now you have the idea. The key is the honesty of the ware-
house firm. If it issues no more receipts than it has gold or silver in
reserve to redeem the receipts, then the receipts can legitimately
serve as forms of money.

If a warehouse company issues more receipts to valuable
money metals than it has metals on reserve, then it has violated
the law against false weights and measures. The difference is that
it is harder to detect’ a false (unbacked) warehouse receipt than it
