98 Honest Money

tificates), When the Fed buys, it increases the money supply (mul-
tiplying because of fractional reserves). When it sells, it deflates
the money supply (shrinking by this same multiplication
number).

But it never sells for more than a few weeks. It is almost
always buying. It is almost always inflating.

Thus, the American business cycle (“boom and bust”) is con-
trolled by a handful of men who are not directly controlled by the
President or the Congress, except in those rare instances when the
Legislature and the Executive agree completely and press their
decision on the Fed.

Oh yes, I forgot to mention that the Fed owns the entire U.S. gold -
stock, Legally, there is no “United States” gold stock. There is only
the Fed’s gold stock. It is stored, not in Fort Knox, Kentucky, but
at 33 Liberty Street, New York City, New York. The U.S. gov-
ernment has always sold its gold to the Fed, beginning in 1914.

Where did the Fed get the money to buy the gold? It created
it, of course. In short, it counterfeited it. But it’s legal.

What is really choice is that in 1933, the U. S. government out-
lawed the private ownership of gold. It bought all the gold it could
forcibly collect from the public, paying the going price of $20.67
per ounce. Then it sold it to the Fed at $20.67 per ounce. The
next year, the government raised the price of gold to $35 an
ounce. Net profit to the Fed: 75%.

This raised the legal reserves for banks, and the money supply
(so-called M-1) zipped upward by 30°%, 1933 to 1935.

“No!” you say to yourself. “It couldn’t be true. The government
confiscated our gold in 1938 so that a private corporation owned by
the member banks could buy it at a discount? Impossible!”

All right, my skeptical friend, pick up a copy of any Friday ,
edition of The Wail Sirect Journal. Somewhere in the second section
(they always shift it around) you will find a table called Federal
Reserve Data. Check the listing under “Member Bank Reserve
Changes.” You will see a quotation for “Gold Stock.” It never
changes: $11,090,000,000. They don’t sell it, and it’s kept on the
books at the meaningless arbitrary price of $42.22 per ounce.
