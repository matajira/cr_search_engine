44 Honest Money

of the main reasons that the Roman Empire fell’ into the hands of
the Christians around 320 4.p. is that the pagan emperors had
destroyed the Roman coinage system. Nobody trusted the money,
so nobody trusted the State.

The Process of Debasement

Why would any private producer of silver ingots want to
debase his product? Because he could get more short-run profits by
doing 80.

Say that you are a corrupt expert at smelting metals. This ©
skilled trade was a near-secret trade in the ancient world. A secret
guild controlled mining and smelting in many cultures. Not many
people knew the secrets. This made corruption easier, whenever
the guild decided to produce short-term profits for its members.

The smelter could do the following. He had molds into which
he poured molten metal. He could pour in pure silver, but silver
was in short supply. This was why it was a precious metal. It was -
a lot cheaper to buy tin. So the cheater would meltdown some tin,
and then pour a little tin into the formerly pure liquid silver.

Who would know? The ingot would still look shiny. It would
look like silver. How many people would own accurate measures
and scales to detect the shift in weight produced by the tin?
Hardly anyone.

The silver producer could sell the debased silver in exchange
for scarce goods and services. But by adding cheap metals (dross),
he could continue to buy as many scarce goods and services as he
had been ‘able to buy the day before with pure silver. People
trusted him. They wouldn’t measure and weigh his ingots. They
wouldn’t insult him in this way.

But what if the authorities found out? He could bribe them.
Only if the bribe was as valuable as the profits on the deception
would he really worry.

It was a simple scheme. Just pour “a little” tin, or other cheap
metal, into the molten silver, pour the molten metal into an ingot,
and there was instant profit.

What is his profit? The extra silver he has left after the tin has
