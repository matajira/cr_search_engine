148 Honest Money

better than your retirement program, I assure you. It’s tax-free.
Unless, of course, the dictator gets shot first. Then your program
may turn out to be a better deal.

‘The End of the Road

These quick fixes only delay the day of financial reckoning.
The Third World debt keeps getting larger. It compounds up-
ward. Third World countries seldom repay any principal. They
borrow the money needed to pay the interest. Today, we define .
national bankruptcy as “not being able to borrow enough money
to make the interest payments for a full year.”

It can’t go on forever. Yet the “experts” have offered no solu-
tion except to keep making more loans. They have no solution.
There is no solution. The debtors will default.

Sowe are back to my original assertion: long-term debt is im-
moral, and God always brings judgment on those who extend it.
We have made bad loans to bad governments for stupid uses. By
“we,” I mean our political representatives and our financial repre-
sentatives, the multinational banks.

But, you say, y local bank hasn’t made loans to Third World
nations. Maybe not, but does your local bank buy the CD's (certi-
ficates of deposit) of the big multinational banks? A lot of local
banks do. If the big banks go down, so do a lot of local banks.

There will be a default. There must be a default. God will not
be mocked. We have deliberately subsidized evil. We have poured
money into foreign government boondoggles, and we have bought
foreign government debt. We have not aided the future-oriented
productive businessmen of the Third World. We have financed
the socialist bureaucracies that oppress them. We have not put
our money into self-liguidating loans, We haye financed deadbeat
governments with their deadweight projects. We will pay the price
for our foolishness.

Debt and Mass Inflation

But everyone is in debt. The government even subsidizes it by
allowing us to write off mortgage interest payments against our
income when we calculate our income taxes. There has been an
orgy of debt over the last generation.
