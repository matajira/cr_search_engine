Conclusion 15

Monopoly

Men cannot be trusted to possess monopolistic power. No “

human agency can safely be trusted with absolute power. An ab-
solute monopoly over anything is exclusively God’s prerogative,
for He alone has an absolute monopoly over everything. All of
men’s possessions and powers are to be limited.

The so-called “natural monopoly” is very nearly mythical.
Very few of them exist. Water in a desert might be one, but most
people choose not to live in deserts or walk through them, so such
a monopoly is not economically relevant. Almost all economically
relevant monopolies are created by, granted by, and sustained by
the institution that possesses God’s delegated monopoly of vio-
lence: the civil government. This is why it is necessary to have
numerous competing civil governments. The Tower of Babel was
the incarnation of institutional evil (Genesis 11).

The monopoly over money is perhaps the most dangerous of
all strictly economic monopolies. Money is the common link in
almost all economic transactions. When monopolists tamper with
the monetary unit, they send information signals to every partici-
pant in the market, When these signals are not created by compe-
titive market forces, they misinform buyers and sellers, savers and
borrowers. This misinformation can result in economic crises:
mass inflation or disguised inflation (price controls), arid then
depression.

There is no known way to protect people from erroneous mon-
etary information if money is controlled by the State or by any
agency licensed by the State. Only competitive market forces can
produce accurate, reliable information on a consistent basis, for
competition will put economic pressure on the producers of false
and misleading information. People who sell better information
appear on the market, and the misinformers start to experience
losses.

Thus, what is needed is a market-produced monetary system.
Historically, gold and silver have been “the people’s choice.” This
could change in the future. But it is difficult to mass-produce gold
