. Biblical Banking 73

applies in every area of economics): the present value of future goods is
always discounted in comparison with the immediate value of those same
goods,

What is this discount called? Tl] bet you’ve already figured it
out. It’s called the rate of interest.

You discount the future value to you of any good compared to
what that same good is worth to you immediately, whether it’s that
Rolls-Royce or an ounce of gold from that piece of property. For
me to get you to hand over the present good today (money), I
have to promise to return it to you in the future, ‘plus extra money
or other benefit. In other words, I have to pay you interest.

In the parable of the talents, the mastér was angry with the
fearful steward because the steward only gave him back his origi-
nal coin. At the very least, the master said, he could have lent it to
the money changers, and have received back some interest.

Banks, Risk, and Interest

Information isn’t free of charge. Someone has to pay for it.
You may be given it as a gift (“Let me give you a piece of my
mind, friend!”}, but people seldam value such free advice (“Buddy,
I don’t think you can spare a piece of your mind!”). So usually we
have to pay for it. Nobody complains about having to pay for
something valuable.

Say that you have a lot of cash. You're a frugal person and”
concerned about your future. You want to have a “nest egg” for
the future. So you’re interested in loaning out some of the money.

I come to you and tell you that I know a businessman with a
great idea for a profitable investment. He wants a partner to put
up the money. He will pay the partner 2570 of the profits. But if he
goes bankrupt, the partner loses the investment. No, you think to
yourself, that’s too risky.

You counter with this offer: have the businessman, guarantee
me out of his own pocket a 10% rate of return on my money,
whether the project works or not. Then I'll loan him the money.

What do I do? First, I go to the businessman. He thinks he
will be able to make 30% on the money.
