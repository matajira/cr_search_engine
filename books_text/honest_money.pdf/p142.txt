138 Honest Mong

But God will not be mocked. The reform will come. It may
have to come through economic collapse, which will involve the
destruction of the present world monetary system. Then, out of
the rubble of broken contracts, broken hopes, and broken State
currencies, something better will arise, if Christians know what to
recommend, and if they are in a position to get a hearing.

The best thing about the Biblical principles of money is that
the State doesn’t have to do much. It can just leave things alone.
A team of bank examiners will have to see to it that the banks and
other financial institutions are not creating money by issuing the
equivalent of unbacked warehouse receipts, but that is just about
it. I am not calling for a top-down imposition of a “new monetary
order.” I am calling only for a bottom-up development of as many
monetary alternatives as people are capable of devising and im-
plementing.

Too Easy . . . or Too Hard?

The principled program is always easy to specify. It is not easy
to impose. That is the problem: imposing a program. A top-down
transformation of the social order just isn’t possible. People rebel.
They learn how to cheat, gum up the works, beat the system. The
State spends an increasing amount of its dwindling resources in
just gaining minimal compliance, let alone active cooperation.

There is only one way to gain long-term social reform: to con-
vince the average citizen of the moral correctness of a general approach
to life, as well as the blessings for adherence to this general approach to
life. That is why God included Deuteronomy 28 in His Word:
blessings and cursings. That is why He commanded the reading
of the whole law every seven years (Deuteronomy 31:9-13): to
reinforce men’s understanding of the general principles of Biblical
Jaw and the case-law applications in daily life.

The law must be simple enough for simple people to understand,
and comprehensive enough for judges to apply to any and every dis-
pute among men. The Bible provides such a law-order.

A successful long-term reform of any institution must gain the
understanding and cooperation of those who will be affected by it.
