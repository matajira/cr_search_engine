6 Honest Money

and silver. It is impossible to have mass inflation with a currency
based on any metal. It costs too much to dig metal out of the
ground.

In contrast, paper and ink are cheap. So are blips on comput-
ers. If something is cheap to produce, you find that people try to
make profits by mass producing it, This is as true of money as it is
of hand-held electronic calculators. But additional hand-held elec-
tronic calculators constitute an economic asset, Additional money
constitutes a redistribution of assets, where the ignorant and vul-
nerable are most likely to be harmed.

Not Quite Honest Money

It is not my job or your job to tell other people what money
they ought to use. It is not the State’s job, either.

It is the State’s job to enforce honest weights and measures,
and also to prohibit the issuing ‘of warehouse receipts that are not
immediately backed by whatever is promised by the receipt. If the
receipt promises to redeem the receipt on demand, there must be
an asset on reserve for every such receipt.

If the State does these two tasks, society will have honest
money. If it doesn’t, society can have only semi-honest money. A
traditional gold standard is an example of semi-honest money:
fractional reserve banking with full redemption of gold coins on
demand. This also involves the governments pledge to buy or sell
gold coins at a specified (definitional) price. But bankers cheat,
and governments cheat, and the traditional gold standard sur-
vived for only about a hundred years, 1815-1914.

The one monetary power that the State legitimately possesses
is its right to specify a form of payment for taxation purposes. To
that extent, the State can influence the selection of monetary
units. But the State does not want to be paid in debased money, so
it becomes a watchdog for the public because of its own self-
interest in protecting itself from unscrupulous counterfeiters.

For decades, small conservative groups have campaigned for a
return to the traditional gold standard. These campaigns have
been about as successful as promoting a national treasure hunt for
