2
THE POLITICS OF MONEY

Therefore the Lord says, The Lord of hosts, the Mighty One of
Israel, “Ah, I wil nd Myself of My adversaries, And take ven-
geance on My enemies. I will turn My hand against you, And
thoroughly purge away your dross, And take away all your alloy. I
will restore your judges as at the first, And your counselors as at
the beginning. Afterward you shall be called the city of righteous-
ness, the faithful city” (Isaiah 1:24-26).

The problem with discussing money is that people with only a
smattering of knowledge about monetary theory or practice think
they know all they really need to know. They hold strong opinions
in many cases. Economists are insufferable; everyone else is
merely intolerable.

People who would never think of voicing an opinion
organic chemistry have fixed views on money. The number of rec-
ommendations concerning the reform of money is legendary: the
recommendations go all the way back to the ancient world.

The ancient world didn’t come to any clear conclusions,
either.

But what about the Christian church? What has it said about
money? Not much. It has legislated against “usury” (interest) for a
millennium and a half, fortunately with little success. So far as I
know, the only people who claim to preach a Christian view of
money are a minority of adherents of the inflationist group called
the Social Credit movement, an intellectual heir of the old “green-
back” movement of late-nineteenth-century America. (The
“greenbackers” were proponents of “green”-backed paper money,

132

on
