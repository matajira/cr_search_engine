Debasing the Currency 47

which was itself a reflection of corrupt hearts, He was talking about
both forms of corruption.

Summary :

The prophets came before Israel with a covenant lawsuit from
God. They warned the people that if they refused to repent, from
the lowest worker to the king himself, that God would bring judg-
ment against them. He would “purge their dross .” This judgment
would be as visible as their sins. This judgment would not be lim-
ited to “internal” crises: psychological fears, and so forth. His
judgment would be both internal and external, just as their sins
were both internal and external.

One sign of their sinful condition was their corrupt silver.
Their money was corrupt, dross-filled. Their money reflected.
their moral condition. It testified against them publicly. They had
debased their money because they had debased their morals. The
two practices went together.

Historically, nations that are marked by honest money do not
fall to external enemies. Wars that are begun with honest money
are fought with dishonest money: debasement on a massive scale.
Both sides do it, but the truth is still the truth: the loser will not be
found with clean hands, monetarily speaking.

Civilizations fall when they become morally corrupt. One sign
of this corruption in virtually all known instances is debased
money. When a society finds that its rulers have debased the cur-
rency unit, the people receive a warning: the rulers are corrupt,
and if the people continue to support these rulers, then they, too,
are corrupt.

And in modern times, civil governments have the full support
of their people for at least “limited inflation ,” meaning “a little cor-
ruption” of the money supply.

We have learned the following lessons from Isaiah’s critique of
Judah:

1, A corrupt tree brings forth corrupt fruit.
2, The people were morally corrupt.
3, God promised to “purge” them because of their sins.
