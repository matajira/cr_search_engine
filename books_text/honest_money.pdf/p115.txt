A Biblical Monetary System 109

Both sides of the loan would be of equal time length. This way,
bankers would not be able to “lend long” and “borrow short.” They
would not be able to loan out money for long periods, yet also
guarantee to return deposits on demand. This is what corrupt
warehouse owners do when they issue more receipts for gold than
they have gold on reserve, and then use gold deposited by one

‘person to pay off the gold withdrawer. Every transaction would be
time-specific. There would be no long-term loans without long-
term lenders.

This .would protect the banking system from bank runs. It
would also protect the community from counterfeit money being
created by fractional reserve bankers.

Government bank examiners would check the banks in the
same way that they check scales of retail sellers. They would see to
it that every loan had a corresponding deposit. Although the bank would
be allowed to pool loans of the same length of maturity (in order to
decrease the risk to a depositor that “his” debtor might default on
the loan), no lending depositor would have cheek-writing privileges.
Check-writing privileges would be offered only to those people who
put their money in a fee-for-service safekeeping account.

This may sound confusing, but it’s not nearly so confusing as
central banking, fractional reserve requirements, - the monetiza-
tion of debt, and other horrors of the modern banking system.

A 100% reserve banking program is simple in principle: some-
thing for something, and nothing for nothing. No “free” anything.
No impossible promises: ‘You can write checks at any time
against money that we already loaned out, so that we can pay you
interest.” When you are promised the use of money that has been
loaned out, you know you're getting conned, When they also pay you
interest on the money you can use any time, you ally know
you're getting conned.’

If Blondie comes to Dagwood and asks to borrow money from
him so that she can buy some new hats, but she also promises him
that he can have his money back at any time, plus she'll pay him
interest on it, he would probably know better. Even Dagwood
isn’t that stupid. But he'll deposit his money in an interest-paying
