32 Honest Mong

Perhaps even more important, the seller of produce has com-
petitors. Buyers catch on when they are being cheated, if they
have access to a rival. The competitors have an economic incen-
tive to warn the buyers, or warn the civil government, about the
fraud at any particular shop. Thus, market competition tends to
pressure produce sellers to stay honest, at least within the gener-
ally accepted “permissible range” of the free market.

Scales of Justice

God links the ownership of scales with His own sovereignty.
The man who owns the scales is a judge. God judges men in terms
of moral standards, He is a Judge with the scales of justice. When
the evil Babylonian king Belshazzar was having his great feast, in
the midst of a military siege by the Medo-Persians, the hand of
God wrote the famous words on the wall: “MENE, MENE,
TEKEL, UPHARSIN.” The king called Daniel to translate, and
Daniel did so: “MENE: God has numbered your kingdom, and.
finished it; TEKEL: You have been weighed in the balances, and
found wanting” (Daniel 5:25-27).

Weighed in the balance: this is symbolic of God’s final judgment.
Therefore, the man who controls the “scales” of civil justice is a
judge. So is the man who controls the actual weights and meas-
ures in the marketplace.

If a man misuses his position and cheats people, he is thereby
testifying falsely to the character of God. He is saying, in effect,
that God cares nothing for justice, that He tips the balance, that
He cheats mankind for His own ends. This is precisely what Satan

_ implies about God’s role as Judge. It is false witness against God.
Thus, God warns men that they must use honest weights and
measures, for He is the sovereign God who delivered them out of
bondage. He implies that He has the power to deliver them back into
bondage if they cheat in this very special area of economics.

Honest Metal Money
What was money in ancient Israel in the days before the
Babylonian captivity? It would have been any item that people
voluntarily accepted in exchange for goods and services. The only
