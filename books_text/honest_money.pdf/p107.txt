Protecting Licensed Counterjeiters 101

us out of in the election of 1916. They just change the rules. No
* more gold on demand.

Then they inflate the currency to pay for the war without rais-
ing visible taxes to cover all expenses.

Since 1933, the United States hasn’t been on a gold standard
for U.S. citizens, and since 1971, it hasn’t been on a gold standard
for foreign central banks. This keeps embarrassing runs on banks
from occurring as often.

But some day, Mexico or Brazil or some huge-debt foreign na-
tion will default, and the biggest banks in the country will become
officially bankrupt. The runs will begin. Then the Fed will step in
and create the cash to stem the runs. Fed officials will inflate their
way out of the crisis. On that day, you had better own gold, silver,
and other similar non-paper assets. The dollar will die.

Summary ‘

As the system of fractional reserve banking has become uni-
versal throughout the world, and as the banks have become more
vulnerable to bank runs, governments have changed the rules in
order to reduce risks for bankers, at least the biggest bankers.

The central banks gained the power to establish reserve re-
quirements: the money that banks must keep on hand against
deposits. Then the Fed lowered these reserve requirements. The
Federal government abolished the gold standard in 1933. The
FDIC was created as an illusion of government-guaranteed bank
deposits in 1934, The international gold standard was abolished in
1971, to reduce the pressure placed on the Fed by foreign central
banks to give up the gold it holds, supposedly in the name of the
Federal government.

And with each reduction in risk for big bankers, they have
made wilder and riskier loans. Today, the international commer-
cial banking system has loaned over one trillion dollars to nations
and major debtors. The Eurodollar market (a giant, unregulated,
almost zero reserve requirement debt market) is over a trillion
dollars now. It was under a billion dollars in 1959.

‘Thus, the world faces a crisis: either more debt to insolvent
