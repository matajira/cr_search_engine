Debasing the Currency 41

nant. Obedience to God’s covenant brings external, visible blessings,
He promised (Deuteronomy 28:1-14), while disobedience brings
external, visible cursings (Deuteronomy 28:15-68). The list of curs-
ings is much longer than the list of blessings. God wanted them to
know just how serious He is about obedience to His law -eternally
serious.

As an “officer of God’s court,” the prophets brought God’s cov-
enantal lawsuit against Israel and Judah. But to make a case, the
prophets had to have evidence. It is not enough in God’s earthly
law court that people are suspected of harboring evil thoughts. It is
not enough to convict someone in God’s earthly court of bad inten-
tions. There must be public evidence of a crime. “Whoever is
worthy of death shall be put to death on the testimony of two or
three witnesses, but he shall not be put te death on the testimony
of one witness” (Deuteronomy 17:6).

This is why God sent many prophets to bring charges against |
Israel. In Isaiah’s day, there also appeared Hosea (Hosea 1:1 has
the same list of kings as that in Isaiah 1:1), Amos (Amos 1:1), and -
about a generation later, Micah (Micah 1:1). They all brought the
same message of God’s anger and coming judgmeni.

What was the public evidence? First, Isaiah pointed to the
false judgment by the rulers. “How the faithful city has become a
harlot! It was full of justice; righteousness lodged, in it, but now
murderers” (1:21). Second, he pointed to the dross metal in the sil-
ver, and the water in the wine (1:22). Third, he returned to the
theme of corrupt judgment: “Your princes are rebellious, And
companions of thieves; Everyone loves bribes, And follows after
rewards. They do not defend the fatherless, Nor does the cause of
the widow come before them” (1:23).

Notice that the sins listed by Isaiah are quite specific:
murderers in the capital city (evidence of a breakdown of law and
order), debased commodities being sold as high quality, and false
judgment - by bribe-seeking, gift-seeking judges. Even before he
began to talk about the spiritual sins of the people (dross in their
hearts), he spoke about the visible sins of the rulers. The rulers
were visibly corrupt, indicating that the people were also corrupt. The
