166 Honest Money «

the best proof we have: God is both wrathful and loving. Without
the fires of hell as the reason for the cross, the agony of Jesus
Christ on the cross was a mistake, a case of drastic overkill.

What about heaven and hell? We know from John’s vision of
the day of judgment, “Death and Hades [hell] were cast into the
lake of fire. This is the second death. And anyone not found writ-
ten in the Book of Life was cast into the lake of fire” (Revelation
20:14-15). |

Those whose names are in the Book of Life spend eternity with
God in their perfect, sin-free, resurrected bodies. The Bible calls
this the New Heaven and the New Earth.

Now, which is more eternal, the lake of fire, or the New
Heaven and the New Earth? Obviously, they are both eternal. So,
God’s wrath is equally ultimate with His love throughout eternity.
Christians all admit this, but sometimes only under extreme pres-
sure. And that is precisely the problem. ,

For over a hundred years, theological liberals have blathered.
on and on about the love of God. But when you ask them, ‘What
about hell?” they start dancing verbally. If you press them, they
eventually deny the existence of eternal judgment. We must un-
derstand: they have no doctrine of the total love of God because
they have no doctrine of the total wrath of God. They can’t really
understand what it is that God is His grace offers us in Christ
because they refuse to admit what eternal judgment tells us about
the character of God.

The doctrine of eternal fiery judgment is by far the most unac-
ceptable doctrine in the Bible, as far as hell-bound humanists are
concerned, They can’t believe that Christians can believe in such
a horror. But we do. We must. This belief is the foundation of
Christian evangelism. It is the motivation for Christian foreign
missions. We shouldn't be surprised that the God-haters would
like us to drop this doctrine. When Christians believe it, they
make too much trouble for God’s enemies.

So if we believe in this doctrine, the doctrine above all others
that ought to embarrass us before humanists, then why do we
start to squirm when God-hating people ask us: ‘Well, what kind
