92 Honest Money

The Mexican government sends the money to Pemex, which
then remits $25 million to pay this quarter’s interest payment to
the New York bank. Pemex pays the bank a fee for “rolling over”
the loan. Three months from now, another $25 million will fall
due. The chairman of the New York bank gets a round of ap-
plause from the bank’s board of directors, and perhaps even a
bonus for his brilliant delaying of the bank’s crisis for another
three months.

The $25 million then multiplies through the U.S. fractional
reserve banking system, creating millions of new commercial dol-
Jars in a mini-wave of inflation.

This scenario could really take place, given United States law.
Is this system just? Would you say that the law respects neither
the mighty nor the poor man?

-The Federal Reserve System

In late November of 1910 (probably November 22), a private
coach carrying some of the nation’s leading bankers and a U.S.
Senator pulled out of the Hoboken, New Jersey, train station and
headed for Georgia. Their ultimate destination: Jekyll Island,
which was owned by some of the richest men on earth as a hunt-
ing club. Membership in the-club was by inheritance only.

On board that train was Senator Nelson Aldrich, the maternal
grandfather of Nelson Aldrich Rockefeller. Also aboard: Henry P.
Davison, a senior partner in the powerful banking firm of J. P.
Morgan Co., Benjamin Strong (another Morgan employee), and
a European expert in banking, Paul Warburg. Representatives of
two other major New York banking firms were also present .

The reporters who gathered at the train station were told noth-
ing, except that the men were all going duck shooting. Six years
later, Bertie Forbes, the man who founded Forbes magazine, re-
ported briefly on the meeting, and most people thought the whole
story was just a “yarn.” Very little has been written on it since 1916,
(Probably the most detailed account is chapter 24 of the highly
favorable biography, Nelson W. Aldrich, by Nathaniel W. Stephen-
son [Scribner’s Sons, 1930; reprinted by Kennikat Press, 1971.])

At that secret meeting, these men designed what became the
