20 Honest Money

People want to be able to buy what they want tomorrow or next
week or next year. They aren’t really sure which economic goods
will be in demand then, so they seek out one good which will
probably be in heavy demand. They can buy units of this good
now, put them away, and then buy what other goods or services
they want later on. In short, money is the most marketable com-
modity. It is marketable because people expect it to be aaluable in
the future.

This isn’t too difficult to understand. But it raises a problem.
The unit we call money is valuable today. We have to sell goods or
services in order to buy it. In other words, money has already es-
tablished itself as the common unit of economic calculation. My
labor is worth a tenth of a unit per hour. A brain surgeon’s labor is
worth a full unit. A new car is worth ten units. Money has ex-
change value today. If it didn’t, it wouldn’t be money. We have all
learned about money’s value in our daily affairs. We are familiar
with it. ~

How do we know what it’s worth today? We know what it was
worth yesterday. We have a historical record for its purchasing
power. If we didn’t know anything about money’s value in the
past, we would not accept it as a unit of account today. If it has no
history, why should anyone expect it to have a future? But if peo-
ple don’t expect it to have a future, it can’t serve as money.

So here is the key question: How did money originate? If it
has to have a history in order to have present value, how did it
come into existence in the first place? Are we confronting a
chicken-and-egg problem?

This was the intellectual problem faced by one of the greatest
economists of all time, Ludwig von Mises, an Austrian scholar. In
his book, The Theory of Money and Credit (1912), he offered a solution
to this important question. Money, he argued, came into exis-
tence because in earlier times, it was valued for other properties.
He thought that gold was probably one of the earliest forms of :
money — not a unique observation, certainly. Before it functioned
as money, it must have served other purposes. Perhaps it was used
as jewelry. Possibly it was used as ornamentation. We know that
