When the State Monopolizes Mong 61

coins (the State) and the person who made the State possible (the
political-religious leader).

It is not surprising that the first coins ever issued were issued
in order to strengthen the State. While Greek coins in the ancient
world were in part used to expand commerce, historians are now
generally agreed that political motives were equally important as
economic motives. The right to issue coinage was a sign of a city-
state’s political and legal independence. In other words, the State’s
officials saw coins as an effective means of strengthening citizens’
loyalty to the existing government.

But the symbolic importance of coins was orly:the beginning.
The State could use coins as a means of collecting taxes. If the
State issued precious metal coins, it could collect coins as taxes,
This made it easier to keep tax records, and politicians always like
to simplify tax collecting! The State could buy goods and services,
including the services of armies, if it had coins.

Where could the State get the precious metals? From mines,
or from successful warfare, or from taxing businessmen who were
involved in trade. Once the State sanctioned money, it would
have led to an increase in demand for certified money. After all, the
State collected its taxes with its own money. This would have cre-
ated demand for money just in itself.

Eventually, the politicians learned about the short-term bene-
fits of debasing the currency. They learned quite early, in fact.
When the State took in gold and salver, it then issued coins that
were pure. But as time went on, and people became accustomed
to the coins, the old debasement trick became too tempting for
politicians to resist.

People don’t like to pay taxes. They never have. Politicians
love to spend money. They always have. So politicians long ago
figured out a way to increase spending without increasing direct
tax collections. If they just took out some of that molten gold or
silver, and poured in some cheaper metal, they could produce
more coins with the extra gold or silver. You have heard all this '
before. (Take a “silver” coin —ha, ha—out of your pocket. You
have in your hand tangible proof that politicians haven't changed
