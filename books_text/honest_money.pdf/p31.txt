The Origins of Money 25

men, go about their daily business, making decisions, planning
for the future, and focusing their attention on their own personal
and family needs, and out of all this hustling and bustling, push-
ing and shoving, comes the most productive economy in the his-
tory of man.

Christians can believe that the world is orderly because it was
created by God. The Bible teaches that God is sovereign, but men
are fully responsible for their actions. As they interact with one ,
another, they learn things. They find out what the y must offer to
other people in order to buy what they want. They also find out
what other people are willing and able to offer them for the things
that they presently own. Market competition is a form of exchang-
ing information, Free market activity can be described as a Process of
discovery.

We don’t need a committee to tell us what we need to do to
satisfy other buyers. In fact, a committee cannot possibly know all
the things that we know as individuals, taken as a group. What
we learn we can put to profitable use later on.

This spread of knowledge is made much easier by the exist-
ence of an agreed-upon currency unit. I don’t mean that we all sat
down and agreed to use it. I mean that people learned that other
people will usually accept a particular currency in exchange for .
goods and services. As this learning process continues, certain
currency units become familiar. It’s always easier for us to deal
with each other if ‘the rules of the game” are known in advance.
The currency unit is the most important single source of information
concerning the state of the actual conditions of supply and demand.

Who decides which currency unit is acceptable? Originally,
the people did who entered into agreements with each other about
buying and selling. They learned what was good for them, and
the rest of us have continued to learn. A currency unit becomes
familiar. We get into the habit of calculating the price of every-
thing in terms of this familiar unit. It saves us time and effort
when we can mentally estimate: “Let’s see, I can buy three of
these, but only two of those, or five of yours, or eight of hers.
Which do I want more?”
