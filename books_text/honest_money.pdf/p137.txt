The Politics of Money 133

meaning the green color of the paper, in contrast to gold-backed,
fully redeemable paper money, the traditional gold standard.)

I ‘have several shelves of their poorly written and poorly
printed tracts, stretching back to the 1920’s. They are worthless as
economic documents. All they call for is government-issued un-
backed fiat money, to stimulate business and stabilize prices.
They preach a crude version of the academically popular defenses
of fiat money.

The basic debate boils down to this: Is the State a reliable in-
stitution te control money, or is the unhampered free market? The
“mild” inflationists are always on the side of government-issued
money. So are the “stable prices” advocates. Only those who
believe that market prices should be market prices are advocates of
market-produced money.

Thus, those who favor free-market money do not get a hear-
ing,. not in academic circles, “monetary crank” (greenback) cir-
cles, or political circles. Those who advocate the removal of mon-
etary power from any known special-interest group, especially the
State, find few supporters. Everyone wants /:s group to control
money —“for the good of the nation,” of course.

The American Christian Political Heritage

A century of confusion has misled Christians in the United
States. The last self-consciously Christian President was Presby-
terian Grover Cleveland, who favored a gold standard, low taxes,
free trade, and who vetoed more bills in two terms than any other
President in history. (He had been known as the “veto mayor” of
Buffalo, New York.) He served two terms, 1885-89 and 1893-97.
From that point on, Christian politics slid down the road toward
modern statism.

Another theologically conservative Presbyterian, but a
political and economic radical, William Jennings Bryan
nominated three times to be the Democratic Party’s candidate for
President, and he owed it all to his famous “cross of gold” speech
of 1896. Almost singlehandedly, Bryan converted the Democratic
Party from a gold standard, low-tariff, free-market political party,

was
