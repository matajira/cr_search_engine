56 Honest Money \
nothing (lots -of debased new money jingling in their pockets).

So corrupt wine sellers accommodate corrupt buyers. “Yes, sir,
a brand-new, 100% top-quality item at low, old prices!” Mean-
while, they have poured water into their wine. So do a lot of other
sellers.

The quality of many products starts going down. Prices stay
artificially low, because in principle people are violating the prin-
ciple of honest weights and measures — all through the society.
“Ms, you get ten yards of 100% silk at last year’s low prices.” It’s a
lie. Either the silk isn’t 100% silk, or it’s an inferior quality silk, or
it’s actually seven yards because the seller has substituted a false
measure.

Price Controls

‘But what if the government steps in and tells all the other sell-
ers except the silversmiths to maintain the old standards of quality?
Then either prices will rise, or else the amount consumers can buy ,
at the old prices will be reduced, or else quality will drop.

But what if the government passes a law against raising
prices? This means that sellers can’t cut the amount sold. What
then? Quality will have to drop.

What if the government passes another law, making it illegal
to cut quality? Then many sellers will go out of business, and con-
sumers will not be able to buy all the goods they want.’

Meanwhile, risk-oriented producers will start selling their
goods in the free market, meaning an unregulated market, mean-
ing the black market,

(i hate to use the term “black market.” I prefer to use the term
“alternative zones of supply.”)

If the silversmiths alone are allowed to debase their product
— money— and the government passes laws against price rises or
quality cutting, the law-abiding consumer and the law-abiding
producer will be ruined.

Summary

In short, if there is any tampering with the monetary unit, and
the government allows such fraud to continue, the whole economy
