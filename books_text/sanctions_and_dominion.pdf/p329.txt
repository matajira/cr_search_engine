Land to Match Men’s Skills 291

one had inherited his property in Canaan, they would be al-
lowed to return to their land in Gilead. They agreed.

Moses made a persuasive offer to the other ten tribes: inherit
the land of two and a half tribes in exchange for land outside
the Promised Land that the nine and a haif tribes did not really
want, plus gain the military support of these tribes, or else
(unstated) forfeit this additional land in Canaan and maybe not
get aid from the two and a half tribes. This offer was strong
enough to keep defectors from refusing to invade Canaan, even
though a person, family, or tribe could not be excluded from
inheritance inside Canaan for non-participationin the conquest.
