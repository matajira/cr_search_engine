2

MILITARY PLANNING VS.
CENTRAL PLANNING

Take ye the sum of all the congregation of the children of Israel, after
their families, by the house of their fathers, with the number of their
names, every male by their polls; From twenty years old and upward, all
that are able to go forth to war in Israel: thou and Aaron shall number
them by their armies (Num. 1:2-3).

This commandment was theocentric. God was the military
head of Israel. He gave them victories as well as defeats, as they
would learn after their rejection of the testimony of Joshua and
Caleb (Num. 14:45). He was in no need of a military census.
Why did He require one? The first reason was that He dele-
gates authority to creatures who are not omniscient. They must
find substitutes for omniscience. Number is one of the most
useful substitutes for comprehensive knowledge — an extraordi-
nary tool.! Second, there was the matter of an atonement pay-
ment. This also required a census.

1. Eugene Wigner, “The Unreasonable Effectiveness of Mathematics in the
Natural Sciences,” Communications on Pure and Applied Mathematics, XU (1960), pp.
1-4. Wigner was a Nobel Prize-winning physicist.
