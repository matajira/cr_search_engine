176 SANCTIONS AND DOMINION

survived. The Israelites once again complained to Moses re-
garding the faithlessness of God.

And there was no water for the congregation: and they gathered
themselves together against Moses and against Aaron. And the people
chode with Moses, and spake, saying, Would God that we had died
when our brethren died before the Lorp! And why have ye brought up
the congregation of the Lorp into this wilderness, that we and our
cattle should die there? And wherefore have ye made us to come up
out of Egypt, to bring us in unto this evil place? it is no place of seed,
or of figs, or of vines, or of pomegranates; neither is there any water to
drink (Num. 20:2-5).

Moses and Aaron once again went to God for a solution.
“And Moses and Aaron went from the presence of the assembly
unto the door of the tabernacle of the congregation, and they
fell upon their faces: and the glory of the Lorp appeared unto
them” (Num. 20:6). This time, God promised another deliver-
ance. He would provide water out of a rock a second time. The
first time, shortly after the exodus, God had commanded Moses
to strike a rock with his red. “Behold, I will stand before thee
there upon the rock in Horeb; and thou shalt smite the rock,
and there shail come water out of it, that the people may drink.
And Moses did so in the sight of the elders of Israel” (Ex. 17:6).
This time, however, He commanded Moses only to speak to the
rock in public: “Take the rod, and gather thou the assembly
together, thou, and Aaron thy brother, and speak ye unto the
rock before their eyes; and it shall give forth his water, and
thou shalt bring forth to them water out of the rock: so thou
shalt give the congregation and their beasts drink” (Num. 20:8).

Moses Rebels Against God

Moses took the rod (v. 9), gathered the assembly before the
rock {v. 10), spoke a word of condemnation to them (v. 10),
and struck it twice (v. 11). The water gushed forth, but so did
