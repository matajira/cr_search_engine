298 SANCTIONS AND DOMINION

law apart from the law’s specified sanctions. They are a cov-
enantal unit: law, sanctions, and eschatology. Modern Christian
theology denies this unbreakable covenantal unity. So did the

exodus generation.
