128 SANCTIONS AND DOMINION

them nothing: free grace. They should have responded with
thanksgiving. But they could not swallow the manna contented-
ly because they could not enjoy the blessings of liberty under
God contentedly. They placed liberty at the bottom of their list
of priorities; they placed food at the top. Paul wrote of this
mentality: “Whose end is destruction, whose God is their belly,
and whose glory is in their shame, who mind earthly things”
(Phil. 3:19).

Values and Choice

Modern free market economic theory is individualistic. It
begins with the individual’s goals, his list of priorities. In theo-
ry, there is no way for an economist to speak of a collective list
of priorities because there is no scientific way to make compari-
sons of different people’s value scales.” Nevertheless, such val-
ue scales do exist because men act corporately to achieve corpo-
rate goals. This is what social policy is all about.

The covenantalist begins with God’s covenant, not with the
individual or the collective. God's law provides the value scale.
His ethical standards for individual behavior tell us what to
place where on our personal scale of values. His ethical stan-
dards for corporate behavior tell us what to place where on
society's scale of values. The story of the exodus and the wilder-
ness stands as a warning beacon to men through the ages: the
blessing of liberty under God is ethically preferable to the promise of
security under man. The promise of security under man is a trap
leading into slavery: an illusion.

We make choices in terms of four things: what we want (a
scale of preferences), what we have (capital), how much time we
think we have, and what we know about the relationships
among them (a plan). This decision-making procedure operates

2. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economies, 1987), ch. 4. This summarizes the debate in the 1930's
between Lionel Robbins (an individualist) and Roy Harrod (a Keynesian).
