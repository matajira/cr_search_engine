252 SANCTIONS AND DOMINION

A vow that called down God’s positive sanctions in exchange
for the payment of a future vow is easy to understand. God was
expected to pay in advance. If He did not deliver what the
vow-taker had asked for, the oath was not inaugurated. But the
vow-taker could show thanks to God, and devotion to God, by
taking a vow after a deliverance by God. The crew of the boat
on which Jonah was travelling made such a post-deliverance
vow: “Wherefore they cried unto the Lorp, and said, We be-
seech thee, O Lorn, we beseech thee, let us not perish for this
man’s life, and lay not upon us innocent blood: for thou, O
Lorp, hast done as it pleased thee. So they took up Jonah, and
cast him forth into the sea: and the sea ceased from her raging.
Then the men feared the Lorp exceedingly, and offered a
sacrifice unto the Lorp, and made vows” (Jonah 1:14-16).
These vows were made by men who were not formally cove-
nanted to God — a biblical example of common grace in opera-
tion.

The stranger could lawfully invoke a vow. The priests were
required to participate in the fulfillment of a vow that involved
priestly activities. “Speak unto Aaron, and to his sons, and unto
all the children of Israel, and say unto them, Whatsoever he be
of the house of Israel, or of the strangers in Israel, that will
offer his oblation for all his vows, and for all his freewill offer-
ings, which they will offer unto the Lorn for a burnt offering”
(Lev. 22:18).

New Testament Oaths

Hierarchy is still part of every covenantal institution. Oaths
are still valid. No covenant can be ratified apart from the invo-
cation of the member's oath. The oath is the required means
for a person to establish a lawful judicial claim on the benefits
— positive sanctions — of membership in the covenant institution.
There has to be formal subordination to the covenant in order
for the sanctions to become operative. There is nothing in the
New Testament that indicates that an oath to join a church,
