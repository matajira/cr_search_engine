276 SANCTIONS AND DOMINION

already spreading there, as Rahab testified later to the spies.
“For we have heard how the Lorp dried up the water of the
Red sea for you, when ye came out of Egypt; and what ye did
unto the two kings of the Amorites, that were on the other side
Jordan, Sihon and Og, whom ye utterly destroyed. And as soon
as we had heard these things, our hearts did melt, neither did
there remain any more courage in any man, because of you: for
the Lorp your God, he is God in heaven above, and in earth
beneath” (Josh. 2:10-11). This was surely to the advantage of
Israelite warriors.

Normally, this atonement money would have been paid
prior to the battle at the time of the numbering (Ex. 30:13).
This had not been required because the nation had already
been numbered in preparation for the conquest of Canaan
(Num. 26:2-51). Immediately after this, God revealed a system
for allocating the land (w. 52-56). Because the battle over
Midian was preparatory for the conquest, God did not require
them to be numbered again. They did not pay the half shekel
of silver to the Levites. But when it became clear in retrospect
that they had not really been at risk during the battle, they
decided to forfeit any personal economic gains they had made
as individuals. They concluded: “No pain, no gain.”

Their payment of atonement money was voluntary. It was a
public admission that this had not been a normal military en-
counter. They paid the atonement money, not as a judicial
covering for having killed Midianites, but as an acknowledg-
ment that they had been physically covered by God during the
battle. They did not keep the booty as a personal reward for
having put their own lives at risk when their lives had not been
at risk.

Moses and Eleazar accepted this payment on behalf of God.
“And Moses and Eleazar the priest took the gold of the captains
of thousands and of hundreds, and brought it into the taberna-
cle of the congregation, for a memorial for the children of
Israel before the Lorn” (v. 54). This memorial would remain as
