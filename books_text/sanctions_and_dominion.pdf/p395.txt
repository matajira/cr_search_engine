How Large Was Israel’s Population? 357

We know that the nation had been growing rapidly prior to the
Pharzoh of the persecution (Ex. 1:7). Because of the relatively
small size of the families in Aaron's day, I believe that the bulk
of the adoptions took place prior to the persecution, probably
under Jacob. We do not know what happened to the birth rate
in Moses’ generation, although the examples we have indicate
that there may have been growth. If the persecution and slav-
ery that were specifically designed by the Pharach to slow the
Israelites’ rate of growth actually worked, then Moses’ genera-
tion suffered a reduced rate of incréase compared to the previ-
ous one. We do not know that this was the case. Aaron had a
brother and a sister (Num. 26:59), but he had four sons. There
is no way that 70 biological Israelites could have multiplied to
22,000 firstborn sons in three generations without adoptions.

Let us assume that population was doubling every genera-
tion. Something in the range of 22,000 firstborn sons were
residing in their fathers’ households. Let us assume that each of
them had a brother, although this is probably too high an
estimate for the Passover. Eventually there would have been
two brothers per household: doubling. So, there were about
22,000 fathers in Joshua’s generation, and 11,000 grandfathers,
if all were still alive. So, the adult males of Israel probably
totalled fewer than 40,000, perhaps as low as 33,000.

How did this small group reach 600,000 adult males one
year after the Passover? The solution has to be mass adoption,
sometime after the Passover, or after the exodus, or after the
golden calf incident. This influx also provided the males that
brought Levi's numbers up to 22,000 males — not firstborn™
— at the time of the Numbers 3 numbering.

son. Perhaps he was an Israelite rather than an adoptee. He may have been operat-
ing in terms of an earlier dormnion outlook. Or maybe God just blessed him with a
lot of children.

34. Because the number of Levite firstborn sons was judicially irrelevant in the
substitution process, they were not numbered separately.
