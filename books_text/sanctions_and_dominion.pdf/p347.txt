Cities of Refuge 309

This Jaw is no longer in force. It cannot be; there is no
earthly high priest. Without a mortal high priest, there would
be no way for an innocent man ever to return to his home.
There would be no prospect of full liberty for the innocent
man. This law was operationally annulled with the exile, when
Israel lost jurisdiction over civil justice inside its boundaries. It
was definitively annulled with the death of the true high priest,
Jesus Christ.

Conclusion

The cities of refuge were designed to reduce conflict, espe-
cially family feuds. The initial burden of proof was on the sus-
pect. If he was fleet of foot, he might escape judgment by the
blood avenger. If he could plead his case to the judges of the
city of refuge, he could live. If he subsequently persuaded his
local court, he would have been returned to the city of refuge
that had sheltered him. If he outlived the high priest, he could
lawfully return to his community.

Blood had been shed, and it had to be atoned for. The
judicial issue here was expiation: cleansing bloodguilt for the
land and for the victim’s family. The refuge city system reduced
the number of unsolved cases of murder. This protected the
land. The death of the high priest provided the final expiation
for the original bloody act. This protected the local community
from the heavy costs of family feuds.

To protect themselves, communities had to keep the roads
open. Roads in Israel were both judicial and sacramental: judi-
cial with respect to the cities of refuge; sacramental with respect
to the festivals and required sacrifices at the central city. There
is no biblical evidence that State highway construction was
justified on the basis of its positive economic effects, although
the roads surely had positive economic effects. What the roads
were intended to provide was access to justice and access to
expiation. Both justice and expiation were geographically
based. “Speak ye comfortably to Jerusalem, and cry unto her,
