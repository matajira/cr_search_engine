Cities of Refuge 303

refuge took him. The avenger of blood — the go’el or ga’al~ had
the authority to execute the suspect on sight prior to his arrival
in a city of refuge.

Blood Avenger, Kinsman-Redeemer

Mosaic law devotes considerable space to this family office.
The kinsman-redeemer was the closest relative. Boaz was Nao-
mi’s kinsman-redeemer (Ruth 2:20). God is spoken of as Israel’s
kinsman-redeemer. “Thus saith the Lorp, your redeemer, the
Holy One of Israel; For your sake I have sent to Babylon, and
have brought down all their nobles, and the Chaldeans, whose
cry is in the ships” (Isa. 43:14). David spoke of God as his kins-
man-redeemer: “Let the words of my mouth, and the medita-
tion of my heart, be acceptable in thy sight, O Lorp, my
strength, and my redeemer” (Ps. 19:14). Job viewed God as the
final judge: “For I know that my redeemer liveth, and that he
shall stand at the latter day upon the earth” (Job 19:25).

Yet this redeemer, this deliverer, was also the judge who had
the authority to execute a man who had killed the redeemer’s
nearest of kin. Until the man arrived in a city of refuge, the
blood avenger served as jury, judge, and executioner. In Israel,
where a death was involved, there was swift justice. This sanc-
tion was permanent.

Sanctions and Incentives

The death penalty is the limit of any civil sanction. The State
is to be neither bloodthirsty, imposing torture or mass execu-
tions, nor soft-hearted, imposing a lesser sanction. The incen-
tive either to exceed or avoid imposing God’s law is always very
great. The Mosaic law had two major methods of assigning the
responsibility for executing the capital sanction: shared respon-
sibility and unitary responsibility. Shared responsibility removed
the full responsibility from the citizen. The method of execu-
tion, stoning, made it impossible for any participant to know if
