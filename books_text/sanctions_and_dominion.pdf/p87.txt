Military Planning vs. Central Planning 49

repeatedly conclude that flipping a coin would be as accurate
(perhaps more accurate) as the forecasts of government econo-
mists; so would making the simple assumption that this ycar
will be pretty much the same as last year.

Faith in the power of statistics to convey relevant economic
information to government planners is visible in a statement by
Eugene Rostow. He assumes that it is an altruistic civil govern-
ment, not the profit-sceking decisions of consumers and pro-
ducers in a free market, which is truly rational. Government
planners alone can see the big picture and rationally guide the
overall economy for the benefit of others, or so we are told.
“The policy of maintaining high levels of employment therefore
implies a policy of seeking to make the current output of the
economy a maximum - that is, to obtain as valuable a yield as
possible from the intelligent current use of the nation’s capital
resources, and its inheritance of capital, organization, skill, and
habit. This goal is the first economic problem of any responsible
government.”"* This faith lodges initiatory and final economic
sovereignty in the State, and in those who are ordained by the
State to conduct its planning activities.

Tn contrast to the god of socialism, this god of the mixed
economy is not an earthly version of Calvin’s predestinating
God, but it is surely an immanent Arminian kind of god. It
docs not predestinate, but it makes incentives available to those
who conform to its laws. It nudges history along its orderly
path. But a blind god is not much of a god (Deut. 4:28), so this
immanent god must be said to be able to see clearly. He must
be given eyes. Samuel Ruggles, the American delegate to the
International Statistical Congress of 1863, was an early prophet
of the statistical millennium: “Statistics are the very eyes of the
statesman, enabling him to survey and scan with clear and
comprehensive vision the whole structure and economy of the

 

16. Eugene ¥, Rostow, Planning for Freedom: The Public Law of American Capitalism
(New Haven, Connecticut: Yale University Press, 1959), p- 69.
