MUSTERING THE ARMY OF THE LORD

And the Lorn spake unto Moses in the wilderness of Sinai, in the
tabernacle of the congregation, on the first day of the second month, in
the second year after they were come out of the land of Egypt, saying,
Take ye the sum of all the congregation of the children of Israel, afier
their families, by the house of their fathers, with the number of their
names, every male by their polls; From kventy years old and upward, all
that are able to go forth to war in Israel: thou and Aaron shall number
them. by their armies, And with you there shall be a man of every tribe;
every one head of the house of his fathers (Num. 1:1-4).

God told Moses to number the fighting men of Israel. ‘This
message came to Moses in the wilderness. “In the wilderness”
(bemidbar) was the original Hebrew title of the fourth book of
the Pentateuch. The title of this book in the Septuagint, the
Greek translation of the Hebrew Scriptures (275 to 100 B.C.),
was Arithmoi,’ from which we get the English word, arithmetic.
The English title, Numbers, is related to arithmoi. The book
begins with God’s command to number the people, This was a
military numbering, ie. a mustering of troops? Every man

L. Jacob Milgrom, The JPS Torah Commentary: Numbers (New York: Jewish Publi-
cation Society, 1990), p. xi.
2. Milgrom, Excursus 2. “The Census and Its Totals,” ibid., p. 336.
