26 SANCTIONS AND DOMINION

extent that they were mentally bound to a time horizon no
longer than their own lifetimes, they became nomads.

These nomads were not ready to fight a war with the Ca-
naanites. Their pilgrim children would be. The pilgrim wants
rest in the place of his dreams. His life’s walk is linear even
though he may wander in circles for a time. He has a goal, so
the fact of his circular wandering is not a disaster. He knows
that he will eventually break out of his familiar pattern.

The stagnation of Israel’s population matched the stagnation
of vision of the exodus generation. Present-oriented people
discount the future at a higher interest rate than future-orien-
ted people do. The present value of a future achievement is
lower for a present-oriented person than for a future-oriented
person. The estimated payoff for thrift and sacrifice in the pres-
ent is lower. The present-oriented person wants immediate
gratification. The conquest generation had no choice but to
defer their gratification. They were under the authority of
present-oriented people. Their deliverance was still in the fu-
ture; their parents’ deliverance had been in the past. In the
interim, the nation stagnated.

For those members of the conquest generation who longed
for deliverance from the stagnation of the wilderness, God's
curse was real. But it offered hope. In the fourth generation,
they would gain their promised inheritance. In the meantime,
the governing principle of their existence was the army’s rule:
“Hurry up and wait.”

Conclusion

The three musterings of Israel were military actions. They
were aspects of holy warfare. The mustered Israelites had to
pay the priests atonement moncy in preparation for the shed-
ding of blood. This bloodshed was covenantal: an aspect of
God’s negative sanctions against covenant-breaking nations. The
Exodus mustering probably was a retroactive payment for the
Levites’ sanctions in God's name and the nation’s name against
