Blessing and Naming 105

name of sanctions in general following sins in general, but they
are not given accurate insights into the future. We see the
future as through a glass darkly today (I Gor. 13:12). Because
no one can legitimately claim the lawful prophetic authority to
direct the imposition of negative civil sanctions under the New
Covenant, which was not the case in Elijah’s day, the Mosaic
Covenant’s negative civil sanctions against false prophets no
longer apply. Because the Old Covenant office of prophet has
been annulled with the completed text of the Bible, the civil
laws governing false prophecy have also been annulled. Excom-
munication by the church has replaced execution by the State
in the matter of false prophecy. If there were still prophets
among us, then we would still need the negative sanction of
capital punishment to protect society from false prophecy.
Under the Mosaic Covenant, God’s sanctions visibly followed
the spoken word of a true prophet. This was the basis of his
authority to demand the imposition of civil sanctions: to deflect
God’s corporate negative sanctions. (The same justification
undergirds civil sanctions everywhere, in every era.) A false
prophet was under the threat of execution: his invoked heaven-
ly sanctions might not come to pass. He who claimed the au-
thority to invoke heavenly sanctions also could invoke civil
sanctions; he was therefore under these sanctions. As a false
prophet, he was also a false witness. The penalty for being a
false prophetic witness in God’s name was death, for such testi-
mony invoked God’s name in an evil cause: a violation of the
third commandment (Ex. 20:7). If there had been no covenan-
tal correlation between sanctions invoked and sanctions per-
ceived, the Mosaic civil law governing the false prophet (Deut.
18:20-22) would not have been enforceable. Guilt or innocence
was determined by the presence of cause and effect: verbal
cause followed by visible effect. This is no longer the case in the
New Covenant because the New Covenant has not retained the
covenantal connection between heavenly sanctions invoked and
heavenly sanctions imposed. This is because covenantally au-
