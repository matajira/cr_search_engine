The Firstborn and Negative Economic Sanctions 99

To save the firstborn son’s double inheritance after the golden
calf incident, each family had to redeem the firstborn son with
a payment of five shekels to the Levites. Without this, the first-
born son forfeited his inheritance in Israel: a mark of God’s
curse on the family. This law was imposed only after the con-
struction of the tabernacle. This sanctified the Levites as perma-
nent substitutes for the firstborn sons.

The specification of five shekels of silver paid to the Levites
as the Levitical adoption entry price, as well as the redemption
price for firstborn sons, indicates that the shekel of the sanctu-
ary was to serve as a standard for Israel's money payment sys-
tem. The five shekels referred to silver. Any debasing of the
currency would be detected. The shekel of the sanctuary was to
remain a monetary standard that was free from political con-
trol. This unit of exchange was not to be tampered with by the
priests; thus, it would condemn any other currency that was
called a shekel but which did not contain 20 gerahs of silver.

The New Covenant has substituted baptism for circumcision.
There is no longer any difference between sons and daughters
with respect to their required subjection to a covenant sign.
The annulment of Passover and the transfer of covenantal
sanctions to the Lord’s Supper ended the judicial discontinuity
in Israel’s history that Passover imposed on Israel. This law was
unique to Mosaic Israel. The unique covenantal threat to first-
born sons no longer exists.
