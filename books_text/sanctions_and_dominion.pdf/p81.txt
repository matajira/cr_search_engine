Military Planning vs. Central Planning 43

And Satan stood up against Israel, and provoked David to number
Israel. And David said to Joab and to the rulers of the people, Go,
number Israel from Beer-sheba even to Dan; and bring the number of
them to me, that I may know it. And Joab answered, The Lorp make
his people an hundred times so many more as they be: but, my lord
the king, are they not all my lord’s servants? why then doth my lord
require this thing? why will he be a cause of trespass to Israel? Never-
theless the king’s word prevailed against Joab, Wherefore Joab depart-
ed, and went throughout all Israel, and came to Jerusalem (1 Chron.
21:14).

I have reproduced both passages in full so there can be no
doubt: they describe the same incident.

Joab falsified his report by refusing to number the tribes of
Benjamin and Levi (I Chron. 21:5-6). No military agent of the
nation was ever allowed to number Levi (Num. 1:49). By refus-
ing to muster Benjamin, Saul’s tribe, the smallest tribe in Israel
(I Sam. 9:21), Joab made certain that the mustering was not of
the entire nation. By not mustering all of the non-priestly
tribes, Joab silently declared that this was not a holy war, for
the priesthood had not authorized it by blowing the twin trum-
pets, nor had the entire nation been mustered.

10. May we legitimately say that Satan and his followers sometimes do God's
work of deception? The Bible repeatedly allirms that this is the case. “And the Lorn
said, Who shall entice Ahab king of [srael, that he may go up and fal at Ramoth-
gilead? And one spake saying afier this manney and another saying after that
manner Then there came out a spirit, anid stood before the Lorv, and said, 1 will
entice him. And the Lop said unto him, Wherewith? And be said, I will go out, and
be a lying spirit in the mouth of all his prophets. And the Lorp said, Thou shait
entice him, and thou shalt aiso prevail: go out, and do even so. Now therefore,
behold, the Lorp hath puca lying spirit in the mouth of these thy prophets, and the
Lorp hath spoken. evil against thee” (IT Chron. 18:19-22). “And for this cause God
shall send them strong delusion, that they should believe a lic: That they all night be
damned who believed not the eruth, but had pleasure in unrighteousness” (II Thess,
911-19). Nevertheless, we must not blame God for what God uses Satan to do. This
is iMogical by the standards of autonomous man (meaning also the standards of
Gordon Clark), but it is required that we hold both positions — God’s absolute
sovereignty and Satan's full responsibility - in order (o avoid making God the author
of sit. On this point, see Romans 9:14-23,
