The Hierarchy of Service/Sanctions 77

viet hierarchy, established by force,” collapsed. Or better put,
its members found new ways of imposing force and surviving,
but without the ideology of international Communism to justify
the use of force.

The Inevitability of Hierarchy

Hierarchy is an inescapable concept, built into the creation
through the dominion covenant: man’s exercise of dominion
over the creation. God acts through intermediaries. The hus-
band exercises headship over his wife and children, for whom
he is responsible. The general exercises authority over his
troops, for whom he is responsible. Hierarchy is an outworking
of representative responsibility: God holds leaders more respon-
sible than followers for the outcome of events, even though
followers, corporately, are the intermediate source of a leader’s
authority. Authority is distributed by God from the top down
and from the bottom up simultaneously.” Nevertheless, God
holds leaders more responsible than followers: with greater
authority comes greater responsibility (Luke 12:47~48).

Hierarchy in Mosaic Israel was based on the sacrosanct yet
limited extent of the sacred. The sacred did not encompass
everything in Israel; on the contrary, it encompassed very little,
and the narrow boundaries of sacred space were evidence of
this. While ethical transgression was common, necessitating
sacred sacrifices, profaning sacred boundaries was rare, for
profanation was frequently fatal. It was rarely repeated by the
same person. Fear of sacred space was widespread.

The fixed hierarchical boundaries of the Levites were tied to
the fixed concentric structure of sacred boundaries. When these
boundaries were annulled by the New Covenant, the old tribal

24. Michael Voslensky, Nomenklatura: The Soviet Ruling Class (Garden City, New
York: Doubleday, 1984); Davis K. Willis, KLASS: How Russians Really Line (New York:
St. Martin’s, 1985),

25. North, Leviticus, ch. 4
