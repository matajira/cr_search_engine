How Large Was Israel's Population? 365

Conclusion

Here are the three problems that are raised by this passage:
1) how the 27-to-one ratio between adult males and firstborn
sons could have existed biologically; 2) how a relatively small
population of sons became the 600,000 fighting males that
invaded Canaan; 3) how Israel grew to 600,000 adult maies in
three generations.

The third problem can be answered in only one way:
through adoption into the nation of Israel. Because the theme
of adoption is so central to the issue of God’s kingdom in histo-
ry, I used this theological model to approach the other two
problems. I asked: Could the 27-to-one ratio have had some-
thing to do with the relationship between biologically firstborn
sons and newly adopted adults? Second, could the disparity
between the birth rates of the Passover’s Israelites and the
newly adopted gentiles explain the seemingly overnight appear-
ance of zero population growth during the wilderness era?

Something took place in Israel’s wilderness experience which
reversed the high population growth rates that had prevailed
since their descent into Egypt. I suggest the following: God's
imposition of low birth rates on rebels. My solution to the echo
problem does not require the death of fourth generation mem-
bers who were born prior to the exodus. It also allows high
birth rates for Joshua’s generation: more than firstborn sons.

Jordan’s solution, that the firstborn sons were under age
five, suffers from the problem of redefining the meaning of
firstborn. The evidence for five years or younger comes from the
adoption price of Leviticus 27:6. It is indirect, at best. This
solution creates problems regarding the birth rates of Joshua’s
generation in Egypt: either below the replacement rate or
skewered very strangely during the last four years in Egypt.

Keil’s thesis of the numbered firstborn as only those born
after the exodus, if coupled with some variant of the early
adoption scenario, is plausible, but only at the expense of radi-
