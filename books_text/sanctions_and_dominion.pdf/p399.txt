How Large Was Israel’s Population? 361

oppression, two generations earlier. For growth to have taken
place, there would have been fewer than 22,000 men when the
oppression began. The faster the growth, the fewer the men. I
believe there was growth: the blessing of God. This was the
testimony of the Israelite midwives. What threat would fewer
than 22,000 men have posed to the Pharaoh of the oppression?
Perhaps he was looking ahead at what might be if he refused to
restrict their growth, but in a society that had enough slaves to
construct the pyramids, there would not have been a great
threat from 10,000 men, let alone 5,000.

My theory of mass adoption and a subsequent tribal mem-
bership re-distribution to the Levites is textually speculative.
The texts say nothing about either event. But something like
this is consistent with the tribal population numbers recorded
so exactly and repeatedly in the texts. The adoption scenario
may seem far-fetched. But adoption into Israel had been going
on from the time of the descent into Egypt, and maybe earlier.

The Mixed Multitude at Passover

There is another important question: Did the firstborn sons
of the mixed multitude die on Passover night? Not necessarily.
The confrontation was between the God of Israel and the gods
of Egypt, who were represented judicially by Pharaoh. Gentile
slaves and other residents.of Egypt are not mentioned prior to
the exodus itself (Ex. 12:38). God had not brought a covenant
lawsuit against them.

Let us consider three possible scenarios. First, it may be that
God spared their sons without blood on their doorposts. The
Bible does not say that the homes of the mixed multitude were
visited with death; only the homes of the Egyptians: “. . . It is
the sacrifice of the Lorn’s passover, who passed over the houses
of the children of Israel in Egypt, when he smote the Egyp-
tians, and delivered our houses. . . .” (Ex. 12:27). The compre-
hensive language of Exodus 12:29-30 may indicate otherwise,
but Exodus 12:27 may be the dominant theme: Israel vs. Egypt.
