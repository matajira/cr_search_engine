How Large Was Israel's Population? 345

aoh could have been biologically a firstborn. His resident son
died in his place.

Some Egyptian households would. have seen their firstborn
sons depart years before. Yet every household had a death. If
my view is correct, then those firstborn fathers whose firstborn
sons had left home came under the sanction of death. I con-
clude this because: 1) every household had a death; 2) there
were still many adult males left alive in Egypt. This indicates
that not every firstborn son died. Those who still lived in their
fathers’ households did die. For a firstborn son who had left
home, and who had a son of his own who died, there also was
a death back home: the firstborn head the household, i.e., his
firstborn father. There was a death in every household.”* Only
two things could save a firstborn father: the death of his
resident firstborn son or the death of a lamb.

Was the threat against Israel the same? Was every male at
risk, or were only their sons at risk? The text seems to indicate
that the same threat applied to the Israelites that applied to the
Egyptians. I conclude that the definition of the term firstborn
applied equally to both nations: the firstborn son in a house-
hold.

We now return to the problem of the 27-to-one ratio be-
tween adult males and firstborn sons.

First Proposed Solution: Firstborn as Young Minors

James Jordan asks us to assume that firstborn sons were
young minors in a household, i-e., that only minors under the
age of five were counted as firstborn sons.” Why should we
assume this? Why should we assume that they could have had

24. No younger brothers headed households in which their firstborn sons had
departed. If every household was headed by a firstborn son, then Egypt was in
replacement-rate mode: zero population growth.

25. James Jordan: “Who Were the Firstborn Sons?” Biblical Horizons, No. 73
(May 1998), p. 4.

 
