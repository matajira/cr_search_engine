94 SANCTIONS AND DOMINION

What was the reason for the link between a redemption pay-
ment for donkeys and men?

First, consider the fact that the donkey had to be killed in a
special way: a broken neck. No other sacrifice in the Mosaic
Covenant was by broken neck. What was the relevance of a
broken neck? Jt was execution without bloodshed. Every other ani-
mal sacrifice involved the knife. The sacrificed animal’s blood
was used in the ceremony as a sign of atonement. But this
animal sacrifice was unique: no blood. It was therefore not a
means of atonement. It was a sacrifice strictly in the sense of an
economic loss.

Second, this law was given prior to the Mosaic dietary laws.
But there was an earlier dietary law in force. If an animal re-
mained in a bloodless condition, its carcass became valueless; its
flesh could not be eaten. No animal could be eaten lawfully with its
blood still in it. The context of this dietary law was God’s cove-
nant with Noah - specifically, the provision dealing with the
killing of men. “But flesh with the life thereof, which is the
blood thereof, shall ye not eat. And surely your blood of your
lives will I require; at the hand of every beast will I require it,
and at the hand of man; at the hand of every man’s brother
will I require the life of man. Whoso sheddeth man’s blood, by
man shall his blood be shed: for in the image of God made he
man” (Gen, 9:4-6). So, the sacrifice of the firstborn donkey was
exclusively a negative economic sanction. Neither the owner
nor the priest could eat it or use its carcass if it remained in an
uncut state. The implication was that it could not be skinned
after its death, for it was to be killed with its blood intact.

Third, the substitute for the firstborn donkey was a lamb.
This is the only case in the Mosaic law of an animal’s substituting for
the firstborn. Clean animals had to be slain; firstborn sons had to
be redeemed by a payment of five shekels; unclean animals had
to be redeemed by a money payment of its market value plus
one-fifth - the payment associated with the Levitical redemp-
