Blessing and Naming 101

them how to attain it: not by avoiding the conquest of Canaan
but by avoiding evil. They viewed peace as the absence of nega-
tive sanctions. This was wrong. Peace is the extension of God's
kingdom in history. “For unto us a child is born, unto us a son is
given: and the government shall be upon his shoulder: and his
name shall be called Wonderful, Counsellor, The mighty God,
The everlasting Father, The Prince of Peace. Of the increase of
his government and peace there shall be no end, upon the
throne of David, and upon his kingdom, to order it, and to
establish it with judgment and with justice from henceforth
even for ever. The zeal of the Lorp of hosts will perform this”
(Isa. 9:6-7). Peace therefore necessarily involves the imposition of
negative sanctions on evil: Satan’s kingdom of man.

Peace with God comes through covenantal faithfulness. By
explicitly invoking positive sanctions, the priests were implicitly
also invoking negative sanctions. Presumably, the main one was
war: the absence of peace for God’s enemies. There is no es-
cape from the two-fold nature of God’s covenantal sanctions. To
place yourself under His blessings, you must also place yourself
under His cursings. Both blessing and cursing come in terms of
His law (Lev. 26; Deut. 28).

The sanction of peace was visible. It was important for this
sanction to be universally respected and sought after, for it was
to serve aS a testimony to pagan nations. The Psalmist wrote:
“God be merciful unto us, and bless us; and cause his face to
shine upon us; Selah, That thy way may be known upon earth,
thy saving health among all nations. Let the people praise thee,
O God; let all the people praise thee. O let the nations be glad
and sing for joy: for thou shalt judge the people righteously,
and govern the nations upon earth. Selah” (Ps. 67:1-4). The
positive covenantal sanction - “saving health” or “salvation”
(yeshuio‘ah) - was to remind men of the reality of God’s cov-
enantal stipulations: “thy way.” God judges the nations of the
