10

TITHE AND SANCTIONS

And the Lorn said unte Aaron, Thou and thy suns and thy father’s
house with thee shall bear the iniquity of the sanctuary: and thou and thy
sons with thee shall bear the iniquity of your priesthood. And thy brethren
also of the tribe of Levi, the tribe of thy father, bring thou with thee, that
they may be joined unto thee, and minister unto thee: but thou and thy
sons with thee shall minister before the tabernacle of witness. And they
shall keep thy charge, and the charge of all the tabernacle: only they shall
not come nigh the vessels of the sanctuary and the altay, that neither
they, nor ye also, die. And they shall be joined unto thee, and keep the
charge of the tabernacle of the congregation, for all the service of the
tabernacle: and a stranger shall not come nigh unto you (Num. 18:1-4).

The theocentric principle here is God’s holiness and man’s
responsibility. God. is set apart from man: holiness.’ This re-
quired the creation of a series of concentric circles of holiness
around the Ark of the Covenant, where God dwelt with Israel.
Someone had to take personal responsibility for guarding?
these zones of holiness from trespassers whose very presence
would profane the sacred space. So, God transferred to the sons
of Aaron the responsibility of guarding the sanctuary. Because

1. Point three: ethics.
2. Point four: sanctions.
