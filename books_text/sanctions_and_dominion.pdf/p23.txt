Preface xxii

eighteenth century, this supposedly set in historical concrete
humanism’s position as the reigning covenant-breaking social
order. Any attempt to re-conquer culture for Christ is heretical,
we are assured.

It is true that sixteenth-century Calvinists were hostile to the
idea that the gospel would eventually convert most of mankind.
On this point, they adhered strictly to the dominant tradition of
Roman Catholic eschatology. Calvin himself was ambivalent on
the issue; there were elements of what would later become
postmillennialism in his thinking.** The others were outright
hostile. (So were the Lutherans.) But Calvinists also believed
that Protestant Christians, although a permanent minority
group worldwide, had the right and moral obligation to defend
their local majority positions in sections of Northern Europe by
means of the sword. They were all theocrats in the traditional
meaning of the word. They believed in the imposition of civil
sanctions in the name of Jesus Christ and His earthly kingdom.

Not so today. Their spiritual heirs, as Enlightenment plural-
ists, have abandoned sixteenth-century Calvinism’s theocratic
ideal, but not its amillennialism. Today, Christians are in the
minority everywhere. So it must stay forever, announce the
theologians of the Protestant ghetto. So it was always intended
to be. Writes Protestant Reformed Church theologian-editor,
David J. Engelsma: “The ungodly always dominate. The world’s
rulers always condemn the cause of the true church. The wick-
ed always oppress the saints. The only hope of the church in
the world, and their full deliverance, is the Second Coming of
Christ and the final judgment. This is Reformed doctrine,””
On the contrary, this is merely ghetto theology’s doctrine.

80. North, Millennialism and Social Theory, Appendix D: “Calvin's Millennial
Confession.” Gf. Gary North, “The Economic Thought of Luther and Galvin,” journal
of Christian Reconstruction, Ik (Summer 1978), pp. 104-106.

81. Editorial, “A Defense of (Reformed) Amillennialism. 3. Apostasy and Persecu-
tion,” The Standard Bearer (May 1, 1995), p. 365.
