Progressive Whining and Final Accounting 137

are there before you, and ye shall fall by the sword: because ye
are turned away from the Lorp, therefore the Lorp will not be
with you” (vv. 41-43). They refused to listen. They attacked
Amalekites and Canaanites who lived in the area. They were
soundly defeated (vv. 43-45).

Israel believed the reports it wanted to hear. When the
people heard of the good land filled with big people, they
discounted the message of the good land. The ‘spies then re-
vised their initial report: bad land, big people. When God told
them they were not able to win a relatively minor battle, they
refused to believe Him. They trusted in themselves. They did
not trust themselves enough to defeat Canaan. They did trust
themselves enough to defeat Amalek. Had they listened to
Joshua and Caleb, they would not have had to bother with
Amalek; they would have marched straight into Canaan. They
lost a minor battle, yet they could have won a major battle. The
issue was their faith in God’s word, not comparative size of
armies. Faith in God’s word is what they lacked.

Conclusion

Evaluation is an aspect of point four of the biblical covenant:
sanctions/judgment. The people were supposed to evaluate
their situation in terms of God’s word, as confirmed by His acts
of deliverance in recent history. They refused to believe God’s
word; therefore, they evaluated their situation incorrectly. They
interpreted the historical facts in terms of covenant-breaking
standards. They did not use God’s word as their standard of
evaluation. God brought corporate judgments against them
because of this open disbelief.

The Israelites had fallen into a bad habit: whining to Moses
to get what they wanted from God. By God's grace, this whin-
ing initially resulted in positive sanctions. The problem was,
they did not recognize that it was God’s grace, not their whin-
ing, that had gained them the objects of their desires. Their
whining was a public testimony of their lack of faith in God's
