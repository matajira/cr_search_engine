Tithe and Sanctions 171

ments in exchange for dollars (1933), the United States no
longer needs to levy taxes for revenue purposes. With the
power to create money, he said, the ultimate revenue source is
the nation’s central bank. Taxes have become means of redis-
tributing wealth among groups and to express public policy “in
subsidizing or in penalizing various industries and. groups.”!7
“The second principal purpose of federal taxes is to attain more
equality of wealth and of income than would result from eco-
nomic forces working alone. The taxes which are effective for
this purpose are the progressive individual income tax, the
progressive estate tax, and the gift tax. What these taxes should
be depends on public policy with respect to the distribution of
wealth and of income.”'* The mechanism of taxation had be-
come an important means of pursuing national policy objec-
tives, not a means of providing the State with revenue. Put
differently, the inflation tax of fiat money creation by the cen-
tral bank had become the revenue source; other forms of State
taxation were means of wealth redistribution. His opinion has
not been shared by many officials, but in theory, the distinction
could be applied. In practice, the central bank’s inflation tax
and other forms of State taxation are.used as both revenue
sources and as social policy tools.

Such a view of the State transforms politics. Politics becomes
the politics of plunder rather than the defense of individual
liberty, the defense of property rights, and protection against
violence. The civil government moves from being an agency
that imposes negative sanctions against criminals on behalf of
their victims'* to an agency that provides positive economic
sanctions to favored members of the community” at the ex-

17. Beardsley Ruml, “Taxes for Revenue Are Obsolete,” American Affairs, 8 (Jan.
1946), p. 36,
18, Idem.

19. Gary North, Victin’s Rights: The Biblical View of Civil fustice (Tyler, Texas:
Institute for Christian Economies, 1990),

20. Or outside the community, e.g., State-to-State foreign aid.
