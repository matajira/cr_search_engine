246 SANCTIONS AND DOMINION

sign.” Females are baptized. They are allowed to take an oath
of allegiance to God through membership in His church with-
out asking permission from their husbands or fathers. In fact,
they are required to make such a covenant in defiance of this
authority. “Think not that I am come to send peace on earth:
I came not to send peace, but a sword. For I am come to set a
man at variance against his father, and the daughter against
her mother, and the daughter in law against her mother in law.
And a man’s foes shall be they of his own household. He that
loveth father or mother more than me is not worthy of me: and
he that loveth son or daughter more than me is not worthy of
me” (Matt. 10:34-37).

Has a woman’s access to baptism changed anything in family
authority? The New Testament does not indicate any major
change in the family other than the elimination of the Mosaic
law’s easy divorce (Deut. 24:1; Matt. 5:31-32). The husband is
still the head of his household (I Tim. 3). Yet the story of Jael
indicates that a wife could lawfully break the vow of her hus-
band when his vow was illegitimate. Her husband had sworn
peace with Sisera (Judg. 4:17). She killed Sisera anyway. She
did this under the general authority of Deborah, who was a
national judge in Israel and under whose leadership the Israel-
ite army was bringing sanctions against Sisera. Jael did not
violate her husband’s oath on her own authority; she did so
under a superior covenantal civil authority. She respected the
civil hierarchy by disobeying a subordinate hierarchy. Her
rebellion against her husband was part of Israel’s rebellion
against Sisera, and is so celebrated in Deborah's song. She was
promoted in honor among women. “Blessed above women shall
Jael the wife of Heber the Kenite be, blessed shall she be above
women in the tent” (Judg. 5:24).

2. Meredith G. Kline, By Oath Consigned: A Reinterpretation of the Covenant Signs of
Circumcision and Baptism (Grand Rapids, Michigan: Eerdmans, 1968), ch. 3.
