The Firstborn and Negative Economic Sanctions 91

was in part an atoning sacrifice and in part a defense of God’s
name.

They gained a unique inheritance because of this. They
would henceforth receive redemption money from the other
tribes, This substitute payment benefitted the other tribes, who
were released from an obligation that would otherwise have
bound firstborn sons. The theological debate is over what that
obligation was, as we shall see. What is clear is that the Levites
did not pay this redemption money to themselves or to the
priests, since they had been chosen by Gad to serve Him as
representatives of the nation. They did not make a substitute
payment, for they had become the substitute payment.

Firstborn Sons as Sacrifices

The Levites became the priestly tribe after the golden calf
incident. They also became substitutes for the firstborn sons.
The question is: Why did the firstborn sons need substitutes?
The traditional Jewish answer is that firstborn sons would have
had to become priests if. the Levites had not replaced them.
Rashi,* the eleventh-century commentator, argued along these
lines.* Samson Raphael Hirsch, the intellectual founder of what
in his day came to be called Orthodox Judaism, wrote in the
nineteenth century: “By the transference of the service of the
Sanctuary from the firstborn to the Levites, the consecration of
the firstborn is not removed. They remain, unaffected by the
transference, consecrated to God.”*

The problem with this argument is that the context does not
indicate sacrifice in the sense of personal service to God. The
context indicates sacrifice in the sense of execution. The first-

4. Rabbi Solomon (Shlomo) Yizchaki.

5. Rashi, Chumash with Targum Ontelos, Haphtaroth and Rashi’s Commentary, A. M.
Silbermann and M. Rosenbaum, translators, 5 vols. (Jerusalem: Silbermann Family,
{1984] 1985 [Jewish year: 8745), IV, p. 11 (Num. 3:12-15).

6. Samson Raphael Hirsch, The Pentateuch, 5 vols. (Gateshead, London: Judaica
Press, [18752] 1989), IV, Numbers, p. 29 (Num. 8:13).
