12

THE PSYCHOLOGY OF VICTORY

Wherefore it is said in the book of the wars of the Lorp, What he did
tn the Red sea, and in the brooks of Arnon (Num. 21:14).

The book of the wars of the Lord is one of several missing
books that are referred to in the Bible. These include the book
of Jasher (Josh. 10:13; If Sam. 1:18), the book of the acts of
Solomon (I Ki. 11:41), the books of Samuel the seer, Nathan
the prophet, and Gad the seer (I Chron. 29:29), the prophecies
of Ahijah the Shilonite and Iddo the seer (II Chron, 9:29), the
book of Shemaiah the prophet (II Chron, 12:15), and the chron-
icles of the kings of the Medes and Persians (Esth. 10:2). This
is the first biblical reference to a now-missing book.

The text informs us that these wilderness wars began at the
border of the wilderness, the Red Sea, where God destroyed
the army of Pharaoh. The missing chronicles continued at least
until the war listed in this chapter: the war at Arnon, the bor-
der of Sihon’s kingdom. The victories associated with the defeat
of Sihon became part of Israel’s folk heritage: proverbs of des-
truction. “Wherefore they that speak in proverbs say, Come
into Heshbon, let the city of Sihon be built and prepared: For
there is a fire gone out of Heshbon, a flame from the city of
