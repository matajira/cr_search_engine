The Lure of Magic 179

proclivity to rebel. This structure of prophecy is an aspect of
God’s grace. Grace tends toward blessings in history."

Moses should have believed God’s positive promise uncondi-
tionally, but he did not. He did not believe that his mere speak-
ing to the rock would result in a flow of water. God did not tell
him to tap the rock. God told him only to take the rod, assem-
ble the people, and speak before the rock. There was a re-
quired plan, but it did not involve striking the rock. There was
visible cause and effect, but it did not involve striking the rock.
The system of cause and effect was supernatural. Speaking to a
rock in public was surely not the normal way to provide large
quantities of water in a wilderness. To speak water out of a rock
is surely a miracle worth recording. But Moses did not believe
God to this degree. He wanted two intermediate steps: striking
the rock twice.

In Genesis 1, God spoke the world into existence. There was
no intermediary causation. God spoke; events consistent with
His word immediately took place. The time interval in each
step was less than one day. There was no uniformitarian conti-
nuity of time, with events taking place slowly in a cosmic pro-
cess of evolutionary development. God spoke the world into
existence. His word was sufficient.

Men do not normally possess such a word of authority. They
speak, but only those who hear and understand can bring to
pass what is spoken. The tower of Babel is the consummate
example. When a breakdown occurred in corporate communi-
cation, men ceased to build. Without the cause and effect of the
continuity of speech, men cannot work together. The division
of labor breaks down.

When a man who possesses authority speaks, his words
initiate a process of cause and effect. But man’s words have no
effect directly on inanimate objects. Mind over matter is God’s

1. This is an argument against both amillennialism and premillennialism. This
argument rests on the covenantal connection between sanctions and inheritance.
