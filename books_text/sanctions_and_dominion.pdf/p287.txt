Oath and Sanctions 249

do so. When they do, they will gain authority over the Chris-
tians for a time. God brings positive sanctions to those who
uphold His law, even at the expense of His covenant people's
authority. While covenant-breakers cannot indefinitely uphold
God's law, since it testifies against them, during the period
when they do uphold it, they gain external blessings.

Paying Off a Vow

The payment of a vow could not be made out of a person’s
second-best capital, unlike a voluntary offering to which no
promise was attached. “And whosoever offereth a sacrifice of
peace offerings unto the Lorn to accomplish his vow, or a free-
will offering in beeves or sheep, it shall be perfect to be accept-
ed; there shall be no blemish therein. Blind, or broken, or
maimed, or having a wen, or scurvy, or scabbed, ye shall not
offer these unto the Lorn, nor make an offering by fire of them
upon the altar unto the Lorp. Either a bullock or a lamb that
hath any thing superfluous or lacking in his parts, that mayest
thou offer for a freewill offering; but for a vow it shall not be
accepted” (Lev. 22:21-23). A person could lawfully give a priest
an animal that was not suitable for the altar, but when it came
to an animal to be sacrificed on the altar, its blemish-free condi-
tion was required.

This rule governed all vow payments. It extended back to
the economic source of the asset: “Thou shalt not bring the hire
of a whore, or the price of a dog, into the house of the Lorn
thy God for any vow: for even both these are abomination unto
the Lorp thy God” (Deut. 23:18). This is why prostitutes were
not citizens of Israel. They had the legal status of uncircum-
cised strangers. They could not bring their tithes and offerings
to God, so they could not be members of the church. This
separated them from citizenship in the holy commonwealth.

12. Gary North, Dominion and Common Grace: The Biblical Basis of Progress CIyler,
‘Texas: Institute for Christian Economics, 1987), ch. 6.
