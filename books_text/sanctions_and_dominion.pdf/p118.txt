80 SANCTIONS AND DOMINION

capital offense for insufficiently holy Israelites to cross its boun-
dary (Ex. 19:12-13).

Participants in Israel’s holy commonwealth knew that they
could not operate alone. They knew also that services had to be
paid for. There were no free lunches in Israel, although the
presence of the manna in the Ark at the very center of the
nation testified that there had been partially subsidized lunches
in the wilderness. The curse of the ground had been removed
for a season.

There was no possibility of equality in Israel; the society was
hierarchical. The closer to the Ark that a man operated lawful-
ly, the greater his sacerdotal authority and responsibility, and
also the greater his economic dependance on the economic
success of others and their obedience to the law. While the high
priest in Israel would probably never go hungry, he risked
God’s sanctions every time he performed a mandated ritual.
The fiery testimony of Nadab and Abihu was sufficient warning.
No one could lawfully trade places with him, and no one who
wanted to live would have tried.

Priestly service was governed by the workshop rule: a place
for everything, and everything in its place. But the farther away
from the holy of holies, the less that Israel was governed by this
narrowly circumscribed law. The office of high priest was
unique. There could only be one high priest at a time. The
farther away from the Ark, the less that any man had a specific
mandated service or required place. Put differently, priestly
service is judicially more specific than non-pricstly service.

Let us consider the most successful of all the non-biblical
systems of sacred hierarchy in man’s history: Hinduism. The
Hindu system of permanent religious castes is the product of a
religious worldview that extends the principle of priestly service
to all of life. A man born into one caste cannot advance himself
or his family if such advancement is dependent upon his per-
forming services that are monopolies of another caste. Social
stagnation in Hindu society is correlative to its vision of the
