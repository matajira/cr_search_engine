Conclusion 327

history — by either the final judgment or the rapture - can.
bring back the covenantal structure of history as it existed
under the Old Covenant.

The New Covenant is, to this extent, a burden of cosmic
proportions for God's people compared to the Old Covenant.
‘The death, resurrection, and ascension of Jesus Christ in histo-
ry brought a harsh legacy into history, we are assured by pessi-
millennialists: the reversal of the covenant’s basis of inheritance.
Their eschatologies are consistent with their view of sanctions,
ie., that covenant-breakers will progressively impose historical
sanctions on covenant-keepers. Pessimillennialists have a consis-
tent theology of historical sanctions and inheritance: covenant-
breakers will inevitably inherit in history because God has pre-
destinated them to impose historical sanctions on the church.
This was the operational eschatology of all those who sought to
stone Caleb and Joshua.

The suggestion that all three eschatological views can coexist
indefinitely inside the same ecclesiastical organization is neces-
sarily a suggestion that neither covenant theology nor eschatol-
ogy matters decisively in the life of the church. Both doctrines
are to this extent adiaphora: things indifferent to the Christian
faith. Ulumately, this suggestion of eschatological pluralism is
highly partisan. It favors the worldview and anti-Christendom
agendas of both amillennialism and premilennialism, for these
outlooks are united in their opposition to the covenantal struc-
ture of inheritance and disinheritance in the New Testament
era. In the name of eschatological neutrality, amillennialists and
premillennialists come to postmillennialists and ask them to
agree that covenantal postmillennialism’s view of the past and
the future is, historically speaking, a moot point. Moot points
are mute points. This is an ancient lure: the myth of neutrality.
It is offered in the name of peace and growth. It is the myth
that undergirds.all forms of confessional pluralism.

This is why a consistent theonomist must reject eschatolo-
gical pluralism as an ideal for the creeds and confessions of the
