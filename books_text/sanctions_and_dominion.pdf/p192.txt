154 SANCTIONS AND DOMINION

evaluation of reality was also rejected by them. They were
consistent. They rejected God, Moses, and the reality imposed
by God and explained by Moses.

Reality was what Moses said it was. God’s visible sanctions
came predictably in terms of Moses’ evaluation. The people
could ignore their own eyes and ears, and did. After the des-
truction of the rebels, the people blamed Moses. The true god
had been represented by the rebels; Moses must have repre-
sented a false God. So, God gave them another dose of reality.

Still, God knew this was not enough. He imposed the test of
the blossoming rod. This finally persuaded them. They were
now afraid to approach the tabernacle. But it had taken nega-
tive sanction after negative sanction to persuade them. Seeing,
they would not see. This wilful blindness brought a series of
negative sanctions on them, just as it had with Pharaoh. They
were not myopic; they were Pharaonic.

They could have avoided the negative sanctions at every
step, had they listened to Moses: prior to the golden calf inci-
dent, prior to the spies’ return, prior to the quail, prior to
Korah’s rebellion, prior to their attribution of righteousness to
the late Rev. Korah. Had they listened to Moses, they would
have seen reality. Refusing to listen to Moses, they refused to
see reality. This was reaffirmed by Jesus in the parable of Laza-
rus and Dives: “Then he [Dives] said, I pray thee therefore,
father [Abraham], that thou wouldest send him to my father’s
house: For I have five brethren; that he may testify unto them,
lest they also come into this place of torment. Abraham saith
unto him, They have Moses and the prophets; let them hear
them. And he said, Nay, father Abraham: but if one went unto
them from the dead, they will repent. And he said unto him, If
they hear not Moses and the prophets, neither will they be
persuaded, though one rose from the dead” (Luke 16:27-31).
Hell is the ultimate negative sanction, yet not even the testimo-
ny of one resurrected from the dead can persuade Moses-re-
jecting men of hell’s reality or of the way of escape. The way of
