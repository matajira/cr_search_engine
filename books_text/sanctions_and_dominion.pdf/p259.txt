Dividing the Inheritance 221

casting of lots was also used to divide up tribal real estate
among each tribe’s families: “every man’s inheritance shall be in
the place where his lot falleth.” It also sounds as though larger
families received larger portions: “And ye shall divide the land
by Jot for an inheritance among your families: and to the more
ye shall give the more inheritance, and to the fewer ye shall
give the less inheritance.” What did these phrases mean? Did
“allocation by lot” apply both to the tribes and families? Did the
“allocation by population” also apply to both? If so, how? How
did God’s allocation by lot integrate with the rulers’ allocation
in terms of population and presumed economic need?

Dual Allocations

The language of Numbers 33:54 indicates that both systems
of allocation governed the initial allocation, first to tribes and
then to families. How might this have worked? Let us consider
Abravanel’s suggestion. The land was divided up into eleven
regions, but initially there were no fixed boundaries assigned to
these large plots. Then lots were cast to determine which tribe
would live in which region. Variations in the land’s productivity
would not become matters of inter-tribal conflict except in the
tribes’ border areas. The falling of the lot would govern the
distribution. Then the question of tribal size became an issue.
Here the rulers would have to decide. The Levites would prob-
ably have played an important role here because they were not
given any rural land. Levites would live in cities in all regions.
They had no self-interest in favoring one tribe over another. By
using the lot method to allocate land regionally, and by using
population to establish boundaries, the system reduced tribal
conflict over the regional assignments, yet it honored consider-
ations of equity: not favoring the members of small tribes by
granting them family plots that were larger on average than the
plots inherited by populous tribes.

If this dual allocation system was established as a way to
reduce the number of inter-tribal conflicts regarding general
