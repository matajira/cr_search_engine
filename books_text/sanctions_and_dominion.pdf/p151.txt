Blessing and Naming 113

warned by Elijah: “And thou shalt speak unto him, saying,
Thus saith the Lorn, Hast thou killed, and also taken posses-
sion? And thou shalt speak unto him, saying, Thus saith the
Lorp, In the place where dogs licked the blood of Naboth shall
dogs lick thy blood, even thine” (I Ki. 21:19). He was deter-
mined to prove Elijah and Micaiah wrong.

Ahab commanded his guards to imprison Micajah. Micaiah
then offered another word of prophecy, also connected to
God’s. visible sanctions in history: “And Micaiah said, If thou
return at all in peace, the Lorp hath not spoken by me. And he
said, Hearken, O people, every one of you” (I Ki. 22:28). The
positive sanction of peace would be the public test. If peace
came to Israel, Micaiah was the false prophet and therefore had
to be executed (Deut. 18:20-22). If peace did not come, then
the court prophets were deserving of execution.

Ahab took the prophet’s words seriously enough to disguise
himself before going into battle ({ Ki. 22:30). This did him no
good. He did not return alive. “And a certain man drew a bow
at a venture [in his simplicity], and smote the king of Israel
between the joints of the harness: wherefore he said unto the
driver of his chariot, Turn thine hand, and carry me out of the
host; for I am wounded” (v. 34). There was nothing random
about this event, despite the bowman’s lack of knowledge re-
garding the identity of his target. Ahab died. “And one washed
the chariot in the pool of Samaria; and the dogs licked up his
blood; and they washed his armour; according unto the word
of the Lorp which he spake” (1 Ki. 22:38). Three prophecies
had come to Ahab regarding his end; two were truc and one
was official. He knew the difference. He sought to kill the first
unofficial prophet (Elijah) and imprisoned the second (Mi-
caiah), But he knew the difference. He sought to bring negative
sanctions against those true prophets who invoked God’s name
and His curses. He listened — i.e., decided his course of action
- to false prophets who invoked God’s name and His blessings.
