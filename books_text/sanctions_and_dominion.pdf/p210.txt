172 SANCTIONS AND DOMINION

pense of negative economic sanctions imposed on others in the
community. Control over taxation and economic regulation has
in the twenticth century become a three-fold matter: the exer-
cise of presumed messianic power, the suppression of one’s
class enemies, and economic selfdefense. Jack Douglas has put
it well: “. . . the welfare states in the most Christian Western
states have strongly emphasized their supposed enactments of
the Christian virtues — compassion, charity, forgiveness, kindli-
ness, and so on, even when they have simultaneously been
rabidly pursuing the seven deadly sins.”*! In fallen man, pow-
er tends to corrupt, and absolute power corrupts absolutely —
Lord Acton’s famous dictum.” This applies to taxation, for the
power to tax involves the power to destroy — Chief Justice John
Marshall’s equally famous dictum.”

Israel’s priesthood was not to become a battlefield of wealth
redistribution among classes. God placed explicit limits on what
the Levites could lawfully request in God’s name: the tithe. The
Levites were under God’s law. Men might pay them less than a
tithe, but this would bring God’s negative sanctions in history.
Individuals might pay more, but this extra giving was not called
a tithe; it was a voluntary offering.

The Levites were restricted by a boundary, the tithe. The
tithe was not.a means of income redistribution among classes.
The tithe was the inheritance of the Levites. It was owed to
them by the tribes. This was not a matter of choice among
those who were under the covenant.

21. Jack D, Douglas, The Myth of the Welfare State (New Brunswick, New Jersey:
Transaction, 1991), p. 11.

22. Letter to Mandell Creighton (April 5, 1887); reprinted in Selected Writings of
Lord Acton, 3 vols. (Indianapolis, Indiana: LibertyClassics, 1986), II, p. 383. The
context of this remark was his rejection of the authority of the Papacy to use the
State to impose physical tormre and execution against heretics. Acton was a Roman
Catholic.

23, McCulloch v. Maryland (1819); reprinted in John Marshall's Defense of McCulloch
«, Maryland (Stanford, California: Stanford University Press, 1969}, p. 46.
