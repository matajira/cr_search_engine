Preface XXxi

is defining this “real kingdom” strictly and solely as the institu-
tional church, he has abandoned a fundamental tenet of the
Protestant Reformation, which denied that the institutional
church constitutes the whole of the kingdom of God in history.
The New Testament kingdom encompasses the institutional
church, but it is far more. On this issue, I appeal to Geerhardus
Vos, a respected theologian in the Dutch Reformed tradition,
who also held a faculty position for over four decades at Prince-
ton Theological Seminary. He wrote of the kingdom of God:
“There is a sphere of science, a sphere of art, a sphere of the
family and the state, a sphere of commerce and industry.
Whenever one of these spheres comes under the controlling
influence of the principle of the divine supremacy and glory,
and this outwardly reveals itself, there we can truly say that the
kingdom of God has become manifest... . On the one hand,
his [Christ’s] doctrine of the kingdom was founded on such a
profound and broad conviction of the absolute supremacy of
God in all things, that he could not but look upon every nor-
mal and legitimate province of human life as intended to form
part of Ged’s kingdom. On the other hand, it was not his inten-
tion that this result should be reached by making human life in
all its spheres subject to the visible church.” The institutional
church is narrower than God’s kingdom. This has always been the
Reconstructionists’ view of the kingdom.” It is quite conven-
tional in Reformed circles, contrary to Rev. Engelsma’s sugges-
tion.

Second, if Rey. Engelsma is not defining the kingdom as the
institutional church alone, then he needs to offer reasons why
the Mosaic civil laws governing adultery and homosexuality are
no longer valid. It is not enough for him merely to say that
they are not valid; he must show us why. He refuses to do this,

45. Geethardus Vos, The Teachings of jesus Concerning The Kingdom and the Church
(Grand Rapids, Michigan: Eerdmans, 1958), p. 88.

46. See R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), pp. 69-70, for a discussion of this point which relies on Vos.
