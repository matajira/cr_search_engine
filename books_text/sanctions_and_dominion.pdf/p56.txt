18 SANCTIONS AND DOMINION

unto the Lorp, when thou numberest [pagad] them; that there
be no plague among them, when thou numberest them” (Ex.
30:12). The same word is sometimes translated as visit. “Thou
shalt not bow down thyself to them, nor serve them: for I the
Lorn thy God am a jealous God, visiting the iniquity of the
fathers upon the children unto the third and fourth generation
of them that hate me” (Ex. 20:5). This visitation implies nega-
tive sanctions: “Thus saith the Lorp unto this people, Thus
have they loved to wander, they have not refrained their feet,
therefore the Lorn doth not accept them; he will now remem-
ber their iniquity, and visit [pagad] their sins” (Jer. 14:10). The
word can mean punish. “And I will punish {pagad] the world for
their evil, and the wicked for their iniquity; and I will cause the
arrogancy of the proud to cease, and will lay low the haughti-
ness Of the terrible” (Isa. 13:11).

The nation faced no military enemy at the time of the Exo-
dus mustering. No one is said to have ordered this mustering,
yet the nation voluntarily consented to it: “And the silver of
them that were mustered of the congregation was an hundred
talents, and a thousand seven hundred and threescore and
fifteen shekcls, after the shcekel of the sanctuary: A bekah for
every man, that is, half a shekel, after the shekel of the sanctu-
ary, for every one that went to be numbered, from twenty years
old and upward, for six hundred thousand and three thousand
and five hundred and fifty men” (Ex. 38:25~26). This corre-
sponded to the required payment of half a shekel of silver per
man (Ex. 30:14-15). This was in addition to their voluntary
offerings of gold and brass, which they brought in such abun-
dance that God ordered Moses to tell them to stop (Ex.
36:5-7).* While the final accounting appears in Exodus 38, it
was recorded here after the construction of the tabernacle had
begun. The donations had come at the time of the mass presen-

8. Gary North, Yols of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1990), ch. 30.
