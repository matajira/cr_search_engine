The Hierarchy of Service/Sanctions 63

4:15). The coverings preserved the visual sanctification of the
objects. The Kohathites were warned not to touch any holy
object (v. 15b). The objects were carried on poles (“staves”)?
inserted through rings’ or carried on top of bars (vv. 10, 12).
God also warned the sons of Aaron not to do this task. “And
the Lop spake unto Moses and unto Aaron, saying, Cut ye not
off the tribe of the families of the Kohathites from among the
Levites: But thus do unto them, that they may live, and not die,
when they approach unto the most holy things: Aaron and his
sons shall go in, and appoint them every one to his service and
to his burden: But they shall not go in to see when the holy
things are covered, lest they die” (Num. 4:17-20). Kohathites
were to be kept away from these objects until the sons of Aaron
took down the hanging wails of separation, so that there was no
longer any “in” to go into.

The Gershonites were prohibited from doing the work of the
Kohathites. Their assignments related to the next circle out-
ward: bearing the curtains of the tabernacle. They disassembled
and reassembled the tabernacle’s coverings and the associated
furnishings (Num. 4:22-28). They were under the jurisdiction
of Ithamar (v. 28).

Similarly, the sons of Merari had their tasks associated with
the next outward circle: bearing the boards and pillars (Num.
4:29-33), They were required to do the same with the outer
support structure of the tabernacle (Num. 4:29-33). Each item
was assigned to one man by name (vy. 32). They, too, were
under the jurisdiction of Ithamar (v. 33). The Hebrew word,
massau, indicates carrying or portering. They used oxcarts to
transport the various materials (Num. 7:7).

The Levites transported the Ark oni a cart in David’s day,
which was clearly in violation of the law. They were supposed

9, Numbers 4:6, 8, 11.

10. “And thou shalt put the staves into the rings by the sides of the ark, that the
ark may be borne with them, The staves shall be in the rings of the ark: they shalt
not be taken from it* (Ex..25:14-18).
