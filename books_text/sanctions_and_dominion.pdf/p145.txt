Blessing and Naming 107

The most famous incident in the Bible was Gideon’s testing of
the fleece: wet fleece, dry ground; dry fleece, wet ground (Jud.
6:37—40). But there were others. Moses’ challenge to Korah and
Dathan was one. “And Moses said, Hereby ye shall know that
the Lorp hath sent me to do all these works; for 1 have not
done them of mine own mind. If these men die the common
death of all men, or if they be visited after the visitation of all
men; then the Lorp hath not sent me. But if the Lorp make a
new thing, and the earth open her mouth, and swallow them
up, with all that appertain unto them, and they go down quick
into the pit; then ye shall understand that these men have
provoked the Lorp” (Num. 16:28-30).

There were times when God’s intervention in history was
understood even by covenant-breakers. When the inhabitants of
each successive Philistine city in which the stolen Ark of the
Covenant resided came down with boils, the civil rulers recog-
nized their problem. They came to the priests for counsel. The
priests recommended a test: “Now therefore make a new cart,
and take two milch kine, on which there hath come no yoke,
and tie the kine to the cart, and bring their calves home from
them: And take the ark of the Lorp, and lay it upon the cart;
and put the jewels of gold, which ye return him for a trespass
offering, in a coffer by the side thereof; and send it away, that
it may go. And see, if it goeth up by the way of his own coast to
Beth-shemesh, then he hath done us this great evil: but if not,
then we shall know that it is not his hand that smote us; it was
a chance that happened to us” (1 Sam. 6:7-9). It was not
chance; it was God. The oxen walked into Israel (v. 12).

In the depths of philosophical despair, the author of Ecclesi-
astes wrote: “All things come alike to all: there is one event to
the righteous, and to the wicked; to the good and to the clean,
and to the unclean; to him that sacrificeth, and to him that
sacrificeth not: as is the good, so is the sinner; and he that
sweareth, as he that feareth an oath. This is an evil among all
things that are done under the sun, that there is one event
