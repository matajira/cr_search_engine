The Psychology of Victory 195

fiery serpents among the people, and they bit the people; and
much people of Israel died” (Num. 21:6). The people then
repented: “Therefore the people came to Moses, and said, We
have sinned, for we have spoken against the Lorp, and against
thee; pray unto the Lorp, that he take away the serpents from
us. And Moses prayed for the people” (w. 7).

The Brass Serpent

At this point, an event took place that raises difficult ques-
tions regarding symbolism in the ancient Near East, as we shall
seé. “And the Lorn said unto Moses, Make thee a fiery serpent,
and set it upon a pole: and it shall come to pass, that every one
that is bitten, when he looketh upon it, shall live. And Moses
made a serpent of brass, and put it upon a pole, and it came to
pass, that if a serpent had bitten any man, when he beheld the
serpent of brass, he lived” (vv. 8-9). This took place sometime
in the second half of the fifteenth century before the birth of
Jesus Christ.

The brass serpent was attached to a pole. This image still
marks the guild of physicians: a serpent entwined around a
pole. A similar symbol, two serpents entwined around a pole,
goes back to Sumeria. The Sumerian god Ninazu was the god
of healing. The son of this god, Ningishzida, was represented
by the two snakes and pole.’ This is known today as the. cadu-
ceus: in Greek mythology, the wand of Hermes. The symbol of
a snake on a pole is also associated with Asklepius, the Greek
god of healing. This god was the Greek deity most frequently
represented in snake form.* The Phoenecians’ healing god,
Eshmun, was represented by a snake.’ The snake-pole symbol

3. E, A, Wallis Budge, Amulets and Talismans (New Hyde Park, New York:
University Books, [19807] 1961), pp. 488-89.

4, John Cuthbert Lawson, Modern Greek Folklore and. Ancient Greek Religion: A
Study in Survivais (New Hyde Park, New York: University Books, (1910] 1964), pp.
274-75.

5. Jacob Milgrom, The JPS Torah Commentary: Numbers (New York: Jewish Publi-
