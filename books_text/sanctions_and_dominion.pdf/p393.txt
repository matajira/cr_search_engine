How Large Was Israel’s Population? 355

have came after this event. The 3,000 slain men were a signifi-
cant percentage of the original Israelite population of about
35,000 men. The magnitude of the loss of population was
consistent with the magnitude of the crime against God. The
loss of one-half of one percent of 600,000 adult males does not
seem sufficiently burdensome. But to take this position, I must
assume that the mixed multitude had no part in the rebellion,
nor would they have participated in the covenant oath at Sinai.
This may be too much to assume. If they were adopted earlier,
then God was being very lenient with the nation.

In the third numbering (Num. 26), the text is silent with
respect to firstborn sons. This is because there was no longer
any need to number them as a group. The ratio of biological
firstborn sons to the total number of Levite males was estab-
lished after the initial numbering of the tribes; so, this ratio was
no longer judicially relevant except for individual families.
From the day of the one-time numbering of the Levites (Num.
3), each non-Levitical family had to pay money to the Aaronic
priesthood for each firstborn male under its authority, whether
of man or beast (Num. 18:15-16). Numbering the firstborn
corporately was no longer necessary.

The Number of Levites

The number of Levites almost perfectly matched the number
of biological firstborn minor sons of the other 12 tribes. This
was God’s doing, not man’s, The adoptions took place before
Exodus 38:25. Moses did not yet know about the substitution of
Levites for firstborn sons. It does not appear that Moses delib-
erately assigned to Levi a number of adoptees that closely
matched the number of firstborn. Presumably, each tribe was
assigned a proportional share of new members. As the other
tribes’ total number of adopted members grew, though not the
total number of biologically firstborn sons, the Levites’ number

32. See below: “The Number of Levites.”
