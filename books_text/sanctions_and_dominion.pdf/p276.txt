238 SANCTIONS AND DOMINION

one son. Either the elders or a priest would have to take formal
action to validate the disinheritance.

New Covenant Inheritance-Disinheritance

With the transfer of the kingdom from Israel to the church
(Matt. 21:43), the laws of inheritance were transformed. With a
new priesthood came a new legal order. This biblical principle
of judicial transformation was announced by the author of the
epistle to the Hebrews: “For the priesthood being changed,
there is made of necessity a change also of the law. For he of
whom these things are spoken pertaineth to another tribe, of
which no man gave attendance at the altar. For it is evident
that our Lord sprang out of Juda; of which tribe Moses spake
nothing concerning priesthood. And it is yet far more evident:
for that after the similitude of Melchisedec there ariseth anoth-
er priest, Who is made, not after the law of a carnal command-
ment, but after the power of an endless life. For he testifieth,
Thow art a priest for ever after the order of Melchiscdec” (Heb.
7:12-17). The Mosaic land inheritance laws ceased with the
advent of the new priesthood. There was no longer any need to
keep the tribes geographically separate. The church recognized
this, of course, but so did the Jews. After the fall of Jerusalem
and the burning of the temple, Judaism replaced the Old Cove-
nant religion. Judaism is not the religion of Old Covenant
Israel, a fact that Jews who are familiar with their tradition
readily admit.®

In Mosaic Israel, a father could not unilaterally disinherit his
son; under New Covenant law, he is allowed to. This transfers
economic authority to the father. The threat of economic disin-
heritance is a hammer that he holds over wayward children. He
is not compelled by New Covenant law to subsidize evil. He was
not compelled by law to subsidize evil under the Mosaic law,

6, Jacob Neusner, judaism and Scripture: The Evidence of Leviticus Rabbah (Universi-
ty of Chicago Press, 1986), Preface.
