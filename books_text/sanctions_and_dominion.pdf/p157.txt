The Altar Offering 119

Cleansing Before Service Begins

After the gifts were delivered to Moses, he spoke to God.
God told him to light the lamps, which Aaron did (Num.
8:1-3). Then came the next step: “And the Lorp spake unto
Moses, saying, ‘Take the Levites from among the children of
Israel, and cleanse them. And thus shalt thou do unto them, to
cleanse them: Sprinkle water of purifying upon them, and let
them shave all their flesh, and let them wash their clothes, and
so make themselves clean” (vv. 5-7). Once they were cleansed,
it was time for a meat (meal) offering (v. 8a), the offering associ-
ated with priestly authority,’ and a sin or purification offering
(v, 8b). This was to take place in front of the assembly (v. 9),
who were the representatives of the congregation.° First, how-
ever, the transfer of representative authority had to pass from
the assembly to the Levites by laying on of hands. “And thou
shalt bring the Levites before the Lorn: and the children of
Israel shall put their hands upon the Levites” (v. 10). This is. a
very important principle: biblical authority flows downward from
God and upward from the people. The Levites represented the
people before God and represented God before the people.
Their authority was mediatorial. God’s acceptance of their
sacrifices judicially sealed the first; the laying on of hands judi-
cially sealed the second.

Aaron offered the Levites as a tribute offering to God: “And
Aaron shall offer the Levites before the Lorn for an offering of
the children of Israel, that they may execute the service of the
Lorp” (v. 11). To confirm this sacrificial offering of an entire
tribe - the representative firstborn ~ the Levites transferred
their offenses symbolically to the two bullocks: one for the

assessment on all members of God's holy army. R. J. Rushdoony, The Institutes of
Biblical Law (Nudey, New Jersey: Craig Press, 1973), pp. 281-82. C£ North, fools af
Dominion, ch. 32.

8. North, Leviticus, ch. 2.

9. Hhid., pp. 91-92,
