How Large Was Israel’s Population? 359

firstborn of the other tribes (vv. 44-45). There were an addi-
tional 273 biologically ttrstborn sons in the overall population,
for whom ransom money was paid to Aaron (Num. 3:46-51).

Adoption into a Tribe

I am assuming here that adoption was by family or tribe, not
by the nation as a whole. Israelites had membership in the con-
gregation through their families, clans, and tribes. Thus, when
the mass adoption took place, each family, clan, and tribe re-
ceived its share of the newcomers. In any case, each tribe did.
The newcomers were not citizens in general; they were citizens
of tribes. Levi would not have been left out of this initial distri-
bution of new members.

Amram’s small family, like Aaron’s, indicates that at the time
of the exodus, Israel’s nuclear families were not large. The
question is: How many families were there in each clan? If
families were small, there would have had to be many families
for Israel’s adult male population to have been 600,000 by the
Exodus numbering. That is, prior adoptions would have had to
multiply the number of nuclear families.

1s the mass adoption of the mixed multitude the likely. scen-
ario, with stable population in the wilderness based on a less-
than-replacement-rate stagnation, but without a population
catastrophe? If so, then the firstborn sons had been born of
Israelite mothers; the adoptees were the mixed multitude.

If Israelite fathers adopted gentile sons, then the bulk of
these adoptees were probably younger men who were of fight-
ing age. They would have been adopted by Moses’ generation.
Thus, the bulk of the population was in Joshua’s generation.
These adults were replaced by the conquest generation. The
sons of Joshua came from the loins of Joshua’s brothers by
adoption. Replacing Moses’ generation was statistically inciden-
tal; it had not been Jarge compared to the 600,000. Thus, the
population moved into replacement-rate mode during the
wilderness. Joshua's generation was much larger than Moses’
