148 SANCTIONS AND DOMINION

things come on schedule. So do all bad things. That future era
would bring good things to Israel and bad things to Canaanites,
whose iniquity would at last be filled (Gen. 15:16). They were to
imitate God, who was willing to wait for his extra sacrifices until
they entered the Promised Land.

The man who gathered sticks on the sabbath failed to honor
the deferred gratification principle of the Old Covenant sab-
bath: rest at the end of the week. He chose to disparage that
future rest by failing to plan ahead. He was present-oriented,
and this cost him dearly on the day of rest.
