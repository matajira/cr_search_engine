Bloodline Inheritance 235

priesthood. Even a king would be restricted from amassing
rural land and houses for his heirs. For all the evil that Ahab
did, it was his theft of Naboth’s vineyard that brought God’s
final sanction against him. God told Elijah: “And thou shalt
speak unto him, saying, Thus saith the Lorn, Hast thou killed,
and also taken possession? And thou shalt speak unto him,
saying, Thus saith the Lorn, In the place where dogs licked the
blood of Naboth shall dogs lick thy blood, even thine” (I Ki.
21:19).*

The land inheritance law elevated family name over econom-
ic productivity, love, or power. The land owner could not disin-
herit his family by selling his rural inheritance to the highest
bidder. The tribal name was elevated over family name. ‘To
preserve each family’s name in a specific tribe, and to preserve
the numerical strength of each tribe, God. established a land
inheritance system for rural property that subsidized the heirs
of the conquest generation.

Old Covenant Inheritance-Disinheritance

There was covenantal disinheritance in Isracl: civil, ecclesias-
tical, or familial. A father was not allowed unilaterally to disin-
herit just one son. A father had to follow strict rules of inheri-
tance. “If a man have two wives, one beloved, and another
hated, and they have born him children, both the beloved and
the hated; and if the firstborn son be hers that was hated: Then
it shall be, when he maketh his sons to inherit that which he
hath, that he may not make the son of the beloved firstborn
before the son of the hated, which is indeed the firstborn: But
he shall acknowledge the son of the hated for the firstborn, by

3. This was fulfilled in I Kings 22:38. For Jezebel, who had planned the evil act.
(1 Ki. 21:5-9), it would be worse: the dogs would actually eat her. “And of Jezebel
also spake the Lorn, saying, The dogs shall eat Jezebel by the wali of Jczreet” (1 Ki.
21:23). This was fulfilled in IL Kings 9:35.
