66 SANCTIONS AND DOMINION

Levites take tithes: and the Levites shall bring up the tithe of
the tithes unto the house of our God, to the chambers, into the
treasure house. For the children of Israel and the children of
Levi shall bring the offering of the corn, of the new wine, and
the oil, unto the chambers, where are the vessels of the sanctu-
ary, and the priests that minister, and the porters, and the
singers: and we will not forsake the house of our God” (Neh.
10:38-39). Again, we read: “And all Israel in the days of Zerub-
babel, and in the days of Nehemiah, gave the portions of the
singers and the porters, every day his portion: and they sancti-
fied holy things unto the Levites; and the Levites sanctified
them unto the children of Aaron” (Neh. 12:47).

Who were the priests who received the tithe of the Levites in
pre-exilic times?!’ Answer: those who were serving God full-
time in sacramental activities at the tabernacle. This included
those Levites who were serving their tour of duty at the taber-
nacle. In the tribal division of labor, they could not spend time
working at other jobs.

Tithe-Exempt

The Levites’ tithe to Aaron is specified as a tithe of every-
thing they collected from the other tribes. “Thus speak unto the
Levites, and say unto them, When ye take of the children of
Israel the tithes which I have given you from them for your
inheritance, then ye shall offer up an heave offering of it for
the Lorn, even a tenth part of the tithe. And this your heave
offering shali be reckoned unto you, as though it were the corn

11, This changed in post-exilic times because so few Levites returned and so
many priests did. Accompanying Zerubbabel were 4,289 priests (Ezra 2:36-39) and
341 Levites (Ezra 2:40-49). Extra-biblical evidence, mainly from Josephus, indicates
that the tithe was divided so that the priests were supported more directly by the
fruits of the people. See Life of Flavius Josephus, 15; Antiquities of the Jews, XI:v:8:
XX:viii:8; ix:2. Against Apion 1:29; cited in “Tithe,” in Cyclopaedia of Biblical, Theologi-
cal, and Ecclesiastical Literature, edited by John M’Clintock and James Strong, 12 vols.
(New York: Harper & Bros, 1894), X, p. 436.

  
