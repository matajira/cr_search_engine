40 SANCTIONS AND DOMINION

priestly sanctions. Those who were not under priestly sanctions
in Mosaic Israel were not allowed to impose holy sanctions.
They could not become members of the army. The biblical
principle of covenant membership is this: those who impose
sanctions must be under them. This is the principle of the rule
of law. These holy military sanctions were positive for the win-
ners and negative for the losers. The primary historical sanction.
of war is death. To begin preparing for a war is to begin pre-
paring for someone's death, possibly one’s own. Death stalks
every battlefield.

Mustering was part of military planning. It was mandatory
prior to a war. More than this: the military census is the biblical
model for all other forms of planning.

For which of you, intending to build a tower, sitteth not down first,
and counteth the cost, whether he have sufficient to finish it? Lest
haply fit happen], after he hath laid the foundation, and is not able to
finish it, all that behold it begin to mock him, Saying, This man began
to build, and was not able to fish. Or what king, going to make war
against another king, sitteth not down first, and consulteth whether he
be able with ten thousand to meet him that cometh against him with
twenty thousand? Or else, while the other is yet a great way off, he
sendeth an ambassage, and desireth conditions of peace (Luke 14:28~
32).

There is more to military planning than numbering the
army. There is also the question of the army’s willingness to
fight: mental preparedness. This applied both to the army and
the civilian population. Moses sent spies into the land and
awaited their reports (Num. 13-14). God did not order this
(Num. 13:2) in order to evaluate the strength of the enemy; He
did this in order to test the spies’ willingness to evaluate the
land’s blessings and military strength in terms of the Israelites’
willingness to fight. A fearful man was not to fight, for his fear
might spread to those around him (Deut. 20:8). Gideon dis-
