Blessing and Naming iil

And all the prophets prophesied so, saying, Go up to Ramoth-gil-
ead, and prosper: for the Lorp shall deliver it into the king’s hand.
And the messenger that was gone to call Micaiah spake unto him,
saying, Behold now, the words of the prophets declare good unto the
king with one mouth: let thy word, I pray thee, be like the word of one
of them, and speak that which is good. And Micaiah said, As the Lorp
liveth, what the Lorp saith unto me, that will I speak. So he came to
the king. And the king said unto him, Micaiah, shall we go against
Ramoth-gilead to battle, or shall we forbear? And he answered him, Go,
and prosper: for the Lorp shall deliver it into the hand of the king.
And the king said unto him, How many times shall I adjure thee that
thou tell me nothing but that which is true in the name of the Lorn?
And he said, I saw all Israel scattered upon the hills, as sheep that have
not a shepherd: and the Logo said, These have no master: let them
return every man to his house in peace. And the king of Israel said
unto Jehoshaphat, Did I not tell thee that he would prophesy no good
concerning me, but evil? (I Ki. 22:12-18).

Micaiah then went on to warn the king regarding the super-
natural source of the good news announced by the king's offic-
ial prophets:

And he said, Hear thou therefore the word of the Lorn: I saw the
Lorgp sitting on his throne, and all the host of heaven standing by him
on his right hand and on his left. And the Lorn said, Who shall per-
suade Ahab, that he may go up and fall at Ramoth-gilead? And one
said on this manner, and another said on that manner. And there came
forth a spirit, and stood before the Lorp, and said, I will persuade him.
And the Lorp said unto him, Wherewith? And he said, I will go forth,
and I will be a lying spirit in the mouth of all his prophets. And he
said, Thou shalt persuade him, and prevail also: go forth, and do so.
Now therefore, behold, the Lorp hath put a lying spirit in the mouth
of all these thy prophets, and the Lorp hath spoken evil concerning
thee (I Ki. 22:19-23)8

5. That this spirit was evil is clear from the context. Prior to Christ's ministry,
Satan had access to the court of heaven (Job 2). After the crucifixion, he no longer
had such access. “And there was war in heaven: Michael and his angels fought
