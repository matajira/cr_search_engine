24 SANCTIONS AND DOMINION

No Population Growth

One month after the tabernacle’s completion, the size of
Israel’s militarily eligible male population was exactly the same
as it had been approximately four months earlier at the Exodus
mustering: 603,550 men (Ex. 38:26; Num. 1:46). This indicates
that Israel’s population had reached zero growth. Thirty-nine
years later, God ordered another mustering. The population
was essentially unchanged at this second compulsory mustering:
601,730 (Num. 26:51); so was the number of Levites (Num.
3:39: 22,000; Num. 26:62: 23,000). Israel suffered from zero
population growth in the wilderness.

The approximately 600,000 fighting-age males in the first
two musterings comprised two generations: Moses’ and Josh-
ua’s. The conquest generation and their adult-age sons replaced
the 600,000 men who had been over age 19 at the time of the
first mustering. If the top two generations were 300,000 each
(population replacement-rate mode), then a family of Joshua’s
generation produced on average one son and grandson age 20
or older by the time of the second Numbers mustering. But
should we assume a stagnant population prior to the exodus?
The textual evidence is against this: families with more than
two children. If population was growing, then Joshua’s genera-
tion was larger than Moses’. This growth process was reversed
in the wilderness, where most Israelites had even smaller fami-
lies than two children. The nation moved from grace to curse.
Why?

Time Perspective

The early chapters of the Book of Numbers record a series
of complaints and rebellions on the part of the Israelites. They
afflicted Moses with their murmering. The exodus generation
was continually looking backward longingly at Egypt, despite
the fact that they had been in bondage there. They were a
