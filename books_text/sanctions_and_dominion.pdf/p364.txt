326 SANCTIONS AND DOMINION

millennialism is God's law without predictable sanctions — sanc-
tions that lead to the victory of God’s kingdom, in time and on
earth.

The New Covenant has not annulled the covenantal struc-
ture of inheritance. On the contrary, the New Covenant re-
affirms it. “Blessed are the meek: for they shall inherit the
earth” (Matt. 5:5). The New Covenant was marked by a transfer
of inheritance from Israel to the church. “Therefore say I unto
you, The kingdom of God shall be taken from you, and given
to a nation bringing forth the fruits thereof” (Matt. 21:43). This
transfer was visibly imposed by God through Rome’s destruc-
tion of the temple in A.D. 70."

Like the generation of the exodus, the vast majority of to-
day’s Christians steadfastly maintain that the covenantal struc-
ture of inheritance no longer applies in history. Amillennialism
and premillennialism march arm in arm on this point. Amillen-
nialists insist that God will progressively impose corporate nega-
tive sanctions against the church. Christendom as a civilization
will be suppressed, if it has not already been consigned by God
to the ash can of history. Not only that, antinomian amillen-
nialists insist, the very idea of Christendom is a perverse legacy
of Old Covenant Israel. They dismiss the ideal of Christendom
as “Constantinian.”

Meanwhile, premillennialists are divided. Historic premillen-
nialists, whose ranks are thin, agree with the amillennialists:
until Christ returns to set up His earthly kingdom, things will
get worse for God’s people. The dispensationalists insist that
things would get worse were it not for the rapture. The church
will be delivered out of history. Both of these eschatologies
agree: the covenantal structure of history has been reversed by
God. Covenant-keepers will be progressively disinherited, which
covenant-breakers will inherit the earth. Only the cessation of

11, David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987).
