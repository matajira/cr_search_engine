Conclusion 323.

They did not believe that the events of the exodus testified to
the reliability of God’s covenant with them. They did not be-
lieve Moses when he prophesied the positive sanction of future
victory. They demanded constant reassurance. “What have you
done for us lately?” was their constant rhetorical question to
Moses, and therefore to God.

In this sense, they were radical empiricists: spiritual forefath-
ers of David Hume and modern existentialists. For the radical
empiricist, there is no continuity of law in history. Patterns of
cause and effect that individuals believe they have observed in
the past do not prove the continuing existence of the same
fixed patterns in their observations, let alone in the world be-
yond their observations, whether in the present or the future.
The fact that a radical empiricist remembers that when he stuck
his finger into boiling water, it hurt, does not prove to him that
it will hurt the next time he does this. The mother’s warning to
her small child whois about to touch a hot stove - “Hot! Hot!”
~ may persuade the small child not to touch it after a few pain-
ful experiences, or even after one, but this does not, persuade
the radical empiricist:to change his theory of causation and
perception.® The small child possesses greater epistemological
clarity and more common sense than the radical empiricist.
Similarly, the children of the exodus generation had more
sense than their parents. They, unlike their parents, learned
from experience.

Sanctions and Inheritance

The Israelites departed from Egypt bearing spoils. The sons
of Israel survived the corporate negative sanction of the death
of the firstborn. Al of Egypt's firstborn sons perished on the

9. I think this has something te do with Jesus’ warning about hell: “Hot! Hot!"
Rebellious children do not listen. They prefer to remain radical empiricists rather
than become Christians. “Show me!” they cry. Jesus rose from the dead to ratify the
reliability of His warning. The radical empiricist then cries; “Show me again!” He
refuses to accept God's testimony (Luke 16:30-31).
