XxX SANCTIONS AND DOMINION

vain promised long life to all who have faithfully discharged the
duties of true piety towards their parents. Still, from the princi-
ple already stated, it is to be understood that this Command-
ment extends further than the words imply; and this we infer
from the following sound argument, viz., that otherwise God’s
Law would be imperfect, and would not instruct us in the per-
fect rule of a just and holy life.”* In other words, the sanc-
tions of the fifth commandment are still in force. God's visible
sanctions in history in general are not random; they reflect His
commitment to defend and extend His law in history.

Calvin’s comments on the fifth commandment were put into
final form by the author in 1563, the year before his death, and
therefore represent the culmination of his thinking. A century
later, in the aftermath of the Restoration of Charles II, his
spiritual heirs began to abandon this outlook. They began to
lose faith in the covenantal predictability of God’s sanctions in
history, especially positive sanctions for covenant-keepers.* In
our day, Meredith Kline has devoted his academic career to
persuading Calvinists to abandon Calvin on this point. If Calvin
was correct here, then Kline’s denial of the continuing New
Covenant authority of the Mosaic law-covenant™ would repre-
sent the abandonment of Calvinism in the name of Lutheran-
ism, which I contend is exactly what Kline’s theology repre-
sents.”

22. Calvin, Harmonies, HL p. 11.

23. The turning point in New England was marked by the publication in 1662
of Michael Wigglesworth’s ovo poems, The Day of Doom and God's Controversie With
New England, In England, the imposition of the Act of Uniformity (1662) and the
expulsion of some two thousand Calvinist pastors from their pulpits were equally
devastating to older Calvinism’s faith in the fature and in God's positive sanctions in
history.

24, Kline, Structure of Biblical Aushority, Pt. 2, ch. 8: “The Intrusion and the
Decalogue.”

25, Kline's former student and full-time disciple Michael Horton is far more
open regarding this quest for a Lutheran-Calvinist reconciliation. The judicial basis
of such a reconciliation is Calvinism’s acceptance of Lutheranism’s ethical dualism,
which Horton seems to accept. In a letter to Christian News (Nov, 13, 1995), a conser-
