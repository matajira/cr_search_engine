Bloodline Inheritance 241

to the heirs of the conquest generation. Tribal decentralization
preserved liberty in Mosaic Israel prior to the exile. After the
exile, those who returned to Israel were a handful compared to
the numbers that inherited at the time of the conquest. Israel
became a captive nation. National preservation, not decentral-
ization, became the primary political goal. This became a priest-
ly function rather than a tribal function.

After the exile, the land inheritance law changed. Inheri-
tance was still governed by the jubilee, but this was to include
pagans who resided in the land at the time of Israel’s return.
“Thus saith the Lord Gop; This shall be the border, whereby ye
shall inherit the Jand according to the twelve tribes of Israel:
Joseph shall have two portions. And ye shall inherit it, one as
well as another: concerning the which | lifted up mine hand to
give it unto your fathers: and this land shall fall unto you for
inheritance” (Ezek. 47:13-14). “So shall ye divide this land unto
you according to the tribes of Israel. And it shall come to pass,
that ye shall divide it by lot for an inheritance unto you, and to
the strangers that sojourn among you, which shall beget chil-
dren among you: and they shall be unto you as born in the
country among the children of Israel; they shall have inheri-
tance with you among the tribes of Israel. And it shall come to
pass, that in what tribe the stranger sojourneth, there shall ye
give him his inheritance, saith the Lord Gop” (Ezek. 47:21-23).
If the jubilee was actually honored, which we do not know, it
was to be honored in a new way. Gentiles were to be given
access to the land. Genocide was no longer part of the Old
Covenant order. This post-exilic law was to serve as a herald of
a New Covenant order to come, where Jews and gentiles would
inherit the kingdom together in terms of confession and ethics,
not blood.
