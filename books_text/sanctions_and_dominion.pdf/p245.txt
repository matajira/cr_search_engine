The Office of Court Prophet 207

Balak was a manipulator. He believed that man, not God, is
sovereign. He believed that the prophet’s power stemmed from
himself or from some impersonal cosmic repository of power. If
he could persuade the prophet to declare a word of power,
then the cosmos would respond and impose that curse. God
had nothing to do with the curse; Balaam was sovereign, not
God. And Balaam had a price. The answer Balak wanted to
know was what this price was. If he could ascertain Balaam’s
price and pay it, he would become sovereign over Israel on the
battlefield. He might even regain Moab’s lost inheritance, which
Israel occupied.

Balaam insisted that he was not in a position to deliver what
the king wanted. “God is not a man, that he should lie; neither
the son of man, that he should repent: hath he said, and shall
he not do it? or hath he spoken, and shall he not make it good?
Behold, I have received commandment to bless: and he hath
blessed; and I cannot reverse it” (Num. 23:19-20). Israel was
going to win: “Behold, the people shall rise up as a great lion,
and lift up himself as a young lion: he shall not lie down until
he eat of the prey, and drink the blood of the slain” (v. 24).

This concerned Balak. He had asked for a prophetic word;
he did not like what he heard: a blessing for Israel. He wanted
to return to the status gue ante: “And Balak said unto Balaam,
Neither curse them at all, nor bless them at all” (v. 25). But he
was too late: the prophetic word had been spoken. “But Balaam
answered and said unto Balak, Told not I thee, saying, All that
the Lorn speaketh, that I must do?” (v. 26). So, Balak suggest-
ed another change of perspective: “And Balak said unto Ba-
laam, Come, I pray thee, I will bring thee unto another place;
peradventure it will please God that thou mayest curse me
them from thence. And Balak brought Balaam unto the top of
Peor, that looketh toward Jeshimon” (vv. 27-28).

This is the classic sign of a manipulator: “Just keep your
options open until you see things my way.” He wants a particu-
lar outcome. If he cannot get what he wants by approaching
