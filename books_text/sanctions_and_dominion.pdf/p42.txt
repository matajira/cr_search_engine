4 SANCTIONS AND DOMINION

under the negative sanctions of the animals. The reappearance
of autonomous nature was regarded by God as being a more
fearful negative sanction against the land than continuing do-
minion by covenant-breaking mankind.

The late twentieth-century environmental movement denies
this view of nature by elevating the supposed needs of imper-
sonal, autonomous nature over the goals of man.* Such a view
of nature is pagan to the core. Increasingly, environmentalism
has become pantheistic and even occult: earth as “Gaia” ~ a
living spirit.”

The Covenantal Structure of the Exodus-Wilderness Books

The Book of Exodus presents the story of God’s deliverance
of the Israelites. The true king delivered them out of their
former bondage to a false king. God intervened in history to
demonstrate His power in history. “For I will at this time send
all my plagues upon thine heart, and upon thy servants, and
upon thy people; that thou mayest know that there is none like
me in all the earth. For now I will stretch out my hand, that I
may smite thee and thy people with pestilence; and thou shalt
be cut off from the earth, And in very deed for this cause have
I raised thee up, for to shew in thee my power; and that my
name may be declared throughout all the earth” (Ex. 9:14-16).
This is point two of the biblical covenant model: hierarchy.*
The evidence of God’s power was His ability to impose negative
sanctions on Pharaoh and those whom he represented. The

6. A manifesto of such a view of autonomous nature is Bill McKibben’s book, The
End of Nature (New York: Random House, 1989).

7. Even when cloakcd in scientific terminology, any attempt to revive the name
of the Greek goddess Gaia in relation to “mother nature” is indicative of an anti-
biblical religious impulse. See Gaia: An Atlas of Planet Management, edited by Norman
Myers (New York: Doubleday/Anchor, 1984); The Gaia Peace Atlas: Survival into the
Third Millennium, edited by Frank Barnaby (New York: Doubleday 1988). For a
detailed critique of the politics and religion of environmentalism, see Michael 5.
Coffman, Saviors of the Earth? (Chicago: Northfield Publishing, 1994).

8. Sutton, That You May Prosper, ch. 2.
