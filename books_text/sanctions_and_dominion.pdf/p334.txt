296 SANCTIONS AND DOMINION

seen the consequences of rebellion and cowardice. They. had
lived all of their adult lives under negative sanctions. They had
grown tired of this pattern of behavior. They wanted their
inheritance, and they were willing to pay for it on God’s terms:
military conquest. This was risky, but it was better than living as
wanderers.

God warned them that if they refused to drive out the Ca-
naanites, they would fall under the same negative sanctions that
Canaan was about to experience. The threat of negative histori-
cal sanctions is inherent in the biblical covenant. Canaan was
about to learn this first-hand. So was Israel. But Israel had
gained positive sanctions — land — by becoming God’s agency of
negative sanctions: a holy army.

This army was being taught a lesson: the trustworthiness of
God’s promises. This was an important lesson for the nation
prior to the conquest. The victors’ positive sanctions in warfare
were gained through the losers’ negative sanctions. Positive and
negative sanctions are military corollaries. Warfare is a zero-
sum activity: losers supply the winnings of the winners. This
makes warfare different trom the free market, where both
trading partners expect to gain through a voluntary exchange.

Conclusion

Moses made it clear to the conquest generation that the
terms of success in history are judicial. The Israelites were
required to maintain their commitment to God’s law. This
included the one-time law of annihilation. The gods of Canaan
were local gods who exercised their authority within the geo-
graphical boundaries of the promised inheritance. This is why
the nations of Canaan had to be destroyed. Military conquest
alone would destroy the authority and jurisdiction of the gods
of Canaan. The defeat of those gods would mark the victory of
God. The historical issue was sanctions.

God announced: “I have given you the land to possess it”
(Num. 33:53). But to collect what He had already given, they
