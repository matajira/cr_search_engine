220 SANCTIONS AND DOMINION

one family), the elders of each tribe knew that larger family
units would have to be allocated more land. The question was:
More land out of what sized tribal portion?

If this command from God was to be honored, there had to
be a fixed reference point. The problem facing the commenta-
tor is to identify this fixed reference point. Was it the size of the
family plot, with the same sized plot distributed by lot to each
warrior? Was the éribal plot size fixed, with the allocation of
plots within this fixed unit determined by the size and number
of the families belonging to the tribe? Or was the constant
factor the general geographical location of the land rather than the
size of the allocation? If so, was this confined to the tribes’
allocation, or were family plots also governed by the “general
location” principle?

First, if each family plot was the same size, with allocation
based on the number of holy warriors within each tribe, then
the tribes’ inheritances would not have been equal in size. Larg-
er tribes would have received larger allocations. This was
Rashi’s opinion. Second, if each tribal allocation was the same
size, then the individual warriors’ inheritances would have
varied in terms of the number of families in the tribe: members
of larger tribes received smaller family inheritance on average.
The families would then have been granted larger or smaller
plots in terms of their size. This was Nachmanides’ opinion,
and he cited rabbinical tradition. This would have meant that
members of small tribes with few families would have been
granted larger plots, on average, than members of populous
tribes.

There is a problem with Nachmanides’ interpretation: a
subsequent clarification of the law. “And ye shall divide the
land by lot for an inheritance among your families: and to the
more ye shall give the more inheritance, and to the fewer ye
shall give the less inheritance: every man’s inheritance shall be
in the place where his lot falleth; according to the tribes of your
fathers ye shall inherit” (Num. 33:54). It appears as though the
