256 SANCTIONS AND DOMINION

not affect the question.”'" Enlightenment political theory, both
right wing (Whig) and lett wing (Rousseau) operated on the
assumption that this hypothetical social contract is the judicial
foundation of civil government's assertion of a right to inflict
violence in defense of civil law. The State created by this con-
tract then becomes the enforcer of its implicit stipulations:
natural law. There is no appeal beyond the State. Covenantal-
ism under God becomes contractualism: a man-created, man-
based social order.

The family covenant under God then becomes the marriage
contract under the State. The justification of divorce becomes
social: what is good for society as interpreted by the State. So
does the justification for disciplining children. The family then
comes under the jurisdiction of the State because God is no
longer taken seriously as one of the participants in the arrange-
ment. The State replaces God as the supreme sanctioning
agent.

Economic contracts are also analogous to the covenant. The
State becomes the ultimate guarantor of a contract’s stipula-
tions. The contract’s participants agree to perform certain du-
ties in exchange for money or other assets. The mutuality of
the contract brings benefits to both signers. The State threatens
negative sanctions for a violation of the contract, thereby in-
creasing the likelihood of performance. Contractualism had
been an aspect of Western political theory since the late Middle
Ages.” The Scholasticism of Thomas Aquinas was the major
source."* Contractualism was developed as a comprehensive
social philosophy — not just for civil government but for society
— by Enlightenment thinkers of the eighteenth century. The

16, Rousseau, “A Dissertation on the Origin and Foundation of the Inequality of
Mankind” (1754), in Jean Jacques Rousseau, The Social Contract and Discourses, Every-
man’s Library (New York: Dutton, [1913] 1966), p. 161.

17. Sir Ernest Barker, “Introduction,” in Barker (ed), Social Contract: Essays by
Locke, Hume, and Rousseau (New York: Oxford University Press, 1962), p. vii.

18, Jbid., pp. viii-ix,
