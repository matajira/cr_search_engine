Progressive Whining and Final Accounting 135

(Num. 14:1-2). Life was their burden; they feared to lose it in
battle. Better to return to slavery: “And wherefore hath the
Lorp brought us unto this land, to fall by the sword, that our
wives and our children should be a prey? were it not better for
us to return into Egypt? And they said one to another, Let us
make a captain, and let us return into Egypt” (vv. 3-4).

Moses and Aaron fell on their faces before the representative
assembly (v. 5). Joshua and Caleb remained on their feet and
spoke out. The land is good; God will give it to us if He favors
us; do not rebel against God or fear the Canaanites (vv. 7-9).
Their words were negative sanctions against the people and the
other ten spies. The people were ready to respond with more
direct sanctions: “But all the congregation bade stone them
with stones” (v. 10a).

At that point, God appeared. He was ready to impose com-
prehensive negative sanctions: “And the Lorn said unto Moses,
How long will this people provoke me? and how long will it be
ere they believe me, for all the signs which I have shewed
among them? 1 will smite them with the pestilence, and disin-
herit them, and will make of thee a greater nation and mightier
than they” (vv. 11-12). Moses intervened and offered the most
effective prayer there is: an appeal to God’s reputation. “And
Moses said unto the Lorp, Then the Egyptians shall hear it, (for
thou broughtest up this people in thy might from among
them;) And they will tell it to the inhabitants of this land: for
they have heard that thou Lorp art among this people, that
thou Lorp art seen face to face, and that thy cloud standeth
over them, and that thou goest before them, by daytime in a
pillar of a cloud, and in a pillar of fire by night. Now if thou
shalt kill all this people as one man, then the nations which
have heard the fame of thee will speak, saying, Because the
Lorp was not able to bring this people into the land which he
sware unto them, therefore he hath slain them in the wilder-
ness” (13-16). Be merciful, therefore; pardon their iniquity (vv.
18-19).
