Progressive Whining and Final Accounting 129

under God’s sovereignty. He, too, has a scale of preferences,’
capital (including mankind),* a time scale (history),’ and a
plan (decree). Men choose analogous to God, as creatures.
Men act re-creatively, not creatively.

Negative Sanctions Applied

Numbers 11 begins with another complaint. We are not told
what it was. We are told that God had finally had enough. He
began a series of lessons that never made any lasting impression
on the exodus generation. He responded to their complaint
with supernatural fire. This sanction matched His emotion:

3. “Will che Lorp be pleased with thousands of rams, or with ten thousands of
rivers of oil? shall I give my firstborn for my transgression, the fruit of my body for
the sin of my soul? He hath shewed thee, O man, what is good; and what doth the
Lor? require of thee, but to do justly, and to love mercy, and to walk humbly with
thy God? (Mic. 6:7-8). “Thy kingdom come. Thy will be done in earth, as it is in
heaven” (Matt. 6:10). Cf. Matthew 23:23.

4. “The earth is the Lorors, and the fulness thereof; the world, and they that
dwell therein” (Ps. 24:1). “For every beast of the forest is miine, and the cattle upon
a thousand hills” (Ps. 30:10).

5. “And I heard the man clothed in linen, which was upon the waters of the
river, when he held up his right hand and his left hand unto heaven, and sware by
him chat liveth for ever that it shall be for a time, times, and an half; and when he
shall have accomplished to scatter the power of the holy people, all these things shall
be finished” (Dan. 19:7). “Heaven and earth shall pass away, but my words shall not
pass away. But of that day and hour knoweth ne man, no, not the angels of heaven,
but my Father only” (Matt. 24:35-36).

6. “For as the heavens are higher than the earth, so are my ways higher than
your ways, and my thoughts than your thoughts. For as the rain cometh down, and
the snow from heaven, and returneth not thither, but watereth the earth, and
maketh it bring forth and bud, that it may give seed to the sower, and bread to the
eater: So shail my word be that goeth forth out of my mouth: it shafl not return unto
me void, but it shall accomplish that which F please, and it shall prosper in the thing
whereto I sent it” (Isa. 55:9-11). “Blessed be the God and Father of our Lord Jesus
Christ, who hath blessed us with all spiritual blessings in heavenly places in Christ:
According as he hath chosen us in him befvre the foundation of the world, that we
should be holy and without blame before him in love: Having predestinated us unto
the adoption of children by Jesus Christ eo himself, according to the good pleasure
of his will” (Eph. 1:3-5). “For we which have believed do enter into rest, as he said,
As I have sworn in my wrath, if they shall enter into my rest: although the works
were finished from the foundation of the world” (Heb. 4:3).
