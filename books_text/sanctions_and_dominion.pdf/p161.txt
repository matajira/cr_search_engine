The Altar Offering 123

possibility. This in turn points to the existence of foreign trade
as a separate occupation, especially for those tribes that had
cities on the Mediterranean eastern coast. One modern academ-
ic interpretation of the Egyptian, Hittite, and Mesopotamian
empires is that these inland empires in the second millennium
B.C. did not engage directly in foreign trade, but worked
through neutral intermediaries along the Mediterranean’s
eastern coast.'4 If this interpretation is correct, then this dy-
nastic practice. placed Israel in a geographically strategic posi-
tion, along with the Phoenicians to the south and the trading
cities north of Tyre and Sidon, such as Ugarit, which served as
a conduit between Greece and the Near Eastern cultures.”
(Professor Gordon believed that the alphabet went from Ugarit
to the Hebrews and Phoenicians to the Greeks.)'® The costs of
land transportation were too high to be profitable except for
jewels and other high value commodities, ie, money assets.
This was as true in the ancient world as it was until the inven-
tion of the railroad. Not having access to any river that led into
the great inland empires, Israel’s coastal tribes would have to
become world traders in order to prosper. This appears to be
what they did.”

Conclusion

Sinful men cannot make acceptable offerings to God apart
from mediation. In this case, a whole system of intermediaries
was in operation. The families gave gifts to the tribal leaders;
the tribal leaders delivered them to Moses; Moses delivered

14. Robert B. Revere, “‘No Man's Coast’; Ports of Trade in the Eastern Mediter-
ranean,” in Trade and Market in the Early Empires: Economies in History and Theory,
edited by Kari Polanyi, Conrad M. Arensberg, and Harry W. Peterson (Chicago:
Regnery, (1957) 1971), ch. 4.

15. Cyrus H. Gordon, The Common Background of Greek and Hebrew Civilizations
(New York: Norton, 1965), ch. 5.

16. Ibid., p. 130.

17. See North, Leviticus, pp. 23-34,
