270 SANCTIONS AND DOMINION

Israel shall hear, and fear, and shall do no more any such wickedness
as this is among you (Deut. 13:6-11).

The threat of this civil sanction was regarded by the Mosaic
law as sufficient to warrant marriage to a female survivor of an
annihilated foreign culture.

Two Forms of Liberating Subordination

Forcible covenantal subordination of foreigners was possible
in two ways: by purchase (slavery) and by captive marriage. The
covenantal goal in both cases was the servant's separation from
bondage to pagan gods. Slavery was a form of evangelism:
when successful, far better than eternity spent in hell. The
captive woman who entered a household by marriage was not
to be treated as a slave. She could not be sold by her husband.
She had been humbled once by the shaving of her head, the
paring of her nails, and her physical status as a wife. She could
not lawfully be humbled a second time by the same man. She
had made the transition out of paganism through marriage; she
therefore could not be made a slave by the man who had
brought her out of paganism. This indicates that the covenantal
purpose of both the permanent servitude of, and marriage to,
captive women was liberation out of paganism. A pagan could
be liberated in either way, but not both. Liberation was a one-
time act.

This means that an ex-pagan wife could not become a per-
manent slave in Israe] unless she was divorced by her husband
and later married a permanent slave or a pagan who then
became a permanent slave through crime or debt. Her status as
a free woman had been guaranteed by her first husband. She
could give up this status through marriage to 4 man who did
not possess it, but it could not be removed from her by law
without her consent unless she was excommunicated for cause
and then committed a crime whose restitution payment re-
quired her sale into slavery. Even this limit to her status is
