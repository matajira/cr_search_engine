146 SANCTIONS AND DOMINION

more likely, namely, that of the professional stick-gatherer. His
work would be most in demand on the sabbath, precisely the
day on which the prohibition against work was enforced. A
woman who failed to gather sticks earlier in the week could buy
some from a professional.

We are not told that the man in. Numbers 15 was such a
professional, but the severity of the punishment clearly would
have made it far more dangerous for such a class of profession-
als to have come into existence. There was a need for a harsh
penalty, men and women being what they are. There is always
a delight in violating God’s commandments if one is a sinner;
if that violation also brings with it certain superficial benefits
above and beyond the mere pleasure of defiance, so much the
better. Sabbath prohibitions involved heavy costs for the obedi-
ent; enforcement of the sabbath required stiff penalties, thus
burdening violators with high costs in the form of high risk.

What were the costs of the sabbath? For the man, it was the
forfeiture of all income — monetary (less likely in a rural soci-
ety), psychological, or in physical property — for that day. But
women also paid. They had to gather all sticks earlier in the
week, It meant more work during the week, either in longer
days or by increasing the intensity of the working day - or
both. Had the working day not been lengthened or intensified,
then other tasks which it was desirable to accomplish would
have to have been foregone, and that, as any wife knows, also
involves costs (especially if a husband or a mother-in-law notices
the failure in question). There would always be a temptation to
forego the gathering of sticks during the week, especially if a
professional would come by with a load of wood on the sabbath
for a reasonably cheap price. If his price was less than the
woman's estimation of the costs involved in gathering the wood
earlier in the week, a bargain was to be expected. By imposing
a rigorous and permanent form of punishment on the violator,
the community was able to force up the price of the sticks; risks
would be so high that few protessionals could survive. How
