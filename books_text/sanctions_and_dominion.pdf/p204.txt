166 SANCTIONS AND DOMINION

agricultural. Local leaders could seek alliances with the Levites
against the central government without feeling threatened. This
acted as a restraint on political centralization.

The Levites received their income from local residents. This
made them dependent on the prosperity of local populations.
If residents in one region had wanted to establish restrictions
on the export or import of goods, the local Levites might have
approved. But there were Levites in other regions who had
different interests. The priests would decide the limits of eccle-
siastical interpretation, but there would have been a tendency
for the national priesthood to balance the interests of ali in
order to keep peace in the tribe. This would have tended to
favor the development of a national body of legal precedents
that did not benefit one region at the expense of another. This
would have consolidated legal opinion in ecclesiastical matters,
but not on the basis of one regionally dominant tribe’s interests.
This consolidated body of precedents would have reflected the
opinions of all the regions, since Levites resided in each region.
The regionalism of the tribes was offset by the nationalism of
Levi. At the same time, the potential regionalism of the priest-
hood in Jerusalem was offset by its economic interests. The
priesthood maximized its income and its political independence
by maximizing the income of the whole nation, since the priest-
hood was paid a tithe of the tithes received by the Levites (vv.
26-28).

Fee for Sacred Services Rendered.

The Levites were forbidden to cheat the priesthood by send-
ing the dregs of their produce. They had to send the best: “Out
of all your gifts ye shall offer every heave offering of the Lorp,
of all the best thereof, even the hallowed part thereof out of it”
(v. 29), The priests were to be well-paid. They were the senior
representatives of the nation before God. To cheat them was to
cheat God. If the nation did this, God would bring negative
sanctions against them: “Will a man rob God? Yet ye have
