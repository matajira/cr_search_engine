How Large Was Israel’s Population? 349

To reproduce themselves, Joshua’s generation required 400,000
sons. (Their grandsons could supply the 200,000 replacements
for Moses’ generation.) The 400,000 sons meant replacement-
rate mode: one son per household. So, about 22,000 sons born
in one year seems reasonable. If this birth rate had continued
for two decades (ages 20 to 40), this would have produced a
little over 400,000 sons.

Yet even if all chis did happen, it does not solve the more
fundamental problem: Where did the 400,000 men of Joshua's
generation come from?

In every plausible scenario, the expositor has to rely on the
adoption argument to make sense of the numbers. Bible-believ-
ing expositors have generally avoided dealing with this demo-
graphic problem. Jordan accepts this with respect to the origi-
nal families, but Keil did not mention it. Biological reproduc-
tion rates do not allow the kind of population growth required
to get from the 70 males who came down to Egypt in the fam-
ine to the 600,000 who were numbered. The texts indicate that
Joshua’s was the third generation: Kohath, Moses, Joshua.
Conclusion: servants must have been adopted into the original
families. Israel then spread out through the land of Goshen.

Could Keil’s thesis be modified to include prior adoptions?
Yes. This would not change his basic point regarding the mean-
ing of firstborn in Numbers 3. But to adopt this solution, we
must assume a drastic reduction of births, i.e., a drastic reversal
of the previous experience of growing families. There was no
population echo in the wilderness. This means that Joshua's
generation either suffered drastically lower birth rates than
their parents or else more of their children died before reach-
ing maturity. A decreased birth rate could have been the effect
of the persecution.

The main problem with Keil’s thesis is the language that
God used to explain His substitution of the Levites for the
firstborn: “And I, behold, I have taken the Levites from among
the children of Israel instead of all the firstborn that openeth
