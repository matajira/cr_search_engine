282 SANCTIONS AND DOMINION

more suitable for cattle ranching than farming, they would reap
an entrepreneurial reward. Because they knew that members of
the tribes legally would not be allowed to buy or sell land to
non-members ~ the jubilee land law (Lev. 25) - they under-
stood that capitalization in their case would go from the land to
their cattle ranching skills. Land ownership in Israel would be
less mobile than cattle ownership. To maximize the value of
their skills in cattle ranching, they needed ownership of special-
ized land. They would not be allowed to buy comparably spe-
cialized land after the conquest. This land’s highest valued use,
they believed, was for cattle ranching. Anyway, for their pur-
poses, this was its highest valued use. They could best capitalize
their cattle raising skills by owning this land rather than tracts
of land across the Jordan. So, they made their offer to Moses.

The subsequent land distribution indicates that the two
tribes represented half the tribe of Manasseh. “And Moses gave
unto them, even to the children of Gad, and to the children of
Reuben, and unto half the tribe of Manasseh the son of Joseph,
the kingdom of Sihon king of the Amorites, and the kingdom
of Og king of Bashan, the land, with the cities thereof in the
coasts, even the cities of the country round about” (Num.
32:33). Thus, we should conclude ‘that half of Manasseh also
had reasons for preferring Gilead. They may have been cattle
specialists. They may have had another reason. In any case,
they preferred to: join their brethren on the wilderness side of
the Jordan.

Covenant Before Contract

Moses had a ready reply. It was the reply of a military com-
mander. “And Moses said unto the children of Gad and to the
children of Reuben, Shall your brethren go to war, and shall ye
sit here? And wherefore discourage ye the heart of the children
of Israel from going over into the land which the Lorp hath
given them?” (vv. 6-7). He understood that the cost of conquest
was the shedding of blood. God had given the Israelites their
