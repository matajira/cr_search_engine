328 SANCTIONS AND DOMINION

churches. There is no eschatological neutrality in the Bible.
Premillennialism, amillennialism, and postmillennialism cannot
all be true. If they are said to be judicially equal, then eschatol-
ogy is necessarily reduced to the status of adiaphora. A church
that is not postmillennial is like the generation of the exodus:
fearful of judicial and cultural victory, committed to wilderness
wandering as a way of cultural life, and hostile to those who,
like Caleb and Joshua, predict inevitable victory in history.

To discuss eschatology apart from a consideration of God’s
law and historical sanctions is to ignore the covenant’s struc-
ture. The covenant is a unified system. It cannot be broken
analytically and still retain its authority. Any consideration of
inheritance, either in eternity or in history, has to include the
doctrines of sanctions, law, authority, and the sovereignty of
God. A discussion of eschatology apart from historical sanctions
is as misleading as a discussion of prophecy apart from the
sovereignty of God. To say that something must happen in the
future while asserting that man is totally free to choose a differ-
ent future is covenantally absurd. It is equally absurd covenan-
tally to discuss eschatology without discussing sanctions: cov-
enantal cause and effect. It is also covenantally absurd to dis-
cuss God’s historical sanctions without discussing God’s law.
The covenant is a unit. It cannot be broken.

Postmillennialists can afford to be patient. They understand
that the future will bring victory for Christ’s church in history.
Christendom will be established in history. So, they can afford
to do the work of dominion inside the boundaries of eschat-
ologically pluralist churches. They know that when victory
becomes visible over time, the defenders of pessimillennialism
will face a much smaller audience. Most people prefer success
to failure, dominion to martyrdom. They understand and be-
lieve the economist’s dictum: “It is better to be rich and healthy
than it is to be poor and sick (other things being equal).” Pessi-
millennialism is popular when things are going badly for the
church and kingdom. It offers deliverance out of history: rap-
