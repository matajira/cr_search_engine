60 SANCTIONS AND DOMINION

geographically sacrosanct outside of the areas associated with
the throne room of God. The holy of holies housed the highest
of highs, associated with God’s mountain.* Like Mt. Sinai,
where God gave ihe law to Moses, so was the tabernacle. Jor-
dan writes: “The boundary around the mountain correlates to
the boundary inside the courtyard that kept the people from
approaching the altar. In this way, then, the Tabernacle (and
later the Temple) were models of the ladder to heaven, of the
holy mountain. . .. The Tabernacle was God's portable moun-
tain.”*

Circles of Authority

The association of the inner circles of the tabernacle with
hierarchy, secrecy, and authority was to make the sacred space
of the tabernacle foreboding to outsiders. The sense of power
was inescapable. The circles of authority grew more diffuse as
they moved away from the tabernacle. Hierarchy meant access
to power; centralization meant access to power. To be a part of
the inner circle meant access to power.

God closed access to these inner circles to those not part of
specific families: Aaron, Kohath, Gershon, and Merari. Apart
from adoption into one of these families, which involved the
surrender of a man’s inheritance in the land, and also involved
the payment of an entry fee,® access to these inner circles was
closed. There could be no competition for such access between
excluded families.

The Levites lived in a camp separated from the other tribes
by 2,000 cubits’ (Josh. 3:4). Non-priestly Israelites were to be
kept away from the inner courts of the tabernacle by the Le-

4, James 6. Jordan, Through New Eyes: Developing a Biblical View of the World
(Brentwood, Tennessee: Wolgemuth & Hyatt, 1988}, pp. 85, 155-163, 212-13,

5. Ibid., p. 218.
6. Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute for
Christian Economics, 1994), ch, 36.

7, About 1,000 yards (914 meters).
