254 SANCTIONS AND DOMINION

tion."* The New Testament Greek word for oath is horkos:
fence.'®

There was an example of such an oath in the persecution of
Paul by a group of Jewish fanatics. “Then the chief captain took
him by the hand, and went with him aside privately, and asked
him, What is that thou hast to tell me? And he said, The Jews
have agreed to desire thee that thou wouldest bring down Paul
to morrow into the council, as though they would enquire
somewhat of him more perfectly, But do not thou yield unto
them: for there lie in wait for him of them more than forty
men, which have bound themselves with an oath, that they will
neither eat nor drink till they have killed him: and now are
they ready, looking for a promise from thee” (Acts 23:19-21).
The Greek word for “oath” in this case meant anathematizing,
ie., selmaledictory. It was an invalid oath, and those who took
it either broke it or starved themselves to death. They did not
kill Paul.

The Christian’s word is so bounded by authority that his
words should be trusted apart from any invocation of supernat-
ural hierarchy. The Pharisees did this on a regular basis. They
also adopted peculiar rules regarding what words constituted a
valid invocation.

Woe unto you, ye blind guides, which say, Whosoever shall swear by
the temple, it is nothing; but whosoever shall swear by the gold of the
temple, he is a debtor! Ye fools and blind: for whether is greater, the
gold, or the temple that sanctifieth the gold? And, Whosoever shall
swear by the altar, it is nothing; but whosoever sweareth by the gift that
is upon it, he is guilty. Ye fools and blind: for whether is greater, the
gilt, or the altar that sanctifieth the gift? Whoso therefore shall swear
by the altar, sweareth by it, and by all things thereon. And whoso shall

14, Gary North, Leviticus: An Economic Commentary CIyler, Texas: Instivute for
Christian Economics, 1994), ch. 6.

18. The third commandment relates to houndaries: point three of the biblical
covenant model. It parallels the eighth commandment, which prohibits theft.
