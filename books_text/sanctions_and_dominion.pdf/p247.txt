The Office of Court Prophet 209

laam; this had nothing to do with Israel's protected covenantal
status.

At What Price?

The economist shares Balak’s view of pricing. He may not
argue that.every man has his price, but he argues that at the
decision-making margin, every man will evaluate a price. The
decision-maker will count the cost of saying xo to the offer. His
cost of saying no is whatever he might have gained by saying
yes, minus whatever it would cost him to say yes and fulfill his
contract.

Ifa future-oriented individual believes that God will impose
hegative sanctions on a scale beyond calculation, he will refuse
to say yes to an offer that endangers his future. Jesus asked:
“For what shall it profit a man, if he shall gain the whole world,
and lose his own soul?” (Mark 8:36). By this He meant that the
cost-benefit ratio of a soul-threatening decision is averwhelm-
ingly on the side of righteousness. The cost of doing evil is too
high. The magnitude of the discrepancy is so great that gaining
the whole world would be a bad bargain. This understanding
had governed His own response to Satan’s temptation: “Again,
the devil taketh him up into an exceeding high mountain, and
sheweth him all the kingdoms of the world, and the glory of
them; And saith unto him, All these things will I give thee, if
thou wilt fall down and worship me. Then saith Jesus unto him,
Get thee hence, Satan: for it is written, Thou shalt worship the
Lord thy God, and him only shalt thou serve” (Matt. 4:8-10).

What this means is that for some decision-makers, the net
cost of saying no to evil is so minuscule as not to be a factor.
The person's fear of God is so great that the price offered does
not register on his scale of values. At what price will a mother
murder her infant son? For some mothers, no such price exists.
For others, it does - a prophetic mark of covenant-breaking.'

 

. “Then 1 will walk contrary unto you also in fury; and I, even 1, will chastise
