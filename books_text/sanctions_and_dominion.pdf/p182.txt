144 SANCTIONS AND DOMINION

fices reminded them that God was willing to wait for His lawful
rewards. They were, too.

Planning Ahead

The law of the sabbath was specific: no work was to be done
on the sabbath. “Ye shall keep the sabbath therefore; for it is
holy unto you: every one that defileth it shall surely be put to
death: for whosoever doeth any work therein, that soul shall be
cut off from among his people. Six days may work be done; but
in the seventh is the sabbath of rest, holy to the Lorp: whoso-
ever doeth any work in the sabbath day, he shall surely be put
to death” (Ex. 31:14-15; cf. 35:2). The law was specific, but
what constituted lawful labor? A case-law application was need-
ed here. Israel soon got one:

And while the children of Israel were in the wilderness, they found
a man that gathered sticks upon the sabbath day. And they that found
him gathering sticks brought him unto Moses and Aaron, and unto all
the congregation. And they put him in ward, because it was not de-
clared what should be done to him. And the Lorp said unto Moses,
The man shall be surely put to death: all the congregation shall stone
him with stones without the camp. And all the congregation brought
him without the camp, and stoned him with stones, and he died; as the
Lorp commanded Moses (Num. 15:32-36).

The stick-gatherer'was working on the sabbath. Perhaps he
was planning to start a fire. This was illegal on the sabbath. “Ye
shall kindle no fire throughout your habitations upon the sab-
bath day” (Ex. 35:3). Perhaps he had run low on fuel. This also
was no excuse. In fact, running low on fuel was Jesus’ model of
the well-intentioned soul who does not plan ahead regarding
the final judgment: the parable of the ten virgins, half of whom
forgot to store up oil for the time when the bridegroom would
return (Matt, 25:1-13). Perhaps he was doing this to re-sell the
sticks later. In this case, he was working commercially. One
