Dividing the Inheritance 227

We are not told how the surveyors knew precisely which
land belonged to which tribe, nor are we told if there had been,
or would be, a demographics-based reallocation after the tribal
lots were cast (Abravanel). We are not told whether a post-
survey reallocation took place. What we are told is that there
had been a previous casting of lots, as indicated-by the com-
plaint by the heirs of Joseph, as well as by the fact chat the
surveyors knew generally where their tribes’ inheritances were.
We also know that another casting of lots lay ahead.

Rashi argued that God would have decided where the lots
fell, with large tribes receiving large inheritances. His view is
theologically conceivable, though not very plausible. (Why had
Moses brought up the family. size issue if God would decide all
this?) Nachmanides argued that the tribal plots were equal.
Abravanel argued that only the tribal regions were determined
by lot.

Nachmanides’ position has to be rejected, at least with res-
pect to family plots. His approach assumed tribal plots of equal
size and allocated by lot, with family plots that varied in size in
terms of family size. But why would there have to be an addi-
tional round of lot-casting? If the clan or family allocations were
based strictly on need, what role did lot-casting play? With
respect to family plots, we are left with some variation of Abrav-
anel’s approach: lot-casting to determine general location, plus
a subsequent reallocation in terms of family size.

This still does not answer the question: Were the tribal allo-
cations equal in size? Ethically speaking, if it was proper to
reallocate land for families after the lots had been cast, then
there seems nothing wrong with applying this principle to
tribes. This does not mean that this was done.

We are not told whether tribal territorial units were equal in
area. We are told that family units were unequal. Joshua’s
comment indicates that lot-casting in some way applied to the
secondary, intra-tribal allocations. What, then, is meant by
Numbers 33:54? “And ye shall divide the land by lot for an
