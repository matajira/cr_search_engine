70 SANCTIONS AND DOMINION

Moses criticized Korah for the latter’s refusal to be content
with the high degree of authority that God had given to the
Levites: “And Moses said unto Korah, Hear, I pray you, ye sons
of Levi: Seemeth it but a small thing unto you, that the God of
Israel hath separated you from the congregation of Israel, to
bring you near to himself to do the service of the tabernacle of
the Lorp, and to stand before the congregation to minister
unto them? And he hath brought thee near to him, and al] thy
brethren the sons of Levi with thee: and seek ye the priesthood
also?” (Num. 16:8-10). But Korah wanted even more authority.

Moses declared himself to be a prophet in terms of God’s
immediate application of negative sanctions: “And Moses said,
Hereby ye shall know that the Lorp hath sent me to do all
these works; for I have not done them of mine own mind. If
these men die the common death of all men, or if they be
visited after the visitation of all men; then the Lorp hath not
sent me. But if the Lorp make a new thing, and the earth open
her mouth, and swallow them up, with all that appertain unto
them, and they go down quick into the pit; then ye shall under-
stand that these men have provoked the Lorp” (Num. 16:28~
30). The Old Covenant prophet was a prophet because God’s
negative sanctions predictably followed the prophet’s declara-
tion of a covenant lawsuit. This is why the office no longer
exists under the New Covenant, and also why the negative civil
sanction of capital punishment for false prophecy (Deut. 18:
20-21) no longer applies. The Old Covenant’s rigorous tempo-
ral predictability no longer exists.

God’s visible judgment against Korah made it clear that the
Mosaic Govenant’s hierarchy of priestly authority was a hierarchy
of power. This is because it was a hierarchy of sacramental holiness.
This hierarchy of holiness was the basis of a division of labor in
the service of God. Not everyone had the authority to approach
God's inner sanctum by passing through the concentric circles
of holiness that surrounded it. Korah had proclaimed the doc-
trine of equal holiness. God pronounced visible judgment
