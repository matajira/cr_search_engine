44 SANCTIONS AND DOMINION

Immediately upon receiving Joab’s report, David knew he
had done a sinful thing. “And David said unto God, I have
sinned greatly, because I have done this thing: but now, I be-
seech thee, do away the iniquity of thy servant; for I have done
very foolishly” (I Chron. 21:8). God then gave David three
terrible choices (v, 12). David told God to decide (v. 13); so,
God brought a plague against the people, killing 70,000 of
them (v. 14). This was consistent with the law of mustering.
“When thou takest the sum of the children of Israel after their
number, then shall they give every man a ransom for his soul
unto the Lorp, when thou numberest them; that there be no
plague among them, when thou numberest them” (Ex. 30:12).
Plague came because David mustered the people without col-
lecting the mandatory atonement money for the priests. This
mustering invited God into their midst as the sanctions-bringes,
but they made no payment. They thereby became profane.

Why did Joab know that the mustering was wrong? Because
no priest had consented to it. No blood money had been paid
to the priesthood. The act was clearly sacrilegious: a profane act
because it violated a sacred boundary. But what could that
boundary have been? It had something to do with the non-
payment of blood money. It had something to do with the
priesthood. Mustering was to precede a holy war. David was
not facing a holy war, yet he mustered Israel’s fighting men.
This was an assertion of a priestly authority that he possessed
only as the national military leader in a time of war. David was
the senior military commander, the one under whom blood
would be shed. He was the senior priest of the military, under
the authority of the high priest. He did not possess this must-
ering authority as senior civil magistrate. This authority was
priestly, not kingly. Thus, it was illegal for the civil government
to conduct this census. It was an assertion of priestly authority
that was legitimate only prior to a holy war.

Joab told the king: “Now the Lorp thy God add unto the
people, how many soever they be, an hundredfold, and that the
