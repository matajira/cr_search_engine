236 SANCTIONS AND DOMINION

giving him a double portion of all that he hath: for he is the
beginning of his strength; the right of the firstborn is his”
(Deut. 21:15-17).

The eldest son received a double portion. The question is:
Why? There are two reasons: judicial and economic. Judicially,
the eldest son was the first son to bear his father’s name. He
was a testimony from God to the man that God had decided to
bless the man’s name in Israel. The son would have a place in
the national covenant in the next generation. He would rule
over his brothers until they departed from the family’s land.
Isaac blessed Jacob, thinking that Jacob was Esau, his firstborn:
“Let people serve thee, and nations bow down to thee: be lord
over thy brethren, and let thy mother’s sons bow down to thee:
cursed be every one that curseth thee, and blessed be he that
blesseth thee” (Gen. 27:29). This was a matter of judicial au-
thority, The firstborn normally replaced his father’s rule over
the family. Second, economic: with greater blessings come
greater responsibilities. This is a basic biblical principle of per-
sonal responsibility. The eldest son presumably had to bear the
primary responsibility in supporting his fecble parents in their
old age. The Bible does not say this explicitly; it is a conclusion
based on the fact of the double portion. This conclusion is
consistent with the principle announced by Christ: “For unto
whomsoever much is given, of him shall be much required: and
to whom men have committed much, of him they will ask the
more” (Luke 12:48b).

The only way that a man could unilaterally disinherit a son
was to disinherit all of his children: to cut off his family’s name
in the tribe. He could do this by pledging his land to a priest
and then breaking the pledge by remaining in control of the
land and its income. At the jubilee, the land went to the priest
if the man failed to redeem the land from the priest (Lev.
27:19-21).* This was the only way that a priest could become

4. North, Levitiens, pp. 608-11.
