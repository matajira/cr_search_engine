Blessing and Naming 109

sanctions. The sons of Aaron were in closest proximity to God’s
earthly throne: the Ark of the Covenant. Thus, they were clos-
est to His earthly sanctions. They could lawfully invoke His
positive sanctions because they lived in proper fear of His nega-
tive sanctions. They knew what had happened to Nadab and
Abihu: strange fire had brought consuming fire (Lev. 10:1-2).

Those who submitted themselves to the authority of the sons of
Aaron submitted themselves to God’s name. This was the judi-
cial basis of their participation in the predictable corporate
blessings of God under the Mosaic Covenant. What was this
name? With respect to His general authority over history, His
is the self-existent name: “And Moses said unto God, Behold,
when I come unto the children of Israel, and shall say unto
them, The God of your fathers hath sent me unto you; and
they shall say to me, What is his name? what shall I say unto
them? And God said unto Moses, I AM THAT I AM: and he
said, Thus shalt thou say unto the children of Israel, 1 AM hath
sent me unto you” (Ex. 3:13-14). With respect to His special
manifestations within Israel's history, He was the God of Abra-
ham, Isaac, and Jacob. He was, judicially speaking, the God of
the covenant. This covenant extends across time. At the close of
his life, Moses told the generation of the conquest:

That thou shouldest enter into covenant with the Lorp thy God,
and into his oath, which the Lorp thy God maketh with thee this day:
That he may establish thee to day for a people unto himself, and that
he may be unto thee a God, as he hath said unto thee, and as he hath
sworn unto thy fathers, to Abraham, to Isaac, and to Jacob. Neither
with you only do I make this covenant and this oath; But with him that
standeth here with us this day before the Lory our God, and also with
him that is not here with us this day (Deut, 29:12-15).. . . Lest there
should be among you man, or woman, or family, or tribe, whose heart
turneth away this day from the Lorp our God, to go and serve the gods
of these nations; lest there should be among you a root that beareth
gall and wormwood; And it come to pass, when he heareth the words
of this curse, that he bless himself in his heart, saying, I shall have
