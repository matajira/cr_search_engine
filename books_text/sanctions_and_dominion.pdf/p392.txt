354 SANCTIONS AND DOMINION

original Israelite family. It made economic sense only if they
actually planned to invade Canaan. The only way for the eco-
nomics of adoption to have paid off for the exodus generation
was for the nation to have invaded Canaan immediately, there-
by receiving its inheritance. The cost of two atonement pay-
ments could have been recovered only through military con-
quest. But after two numberings, the nation suffered a failure
of nerve. The inheritance was delayed for another generation.

What if the mixed multitude had been adopted on the day
after the death of Egypt’s firstborn? They were not part of the
original Passover, but perhaps they participated in the spoiling
of the Egyptians.” This is difficult to imagine: a mass adoption
of foreigners followed that very day by a shared inheritance.
This would have meant that the adoptees paid their own atone-
ment money, and also that the per capita wealth extracted by
the spoiling was vastly smaller. But this does not change the
economics of the mustering process. The adoptees were sup-
plied with the atonement money they needed to pay the Lev-
ites. Either the Egyptians gave it to them directly or the Israel-
ites did. In cither case, the original Israelite families, other than
the Levites, wound up with far less wealth than if there had
been no adoption.

The mandated payment enforced a huge transfer of wealth
from the 12 tribes to the priests and the tabernacle. This also
indicates that the Israelites had stripped Egypt of an immense
treasure: large enough to fund the payment of three wilderness
numberings of mostly adopted foreigners, plus the voluntary
offering prior to the building of the tabernacle.

Because of the number of Israelites slain by the Levites after
the golden calf incident, I believe that the mass adoption may

31, We say “the Egyptians,” but we mean something more circumscribed: those
Egyptians living dose to the Pharaoh's court. These were the leaders of the nation.
It was they who supplied the enormous quantity of gold and silver used later by
Israel to build a golden calf, build the tabernacle, and pay for three musterings. To
say that these Egyptians had been rich is not putting it strongly enough.
