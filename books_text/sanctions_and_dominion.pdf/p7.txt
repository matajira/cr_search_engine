TABLE OF CONTENTS

Preface 0... ee ee eens ix
Introduction 6... 0. ee ce eee eens 1
1, Mustering the Army of the Lord ...............- 14
2. Military Planning vs. Central Planning ............ 23
3. The Hierarchy of Sanctions/Service .............- 55
4. The Firstborn and Negative Economic Sanctions .... 82
5. Blessing and Naming ................0 0000. 100
6. The Altar Offering ©... 00... 0. cece eee eee 115
7. Progressive Whining and Final Accounting ........ 125
8. Deferred Gratification .... 0.6.6.6. eee eee 140
9. Evaluation and Sanctions .....0 0.0.0... 00.0000 149
10. Tithe and Sanctions .. 6... 0... eee ee 157
11. The Lure of Magic... 0.0.6 cece 175
12. The Psychology of Victory ...... 6.00.00 c eee 191
13. The Office of Court Prophet ..............2... 203
14. Dividing the Inheritance. ... 0.0.00... 0000 e eae 216
15. Bloodline Inheritance ............ 000000000 ee 231
16. Oath and Sanctions .... 0.0.66. 242
17. War Brides... 0. cc cee 265
18. The Spoils of Holy Warfare ..... 0.6.00. c eee 273
19. Land to Match Men’s Skills... 6.2.0.0. ..-. 00005 278
20. Sanctions and Inheritance ......... 000000000. 292
21. Cities of Refuge «2.0.2. 299
22. Tribal Inheritance 0.6.0... - 6c eeeeeeeeeee 311
Gonclusion 60... ce ete 318
Appendix: How Large Was Israel’s Population? ....... 330
Scripture Index 2.0.0... cee eee es 370

Index 2... eee eee 382
