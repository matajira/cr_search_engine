392 SANCTIONS AND DOMINION

opinion, 79, 102, 103, 158
priesthood &, 238
rule of, 40
sacred space, 61
sanctions &, 40, 103, 325
seed, 233, 317
value scale, 128
leadership, 319-21
Lenin, V. L., xxxvii
levirate marriage, 239
Levites
ages of service, 120-21
atonement money, 31—
32, 35-36, 91
authority, 64, 119
blood avengers, 98
cities, 165
decentralization, 165-66
defended Ark, 34-35
dependent, 72-73
disadvantage, 35
division of labor, 116
golden calf, 21
guardians, 35-36
influence, 237
inheritance, 35-36, 237
legal counsellors, 165-66
mediatorial authority,
119
monopoly, 71, 73, 165
numbering, 30-32
offered to God, 119-20
post-exilic, 66n
regionalism, 166
risk, 165, 324
sanctions, 57
services, 71, 73
singing, 73

substitutes, 91
sword, 35, 57, 59, 61
tabernacle, 62-64
tithe, 65-68, 72-73, 165
tradesmen, 64
urban, 71-72
Leviticus (holiness), 5
Lewis, C. S., 214
liability, 310
liberalism, 78
liberation, 270
liberty, 76, 79, 128, 314
localism, 30
lot, 217-19, 221-22, 223-24,
226-27, 229
Luther, Martin, xiv-xv
Lutheranism, xvii

Machen, J. Gresham, xviii-xix

magic, 186-88, 190, 203

Manasseh, 282, 284

manna, 58, 80, 127, 130, 138,
142, 194, 332, 352

marriage, 265-66, 313-15

Marshall, John, 172

Marx, Karl, xxiv

masters, 319-21

McCalman, David, 224n

McDonald’s, 163n

meat, 161-63

meek, 13, 326

Merari, 63, 116, 124

mercy, 129n, 135

mercy seat, 58n

Micaiah, 112-14

Michels, Robert, 76

Midian, 204, 215, 265-66,
273-77, 293, 334
