Oath and Sanctions 263

not lawfully be invoked to authenticate a personal oath in pub-
lic unless there is a covenantal agency empowered by God to
bring negative sanctions against the oath-taker: church, family,
or State. A personal oath to God apart from a sanctioning agen-
cy must not be sworn in public. It must be sworn in private.

Under the Mosaic covenant, a wife or daughter had to have
her oath confirmed by her husband or father in order for it to
be binding, unless she governed her own household. The wid-
ow was the representative independent woman: head of her
own household. In New Covenant times, because of baptism’s
lawful application to both sexes, adult daughters are legally
independent; so, they possess this covenantal authority. When
a judicially independent woman pays for the fulfillment of her
oath, she can lawfully take a vow. God will hold her to it.

A promise that does not invoke God’s name or sanctions is a
contract. The presence of a self-maledictory oath distinguishes
a covenant from a contract. A contract does not possess the
sanctified status of a covenant. Breaking a contract that results
in harm to another person is a sin, but it is not of the same
magnitude as breaking an oath-invoked covenant. Contracts are
not to be elevated to the status of covenants. Covenants are not
to be debased to the level of contracts. Covenantalism is the
biblical basis of contractualism, not the other way around. Whig
political theory since Adam Smith has failed to understand this
crucial aspect of causality: from covenant to contract. Whig
social theory places contractualism at the center.

The civil government is a covenantal institution. While con-
tractualism is valid for explaining economic relationships — the
quest for mutual self-interest by means of the division of labor
— it is not valid as a form of political theory, despite the pres-
ence of personal self-interest and the division of labor in politi-
cal affairs. Civil government has the God-given monopolistic
authority to impose physical sanctions on evil-doers: the sword.
The free market does not legitimately authorize any agency to
