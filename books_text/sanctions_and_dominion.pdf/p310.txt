272 SANCEIONS AND DOMINION

cends political groupings. It crosses borders. This was true in
the post-exilic era, too. Jews spread throughout the empires of
the Near East, residing under the civil rule of many gods. The
Mosaic laws of warfare ended with the end of Israel as a sepa-
rate civil jurisdiction. The total annihilation of Canaan was a
one-time genocidal requirement. The Mosaic laws of near-anni-
hilation also ended when the boundaries of Israel no longer
served to identify the monopolistic civil jurisdiction of God
within the confines of a single nation.

Conclusion

The destruction of Midian involved the capture of virgin
girls. These women were marriageable. Their eligibility for
marriage was what authorized Israel to spare their lives. They
were no longer seen as a threat to Israel’s covenant. The gods
of their cities were covenantally dead. The captives recognized
this. They had no covenantally significant religion to invoke
against Israel.
