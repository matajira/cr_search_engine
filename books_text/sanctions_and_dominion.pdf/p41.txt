Introduction 3

wages, and interest-rent’ can be invested. If these investments
are based on accurate forecasts of the future, and if they are
implemented on a cost-effective basis, they produce an expan-
sion of capital, which is a tool of dominion. With greater capi-
tal, more of the earth can be brought under mankind's domin-
ion. The positive feedback of compound growth, if extended
over time, becomes the basis of economic transformation and
the conquest of nature, or as economic historian John U. Nef
put it, the conquest of the material world.’

We conclude that one of the foundations of mankind’s fulfill-
ment of the dominion covenant (Gen. 1:26-28) is long-term
economic growth. Without the possibility of remvested earnings
and the growth of capital - above all, accurate information and
the social means of implementing it - there would be no way
for mankind to extend God’s kingdom across the face of the
earth, transforming nature to reflect the covenantal, hierarchi-
cal rule of God in history through His ordained agent, man.
The idea of an “unspoiled nature” that has not been influenced
by man and reshaped by man in terms of man’s desires and
needs is an anti-biblical concept. God made it plain to Israel:
better the rule of covenant-breaking Canaanites than the rule of
nature. “And I will send hornets before thee, which shall drive
out the Hivite, the Canaanite, and the Hittite, from before thee.
I will not drive them out from before thee in one year; lest the
land become desolate, and the beast of the field multiply
against thee. By little and little I will drive them out from be-
fore thee, until thou be increased, and inherit the land” (Ex.
23:28-30). In other words, God’s negative sanctions against
Canaan were to be delayed so that the land would not fall

4. Rent is another word for interest. It arises from the same phenomenon: the
discount which all men apply always in the present to the value of expected future
income. See Ludwig von Mises, Human Action: A Treatise on Economics (New Haven,
Connecticut: Yale University Press, 1949), ch. 19,

5. John U. Nef, The Conquest of the Material World: Essays on the Coming of Industri-
alism (Cleveland, Ohio: Meridian, [1964} 1967),
