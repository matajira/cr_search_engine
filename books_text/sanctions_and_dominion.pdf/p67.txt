Military Planning vs. Central Planning 29

Church and State in Wartime

Man is not God, No numerical census will ever equal God’s
omniscience. No substitute for omniscience will ever approach
God’s omniscience as a statistical limit. No expenditure of eco-
nomic resources in data-gathering will ever replace reliance on
God’s covenantal sanctions in history. The creature will remain
a creature. The quest for omniscience is therefore an unholy
quest. Omniscience is an illegitimate goal. This is one reason
why God placed strict limits on mustering. It was not a normal
event. It was done only prior to holy warfare, except for the
instance of the mustering in Exodus 38, which may have been
a retroactive atonement for the golden calf.

God told Moses and Aaron to muster the entire adult male
population, family by family. Aaron’s presence was mandatory.
Mustering in Mosaic Israel was an act preparatory to holy war.
No holy war could be called without the consent of the high
priest, for the Aarontc priests had to blow the silver trumpets
that assembled the nation (Num. 10:2, 8). Blowing only one
trumpet gathered the princes (v. 4). Both trumpets had to be
blown to assemble the whole nation at the tabernacle (v. 3).
This twin trumpet signal sounded the alarm. A second blowing
of both trumpets by the priests launched the army on its march
(vy. 5-6). Without the participation of the priests, no holy war
could be launched. This gave the priesthood a veto over national
military action. No military mustering was legal without their
participation.

Moses was present in his capacity as a civil ruler. He was the
representative of the nation in its judicial capacity. So, in the
case of holy warfare, the supreme civil commander functioned
in a priestly capacity, even though he was not a priest. We
might call this quasi-priestly authority: legitimate power over
life and death. The entire nation is at risk. In wartime, the
senior military commander lawfully possesses such quasi-priestly
authority. He does not possess it in peacetime, except in prepa-
ration for war.
