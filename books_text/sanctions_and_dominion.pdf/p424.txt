386 SANCTIONS AND DOMINION

Darius, 108

Darwinism, 257-62

Dathan, 149, 164

daughters’ inheritance, 234,
239, Chap. 20

David, 38-39, 42-45, 84, 110,
250

dead body, 121-22

death, xii-xiii, xxxvii, 8, 57, 87,
197

death penaity, 303

Deborah, 38n, 246, 288-89

Decalogue, xi

decentralization, 165-66, 237,
241, 314

defeat, xxxv

deferred gratification, 27,

Chap. 8

democracy, 69, 76, 79, 149

dentistry, 79

despair, 107

Deuteronomy (point five), x,
192

devoted item, 159, 193

disinheritance, 7-9, 235-38,
325 (see also inheritance)

Disney World, 281

Disneyland, 280-81

division of labor, 61-64, 68-71,
116, 280

divorce, 256

Dolan, Edwin, 288

dominion (capital &), 3

dominion covenant, 3, 77

donkey, 89, 93-95, 211-12

double portion, 85, 236, 239

Douglas, Jack, 172

dowry, 313

drought, 102
dualism, xiv (Note), xvii, xviii,
xxi

echo effect (population), 341,
346, 349, 365, 368
economic growth, xxxvi, 2-3
economic planning, 46-52
economic theory, 128
economics, 6-7, 8-9
economist, 209
Eden, 181-86
Edom, 199
Egypt, 16, 24, 41, 86-88, 127,
135, 150-51, 278-79, 293
Egyptians
citizenship, 199
firstborn, 343-45
sanctions, 319
spoiling, 5, 7, 86, 323-
24, 354n
wealth, 86
Eleazar, 62
elders, 130
Eli, 110
Eliade, Mircea, 182n
Elijah, 104, 113
elite, 258
Ely, Richard, 51
embassy, 302
empiricism, 323
Engelsma, David J., xxvi-xxxvi
Enlightenment, xvi-xviii, xxi,
257
entrepreneurship, 156
environmentalism, 4
envy, 164
Ephraim, 223-24, 226, 229
