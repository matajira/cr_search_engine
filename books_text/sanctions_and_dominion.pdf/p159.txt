The Altar Offering 121

served as assistants, presumably doing lighter physical labor
and guard duty.” “This is it that belongeth unto the Levites:
from twenty and five years old and upward they shall go in to
wait upon the service of the tabernacle of the congregation:
And from the age of fifty years they shall cease waiting upon
the service thereof, and shall serve no more: But shall minister
with their brethren in the tabernacle of the congregation, to
keep the charge, and shall do no service. Thus shalt thou do
unto the Levites touching their charge” (vv. 24-26).

The Levites would have served under the priests during the
three major feasts, when there was need for many servants. In
other times, they probably served on a rotating basis. The Le-
vitical cities had Levites as residents; they were not all perma-
nent dweilers living close to the Ark of the Covenant.

All of this took place before the second numbering, for the
next section states: “And the Lorp spake unto Moses in the
wilderness of Sinai, in the first month of the second year after
they were come out of the land of Egypt, saying, Let the chil-
dren of Israel also keep the passover at his appointed scason.”
(Num. 9:1-2), The Passover was celebrated that month (v. 5),
before-the second numbering a month later.

The dedication of the gifts took 12 days. Then came the
cleansing and dedication of the Levites. It is not clear whether
this took place on day 12 or day 13. It did not take place on
day 14, since that was the start of Passover: the 14th day of the
first month (Num. 9:5).

The Secondary Passover

It was at this time that the question was raised concerning
ritual cleanliness and distant journeys. Certain men came to
Moses and asked him regarding their contact with a dead body.
They had not been allowed to make an offering associated with

12. Gordon J. Wenham, Numbers: An Introduction and Commentary (Downers
Grove, Illinois: Inter-Varsity Press, 1981), p. 97.
