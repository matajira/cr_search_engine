98 SANCTIONS AND DOMINION

with the sanctuary shekel. This would keep the moneychangers
more honest. The priesthood also would have greater difficulty
debasing their shekel. If this judicial connection is correct, then
the priestly shekel was originally intended by God to become
the standard for the weight and fineness of silver for the other
currency units specified as shekels.

Conclusion

God’s slaying of Egypt’s firstborn identified Him as the na-
tion’s blood avenger — the kinsman-redeemer ~ on their behalf.
Only a slain lamb would have protected the Egyptians. God
executed the firstborn of Egypt in a bloodless manner, just as
Israelites were subsequently required to kill every firstborn
donkey that they chose not to redeem with a slain lamb. The
firstborn male donkey was symbolic of the firstborn sons in
Egypt, whether Egyptian or Israelite. Only the shed blood of a
lamb could save them.

The Israelites owed the Levites payment because the Levites
had shed blood on their behalf. The Levites had saved Israel
from the judgment of God (Ex. 32). God separated them from
the other tribes because they were His agents of wrath as well
as His agents of sacrificial substitution: blood’ avengers and.
kinsman-redeemers. They were the agents of sacrifice, both as
recipients of the sacrificial funds and as guardians of the place
of sacrifice. They were holy.

God brought sanctions on Passover night: positive for Israel
and negative for Egypt. This changed the judicial status of
firstborn sons in Israel. Before the first Passover night, the birth
of the firstborn son was exclusively a positive event: the exten-
sion of a man’s strength. After the first Passover night, the
firstborn sons of Israel were set apart by God. They were His.
They were His, not in the sense of priestly servants, but in the
sense of being destined for execution, yet unfit as altar sacri-
fices. This was the symbolic negative sanction hanging over the
head of every family: the threat of disinheritance by execution.
