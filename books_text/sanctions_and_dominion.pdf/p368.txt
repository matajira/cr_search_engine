Appendix

HOW LARGE WAS
ISRAELS POPULATION?

So were all those that were numbered of the children of Israel, by the
house of their fathers, from twenty years old and upward, all that were
able to go forth to war in Israel; Even all they that were numbered were
six hundred thousand and three thousand and five hundred and fifty
(Num. 1:45-46).

And all the firstborn mates by the number of names, from a month old
and upward, of those that were numbered of them, were twenty and two
thousand two hundred and threescore and thirteen (Num. 3:43).

Commentators have argued for over a century about the size
of Israel’s population. Liberals and those influenced by them
want to downsize it. Conservatives are at a loss explaining how
it got as large as the biblical texts say that it did. There are a
whole series of problems in assessing the demographics of
Mosaic Israel.

Conservative Bible commentator Gordon Wenham provides
several arguments as to why the texts’ population figures are
wrong - perhaps by as much as a factor of 100 to one. His
arguments reveal the extent to which modern evangelical Bible
commentators have mimicked higher critics in their ready
acceptance of the hypothesis of extensive textual corruption.
