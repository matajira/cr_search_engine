258 SANCTIONS AND DOMINION

shifted dramatically: Darwinism was increasingly used to sup-
port statism, in which State economic planning was invoked by
Darwinists to defend the survival of the fittest society through
scientific planning. Rather than viewing society as analogous to
nature — red in tooth and claw - the new social Darwinism
viewed society as analogous to a plant breeder’s greenhouse,
with the scientific elite serving as the breeder. The directionless,
anti-teleological free market was dismissed as anachronistic,
even as a directionless, anti-teleological nature was also dis-
missed, Both were understood as having been superseded by
planning, teleological man, meaning a scientific elite acting in
the name of mankind. The primary theorist who announced
this new social Darwinism was Lester Frank Ward.”!
Darwinism undermined Whiggism by undermining Newton-
ianism. Whiggism had been undergirded by a Newtonian view
of the universe: mathematical and mechanical. Newton’s Uni-
tarian god supposedly created it and sustained it. By 1850, such
a god was no longer regarded as necessary to social theory, but
the social fruits of that god’s authority still lingered: faith in the
coherence of a sacial system established through a myriad of
voluntary contracts. Society was believéd to be the result of
human action but not of human design ~ Scottish Enlighten-
ment theorist Adam Ferguson's worldview a century earlier.”
The Newtonian worldview was essentially mechanical. In
Newtonianism, mathematics rules the cosmos. To the extent
that physics became the ideal model for men’s social theories,
the quest for social order became the quest for the mathemati-
cally fixed laws of society.” Natural law was understood as

21. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), Appendix A.

22. FA. Hayek, “The Results of Human Action but not of Human Design,” in
Hayek, Studies in Philosophy, Politics and Fconamies (University of Chicago Press, 1967),
ch. 6.

23. Louis I. Bredvold, Brave New World of the Enlightenment (Ann Arbor: Universi-
ty of Michigan Press, 1961), ch. 2.
