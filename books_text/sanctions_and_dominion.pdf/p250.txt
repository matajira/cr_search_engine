212 SANCTIONS AND DOMINION

himself from God. The donkey was not self-interested; Balaam
was. This incident reveals that covenant-breakers in their rebel-
lion do not have the common sense of a donkey. A donkey
serves its master better than a covenant-breaker serves God
{Isa. 1:3). The words of Balaam to the donkey should warn
covenant-breakers of the wrath to come: “I would there were a
sword in mine hand, for now would I kill thee” (Num. 22:29b}.
If Balaam was ready to kill his faithful donkey out of personal
pride, what is God ready to do with covenant-breakers who
resist Him out of this same pride?

Balaam’s Motivation

Balaam did not want Balak to have the last word in this
matter. He revealed another prophecy that he had been given
in the vision (Num. 24:16). He presented the king with a mes-
sianic prophecy: “I shall see him, but not now: I shall behold
him, but not nigh: there shall come a Star out of Jacob, and a
Sceptre shall rise out of Israel, and shall smite the corners of
Moab, and destroy all the children of Sheth. And Edom shall be
a possession, Seir also shall be a possession tor his enemies; and
Israel shall do valiantly. Out of Jacob shall come he that shall
have dominion, and shall destroy him that remaineth of the
city” (vv. 17-19). He prophesied the extinction of the Amalek-
ites and the Kenites (vv. 20-22). Then he departed (v: 25). But
as. we learn in Numbers 31, Balaam’s last word was an incom-
plete word. He remained the king’s agent rather than God’s.

The account of Balaam’s prophecy indicates that he was
weak, hoping that he might profit from his position as a court
prophet, but strong enough to resist Balak’s request that he
utter a curse against Israel in God’s name. He refused to say no
each time that Balak asked him to reconsider or to look at the
matter from a new geographical perspective. He played with
fire, but he initially avoided getting burned. The problem was,
he could not stop playing with fire. His seeming immunity led
to his eventual destruction.
