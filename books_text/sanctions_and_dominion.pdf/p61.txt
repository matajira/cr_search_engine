Mustering the Army of the Lord 23

payment.“* I would add that it was God’s presence in His cov-
enantal capacity as sanctions-bringer. When God is present in
His office as sanctions-bringer, the unholy man is profane: a
boundary violator.’ Prior to holy warfare, God approached
the nation of Israel as the sanctions-bringer: positive and nega-
tive. “For the Lorn thy God walketh in the midst of thy camp,
to deliver thee, and to give up thine enemies before thee;
therefore shall thy camp be holy: that he see no unclean thing
in thee, and turn away from thee” (Deut. 23:14), God would
then bring sanctions against whichever army was not holy. The
payment of atonement money judicially set apart God’s assem-
bled army as the holy army. This is why the Israelites of Josh-
ua’s day had to be circumcised before they could. begin the
conquest (Josh. 5:5). They could not be covenantally holy inside
the boundaries of the holy land apart from circumcision. God’s
presence in their midst in his capacity as sanctions-bringer
would destroy them, just as the angel of the Lord had almost
destroyed Moses’ uncircumcised son Gershom when Moses
brought him across the border into Egypt (Ex. 4:24-26).
The second mustering came after a series of successful wars
outside of Ganaan (Num. 21). Why didn’t God muster the na-
tion prior to these wars? Because they were defensive wars. The
Canaanites had initiated them. After the treachery of the
Midiantites, however, God told Moses to vex them (Num.
23:17). Then the mustering took place. The conquest of Midian
and then Canaan were offensive wars for Israel. They were holy
wars in God’s name. They involved violating the national boun-
daries of societies that had been set apart by God for judgment.

14. Jordan, Law of the Covenant, pp. 228-29.

15, On the distinction between common and profan
An Economic Commentary (Tyler, Texas: Institute for Chui
6.

16. On the angel’s attack on Gershom rather than Moses, see Gary North, “The
Marriage Supper of the Lamb,” Christianity and Civilization, 4 (1984), p. 220. See atso
George Bush, Noles on Exodus (Minneapolis, Minnesota: James & Klock, [1852] 1976),
L, p. 67.

ee Gary North, Leviticus:
fan Economics, 1994), ch.

 
   
