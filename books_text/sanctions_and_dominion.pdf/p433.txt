politics, 168-72, 198, 263-64
population
atonement money, 19-20
echo, 341, 346, 349
Jacob to Moses, 338-43
stagnation, 20, 24, 25
wilderness, 341
postmillennialism, xxvii, xxxvi-
xxxvii, 328
power, 60, 70, 214, 240, 262
premillennialism, xiii, 326-27
Presbyterianism, 257
present-orientation, 25, 147,
210, 295, 321-23
price, 174, 205-206, 209-10
priesthood
annulment, 75
Ark of the Covenant, 56
blessings, 102
bloodshed, 56
change in, 238
concentric circles, 56
death, 57
false, 110-13
guardians, 56-57, 98
holy army, 56
law &, 238
meat-eating, 161-63
Melchizedek, 238
pagan, 187, 189
representation, 92-93,
324
rural land, 236-37
Sadducees, 71
veto power (war), 29
priorities, 126-29
prisons, 300

Index 395

profane, 23, 37, 44, 56, 69, 74,
97, 116, 186, 250, 253-54
profits, 2
Progressivism, 259-61
promise (conditional), 297
Promised Land, 1, 131, 133-
34, 138, 141-43, 148, 151,
177, 271
property right, 288
prophecy, 177-78 188-89, 206
prophet
Ahab, 110-11
Balak’s view, 207
court, 204, 206
death penalty, 103-105,
178
deceived, 112
ethical message, 178
marks of, 322
predictable sanctions, 70,
103-105
word of, 178, 207
prostitutes, 249
public goods, 288
Puritanism, xx

quail, 131-32, 332

radicalism, 78

Rahab, 266

randomness, 106

Rashi, 217-18

Rayburn, Sam, xxv-xxvi
realism, 180-81, 181-83, 186.
reality, 154, 156

Red Sea, 191, 319, 331
redemption, xxxiv
Reformation (oath), xiv-xvi
