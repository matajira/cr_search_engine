20

SANCTIONS AND INHERITANCE

Speak unto the children of Israel, and say unio them, When ye are
passed over Jordan into the land of Canaan; Then ye shall drive out all
the inhabitants of the land from before you, and destray all their pictures,
and destroy all their molten images, and quite pluck down all their high
places: And ye shall dispossess the inhabitants of the land, and dwell
therein: for I have given you the land ta possess it (Num. 33:51-53).

God instructed Moses to repeat the command given to the
generation of the exodus: exterminate the Canaanites. This
work of extermination was to mark them as a covenant people.

And he said, Behold, I make a covenant: before all thy people I will
do marvels, such us have not been done in all the earth, nor in any
nation: and all the people among which thou art shall see the work of
the Loro: for it is a terrible thing that I will do with thee. Observe thou
that which I command thee this day: behold, I drive out before thee
the Amorite, and the Canaanite, and the Hittite, and the Perizzite, and
the Hivite, and the Jebusite. Take heed to thyself, lest thou make a
covenant with the inhabitants of the land whither thou goest, lest it be
for a snare in the midst of thee: But ye shall destroy their altars, break
their images, and cut down their groves: For thou shale worship no
other god: for the Lorp, whose name is Jealous, is a jealous God: Lest
thou make a covenant with the inhabitants of the land, and they go a
whoring after their gods, and do sacrifice unto their gods, and one call
