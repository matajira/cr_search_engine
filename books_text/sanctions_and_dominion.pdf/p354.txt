316 SANCTIONS AND DOMINION

ity equal to that of the Bible. A claim of judicial equality is in
fact a claim of judicial superiority, for that which lawfully inter-
prets past judgments is judicially superior to the past. God’s
revelation in Numbers 36 was superior to what He revealed in
Numbers 27, That which He revealed in Numbers 27 was
superior to what He had revealed in Leviticus 25.

Casuistry did not cease with the closing of the canon of
Scripture. Casuistry is basic to every legal system. In the West,
casuistry ceased to be practiced by Protestants around the year
1700.? The demise of Protestant casuistry was part of a larger
social transformation: the replacement of Puritanism’s theocra-
tic ideal by Newtonian rationalism, Enlightenment speculation,
and political secularism. The kingdom of God was progressively
restricted to heart, hearth, and church. This was a denial of the
comprehensive claims of God on man and his institutions.*
The revelation of the Bible was assumed to be irrelevant to civil
affairs. The revealed law of God was assumed to be subordinate
to both natural law and common law because natural law is
supposedly more universal than biblical law, and common law
is second in authority after natural law. The categories of space
and time were invoked against biblical law. They still are, al-
though the category of time is generally given precedence: the
doctrine of evolution. Cultural relativism has generally replaced
natural law theory in academic circles.

This raises the issue of eschatology. If God’s law can never
extend to the four corners of the earth through mass evangel-
ism and conversion, then the common-ground categories of
space and time will continue to supersede the category of bibli-
cal law in the thinking of the vast majority of Christians. That
is, if progressive sanctification is eschatologically impossible
outside the boundaries of heart, hearth, and church, the king-

2. Kenneth E, Kirk, Conscience and its Problems: An Introduction to Casuistry (new
ed.; London: Longmans, Green, 1948), pp. 206-207.

8, Kenneth L. Gentry, Jn, The Greatness of the Creat Commission: The Christian
Enterprise in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990).
