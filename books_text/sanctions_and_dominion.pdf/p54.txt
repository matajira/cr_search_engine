16 SANCTIONS AND DOMINION

Ye have seen what I did unto the Egyptians, and how I bare you on
eagles’ wings, and brought you unto myself. Now therefore, if ye will
obey my voice indeed, and keep my covenant, then ye shall be a pecu-
liar treasure unto me above all people: for all the earth is mine: And ye
shall be unto me a kingdom of priests, and an holy nation. These are
the words which thou shalt speak unto the children of Israel. And
Moses came and called for the elders of the people, and laid before
their faces all these words which the Lorp commanded him. And all the
people answered together, and said, All that the Lorp hath spoken we
will do, And Moses returned the words of the people unto the Lorp
(Ex. 19:4-8).

God had imposed negative corporate sanctions on Egypt.
This was the basis of the Israelites’ deliverance. There was a
message here: negative corporate sanctions are the concomitant
of positive corporate sanctions in the struggle for covenantal su-
premacy in history. In this sense, the struggle is analogous to a
military struggle, not a free market transaction in which both
parties benefit. God’s grace preceded His law. Now He called
the nation to obedience. He set forth a conditional promise: if
they obeyed Him, He would make them a kingdom of priests.
Moses informed the elders and the people of what God had
said, and they swore allegiance to Him. The Book of Numbers
is a history of how they repeatedly broke their agreement, and
the consequences thereof.

The Exodus Numbering

Approximately nine months after this corporate act of na-
tional covenant renewal, Israel had completed the construction
of the tabernacle: “And it came to_pass in the first month in the
second year, on the first day of the month, that the tabernacle
was reared up” (Ex. 40:17).> We are not told how long it took

5. In 1611, “reared” referred to buildings, while “raised” referred to children
(Gen. 38:8; Ex. 9:16; Josh 3:7). By 1900, American grammar had reversed the usage.
Hardly anyone except English teachers in the United States says “reared” these days.
