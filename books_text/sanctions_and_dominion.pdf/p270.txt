232 SANCTIONS AND DOMINION

will be placed under negative sanctions by the owner, Jesus’
parable of the talents is the New Testament summary of this
arrangement (Matt. 25:14-30).

Milgrom argues that the case of the daughters of Zelophe-
had marks off a separate section of the Book of Numbers. The
first encounter opens the new section (Num. 27); the second
encounter closes it (Num. 36). The chapters that follow the
second Numbers mustering (Num. 26) differ sharply with the
preceding ones. The earlier chapters display the murmurings
of the exodus generation; the second section displays the faith-
fulness of the conquest generation.’

Five daughters of Zelophehad came to Moses with a prob-
lem. They had no brothers. Their father had died. They asked
a question: “Why should the name of our father be done away
from among his family, because he hath no son? Give unto us
therefore a possession among the brethren of our father” (v. 4).
The judicial issue here was a man’s name. The economic issue
was inheritance. A righteous man’s name was supposed to be
preserved in Israel. His inheritance in the land was proof of his
righteous status. The daughters were careful to identify their
father as having been in the company of the saints: “Our father
died in the wilderness, and he was not in the company of them
that gathered themselves together against the Lorn in the com-
pany of Korah; but died in his own sin, and had no sons” (v. 3).

Korah’s name had become tainted. His family had forfeited
his goods: “And the earth opened her mouth, and swallowed
them up, and their houses, and all the men that appertained
unto Korah, and all their goods” (Num. 16:32). But his sons
must have broken with the sin of their father, for they survived
the judgment: “And the earth opened her mouth, and swal-
lowed them up together with Korah, when that company died,
what time the fire devoured two hundred and fifty men: and

L. Jacob Milgrom, The JPS Torah Commentary: Numbers (New York: Jewish Publi-
cation Society, 1990), p. xiii,
