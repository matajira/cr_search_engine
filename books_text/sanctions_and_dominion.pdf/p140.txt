102 SANCTIONS AND DOMINION

earth; they are all bound by the cross-boundary stipulations’ of
His covenant; He brings predictable corporate sanctions in
terms of these stipulations.

Who was under God's special covenantal sanctions, as distin-
guished from His common-grace, cross-boundary sanctions
(e.g., Nineveh in Jonah’s day)? That person who was under
oath-bound covenantal authority and who therefore bore God's
name. Who invoked the name of God and the name of the
person? The sons of Aaron and those with priestly authority
who were operating under their jurisdiction. Had their invoca-
tion of God’s blessings not been followed by covenantal bless-
ings, this would have called into question their authority to
name the people. This is why one sign of forfeited authority by
the priesthood was the failure of the blessings to appear. This
surely was what the drought in Elijah’s day was all about: the
failure of the gods invoked by the priests of Ahab’s Israel to
bring corporate blessings.

Why did these blessings have to be invoked publicly by the
sons of Aaron? First, because Aaron’s sons were the guardians
of the four covenantal oaths of society: marital (Num. 5), per-
sonal (Num. 6), civil? and ecclesiastical. Second, they were
God’s highest judicial representatives between God and man,
which is why they conducted the altar’s sacrifices, and why their
family representative, the high priest, alone had lawful access to
the inner sanctum of the tabernacle: the holy of holies (Ex.
30:10). As such, they interpreted God’s word authoritatively.
The ordained representatives of a society serve as the interpret-
ers of the law. They possess lawful authority to enforce the law.
Enforcement involves the imposition of sanctions. Without
sanctions, their interpretations are mere opinion.

1. On cross-boundary laws, see Gary North, Leviticus: dn Economie Commentary
(Tyler, Texas: Institute for Christian Economics, 1994), pp. 643-43.

2. Ibid., ch. 6: “Guardian of the Civil Oath.”
