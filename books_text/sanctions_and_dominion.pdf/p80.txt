42 SANCTIONS AND DOMINION

A Strictly Civil Census

God required Moses to muster the people shortly after the
exodus, before the nation had sinned by bringing negative
sanctions against Joshua and Caleb (Num. 14). God had not yet
forbade this generation from entering the Promised Land. The
second mustering took place just prior to the next generation's
invasion of the land. Mustering was related to the payment of
blood money to the priests (Ex. 30:13); both were religiously
holy acts. The ritual payment of atonement money ceased with
the demise of the Mosaic priesthood. Mustering was associated
strictly with that priesthood. Abram had not been required to
make such a payment to Melchizedek prior to his battle with
Chedorlaomer (Gen. 14).

Such mustering was not lawful apart from the threat of war
and a payment to the Levites. When God was angry with the
people of Israel, He caused David to muster the nation, so that
He could bring judgment against them. “And again the anger
of the Lorp was kindled against Israel, and he moved David
against them to say, Go, number Israel and Judah” (II Sam,
24:1). This mustering was illegal, as Joab understood:

For the king said to Joab the captain of the host, which was with
him, Go now through all the tribes of Israel, from Dan even to Beer-
sheba, and number ye the people, that I may know the number of the
people. And Joab said unto the king, Now the Lorp thy God add unto
the people, how many soever they be, an hundredfold, and that. the
eyes of my lord the king may see it: but why doth my lord the king
delight in this thing? Notwithstanding the king’s word prevailed against
Joab, and against the captains of the host. And Joab and the captains of
the host went out from the presence of the king, to number the people
of Israel (II Sam. 24:2-4).

God did this to David, and through his representative covenan-
tal leadership, to Israel, by way of Satan, who acts as an inter-
mediary in such matters.
