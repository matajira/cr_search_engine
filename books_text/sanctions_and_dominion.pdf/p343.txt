Cities of Refuge 305

into, these cities of refuge, the State had an obligation to build
and maintain them as part of the judicial system. There would
have been no toll roads into these cities. To have established
tolls would have been to sell access to justice. The Bible docs
not authorize such a system of justice.

The Presumption of Guilt and Innocence

The man who accidentally killed his neighbor was automati-
cally presumed guilty by the blood avenger. This means that
the slain victim, who could not speak on his own behalf, was
automatically presumed innocent. The suspect was not allowed
to defend himself physically against the blood avenger; to have
done so would have been assault or murder. In the latter case,
the State would have imposed the death penalty. The innocent
man would have forfeited his innocence by killing his lawful
pursuer. The courts recognized the right of the blood avenger
to execute the suspect. Had they not done so, powerful suspects
could have disposed of speedy but ineffective avengers.

With respect to everyone else in Israel, the suspect was not
considered guilty. He had the right of protection if he reached
a city of refuge. He could not be killed by the blood avenger
within the walls of that city. At the death of the high priest, he
returned home in safety: innocent for life.

This reduced the cost of murder trials in Israel. The suspect
bore the cost of fleeing to a city of refuge. He would have start-
ed running as soon as he recognized that his victim was dead.
This surely would have increased the likelihood that he would
have done his best to save the victim’s life if he appeared as
though he could be saved. But once the victim was dead, there
was no time to waste.

The blood avenger, who was probably biased in favor of the
victim, would bear the cost of pursuing the suspect. No one else
would. The judges in a city of refuge, presumably with no bias
against the suspect, would conduct the preliminary hearing. If
the suspect wanted to bring witnesses on his behalf, he or the
