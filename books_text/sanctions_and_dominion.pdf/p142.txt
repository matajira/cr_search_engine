104 SANCTIONS AND DOMINION

which thou hast not known, and let us serve them; Thou shalt not
hearken unto the words of that prophet, or that dreamer of dreams:
for the Lorp your God proveth you, to know whether ye love the Lorp
your God with all your heart and with all your soul. Ye shall walk after
the Lorn your God, and fear him, and keep his commandments, and
obey his voice, and ye shall serve him, and cleave unto him. And that
prophet, or that dreamer of dreams, shall be put to death; because he
hath spoken to turn you away from the Lorp your God, which brought
you out of the land of Egypt, and redeemed you out of the house of
bondage, to thrust thee out of the way which the Lorn thy God com-
manded thee to walk in. So shalt thou put the evil away from the midst
of thee (Deut. 13:1-5).

But the prophet, which shall presume to speak a word in my name,
which I have not commanded him to speak, or that shall speak in the
name of other gods, even that prophet shall die. And if thou say in
thine heart, How shall we know the word which the Lorn hath not
spoken? When a prophet speaketh in the name of the Lorn, if the
thing follow not, nor come to pass, that is the thing which the Lorp

hath not spoken, but the prophet hath spoken it presumptuously: thou
shalt not be afraid of him (Deut. 18:20-22).

The false prophet was marked by either of two ways: 1) he
prophesied an event, it came to pass, and then he told people
to worship another god; 2) he foretold the future in God’s
name, but the event did not come to pass. A false prophet had
to be executed. This is why the false priests on Mt. Carmel were
executed by Elijah (I Ki. 18:40): they had prophesied the fall-
ing of fire on the sacrifice, but nothing happened; they had also
called people to worship false gods. The agent of the sanction
of execution, Elijah, had prophesied that the fire would fall on
the sacrifice when he invoked God’s name; it did. He had
called on the people to decide: worship God or Baal (I Ki.
18:21).

The reason why these two Mosaic laws no longer apply
under the New Covenant is that the special office of prophet no
longer exists. Men can lawfully speak prophetically in God's
