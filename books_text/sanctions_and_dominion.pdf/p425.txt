equality, 69-70, 78, 80, 151-53,
173
Esau, 85
eschatology
adiaphora?, 327-28
Calvin's, xxiii
Canaan, 297
church, 327
conquest, 325
covenant structure, 328
Israel’s, 325
kingdom, 316-17
life expectancy, xiii
neutrality, 327-28
pessimillennialism, 327
pluralism, 327
Presbyterianism, xviii-xix
revelation &, 315-16
sanctions &, xiv, xix-xxii,
xxvi-xxvii, 297-98,
328
stalemate, xxix
theonomy, 325
see also amillennialism,
postmillennialism,
premillennialism
establishment, 213
ethics, 190, 261-62
evaluation, 137-38, 153-55,
185
evangelism, 270, 316
excommunication, xvi, xxxii,
105, 237, 271
exile, 241, 272, 315
experience, 323
extradition, 301

familism, 239-40

387

family, 219-20, 235-39, 245,
253, 256, 265-68, 312
fat, 163
fate, 106
fear, 40-41, 126, 209, 275-76
Ferguson, Adam, 258
feuds, 304, 307-308
fig leaves, 184
Finland, xxvi
fire, 71, 109, 153, 144-47, 194,
212-13
firstborn
Adam, 324
animals, 89-90
atonement, Chap. 4,
347-48
blessing, 85
consecrated, 91-92
double portion, 85, 92,
236, 239
golden calf, 90, 92-93
Egyptians, 323-24
holy, 82
inheritance, 25, 236, 239,
323-24
negative sanctions, 86
owned by God, 82
priests?, 91-92
ratio, 337-38, 343, 348,
365
representatives, 93
sacrifices, 83-84, 91-93
strength, 84
firstfruits, 122
flags, 32
fools, 11
foreign trade, 122-23
Franklin, Ben, xxvi, xxxvi
