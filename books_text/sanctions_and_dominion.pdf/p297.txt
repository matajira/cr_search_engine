Oath and Sanctions 259

governing historical processes, bringing order to these process-
es. This worldview collapsed under the weight of a reforming
social Darwinism, which relied increasingly on the State as an
agent of social change, scientific planning, and ethical action. In
Europe, this new social outlook was called social democracy. In
the United States, it was called Progressivism.

No better statement of the transformation can be found than
liberal Presbyterian elder Woodrow Wilson’s 1908 book, Consti-
tutional Government of the United States. In 1907, Wilson openly
moved from laissez-faire Jeffersonianism to Progressivism. He
was the president of Princeton University at the time. He wrote
Constitutional Government as a thinly disguised fat campaign tract
for the Democratic Party’s 1908 nomination for the Presidency.
The book was published in the year of the Presidential election.
Fundamentalist Presbyterian elder and radical political populist
William Jennings Bryan received the nomination for the third
time; Wilson had to wait four more years to attain his goal. He
was elected President in 1912 and again in 1916.

His book praised the Presidency as the central political
office: head of the party. This was a self-conscious break from
the U.S. Constitution's view of the office. The Constitution does
not mention political parties, and the Framers had hated polliti-
cal factions in 1787, They also hated big government (Hamilton
excepted). They inserted checks and balances into the Constitu-
tion in order to prevent the growth of a large central govern-
ment. Wilson, having switched to Progressivism, had to under-
mine this older political faith. He turned to Darwin for the
solution. The Framers had been Whigs because they had been
Newtonians, he correctly argued. This Newtonian Whig world-
view is incorrect, he insisted, and so is the Constitutional order
that assumes it. “The government of the United States was
constructed upon the Whig theory of political dynamics, which
was a sort of unconscious copy of the Newtonian theory of the
universe. In our own day, whenever we discuss the structure or
development of anything, whether in nature or in society, we
