230 SANCTIONS AND DOMINION

that a tribe that had contributed large numbers of warriors
received an equal share of the spoils. This would have penal-
ized the warriors of the larger tribes: smailer plots, on average.
A large family in a small tribe might have received more land
than a large family in a large tribe if all tribal territories were
equal. This would have constituted an economic penalty on
large tribes. Joshua’s answer to the heirs of Joseph indicates
that the more that a tribe contributed to the victory, the more
land it deserved in the post-conquest allocation.

The distribution of family plots was by lot and by post-lot re-
allocation on the basis of family size. It seems likely that this
same system governed the allocation of tribal territory. The lot
determined the general region of one’s inheritance: tribal terri-
tory and family plots. The question of need in relation to popu-
lation governed the family allocation procedure and probably
governed the tribal.
