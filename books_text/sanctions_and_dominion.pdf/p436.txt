398 SANCTIONS AND DOMINION

Enlightenment, xvii
messianic, 103n, 169
ministerial, xviii
negative sanctions, 103
planning, 49-53, 258,
260-61
Progressivism, 260-61
sanctions &, xviii, 37,
303-304

state of nature, 255-56

statism, 262

statistics, 46-52

stewardship, 216, 231-32

sticks, 144-45

stoning, 303-304, 307

stranger, 252

strict liability, 310

Sullivan brothers, 30

surveyors, 226-27

sword, 23-24, 35, 57, 64-65,
103, 116, 135-37, 246, 263,

275, 284, 285
symbol (tree), 183-85
tabernacle

barbecue, 161
construction, 16-17
defenders, 33-35, 38
inner circles, 60
jobs, 62-63
mountain imagery, 60,
79-80
sacred boundaries, 64,
74
tablets, 61
talent, 19
taxation, 46, 118, 168-72, 274
Ten Commandments, xi
tents, 32

theocracy, xxi, xxiv
theonomy, 325-26
time, 147
time~preference, 295, 321~23
tithe
booty, 274
boundary, 172
decentralization, 165-66
exempt, 66-68, 167
fair, 166-68
herd, 274
hierarchy, 65-68
inheritance, 165,.172-73
ministers, 68
monopoly, 71-72
non~market, 71-72
priestly price, 173
proportional, 167
sacraments, 167-68
salvation &, 167-68,
173-74
tithe of, 65-66, 166-67
tower of Babel, 179
trade, 122-23, 141
transportation, 159-60
tree in Eden, 181-86
trial, 300-301, 305-306
tribe, 30, 166, 234-35, 238,
241, 307-308, 313-15
trumpets, 29

urbanization, 72-73
Uzzah, 34, 64

value scales, 128-29
vegetarianism, 161-63, 174
veil of temple, 71, 78
Velikovsky, Immanuel, 196n
