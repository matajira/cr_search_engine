5

POSTMILLENNIALISM AND
THE SALVATION OF THE JEWS

[E]ven the Jews will be provoked to jealousy. Paul cited Deu-
teronomy 32:21 concerning the Jews: “But I say, Did not Israel
know? First Moses saith, I will provoke you to jealousy by them
that are no people, and by a foolish nation I will anger you”
(Romans 10:19), The Gentiles have received the great blessing.
‘I say then, Have they [the Jews] stumbled that they should fall?
God forbid; but rather through their fall salvation is come unto
the Gentiles, for to provoke them to jealousy” (Romans 11:11).
This becomes a means of converting the remnant of Israel in
the future, and when they are converted, Paul says, just think of
the blessings that God will pour out on the earth, given the fact
that the fall of Israel was the source of great blessings for the
Gentile nations. “Now if the fall of them be the riches of the
world, and the diminishing of them the riches of the Gentiles,
how much more their fulness?” (Romans 11:12). When the Jews
received their promise, the age of blessings will come. When they
submit to God's peace treaty, the growth of che kingdom will be
spectacular. This is what Paul means by his phrase, “how much
more.” This leads to stage ten, the explosion of conversions and
blessings. If Gad responds to covenantal faithfulness by means of
blessings, just consider the implications of widespread conversions
among the Jews. When the fulness of the Gentiles has come in,
then Israel will be converted (Romans 11:25). The distinction be-
tween Jew and Gentile will then be finally erased in history, and
the kingdom of God will be united as never before.’

1. Gary North, Unconditional Surrender: God's Program for Victory (Tyler, TX:
Geneva Press, 1981), p. 199.

44
