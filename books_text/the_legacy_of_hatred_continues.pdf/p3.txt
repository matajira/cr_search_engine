Publisher’s Foreword #y Gary North

TABLE OF CONTENTS

  

 
 

Preface... oxi
Introduction .. wl
1, “Hero or Bum”? . 0... cece 13
2. The Law, Eschatology, and “Anti-Semitism” .......... 19
3. Dispensationalism’s “Future Holocaust”.............. 26
4. The Church and Israel:
Is Lindsey Telling the Truth?.................... 33
5. Postmillennialism and the Salvation of the Jews ....... 44
6. To Those Who Wonder If Reconstructionism
Is Anti-Semitic: A Response by a Jewish
Christian Reconstructionist ..........6... 0.00005 56
For Further Reading ........- 6.0.0 esse erence e reece 62

iii
