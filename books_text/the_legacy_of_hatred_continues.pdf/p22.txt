10 The Legacy of Hatred Continues

border on slander and would be filled with numerous inac-
curacies, both biblical and historic. After reading the book, we
soon learned that our initial fears had been justified.

Two years ago Hal Lindsey, in a taped critique of “Dominion
Theology,” lamented the fact that in all the years that people had
written responses to his books, no one had ever come to him to get
a more accurate representation of his position. Lindsey said:

After I read [Paradise Restored by David Chilton] (’m men-
tioned several times in here, and that doesn’t bother me, the fact
that [Chilton] did not call and talk with me). You know, they all
criticize Dave Hunt because he didn’t first go to them and talk to
them before he wrote about them. I’ve had whole books written about
me, and nat one of them had ever come to me, . . , I dont care. ma
public person, and the minute you write a book, then you should
be open for someone else to take it apart, and they do, believe
me, I don’t care, the only thing I don’t like is when they start us-
ing character assassination and when they start saying you're
stupid, and things like that, or they take this “talking down”
tone, I don’t appreciate that. But if they analyze what I've said
according to the Scripture, that’s their right, and their duty.'+

After hearing this, f assumed that Hal Lindsey would welcome
the mediated one-on-one exchange. The professor at Dallas Theo-
logical Seminary wrote the following in response to my request:

Bad news! Lindsey is not interested in talking about it. His
basic word is write a book in response,

Isent another letter to Mr, Lindsey, following the procedures out-
lined in Matthew 18 and Galatians 6:1, asking him why he did not
want to meet over this most scrious charge of “anti-Semitism.” No
reply was forthcoming.

Matthew 18 and Galatians 6:1 are clear. Before accusations are
made public, an attempt at personal reconciliation is a pre-
requisite. Lindsey’s book makes serious ¢eéhical charges: “anti-

14. Excerpted from Hal Lindsey, “Thc Dominion Theology Heresy,” #217. Hal
Lindsey Tape Ministries, P.O. Box 4000, Palos Verdes, California 90274.
