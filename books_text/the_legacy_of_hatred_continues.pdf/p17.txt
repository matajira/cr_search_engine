Introduction 5

study of the published literature of Dominion Press and its vari-
ous affiliates.” We will test that claim in the following chapters.

Most of Lindsey's other charges have been answered in nu-
merous Christian Reconstruction publications, publications that
Lindsey has ignored.

For a rebuttal of Lindsey’s interpretation of Matthew 24 and
the recent development of the aberrational dispensational premil-
lennial eschatology, see Gary DeMar, The Debate over Christian
Reconstruction, For a discussion of the theology of Christian Recon-
struction, a comprehensive definition of terms, a discussion of the
kingdom both biblically and in the history of Christian thought,
the place of the law in the Christian's life, and a study of eschatol-
ogy in the history of the church, see Gary DeMar and Peter Leit-
hart, 7he Reduction of Christianity: A Biblical Response to Dave Hunt.
For the dating of the Book of Revelation, see Kenneth Gentry, Jr.,
Before Jerusalem Fell: Dating the Book of Revetation, For most every-
thing else, see Greg L. Bahnsen and Kenneth Gentry, Jr., House
Divided: The Break-Up of Dispensational Theology.®

Is Only Dispensationalism Anti- “Anti-Semitic”?

The most serious charge made by Lindsey is that only dispen-
sational premillennialism can save the world from “anti-
Semitism,” and that al] other millennial positions are inherently
“anti-Semitic.” Lindsey writes:

The purpose of this book is to warn about a rapidly expanding
new movement in the Church that is subtly introducing the same
old errors that eventually but inevitably led to centuries of atrocities
against the Jews and culminated with the Holocaust of the Third
Reich, I do not believe that the leaders of this new movement are
consciously anti-Semitic — their historical predecessors were not
either. But just as their historical counterparts did, they are set-
ting up a philosophical system that will resule in anti-Semitism?

8. These books can be ordered from Dominion Press, P. O. Box 8204, Ft.
Worth, Texas 76124.
9. Lindsey, Read to Holocaust, p. 3
