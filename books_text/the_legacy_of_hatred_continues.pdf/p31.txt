2

THE LAW, ESCHATOLOGY,
AND “ANTI-SEMITISM”

“Whoever sheds man’s blood, by man his blood shall be shed,
for in the image of God He made man” (Genesis 9:6).

It’s amazing that Hal Lindsey thinks that dispensational pre-
millennialism —an aberrational view of eschatology — will save the
Jews from persecution and a possible new holocaust. If tyrants
like Adolf Hitler spit at the law of God, then what makes Hal
Lindsey think that modern tyrants will not spit at the eschatology
of God? In fact, we know that Hitler spat at both the law of God
and the eschatology of God. Hitler boasted that his “Third Reich
which was born on January 30, 1933... would endure for a
thousand years, and in Nazi parlance it was often referred to as
the ‘Thousand- Year Reich.’”! Hitler proctaimed a counterfeit law
and kingdom to supplant Christianity.

Hitler despised Jesus Christ and His law. Martin Bormann,
one of the men closest to Hitler, said publicly in 1941, “National
Socialism and Christianity are irreconcilable.”? Christianity was
the great enemy to Hitler: “We know now what Hitler envisioned
for the German Christians: the utter suppression of their religion.”

1, William L. Shiver, The Rise and Fall of the Third Reich: A History uf Nazi Ger-
many (New York: Simon and Schuster, 1960), p. 5.

2. William L, Shirer, The Nightmare Years: 1930-1940, Vol. 2 of 20th Century
Journey: A Memoir of a Life and the Times (Boston, MA: Little, Brown, 1984),
p. 156.

3. Ibid. .

19
