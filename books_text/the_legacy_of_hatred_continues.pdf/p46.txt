34 The Legacy of Hatred Continues

Origen (185-253), who, Lindsey says, was responsible for the
acceptance of allegorical interpretation in the church.*

In order to assess this argument, we must deal with two sepa-
rate but related questions. First, what principles of interpretation
guided the earliest post-apostolic Fathers? Was Origen indeed re-
sponsible for the spread and acceptance of “allegorical” interpreta-
tion, or was this method of interpretation popular prior to
Origen? Second, we must ask what the church Fathers prior to
Origen said about the relationship of the Old Testament Israel to
the New Testament church. Did the Fathers of the first two cen-
turies believe the church to be the New Israel?

Biblical Interpretation of the Early Fathers

To assess the church Fathers’ approach to biblical interpreta-
tion fairly, we must distinguish between allegorical and typologi-
cal interpretation. The key difference between the two approaches
is in their attitudes toward the historical character of the Bible.
The allegorical interpreter seeks to find eternal truths in Scrip-
ture, and in doing so may either reject the historical meaning of
the Bible, or relegate the historical sense to a very secondary
place. Typology, by contrast, takes history seriously; typology is
based on the idea that Old Testament history prefigured the
events of the New Testament. Thus, for example, the allegorical
interpreter would search the Old Testament for symbols of virtue
or illustrations of philosophical ideas, while the typological inter-
preter would search the Old Testament for shadowy images of the
coming Messiah.*

Of course, there are also similarities between allegory and
typology. Both approaches search for a “deeper” meaning, either

3. Lindsey notes that, despite the evil consequences that he attributes to
Origen, Origen was not himself an evil man.

4. On the distinction between allegory and typology, see J. N. D. Kelly, Early
Christian Doctrines (rev, ed.; San Francisco, CA: Harper & Row, [1960] 1978), pp.
69-75; Moises Silva, Has the Church Misread the Bible?: The History of Interpretation in
the Light of Current Issues (Grand Rapids, MI: Zondervan, 1987), pp. 69-75.
