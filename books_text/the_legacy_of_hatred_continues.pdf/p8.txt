viii The Legacy of Hatred Continues

Is this the work of a man who is dealing with biblical texts
faithfully? Would any dispensationalist writer other than Hal
Lindsey have been so rash? Will any other Christian leader come
to his defense and assure us that he is right on target?

He singles out David Chilton, me, Rousas J. Rushdoony, and
others who share our views. We are ‘he anti-Semites. He had said
this before, on an audiotape issued by his organization in 1987,
which was still being issued in 1988: “The Dominion Theology
Heresy,” tape #217, On that tape, the lawyers at Bantam Books
did not control him; he did not say that we are “unconscious” anti-
Semites, as he does in his book. He said this loud and clear:

“Man, this is one of the things that’s dangerous. This is the
most anti-Semitic movement I’ve seen since Adolph Hitler.”

Not “one of the most”—¢4e most. Christian charity, thy name is
not Hal Lindsey.

This time, he got Bantam Books to help him say it again,
though more mildly, to a far larger audience. Bantam gave him a
“bully pulpit” for flagrant misrepresentation.

With a sensational title like The Read to Holocaust, it may sell
well. This is what Mr. Lindsey sold throughout the 1970's: sensa-
tionalism. And also prophecies that never come true. But then his
market dried up. He did not write a best-seller in the 1980's.

On the other hand, The Road io Holocaust may be Mr. Lindsey’s
last gasp. Maybe Christians are tired of the sensationalism.
Maybe they are tired of being manipulated. If this book fails to hit
the best-seller charts and stay there, Mr. Lindsey will have to con-
tent himself with declining book royalties from his pile of aging
paperback books, filled with prophecies that did not come true.
The “days of wine and roses” will be aver. When a man reaches
age 60, this is a scary prospect.

A Truly Boring Book

If you had been called the publisher of the most anti-Semitic
movement since Adolph Hitler, { think you would sit up and take
notice. You would read everything in the book, line by line. But
