22 The Legacy of Hatred Continues

Gods will, and then gives the power to perform it. But He does it
from the inside.”® And Christian Reconstructionists agree. Why
isn’t this enough to stop Christians from being consciously or un-
consciously “anti-Semitic”? If God’s Spirit working in us will not
do it, then what makes Lindsey think that God’s eschatology will
accomplish what God’s Spirit cannot do?

“God's will” is God’s law. But Hal Lindsey wants the Christian
to distance himself from the law of God because of the possibility
of “legalism.” But to distance oneself from God’s law is “lawless-
ness,” not grace. Scripture tells us that “sin is lawlessness” {1 John
3:4). How do you know when you are sinning? How do you know
when a person is being “anti-Semitic”? The law tells you. Legal-
ism is the belief that we are justified by the law, Lindsey is confus-
ing legalism with obedience,

Lindsey tells us that “The Holy Spirit Did Not Come to
Help Us Keep the Law.”!° Who then does help us keep the law,
whether that law is written on the heart or written in the Bible?
(Actually, they’re the same law. God only has one law.) Jesus said,
“If you love Me, you will keep My commandments’ ( John 14:15).
Love for Jesus is manifested through obedience to His command-
ments. Love toward my neighbor, Jews included, is measured by
obedience to the Ten Commandments (Rom. 13:8-10). But I can’t
live up to the commandments, and neither can I love my neighbor
as I ought. Jews are my neighbors. The Holy Spirit helps me to
live a godly life so I will not hurt my neighbors (Jews included).
“Love does no wrong to a neighbor” (Rom. 13:10), But love and
godliness are not subjective. Paul quotes the sixth commandment
in Romans 13:9: “You shall not murder.”

Lindsey complicates this whole “anti-Semitism” issue because
he cannot reconcile his dispensational view of the law with plain
common sense and biblical truth. Let me lay it out so a first-grade
student can understand it.

9. tbid., p. 154,
10. bid, p. 158.
