Postmillenniatism and the Salvation of the Jews 55

Jennial position that denies a future large scale conversion of Jews.

Second, the concern for the Jews among postmillennialist
writers makes “anti-Semitism” unthinkable. There may be “anti-
Semitic” postmillennialists, but then there are also “anti-Semitic”
dispensationalists. “Anti-Semitism” is not a simple deduction of
eschatology.

Finally, Hal Lindsey has seriously distorted his case against
“Dominion Theology.” He implies that the postmillennial “Do-
minionists” have no place for Israel, yet he quotes postmillennial-
ist John Murray in his discussion of Romans 9-li. He never in-
forms his readers that leading “Dominion Theologians” refer to
Romans 11 as evidence of a future conversion of Israel. As a result
of these distortions, his case against “Dominion Theology” loses
all credibility.

33. E.g., William Hendriksen, Axpositiun Pauls Epistle to the Romans (Grand
Rapids, MI: Eerdmans, 1981), pp, 377-82; Anthony A. Hoekema, The Bible and
the Future (Grand Rapids, MY: Eerdmans, 1979), pp. 143-47. We do not at all mean
to imply that amillennialism leads to “anti-Semitism.”
