20 The Legacy of Hatred Continues

Mcin Kampf Substitutes for the Bible

Mein Kampf was to be regarded as “the most sacred book,” con-
taining the “purest and truest ethics for the present and future life
of” the German nation.* Hitler adopted a nature-based personal
and civic ethic founded solely on general revelation without the
corrective input of biblical revelation, God’s laws were contrary to
National Socialism. William Demarest writes:

A classic example of the claim that knowledge of God and
His will is gained from general revelation is found in the ideol-
ogy of Nazi Germany. Hitlers National Socialist propagandists
appealed to the revelation of God in reason, conscience, and the
orders of Creation as justification for the Nazi state thealogy or
cultural religion. Biblical revelation in Old and New Testaments
was regarded by the Third Reich as a “Jewish Swindle” and thus
was set aside in favor of the Nazi natural theology.>

There was a strong pagan attachment to “blood and land” and a
belief that every people, even the Jews, has “a Nomos”6 that “is the
source of morality for the society.”’ In order to make National So-
cialism work, Hitter had to rid the nation of Christianity. This in-
cluded the law of God.

God’s Law a Help to the Jews

“Biblical revelation was set aside” in Nazism as it is set aside
by dispensationalism today (although for very different reasons).

4. Ibid., p, 157,
5, William Demarest, General Revelation: Historical Views and Contemporary Issues
(Grand Rapids, MI: Zondervan, 1982), p. 15.

6. Nazi Nomos (Greek: Law) manifests itself in a “divinely prescribed natural
constitution.” Hitler's views on social theory were a strange mix of the occult,
Wagner legend, paganisin, and a dash of insanity. Under the leadership of Alfred
Rosenberg, an outspoken pagan, “the Nazi regime intended eventually to de-
stroy Christianity in Germany, if it could, and substitute the old paganism of the
early tribal Germanic gods and the new paganism of the Nazi extremists.” Shirer,
Hise and Fall, p. 240. Hitler’s dream was for a pure race and a pure nation. All
competitors would have been exterminated in time, The Jews happened to be the
closest scapegoats.

7. Richard V. Pierard, “Why Did Protestants Welcome Hitler?,” Fides et
Historia (Spring 1978), p. 15.
