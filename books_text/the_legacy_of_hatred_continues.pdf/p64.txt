52 The Legacy of Hatred Continues

restoration, although in like manner national, need not be assumed
to include the salvation of every individual Jew.” This will not be
the end of history, however; rather, “much will remain to be ac-
complished after that event; and in the accomplishment of what
shall then remain to be done, the Jews are to have a prominent
agency.”?*

John Brown, a nineteenth-century Scottish theologian, wrote
this in his commentary on Romans:

The apostle [Paul] contrasts the former state of the Gentiles
with their present state, and the present state of the Jews with
their future state. The past state of the Gentiles was a state of
disobedience — their present state, as state of gracious salvation.
The present state of the Jews is a state of disobedience —their
future state is to be a state of gracious salvation.

The reason for God's rejection of the Jews and for their future res-
toration is to display both the total depravity of men—both Jew
and Gentile—and the pure and sovereign grace of salvation.”6
Southern Presbyterian theologian Robert L. Dabney included
under the category of “unfulfilled prophecy” the “general and na-
tional return of the Jews to the Christian church. (Rom. ix: 25,
26).”27 In a discussion of the premillennial views of the Plymouth
Brethren, Dabney argues that “pre-adventism’ is “unfavorable to
the promise of Israel’s ingathering.”** He went on to explain:

True, it [i.c., premilleunialism] teaches that Israel will be
saved after (immediately after), and by means of the advent, but
most inconsistently. For first, Paul says, they are to come in

24. Charles Hodge, A Commentary on Romans (London: Banner of Truth Trust,
[1864] 1972), p. 374. See also Hodge, Systematic Theology, 3 vols. (New York:
Charles Scribner’s Sons, 1917), vol. 3, pp. 804-13.

25, John Brown, Analytical Exposition of the Episile of Paul the Apostle to the Romans
(Edinburgh: Oliphant, Anderson, & Ferrier, 1883), p. 417.

26. Tbid., pp. 18-419.

27, Rohert L. Dabney, Lectures on Systematic Theology (Grand Rapids, MI:
Zondervan, [1878] 1972), p. 838.

28. CR. Vaughan, ed., Discussions by Robert Dabney (Richmond, VA: Presby-
terian Committee of Publication, 1890), vol. 1, p. 2i1.
