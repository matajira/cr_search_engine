32 The Legacy of Hatred Continues

interpretation of Scripture to foist upon all non-dispensationalists
the stigma of being “anti-Semitic,” if they do not adopt the new
and improved Lindseyite dispensationalism.

Lindsey’s Cryptic Postmiilennialism

As we will show in Chapters 4 and 5, the only millennial view
that has a significant prophetic role for [srael to play this side of
the rapture is fostmillennialism. There is so much historic evi-
dence for this in the postmillennial literature that Hal Lindsey is
dishonest in not referring to it. Bantam Books informed us that
Reverend Lindsey did “his exhaustive study of the published liter-
ature of Dominion Press and its various affiliates.” An “exhaustive
study” would have shown Lindsey that his concerns of “anti-
Semitism” are similar to those of postmillennialists because it’s in
postmillennialism that Israel has a future prior to the rapture.
Just from a pragmatic perspective, postmillennialism needs the
Jews. Without the conversion of the Jews there are no blessings to
the Gentiles:

Now if their transgression be riches for the world and their
failure be riches for the Gentiles, how much more will their ful-
fillment be! (Rom. 11:12).

Dispensationalists see this verse being fulfilled after the rap-
ture. Hal Lindsey wants to believe that Romans 11:12 will be fulfilled
prior to the rapture, thus, there can be no persecution of the Jews
or a holocaust. Postmtllennialists have always belteved that Romans 1:12
will be fulfilled prior to the rapture. Also, the postmillennial view of
prophecy does not teach that two-thirds of the Jews living in Israel
will be wiped out during the so-called “Great Tribulation.”
