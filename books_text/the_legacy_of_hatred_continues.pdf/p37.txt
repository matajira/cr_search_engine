The Lau, Eschatology, and “Anti-Semitism” 25

ment, was never intended for the Gentile nations. Hitler mur-
dered millions of Jews, but what law would Hal Lindsey use to
judge him? The Ten Commandments? But that’s “Jewish law,”

“O How I Love Thy Law” (Psalm 119:97)

The Reformed theonomic postmillennialist takes issue with
Johnson, Barnhouse, and Lindsey. It was a blessed hour when the
Reformation churches wrote the Ten Commandments into their
creeds and catechisms and sought to bring ad! nations under God’s
law, It was a tragic hour when dispensationalism erased them. By
bringing all nations under God’s law, they will be driven to
despair in their own impossible self-effort to please God, and they
will then be Ied to faith in Jesus, embracing His perfect life and
shed blood as the only hope for redemption.

The Postmillennialist Has the Answer

Lindsey believes that dispensational eschatology is the answer
to the “anti-Semitism” problem. Christian Reconstructionists be-
lieve that the outpouring of God’s Spirit, the proclamation of the
gospel, the conversion of sinners, obedience to God’s law as a
standard of behavior in response to His gracious redemption, and
the conversion of the Jews before the rapture are the answers. The
Jews, and all people, because they are created in the image of
God, are protected by the law of God. Remove the law of God,
and you remove this protection.
