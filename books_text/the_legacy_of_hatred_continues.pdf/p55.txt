The Church and Israel: Is Lindsey Telling the Truth? 43

the Old Testament waned somewhat. Thus, the designation of the
church as the true Israel was used less frequently. Pelikan sum-
marizes the question superbly: “No title for the church in early
Christianity is more comprehensive than the term ‘the people of
God, which originally meant ‘the new Israel but gradually lost
this connotation as the Christian claim to be the only true people
of God no longer had to be substantiated.”3*

Conclusion

Let us summarize the two major points of this chapter. First,
Lindsey asserts that Origen was responsible for the spread and
acceptance of “allegorical” interpretation in the church. We have
shown that “allegorical” interpretation was used by Christian
theologians prior to Origen, and that Origenist allegorism was
not universally adopted. Second, Lindsey argues that the church
began to understand herself as the New Israel only after Origenist
interpretive principles had become widespread. We have shown
that the earliest post-Apostolic theologians referred to the church
as a New Israel, and as the heir of God’s covenant promises.

Lindsey’s accusations of potential “anti-Semitism” against
“Dominion Theologians” depend on the accuracy of his historical
survey in Chapter 1 of The Road te Holocaust. But his historical
survey is full of falsehoods and oversimplifications. As a result,
Lindsey’s accusations, if correct, apply to the whole church and
the whole of Christian theology.

Does this mean that non-dispensationalist views of eschatol-
ogy have no place for the Jewish people in prophecy? On the con-
trary, postmillennialism has always emphasized the importance of
Israel’s conversion to Christ, as we shall show in the next chapter.

33, Pelikan, Kmergence of the Catholic Tradition, p, 26.
