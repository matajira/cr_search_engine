To Those Who Wonder if Reconstructionism Is Anti-Semitic 59

An article in the British and Foreign Evangelical Review in 1857
asked the question in its title: “Will the Jews, as a Nation, be
Restored to their own Land?” ‘This question was answered
affirmatively; the (unsigned) article concluded that Scripture
taught that the Jews must be restored to their land if certain
prophecics would be fulfilled. But contra dispensationalism, the
article asserted, “The condition of the restoration . . . 15 repentance, true
religion. But it is agreed on all hands—with exceptions that need
not detain us—that the Jews, as a nation, wil! be converted to
Christianity, at some time yet future. The condition then will be
complied with” (p. 818).

This excerpt highlights the difference between the attitude of
the reconstructionist and the dispensationalist toward the nation
of Israel. Dispensationalists believe that the Jewish people have a
title to the land that transcends virtually any other consideration,
including unbelief, rebcllion, and hatred toward Christ and His
church. Consequently, anti-zionism is equated with anti-
semitism,

The reconstructionist, on the other hand, makes a distinction.
He believes thal the Jewish people may exercise the title only
when they comply with the condition of repentance and faith. He
has nothing against Jews living in “cretz yisrac!” per sc, but he
recognizes that the far more significant question is Isracl’s faith.
In light of this, it might be appropriate to ask which theological
system has the true and best interests of che Jew close to its heart?
Tf one’s heart’s desire and prayer to God for Isracl agrecs with the
inspired Aposile’s as recorded in Romans 10, can he thereby be
called anti-semitic?

It is of more than passing interest that the above-mentioned
article refers to the Jewish people as “a standing miracle, an ever-
existing monument of the truth of prophecy.” The author also
maintained that, “the Jews, as a nation, wel be converted wo
Christianity. . . . This is so clearly taught in the eleventh chapter
of the Epistle to the Romans that one could scarcely deny it and
retain his Christian character” (p. 812). Yet, he felt compelled to
offer this disclaimer in a footnote: “It is proper for (the author) to

  

   
