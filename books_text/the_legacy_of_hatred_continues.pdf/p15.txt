Introduction 3

that the demands of the law are fulfilled as the necessary condition
of grace.’ In other words, we earn grace by keeping the Law.”® But
Lindsey only quotes half of Rushdoony’s sentence, and Lindsey
does not inform his readers that he has made a cut by adding an
ellipsis after “grace.” Here is Rushdoony’s fu’ statement with the
missing section in italic: “So central is the Law of God, that the
demands of the law are fulfilled as the necessary condition of grace,
and God fulfills the demands of the law on Jesus Christ."° Rushdoony
asserts what the Bible teaches: Jesus fulfilled all “the demands of the
law”: “God made [ Jesus] who knew no sin to be sin on our behalf,
that we might become the righteousness of God in Him’ (2 Cor.
5:21). Lindsey's chopped quotation makes it sound like Rush-
doony is saying that sinners must earn their salvation by keeping
the law. Lindsey goes beyond poor scholarship to deception.

Third, nearly 90 books have been published by writers iden-
tified with Christian Reconstructionism. (Rushdoony’s two-
volume set of The Institutes of Biblical Law alone is over 1,600
pages!) This does not count the hundreds of newsletters and tapes
published over the years. How many books does Hal Lindsey
cite? Twelve! Eight of the twelve books are only quoted once! And
we've just seen that one of those quotations is chopped and dis-
torted. The others are mere extractions that are used to put the
worst out-of-context slant on the author.

There is no mention of Rushdoony’s works on eschatology
(Thy Kingdom Come: Studies in Daniel and Revelation and God’s Plan for
Victory: The Meaning of Postmillennialism) or the issue of The Journal
of Christian Reconstruction on the Millennium. And conspicuously
absent is any mention of the two appendixes in The Debate over

 

 

the day. Thy commandments make me wiser than my enemies, for they are ever
mine. I have more insight than all my teachers, for Thy testimonies are my
meditation.” Are we to conclude, based on the dispensationalist’s view of the law,
that Psalm 119 is no longer applicable during the so-called “church age”? How
about Psalm 23? What about the Book of Proverbs?

5. Lindsey, Kead le Holocaust, p. 157.

6. Rousas John Rushdoony, The Institutes of Biblical Law (Phillipsburg, NJ:
Presbyterian and Reformed, 1973), p. 75.
