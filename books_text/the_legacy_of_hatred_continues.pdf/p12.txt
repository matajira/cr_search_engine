xii The Legacy of Hatred Continues

swer our critics, then there remains the presumption of guilt: “You
can’t answer your critics because what they say about you is true.”
If we do answer, we’re accused of being contentious: “Why are
you dividing the already grievously divided body of Christ?” A
quick reading of the New Testament will show that the Apostles
were zealous for truth. That's our goal as well. We want to pre-
serve a legacy of truth and love so those dead in their trespasses
and sins might embrace Jesus Christ as their Lord and Savior. We
are asking Hal Lindsey to do the same. The Road to Holocaust is a
holocaust on the reputations of his brothers and sisters in Christ.
Hal Lindsey should apologize to those he defames, repent of his
false accusations, and have the book pulled from the market.
