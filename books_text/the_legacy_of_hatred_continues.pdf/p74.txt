FOR FURTHER READING
by Gary North

“You can’t beat something with nothing!” So says an old politi-
cal proverb. You have just read a critique of Hal Lindsey's The
Road to Holocaust. Maybe you have at least a few doubts about
what he says. But you may not be ready to re-think everything
you have always believed about Bible ethics and prophecy. You
want evidence that you are not being asked to give up everything,
getting little in return. You want evidence that there is something
better. You also want evidence that dispensational theology really
is incorrect.

That is what this brief reading list will provide: Bible-based
evidence. A longer version appears in the book, House Divided: The
Break-Up of Dispensational Theology (Tyler, TX: Institute for Chris-
tian Economics, 1989), But this will surely get you started.

Tf you are a typical dispensationalist, you have not been told
that other eschatological and ethical views are all centuries older
than yours, and that a large body of literature, scholarly and popu-
lar, has in the past been readily available to Bible-believing Chris-
tians. This is part of the great cover-up. Now you know better.

Tf you refuse to read books, read Psalm 110. Read First Corin-
thians 15. Jesus sits at God’s right hand in heaven. He will remain
there until His last enemy is defeated: death (I Cor. 15:25-26).
Jesus will not return physically until He is ready to put an end to
all death ~ physical death — at the final judgment. Psalm 110 is the
Old Testament passage quoted most often in the New Testament,
and it was the most widely quoted Old Testament passage until
the middle of the second century. It is a verse that cannot be ex-
plained sensibly by premillennialism.

62
