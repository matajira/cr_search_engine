6 The Legacy of Hatred Continues

You see, Christian Reconstructionists, who are predominately
postmillennial, are not “consciously anti-Semitic,” but their views
will lead to “anti-Semitism.” Hal Lindsey apparently claims to be
clairvoyant. This is slander and defamation of character and bor-
ders on libel, especially when he Aides the truth about the Chris-
tian Reconstructionists’ position, as we shall see!

But don’t breathe a sigh of relief if you’re not a postmillen-
nialist. Amillennialists and historic premillennialists get the same
treatment. For the amillennialist, the reasoning goes something
like this: Martin Luther was “anti-Semitic.” Martin Luther was
amillennial. Adolf Hitler shared Luther's views on the Jews.
Adolf Hitler killed six million Jews. Therefore, amillennialists are
unconsciously “anti-Semitic,” and their eschatology could lead
“Israel and us” to another holocaust.

We will contend that dispensational premillennialism leaves
the Jews without hope prior to the rapture. Dispensational pre-
millennialism has made a radical distinction between the church
and Israel with the result that God has forsaken Israel until after
the church is raptured. This is standard dispensationalism. There
can be no special status for Israel during the “Great Parenthesis,”
the mythical period of time in which Hal Lindsey and other dis-
pensationalists believe we are living. Israel has no future until after
the rapture when there will be ze Christians living on the earth for
a period of time.

On the other hand, postmillennialism is the only millennial
view that has a plan for Israel before the rapture, a view that has a
long history in the church. Postmillennialism is dependent upon the
conversion of the Jews. Hal Lindsey never tells his readers of
postmillennialism’s belief in a mass conversion of Jews. To
support any Jewish pogrom or holocaust would pull the key-
stone from postmillennialism. Why has Hal Lindsey failed to
alert his readers of our position?

The dispensationalist Hal Lindsey has a real problem on his
hands. With no favored status for Israel until after the rapture, he
must find a way to divert attention from the “unconscious” “anti-
