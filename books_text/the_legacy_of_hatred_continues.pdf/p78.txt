66 The Legacy of Hatred Continues

Sutton, Ray R. “Does Israel Have a Future?” Covenant Renewal
(December 1988). Examines several different views of Israel's
future, and argues for the covenantal view. Free.

Toon, Peter, ed. Puritans, the Millennium and the Future of Israel: Purt-
tan Eschatology 1600-1660, Cambridge: James Clarke, 1970.
Detailed historical study of millennial views with special atten-
tion to the place of Israel in prophecy. Out of print.

Works Critical of Dispensationalism

Allis, Oswald T. Prophecy and the Church. Philadelphia, PA: Presby-
terian and Reformed, 1945. Classic comprehensive critique of
dispensationalism. It has never been refuted.

Bacchiocchi, Samuele. Hal Lindsey’s Prophetic Jigsaw Puzzle: Five
Predictions That Failed! Berrien Springs, MI: Biblical Perspec-
tives, 1987. Examines Lindsey's failed prophecies, yet argues
for an imminent Second Coming, The author is a Seventh
Day Adventist scholar.

Bahnsen, Greg L. and Kenneth L. Gentry. House Divided: The
Break-Up of Dispensational Theology, Tyler, TX: Institute for
Christian Economics. Response to H. Wayne House and
‘Thomas Ice, Dominion Theology: Blessing or Curse?. A devastat-
ing refutation that leaves modern dispensational theology
without a theological leg to stand on. It ends the debate.

Boettner, Loraine. The Millennium. Revised edition. Phillipsburg,
NJ: Presbyterian and Reformed, [1957] 1984. Classic study of
millennial views, and defense of postmillennialism.

Brown, David. Christ's Second Coming: Will It Be Premillennial?
Grand Rapids, MI: Baker, [1976] 1983. Detailed exegetical
study of the Second Coming and the Millennium.

Crenshaw, Curtis I. and Grover E. Gunn, IU. Dispensattonalism
Today, Yesterday, and ‘lomorrow. Memphis, TN: Footstool
Publications, 1985. Two Dallas Seminary graduates take a cri-
tical and comprehensive look at dispensationalism.

DeMar, Gary. The Debate Over Christian Reconstruction. Ft. Worth,
TX: Dominion Press, 1988. Response to Dave Hunt and
Thomas Ice.
