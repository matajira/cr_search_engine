Other books by Gary DeMar

God and Government:
A Biblical and Historical Study, 1982

God and Government:
Issues in Biblical Perspective, 1984

God and Government:
The Restoration of the Republic, 1986

Ruler of the Nations:
Biblical Blueprints for Government, 1987

The Reduction of Christianity:
A Biblical Response to Dave Hunt, 1988
(with Peter Leithart)}

Surviving College Successfully:
A Complete Manual for the Rigors
of Academic Combat, 1988

Something Greater Ts Here, 1988

The Debate over Christian Reconstruction, 1988
You've Heard It Said, 1989

Paying for the College of Your Choice, 1990

Other books by Peter J. Leithart

A Christian Response ta Dungeons and Dragons:
The New Age Catechism, 1987
(with George Grant)
