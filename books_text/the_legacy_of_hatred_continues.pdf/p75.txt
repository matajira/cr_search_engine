For Further Reading 63

A Positive Ethical Alternative: Biblical Law
Bahnsen, Greg L. By This Standard: The Authority of God's Law To-
day, Tyler, TX; Institute for Christian Economics, 1985.
Bahnsen, Greg L. Theonomy and Christian Ethics, Phillipsburg,
New Jersey: Prersbyterian & Reformed, (1977) 1984.
Jordan, James. The Law of the Covenant: An Exposition of Exodus
21-23. Tyler, TX: Institute for Christian Economics, 1984,
North, Gary. Tools of Dominion: The Case Laws of Exodus. 2 vols.
Tyler, TX: Institute for Christian Economics, 1989,
Rushdvony, Rousas John. The Institutes of Biblical Law. Phillips-
burg, New Jersey: Presbyterian & Reformed, (1973) 1988.

Rushdoony, Rousas John. Law and Liberty Vallecito, CA: Ross
House Books, (1971) 1988.

A Positive Eschatological Alternative: Postmillennialism

Alexander, J. A. The Prophecies of Isaiah, A Commentary on Matthew
(complete through chapter 16), A Commentary on Mark, and A
Commentary on Acts. Various Publishers; Zondervan especially,
Nineteenth-century Princeton Seminary Old Testament scholar.

Brown, John, The Discourses and Sayings of Our Lord and commen-
taries on Romans, Hebrews, and 1 Peter. Various publishers.
Nineteenth-century Scottish Calvinist.

Campbell, Roderick. fsrael and the New Covenant. Phillipsburg,
New Jersey: Presbyterian & Reformed, [1954] 1981. Neglected
study of principles for interpretation of prophecy; examines
themes in New Testament biblical theology. A very good,
easy-to-read introduction,

Chilton, David. The Duys of Vengeance: An Exposition of the Book of
Revelation, Ft. Worth, TX: Dominion Press. Massive postmil-
lennial commentary on the Book of Revelation

Chilton, David. The Great Tribulation. Ft. Worth, TX: Dominion
Press, 1987. Popular exegetical introduction to postmillennial
interpretation.
