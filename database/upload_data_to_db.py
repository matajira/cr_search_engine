import glob
import os
import re
from elasticsearch import Elasticsearch

PATH_FOR_BOOKS = "../books_text/"
ES_INDEX = "books"
ES_DOC_TYPE = "books"

class BookPage():
    def __init__(self, path):
        self.page = re.search("p([0-9]*).txt", path).group(1) if re.search("p([0-9]*).txt", path) else ""
        self.book = re.search(".*/(.*)/p[0-9]*.txt", path).group(1) if re.search(".*/(.*)/p[0-9]*.txt", path) else ""
        self.path = re.search("(.*.txt)", path).group(1) if re.search("(.*.txt)", path) else ""
        self.key = self.path + self.book + self.page
        self.text = self.read_page_text()

    def read_page_text(self):
        if self.path:
            with open(self.path, "r") as file:
                return file.read().encode("utf-8")
        return ""

    def write_to_elasticsearch(self, es, index, doc_type):
        dictionary = dict(page=self.page, book=self.book, path=self.path, text=self.text, id=self.key)
        es.index(index=index, doc_type=doc_type, body=dictionary)


def initialize_connection_to_es_and_assure_index_exist():
    es = Elasticsearch(host='localhost')
    try:
        es.indices.delete(index="books")
        es.indices.create(index="books")
    except Exception as error:
        print(error.args)
    return es

def get_path_for_all_the_text_pages():
    return glob.glob(pathname=f"{PATH_FOR_BOOKS}/*/*.txt", recursive=True)

def upload_all_pages_to_elastic_search(all_the_txt_files, es):
    for i in all_the_txt_files:
        book_page = BookPage(i)
        book_page.write_to_elasticsearch(es=es, index=ES_INDEX, doc_type=ES_DOC_TYPE)

if __name__ == "__main__":
    os.chdir(PATH_FOR_BOOKS)
    es = initialize_connection_to_es_and_assure_index_exist()
    all_the_txt_files = get_path_for_all_the_text_pages()
    upload_all_pages_to_elastic_search(all_the_txt_files, es)
