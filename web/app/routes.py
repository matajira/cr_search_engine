from flask import render_template, request
from app import app
from app.forms import SearchForm
import subprocess
import re
from app.search import Search, Match

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')

@app.route('/search', methods=['GET', 'POST'])
def search():
    form = SearchForm()
    if form.validate_on_submit():
        text_to_search = request.form.get("search")
        return search_text_in_books(form, text_to_search)
    return render_template('search.html', title='Results', form=form)

def search_text_in_books(form, text):
    search_results = Search.search_this_text_es(text)
    return render_template('search.html', title='Results', form=form, search_results=search_results)
