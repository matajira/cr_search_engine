from datetime import date
from elasticsearch import Elasticsearch

class Search:
    def search_this_text_es(text):
        def query_es(text):
            es = Elasticsearch(host='localhost')
            body = """
            {
            "query" : {
            "match": { "text": "PLACEHOLDER" }
            },
            "highlight" : {
            "tags_schema": "styled",
            "pre_tags" : ["<mark><b>"],
            "post_tags" : ["</mark></b>"],
            "number_of_fragments": 0,
            "fields" : {
            "text" : {}
            }
            }
            }
            """
            full_search_result = es.search(body=body.replace("PLACEHOLDER", text), index="books", size=100)
            return full_search_result

        def create_match_list_from_elasticsearch_result(elasticsearch_result):
            return [Match(text=x["_source"]["text"], page=int(x["_source"]["page"]), highlights=x["highlight"]["text"], book=x["_source"]["book"], path=x["_source"]["path"]) for x in elasticsearch_result["hits"]["hits"]]

        full_search_result = query_es(text)
        match_list = create_match_list_from_elasticsearch_result(full_search_result)
        return match_list


class Match:
    def __init__(self, text, book, page, highlights, path):
        if text == "":
            raise("Text should not be empty")
        if book == "":
            raise("Book should not be empty")
        if not isinstance(page, int):
            raise("Page should be a number")
        self.text = text
        self.book = book
        self.page = page
        self.highlights = highlights
        self.path = path
        self.whole_page_text = self.load_whole_text()

    def load_whole_text(self):
        with open(self.path, "r") as file:
            return file.read()

